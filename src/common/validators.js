import { validURL } from "utils/regex";

export const checkedURL = str => {
  return !!validURL.test(str);
}

const checked = (value, options) => {
  if (value !== true) {
    return options.message || 'must be checked';
  }
};
export default {
  checked
};
