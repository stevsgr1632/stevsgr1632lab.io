import * as ActionType from './contant';

const initState = '';
const STORAGE = 'historyRedux';
const reducer = (state = initState, action) => {
  const { type, value } = action;
  
  switch (type) {
    case ActionType.PATHNAME_BEFORE:
      localStorage.setItem(STORAGE, value);
      return value;
    default:
      state = localStorage.getItem(STORAGE);
      return state;
    }
}

export default reducer;
