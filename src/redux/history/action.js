import store from '../store';
import * as ActionType from './contant';

export default {
  getPathNameBefore: (value) => {
    const action = { type: ActionType.PATHNAME_BEFORE, value };
    store.dispatch(action);
  }
}