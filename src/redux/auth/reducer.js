import * as ActionType from './contant';
import { decodeToken } from "utils/initialEndpoint";
import localStorageService from "utils/localStorageService";

const initState = {};
const initializeState = () => {
  const auth = localStorageService.state_auth.getAccessToken();
  const state = auth ? auth : initState;
  return state;
};

const reducer = (state = initializeState(), action) => {
  const { type, value } = action;
  let attributes;
  let accessToken;
  let refreshToken;

  switch (type) {
    case ActionType.AUTH_SET:
      attributes = value.attributes || {};
      accessToken = attributes.accessToken;
      refreshToken = attributes.refreshToken;

      attributes.accessTokenExpired = decodeToken(accessToken);
      attributes.refreshTokenExpired = decodeToken(refreshToken);

      localStorageService.state_auth.setToken(value);
      return { ...value };
    case ActionType.AUTH_SET_TOKEN:
      attributes = { ...state.attributes, ...value.attributes };
      accessToken = attributes.accessToken;
      refreshToken = attributes.refreshToken;

      attributes.accessTokenExpired = decodeToken(accessToken);
      attributes.refreshTokenExpired = decodeToken(refreshToken);

      const auth = { ...state, attributes };
      localStorageService.state_auth.setToken(auth);
      return { ...auth };
    case ActionType.AUTH_CLEAR:
      localStorageService.state_auth.clearToken();
      localStorage.clear();
      sessionStorage.clear();
      return {}
    default:
      return state;
  }
}

export default reducer;
