import * as ActionType from './contant';

const initState = '';
const STORAGE = 'menuAccess';

export const roleReducer = (state = '', action) => {
  const { type, value } = action;
  
  switch (type) {
    case ActionType.SET_ROLE_ACCESS:
      return value;
    default:
      return state;
  }
}

const reducer = (state = initState, action) => {
  const { type, value } = action;
  
  switch (type) {
    case ActionType.SET_MENU_ACCESS:
      localStorage.setItem(STORAGE, JSON.stringify(value));
      return value;
    default:
      return state;
    }
}

export default reducer;
