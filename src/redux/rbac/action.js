import store from '../store';
import * as ActionType from './contant';

export default {
  setMenuAccess: (value) => {
    const action = { type: ActionType.SET_MENU_ACCESS, value };
    store.dispatch(action);
  },
  setRoleAccess: (value) => {
    const action = { type: ActionType.SET_ROLE_ACCESS, value };
    store.dispatch(action);
  },
}