import auth from './auth/reducer';
import lang from './lang/reducer';
import historyRedux from './history/reducer';
import menuAccess, { roleReducer as roleAkses } from './rbac/reducer';
import userInfo from './userInfo/reducer';

export default { auth, lang, historyRedux, menuAccess, roleAkses, userInfo };
