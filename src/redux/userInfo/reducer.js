import * as ActionType from './contant';

const initState = {};
export const storage = 'userInfo';
const reducer = (state = initState, action) => {
  const { type, value } = action;
  
  switch (type) {
    case ActionType.SET_USER_INFO:
      window.localStorage.setItem(storage,JSON.stringify(value));
      return value;
    default:
      return state;
    }
}

export default reducer;
