import store from '../store';
import * as ActionType from './contant';

export default {
  setUserInfo: (value) => {
    const action = { type: ActionType.SET_USER_INFO, value };
    store.dispatch(action);
  }
}