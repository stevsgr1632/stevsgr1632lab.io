export default {
    items: [
        { name: 'Home', url: '/home', icon: 'icon-home' },
        { name: 'Dashboard', url: '/dashboard', icon: 'icon-speedometer' },

        // { title: true, name: 'Konten Digital' },
        {
            name: 'Konten Digital',
            icon: 'icon-notebook',
            url: '/digital',
            children: [
                { name: 'Semua Konten', url: '/digital/all', icon: 'icon-layers' },
                { name: 'Buku Elektronik', url: '/digital/ebook', icon: 'icon-screen-tablet' },
                { name: 'Audio-Video Elektronik', url: '/digital/audio', icon: 'icon-film' },
                { name: 'Jurnal Elektronik', url: '/digital/journal', icon: 'icon-drop' },
            ]
        },

        // { title: true, name: 'Konten Fisik' },
        {
            name: 'Konten Fisik',
            icon: 'icon-book-open',
            url: '/content',
            children: [
                { name: 'Semua Konten', url: '/content', icon: 'icon-screen-tablet' },
                { name: 'Buku Cetak', url: '/content/form', icon: 'icon-screen-tablet' },
                { name: 'ATK', url: '/content/atk', icon: 'icon-printer' },
            ]
        },

        // { name: 'List', url: '/com/list' },
        {
            name: 'Organsasi',
            url: '/table',
            icon: 'icon-organization',
            children: [
                { name: 'Group', url: '/holding/list' },
                { name: 'Penerbit', url: '/organisasi' }
            ],
        },
        {
            name: 'Kategori Produk',
            icon: 'icon-folder',
            url: '/kategori',
            children: [
                { name: 'Kategori', url: '/kategori/index' },
                { name: 'Jenis', url: '/kategori/jenis' },
            ]
        },
        {
            name: 'Ilibrary & ePustaka',
            icon: 'icon-book-open',
            url: '/ilibrary',
            children: [
                { name: 'ePustaka', url: '/epustaka' },
                { name: 'Ilibrary', url: '/ilibrary' },
            ]
        },
        {
            name: 'Pengadaan',
            icon: 'icon-drawer',
            url: '/pengadaan',
            children: [
                { name: 'Rencana Anggaran Biaya (RAB)', url: '/rab' },
            ]
        },
        // { name: 'Anggota', url: '/anggota', icon: 'icon-people' },
        { name: 'Librarian', url: '/librarian', icon: 'icon-people' },
        {
            name: 'CRUD',
            url: '/crud',
            icon: 'icon-grid',
            children: [
                { name: 'Simple Crud', url: '/crud/simple', icon: 'icon-tag' },
            ]
        },

        {
            name: 'Pengadaan',
            url: '/procurement',
            icon: 'icon-envelope-letter',
            children: [
                { name: 'RAB Perencanaan', icon: 'icon-folder-alt', url: '/procurement/plan' },
                { name: 'Rencana Anggaran (RAB)', icon: 'icon-folder-alt', url: '/procurement/rab' },
                { name: 'Pengadaan Berjalan', icon: 'icon-docs', url: '/procurement/ongoing' }
            ]
        },

        {
            name: 'Utility',
            url: '/utility',
            icon: 'icon-envelope-letter',
            children: [
                { name: 'Document', icon: 'icon-folder-alt', url: '/utility/document' }
            ]
        },

        { divider: true },

        { title: true, name: 'Konfigurasi' },
        {
            name: 'Master',
            icon: '',
            children: [
                { name: 'Jenis Dokumen', url: '/master/document-type', icon: 'icon-calculator' },
                { name: 'Tahun Anggaran', url: '/master/fiscal-year', icon: 'icon-calendar' },
                { name: 'Sumber Pendanaan', url: '', icon: 'icon-calculator' },
                { name: 'Status', url: '', icon: 'icon-calculator' },
                { name: 'Bank', url: '/master/bank', icon: 'icon-calculator' }
            ]
        },
        {
            name: 'Pengaturan',
            url: '/pages',
            icon: 'icon-wrench',
            children: [
                { name: 'Pengguna Aplikasi', url: '/login', icon: 'icon-people' },
                { name: 'Register', url: '/register', icon: 'icon-star' },
                { name: 'Error 404', url: '/404', icon: 'icon-star' },
                { name: 'Error 500', url: '/500', icon: 'icon-star' },
            ],
        },
        // {
        //     name: 'Disabled',
        //     url: '/dashboard',
        //     icon: 'icon-ban',
        //     attributes: { disabled: true },
        // },
    ],
};