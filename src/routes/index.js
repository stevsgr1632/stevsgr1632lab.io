import React from 'react';
import pathName from './pathName';
const {
  transaction, utility,
  organisasi, procurement,
  digital, penerbit, kategori,
  usermanagement, kontenDigital,
  dashboardCatalog,dashboardDinas,
  iLibEpus, anggota,holding, librarian,
  profile, settings, dashboardPublisher, 
  initial, forbidden, home, login, dashboard,
  rab: { admin, dinas, catalog, selectcontent },
  
} = pathName;

const routes = [
  { path: initial, component: React.lazy(() => import('views/_pages/Initial')) },
  { path: forbidden, component: React.lazy(() => import('views/_pages/NotAllowed')) },
  { path: login, component: React.lazy(() => import('views/_pages/LoginPage')) },
  { path: home, component: React.lazy(() => import('views/_pages/Dashboard')) },
  { path: profile, component: React.lazy(() => import('views/_pages/ProfilePage')) },
  { path: settings, component: React.lazy(() => import('views/_pages/SettingsPage')) },
  { path: dashboard, component: React.lazy(() => import('views/_pages/Dashboard')), exact: true },
  { path: dashboardCatalog, component: React.lazy(() => import('views/_pages/DashboardCatalog')), exact: true },
  { path: dashboardPublisher.main, component: React.lazy(() => import('views/_pages/DashboardPenerbit')), exact: true },
  { path: dashboardPublisher.penerbit, component: React.lazy(() => import('views/dashboardPenerbit/secondDashboard')), exact: true },
  { path: dashboardDinas.main, component: React.lazy(() => import('views/_pages/DashboardDinas')), exact: true },
  { path: dashboardDinas.pengadaan(':year'), component: React.lazy(() => import('views/dashboardDinas/Pengadaan')), exact: true },
  { path: digital.listAll, component: React.lazy(() => import('views/digitalContent/Semua')), exact: true },
  { path: digital.listEbook, component: React.lazy(() => import('views/digitalContent/Ebook/IndexPublish')), exact: true },
  { path: digital.listEbookUnpublished, component: React.lazy(() => import('views/digitalContent/Ebook/IndexUnpublish')), exact: true },
  { path: digital.listEbookAdd, component: React.lazy(() => import('views/digitalContent/Ebook/add')), exact: true,type:'Tambah' },
  { path: digital.listUnpublish, component: React.lazy(() => import('views/digitalContent/Unpublish')), exact: true},
  { path: digital.listUnpublishExport, component: React.lazy(() => import('views/digitalContent/Unpublish/ExportFile')), exact: true },
  { path: digital.importFileCatalog, component: React.lazy(() => import('views/digitalContent/Ebook/component/ImportFileCatalog')), exact: true },
  { path: digital.exportFileCatalog, component: React.lazy(() => import('views/digitalContent/Ebook/component/ExportFileCatalog')), exact: true },
  { path: digital.listEbookEdit(':id'), component: React.lazy(() => import('views/digitalContent/Ebook/edit')), type:'Sunting'},
  { path: digital.showEbook(':id'), component: React.lazy(() => import('views/digitalContent/Ebook/detail')), exact: true },
  { path: digital.readEbook(':id'), component: React.lazy(() => import('views/digitalContent/Ebook/component/Baca')), exact: true },
  { path: digital.listAudio, component: React.lazy(() => import('views/digitalContent/Audio')), exact: true },
  { path: digital.listJournal, component: React.lazy(() => import('views/digitalContent/Journal')), exact: true },

  { path: kategori.list, component: React.lazy(() => import('views/kategoriProduk/Kategori')), exact: true },
  { path: kategori.add, component: React.lazy(() => import('views/kategoriProduk/Kategori/FormAdd')),type:'Tambah' },
  { path: kategori.edit(':id'), component: React.lazy(() => import('views/kategoriProduk/Kategori/FormEdit')),type:'Sunting' },

  { path: kategori.jenis, component: React.lazy(() => import('views/kategoriProduk/Jenis')) },
  { path: penerbit.digitalContent.listAll, component: React.lazy(() => import('views/digitalContent/Ebook')), exact: true },

  { path: admin.list, component: React.lazy(() => import('views/pengadaan/Admin/RAB')), exact: true },
  { path: admin.add, component: React.lazy(() => import('views/pengadaan/Admin/RAB/FormAdd.jsx')), exact: true,type:'Tambah' },
  { path: dinas.list.rencana, component: React.lazy(() => import('views/pengadaan/Dinas/RAB')), exact: true },
  { path: dinas.list.berjalan, component: React.lazy(() => import('views/pengadaan/Dinas/RAB/indexBerjalan.jsx')), exact: true },
  { path: dinas.list.selesai, component: React.lazy(() => import('views/pengadaan/Dinas/RAB/indexSelesai.jsx')), exact: true },
  { path: dinas.add, component: React.lazy(() => import('views/pengadaan/Dinas/RAB/FormAdd.jsx')), exact: true,type:'Tambah' },

  { path: catalog.list, component: React.lazy(() => import('views/pengadaan/Catalog')), exact: true },
  { path: selectcontent.list, component: React.lazy(() => import('views/pengadaan/SeleksiContent')), exact: true },
  { path: selectcontent.add, component: React.lazy(() => import('views/pengadaan/SeleksiContent/FormAdd.jsx')), exact: true,type:'Tambah' },
  { path: selectcontent.buatBerjalan(':id'), component: React.lazy(() => import('views/pengadaan/SeleksiContent/JadikanBerjalan.jsx')), exact: true },
  { path: selectcontent.edit(':id'), component: React.lazy(() => import('views/pengadaan/SeleksiContent/FormEdit.jsx')),type:'Sunting', exact: true },
  { path: catalog.add, component: React.lazy(() => import('views/pengadaan/Catalog/FormAdd.jsx')),type:'Tambah' },

  { path: admin.edit(':id'), component: React.lazy(() => import('views/pengadaan/Admin/RAB/FormEdit.jsx')),type:'Sunting', exact: true },
  { path: admin.detail(':id'), component: React.lazy(() => import('views/pengadaan/Admin/RAB/GetDetail.jsx')), exact: true },
  { path: admin.projectSelect(':id'), component: React.lazy(() => import('views/pengadaan/Admin/RAB/ProjectSelect.jsx')), exact: true },

  { path: dinas.edit(':id'), component: React.lazy(() => import('views/pengadaan/Dinas/RAB/FormEdit.jsx')),type:'Sunting',  exact: true },
  { path: dinas.detail(':id'), component: React.lazy(() => import('views/pengadaan/Dinas/RAB/GetDetail.jsx')), exact: true },
  { path: dinas.projectSelect(':id'), component: React.lazy(() => import('views/pengadaan/Dinas/RAB/ProjectSelect.jsx')), exact: true },

  { path: kontenDigital.listCollection, component: React.lazy(() => import('views/kontenDigital/koleksi')), exact: true },
  { path: kontenDigital.detail(':epustaka_id/:catalog_id'), component: React.lazy(() => import('views/kontenDigital/koleksi/DetailForm')), exact: true },

  { path: iLibEpus.ilibrary, component: React.lazy(() => import('views/ilibraryEpustaka/Ilibrary')), exact: true },
  { path: iLibEpus.epustaka.list, component: React.lazy(() => import('views/Epustaka')), exact: true },
  { path: iLibEpus.epustaka.add, component: React.lazy(() => import('views/Epustaka/FormAdd')), exact: true,type:'Tambah' },
  { path: iLibEpus.epustaka.edit(':id'), component: React.lazy(() => import('views/Epustaka/FormEdit')),type:'Sunting',  exact: true },
  { path: iLibEpus.epustaka.catalog(':id'), component: React.lazy(() => import('views/Epustaka/catalog')), exact: true },
  { path: iLibEpus.epustaka.catalog(':epustaka_id/:catalog_id'), component: React.lazy(() => import('views/Epustaka/pengadaan')), exact: true },

  { path: anggota.list, component: React.lazy(() => import('views/anggota')), exact: true },
  { path: anggota.add, component: React.lazy(() => import('views/anggota/FormAdd')),type:'Tambah' },
  { path: anggota.edit(':id'), component: React.lazy(() => import('views/anggota/FormEdit')),type:'Sunting' },

  { path: holding.list, component: React.lazy(() => import('views/organisasi/OrganizationGroup')), exact: true },
  { path: holding.add, component: React.lazy(() => import('views/organisasi/OrganizationGroup/FormAdd')), type:'Tambah' },
  { path: holding.edit(':id'), component: React.lazy(() => import('views/organisasi/OrganizationGroup/FormEdit')), type:'Sunting' },

  { path: organisasi.list, component: React.lazy(() => import('views/organisasi/Penerbit')), exact: true },
  { path: organisasi.add, component: React.lazy(() => import('views/organisasi/Penerbit/FormAdd')),type:'Tambah' },
  { path: organisasi.edit(':id'), component: React.lazy(() => import('views/organisasi/Penerbit/FormEdit')),type:'Sunting' },

  // { path: '/organisasi', component: React.lazy(() => import('views/organisasi/Penerbit')), exact: true },
  { path: librarian.add, component: React.lazy(() => import('views/librarians/FormAdd')),type:'Tambah' },

  { path: procurement.list, component: React.lazy(() => import('views/procurement/Plan')), exact: true },
  { path: procurement.add, component: React.lazy(() => import('views/procurement/Plan/add')), exact: true,type:'Tambah' },
  { path: procurement.edit(':edit'), component: React.lazy(() => import('views/procurement/Plan/edit')), exact: true,type:'Sunting' },

  { path: transaction.penerbit.list, component: React.lazy(() => import('views/transaction/Penerbit')) },
  { path: transaction.admin.list, component: React.lazy(() => import('views/transaction/Admin')) },

  { path: usermanagement.user.list, component: React.lazy(() => import('views/usermanagement/User')), exact: true },
  { path: usermanagement.user.add, component: React.lazy(() => import('views/usermanagement/User/FormAdd')), exact: true,type:'Tambah' },
  { path: usermanagement.user.edit(':id'), component: React.lazy(() => import('views/usermanagement/User/FormEdit')),type:'Sunting' },

  { path: usermanagement.role.list, component: React.lazy(() => import('views/usermanagement/user-group')), exact: true },
  { path: usermanagement.role.add, component: React.lazy(() => import('views/usermanagement/user-group/FormAdd')), exact: true,type:'Tambah' },
  { path: usermanagement.role.edit(':id'), component: React.lazy(() => import('views/usermanagement/user-group/FormEdit')),type:'Sunting' },
  { path: usermanagement.role.roleAkses(':id'), component: React.lazy(() => import('views/usermanagement/user-group/RoleAkses')) },

  { path: utility.penerbit.announcement.list, component: React.lazy(() => import('views/utility/Penerbit/Announcement')), exact: true },
  { path: utility.penerbit.document.list, component: React.lazy(() => import('views/utility/Penerbit/NewsDocument')), exact: true },

  { path: utility.announcement.list, component: React.lazy(() => import('views/utility/Announcement')), exact: true },
  { path: utility.announcement.add, component: React.lazy(() => import('views/utility/Announcement/FormAdd')),type:'Tambah' },
  { path: utility.announcement.edit(':id'), component: React.lazy(() => import('views/utility/Announcement/FormEdit')),type:'Sunting' },

  { path: utility.document.list, component: React.lazy(() => import('views/utility/news-document')), exact: true },
  { path: utility.document.add, component: React.lazy(() => import('views/utility/news-document/FormAdd')),type:'Tambah' },
  { path: utility.document.edit(':id'), component: React.lazy(() => import('views/utility/news-document/FormEdit')),type:'Sunting' },
];

export default routes;
