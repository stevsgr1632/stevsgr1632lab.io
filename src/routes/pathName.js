export default {
  base: '/',
  developer: 'https://aksaramaya.com',
  login: '/login',
  register: '/register',
  forgotPass: '/lupa-password',
  home: '/home',
  profile: '/profile',
  settings: '/settings',
  forbidden: '/forbidden',
  initial: '/initial',
  dashboard: '/dashboard',
  dashboardPublisher: {
    main:'/dashboard-publisher',
    penerbit:'/dashboard-publisher/imprint'
  },
  dashboardDinas: {
    main: '/dashboard-instansi',
    pengadaan : params => `/dashboard-instansi/pengadaan/${params}`
  },
  dashboardCatalog: '/dashboard-catalog',
  digital: {
    listAll: '/digital/all',
    listEbook: '/digital/ebook',
    listEbookUnpublished: '/digital/ebookunpublished',
    listEbookAdd: '/digital/ebook/add',
    listEbookEdit: params => `/digital/ebook/edit/${params}`,
    listUnpublish : '/digital/unpublish',
    listUnpublishExport : '/digital/unpublish/export',
    listAudio: '/digital/audio',
    listJournal: '/digital/journal',
    showEbook: params => `/digital/ebook/${params}`,
    readEbook: params => `/digital/ebook/baca/${params}`,
    importFileCatalog: '/digital/ebook/import',
    exportFileCatalog: '/digital/ebook/export'
  },
  penerbit: {
    digitalContent: {
      listAll: '/pub-digital/ebookpublisher'
    }
  },
  kontenDigital: {
    listCollection: '/digital/collection',
    detail:params => `/digital/collection/${params}`,
    detailColletion:params => `/digital/collection/${params.epustaka_id}/${params.catalog_id}`
  },
  transaction: {
    admin: {
      list: '/transaction-admin',
      add: '/transaction-admin/add'
    },
    penerbit: {
      list: '/transaction',
      add: '/transaction/add'
    }
  },
  kategori: {
    list: '/kategori/index',
    add: '/kategori/index/add',
    edit: params => `/kategori/index/${params}`,
    jenis: '/kategori/index/jenis',
  },
  rab: {
    admin: {
      list: '/rab',
      add: '/rab/add',
      edit: params => `/rab/${params}`,
      detail: params => `/rab/detail/${params}`,
      projectSelect: params => `/rab/detail/${params}/project-selection`,

    },
    dinas: {
      list: {
        rencana: '/rab/dinas',
        berjalan: '/rab/dinas/berjalan',
        selesai: '/rab/dinas/selesai'
      },
      add: '/rab/dinas/add',
      edit: params => `/rab/dinas/${params}`,
      detail: params => `/rab/dinas/detail/${params}`,
      projectSelect: params => `/rab/dinas/detail/${params}/project-selection`,
    },
    catalog: {
      list: '/procurementList',
      add: '/procurementList/add'
    },
    selectcontent: {
      list: '/content-selection',
      add: '/content-selection/add',
      edit: params => `/content-selection/edit/${params}`,
      buatBerjalan: params => `/content-selection/jadikan-berjalan/${params}`,
    }
  },
  iLibEpus : {
    ilibrary:'/ilibrary',
    epustaka:{
      list:'/epustaka',
      add:'/epustaka/add',
      edit:params => `/epustaka/edit/${params}`,
      catalog:params => `/epustaka/${params}`,
      pengadaan:params => `/epustaka/${params.epustaka_id}/${params.catalog_id}`
    }
  },
  anggota: {
    list: '/librarian',
    add: '/librarian/add',
    edit: params => `/librarian/${params}`,
  },
  holding: {
    list: '/holding/list',
    add: '/holding/list/add',
    edit: params => `/holding/list/${params}`,
  },
  organisasi: {
    list: '/organisasi',
    add: '/organisasi/add',
    edit: params => `/organisasi/${params}`,
  },
  librarian: {
    add: '/librarian/add',
  },
  procurement: {
    list: '/procurement/plan',
    add: '/procurement/plan/add',
    edit: params => `/procurement/plan/${params}`,
  },
  usermanagement: {
    user: {
      list: '/usermanagement/user',
      add: '/usermanagement/user/add',
      edit: params => `/usermanagement/user/${params}`,
    },
    role: {
      list: '/usermanagement/role',
      add: '/usermanagement/role/add',
      edit: params => `/usermanagement/role/${params}`,
      roleAkses: params => `/usermanagement/role-akses/${params}`,
    }
  },
  utility: {
    penerbit: {
      announcement: {
        list: '/gen-announcement',
      },
      document: {
        list: '/gen-document',
      }
    },
    announcement: {
      list: '/utility/announcement',
      add: '/utility/announcement/add',
      edit: params => `/utility/announcement/${params}`,
    },
    document: {
      list: '/utility/document',
      add: '/utility/document/add',
      edit: params => `/utility/document/${params}`,
    }
  }
};