import React from 'react';
import PustakaList from './PustakaList';
import LoadingSpin from 'components/LoadingSpin';
import { Modal, Row, Col,Typography } from 'antd';
import { noImagePath } from "utils/initialEndpoint";
/* eslint-disable */

const PustakaDetail = (props) => {
    
    const { visible, loading, setVisible,info,setState, setInfo,state} = props;
    const { Text } = Typography;
    const handleDataDetail = () =>{
        setVisible(false);
        setInfo({});
        const pagination = {...state.pagination, total: 0};
        setState({...state,dataSource:[],pagination});

    }


    return (
        <Modal 
            width={1000}
            visible={visible} 
            onCancel={() => handleDataDetail()}
            footer={null}>
            <h3>Informasi ePustaka</h3>
            <hr/>
            <LoadingSpin loading={loading} >
               <Row style={{fontWeight: 'normal', fontSize:'11pt'}} gutter={[16, 36]}>
                    <Col xs={24} sm={16} md={5} lg={4} xl={4}>
                        <img src={(info.epustaka_logo) ? info.epustaka_logo : noImagePath } alt={(info.epustaka_logo) ? info.epustaka_logo : noImagePath } width={'100%'}/>
                    </Col>
                    <Col xs={16} sm={16} md={10} lg={10} xl={10}>
                        <Row>
                            <Col flex="100px">Ilibrary</Col>
                            <Col flex="auto">: {info.ilibrary_name ? info.ilibrary_name : '-'}</Col>
                        </Row>
                        <Row>
                            <Col flex="100px">ePustaka</Col>
                            <Col flex="auto">: {info.epustaka_name ? info.epustaka_name : '-'}</Col>
                        </Row>
                        <Row>
                            <Col flex="100px">email</Col>
                            <Col flex="auto">: {info.epustaka_email ? info.epustaka_email : '-'}</Col>
                        </Row>
                        <Row>
                            <Col flex="100px">Member</Col>
                            <Col flex="auto">: {info.member_qty ? `${info.member_qty} Orang` : '-'} </Col>
                        </Row>
                        <Row>
                            <Col flex="100px">Katalog</Col>
                            <Col flex="auto">: {info.catalog_qty ? `${info.catalog_qty} Buku` : '-'} </Col>
                        </Row>
                        <Row>
                            <Col flex="100px">Copy</Col>
                            <Col flex="auto">: {info.catalog_copy ? info.catalog_copy : '-'}</Col>
                        </Row>
                    </Col>
                    <Col xs={24} sm={16} md={8} lg={10} xl={10}>
                        <Text style={{fontSize:'12pt'}} strong>Klasifikasi Buku</Text>
                        <PustakaList {...props} />
                    </Col>
               </Row>
            </LoadingSpin>
        </Modal>
    )
}

export default PustakaDetail;
