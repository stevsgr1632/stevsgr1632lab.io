import React, { Fragment, useState } from 'react';
import {List,Card,Button} from 'antd';
import './styles/listcard.scss';
import pathName from 'routes/pathName';
import { Link } from 'react-router-dom';
import PustakaDetail from './PustakaDetail';
import oProvider from 'providers/oprovider';
import ModalDelete from './component/ModalDelete';
import { noImagePath } from "utils/initialEndpoint";
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
/* eslint-disable */

const resource = 'epustaka-info';

const ListCard = (props) => {
    const { Meta } = Card;
    const { data } = props;
    const { iLibEpus : { epustaka : epustakaPath } } = pathName;
    const [visible,setVisible] = useState(false);
    const [loadingModal, setLoadingModal] = useState(false);
    const [limit,setLimit] = useState(10);
    const [info, setInfo] = useState({});
    const [state, setState] = useState({
      loading : false,
      pagination: {
        defaultCurrent: 1,
        pageSize: limit,
        total: 0 , 
        showSizeChanger: true
      },
      dataSource: []
    });

    const grid = { 
      gutter: 16,
      xs: 1,
      sm: 2,
      md: 3,
      lg: 4,
      xl: 5,
      xxl: 6,
    }

    const style = {
      card : { textAlign: 'center',padding:'10px'},
      total : {margin:'20px 0px'},
      btn : {margin:'auto'},
      img : {width:'100%',borderBottom:'1px solid #f0f0f0',paddingBottom:'5px'}
    }

    const showModal = async (params) => {
      setVisible(true);
      setLoadingModal(true);
      setState({...state,loading:true});
      try {
        const response = await oProvider.list(`${resource}?id=${params.id}`);
        
        if(response){
          const { data } = response;
          const { categories : dataSource } = data;
          const pagination = {...state.pagination,total:dataSource.length}
          setInfo({...data});
          setState({...state,dataSource,pagination,loading:false});
        }
        setLoadingModal(false);
      } catch (error) {
        console.log(error?.message);
        setLoadingModal(false);
        return false;
      }
    };

    const editEpustaka = (item) => {
      props.history.push(epustakaPath.edit(item.id));
    };
    
    const deleteEpustaka = (item) => {
      return ModalDelete(item,props.onDelete);
    };
    
    return (
      <Fragment>
        <List
          grid={grid}
          {...data}
          renderItem={item => (
          <List.Item>
            <Card 
              style={style.card}
              bodyStyle={style.card}
              cover={
              <div className="epustaka__list">
                <div className="epustaka__list--action">
                  <Button shape="circle" icon={<EditOutlined />} onClick={() => editEpustaka(item)}/>
                  <Button shape="circle" icon={<DeleteOutlined />} onClick={() => deleteEpustaka(item)}/>
                </div>
                <div className="epustaka__list--img-container">
                  <img 
                    alt="example" 
                    className="epustaka__list--cover" 
                    src={ (item.epustaka_logo) ? item.epustaka_logo : noImagePath} 
                  />
                </div>
              </div>}
            >
              <Meta title={item.epustaka_name} />
              {/* <h5>{item.ilibrary_name}</h5> */}
              <div style={style.total}>
                  <Link to="#" onClick={()=>props.viewList(item.id,item.epustaka_name)}><p>Jumlah Katalog : {(item.total) ? item.total : '0'}</p></Link>
              </div>
              <div style={style.btn}>
                  <Button type="primary" htmlType="button" block onClick={()=>showModal(item)}>Lihat Detail</Button>
              </div>
            </Card>
          </List.Item>
        )}
      />
      <PustakaDetail state={state} setVisible={setVisible} visible={visible} loading={loadingModal} info={info} setLimit={setLimit} setInfo={setInfo} setState={setState}/>
    </Fragment>
    )
}

export default ListCard;
