import React from 'react';
import { Button,Form, Row, Col,Space,
Typography, Divider,Modal } from 'antd';
import MessageMod from 'common/message';
import oProvider from 'providers/oprovider';
import LoadingSpin from 'components/LoadingSpin';
import InputText from 'components/ant/InputText';
import InputFile3 from 'components/ant/InputFile3';
import { noImagePath } from "utils/initialEndpoint";
import InputNumber from 'components/ant/InputNumber';
import InputSelect from 'components/ant/InputSelect';
import InputSwitch from 'components/ant/InputSwitch';
import MocoEditorQuill from 'components/MocoEditorQuill';
/* eslint-disable */

const FormContext = (props) => {
  const [form] = Form.useForm();
  const { handleSimpan, onCancel, schema, paramsId = '' } = props;
  const [dataApi,setdataApi] = React.useState({});
  const [dataImg,setdataImg] = React.useState({src: '', visible : false});
  const [dataSubmit,setdataSubmit] = React.useState({});
  const [loading,setloading] = React.useState(false);
  const contentData  = form.getFieldValue('epustaka_aboutus');

  const handleUploadFile = async (name,file,data) => {
    let reader = new FileReader();
    reader.onload = (e) => {
      setdataImg({...dataImg, src : data?.file_url_download});
    }
    reader.readAsDataURL(file);
    setdataSubmit({...dataSubmit, [name] : data?.file_url_download});
  };

  const handleChange = (name,value) => {
    if(['epustaka_utama','epustaka_isdefault','epustaka_isactive'].includes(name)) value = value ? 1 : 0;
    if(['loan_term','maximum_borrow'].includes(name)) value = parseInt(value);
    setdataSubmit({...dataSubmit, [name] : value });
  };

  const handleChangeEditor = (value) => {
    form.setFieldsValue({ epustaka_aboutus : value})
  }

  const showImage = () => {
    setdataImg({...dataImg, visible : true });
  };

  const handleSimpanValid = (e) => {
    
    e.preventDefault();
    form.validateFields()
    .then(values => {
      dataSubmit.epustaka_aboutus = values.epustaka_aboutus;
      handleSimpan(dataSubmit);
    })
    .catch(errorInfo => {
      MessageMod({type : 'error', text : `Kesalahan, check form input kembali `});
      return false;
    });
  };

  React.useEffect(() => {
    setloading(true);
    const fetchData = async () => {
      try {
        const [
          {data : ilibrary },
        ] = await Promise.all([
          oProvider.list(`ilibrary-dropdown`),
        ]);
  
        if(props.type === 'edit'){
          let { data } = await oProvider.list(`epustaka-get-one?id=${paramsId}`);
          data.epustaka_isactive = data?.epustaka_isactive ? true : false;
          data.epustaka_isdefault = data?.epustaka_isdefault ? true : false;
          data.epustaka_isprivate = data?.epustaka_isprivate ? true : false;
          data.loan_term = parseInt(data?.epustaka_roles?.loan_term);
          data.maximum_borrow = parseInt(data?.epustaka_roles?.maximum_borrow);
          form.setFieldsValue({...data});
          setdataSubmit({...dataSubmit,...data});
          setdataImg({ ...dataImg, src : data?.epustaka_logo });
        } else if(props.type === 'add') {
          const initialValue = {
            epustaka_isprivate:false,
            epustaka_isactive:true,
            loan_term:0,
            maximum_borrow:0
          };
          setdataSubmit({...dataSubmit,...initialValue});
          form.setFieldsValue({...initialValue});
        }
        
        const kategori = [
          {text : 'Lembaga', value : 'L'},
          {text : 'Perorangan', value : 'P'}
        ];
        
        setdataApi({ilibrary,kategori});
        setloading(false);
      } catch (error) {
        console.log(error?.message);
        setloading(false);
      }
    }
    fetchData();
  },[]);


  return(
  <LoadingSpin loading={loading}>
    <Form form={form} >
    <Row gutter={[16,0]}>
      <Col md={24} lg={4} xl={4}>
        <InputFile3 name="epustaka_logo" accept=".png,.jpeg,.jpg" schema={schema} onChange={handleUploadFile} imgUrl={(dataImg?.src) ? dataImg.src : ''} handlePreview={showImage}/> 
      </Col>
      <Col md={24} lg={20} xl={20}>
        <Row gutter={[16,0]}>
          <Col span={12}>
            <InputSelect name="ilibrary_id" schema={schema} data={dataApi.ilibrary} others={{showSearch : true }} onChange={handleChange} />
          </Col>
          <Col span={12}>
            <InputText name="epustaka_name" schema={schema} onBlur={handleChange} />
          </Col>
          <Col span={12}>
            <InputText name="epustaka_phone" schema={schema} onBlur={handleChange} />
          </Col>
          <Col span={12}>
            <InputText name="epustaka_email" schema={schema} onBlur={handleChange} />
          </Col>
          <Col span={24}>
            <InputText name="epustaka_address" schema={schema} onBlur={handleChange} />
          </Col>
          <Col span={24}>
            <MocoEditorQuill name='epustaka_aboutus' form={form} value={contentData} schema={schema} onChange={handleChangeEditor} others={{required:true}}/>
          </Col>
          <Col span={12}>
            <InputSelect name="epustaka_category" schema={schema} data={dataApi.kategori} others={{showSearch : true }} onChange={handleChange} />
          </Col>
          <Col span={12}></Col>
          <Col span={8}>
            <InputSwitch name="epustaka_isdefault" schema={schema} onChange={handleChange} />
          </Col>
          <Col span={8}>
            <InputSwitch name="epustaka_isprivate" schema={schema} onChange={handleChange} />
          </Col>
          <Col span={8}>
            <InputSwitch name="epustaka_isactive" schema={schema} onChange={handleChange} />
          </Col>
          <Divider orientation="left"><Typography.Text strong>Konfigurasi</Typography.Text></Divider>
          <Col span={12}>
            <InputNumber name="loan_term" schema={schema} onBlur={handleChange} />
          </Col>
          <Col span={12}>
            <InputNumber name="maximum_borrow" schema={schema} onBlur={handleChange} />
          </Col>
        </Row>
      </Col>
    </Row>
      <Space direction="horizontal" style={{float:'right'}}>
        <Button type="primary" danger htmlType="button" onClick={onCancel}>Batal</Button>
        <Button type="primary" htmlType="button" onClick={handleSimpanValid}>Simpan</Button>
      </Space>
    </Form>
    <Modal
      width="30vw"
      title='Preview'
      visible={dataImg?.visible}
      onCancel={() => setdataImg({...dataImg, visible : false })}
      footer={null}
    >
      <img src={(dataImg?.src) ? dataImg?.src : noImagePath} alt="epustakalogo" width="100%" />
    </Modal>
  </LoadingSpin>
  );
};

export default FormContext;