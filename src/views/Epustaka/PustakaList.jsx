import React from 'react';
import config from './index.config';
import MocoTable from 'components/MocoTable';
/* eslint-disable */

const PustakaList = (props) => {
    const { pustaka_detail } = config;
    const { state,setState,setLimit } = props;

    const onShowSizeChange = (current, pageSize) => {
        setLimit(pageSize);
    }

    const handleTableChange = (pagination, filters, sorter) => {
        setState({ ...state,pagination,onShowSizeChange:onShowSizeChange});
    }

    return (
        <div>
            <MocoTable
                {...pustaka_detail.config(props)}
                {...state}
                onChange={handleTableChange}
           />
        </div>
    )
}

export default PustakaList;
