import React, { createContext, useEffect,useState } from 'react';
import ListCard from './ListCard';
import config from './index.config';
import storeRedux from 'redux/store';
import queryString from "query-string";
import pathName from 'routes/pathName';
import randomString from 'utils/genString';
import oProvider from 'providers/oprovider';
import getMenuAkses from "utils/getMenuAkses";
import FilterForm from './component/FilterForm';
import { MessageContent } from 'common/message';
import { Breadcrumb, Content } from 'components';
import LoadingSpin from 'components/LoadingSpin';
/* eslint-disable */

export const MenuAksessContext = createContext([]);

const { iLibEpus: epustakaUrl } = pathName;
const resource = 'epustaka';

const Epustaka = (props) => {

  const { list } = config;
  const [menuAkses, setmenuAkses] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [limit, setLimit] = useState(10);
  const [stateList, setStateList] = useState({
    loading : false,
    pagination: {
      defaultCurrent: 1,
      pageSize: 10,
      total: 0 , 
      showSizeChanger: true
    },
    dataSource: []
  });
  const [model, setModel] = useState({
    epustaka_name : '',
    ilibrary_id: ''
  });

  const onShowSizeChange = (current,size) =>{
    setLimit(size);
  }

  const onPageChange = (page) =>{
    const pagination = {...stateList.pagination,current:page};
    setStateList({...stateList,pagination});
  }

  const fetchData = async () => {
    setLoading(true);
    try {

      let url = `?page=${stateList.pagination.current ?? stateList.pagination.defaultCurrent}&limit=${limit}`;

      const paramsUrl = queryString.stringify(model, {skipEmptyString : true});
      const params = (paramsUrl) ? `&${paramsUrl}` : '';

      const response = await oProvider.list(`${resource}${url}${params}`);

      if(response){
        const { data , meta } = response;
        const pagination = {...stateList.pagination,pageSize:limit, total:meta.total,onShowSizeChange:onShowSizeChange,onChange:onPageChange};
        setStateList({...stateList,dataSource:data,pagination});
      }
      setLoading(false); 
    } catch (error) {
      console.log(error?.message);
      setLoading(false); 
    }
  }

  const downloadExcel = async () => {
    let key = randomString(17);
    MessageContent({ type :'loading', content:'Mengunduh ... ',key, duration: 0 });
    try {

      const paramsUrl = queryString.stringify(model, {skipEmptyString : true});
      const params = (paramsUrl) ? `?${paramsUrl}` : '' ;
      const response = await oProvider.downloadfile(`epustaka-download-excel${params}`);

      if(response){
        MessageContent({ type :'success', content: 'Selesai!', key, duration: 2 });
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'ePustaka-MCCP.xlsx'); //or any other extension
        document.body.appendChild(link);
        link.click();
      }
    } catch (error) {
      MessageContent({ type :'error', content: 'Terjadi kesalahan saat download !', key, duration: 2 });
      console.log('error',error);
      return false;
    }
  }

  const viewListCatalog = (id,name)=>{
    props.history.push(epustakaUrl.epustaka.catalog(id)); 
  } 

  const changeModel = (name,value) => {
    const pagination = {...stateList.pagination,current:1};
    setStateList({...stateList,pagination});
    setModel({...model,[name]:value});
  }

  useEffect(()=>{
    if(storeRedux.getState().menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(epustakaUrl.epustaka.list);
      setmenuAkses(menuAccessFromLocalStorage);
    }
  },[storeRedux.getState().menuAccess]);

  useEffect(()=>{
    fetchData();
  },[model,stateList.pagination.current,limit]);

  return (
    <MenuAksessContext.Provider value={menuAkses}>
      <Breadcrumb items={list.breadcrumb} />
      <Content {...{...props, ...list.content}} menuAkses={menuAkses}>
        <LoadingSpin loading={loading}>
          <FilterForm {...props} changeModel={changeModel} downloadExcel={downloadExcel}/>
          <ListCard {...props} data={stateList} viewList = {viewListCatalog} onDelete={fetchData} onPageChange={onPageChange}/>
        </LoadingSpin>
      </Content>
    </MenuAksessContext.Provider>
  )
}

export default Epustaka;
