import moment from 'moment';
import pathName from 'routes/pathName';
import { numberFormat } from "utils/initialEndpoint";
/* eslint-disable */

const { initial, iLibEpus } = pathName;

export default {
  list: {
    breadcrumb: (params) =>{
        return [
            { text: 'Beranda', to: initial },
            { text: 'ePustaka', to: iLibEpus.epustaka.list },
            { text: 'Katalog', to: iLibEpus.epustaka.catalog(params.epustaka_id) },
            { text: 'History Pengadaan' }
        ]
    },
    content: {
        title: 'Riwayat Pengadaan Buku',
        subtitle: 'Transaksi Pengadaan Buku',
    },
    config: (...props) => {
      return {
        columns: [
          {
            title: 'Tanggal',
            dataIndex: 'transaction_date',
            fixed: 'left',
            key: 'transaction_date',
            render: row => {
              return row !== null ? moment(row).format('DD-MM-YYYY') : '-'; 
            }
          },
          {
            title: 'Nomor Transaksi',
            dataIndex: 'transaction_number',
            key: 'transaction_number',
            width: '40%',
            render: row => {
                return row !== null ? row : '-'; 
            }
          },
          {
            title: 'Jumlah',
            dataIndex: 'epustaka_catalog_qty',
            align:'right',
            key: 'category_name',
            render: row => {
                return row !== null ? row : '-'; 
            }
          },
          {
            title: 'Harga',
            dataIndex: 'catalog_price',
            key: 'catalog_price',
            align:'right',
            render: x => {
                return x ? numberFormat(x) : '-'; 
            }
          },
          {
            title: 'Diskon',
            dataIndex: 'discount_amount',
            key: 'discount_amount',
            align:'right',
            render: x => {
              return x ? numberFormat(x) : '-'; 
            }
          },
          {
            title: 'Total (Rp)',
            dataIndex: 'total_amount',
            key: 'total_amount',
            align:'right',
            width: 120,
            render: x => {
              return x ? numberFormat(x) : '-'; 
            },
          },
        ]
      }
    }
  },
  model: ({epustaka_id,catalog_id}) => ({
    epustaka_id: epustaka_id,
    catalog_id: catalog_id,
    start_date: '',
    end_date: ''
  })
};