import React, { Fragment,useState, useEffect } from 'react';
import config from './index.config';
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
/* eslint-disable */

const resource = 'epustaka-history-purchase';

const ListForm = (props) => {
    const {model} = props;
    const [limit, setLimit] = useState(10);
    const [url, setUrl] = useState('');
    const [state, setState] = useState({
        loading : false,
        pagination: {
          defaultCurrent: 1,
          pageSize: 10,
          total: 0, 
          showSizeChanger: true, 
        },
        dataSource: []
      });


    const handleTableChange = (pagination, filters, sorter) =>{
        let url = '';
        if (pagination) url += `&page=${pagination.current}`;
        if (sorter && sorter.field) {
          url += `&sortBy=${sorter.field}`;
          url += `&sortDir=${sorter.order === 'ascend' ? 'asc' : 'desc'}`;
        }
        setUrl(url);
    }

    const onShowSizeChange = (current, pageSize) => {
        setLimit(pageSize);
    }

    const refresh = async (url, model) =>{
        setState({ ...state,loading: true });
        try {
          if(model.epustaka_id && model.catalog_id){
              if(model.start_date && model.end_date){
                  url += `&start_date=${model.start_date}&end_date=${model.end_date}`;
              }
              const responsesData = await oProvider.list(`${resource}?epustaka_id=${model.epustaka_id}&catalog_id=${model.catalog_id}${url}`);
              if(responsesData){
                  const { meta , data } = responsesData;
                  const pagination = {...state.pagination, pageSize: limit, total: parseInt(meta.total), onShowSizeChange:onShowSizeChange };
                  setState({...state,pagination,dataSource:data,loading:false});
              }
          }
        } catch (error) {
          setState({ ...state,loading: false });
          console.log(error?.message);
          return false;
        }
    }

    useEffect(()=>{
        refresh(url,model);
    },[url,model,limit]);
    

    return (
        <Fragment>
            <MocoTable
                {...config.list.config(props)}
                {...state}
                scroll={{ x: 'max-content' }}
                onChange={handleTableChange}
            />
        </Fragment>
    )
}

export default ListForm;
