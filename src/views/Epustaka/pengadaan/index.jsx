import React, { useState, useEffect, Fragment } from 'react';
import { Form, Row, Col,DatePicker } from 'antd';
import ListForm from './ListForm';
import config from './index.config';
import oProvider from 'providers/oprovider';
import {Breadcrumb,Content} from 'components';
import LoadingSpin from 'components/LoadingSpin';
/* eslint-disable */

const Pengadaan = (props) => {
    const { content } = config.list;
    const { epustaka_id, catalog_id  } = props.match.params;
    const [model, setModel] = useState(config.model({epustaka_id,catalog_id}));

    const [form] = Form.useForm();
    const breadcrumbItem = config.list.breadcrumb({epustaka_id});

    const [loadingPage, setLoadingPage] = useState(false);
    const [stateBook,setStateBook] = useState({
        judulBuku : '-',
        penerbit : '-',
        pengarang : '-',
    });    
    
    const handleChange = (name, newDates) =>{
        setModel({...model,start_date:newDates[0],end_date:newDates[1]});
    }

    const fetchHistoryData = async () =>{
        setLoadingPage(true);

        try {
          const detailBook = await oProvider.list(`catalog-view-detail?id=${catalog_id}`);
          if(detailBook){
              const {data : { catalog_title, organization_name , catalog_authors} } = detailBook;
              setStateBook({
                  ...stateBook,
                  judulBuku : catalog_title,
                  penerbit : organization_name,
                  pengarang : catalog_authors
              })
          }
          setLoadingPage(false);
        } catch (error) {
          setLoadingPage(false);
          console.log(error?.message);
          return false;
        }
    }

    useEffect(() => {
        fetchHistoryData();
    }, [])

    return (
        <Fragment>
            <Breadcrumb items={breadcrumbItem}/>
            <Content {...{...props, ...content }}>
                <LoadingSpin loading={loadingPage}>
                        <Row>
                            <Col flex={32}>
                                <div style={{fontSize:'14pt'}}>
                                    Judul Buku : {stateBook.judulBuku}
                                </div>
                                <div className="mt-2" style={{fontSize:'11pt'}}>
                                    Penerbit : {stateBook.penerbit}
                                </div>
                                <div className="mt-1" style={{fontSize:'11pt'}}>
                                    Pengarang : {stateBook.pengarang}
                                </div>
                            </Col>
                            <Col flex={0} style={{textAlign:'right'}}>
                                <Form form={form} layout={{labelCol: { span: 8 },wrapperCol: { span: 16 }}}>
                                        <Form.Item label="Tanggal">
                                            <DatePicker.RangePicker onChange={handleChange}/>
                                        </Form.Item>
                                </Form>
                            </Col>
                        </Row>
                    <hr/>
                    <ListForm {...props} model={model}/>
                </LoadingSpin>
            </Content>
        </Fragment>
    )
}

export default Pengadaan
