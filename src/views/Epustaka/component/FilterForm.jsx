import React, { Fragment, useEffect, useState } from 'react';
import { Row, Col, Button, Form, Input,Tooltip } from 'antd';
import InputSelect from 'components/ant/InputSelect';
import oProvider from 'providers/oprovider';
/* eslint-disable */

const FilterForm = (props) => {
    const { changeModel, downloadExcel } = props;
    const [form] = Form.useForm();
    const [ilibData,setIlibData] = useState([]);
    const schema = {
        ilibrary_id : {
            label: 'iLibrary'
        },
        epustaka_name:{
            label: 'ePustaka'
        }
    }

    
    const initialData = async () => {
        try {
            const response = await oProvider.list('ilibrary-dropdown');
            if(response){
                const { data } = response;
                data.unshift({text:'-- Semua iLibrary --',value:''});
                setIlibData(data);
            }
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    const handleChange = (name,value,e) => {
        if(changeModel){
            changeModel(name,value,e);
        }
    }

    const handleNameChange = (value,e) => {
        changeModel('epustaka_name', value);
    }

    useEffect(()=>{
        initialData()
    },[]);

    return (
        <Fragment>
            <Form form={form}>
                <Row gutter={[16,16]}>
                    <Col xs={24} sm={24} md={10} lg={10} xl={10}>
                        <InputSelect name='ilibrary_id' data={ilibData} schema={schema} labelCol={12} onChange={handleChange} others={{showSearch:true}}/>
                    </Col>
                    <Col xs={24} sm={24} md={10} lg={10} xl={10}>
                        <Form.Item name='epustaka_name' label={schema.epustaka_name.label} labelCol={12}>
                            <Input.Search onSearch={handleNameChange}  enterButton="Cari"/>
                        </Form.Item>
                    </Col>
                    <Col xs={24} sm={24} md={4} lg={4} xl={4} style={{textAlign:'right'}}>
                        <Tooltip title='Download Excel'>
                            <Button type="primary" onClick={downloadExcel} style={{width:'100px',background:'#CB1C31',border:'none'}}>
                                Excel
                            </Button>
                        </Tooltip>
                    </Col>
                </Row>
            </Form>
        </Fragment>
    )
}

export default FilterForm
