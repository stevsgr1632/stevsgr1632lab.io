import React from 'react';
import { Modal,Typography,Space } from "antd";
import { DeleteOutlined } from '@ant-design/icons';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
/* eslint-disable */

const ModalDelete = (item,cb) => {
  Modal.confirm({
    title: 'Hapus ePustaka',
    centered : true,
    okType: 'danger',
    cancelText : 'Batal',
    okText: 'Ya',
    icon: <DeleteOutlined />,
    content: <Space direction="vertical">
      <Typography.Text>Anda yakin ingin menghapus data ini (<b>{item?.epustaka_name}</b>) dan semua data terkait?</Typography.Text>
      <Typography.Text type="danger">Tindakan ini tidak bisa dibatalkan</Typography.Text>
    </Space>,
    onOk() {
      return new Promise(async (resolve, reject) => {
        try {
          let response = await oProvider.delete(`epustaka?id=${item.id}`);
          if(response){
            Notification({ type : 'success', response });
            cb();
            resolve();
          }
        } catch (error) {
          console.log(error?.message);
          reject();
        }
      }).catch(() => console.log('Oops errors!'));
    },
    onCancel() {},
  });
};

export default ModalDelete;