import React,{useState} from 'react';
import { Modal, Row ,Col} from 'antd';
import { noImagePath } from "utils/initialEndpoint";
/* eslint-disable */

const ModalDetail = ({ data = {}, visible, setModal }) => {
    const [tabIdx, setTabIdx] = useState(0);

    const styles = {
        tabSelected: {
          borderBottom: '3px solid #cc0000',
        },
        tabNotSelected: {
          borderBottom: '1px solid #cccccc',
          cursor: 'pointer',
        }
    }

    return (
        <Modal
            width={750}
            visible={visible}
            onCancel={() => setModal({ visible: false })}
            footer={null}
        >
            <h1>Detail Produk</h1>
            <Row gutter={24}>
                <Col lg={7}>
                    <img src={(data.catalog_cover) ? data.catalog_cover : noImagePath} alt={(data.catalog_cover) ? data.catalog_cover : noImagePath} width={'100%'}/>
                </Col>
                <Col lg={15}>
                    <div><strong>{data.catalog_title}</strong></div>
                    <div className="mt-4">Kepengarangan</div>
                    <div><strong>{data.catalog_authors}</strong></div>
                    <div className="mt-4">Diterbitkan</div>
                    <div><strong>{data.catalog_publish_date}</strong></div>
                    <div className="mt-4">Kategori</div>
                    <div><strong>{data.category_name}</strong></div>
                    <div className="mt-4">Ditambahkan pada</div>
                    <div><strong>{data.catalog_publish_date}</strong></div>
                </Col>
            </Row>
            <div className="mt-5"></div>
            <Row>
                <Col span={12} style={tabIdx === 0 ? styles.tabSelected : styles.tabNotSelected} onClick={() => setTabIdx(0)}>
                    <h5>Sinopsis</h5>
                </Col>
                <Col span={12} style={tabIdx === 1 ? styles.tabSelected : styles.tabNotSelected} onClick={() => setTabIdx(1)}>
                    <h5>Detail</h5>
                </Col>
            </Row>

            <div style={{ display: tabIdx === 0 ? 'block' : 'none',height:'50vh',overflowY:'scroll' }}>
                <div style={{ padding: '10px 0' }}>
                    {<div dangerouslySetInnerHTML={{ __html: data.catalog_description }} /> || '-'}
                </div>
            </div>

            <div style={{ display: tabIdx === 1 ? 'block' : 'none' }}>
                <div style={{ padding: '10px 0' }}>
                    <Row>
                        <Col span={12}>
                            <div className="mt-3">SKU</div>
                            <div><strong>{data.catalog_sku}</strong></div>
                            <div className="mt-3">Asal kota penerbit</div>
                            <div><strong>{data.catalog_publish_city}</strong></div>
                            <div className="mt-3">ISBN</div>
                            <div><strong>{data.catalog_isbn}</strong></div>
                            <div className="mt-3">EISBN</div>
                            <div><strong>{data.catalog_eisbn}</strong></div>
                            <div className="mt-3">EISSN</div>
                            <div><strong>{data.catalog_eissn}</strong></div>
                            <div className="mt-3">Jumlah Halaman</div>
                            <div><strong>{data.catalog_pages}</strong></div>
                        </Col>
                        <Col span={12}>
                            <div className="mt-3">Sub Kategori</div>
                            <div><strong>{(data.sub_category || [])[0]}</strong></div>
                            <div className="mt-3">Bahasa</div>
                            <div><strong>{data.catalog_language || '-'}</strong></div>
                            <div className="mt-3">Tanggal Publish</div>
                            <div><strong>{data.catalog_publish_date || '-'}</strong></div>
                        </Col>
                    </Row>
                </div>
            </div>
        </Modal>
    )
}

export default ModalDetail;
