import React, { Fragment , useEffect,useState} from 'react';
import config from '../index.config';
import ModalDetail from './FormDetail';
import pathName from "routes/pathName";
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
/* eslint-disable */

const resource = 'epustaka-catalog';

const FormList = (props) => {
  const { iLibEpus: epustakaUrl } = pathName;
  const {model} = props;
  const [url, setUrl] = useState('');
  const [modalState,setModalState] = useState({
    loading : false,
    visible:false,
    data:{}
  });
  const catalogId = props.match.params.id;
  const [limit, setLimit] = useState(10);
  const [state, setState] = useState({
    loading : false,
    pagination: {
      defaultCurrent: 1,
      pageSize: 10,
      total: 0, 
      showSizeChanger: true, 
    },
    dataSource: []
  });

  const onShowSizeChange = (current, pageSize) => {
    setLimit(pageSize);
  }

  const refresh = async (url,model)=>{
    setState({ ...state,loading: true });
    if(model.id !==''){
      if(model.category) url += `&category=${model.category}`;
      if(model.publisherId) url += `&publisherId=${model.publisherId}`;
      if(model.search) url += `&search=${model.search}`;
      
      try {
        const { meta, data } = await oProvider.list(`${resource}?id=${model.id}${url}`);
        const pagination = {...state.pagination, pageSize: limit, total: parseInt(meta.total), onShowSizeChange:onShowSizeChange };
        setState({ ...state,pagination,dataSource: data,loading: false });
      } catch (error) {
        setState({ ...state, loading: false });
        console.log(error?.message);
        return false;
      }

    }
  }

  const modalDetail = async (row,props) =>{
    try {
      const { data } = await oProvider.list(`catalog-view-detail?id=${row.id}`);
      setModalState({ ...modalState, loading:true, visible:true, data });
    } catch (error) {
      setModalState({ ...modalState, loading:false, visible:false });
      console.log(error?.message);
      return false;
    }
  }

  const viewHistoryPurchase = async (row, props) =>{
    props.history.push(epustakaUrl.epustaka.pengadaan({epustaka_id:catalogId,catalog_id:row.id}))
  }

  const handleTableChange = (pagination, filters, sorter) => {
    let url = '';
    if (pagination) url += `&page=${pagination.current}`;
    if (sorter && sorter.field) {
      url += `&sortBy=${sorter.field}`;
      url += `&sortDir=${sorter.order === 'ascend' ? 'asc' : 'desc'}`;
    }
    setUrl(url);
  }

  useEffect(()=>{
    refresh(url,model);
  },[url,model,limit]);

  return (
    <Fragment>
      <MocoTable
        {...config.list.config(props,modalDetail,viewHistoryPurchase)}
        {...state}
        onChange={handleTableChange}
      />
      <ModalDetail {...modalState} setModal={setModalState} />
    </Fragment>
  )
}

export default FormList;
