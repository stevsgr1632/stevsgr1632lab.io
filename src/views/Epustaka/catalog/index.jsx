import React from 'react';
import { Col, Row,Form,Typography } from 'antd';
import FormList from './FormList';
import config from '../index.config';
import oProvider from 'providers/oprovider';
import {Breadcrumb,Content} from 'components';
import LoadingSpin from 'components/LoadingSpin';
import InputSelect from 'components/ant/InputSelect';
import InputSearch from 'components/ant/InputSearch';
import useCustomReducer from 'common/useCustomReducer';
/* eslint-disable */

const resource = 'epustaka-get-one';
const initialData = {
  data : {},
  libName : '-',
  loading : false,
  model : config.model,
};
const Katalog = (props) => {
  const { breadcrumb, content } = config.catalog;
  const { Title } = Typography;
  const [form] = Form.useForm();
  const [dataReducer, reducerFunc] = useCustomReducer(initialData);

  const handleChange = (name,value) =>{
    reducerFunc('model',{[name]:value});
  }

  const fetchData = async () => {
    reducerFunc('loading',true,'conventional');
    try {
      const id = props.match.params.id;
      const getOne = await oProvider.list(`${resource}?id=${id}`);

      if(getOne){
          const {data:pustaka} = getOne;
          setLibName(pustaka.epustaka_name);
      }

      const [
        {data:kategori},
        {data:penerbit},
      ] = await Promise.all([
        oProvider.list(`categories-dropdown`),
        oProvider.list(`organization-dropdown`)
      ]);

      kategori.unshift({ value: '', text: '-- Semua Kategori --' });
      penerbit.unshift({ value: '', text: '-- Semua Penerbit --' });
      reducerFunc('model',{id});
      reducerFunc('data',{kategori,penerbit});
      
      reducerFunc('loading',false,'conventional');
    } catch (error) {
      reducerFunc('loading',false,'conventional');
      console.log(error?.message);
      return false;
    }
  }

  React.useEffect(()=>{
    fetchData();
  },[]);

  return (
    <>
      <Breadcrumb items={breadcrumb} />
      <Content {...{...props, ...content }}>
        <LoadingSpin loading={dataReducer?.loading}>
          <Title level={4}>{dataReducer?.libName}</Title>
          <hr/>
          <Form form={form}>
            <Row gutter={16}>
              <Col span={8}>
                <InputSelect name="publisherId" defaultVal={dataReducer?.model?.penerbit || ''} data={dataReducer?.data?.penerbit}  text="Penerbit" others={{showSearch:true}} onChange={handleChange}/>
              </Col>
              <Col span={8}>
                <InputSelect name="category" defaultVal={dataReducer?.model?.kategori || ''} data={dataReducer?.data?.kategori} text="Kategori" others={{showSearch:true}} onChange={handleChange} />
              </Col>
              <Col span={8} style={{marginTop:'0.6em'}}>
                <InputSearch name="search" text="Pencarian" onSearch={handleChange}/>
              </Col>
            </Row>
          </Form>
          <hr />
          <FormList {...props} model={dataReducer?.model}/>
        </LoadingSpin>
      </Content>
    </>
  )
}

export default Katalog;
