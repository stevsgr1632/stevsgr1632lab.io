import React from 'react';
import Form from './Form';
import config from './index.config';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import { Breadcrumb, Content } from 'components';
/* eslint-disable */

const FormAdd = props => {
  const { add, schema } = config;
  const { iLibEpus : { epustaka } } = pathName;

  const openNotification = params => {
    Notification({...params});
  };

  const handleSimpan = async (dataSubmit) => {
    try {
      let response = await oProvider.insert('epustaka', dataSubmit);
      if(response){
        openNotification({response});
        props.history.push(epustaka.list);
      }
    } catch (error) {
      console.log('error',error);
      openNotification({type: 'error', response : {code : 'error',message : error?.response?.data?.message } });
    }
  };
  const onCancel = e => {
    e.preventDefault();
    props.history.push(epustaka.list);
  };

  const propsForm = {schema, handleSimpan, onCancel};
  
  return(<>
    <Breadcrumb items={add.breadcrumb} />
    <Content {...{...props, ...add.content}}>
      <Form 
        {...propsForm}
        type="add"
      />
    </Content>
  </>);
};

export default FormAdd;