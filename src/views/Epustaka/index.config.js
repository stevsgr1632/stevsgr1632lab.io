import React from 'react';
import {PlusOutlined} from '@ant-design/icons';
import { Typography, Space,
  Dropdown ,Menu,Button} from 'antd';
import pathName from 'routes/pathName';
import ActionTableDropdown from "components/ActionTableDropdown";
/* eslint-disable */

const { initial, iLibEpus } = pathName;
const { Text } = Typography;

const actionMenu = (row, params) => {
  const [props,modalDetail,viewHistoryPurchase] = params;

  return (
    <Menu>
      <Menu.Item>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => modalDetail(row, props)} text="Detail Katalog" />
      </Menu.Item>
      <Menu.Item>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => viewHistoryPurchase(row, props)} text="Riwayat Pembelian" />
      </Menu.Item>
    </Menu>
  );
};

export default {
  list: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'ePustaka', to: iLibEpus.epustaka.list },
    ],
    content: {
      title: 'Daftar ePustaka Instansi',
      subtitle: 'Daftar ePustaka & Katalog',
      toolbars : [
        {
          text: '', type: 'primary', icon: <PlusOutlined/>,
          onClick: (e) => {
            e.history.push(iLibEpus.epustaka.add);
          }
        },
      ]
    },
    config: (...props) => {
      return {
        columns: [
          {
            title: 'ISBN',
            dataIndex: 'catalog_isbn',
            key: 'catalog_isbn',
            render: row => {
              // return row !== null ? row : ' - ';
              return {
                  props: {
                    style: { verticalAlign: 'top' },
                  },
                  children: <div>{row}</div>,
              };
            }
          },
          {
            title: 'Nama Katalog',
            key: 'catalog_title',
            render: row => {
              return (row !== null ? 
                <Space direction="vertical">
                  <Text>{row.catalog_title}</Text> 
                  <Text>Pengarang</Text>
                  <Text>{row.catalog_authors} </Text>
                </Space>
              : ' - ');
            }
          },
          {
            title: 'Kategori',
            dataIndex: 'category_name',
            key: 'category_name',
            render: row => {
              return {
                  props: {
                    style: { verticalAlign: 'top' },
                  },
                  children: <div>{row !== null ? row : ' - '}</div>,
              }
            }
          },
          {
            title: 'Penerbit',
            dataIndex: 'organization_name',
            key: 'organization_name',
            render: row => {
              return {
                  props: {
                    style: { verticalAlign: 'top' },
                  },
                  children: <div>{row !== null ? row : ' - '}</div>,
              }
            }
          },
          {
            title: 'Copy',
            dataIndex: 'catalog_quantity',
            key: 'catalog_quantity',
            render: row => {
              return {
                  props: {
                    style: { verticalAlign: 'top' },
                  },
                  children: <div>{row !== null ? row : ' - '}</div>,
              }
            }
          },
          {
            title: 'Aksi',
            key: 'operation',
            fixed: 'right',
            width: 100,
            render: (row) => {
              return ({
                props: {
                  style: { verticalAlign: 'top' },
                },
                children: 
                  <span>
                    <Dropdown overlay={actionMenu(row, props)}>
                      <Button
                        type="link"
                        style={{ margin: 0 }}
                        onClick={e => e.preventDefault()}
                      >
                        <i className="icon icon-options-vertical" />
                      </Button>
                    </Dropdown>
                  </span>,
              })
            },
          },
        ]
      }
    }
  },
  catalog:{
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'ePustaka', to: iLibEpus.epustaka.list},
      { text: 'Daftar Katalog'}
    ],
    content: {
      title: 'Daftar Katalog Ebook',
      subtitle: 'Transaksi Pengadaan Buku',
    },
  },
  add:{
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'ePustaka', to: iLibEpus.epustaka.list},
      { text: 'ePustaka Baru'}
    ],
    content: {
      title: 'Buat ePustaka Baru',
    },
  },
  edit:{
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'ePustaka', to: iLibEpus.epustaka.list},
      { text: 'Update ePustaka'}
    ],
    content: {
      title: 'Perbaharui ePustaka',
    },
  },
  pustaka_detail : {
    config : (...props) =>{
      return {
        columns: [
          {
            title: 'Kategori',
            dataIndex: 'category_name',
            key: 'category_name',
            width:'60%',
            render: row => {
              return {
                  props: {
                    style: { textAlign: 'left' },
                  },
                  children: <div>{row !== null ? row : ' - '}</div>
              };
            }
          },
          {
            title: 'Judul',
            dataIndex: 'category_qty',
            align:'right',
            key: 'category_qty',
            render: row => {
              return {
                  props: {
                    style: { textAlign: 'right' },
                  },
                  children: <div>{row !== null ? row : ' - '}</div>
              };
            }
          },
          {
            title: 'Copy',
            dataIndex: 'category_copy',
            align:'right',
            key: 'category_copy',
            render: row => {
              return {
                  props: {
                    style: { textAlign: 'right' },
                  },
                  children: <div>{row !== null ? row : ' - '}</div>
              };
            }
          },
        ]
      }
    },
    model: {
      id: '',
      category: '',
      publisherId: '',
      search: ''
    },
  },
  model: {
    id: '',
    category: '',
    publisherId: '',
    search: ''
  },
  schema : {
    ilibrary_id: {
      label: 'iLibrary',
      rules: [
        { required: true, message: 'Silahkan pilih iLibrary' },
      ]
    },
    epustaka_name: {
      label: 'Nama ePustaka',
      rules: [
        { required: true, message: 'Silahkan input judul' },
      ]
    },
    epustaka_phone: {
      label: 'Telepon',
      rules: [
        { required: true, message: 'Silahkan input telepon' },
        // (params) => ({
        //   validator(rule, value) {
        //     // console.log(rule,value,params);
        //     let errorMsg = 'Data input yang Anda masukkan harus berupa nomor telepon benar !';
        //     let regex = regexList('phone');
        //     if (value.match(regex) || !value) {
        //       return Promise.resolve();
        //     } else {
        //       return Promise.reject(errorMsg);
        //     }
        //   },
        // }),
      ]
    },
    epustaka_email: {
      label: 'Email',
      rules: [
        { required: true, message: 'Silahkan input email' },
        // (params) => ({
        //   validator(rule, value) {
        //     // console.log(rule,value,params);
        //     let errorMsg = 'Data input yang Anda masukkan harus berupa email benar !';
        //     let regex = regexList('email');
        //     if (value.match(regex) || !value) {
        //       return Promise.resolve();
        //     } else {
        //       return Promise.reject(errorMsg);
        //     }
        //   },
        // }),
      ]
    },
    epustaka_address: {
      label: 'Alamat',
      rules: [
        { required: true, message: 'Silahkan input alamat' },
      ]
    },
    epustaka_aboutus: {
      label: 'Tentang ePustaka',
      rules: [
        { required: true, message: 'Silahkan input deskripsi tentang ePustaka' },
      ]
    },
    epustaka_category: {
      label: 'Kategori',
      rules: [
        { required: true, message: 'Silahkan pilih kategori' },
      ]
    },
    epustaka_isdefault: {
      label: 'ePustaka Utama',
    },
    epustaka_isprivate: {
      label: 'Private ?',
      rules: [
        { required: true, message: 'Silahkan check ' },
      ]
    },
    epustaka_isactive: {
      label: 'Aktif ?',
      rules: [
        { required: true, message: 'Silahkan check keaktifan' },
      ]
    },
    loan_term: {
      label: 'Lama Peminjaman (Hari)',
      rules: [
        {required: true, message: 'Lama hari antara 0 s/d 15', type:'number',min: 0, max:15},
      ]
    },
    maximum_borrow: {
      label: 'Max. Buku Dipinjam',
      rules: [
        { required: true, message: 'nilai maximal antara 0 s/d 10',type:'number', min: 0, max:10},
      ]
    }, 
    epustaka_logo: {
      label: 'ePustaka Logo',
      rules: [
        { required: true, message: 'Silahkan upload logo ' },
      ]
    }, 
  }
};