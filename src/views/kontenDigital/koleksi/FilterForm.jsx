import React,{useState, useEffect} from 'react';
import { Form } from 'antd';
import { InputForm } from 'components';
import { DataIndexContext } from "./index";
import oProvider from 'providers/oprovider';
/* eslint-disable */

const FilterForm = (props) => {
    const dataFromIndex = React.useContext(DataIndexContext);
    const nameForm = ['organization_id','publisher_id','epustaka_id','category_id','procurement_id','search','tanggal'];
    const [form] = Form.useForm();
    const [data, setData] = useState({});
    const [filter, setFilter] = useState(props.filter);
    const [search, setSearch] = useState('');
    const [loading, setLoading] = useState({  
        organization_id: true,
        publisher_id: true,
        epustaka_id: true,
        category_id: true,
        procurement_id: true, 
    });

    const clearFilter = () => {
        const clearFilter = {
            organization_id: '',
            publisher_id: '',
            epustaka_id: '',
            category_id: '',
            procurement_id: '',
            search: '',
            tanggal:[]
        };
        props.schema.form.setFieldsValue({...clearFilter});
        setFilter({...clearFilter});
    }

    const initilize = async () => {
        // clearFilter();
        setData({ ...data, ...dataFromIndex });
        setLoading({});
    }

    const handleChange = (name, value) =>{
        if(name ==='action'){
            if(value==='clear'){
                clearFilter();
            }
            if(value==='search'){
                setFilter({ ...filter, search: search });
            }
        }else{

            if(nameForm.includes(name)){
                if(name==='search'){
                    setSearch(value);
                }else{
                    if(name==='tanggal'){
                        if(value[0]!=='' && value[1]!==''){
                            setFilter({ ...filter, [name]:value});
                        }

                        if(value[0]==='' && value[1]===''){
                            setFilter({ ...filter, [name]:value});
                        }
                    }else{
                        setFilter({ ...filter, [name]:value});
                    }
                   
                }
            }                      
        }
    }

    const handleEnter = () =>{
        setFilter({ ...filter, search: search });
    }

    const organizationChange = async (orgId) => {
        setLoading({...loading,publisher_id:true})
        try {
            const response = await oProvider.list(`organization-dropdown?showAll=1&id=${orgId}`);
            if(response){
                const { data : orgList } = response;
                orgList.unshift({ value: '', text: '-- Semua Penerbit --' })
                setData({...data,publisher:orgList});
                setLoading({...loading,publisher_id:false});
            }
        } catch (error) {
            console.log(error?.messagge);
            setLoading({...loading,publisher_id:false});
        }
    }


    useEffect(() => {
        props.onFilterChange(filter);
    }, [filter]);

    useEffect(()=>{
        if(filter.organization_id){
            organizationChange(filter.organization_id);
        } 
    },[filter.organization_id]);

    useEffect(()=>{
        initilize();
    },[dataFromIndex]);

    return (
        <div>
           <InputForm {...props}
            form={form}
            onChange={handleChange}
            defCol={{ xs: 12, sm:12, md: 12, lg: 12, xl: 8 }}
            controls={[
                { type: 'select', name: 'organization_id', data: data.organization, loading: loading.organization_id, others:{showSearch:true} },
                { type: 'select', name: 'publisher_id', data: data.publisher, loading: loading.publisher_id, others:{showSearch:true} },
                { type: 'select', name: 'epustaka_id', data: data.epustaka, loading: loading.epustaka_id, others:{showSearch:true} },
                { type: 'select', name: 'category_id', data: data.category,loading: loading.category_id, others:{showSearch:true} },
                { type: 'select', name: 'procurement_id', data: data.procurement, loading: loading.procurement_id,others:{showSearch:true} },
                { type: 'rangepicker', name: 'tanggal',style:{width:'100%'}},
                { type: 'text', name: 'search', defcol: { md: 16 },onPressEnter:()=>{handleEnter()}, value:{search} },
                {
                type: 'action', name: 'action', defcol: { md: 8 }, useLabel: true, actions: [
                    { text: 'Pencarian', type: 'primary', style: { marginRight: 5 }, action: 'search' },
                    { text: 'Reset', type: 'danger', action: 'clear' }
                ]
                },
            ]}>
            {props.children}
           </InputForm>
        </div>
    )
}

export default FilterForm
