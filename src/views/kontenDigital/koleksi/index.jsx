import React, { useState, useEffect } from "react";
import ListForm from "./ListForm";
import config from './index.config';
import FilterForm from "./FilterForm";
import ModalDetail from "./ModalDetail";
import pathname from 'routes/pathName.js';
import oProvider from 'providers/oprovider';
import Breadcrumb from 'components/Breadcrumb';
import LoadingSpin from 'components/LoadingSpin';
/* eslint-disable */

export const DataIndexContext = React.createContext();

const Koleksi = (props) => {
    const {schema, scope } = config;
    const [filter,setFilter] = useState(config.model);
    const [data, setData] = useState({});
    const [loading,setLoading] = useState(false);
    const [visible, setVisible] = useState(false);
    const [stateDataDetail, setstateDataDetail] = useState({
        loading:false,
        data:{}
    });

    const onFilterChange = (values) => {
        setFilter(values);
    }

    const modalDetail = async (epustaka_id,catalog_id) =>{
        props.history.push(pathname.kontenDigital.detailColletion({epustaka_id,catalog_id}));
        // await oProvider.list(`ilibrary-collection-getone?epustaka_id=${epustaka_id}&catalog_id=${catalog_id}`).then((result)=>{
        //     // console.log('ini result',result)
        //     setstateDataDetail({...stateDataDetail,data:result.data,loading:false});
        // }).catch((error)=>{
        //     //console.log('terjadi kesalahan ',error);
        //     setstateDataDetail({...stateDataDetail,loading:false});
        // });
    }

    const fetchData = async () => {
        setLoading(true);

        try {
            const publisher = [{ value: '', text: '-- Semua Penerbit --' }];
          const [
            {data: organization},
            {data: epustaka},
            {data: category},
            {data: procurement},
          ] = await Promise.all([
            oProvider.list('organization-group-dropdown'),
            oProvider.list('epustaka-dropdown'),
            oProvider.list('categories-dropdown'),
            oProvider.list('catalog-transaction-dropdown'),
          ]);
          
          organization.unshift({ value: '', text: '-- Semua Instansi --' });
          epustaka.unshift({ value: '', text: '-- Semua ePustaka --' });
          category.unshift({ value: '', text: '-- Semua Kategori --' });
          procurement.unshift({ value: '', text: '-- Semua Pengadaan --' });
          
          setData({...data,organization,publisher,epustaka,category,procurement});
          setLoading(false);
        } catch (error) {
          console.log(error?.message);
          setLoading(false);
          return false;
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <DataIndexContext.Provider value={data}>
            <Breadcrumb items={scope.breadcrumb} />
            <LoadingSpin loading={loading}>
                <FilterForm filter={filter} scope={scope} schema={schema} onFilterChange={onFilterChange} />
                <ListForm filter={filter} modalDetail={modalDetail}/>
                <ModalDetail 
                    visible={visible} 
                    setVisible={setVisible}
                    stateDataDetail={stateDataDetail}
                    setstateDataDetail={setstateDataDetail}/>
            </LoadingSpin>
        </DataIndexContext.Provider>
    );
};

export default Koleksi;
