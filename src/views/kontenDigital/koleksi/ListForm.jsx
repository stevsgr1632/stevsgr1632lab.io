import React,{ useState, useEffect } from 'react';
import config from './index.config';
import oProvider from 'providers/oprovider';
import { Content, MocoTable } from 'components';
/* eslint-disable */

const resource = 'ilibrary-collection';

const ListForm = (props) => {
    const { filter,modalDetail } = props;
    const [limit, setLimit] = useState(10);
    const [state, setState] = useState({
        loading : false,
        pagination: {
          defaultCurrent: 1,
          pageSize: limit,
          total: 0, 
          showSizeChanger: true, 
        },
        dataSource: []
    });

    const handleTableChange = (pagination, filter, sorter) => {
        setState((state) => ({ ...state, pagination, filter, sorter }));
    }

    const onShowSizeChange = (current, pageSize) => {
        setLimit(pageSize);
    }


    const refreshList = async (filter) =>{
        setState({ ...state, loading: true });

        let url = `search=${filter.search}`;

        if(filter.category_id){
            url +=  `&category_id=${filter.category_id}`;
        }
        if(filter.epustaka_id){
            url +=  `&epustaka_id=${filter.epustaka_id}`;
        }
        if(filter.organization_id){
            url +=  `&organization_id=${filter.organization_id}`;
        }
        if(filter.procurement_id){
            url +=  `&procurement_id=${filter.procurement_id}`;
        }
        if(filter.publisher_id){
            url +=  `&publisher_id=${filter.publisher_id}`;
        }
        if(filter.tanggal.length > 1){
            url +=  `&start_date=${filter.tanggal[0]}&end_date=${filter.tanggal[1]}`;
        }
        
        await oProvider.list(`${resource}?${url}`).then(result =>{
          const { meta, data} = result;
          const pagination = { ...state.pagination, pageSize: limit, total: parseInt(meta.total), onShowSizeChange:onShowSizeChange };
          setState({ ...state, pagination, dataSource: data, loading: false });
        }).catch(error => {
          // console.log('ini error : ',error);
          setState({ ...state, loading: false });
          return false;
        });
    }

    useEffect(()=>{
        refreshList(filter);
    },[limit,filter])

    return (
        <Content>
            <MocoTable
            {...config.list.config(props,modalDetail)}
            {...state}
            onChange={handleTableChange}
            />
        </Content>
    )
}

export default ListForm;
