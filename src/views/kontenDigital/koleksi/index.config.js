import React from 'react';
import { Collapse } from 'antd';
import moment from 'moment';
import { Link } from 'react-router-dom';
import pathName from 'routes/pathName';
import { numberFormat } from "utils/initialEndpoint";
/* eslint-disable */

const {base:baseUrl,kontenDigital} = pathName;
export default {
    scope: {
        title: "Koleksi iLibrary",
        subtitle: "Koleksi Buku Dinas",
        breadcrumb: [
          { text: "Beranda", to: baseUrl },
          { text: "Koleksi iLibrary" }
        ]
    },
    model :{
        organization_id: '',
        publisher_id: '',
        epustaka_id: '',
        category_id: '',
        procurement_id: '',
        search: '',
        tanggal:[],
    },
    schema :{
        organization_id: { label: "Institusi" },
        publisher_id: { label: "Penerbit" },
        epustaka_id: { label: "ePustaka" },
        category_id: { label: "Klasifikasi" },
        procurement_id: { label: "Jenis Pengadaan" },
        tanggal: { label: "Tanggal Pengadaan" },
        search: { label: "Pencarian" },
    },
    list:{
        config: ({filter,modalDetail}) =>{
            return {
                columns:[
                    {
                      title: 'Kode',
                      key: 'ieisbn_code',
                      render: row => {
                        return {
                          props: {
                            style: { verticalAlign: 'top' },
                          },
                          children: (row !== null) ? 
                                    (<div>
                                        <div>ISBN:{row.catalog_isbn}</div>
                                        <div>EISBN:{row.catalog_eisbn}</div>
                                    </div>) 
                                :   (<div>
                                        <div>ISBN:-</div>
                                        <div>EISBN:-</div>
                                    </div>)
                        } 
                      }
                    },
                    {
                      title: 'Judul Buku',
                      render: row => {
                          return {
                            props: {
                              style: { verticalAlign: 'top' },
                            },
                            children: row !== null ? 
                                    (<div>
                                        <div>
                                          <Link to="#" onClick={()=>modalDetail(row.epustaka_id,row.catalog_id)}>{row.catalog_title}</Link>
                                        </div>
                                        <div>Pengarang : {row.catalog_authors}</div>
                                    </div>) 
                                    : '-'
                          }

                      }
                    },
                    {
                      title: 'Klasifikasi',
                      dataIndex: 'category_name',
                      key: 'category_name',
                      render: row => {
                          return {
                            props: {
                              style: { verticalAlign: 'top' },
                            },
                            children: row !== null ? row : '-'
                          }
                          
                      }
                    },
                    {
                      title: 'Pengadaan',
                      dataIndex: 'purchase_qty',
                      align:'right',
                      key: 'purchase_qty',
                      render: row => {
                          return {
                            props: {
                              style: { verticalAlign: 'top' },
                            },
                            children:row !== null ? row : '-'
                          }
                          
                      }
                    },
                    {
                      title: 'Copy',
                      dataIndex: 'catalog_qty',
                      key: 'catalog_qty',
                      align:'right',
                      render: row => {
                        return {
                          props: {
                            style: { verticalAlign: 'top' },
                          },
                          children:row !== null ? row : '-'
                        }
                      }
                    },
                    {
                      title: 'Pengadaan',
                      dataIndex: 'procurements',
                      key: 'procurements',
                      render: row => {
                          if(row.length > 0) {
                            return (
                              <Collapse>
                                <Collapse.Panel header={'Klik Untuk Melihat Detail'} key="1">
                                  {
                                      row.map((x,index)=> (
                                        <div key={index} style={{marginBottom:'5px'}}>
                                            <div><strong>{x.transaction_number}</strong></div>
                                            <div>Tanggal :{moment(x.transaction_date).format('DD MMMM YYYY')}</div>
                                            <div>Jumlah Copy : {x.number_of_copy}</div>
                                            <hr></hr>
                                        </div>
                                      ))
                                  }
                                </Collapse.Panel>
                              </Collapse>
                          )
                        } else {
                            return ('-');
                        }
                        
                      },
                    },
                ]
            }
        }
    },
    detail: {
      breadcrumb: [
        { text: "Beranda", to: baseUrl },
        { text: "Koleksi iLibrary", to :  kontenDigital.listCollection},
        { text: "Detail Koleksi" },
      ],
      content: {
        title: 'Informasi Konten',
        subtitle: 'Informasi Konten Digital',
      },
      list : (...props) => {
        return{
          columns:[
            {
              title: 'Tanggal',
              dataIndex:'transaction_date',
              key: 'transaction_date',
              render: row => {
                return (row) ? moment(row).format('DD - MM - YYYY') : '-';
              }
            },
            {
              title: 'Ketengan',
              key: 'transaction_note',
              dataIndex:'transaction_note',
              render: row => {
                return (row) ? row : '-'
              }
            },
            {
              title: 'Jumlah Copy',
              dataIndex:'epustaka_catalog_qty',
              align:'right',
              key: 'epustaka_catalog_qty',
              render: x => {
                return (x) ? numberFormat(x) : '-'
              }
            },
          ]
        }
      }
    }
        
}