import React, { useEffect, useState } from 'react';
import MocoTable from 'components/MocoTable';
import config from '../index.config';
/* eslint-disable */

const DetailListForm = (props) => {
    const { data } = props
    const { detail } = config;
    const [state, setState] = useState({
        loading:false,
        pagination : {
            defaultCurrent : 1,
            pageSize: 10,
            total: 0
        },
        dataSource: []
    });

    useEffect(()=>{
        setState({...state,dataSource:data});
    },[data])

    return (
        <MocoTable
        {...detail.list(props)}
        {...state}
        />
    )
}

export default DetailListForm
