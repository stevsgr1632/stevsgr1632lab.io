import React, { Fragment, useEffect, useState } from 'react';
import {Row,Col} from 'antd';
import config from './index.config';
import oProvider from 'providers/oprovider';
import {Content,Breadcrumb} from 'components';
import LoadingSpin from 'components/LoadingSpin';
import { noImagePath } from "utils/initialEndpoint";
import DetailListForm from './components/DetailListForm';
/* eslint-disable */

const DetailForm = (props) => {
    const epustaka_id = props.match.params.epustaka_id;
    const catalog_id = props.match.params.catalog_id;
    const { detail } = config;
    const [loading, setLoading] = useState(false);
    const [state, setState] = useState({});

    const style = {
        row : [8,8]
    }

    const fetchData = async () => {
        setLoading(true);
        try {
           const response = await oProvider.list(`ilibrary-collection-getone?epustaka_id=${epustaka_id}&catalog_id=${catalog_id}`);
           if(response){
               const { data } = response;
               setState({...state,...data});
               console.log('response',response);
               setLoading(false);
           }
        } catch (error) {
            setLoading(false);
        }
    }

    useEffect(()=>{
        fetchData()
    },[]);

    return (
        <Fragment>
            <Breadcrumb items={detail.breadcrumb} />
            <Content {...{...props, ...detail.content}}>
                <LoadingSpin loading={loading}>
                    <div style={{fontSize:'12pt'}}>
                        <Row gutter={style.row} style={{marginBottom:'15px'}}>
                            <Col span={4}>
                                <div style={{width:'100%'}}>
                                    <img src={(state.catalog_cover_original) ? state.catalog_cover_original : noImagePath} 
                                        onError={()=>setState({...state,catalog_cover_original:noImagePath})} 
                                        alt='Cover Buku'
                                        width="100%"/>
                                </div>
                            </Col>
                            <Col span={20}>
                                <Row gutter={style.row}>
                                    <Col span={4}>
                                        Judul Katalog
                                    </Col>
                                    <Col span={20}>
                                        : {(state.catalog_title) ? state.catalog_title : '-'}
                                    </Col>
                                </Row>
                                <Row gutter={style.row}>
                                    <Col span={4}>
                                        ISBN
                                    </Col>
                                    <Col span={8}>
                                        : {(state.catalog_isbn) ? state.catalog_isbn : '-'}
                                    </Col>
                                    <Col span={4}>
                                        Bahasa
                                    </Col>
                                    <Col span={8}>
                                        : {(state.catalog_language) ? state.catalog_language : '-'}
                                    </Col>
                                </Row>
                                <Row gutter={style.row}>
                                    <Col span={4}>
                                        e-ISBN
                                    </Col>
                                    <Col span={8}>
                                        : {(state.catalog_eisbn) ? state.catalog_eisbn : '-'}
                                    </Col>
                                    <Col span={4}>
                                        Kota Terbit
                                    </Col>
                                    <Col span={8}>
                                        : {(state.city) ? state.city : '-'}
                                    </Col>
                                </Row>
                                <Row gutter={style.row}>
                                    <Col span={4}>
                                    Kategory
                                    </Col>
                                    <Col span={8}>
                                        : {(state.category_name) ? state.category_name : '-'}
                                    </Col>
                                    <Col span={4}>
                                        Jumlah Copy
                                    </Col>
                                    <Col span={8}>
                                        : {(state.catalog_copy) ? state.catalog_copy : '-'}
                                    </Col>
                                </Row>
                                <Row gutter={style.row}>
                                    <Col span={4}>
                                    Tahun Terbit
                                    </Col>
                                    <Col span={8}>
                                        : {(state.catalog_epublish_date) ? state.catalog_epublish_date : '-'}
                                    </Col>
                                    <Col span={4}>
                                        ePustaka
                                    </Col>
                                    <Col span={8}>
                                        : {(state.epustaka_name) ? state.epustaka_name : '-'}
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row gutter={style.row}>
                            <Col span={4}>
                                Sipnosis
                            </Col>
                            <Col span={20}>
                                {(state.catalog_sipnosis) ? state.catalog_sipnosis : '-'}
                            </Col>
                        </Row>
                        <Row gutter={style.row}>
                            <Col span={4}>
                            </Col>
                            <Col span={14}>
                                <p>Riwayat Pengadaan</p>
                                <DetailListForm data={state.procurement || []}/>
                            </Col>
                        </Row>
                    </div>
                </LoadingSpin>
            </Content>
        </Fragment>
    )
}

export default DetailForm;
