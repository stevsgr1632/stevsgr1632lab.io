import React from 'react';
import { Modal, Row, Col } from 'antd';
import LoadingSpin from 'components/LoadingSpin';
import { noImagePath } from "utils/initialEndpoint";
/* eslint-disable */

const ModalDetail = (props) => {
    const { visible,setVisible,stateDataDetail,setstateDataDetail } = props;
    const { data } = stateDataDetail;

    const handleDetail = () =>{
        setVisible(false);
        setstateDataDetail({...stateDataDetail,data:{}});
    }

    return (
        <Modal 
            width={1000} 
            visible={visible} 
            onCancel={()=>handleDetail()} 
            footer={null}>
            <h4>Detail Koleksi</h4>
            <hr/>
            <LoadingSpin loading={stateDataDetail.loading}>
                <Row style={{fontWeight: 'normal', fontSize:'12pt'}} gutter={[16, 36]}>
                    <Col xs={16} sm={16} md={18} lg={19} xl={19}>
                        <h4>{(data.catalog_title) ? (data.catalog_title) : '-' }</h4>
                        <div style={{marginBottom:'8px'}}>Kota Terbit : {(data.city) ? (data.city) : '-' }</div>
                        <Row>
                            <Col xs={16} sm={16} md={14} lg={14} xl={14}>
                                <Row>
                                    <Col flex="100px">ISBN</Col>
                                    <Col flex="auto">: {data.catalog_isbn ? data.catalog_isbn : '-'}</Col>
                                </Row>
                                <Row>
                                    <Col flex="100px">E-ISBN</Col>
                                    <Col flex="auto">: {data.catalog_eisbn ? data.catalog_eisbn : '-'}</Col>
                                </Row>
                                <Row>
                                    <Col flex="100px">Tahun terbit</Col>
                                    <Col flex="auto">: {data.catalog_epublish_date ? data.catalog_epublish_date : '-'}</Col>
                                </Row>
                            </Col>
                            <Col xs={16} sm={16} md={10} lg={10} xl={10}>
                                <Row>
                                    <Col flex="100px">ePustaka</Col>
                                    <Col flex="auto">: {data.epustaka_name ? data.epustaka_name : '-'}</Col>
                                </Row>
                                <Row>
                                    <Col flex="100px">Kategori</Col>
                                    <Col flex="auto">: {data.category_name ? data.category_name : '-'}</Col>
                                </Row>
                                <Row>
                                    <Col flex="100px">Penerbit</Col>
                                    <Col flex="auto">: {data.publisher_name ? data.publisher_name : '-'}</Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col> 
                    <Col xs={10} sm={10} md={5} lg={5} xl={5}>
                        <img src={(data.catalog_cover_original) ? data.catalog_cover_original : noImagePath } alt={(data.epustaka_logo) ? data.epustaka_logo : noImagePath } width={'100%'}/>
                    </Col> 
                </Row>
            </LoadingSpin>
        </Modal>
    )
}

export default ModalDetail;
