import React from 'react';
import FormControl from './Form';
import config from './index.config';
import oProvider from 'providers/oprovider';
import { Breadcrumb, Content } from 'components';
/* eslint-disable */

const resource = 'librarian';

const App = (props) => {
  const { schema } = config;
  const { breadcrumb, content } = config.add;

  const backToList = () => {
    props.history.push('/librarian/index');
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    props.form.validateFields(async (errors, values) => {
      // console.log(errors);
      if (!errors) {
        try {
          console.log(values);
          // inserting to API
          await oProvider.insert(`${resource}`, { ...values });
          backToList();
        } catch (error) {
          console.log(error?.message);
          return false;
        }
      }
    })
  }

  schema.form = props.form;

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <FormControl
          onSubmit={handleSubmit}
          onCancel={backToList}
          schema={schema}
        />
      </Content>
    </React.Fragment>
  );
}

export default App;