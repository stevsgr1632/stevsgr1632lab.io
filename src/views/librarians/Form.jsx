import React from 'react';
import config from './index.config';
import { Button, Form, Row, Col } from 'antd';
import InputText from 'components/ant/InputText';
import InputSelect from 'components/ant/InputSelect';
import InputSelectAsync from 'components/ant/InputSelectAsync';
/* eslint-disable */

const Edit = (props) => {
  const [form] = Form.useForm();
  const { onSubmit, onCancel, schema } = props;
  const defCol = { lg: { span: 16 }, xl: { span: 14 } }

  return (
    <React.Fragment>
      <Form form={form} onFinish={onSubmit}>
        <Row>
          <Col {...defCol}>
            <InputText placeholder="Nama Anggota" name="librarian_name" schema={schema} />
          </Col>
        </Row>
        <Row>
          <Col {...defCol}>
            <InputText placeholder="Email Anggota" pattern="\S+@\S+\.\S+" title="Masukan Format email" name="librarian_email" schema={schema} />
          </Col>
        </Row>
        <Row>
          <Col {...defCol}>
            <InputText placeholder="No Telp Anggota" pattern="\(?(?:\+62|62|0)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}" title="Masukan Format No Telepon Indonesia" name="librarian_phone" schema={schema} />
          </Col>
        </Row>
        <Row>
          <Col {...defCol}>
            <InputSelectAsync name="librarian_type" schema={schema} urlapi={'generic-lookup?lookup_name=TEAM-DINAS'} placeholder="Pilih Tipe Anggota" />
          </Col>
        </Row>
        <Row>
          <Col {...defCol}>
            <InputSelect name="librarian_isactive" schema={schema} data={config.data.status} placeholder="Pilih Status Anggota" />
          </Col>
        </Row>
        <hr />
        <Button htmlType="submit">Save</Button>
        <Button className="ml-1" onClick={onCancel}>Cancel</Button>
      </Form>
    </React.Fragment>
  );
}

export default Edit;