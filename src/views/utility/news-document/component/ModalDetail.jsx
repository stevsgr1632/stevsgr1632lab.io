import React from 'react';
import Axios from 'axios';
import { Modal, Row, 
  Col, Button } from 'antd';
import randomString from 'utils/genString';
import { MessageContent } from 'common/message';
import { formDateDisplayValue } from 'utils/initialEndpoint';
import { DownloadOutlined,EyeOutlined } from '@ant-design/icons';
/* eslint-disable */

const App = ({ model = {}, visible, setmodalData }) => {

  const handleClickUnduh = async (modelData) => {
    try {
      let key = randomString(17);
      MessageContent({ type :'loading', content:'Mengunduh ... ',key, duration: 0 });
        const file = await Axios({
            url: modelData?.news_document_url,
            method: 'GET',
            responseType: 'blob', // important
        })
        if(file){
          MessageContent({ type :'success', content: 'Selesai!', key, duration: 1 });
          const url = window.URL.createObjectURL(new Blob([file.data]));
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', `${modelData.news_document_name}.${modelData.news_document_filetype}`); //or any other extension
          document.body.appendChild(link);
          link.click();
        }
    } catch (error) {
      if(error.response){
        const { data, status } = error.response;
        const errorResponse = {
          code : status,
          message : (data.message) ? data.message : data
        }
        Notification({response:errorResponse, type : 'error'});
    }
    return false;
    }
  }
  const handleClickShow = e => {
    console.log('show',e);
  }

  const stylingColor= {
    color:'#fff',
    backgroundColor:'#4DBD74',
  }

  const ukuranFile = params => {
    let data = Math.ceil(params / 1024);
    if(data < 1000) {
      return `${data} KB`;
    }else if(data > 1000){
      data = params / 1024 / 1024;
      return `${Math.ceil(data)} MB`;
    }
  }

  return (
    <Modal
      width={800}
      visible={visible}
      onCancel={() => setmodalData({ visible: false })}
      footer={null}
    >
      <h2>Pratinjau</h2>
      <div className="mt-3"></div>
      <Row gutter={24}>
        <Col span={6}>
          <h5><strong>Tanggal Unggah</strong></h5>
        </Col>
        <Col span={18}>
          <div>
            <h5>: {formDateDisplayValue(model?.created_at,'DD MMMM YYYY')}</h5>
          </div>
        </Col>
      </Row>
      <div className="mt-3"></div>
      <hr/>
      <div><h4>{model?.news_document_name}</h4></div>
      <div className="mt-3"></div>
      <div><p>{model?.news_document_note}</p></div>
      <hr/>
      <div className="mt-3"></div>
      <div style={{display:'grid',gridColumnGap:'1em',gridTemplateColumns:'7.25em 1fr'}}>
        <div style={{display: 'flex',justifyContent:'center',alignItems: 'center',borderRadius:'.2em',...stylingColor}}>
          <b>{model?.news_document_filetype?.toUpperCase()}</b>
        </div>
        <div>
          <a href='#'>{model?.news_document_filename} per {formDateDisplayValue(model?.created_at,'DD MMMM YYYY')}</a>
          <h5>{ukuranFile(model?.news_document_filesize)}</h5>
          <div>
            <Button style={{...stylingColor,marginRight:'1em'}} onClick={()=>handleClickUnduh(model)}><DownloadOutlined /></Button>
            <Button type="link" href={model?.news_document_url} style={{...stylingColor}} target="_blank" onClick={handleClickShow}><EyeOutlined /></Button>
          </div>

        </div>
      </div>
    </Modal >
  );
}

export default App;