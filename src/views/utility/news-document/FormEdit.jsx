import React from 'react';
import FormControl from './Form';
import config from './index.config';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import { Breadcrumb, Content } from 'components';
/* eslint-disable */

const { utility } = pathName;
const resource = 'news-document';

const App = (props) => {
  const { schema } = config;
  const { breadcrumb, content } = config.edit;
  const documentId = props.match.params.id;
  const [fileURL,setfileURL] = React.useState('');
  const [data,setData] = React.useState([]);
  
  const backToList = () => {
    props.history.push(utility.document.list);
  };

  const openNotification = resp => {
    Notification({response : resp });
  };

  // const getDataCatalog = async id => {
  //   try {
  //     const { data } = await oProvider.list(`news-document-get-one?id=${id}`);
  //     return setData(data);
  //   } catch (error) {
  //     console.log(error?.message);
  //   }
  // }

  const handleSubmit = async (e) => {
    let copyData = { ...e,...fileURL };
    try {
      let response = await oProvider.update(`${resource}?id=${documentId}`, copyData);
      if(response) {
        openNotification(response);
        backToList();
      }
    } catch (error) {
      console.log(error?.message);
      return false;
    }
  };

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <FormControl
          onSubmit={handleSubmit}
          onCancel={backToList}
          setdataFile={(e)=>setfileURL(e)}
          schema={schema}
          type='edit'
          paramsId={documentId}
          data={data}
        />
      </Content>
    </React.Fragment>
  );
}

export default App;