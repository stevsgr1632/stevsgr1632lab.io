import React from 'react';
import config from './index.config';
import { Modal,Typography } from 'antd';
import { MenuAksesContext } from './index';
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
import { getOne } from 'providers/dropdownapi';
import ModalDetailComp from './component/ModalDetail';
/* eslint-disable */

const resource = 'news-document';
const { confirm } = Modal;

const List = (props) => {
  const {model} = props;
  const menuAksesContext = React.useContext(MenuAksesContext);
  const [state, setState] = React.useState({loading : false});
  const [url, setUrl] = React.useState('');
  const [modalData, setmodalData] = React.useState({visible : false, model : {}});

  const { Text } = Typography;
  const showDelete = (row,history) => {
    // console.log(row,history);
    confirm({
      title: 'Hapus Dokumen SOP',
      content: <>
      <Text>Anda yakin ingin menghapus data ini (<b>{row.news_document_name}</b>) dan semua data terkait?</Text>
      <br />
      <Text type="danger">Tindakan ini tidak bisa dibatalkan</Text>
    </>,
      onOk() {
        return new Promise(async (resolve, reject) => {
          await oProvider.delete(`${resource}?id=${row.id}`);
          refresh(url, model);
          setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {},
    });
  };

  const refresh = async (url,model) => {
    setState(prev => ({...prev,loading : true}));
    if (model.search || model.status) {
      let paramsObject = {};
      if (model.search) paramsObject['news_document_name'] = model.search;
      if (model.status) paramsObject['fileType'] = model.status;     
      url += (url ? '&' : '') + new URLSearchParams(paramsObject).toString();
    }
    try {
      const { meta, data } = await oProvider.list(`${resource}${url ? '?' : ''}${url}`);
      setState({ loading : false, pagination: { pageSize: meta.limit, total: parseInt(meta.total) }, dataSource: data });
    } catch (error) {
      setState(prev => ({ ...prev, loading : false }));
      console.log(error?.message); 
      return false;
    }
  };

  const handleTableChange = (pagination, filters, sorter) => {
    let paramsObject = {};
    let url = '';
    if (pagination) url += `&page=${pagination.current}`;
    if (sorter && sorter.field) {
      url += `&sortBy=${sorter.field}`;
      url += `&sortDir=${sorter.order === 'ascend' ? 'asc' : 'desc'}`;
    }
    if (model.search) paramsObject['news_document_name'] = model.search;
    setUrl(url);
  };

  const modalDetail = async row => {
    try {
      const data = await getOne(`news-document-get-one?id=${row.id}`);
      setmodalData({visible : true, model : {...data} });
    } catch (error) {
      console.log(error?.message);
      return false;
    }
  };

  React.useEffect(() => {
    refresh(url,model);
  }, [url,model]);

  return (
    <React.Fragment>
      <MocoTable
        {...config.list.config(props,showDelete,modalDetail,menuAksesContext)}
        {...state}
        onChange={handleTableChange}
      />
      <ModalDetailComp {...modalData} setmodalData={setmodalData} />
    </React.Fragment>
  );
}

export default List;