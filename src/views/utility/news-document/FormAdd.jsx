import React from 'react';
import FormControl from './Form';
import config from './index.config';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import { Breadcrumb, Content } from 'components';
/* eslint-disable */

const { utility } = pathName;
const resource = 'news-document';

const App = (props) => {
  const { schema } = config;
  const { breadcrumb, content } = config.add;
  const [fileURL,setfileURL] = React.useState('');

  const backToList = () => {
    props.history.push(utility.document.list);
  };

  const openNotification = resp => {
    Notification({response : resp });
  };
  const handleOtherSubmit = (e) => {
    console.log(e);
  }

  const handleSubmit = async (e) => {
    let copyData = { ...e,...fileURL };
    try {
      let response = await oProvider.insert(`${resource}`, copyData);
      if(response) {
        openNotification(response);
        backToList();
      }
    } catch (error) {
      console.log(error?.message); 
      return false;
    }
  }

  return (<>
    <Breadcrumb items={breadcrumb} />
    <Content {...content}>
      <FormControl
        onSubmit={handleSubmit}
        onCancel={backToList}
        schema={schema}
        setdataFile={(e)=>setfileURL(e)}
        otherSubmit={handleOtherSubmit}
        type="add"
        data={[]}
      />
    </Content>
  </>);
}

export default App;