import React from 'react';
import pathName from 'routes/pathName';
import { Button,Menu, Dropdown } from 'antd';
import {PlusOutlined} from '@ant-design/icons';
import { getOneMenu } from 'utils/getMenuAkses';
import ActionTableDropdown from "components/ActionTableDropdown";
/* eslint-disable */

const { initial, utility, } = pathName;
const actionMenu = (row,params) => {
  const [props,showDelete,modalDetail,menuAksesContext] = params;
  const { history } = props;

    const sunting = (type) => {
      switch (type){
        case "first":
          props.history.push(utility.document.edit(`${row.id}`));
          break;
        case "second":
          props.viewDetail(row);
          break;
        case "third":
          props.history.push(utility.document.detail(`${row.id}`));
          break;
        case "detail":
          modalDetail(row);
          break;
        default:
          props.viewDetail(row);
      }
    };
  
    return (
      <Menu>
        <Menu.Item>
          <Button type="link" style={{ margin: 0 }} onClick={() => modalDetail(row)}>Pratinjau</Button>
        </Menu.Item>
        { getOneMenu(menuAksesContext,'Sunting') && 
        <Menu.Item>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => sunting('first')} text="Sunting" />
        </Menu.Item>
        }
        { getOneMenu(menuAksesContext,'Hapus') && 
        <Menu.Item>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => showDelete(row,history)} text="Hapus" />
        </Menu.Item>
        }
      </Menu>
    )
  };
export default {
  list: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Dokumen' },
    ],
    content: {
      title: 'Dokumen SOP',
      subtitle:'Lakukan pilah untuk memudahkan pencarian dan pembaruan',
      toolbars: [
        {
          text: '', type: 'primary', icon: <PlusOutlined/>,
          onClick: (e) => {
            e.history.push(utility.document.add);
          }
        },
      ]
    },
    config: (...props) => {
      return {
        columns: [
          {
            title: 'Nama Dokumen',
            dataIndex: 'news_document_name',
            key: 'news_document_name',
            width: 380,
            render: row => {
              return row !== null ? row : ' - ';
            },
            sorter:true
          },
          {
            title: 'Deskripsi',
            dataIndex: 'news_document_note',
            key: 'news_document_note'
          },
          {
            title: 'Tipe',
            width: 90,
            dataIndex: 'news_document_filetype',
            key: 'news_document_filetype',
            render: row => {
              return row !== null ? row : ' - ';
            }
          },
          {
            title: 'Ukuran',
            width: 100,
            dataIndex: 'news_document_filesize',
            key: 'news_document_filesize',
            align: 'right',
            render: row => {
              return row !== null ? parseFloat(row).toLocaleString("id-ID") : ' - ';
            }
          },
          {
            title: 'Aksi',
            key: 'aksi',
            width: 80,
            className: 'text-center',
            render: (row) => (
              <span>
                <Dropdown overlay={actionMenu(row, props)}>
                  <Button type="link" style={{ margin: 0 }} onClick={e => e.preventDefault()}>
                    <i className="icon icon-options-vertical" />
                  </Button>
                </Dropdown>
              </span>
            ),
          },
        ]
      }
    }
  },
  add: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Dokumen', to: utility.document.list },
      { text: 'New Document'},
    ],
    content: {
      title: 'Tambah Dokumen SOP',
      subtitle:'Masukkan data sesuai kebutuhan dan teliti kembali sebelum menyelesaikan'
    },
  },
  edit: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Dokumen', to: utility.document.list },
      { text: 'Pembaruan Dokumen' },
    ],
    content: {
      title: 'Pembaruan Dokumen',
      subtitle: 'Ubah Data Sesuai Kebutuhan dan Teliti Kembali Sebelum Menyelesaikan'
    },
  },
  schema: {
    news_document_name: {
      label: 'Nama Dokumen',
      rules: [
        { required: true, message: 'Silahkan input nama dokumen SOP' },
      ]
    },
    news_document_filetype: {
      label: 'Unggah Berkas',
      rules: [
        { required: true, message: 'Silahkan input berkas dokumen SOP' },
      ]
    },
    news_document_note: {
      rules: [
        { required: true, message: 'Silahkan input deskripsi dokumen SOP' },
      ]
    },
  },
  model: {
    status: '',
    search: ''
  },
  data: {
    status: [
      { value: '', text: 'Semua Tipe Berkas' },
      { value: 'pdf', text: 'PDF' },
      { value: 'docx', text: 'DOCX' },
      { value: 'xls', text: 'XLS' },
      { value: 'ppt', text: 'PPT' },

    ],
  }
};