import React from 'react';
import ListForm from './FormList';
import config from './index.config';
import { Col, Row,Form } from 'antd';
import { connect } from 'react-redux';
import pathName from 'routes/pathName';
import getMenuAkses from "utils/getMenuAkses";
import { Breadcrumb, Content } from 'components';
import InputSelect from 'components/ant/InputSelect';
import InputSearch from 'components/ant/InputSearch';
/* eslint-disable */

export const MenuAksesContext = React.createContext([]);

const App = (props) => {
  const [form] =Form.useForm();
  const { list, data } = config;
  const { utility : { document : documentPath } } = pathName;
  const [model, setModel] = React.useState(config.model);
  const [menuAkses, setmenuAkses] = React.useState([]);

  const handleChange = (name, value) => {
    setModel(prev => ({ ...prev, [name]: value }));
  }

  React.useEffect(() => {
    if(props.menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(documentPath.list);
      setmenuAkses(menuAccessFromLocalStorage);
    }    
  },[props.menuAccess]);

  return (
    <React.Fragment>
    <MenuAksesContext.Provider value={menuAkses}>
      <Breadcrumb items={list.breadcrumb} />
      <Content {...{...props,...list.content, menuAkses}}>
        <Form form={form}>
        <Row gutter={16} style={{marginTop:8}}>
            
          <Col span={8} >
            <InputSelect name="status" defaultVal={model.status || ''} text="Jenis File" data={data.status} onChange={handleChange} />
          </Col>
          <Col span={16} style={{marginTop:'0.6em'}}>
            <InputSearch name="search" text="Pencarian" placeholder="Pencarian" onSearch={handleChange} />
          </Col>
        </Row>
        </Form>
        <hr />
        <ListForm {...props} model={model} />
      </Content>
    </MenuAksesContext.Provider>
    </React.Fragment>
  );
};

const mapStateToProps = ({menuAccess}) => ({menuAccess});
export default connect(mapStateToProps)(App);