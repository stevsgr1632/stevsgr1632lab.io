import React from "react";
import { Button, Form, Row, Col, 
  Modal, Input, Space } from "antd";
import { getOne } from "providers/dropdownapi";
import InputText from "components/ant/InputText";
import InputFile from "components/ant/InputFile";
import LoadingSpin from "components/LoadingSpin";
import { UploadOutlined } from "@ant-design/icons";
import useCustomReducer from "common/useCustomReducer";
import InputTextArea from "components/ant/InputTextArea";
/* eslint-disable */

const initialData = {
  dataImg : '',
  dataFile : '',
  loading : {component : false, upload : false},
  modalPreview : { visible : false, title : ''},
};
const FormBase = (props) => {
  const [form] = Form.useForm();
  const { onSubmit, onCancel,schema, paramsId  } = props;
  const defCol = { lg: { span: 16 }, xl: { span: 14 } };
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  const styleButton = {
    backgroundColor: "#4dbd74",
    color: "#f0f3f5",
    borderColor: "#4dbd74",
  };
  React.useEffect(() => {
    const fetchData = async () => {
      reducerFunc('loading',{ component : true});
      try{
        if (paramsId && props.type === "edit") {
          const data = await getOne(`news-document-get-one?id=${paramsId}`);
          reducerFunc('dataFile',data.news_document_url,'conventional');
          reducerFunc('modalPreview',{ title : data.news_document_filename });
          form.setFieldsValue(data);
        }
        reducerFunc('loading',{ component : false});
      } catch(error){
        reducerFunc('loading',{ component : false});
        console.log(error?.message);
        return false;
      }
    };
    fetchData();
  }, []);

  const handleUploadFile = async (name, file, data) => {
    reducerFunc('loading',{ upload : true});
    reducerFunc('dataFile',data.file_url_download || '','conventional');
    props.setdataFile({
      news_document_filename: data.originalFilename,
      news_document_filetype: data.fileExt,
      news_document_filesize: data.size,
      news_document_url: data.file_url_download
    });
    reducerFunc('modalPreview',{ title : file.name });
    reducerFunc('loading',{ upload : false});
  };

  const handlePreview = () => reducerFunc('modalPreview',{ visible : true});
  const handleCancel = () => reducerFunc('modalPreview',{ visible : false});

  return (
    <LoadingSpin loading={dataReducer?.loading?.component}>
      <>
        <Form form={form} onFinish={onSubmit}>
          <Row gutter={16}>
            <Col span={12}>
              <InputText
                name="news_document_name"
                placeholder="Masukkan nama dokumen SOP"
                schema={schema}
              />
            </Col>
            <Col span={12}>
              <InputFile
                name="news_document_filetype"
                listType="picture"
                accept=".pdf,.doc,.docx,.xls,.xlsx,.ppt,.pptx"
                schema={schema}
                uploadURL="upload-document"
                onChange={handleUploadFile}
                sendAnonymously
                otherComponent={
                  <Input type="text" readOnly value={dataReducer?.modalPreview?.title} />
                }
              >
                <Button style={styleButton}>
                  <UploadOutlined /> Pilih Berkas
                </Button>
              </InputFile>
              <small>Format: PDF,DOCX,XLS,PPT</small>
            </Col>
            <Col
              {...defCol}
              style={{ display: "flex", alignItems: "center" }}
            ></Col>
          </Row>
          <Row>
            <Col span={24}>
              <label >Deskripsi</label>
              <InputTextArea
                name="news_document_note"
                placeholder="Masukkan deskripsi singkat"
                schema={schema}
              />
            </Col>
          </Row>
          <hr />
          <Space direction="horizontal" style={{float:'right'}}>
            <Button danger  type="primary" onClick={onCancel} >Batal</Button>
            <Button  htmlType="submit" className="ml-1" type="primary" >Simpan</Button>
          </Space>
        </Form>
        <Modal
          visible={dataReducer?.modalPreview?.visible}
          title={dataReducer?.modalPreview?.title}
          footer={null}
          centered
          onCancel={handleCancel}
        >
          <img alt="example" style={{ width: "100%" }} src={dataReducer?.dataImg} />
        </Modal>
      </>
    </LoadingSpin>
  );
};

export default FormBase;
