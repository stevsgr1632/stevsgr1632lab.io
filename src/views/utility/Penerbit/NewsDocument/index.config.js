import pathName from 'routes/pathName';

const { initial, utility } = pathName;

export default {
  list: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Dokumen', to: utility.document.list },
    ],
    content: {
      title: 'Dokumen SOP',
      subtitle:'Lakukan pilah untuk memudahkan pencarian dan pembaruan',
    },
  },
  model: {
    search: ''
  },
};