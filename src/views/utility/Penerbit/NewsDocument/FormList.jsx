import React from 'react';
import {Button,List} from 'antd';
import MocoList from 'components/MocoList';
import oProvider from 'providers/oprovider';
import {getOne} from 'providers/dropdownapi';
import { EyeOutlined } from '@ant-design/icons';
import ModalDetailComp from './component/ModalDetail';
/* eslint-disable */
const resource = 'news-document?limit=10';

const ListData = (props) => {
  const {model} = props;
  const [state, setState] = React.useState({});
  const [modalData, setmodalData] = React.useState({visible : false, model : {}});

  const modalDetail = async (e,row) => {
    e.preventDefault();
    try {
      const data = await getOne(`news-document-get-one?id=${row.id}`);
      setmodalData({visible : true, model : {...data} });
    } catch (error) {
      console.log(error?.message);
      return false;
    }
  };

  const refresh = async (model) => {
    try {
      const {data} = await oProvider.list(`${resource}&news_document_name=${model.search}`);
      setState({ dataSource: data });
    } catch (error) {
      console.log(error?.message);
      return false;
    }
  };

  const stylingColor= {
    color:'#fff',
    backgroundColor:'#4DBD74',
  };

  React.useEffect(() => {
    refresh(model);
  }, [Object.values(model).join()]);

  React.useEffect(() => {
    async function fetchData(){
      try {
        const {data} = await oProvider.list(`${resource}`);
        setState({ dataSource: data });
      } catch (error) {
        console.log(error?.message);
        return false;
      }
    }
    fetchData();
  }, []);

  return (
    <>
      <MocoList
        itemLayout="horizontal"
        dataSource={state?.dataSource}
        renderItem={item => (
          <List.Item>
            <List.Item.Meta
              title={<a href="https://aksaramaya.com" onClick={(e) => modalDetail(e,item)}><strong>{item.news_document_name}</strong></a>}
              description={item.news_document_note.substr(0,50)}
            />
            <div>
              <Button type="link" href={item.news_document_url} style={{...stylingColor}} onClick={(e) => modalDetail(e,item)}><EyeOutlined /></Button>
            </div>
          </List.Item>
        )}
      />
      <ModalDetailComp {...modalData} setmodalData={setmodalData} />
    </>
  );
}

export default ListData;