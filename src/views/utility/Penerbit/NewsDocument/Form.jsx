import React from "react";
import { UploadOutlined } from "@ant-design/icons";
import { Button, Form, Row, 
  Col, Modal, Input } from "antd";
import oProvider from "providers/oprovider";
import InputText from "components/ant/InputText";
import InputFile from "components/ant/InputFile";
import LoadingSpin from "components/LoadingSpin";
import useCustomReducer from "common/useCustomReducer";
import InputTextArea from "components/ant/InputTextArea";
/* eslint-disable */
const initialData = {
  loading : false,
  loadingupload : false,
  catParent : [],
  dataImg : "",
  previewVisible : false,
  previewTitle : '',
  dataFile : ''
};

const FormBase = (props) => {
  const [form] = Form.useForm();
  const { onSubmit, onCancel, schema, paramsId = "" } = props;
  const defCol = { lg: { span: 16 }, xl: { span: 14 } };
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  const styleButton = {
    backgroundColor: "#4dbd74",
    color: "#f0f3f5",
    borderColor: "#4dbd74",
  };
  React.useEffect(() => {
    const fetchData = async () => {
      reducerFunc('loading',true,'conventional');
      try {
        const catParent = await oProvider.list(`categories-dropdown`);
        reducerFunc('catParent',catParent.data,'conventional');
  
        if (props.type === "edit") {
          const { data } = await oProvider.list(`news_document?id=${paramsId}`);
          reducerFunc('dataImg',data[0].category_image,'conventional');
          form.setFieldsValue(data[0]);
        }
        reducerFunc('loading',false,'conventional');
      } catch (error) {
        reducerFunc('loading',false,'conventional');
        console.log(error?.message);
        return false;
      }
    };
    fetchData();
  }, []);

  const handleUploadFile = async (name, file, data) => {
    reducerFunc('loadingupload',true,'conventional');
    reducerFunc('dataFile',data.file_url_download || '','conventional');
    props.setdataFile({
      news_document_filename: data.originalFilename,
      news_document_filetype: data.fileExt,
      news_document_filesize: data.size,
      news_document_url: data.file_url_download
    });
    reducerFunc('previewTitle',file.name,'conventional');
    reducerFunc('loadingupload',false,'conventional');
  };

  const handlePreview = () => reducerFunc('previewVisible',true,'conventional');
  const handleCancel = () => reducerFunc('previewVisible',false,'conventional');

  return (
    <LoadingSpin loading={dataReducer?.loading}>
      <>
        <Form form={form} onFinish={onSubmit}>
          <Row gutter={16}>
            <Col span={12}>
              <InputText
                name="news_document_name"
                placeholder="Masukkan nama dokumen SOP"
                schema={schema}
              />
            </Col>
            <Col span={12}>
              <InputFile
                name="news_document_filetype"
                listType="picture"
                accept=".pdf,.docx,.xls,.ppt"
                schema={schema}
                uploadURL="upload-document"
                onChange={handleUploadFile}
                sendAnonymously
                otherComponent={
                  <Input type="text" readOnly value={dataReducer?.previewTitle} />
                }
              >
                <Button style={styleButton}>
                  <UploadOutlined /> Pilih Berkas
                </Button>
              </InputFile>
              <small>Format: PDF,DOCX,XLS,PPT</small>

              {dataReducer?.dataImg && (
                <LoadingSpin loading={dataReducer?.loadingupload}>
                  <div style={{ cursor: "pointer" }}>
                    <img
                      src={dataReducer?.dataImg}
                      onClick={handlePreview}
                      width="150px"
                      height="200px"
                      alt="no-data"
                    />
                  </div>
                </LoadingSpin>
              )}
            </Col>
            <Col
              {...defCol}
              style={{ display: "flex", alignItems: "center" }}
            ></Col>
          </Row>
          <Row>
            <Col span={24}>
              <InputTextArea
                name="news_document_note"
                placeholder="Masukkan deskripsi singkat"
                schema={schema}
              />
            </Col>
          </Row>
          <hr />
          <Button htmlType="submit">Save</Button>
          <Button className="ml-1" onClick={onCancel}>
            Cancel
          </Button>
        </Form>
        <Modal
          visible={dataReducer?.previewVisible}
          title={dataReducer?.previewTitle}
          footer={null}
          centered
          onCancel={handleCancel}
        >
          <img alt="example" style={{ width: "100%" }} src={dataReducer?.dataImg} />
        </Modal>
      </>
    </LoadingSpin>
  );
};

export default FormBase;
