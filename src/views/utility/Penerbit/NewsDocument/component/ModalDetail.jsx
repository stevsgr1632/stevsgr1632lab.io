import React from 'react';
import moment from 'moment';
import { Modal, Row, Col, Button } from 'antd';
import { DownloadOutlined } from '@ant-design/icons';
/* eslint-disable */

const App = ({ model = {}, visible, setmodalData }) => {

  const handleClickUnduh = e => {
    console.log('unduh',e);
  }

  const stylingColor= {
    color:'#fff',
    backgroundColor:'#4DBD74',
  }

  const ukuranFile = params => {
    let data = Math.ceil(params / 1024);
    if(data < 1000) {
      return `${data} KB`;
    }else if(data > 1000){
      data = params / 1024 / 1024;
      return `${Math.ceil(data)} MB`;
    }
  }

  return (
    <Modal
      width={800}
      visible={visible}
      onCancel={() => setmodalData({ visible: false })}
      footer={null}
    >
      <h2>Pratinjau</h2>
      <div className="mt-3"></div>
      <Row gutter={24}>
        <Col span={6}>
          <h5><strong>Tanggal Unggah</strong></h5>
        </Col>
        <Col span={18}>
          <div>
            <h5>: {moment(model?.created_at).format('DD MMMM YYYY')}</h5>
          </div>
        </Col>
      </Row>
      <div className="mt-3"></div>
      
      <div><h4>{model?.news_document_name} per {moment(model?.created_at).format('DD MMMM YYYY')}</h4></div>
      <div className="mt-3"></div>
      <div><h5>{model?.news_document_note}</h5></div>

      <div className="mt-3"></div>
      <div style={{display:'grid',gridColumnGap:'1em',gridTemplateColumns:'7.25em 1fr'}}>
        <div style={{display: 'flex',justifyContent:'center',alignItems: 'center',borderRadius:'.2em',...stylingColor}}>
          <b>{model?.news_document_filetype?.toUpperCase()}</b>
        </div>
        <div>
          <a href='#'>{model?.news_document_filename} per {moment(model?.created_at).format('DD MMMM YYYY')}</a>
          <h5>{ukuranFile(model?.news_document_filesize)}</h5>
          <div>
            <Button type="link" href={model?.news_document_url} style={{...stylingColor,marginRight:'1em'}} target="_blank" onClick={handleClickUnduh}><DownloadOutlined /></Button>
          </div>

        </div>
      </div>
    </Modal >
  );
}

export default App;