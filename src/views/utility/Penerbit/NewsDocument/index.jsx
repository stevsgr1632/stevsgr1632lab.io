import React from 'react';
import ListForm from './FormList';
import config from './index.config';
import { Col, Row,Form } from 'antd';
import { Breadcrumb, Content } from 'components';
import InputSearch from 'components/ant/InputSearch';
// import pathName from 'routes/pathName';
/* eslint-disable */

export default (props) => {
  // const { utility : { penerbit : { document : documentPath } } } = pathName;
  const { list } = config;
  const [model, setModel] = React.useState(config.model);
  const [form] = Form.useForm();

  const handleChange = e => {
    if(!e.target.value){
      setModel(prev => ({ ...prev, [e.target.name]: e.target.value }));
    }
  };

  const handleChangeSearch = (name, value) => {
    setModel(prev => ({ ...prev, [name]: value }));
  };

  return (
    <React.Fragment>
      <Breadcrumb items={list.breadcrumb} />
      <Content {...{...props,...list.content}}>
        <Form form={form}>
        <Row gutter={16} style={{marginTop:8}}>
          <Col span={24} style={{marginTop:'0.6em'}}>
            <InputSearch name="search" text="Pencarian" placeholder="Pencarian" onChange={handleChange} onSearch={handleChangeSearch} />
          </Col>
        </Row>
        </Form>
        <hr />
        <ListForm {...props} model={model} />
      </Content>
    </React.Fragment>
  );
};