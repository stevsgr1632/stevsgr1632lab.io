import pathName from 'routes/pathName';

const { initial, utility } = pathName;

export default {
  model: {
    search: '',
  },
  list: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Pengumuman', to: utility.penerbit.announcement.list },
    ],
    content: {
      title: 'Daftar Pengumuman',
    },
  },
}