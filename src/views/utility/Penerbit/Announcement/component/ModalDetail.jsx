import React from 'react';
import moment from 'moment';
import { Modal, Row, Col } from 'antd';
/* eslint-disable */
// const styles = {
//   tabSelected: {
//     borderBottom: '3px solid #cc0000',
//   },
//   tabNotSelected: {
//     borderBottom: '1px solid #cccccc',
//     cursor: 'pointer',
//   }
// }

const App = ({ model = {}, visible, setmodalData }) => {
  return (
    <Modal
      width={800}
      visible={visible}
      onCancel={() => setmodalData({ visible: false })}
      footer={null}
    >
      <h2>Pratinjau</h2>
      <div className="mt-3"></div>
      <Row gutter={24}>
        <Col span={6}>
          <h5><strong>Dari</strong></h5>
          <h5><strong>Kepada</strong></h5>
          <h5><strong>Tanggal Kirim</strong></h5>
          <h5><strong>Batas Tayang</strong></h5>
        </Col>
        <Col span={18}>
          <div>
            <h5>: {model?.news_created_user}</h5>
            <h5>: {model?.news_target?.join(', ')}</h5>
            <h5>: {moment(model?.news_start_date).format('DD MMMM YYYY')}</h5>
            <h5>: {moment(model?.news_end_date).format('DD MMMM YYYY')}</h5>
          </div>
        </Col>
      </Row>
      <div className="mt-3"></div>
      <h3>{model?.news_title}</h3>
      <div dangerouslySetInnerHTML={{__html: model?.news_content }}></div>
    </Modal >
  );
}

export default App;