import React from 'react';
import { List } from 'antd';
import MocoList from 'components/MocoList';
import oProvider from 'providers/oprovider';
import ModalDetailComp from './component/ModalDetail';
/* eslint-disable */

const resource = 'news?limit=10';

const ListData = (props) => {
  const { model } = props;
  const [state, setState] = React.useState({});
  const [modalData, setmodalData] = React.useState({visible : false, model : {}});

  const modalDetail = async (e,row) => {
    e.preventDefault();
    try {
      const {data : response} = await oProvider.list(`news-get-one-view?id=${row.id}`);
      setmodalData(prev => ({...prev,visible : true,model:{...response}}));
    } catch (error) {
      console.log(error?.message);
      return false;
    }
  }

  const refresh = async model => {
    try {
      const { meta, data } = await oProvider.list(`${resource}&searching=${model.search}`);
      setState({ dataSource: data });
    } catch (error) {
      console.log(error?.message);
      return false;
    }
  }

  React.useEffect(() => {
    refresh(model);
  }, [model]);

  React.useEffect(() => {
    async function fetchData(){
      try {
        const { meta, data } = await oProvider.list(`${resource}`);
        setState({ dataSource: data });
      } catch (error) {
        console.log(error?.message);
        return false;
      }
    }
    fetchData();
  }, []);

  return (<>
    <MocoList
      itemLayout="horizontal"
      dataSource={state?.dataSource}
      renderItem={item => (
        <List.Item>
          <List.Item.Meta
            title={<a href="https://aksaramaya.com" onClick={(e) => modalDetail(e,item)}><strong>{item.news_title}</strong></a>}
            description={<div dangerouslySetInnerHTML={{ __html: item.news_content.substr(0,50) + '...' }} />}
          />
        </List.Item>
      )}
    />
    <ModalDetailComp {...modalData} setmodalData={setmodalData} />
  </>);
}

export default ListData;