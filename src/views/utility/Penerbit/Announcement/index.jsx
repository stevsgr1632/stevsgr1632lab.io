import React, { useState } from 'react';
import ListForm from './FormList';
import config from './index.config';
import { Col, Row, Form } from 'antd';
import { Breadcrumb, Content } from 'components';
import InputSearch from 'components/ant/InputSearch';
// import pathName from 'routes/pathName';
/* eslint-disable */
export default (props) => {
  // const { utility : { penerbit : { announcement : announcementPath } } } = pathName;
  const [form] = Form.useForm();
  const { list } = config;
  const [model, setModel] = useState(config.model);

  const handleChange = (name, value) => {
    setModel(prev => ({ ...prev, [name]: value }));
  };
  const handleData = e => {
    if(!e.target.value){
      setModel(prev => ({ ...prev, [e.target.name]: e.target.value }));
    }
  };

  return (
    <React.Fragment>
      <Breadcrumb items={list.breadcrumb} />
      <Content {...{ ...props, ...list.content }}>
        <Form form={form}>
          <Row gutter={16}>
            <Col md={24} lg={24}>
              <InputSearch name="search" text="Pencarian" placeholder="Cari Berdasarkan judul" onChange={handleData} onSearch={handleChange} />
            </Col>
          </Row>
        </Form>
        <hr />
        <ListForm {...props} model={model} />
      </Content>
    </React.Fragment>
  );
};