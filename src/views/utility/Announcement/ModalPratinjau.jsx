import React from 'react';
import { Modal,Row,Col, Typography } from "antd";
import { formDateDisplayValue } from "utils/initialEndpoint";
/* eslint-disable */
const format = 'DD MMMM YYYY';
const ModalPratinjau = props => {
  return(
  <Modal 
    title='Pratinjau'
    visible={props.visible}
    onCancel={() => props.setPratinjau('pratinjau',{visible : false})}
    footer={null}
  >
    <Row>
      <Col span={8}><b>Dari</b></Col>
      <Col span={16}>: {props.data?.news_created_user}</Col>
      <Col span={8}><b>Kepada</b></Col>
      <Col span={16}>: {props.data?.news_created_user}</Col>
      <Col span={8}><b>Tanggal Kirim</b></Col>
      <Col span={16}>: {formDateDisplayValue(props.data?.news_start_date,format)}</Col>
      <Col span={8}><b>Batas waktu tayang</b></Col>
      <Col span={16}>: sampai dengan {formDateDisplayValue(props.data?.news_end_date,format)}</Col>
    </Row>
    <hr />
    <Typography.Title level={4}>{props.data?.news_title}</Typography.Title>
    <div dangerouslySetInnerHTML={{__html : props.data?.news_content }}></div>
  </Modal>);
};

export default ModalPratinjau;