import React from 'react';
import ListForm from './FormList';
import config from './index.config';
import { connect } from 'react-redux';
import pathName from 'routes/pathName';
import randomString from 'utils/genString';
import oProvider from 'providers/oprovider';
import getMenuAkses from "utils/getMenuAkses";
import { MessageContent } from 'common/message';
import { Breadcrumb, Content } from 'components';
import { Col, Row, Form, DatePicker } from 'antd';
import InputSearch from 'components/ant/InputSearch';
/* eslint-disable */
export const MenuAksesContext = React.createContext([]);

const App = (props) => {
  const [form] = Form.useForm();
  const { utility : { announcement : announcementPath } } = pathName;
  const { list } = config;
  const [model, setModel] = React.useState(config.model);
  const [menuAkses, setmenuAkses] = React.useState([]);

  const handleChange = (name, value) => {
    setModel(prev => ({ ...prev, [name]: value }));
  }

  const handleDateChange = async (_, newDates) => {
    setModel(prev => ({ ...prev, 'start_date': newDates[0], 'end_date': newDates[1] }));
  }

  const downloadExcel = async () => {
    let key = randomString(17);
    MessageContent({ type :'loading', content:'Mengunduh ... ',key, duration: 0 });
    try {
      const response = await oProvider.downloadfile('news-download-excel');
      if(response){
        MessageContent({ type :'success', content: 'Selesai!', key, duration: 2 });
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'pengumuman-MCCP.xls'); //or any other extension
        document.body.appendChild(link);
        link.click();
      }
    } catch (error) {
      MessageContent({ type :'error', content: 'Terjadi kesalahan saat download !', key, duration: 2 });
      console.log(error?.message);
      return false;
    }
  }

  const { RangePicker } = DatePicker;

  React.useEffect(() => {
    if(props.menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(announcementPath.list);
      setmenuAkses(menuAccessFromLocalStorage);
    }    
  },[props.menuAccess]);

  return (
    <React.Fragment>
    <MenuAksesContext.Provider value={menuAkses}>
      <Breadcrumb items={list.breadcrumb} />
      <Content {...{ ...props, ...list.content, menuAkses, downloadExcel }}>
        <Form form={form}>
          <Row gutter={16}>
            <Col md={24} lg={16}>
              <InputSearch name="search" text="Pencarian" placeholder="Pencarian berdasarkan judul" onSearch={handleChange} />
            </Col>
            <Col md={24} lg={8}>
              <label>Tanggal</label>
              <br />
              <RangePicker style={{ width: '100%' }}
                placeholder={['Tanggal Mulai', 'Tanggal Berakhir']}
                onCalendarChange={handleDateChange} />
            </Col>
          </Row>
        </Form>
        <hr />
        <ListForm {...props} model={model} />
      </Content>
    </MenuAksesContext.Provider>
    </React.Fragment>
  );
};
const mapStateToProps = ({menuAccess}) => ({menuAccess});
export default connect(mapStateToProps)(App);