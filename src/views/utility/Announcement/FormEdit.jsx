import React from 'react';
import { message } from 'antd';
import FormControl from './Form';
import config from './index.config';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import { Breadcrumb, Content } from 'components';
/* eslint-disable */

const resource = 'news';

const App = (props) => {
  const { schema } = config;
  const { breadcrumb, content, pushBackUrl } = config.edit;
  const newsId = props.match.params.id;

  const backToList = () => {
    props.history.push(pushBackUrl);
  };

  const openNotification = resp => {
    Notification({ response : resp });
  };

  const handleSubmit = async (values) => {
    values['news_target'] = values['news_target'].join();
    values['news_start_date'] = values['news_range'][0].format("DD-MM-YYYY");
    values['news_end_date'] = values['news_range'][1].format("DD-MM-YYYY");
    delete values['news_range'];

    try {
      const response = await oProvider.update(`${resource}?id=${newsId}`, { ...values });
      openNotification(response);
      backToList();
    } catch (error) {
      message.error('Data tidak berhasil disimpan');
      return false;
    }
  }

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <FormControl
          onSubmit={handleSubmit}
          onCancel={backToList}
          schema={schema}
          type='edit'
          paramsId={newsId}
        />
      </Content>
    </React.Fragment>
  );
}

export default App;