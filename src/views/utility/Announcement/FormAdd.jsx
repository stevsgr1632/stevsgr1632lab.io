import React from "react";
import FormControl from "./Form";
import config from "./index.config";
import oProvider from "providers/oprovider";
import { message, notification } from "antd";
import { Breadcrumb, Content } from "components";
/* eslint-disable */

const resource = 'news';

const App = (props) => {
  const { schema } = config;
  const { breadcrumb, content, pushBackUrl } = config.add;

  const backToList = () => {
    props.history.push(pushBackUrl);
  };

  const openNotification = resp => {
    notification.info({
      message: resp.code,
      description: `${resp.message} Terima kasih.`,
      placement: 'bottomRight',
    });
  };

  const handleSubmit = async (values) => {
    values['news_target'] = values['news_target'].join();
    values['news_start_date'] = values['news_range'][0].format("DD-MM-YYYY");
    values['news_end_date'] = values['news_range'][1].format("DD-MM-YYYY");
    delete values['news_range'];

    try {
      // inserting to API
      const response = await oProvider.insert(`${resource}`, { ...values });
      openNotification(response);
      backToList();
    } catch (error) {
      message.error('Data tidak berhasil disimpan');
      return false;
    }
  };

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <FormControl
          onSubmit={handleSubmit}
          onCancel={backToList}
          schema={schema}
        />
      </Content>
    </React.Fragment>
  );
};

export default App;
