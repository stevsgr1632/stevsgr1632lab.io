import React from 'react';
import config from './index.config';
import { Modal, Typography } from 'antd';
import { MenuAksesContext } from './index';
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
import ModalPratinjau from "./ModalPratinjau";
import Notification from 'common/notification';
import { DeleteOutlined } from '@ant-design/icons';
import useCustomReducer from 'common/useCustomReducer';
/* eslint-disable */
const resource = 'news';
const { confirm } = Modal;
const { Text } = Typography;
const initialData = {
  url : '',
  state : {loading:false},
  pratinjau : {visible : false},
};

const List = (props) => {
  const menuAksesContext = React.useContext(MenuAksesContext);
  const { model } = props;
  const [dataReducer,refucerFunc] = useCustomReducer(initialData);

  const showConfirmDelete = (props) => {
    confirm({
      title: 'Hapus Pengumuman',
      icon: <DeleteOutlined />,
      content: <React.Fragment>
        <Text>Anda yakin ingin menghapus data ini (<b>{props.news_title}</b>) dan semua data terkait?</Text>
        <br />
        <Text type="danger">Tindakan ini tidak bisa dibatalkan</Text>
      </React.Fragment>,
      onOk: async () => {
        try {
          const response = await oProvider.delete(`${resource}?id=${props.id}`);
          if (!response) {
            Notification({ type : 'error', response : {
                message: 'Data gagal dihapus',
                description: 'Silahkan muat ulang halaman dan coba lagi',
              } 
            });
            return;
          }
  
          await refresh(dataReducer?.url, model);
          Notification({ type : 'success', response });
        } catch (error) {
          console.log(error?.message);
          return false;
        }
      },
      onCancel() { },
    });
  };

  const modalPratinjau = async row => {
    try {
      // let resp = await oProvider.list(`news-select-one?id=${row.id}`);
      refucerFunc('pratinjau',{visible : true, data : row});
    } catch (error) {
      console.log(error?.message);
    }
  }

  const refresh = async (url, model) => {
    refucerFunc('state',{loading : true});
    if (model.search || model.start_date || model.end_date) {
      let paramsObject = {};
      if (model.search) paramsObject['searching'] = model.search;
      if (model.start_date) paramsObject['startDate'] = model.start_date;
      if (model.end_date) paramsObject['endDate'] = model.end_date;
      url += (url ? '&' : '') + new URLSearchParams(paramsObject).toString();
    }
    try {
      const { meta, data } = await oProvider.list(`${resource}${url ? '?' : ''}${url}`);
      refucerFunc('state',{ loading : false, pagination: { pageSize: meta.limit, total: parseInt(meta.total) }, dataSource: data });
    } catch (error) {
      refucerFunc('state',{loading : false});
      console.log(error?.message);
      return false;
    }
  }

  const handleTableChange = (pagination, filters, sorter) => {
    let paramsObject = {};
    if (pagination) paramsObject['page'] = pagination.current;
    if (sorter && sorter.field) {
      paramsObject['sortBy'] = sorter.field;
      paramsObject['sortDir'] = sorter.order === 'ascend' ? 'asc' : 'desc';
    }
    if (model.search) paramsObject['searching'] = model.search;
    refucerFunc('url',new URLSearchParams(paramsObject).toString(),'conventional');
  }

  React.useEffect(() => {
    refresh(dataReducer?.url, model);
  }, [dataReducer?.url, Object.values(model).join()]);

  return (<>
    <MocoTable
      {...config.list.config(props, modalPratinjau, showConfirmDelete, menuAksesContext)}
      {...dataReducer?.state}
      onChange={handleTableChange}
    />
    <ModalPratinjau {...dataReducer?.pratinjau} setPratinjau={refucerFunc} />
  </>);
}

export default List;