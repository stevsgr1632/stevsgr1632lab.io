import React, { useState } from 'react';
import { Button, Form, Row, Col, 
  DatePicker, Input, Space } from 'antd';
import moment from 'moment';
import oProvider from 'providers/oprovider';
import MocoEditor from 'components/MocoEditor';
import LoadingSpin from 'components/LoadingSpin';
import InputText from 'components/ant/InputText';
import InputSelect from 'components/ant/InputSelect';
/* eslint-disable */

const resource = 'news';

const FormBase = (props) => {
  const [form] = Form.useForm();
  const { onSubmit, onCancel, schema, schema: { news_range }, paramsId } = props;
  const [role, setRole] = useState([]);
  const [loading, setLoading] = useState(false);

  const { RangePicker } = DatePicker;

  const handleEditorChange = value => {
    form.setFieldsValue({ news_content: value });
  };

  React.useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const role = await oProvider.list('role-dropdown');
        setRole(role.data);
        if (paramsId && props.type === 'edit') {
          const formData = await oProvider.list(`${resource}-select-one?id=${paramsId}`);
          formData['news_range'] = [moment(formData['news_start_date']), moment(formData['news_end_date'])];
          delete formData['news_start_date'];
          delete formData['news_end_date'];
          form.setFieldsValue(formData);
        }
        setLoading(false);
      } catch (error) {
        setLoading(false);
        console.log(error?.message);
        return false;
      }
    }
    fetchData();
  }, []);

  const contentData = form.getFieldValue('news_content');

  return (
    <LoadingSpin loading={loading}>
      <Form form={form} onFinish={onSubmit} labelCol={{ span: 24 }}>
        <InputText name="news_title" schema={schema} />
        <Form.Item required label={schema.news_content.label}>
          <MocoEditor value={contentData ? contentData.toString() : ''} noLabel onChange={handleEditorChange} />
          <Form.Item name="news_content" rules={schema.news_content.rules} noStyle>
            <Input type="hidden" />
          </Form.Item>
        </Form.Item>
        <Row gutter={16}>
          <Col lg={16} md={24}>
            <InputSelect name="news_target" schema={schema} data={role}
              mode="multiple" addressApi="role-dropdown" placeholder="Pilih Target Penerima Informasi" />
          </Col>
          <Col lg={8} md={24}>
            <Form.Item name={"news_range"} label={news_range.label} rules={news_range.rules}>
              <RangePicker style={{ width: '100%' }}
                placeholder={['Tanggal Mulai', 'Tanggal Berakhir']} />
            </Form.Item>
          </Col>
        </Row>
        <hr />
        <Space direction="horizontal" style={{float:'right'}}>
          <Button onClick={onCancel} type="primary" danger>Batal</Button>
          <Button htmlType="submit" className="ml-1" type="primary" >Simpan</Button>
        </Space>
      </Form>
    </LoadingSpin>
  );
}

export default FormBase;