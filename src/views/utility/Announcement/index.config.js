import React from 'react';
import moment from 'moment';
import { Tag, Button, Dropdown,
  Menu, Collapse } from 'antd';
import pathName from 'routes/pathName';
import { PlusOutlined, 
  FileExcelOutlined } from '@ant-design/icons';
import randomString from 'utils/genString';
import { getOneMenu } from 'utils/getMenuAkses';
import ActionTableDropdown from "components/ActionTableDropdown";
/* eslint-disable */

const { initial, utility } = pathName;

const actionMenu = (row, params) => {
  const [props,modalPratinjau,modalDelete,menuAksesContext]= params;
  let endDate = moment(row.news_end_date);
  let now = moment(new Date());
  let diff = now.diff(endDate);
  return (
    <Menu>
      {
      diff < -1 &&
      <Menu.Item>
        <Button type="link" onClick={() => modalPratinjau(row)}>Pratinjau</Button>
      </Menu.Item>
      }
      { getOneMenu(menuAksesContext,'Sunting') && 
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => props.history.push(utility.announcement.edit(row.id))} text="Sunting" />
      </Menu.Item>
      }
      { getOneMenu(menuAksesContext,'Hapus') &&
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => modalDelete(row)} text="Hapus" />
      </Menu.Item>
      }
    </Menu>
  );
}

export default {
  add: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Pengumuman', to: utility.announcement.list },
      { text: 'Tambah Pengumuman' },
    ],
    content: {
      title: 'Tambah Pengumuman',
      subtitle: 'Tambah Data Sesuai Kebutuhan dan Teliti Kembali Sebelum Menyelesaikan',
    },
    pushBackUrl: utility.announcement.list,
  },
  edit: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Pengumuman', to: utility.announcement.list },
      { text: 'Pembaharuan Pengumuman' },
    ],
    content: {
      title: 'Pembaharuan Pengumuman',
      subtitle: 'Ubah Data Sesuai Kebutuhan dan Teliti Kembali Sebelum Menyelesaikan'
    },
    pushBackUrl: utility.announcement.list,
  },
  model: {
    search: '',
    start_date: '',
    end_date: ''
  },
  list: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Pengumuman', to: utility.announcement.list },
    ],
    content: {
      title: 'Daftar Pengumuman',
      subtitle: 'Lakukan pilah untuk memudahkan pencarian dan pembaruan',
      toolbars: [{
          text: '', type: 'primary', icon: <FileExcelOutlined />,
          onClick: (e) => {
            e.downloadExcel();
          },
          tooltip: 'Download data'
        },
        {
          text: '', type: 'primary', icon: <PlusOutlined />,
          onClick: (e) => {
            e.history.push(utility.announcement.add);
          }
        },
      ]
    },
    config: (...props) => {
      return {
        columns: [
          {
            title: 'Judul',
            sorter: true,
            render: (row) => {
              return {
                props: {
                  style: { verticalAlign: 'top' },
                },
                children: (
                  <span>
                    <div>
                      <strong>{row.news_title}</strong>
                    </div>
                    <div dangerouslySetInnerHTML={{ __html: row.news_content }} />
                  </span>
                )
              }
            }
          },
          {
            title: 'Pembuat',
            width: 180,
            key: 'news_created_user',
            sorter: true,
            render: row => {
              return {
                props: {
                  style: { verticalAlign: 'top' },
                },
                children: (
                  <span>
                    <div>{row.news_created_user}</div>
                  </span>
                )
              }
            }
          },
          {
            title: 'Target Penerima Informasi',
            key: 'news_targets',
            render : x => {
              return {
                props: {
                  style: { verticalAlign: 'top' },
                },
                children: (
                  <Collapse>
                    <Collapse.Panel header={'Klik Untuk Melihat Detail'} key="1">
                      <ul>
                        {x.news_targets.map((item,index) => (<li key={index}>{item}</li>))}
                      </ul>
                    </Collapse.Panel>
                  </Collapse>
                )
              }
            }
          },
          {
            title: 'Waktu Tampil',
            width: 130,
            key: 'news_start_date',
            dataIndex: 'news_start_date',
            render: (row, row_full) => {
              return {
                props: {
                  style: { verticalAlign: 'top' },
                },
                children: (
                  <span>
                    <div>
                      {moment(row).format("DD-MM-YYYY")}
                    </div>
                    <div>
                      {moment(row_full.news_end_date).format("DD-MM-YYYY")}
                    </div>
                  </span>
                ),
                sorter: true
              }
            }
          },
          {
            title: 'Status',
            width: 130,
            key: 'news_status_name',
            dataIndex: 'news_status_name',
            render: (status,row) => {
              let color,nameStatus;
              let endDate = moment(row.news_end_date);
              let now = moment(new Date());
              let diff = now.diff(endDate);
              console.log(diff);
              if(diff < -1){
                color = 'green';
                nameStatus = 'Aktif';
              }else{
                color = 'volcano';
                nameStatus = 'Kedaluwarsa';
              }
              return {
                props: {
                  style: { verticalAlign: 'top' },
                },
                children: (
                  <Tag color={color} key={randomString(5)}>
                    {nameStatus.toUpperCase()}
                  </Tag>
                ), sorter: true
              }
            }
          },
          {
            title: 'Aksi',
            key: 'operation',
            width: 80,
            className: 'text-center',
            render: row => {
              return {
                props: {
                  style: { verticalAlign: 'top' },
                },
                children: (
                  <Dropdown overlay={actionMenu(row, props)}>
                    <Button type="link" style={{ margin: 0 }} onClick={e => e.preventDefault()}>
                      <i className="icon icon-options-vertical" />
                    </Button>
                  </Dropdown>
                )
              }
            }
          },
        ]
      }
    },
  },
  schema: {
    news_title: {
      label: "Judul",
      rules: [
        { required: true, message: "Silahkan masukkan judul" },
        { max: 250, message: "Judul maksimal 250 karakter" },
      ]
    },
    news_content: {
      label: "Isi Pengumuman",
      rules: [
        { required: true, message: "Silahkan masukkan isi Pengumuman" },
        { min: 50, message: "Isi Pengumuman minimal 50 karakter" },
      ]
    },
    news_target: {
      label: "Target Penerima Informasi",
      rules: [
        { required: true, message: "Silahkan masukkan Target Penerima Informasi" },
      ]
    },
    news_range: {
      label: "Waktu Tampil",
      rules: [
        { required: true, message: "Silahkan masukkan waktu tampil pemberitahuan" },
        () => ({
          validator(_, values) {
            if (values && values[0] > values[1]) {
              return Promise.reject('Tanggal mulai harus tidak lebih dari tanggal berakhir!');
            }
            return Promise.resolve();
          },
        }),
      ]
    },
  },
}