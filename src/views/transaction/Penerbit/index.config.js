import React from 'react';
import { Button, Dropdown,
  Menu, Badge, Row, Col, 
  Space, Typography } from 'antd';
import pathName from 'routes/pathName';
import randomString from 'utils/genString';
import { getOneMenu } from 'utils/getMenuAkses';
import ActionTableDropdown from "components/ActionTableDropdown";
import { numberFormat,formDateDisplayValue } from 'utils/initialEndpoint';
/* eslint-disable */
const { initial, utility, transaction, baseUrl } = pathName;

const actionMenu = (row, params) => {
  const [props, modalDelete,menuAksesContext,modalApprove]= params;
  return (
    <Menu>
      { getOneMenu(menuAksesContext,'List') && row.transaction_status === '19e5c380-9765-4c58-875e-25f2826d67db' && <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => modalApprove(row)} text="Approve Transaksi" />
      </Menu.Item>}
      { getOneMenu(menuAksesContext,'Sunting') && <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => props.history.push(utility.announcement.edit(row.id))} text="Sunting" />
      </Menu.Item>}
      { getOneMenu(menuAksesContext,'Hapus') && <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => modalDelete(row)} text="Hapus" />
      </Menu.Item>}
    </Menu>
  );
};
const actionMenuDetail = (row, params) => {
  const [props,modalAktivasi] = params; // {history, match, location,staticContext}
  return (
    <Menu>
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => modalAktivasi(row)} text="Aktivasi Konten" />
      </Menu.Item>
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => console.log(props)} text="Detail Konten" />
      </Menu.Item>
    </Menu>
  );
};

const statusColor = status => {
  let color = '';
  switch (status.toLowerCase()) {
    case 'aktivasi buku':
      color = '#ff751a';
      break;
    case 'aktivasi gagal':
      color = '#ff3333';
      break;
    case 'transfer berhasil':
      color = '#52c41a';
      break;
    case 'approve penerbit':
      color = '#52c41a';
      break;
    default:
      color = '#108ee9';
      break;
  }
  return (
    <Badge color={color} text={status} key={randomString(5)}/>
  );
};

export default {
  scope: {
    title: "Transaksi Order Pemesanan",
    subtitle: "Lakukan pilah untuk memudahkan pencarian",
    breadcrumb: [
      { text: "Beranda", to: baseUrl },
      { text: "Transaksi", to: transaction.penerbit.list },
    ]
  },
  model: {
    search: '',
    start_date: '',
    end_date: '',
    status_code:'',
    ilibrary_id:''
  },
  schema : {
    tanggal: { label: "Tanggal Order" },
    status_code: { label: "Status" },
    ilibrary_id: { label: "iLibrary" },
    search: { label: "Pencarian" },
  },
  breadcrumb: [
    { text: 'Beranda', to: initial },
    { text: 'Transaksi', to: transaction.penerbit.list },
  ],
  config: (...props) => {
    return {
      columns: [
        {
          title: 'Tanggal Order',
          width: 130,
          key: 'transaction_date',
          dataIndex: 'transaction_date',
          render: (x, row) => x ? formDateDisplayValue(x,'DD-MM-YYYY') : '-',
        },
        {
          title: 'Nomor Transaksi',
          sorter: true,
          key:'transaction_number',
          dataIndex : 'transaction_number',
          render: (x) => x,
        },
        {
          title: 'ePustaka',
          sorter: true,
          key : 'ilibrary_name',
          dataIndex : 'ilibrary_name',
          render: (x) => x,
        },
        {
          title: 'Metode bayar',
          width: 180,
          key: 'transaction_amount',
          dataIndex: 'transaction_amount',
          render : x => '-'
        },
        {
          title: 'Nominal (Rp)',
          width: 180,
          key: 'catalog_selection_amount',
          dataIndex: 'catalog_selection_amount',
          render : x => numberFormat(x),
        },
        {
          title: 'Status',
          key: 'status_name',
          dataIndex: 'status_name',
          render: x => statusColor(x),
        },
        {
          title: 'Aksi',
          key: 'operation',
          width: 80,
          className: 'text-center',
          render: row => (
            <Dropdown overlay={actionMenu(row, props)}>
              <Button type="link" style={{ margin: 0 }} onClick={e => e.preventDefault()}>
                <i className="icon icon-options-vertical" />
              </Button>
            </Dropdown>
          ),
        },
      ]
    }
  },
  configDetail : (...params) => {
    return {
      columns : [
        {
          title: 'Penerbit',
          sorter: true,
          key : 'organization_group_name',
          dataIndex : 'organization_group_name',
          render: x => {
            return {
              props: {
                style: { verticalAlign: 'top' },
              },
              children: (
                <Typography.Text>{x}</Typography.Text>
              )
            }
          },
        },
        {
          title: 'Work Order',
          key : 'catalog_publisher',
          render: row => {
            return {
              props: {
                style: { verticalAlign: 'top' },
              },
              children: (
                <Space direction="vertical">
                  <Space direction="horizontal">
                    Nomor : <Typography.Text>{row.transaction_wo_number}</Typography.Text>
                  </Space>
                  <Space direction="horizontal">
                  Tanggal : <Typography.Text>{row.transaction_wo_date ? formDateDisplayValue(row.transaction_wo_date,'DD-MM-YYYY') : '-'}</Typography.Text>
                  </Space>
                  <Typography.Text strong>Bagi Hasil</Typography.Text>
                  <Typography.Text>Penerbit({row.margin_publisher}%) : {numberFormat(row.margin_publisher_amount)}</Typography.Text>
                  <Typography.Text>Aksaramaya({row.margin_am}%) : {numberFormat(row.margin_am_amount)}</Typography.Text>
                </Space>
              )
            }
          },
        },
        {
          title: 'Pengadaan (Rp)',
          align:'right',
          key : 'transaction_amount',
          dataIndex : 'transaction_amount',
          render: x => ({
            props : {
              style : { verticalAlign: 'top' }
            },
            children : (
              <Typography.Text>{numberFormat(x)}</Typography.Text>
            )
          })
        },
        {
          title: 'Diskon (%)',
          align:'right',
          key : 'transaction_disc1',
          dataIndex : 'transaction_disc1',
          render: x => ({
            props : {
              style : { verticalAlign: 'top' }
            },
            children : (
              <Typography.Text>{x}</Typography.Text>
            )
          })
        },
        {
          title: 'Total (Rp)',
          align:'right',
          key : 'net_amount',
          dataIndex : 'net_amount',
          render: x => ({
            props : {
              style : { verticalAlign: 'top' }
            },
            children : (
              <Typography.Text>{numberFormat(x)}</Typography.Text>
            )
          })
        },
        {
          title: 'Status',
          key : 'status_name',
          dataIndex : 'status_name',
          render: x => ({
            props : {
              style : { verticalAlign: 'top' }
            },
            children : statusColor(x)
          })
        },
        {
          title: 'Aksi',
          key: 'operation',
          width: 80,
          className: 'text-center',
          render: row => ({
            props : {
              style : { verticalAlign: 'top' }
            },
            children : (
              <Dropdown overlay={actionMenuDetail(row, params)}>
                <Button type="link" style={{ margin: 0 }} onClick={e => e.preventDefault()}>
                  <i className="icon icon-options-vertical" />
                </Button>
              </Dropdown>
            )
          })      
        },
      ]
    }
  },
  configAktivasi : (...params) => {
    return {
      columns : [
        {
          title: 'ISBN',
          key:'transaction_isbn',
          dataIndex : 'transaction_isbn',
          render: (x) => x ? x : '-',
        },
        {
          title: 'Nama Katalog',
          key:'catalog_name',
          dataIndex : 'catalog_name',
          render: (x) => x ? x : '-',
        },
        {
          title: 'ePustaka',
          sorter: true,
          key : 'ilibrary_name',
          dataIndex : 'ilibrary_name',
          render: (x) => x ? x : '-',
        },
        {
          title: 'Copy',
          width: 120,
          align: 'right',
          key: 'catalog_copy',
          dataIndex: 'catalog_copy',
          render : x => x ? numberFormat(x) : '0',
        },
      ]
    }
  },
  approve:{
    content: {
      title :'Order Pembelian Katalog',
      subtitle:'Persetujuan Pembelian Katalog',
    },
    list_left:(...params) => {
      return {
        columns : [
          {
            title: 'Perhitungan Bagi Hasil (Rp)',
            render: row => {
              return (
                <Row gutter={[0,0]}>  
                    <Col span={16}>{(row.keterangan) ? row.keterangan : '-'}</Col>
                    <Col span={8} style={{textAlign:'right'}}>
                      {(row.amount) ? numberFormat(row.amount) : '-'}
                    </Col>
                </Row>
              );
            }
          }
        ]
      }
    },
    list_right:(...params) =>{
      return {
        columns : [
          {
            title: 'Penerbit',
            key : 'organization_name',
            dataIndex:'organization_name',
            render: x => x ? numberFormat(x) : 0
          },
          {
            title: 'Copy',
            key : 'catalog_copy',
            dataIndex:'catalog_copy',
            align:'right',
            render: x => x ? numberFormat(x) : 0
          },
          {
            title: 'Nominal (Rp)',
            key : 'amount',
            dataIndex:'amount',
            align:'right',
            render: x => x ? numberFormat(x) : 0
          }]
        }
      }
  }
}