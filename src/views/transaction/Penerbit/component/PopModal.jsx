import React from 'react';
import config from '../index.config';
import { Button, Col, Row, Table } from 'antd';
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
import Notification from 'common/notification';
import LoadingSpin from 'components/LoadingSpin';
import MocoPopModal from 'components/MocoPopModal';
import useCustomReducer from 'common/useCustomReducer';
import { numberFormat,formDateDisplayValue } from 'utils/initialEndpoint';
/* eslint-disable */

const initialData = {
  state : {},
  loading : false,
  btnLoading : false,
  stateBagi : {
    loading:false,
    pagination:false,
    dataSource:[]
  },
  statePenerbit : {
    loading:false,
    pagination:false,
    dataSource:[]
  }
};

export default (props) => {
  const { approve } = config;
  const { modal, onClose } = props;
  const [ dataReducer, reducerFunc] = useCustomReducer(initialData);

  const style = {
    col_1 : { xs:24, sm:24, md:12, lg:12, xl:12 },
    col_2 : { xs:20, sm:20, md:20, lg:12, xl:12 }  
  }

  const onCloseModal = () => {
    reducerFunc('state',{dataSource:[]});
    reducerFunc('stateBagi',{dataSource:[]});
    reducerFunc('statePenerbit',{dataSource:[]});
    onClose();
  }

  const handleProcess = async () => {
      reducerFunc('btnLoading',true,'conventional');
      try {
        const response = await oProvider.update(`selection-planning-publisher-approve?transaction_id=${modal.data.catalog_selection_id}&organization_group_id=${modal.data.organization_group_id}`)
        if(response){
          Notification({
            response : {
              code : 'Approve transaksi berhasil',
              message : response.message
            },
            type: 'success'
          });
        }
        reducerFunc('btnLoading',false,'conventional');
        onCloseModal();
      } catch (error) {
        Notification({
          response : {
            code : 'Terjadi kesalahan saat proses data',
            description : error.response?.data?.message
          },
          type: 'error'
        });
        reducerFunc('btnLoading',false,'conventional');
        console.log('error',error);
      }
  }

  const fetchData = async (row) => {
    reducerFunc('loading',true,'conventional');
    try {
      const response = await oProvider.list(`selection-planning-institution-getone?transaction_id=${row.catalog_selection_id}&organization_group_id=${row.organization_group_id}`)

      if(response){
        const { data } = response;
        reducerFunc('state',data);
        reducerFunc('statePenerbit',{dataSource:data.publishers});

        const dataBagi = [
          {id:1,keterangan:'Nilai Penjualan',amount:data?.transaction_amount},
          {id:2,keterangan:'Penerbit',amount:data?.amount_publisher},    
          {id:3,keterangan:'Aksaramaya',amount:data?.amount_am}
        ];
        reducerFunc('stateBagi',{dataSource:dataBagi});
      }
      reducerFunc('loading',false,'conventional');
    } catch (error) {
      console.log('error',error.response);
      reducerFunc('loading',false,'conventional');
    }
  } 

  React.useEffect(()=>{ 
    if(modal.data){
      fetchData(modal.data);
    }
  },[modal.data]);

  return (
    <MocoPopModal 
        {...approve.content} 
        width='80%'
        visible={modal.visible} 
        onClose={onCloseModal}>
        <LoadingSpin loading={dataReducer.loading}>
          <Row gutter={[16,16]}>
            <Col {...style.col_2}>
                <Row><Col span={8}>Nama iLibrary</Col><Col span={16}>: {(dataReducer?.state?.ilibrary_name) ? dataReducer?.state?.ilibrary_name : '-' } </Col></Row>
                <Row>
                    <Col span={8}>Tanggal</Col>
                    <Col span={16}>
                        : {(dataReducer?.state?.transaction_date) ? formDateDisplayValue(dataReducer?.state?.transaction_date,'DD-MM-YYYY')  : '-' } 
                    </Col>
                </Row>
                <Row><Col span={8}>Keterangan</Col><Col span={16}>: {(dataReducer?.state?.transaction_note) ? dataReducer?.state?.transaction_note : '-' } </Col></Row>
            </Col>
            <Col {...{...style.col_2, lg:8, xl:8}}>
                <Row><Col span={8}>Sumber Dana</Col><Col span={16}>: {(dataReducer?.state?.source_fund_name) ? dataReducer?.state?.source_fund_name : '-' } </Col></Row>
                <Row><Col span={8}>No SPK</Col><Col span={16}>: {(dataReducer?.state?.transaction_number) ? dataReducer?.state?.transaction_number : '-' } </Col></Row>
                <Row>
                    <Col span={8}>Nilai Penjualan (Rp)</Col>
                    <Col span={16}>
                        : {(dataReducer?.state?.transaction_amount) ? numberFormat(dataReducer?.state?.transaction_amount) : '-' } 
                    </Col>
                </Row>
            </Col>
            <Col span={4} style={{textAlign:'right',verticalAlign:'bottom'}}>
                <Button type='primary' style={{position:'absolute',bottom:'0',right:'0'}} onClick={handleProcess} loading={dataReducer.btnLoading}>Proses</Button>
            </Col>
          </Row>
          <hr/>
          <Row gutter={[16,16]}>
            <Col {...style.col_1}>
              <MocoTable 
                {...approve.list_left(props)}
                {...dataReducer.stateBagi}
                footer={(currentPageData) => {
                  if(currentPageData.length > 0 ){
                      const amount_publisher = (currentPageData[1].amount/currentPageData[0].amount) * 100;
                      const amount_am = (currentPageData[2].amount/currentPageData[0].amount) * 100;
                      return `Sistem bagi hasil : ${(amount_publisher) ? amount_publisher : '0'}:${amount_am ? amount_am : '0'}`;
                  }
                  return 'Sistem bagi hasil : - ';  
                }}
              />
            </Col>
            <Col {...style.col_1}>
              <MocoTable 
                {...approve.list_right(props)}
                {...dataReducer.statePenerbit}
                summary={(pageData) => {
                  let totalCopy = 0;
                  let totalNominal= 0;
                  
                  pageData.forEach(x => {
                      totalCopy += x.catalog_copy;
                      totalNominal += x.amount
                  });
                  return (
                    <Table.Summary.Row style={{textAlign:'right',fontWeight:'bold'}}>
                      <Table.Summary.Cell>
                        Total (Rp)
                      </Table.Summary.Cell>
                      <Table.Summary.Cell>
                        {numberFormat(totalCopy)}
                      </Table.Summary.Cell>
                      <Table.Summary.Cell>
                        {numberFormat(totalNominal)}
                      </Table.Summary.Cell>
                    </Table.Summary.Row>
                  )
                }}
              />
            </Col>
          </Row>
        </LoadingSpin>
    </MocoPopModal>
  )
};
