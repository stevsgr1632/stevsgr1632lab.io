import React from 'react';
import { Form } from 'antd';
import {DataIndexContext} from '../index';
import InputForm from 'components/ant/InputForm';
/* eslint-disable */
const FilterList = props => {
  const {dataReducer,reducerFunc} = React.useContext(DataIndexContext);
  const [form] = Form.useForm();
  
  const onChange = (name, value) => {
    switch (name) {
      case 'action':
        if (value === 'clear') {
          clearFilter();
        }
        break;
      case 'tanggal':
        reducerFunc('filter', {start_date: value[0], end_date: value[1] });
        break;
      default:
        if(!Array.isArray(name)) reducerFunc('filter',{ [name]: value });
        break;
    }
  };

  const clearFilter = () => {
    const clearData = { status_code: '', ilibrary_id: '', search: '', start_date: '', end_date: '' };
    form.setFieldsValue({...clearData,tanggal: ['','']});
    reducerFunc('filter', clearData);
  };

  return (<div>
    <InputForm {...props}
      form={form}
      onChange={onChange}
      defCol={{ md: 12, lg: 12, xl: 8 }}
      controls={[
        { type: 'rangepicker', name: 'tanggal', placeholder:['Tanggal Awal','Tanggal Akhir'], style:{width:'100%'} },
        { type: 'select', name: 'ilibrary_id',data: dataReducer?.data?.ilib_data, others:{ showSearch:true }},
        { type: 'select', name: 'status_code', data: dataReducer?.data?.status_code },
        { type: 'text', name: 'search', defcol: { md: 16 } },
        {
          type: 'action', name: 'action', defcol: { md: 8 }, useLabel: true, actions: [
            { text: 'Pencarian', type: 'primary', action: 'search' },
            { text: 'Reset', type: 'danger', action: 'clear' }
          ]
        },
      ]}>
      {props.children}
    </InputForm>
  </div>);
};

export default FilterList;