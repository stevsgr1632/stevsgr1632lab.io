import React from 'react';
import config from '../index.config';
import { Button, Col, Modal, Popconfirm, 
  Row, Space, Typography,Table } from "antd";
import oProvider from "providers/oprovider";
import MocoTable from "components/MocoTable";
import Notification from "common/notification";
import { numberFormat } from "utils/initialEndpoint";

export default (props) => {
  const { listReducer, listReducerFunc,refresh } = props;
  
  const handleAktivasi = async () =>{
    try {
      if(listReducer?.aktivasiData?.organizationGroupId){
        // console.log(organization_group_id.value);
        const response = await oProvider.insert('selection-planning-activation',{
          "transaction_id": listReducer?.aktivasiData?.id,
          "organization_group_id": listReducer.aktivasiData.organizationGroupId
        });
        if(response){
          Notification({
            type : 'success',
            response
          });
          listReducerFunc('aktivasiData', { visible : false });
          refresh();
        }
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <Modal 
      visible={listReducer.aktivasiData.visible}
      onCancel={() => listReducerFunc('aktivasiData', { visible : false })}
      footer={null}
      centered
      width="80vw"
      title={
      <Space direction="vertical">
        <Typography.Title level={3} style={{margin:0}}>Aktivasi Konten Digital</Typography.Title>
        <Typography.Text>Transaksi Pengadaan Buku</Typography.Text>
      </Space>}
    >
      <>
      <Row gutter={16}>
        <Col span={3}>
          <Typography.Text>Penerbit</Typography.Text>
        </Col>
        <Col span={21}>
          <Typography.Text>: {listReducer?.aktivasiData?.organization_group_name}</Typography.Text>
        </Col>
        <Col span={3}>
          <Typography.Text>Total Buku</Typography.Text>
        </Col>
        <Col span={21}>
          <Typography.Text>: {listReducer?.aktivasiData?.total_buku ?? 100}</Typography.Text>
        </Col>
        <Col span={3}>
          <Typography.Text>Copy</Typography.Text>
        </Col>
        <Col span={21}>
          <div id="proses_aktivasi"></div>
          <Space style={{display:'flex',justifyContent:'space-between'}}>
            <Typography.Text>: {listReducer?.aktivasiData?.total_copy ?? 100}</Typography.Text>
            <Popconfirm
              title="Apakah Anda yakin ingi mengaktivasi?"
              onConfirm={() => handleAktivasi()}
              onCancel={() => false}
              okText="Ya, Aktivasi"
              cancelText="Tidak"
              getPopupContainer={() => document.getElementById('proses_aktivasi')}
              style={{width:"100%"}}
              placement="topRight"
            >
              <Button type="primary" danger>Proses Aktivasi</Button>
            </Popconfirm>
          </Space>
        </Col>
      </Row>

      <MocoTable
        columns={config.configAktivasi(props).columns}
        {...listReducer?.aktivasiData?.table}
        summary={pageData => {
          let totalCopy = listReducer?.aktivasiData?.table?.dataSource?.reduce((total,num) => total + num.catalog_copy, 0);
          return(
          <Table.Summary.Row style={{textAlign:'right'}}>
            <Table.Summary.Cell colSpan={3}>
              <Typography.Text strong>Total</Typography.Text>
            </Table.Summary.Cell>
            <Table.Summary.Cell>
              <Typography.Text strong>{numberFormat(totalCopy)}</Typography.Text>
            </Table.Summary.Cell>
          </Table.Summary.Row>)
        }}
      />
      </>
    </Modal>
  );
};