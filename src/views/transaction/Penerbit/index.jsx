import React from 'react';
import ListForm from './FormList';
import config from './index.config';
import { connect } from 'react-redux';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import getMenuAkses from "utils/getMenuAkses";
import FilterList from './component/FilterList';
import { Breadcrumb, Content } from 'components';
import useCustomReducer from 'common/useCustomReducer';
/* eslint-disable */
export const MenuAksesContext = React.createContext([]);
export const DataIndexContext = React.createContext({});

const initialData = {
  data : {},
  menuAkses : [],
  filter : config.model
};

const TransPenerbit = props => {
  const {breadcrumb,model,schema, scope } = config;
  const { transaction : { penerbit : transPenerbit } } = pathName;
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  React.useEffect(() => {
    if(props.menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(transPenerbit.list);
      reducerFunc('menuAkses',menuAccessFromLocalStorage,'conventional');
    }    
  },[props.menuAccess]);

  React.useEffect(() => {
    const fetchData = async() => {
      try {
        const [
          {data : dataRes},
          {data : dataIlib},
          {data : orgGroupDpwn}
        ] = await Promise.all([
          oProvider.list('status-dropdown?process_name=CATALOG_PURCHASE'),
          oProvider.list('ilibrary-dropdown?showAll=1'),
          oProvider.list(`organization-group-dropdown`)
        ]);
        
        dataRes.unshift({ value: '', text: '-- Semua Status --' });
        dataIlib.unshift({ value: '', text: '-- Semua iLibrary --' });
        reducerFunc('data',{status_code:dataRes,ilib_data:dataIlib,orgGroupDpwn});
      } catch (error) {
        console.log(error?.message);
        return false;
      }
    }
    fetchData();
  },[]);
  
  const propFilter = {schema,scope,history:props.history};

  return(
  <DataIndexContext.Provider value={{
    dataReducer,
    reducerFunc,
    model
  }}>
    <MenuAksesContext.Provider value={dataReducer.menuAkses}>
    <Breadcrumb items={breadcrumb} />
    <FilterList {...propFilter} />
    <Content {...props}>
      <ListForm {...props}/>
    </Content>
    </MenuAksesContext.Provider>
  </DataIndexContext.Provider>
  );
};
const mapStateToProps = ({menuAccess}) => ({menuAccess});
export default connect(mapStateToProps)(TransPenerbit);