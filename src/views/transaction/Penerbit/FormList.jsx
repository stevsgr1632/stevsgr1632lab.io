import React from 'react';
import config from './index.config';
import PopModal from './component/PopModal';
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
import { Modal, Typography,Table } from 'antd';
import Notification from 'common/notification';
import { MessageContent } from 'common/message';
import { DeleteOutlined } from '@ant-design/icons';
import { numberFormat } from 'utils/initialEndpoint';
import ModalAktivasi from './component/ModalAktivasi';
import useCustomReducer from 'common/useCustomReducer';
import { DataIndexContext,MenuAksesContext } from './index';
/* eslint-disable */
const resource = 'selection-planning-run';
const { confirm } = Modal;
const { Text } = Typography;
const initialData = {
  url : '',
  modal : { visible:false },
  state : { loading : false },
  aktivasiData : {
    visible : false, 
    loading : false, 
    table : {
      dataSource : [],
      pagination : {
        hideOnSinglePage : true,
        pageSize: 10,
      }
    },
    organizationGroupId : '',
  },
};

const List = (props) => {
  const {dataReducer} = React.useContext(DataIndexContext);
  const menuAksesContext = React.useContext(MenuAksesContext);
  const [listReducer,listReducerFunc] = useCustomReducer(initialData);

  const modalDelete = (props) => {
    confirm({
      title: 'Hapus Pengumuman',
      icon: <DeleteOutlined />,
      content: <>
        <Text>Anda yakin ingin menghapus data ini (<b>{props.news_title}</b>) dan semua data terkait?</Text>
        <br />
        <Text type="danger">Tindakan ini tidak bisa dibatalkan</Text>
      </>,
      onOk: async () => {
        try {
          const response = await oProvider.delete(`${resource}?id=${props.id}`);
          if (!response) {
            Notification({
              response : {
                code : 'Data gagal dihapus',
                description : 'Silahkan muat ulang halaman dan coba lagi'
              },
              type: 'error'
            });
            return;
          }
          await refresh(listReducer.url);
          Notification({
            response : {
              code : 'Data berhasil dihapus',
              description : response.message
            },
            type: 'success'
          });
        } catch (error) {
          console.log(error?.message);
        }
      },
      onCancel() { },
    });
  }

  const refresh = async (url) => { 
    listReducerFunc('state',{loading : true});
    let paramsObject = {};
    Object.keys(dataReducer.filter).forEach(x => {
      if(dataReducer.filter[x]){
        paramsObject[x] = dataReducer.filter[x];
      }
    });
    url += (url ? '&' : '') + new URLSearchParams(paramsObject).toString();
    try {
      const { data } = await oProvider.list(`${resource}${url ? '?' : ''}${url}`);

      // add row 'id' in each array object for row expandable
      let dataKey = 1;
      const newData = data.map(function(el) {
        if(el.id){
          return el
        } else {
          var o = Object.assign({}, el);
          o.id = dataKey++;
          return o;
        }
      })
      listReducerFunc('state',{ 
        loading:false, 
        pagination: {
          hideOnSinglePage : true,
          pageSize: 10, 
          total: data.length 
        }, 
        dataSource: newData 
      });
    } catch (error) {
      listReducerFunc('state',{loading : false});
      console.log(error?.message);
      return false;
    }
  }

  const modalAktivasi = async (row) => {
    const key = 'modalAktivasi';
    MessageContent({ type :'loading', content:'Loading ... ',key, duration: 0 });
    let organization_group_id = dataReducer?.data?.orgGroupDpwn?.find(x => x.text.toLowerCase() === row.organization_group_name.toLowerCase());
    try {
      const { data } = await oProvider.list(`selection-planning-publisher-getone?organization_group_id=${organization_group_id.value}&transaction_id=${row.id}`);
      if(data){
        listReducerFunc('aktivasiData',{
          ...row, 
          visible : true, 
          organizationGroupId : organization_group_id.value, 
          table : {
            ...listReducer.aktivasiData.table, 
            dataSource : data.catalogs 
          } 
        });
        MessageContent({ type :'success', content: 'Selesai!', key, duration: 1 });
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  const modalApprove = (row) => {
    listReducerFunc('modal',{visible:true,data:row});
  }
  
  const onCloseModal = () => {
    listReducerFunc('modal',{visible:false,data:{} });
    refresh(listReducer.url);
  }

  const handleTableChange = (pagination, filters, sorter) => {
    let paramsObject = {};
    if (pagination) paramsObject['page'] = pagination?.current ?? 1;
    if (sorter && sorter.field) {
      paramsObject['sortBy'] = sorter.field;
      paramsObject['sortDir'] = sorter.order === 'ascend' ? 'asc' : 'desc';
    }
    if (dataReducer?.filter?.search) paramsObject['search'] = dataReducer?.filter?.search;
    listReducerFunc('url',new URLSearchParams(paramsObject).toString(),'conventional');
  }

  React.useEffect(() => {
    const loaded = listReducer.state.loading;
    listReducerFunc('state',{loading : true});
    const search1 = dataReducer?.filter?.search;
    const handler = setTimeout(() => {
      const search2 = dataReducer?.filter?.search;
      if (search1 === search2 && loaded) {
        refresh(listReducer.url);
      }
    }, 1500);

    return () => { clearTimeout(handler) };
  }, [dataReducer?.filter?.search]);

  React.useEffect(() => {
    refresh(listReducer.url);
  }, [listReducer.url,
    Object.values(dataReducer?.filter).join(),
  ]);
  const propsModalAktivasi = {listReducer,listReducerFunc,refresh};

  return (
    <>
      <MocoTable
        {...config.config(props, modalDelete, menuAksesContext,modalApprove)}
        {...listReducer.state}
        expandable={{
          expandedRowRender: record => {
            let totalHarga = record.details.reduce((total,num) => total + num.net_amount, 0);
            return (<MocoTable 
              {...config.configDetail(props,modalAktivasi)} 
              dataSource={record.details}
              pagination={{ 
                pageSize: 10,
                total: record.details.length,
                hideOnSinglePage : true,
              }}
              summary={pageData => {
                return(
                <Table.Summary.Row style={{textAlign:'right'}}>
                  <Table.Summary.Cell colSpan={4}>
                    <Typography.Text strong>Total (Rp)</Typography.Text>
                  </Table.Summary.Cell>
                  <Table.Summary.Cell>
                    <Typography.Text strong>{numberFormat(totalHarga)}</Typography.Text>
                  </Table.Summary.Cell>
                </Table.Summary.Row>)
              }}
            />)
          },
        }}
        onChange={handleTableChange}
      />
      <PopModal modal={listReducer.modal} onClose={onCloseModal}/>
      <ModalAktivasi {...propsModalAktivasi} />
    </>
  );
}

export default List;