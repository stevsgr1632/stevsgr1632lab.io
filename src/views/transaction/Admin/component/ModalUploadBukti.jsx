import React from 'react';
import {Modal, 
  Row, Col,Typography,Form,
  Progress,Input,Button,InputNumber 
} from 'antd';
import config from '../index.config';
import Message from 'common/message';
import {DataIndexContext} from '../index';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import {UploadOutlined} from '@ant-design/icons';
import InputText from 'components/ant/InputText';
import InputFile from 'components/ant/InputFile';
import InputDate from 'components/ant/InputDate';
import useCustomReducer from 'common/useCustomReducer';
import {numberFormat,formDateDisplayValue} from 'utils/initialEndpoint';
/* eslint-disable */
const initialData = {
  loading : false,
  dataRow : {},
  dataSubmit : {},
  progress : 0,
  previewUpload : {visible:false,title:'',src:''}
};

const ModalUploadBukti = props => {
  const [form] = Form.useForm();
  const {schema} = config;
  const [modalData,modalFunc] = useCustomReducer(initialData);
  const {dataReducer,reducerFunc} = React.useContext(DataIndexContext);

  const styleButton = {
    backgroundColor:'#4dbd74',color:'#f0f3f5',borderColor:'#4dbd74'
  };
  const handleOk = async e => {
    e.preventDefault();
    modalFunc('loading',true,'conventional');
    try {
      let resp = await oProvider.update('catalog-trans-upload',modalData?.dataSubmit);
      if(resp){
        modalFunc('loading',false,'conventional');
        Notification({response : resp});
        reducerFunc('visibility',{upload : false});
      }else{
        modalFunc('loading',false,'conventional');
      }
    } catch (error) {
      Message({type:'error',text:error.message});
      modalFunc('loading',false,'conventional');
      return false;
    }
  };
  
  const handleCancel = e => {
    e.preventDefault();
    modalFunc('dataSubmit',{});
    modalFunc('previewUpload',{visible:false,title:'',src:''});
    reducerFunc('visibility',{upload : false});
  };

  const handlePreviewUpload = () => {
    modalFunc('previewUpload',{visible:true});
  };

  const handleBlur = (name,value) => {
    modalFunc('dataSubmit',{ [name] : value });
  };
  const handleBlurTotAmount = value => {
    handleBlur('transfer_amount',value);
  };

  const handleUploadImage = async (name,file,data) => {
    console.log(name,file,data);
    handleBlur(name,data?.file_url_download);
    modalFunc('previewUpload',{visible:false,title:file.name,src:data.file_url_download});
  };

  React.useEffect(() => {
    if(dataReducer?.data?.row && dataReducer?.visibility?.upload){
      // detail data per row
      let allData = {...dataReducer?.data?.row,...dataReducer?.data?.parentData};
      const fetchData = async() => {
        try {
          let resp = await oProvider.list(`catalog-trans-getone?transaction_id=${allData.id}&organization_id=${allData.organization_id}`);
          modalFunc('dataSubmit',{ 
            transaction_id  :allData.id,
            organization_id :allData.organization_id,
            bank_id         :resp.bank_id,
            bank_name       :resp.bank_name,
            account_number  :resp.account_number,
            account_name    :resp.account_name,
           });
           modalFunc('dataRow',{ ...resp,transaction_amount:allData.transaction_amount });
        } catch (error) {
          console.log(error?.message);
          return false;
        }
      }
      fetchData();
    }
  },[dataReducer?.visibility?.upload]);

  return (
  <Modal
    title={<h3>Upload Bukti Transfer</h3>}
    visible={dataReducer?.visibility?.upload}
    onOk={handleOk}
    okButtonProps={{ loading : modalData.loading }}
    cancelButtonProps={{}}
    width="70vw"
    centered
    onCancel={handleCancel}
  >
    <Row gutter={24}>
      <Col span={4}>
        <Typography.Text>Penerbit</Typography.Text>
      </Col>
      <Col span={7}>
        <Typography.Text>: {modalData?.dataRow?.organization_name}</Typography.Text>
      </Col>
      <Col span={4}>
        <Typography.Text>Total Buku</Typography.Text>
      </Col>
      <Col>: {modalData?.dataRow?.number_of_copy}</Col>
    </Row>
    <Row gutter={24}>
      <Col span={4}>
        <Typography.Text>No. Transaksi</Typography.Text>
      </Col>
      <Col span={7}>
        <Typography.Text>: {modalData?.dataRow?.transaction_number}</Typography.Text>
      </Col>
      <Col span={4}>
        <Typography.Text>Harga</Typography.Text>
      </Col>
      <Col>: {numberFormat(modalData?.dataRow?.transaction_amount)}</Col>
    </Row>
    <Row gutter={24}>
      <Col span={4}>
        <Typography.Text>Tanggal</Typography.Text>
      </Col>
      <Col span={7}>
        <Typography.Text>: {formDateDisplayValue(modalData?.dataRow?.transaction_date,'DD-MM-YYYY')}</Typography.Text>
      </Col>
      <Col span={4}>
        <Typography.Text>Diskon</Typography.Text>
      </Col>
      <Col>: {numberFormat(modalData?.dataRow?.discount_amount)}</Col>
    </Row>
    <Row gutter={24}>
      <Col span={4}>
        <Typography.Text>e-Pustaka</Typography.Text>
      </Col>
      <Col span={7}>
        <Typography.Text>: {Array.isArray(modalData?.dataRow?.epustaka_name) ? modalData?.dataRow?.epustaka_name.join() : modalData?.dataRow?.epustaka_name}</Typography.Text>
      </Col>
      <Col span={4}>
        <Typography.Text style={{fontWeight:'bold'}}>Total Buku</Typography.Text>
      </Col>
      <Col>
        <strong>: {numberFormat(modalData?.dataRow?.transaction_amount - modalData?.dataRow?.discount_amount)}</strong>
      </Col>
    </Row>
    
    <hr/>
    
    <strong>Rekening</strong>
    <br/>
    <h6>{modalData?.dataRow?.bank_name}</h6>
    <h6>{modalData?.dataRow?.account_name} a/n {modalData?.dataRow?.account_number}</h6>
    <h6>{modalData?.dataRow?.organization_name}</h6>
    <br/>

    <Form form={form}>
      <Row gutter={24}>
        <Col span={8}>
          <Form.Item name="transfer_amount" label="Total Ditransfer" labelCol={{ span: 24 }} 
            rules={[
              { required: true, message: 'Silahkan isi total transfer' },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (value <= parseInt(modalData?.dataRow?.transaction_amount - modalData?.dataRow?.discount_amount)) {
                    return Promise.resolve();
                  }
                  return Promise.reject(`Total di transfer HARUS lebih kecil atau sama dengan total harga buku : Rp ${modalData?.dataRow?.transaction_amount - modalData?.dataRow?.discount_amount}`);
                },
              }),
            ]}
          >
            <InputNumber autoFocus min={0} style={{ width: '100%' }} onChange={handleBlurTotAmount} />
          </Form.Item>
        </Col>
        <Col span={8}>
          <div id="area"></div>
          <InputDate name="transfer_date" schema={schema} 
            onChange={handleBlur} 
            getPopupContainer={() => document.getElementById('area')} 
            style={{width:'100%'}} 
          />
        </Col>
        <Col span={8}>
          <InputText pattern="[a-zA-Z0-9]+" title="Silahkan input kombinasi nomor dan angka saja" name="transfer_number" schema={schema} onBlur={handleBlur} />
        </Col>
      </Row>

      <Row gutter={24}>
        <Col span={12}>
          <InputFile 
            name="transfer_bank_image"
            listType="picture" 
            accept=".png,.jpg,.jpeg" 
            schema={schema} 
            onChange={handleUploadImage}
            onProgress={({ percent }) => {
              if (percent === 100) {
                setTimeout(() => modalFunc('progress',0,'conventional'), 1000);
              }
              return modalFunc('progress',~~(percent,'conventional'));
            }}
            otherComponent={<Input type="text" placeholder="Upload Foto" readOnly value={modalData?.previewUpload?.title ? modalData?.previewUpload?.title : ''} />}
          >
            <Button style={styleButton}>
              <UploadOutlined /> Pilih Foto
            </Button>
          </InputFile>
          <small>Upload Foto : jpg, jpeg, png. Maks 2MB</small> 
          {modalData.progress > 0 ? <Progress percent={modalData?.progress} /> : null}
        </Col>
        <Col span={12} style={{cursor:'pointer'}}>
          {modalData.previewUpload?.title && <img src={modalData.previewUpload?.src} onClick={handlePreviewUpload} width="100%" alt="no-data" />}
        </Col>

      </Row>
    </Form>
    <Modal
      visible={modalData?.previewUpload?.visible}
      title={modalData?.previewUpload?.title}
      footer={null}
      centered
      onCancel={() => modalFunc('previewUpload',{visible:false})}
      >
      <img alt="no-picts" style={{ width: '100%' }} src={modalData?.previewUpload?.src} />
    </Modal>
  </Modal>
  );
};

export default ModalUploadBukti;