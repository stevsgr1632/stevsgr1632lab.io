import React from 'react';
import {Modal, Row,
  Col,Typography } from 'antd';
import {DataIndexContext} from '../index';
import oProvider from '../../../../providers/oprovider';
import { numberFormat,formDateDisplayValue } from "../../../../utils/initialEndpoint";
/* eslint-disable */
const ModalDetailBuku = () => {
  const {dataReducer,reducerFunc} = React.useContext(DataIndexContext);
  const [dataRow,setdataRow] = React.useState({});
  
  const handleCancel = e => {
    e.preventDefault();
    reducerFunc('visibility',{detail : false});
  };

  React.useEffect(() => {
    if(dataReducer.data.row && dataReducer.visibility.detail){
      // detail data per row
      let allData = {...dataReducer?.data?.row,...dataReducer?.data?.parentData};
      const fetchData = async() => {
        try {
          let resp = await oProvider.list(`catalog-trans-getone?transaction_id=${allData.id}&organization_id=${allData.organization_id}`);
          setdataRow(prev => ({...prev,...resp,transaction_amount:allData.transaction_amount}));
        } catch (error) {
          console.log(error?.message);
          return false;
        }
      }
      fetchData();
    }
  },[dataReducer.visibility.detail]);

  return (
  <Modal
    title={<h3>Detail Buku</h3>}
    visible={dataReducer?.visibility?.detail}
    width="70vw"
    centered
    footer={null}
    onCancel={handleCancel}
  >
    <Row gutter={24}>
      <Col span={4}>
        <Typography.Text>Penerbit</Typography.Text>
      </Col>
      <Col span={7}>
        <Typography.Text>: {dataRow.organization_name}</Typography.Text>
      </Col>
      <Col span={4}>
        <Typography.Text>Total Buku</Typography.Text>
      </Col>
      <Col>: {dataRow.number_of_copy}</Col>
    </Row>
    <Row gutter={24}>
      <Col span={4}>
        <Typography.Text>No. Transaksi</Typography.Text>
      </Col>
      <Col span={7}>
        <Typography.Text>: {dataRow.transaction_number}</Typography.Text>
      </Col>
      <Col span={4}>
        <Typography.Text>Harga</Typography.Text>
      </Col>
      <Col>: {numberFormat(dataRow.transaction_amount)}</Col>
    </Row>
    <Row gutter={24}>
      <Col span={4}>
        <Typography.Text>Tanggal</Typography.Text>
      </Col>
      <Col span={7}>
        <Typography.Text>: {formDateDisplayValue(dataRow.transaction_date,'DD-MM-YYYY')}</Typography.Text>
      </Col>
      <Col span={4}>
        <Typography.Text>Diskon</Typography.Text>
      </Col>
      <Col>: {numberFormat(dataRow.discount_amount)}</Col>
    </Row>
    <Row gutter={24}>
      <Col span={4}>
        <Typography.Text>e-Pustaka</Typography.Text>
      </Col>
      <Col span={7}>
        <Typography.Text>: {dataRow?.eputaka_name?.join() ?? '-'}</Typography.Text>
      </Col>
      <Col span={4}>
        <Typography.Text style={{fontWeight:'bold'}}>Total Buku</Typography.Text>
      </Col>
      <Col>
        <b>: {numberFormat(dataRow.transaction_amount - dataRow.discount_amount)}</b>
      </Col>
    </Row>
    <hr/>
    <p><strong>Dibayar kerekening</strong></p>
    <Row gutter={24}>
      <Col span={12}>
        <Typography.Text>{dataRow.bank_name}</Typography.Text>
      </Col>
      <Col span={4}>Tanggal</Col>
      <Col span={8}>
        <Typography.Text>: {formDateDisplayValue(dataRow.transaction_date,'DD MMMM YYYY')}</Typography.Text>
      </Col>
    </Row>
    <Row gutter={24}>
      <Col span={12}>
        <Typography.Text>{dataRow.account_name}</Typography.Text>
      </Col>
      <Col span={4}>No. Bukti</Col>
      <Col span={8}>
        <Typography.Text>: {dataRow.transaction_number}</Typography.Text>
      </Col>
    </Row>
    <Row gutter={24}>
      <Col span={12}>
        <Typography.Text>{dataRow.account_number}</Typography.Text>
      </Col>
      <Col span={4}>Jumlah</Col>
      <Col span={8}>
        <Typography.Text>: {numberFormat(dataRow.transaction_amount - dataRow.discount_amount)}</Typography.Text>
      </Col>
    </Row>
  </Modal>
  );
};

export default ModalDetailBuku;