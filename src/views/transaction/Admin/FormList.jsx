import React from 'react';
import config from './index.config';
import { DataIndexContext } from './index';
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
import Notification from 'common/notification';
import { DeleteOutlined } from '@ant-design/icons';
import { numberFormat } from 'utils/initialEndpoint';
import { Modal, Typography, Space, Table } from 'antd';
/* eslint-disable */

const resource = 'catalog-trans-inquiry-admin';
const { confirm } = Modal;
const { Text } = Typography;

const List = (props) => {
  const {dataReducer} = React.useContext(DataIndexContext);
  const [state, setState] = React.useState({loading:false});
  const [url, setUrl] = React.useState('');

  // Modal Delete
  const modalDelete = (props) => {
    confirm({
      title: 'Hapus Pengumuman',
      icon: <DeleteOutlined />,
      content: <React.Fragment>
        <Space direction="vertical">
          <Text>Anda yakin ingin menghapus data ini (<b>{props.news_title}</b>) dan semua data terkait?</Text>
          <Text type="danger">Tindakan ini tidak bisa dibatalkan</Text>
        </Space>
      </React.Fragment>,
      onOk: async () => {
        try {
          const response = await oProvider.delete(`${resource}?id=${props.id}`);
          if (response) {
            Notification({type: 'success', response : {
                message : 'Data berhasil dihapus',
                code    : response.message,
              }
            });
            refresh(url);
          }
        } catch (error) {
          Notification({type: 'error', response : {
              message : 'Data gagal dihapus',
              code    : 'Silahkan muat ulang halaman dan coba lagi',
            } 
          });
          console.log(error.message);
          return false;
        }
      },
      onCancel() { },
    });
  };

  const refresh = async (url) => { 
    setState(prev => ({...prev,loading:true}));
    let paramsObject = {};
    Object.keys(dataReducer.filter).map(x => {
      if(dataReducer.filter[x]){
        paramsObject[x] = dataReducer.filter[x];
      }
    });
    url += (url ? '&' : '') + new URLSearchParams(paramsObject).toString();
    try {
      const { data } = await oProvider.list(`${resource}${url ? '?' : ''}${url}`);
      setState({ loading:false, pagination: { pageSize: 10, total: data.length }, dataSource: data });
    } catch (error) {
      setState(prev => ({ ...prev, loading:false }));
      console.log(error?.message);
      return false;
    }
  }

  const handleTableChange = (pagination, filters, sorter) => {
    let paramsObject = {};
    if (pagination) paramsObject['page'] = pagination.current;
    if (sorter && sorter.field) {
      paramsObject['sortBy'] = sorter.field;
      paramsObject['sortDir'] = sorter.order === 'ascend' ? 'asc' : 'desc';
    }
    if (dataReducer?.filter?.search) paramsObject['search'] = dataReducer?.filter?.search;
    setUrl(new URLSearchParams(paramsObject).toString());
  };

  React.useEffect(() => {
    const loaded = state.loading;
    setState(prev => ({ ...prev, loading: true }));
    const search1 = dataReducer?.filter?.search;
    const handler = setTimeout(() => {
      const search2 = dataReducer?.filter?.search;
      if (search1 === search2 && loaded) {
        refresh(url);
      }
    }, 1500);

    return () => { clearTimeout(handler) };
  }, [dataReducer?.filter?.search]);

  React.useEffect(() => {
    refresh(url);
  }, [
    url,
    Object.values(dataReducer.filter).join(),
  ]);

  return (
    <MocoTable
      {...config.config(props, modalDelete, dataReducer.menuAkses)}
      {...state}
      expandable={{
        expandedRowRender: record => {
          let totalHarga = record.detail.reduce((total,num) => total + num.total_amount, 0);
          return (<MocoTable 
            {...config.configDetail(props,state.dataSource.find(x => x.id === record.id))} 
            dataSource={record.detail}
            pagination={{ 
              pageSize: 10,
              total: record.detail.length,
              hideOnSinglePage : true,
            }}
            summary={pageData => {
              return(
              <Table.Summary.Row style={{textAlign:'right'}}>
                <Table.Summary.Cell colSpan={5}>
                  <Typography.Text strong>Total (Rp)</Typography.Text>
                </Table.Summary.Cell>
                <Table.Summary.Cell>
                  <Typography.Text strong>{numberFormat(totalHarga)}</Typography.Text>
                </Table.Summary.Cell>
              </Table.Summary.Row>)
            }}
          />)
        },
      }}
      onChange={handleTableChange}
    />
    
  );
}

export default List;