import React from 'react';
import ListForm from './FormList';
import config from './index.config';
import { connect } from 'react-redux';
import pathName from 'routes/pathName';
import oprovider from 'providers/oprovider';
import oProvider from 'providers/oprovider';
import getMenuAkses from "utils/getMenuAkses";
import FilterList from './component/FilterList';
import { Breadcrumb, Content } from 'components';
import useCustomReducer from 'common/useCustomReducer';
import ModalDetailBuku from './component/ModalDetailBuku';
import ModalUploadBukti from './component/ModalUploadBukti';
/* eslint-disable */
export const DataIndexContext = React.createContext({});
const initialData = {
  data : {},
  menuAkses : [],
  filter : config.model,
  visibility : {upload : false,detail : false}
};
const TransAdmin = props => {
  const { transaction : { admin : transAdmin } } = pathName;
  const {breadcrumb,model,schema, scope } = config;
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  const modalUploadBukti = (...params) => {
    const [row,parentData] = params;
    reducerFunc('data',{row,parentData});
    reducerFunc('visibility',{upload : true});
  };
  
  const modalDetailBuku = (...params) => {
    const [row,parentData] = params;
    reducerFunc('data',{row,parentData});
    reducerFunc('visibility',{detail : true});
  };

  React.useEffect(() => {
    if(props.menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(transAdmin.list);
      reducerFunc('menuAkses',menuAccessFromLocalStorage,'conventional');
    }    
  },[props.menuAccess]);

  React.useEffect(() => {
    const fetchData = async() => {
      try {
        const [
          {data : statusList}, 
          {data : epustakaList}
        ] = await Promise.all([
          oProvider.list('status-dropdown?process_name=CATALOG_PURCHASE'),
          oprovider.list('epustaka-dropdown')
        ]);

        statusList.unshift({ value: '', text: '-- Semua Status --' });
        epustakaList.unshift({ value: '', text: '-- Semua ePustaka --' });
        reducerFunc('data',{statusList,epustakaList});
      } catch (error) {
        console.log(error?.message);
        return false;
      }
    }
    fetchData();
  },[]);
  
  const propReducer = {dataReducer,reducerFunc};
  const propFilter = {...propReducer,schema,scope,history:props.history};
  const propList = {modalUploadBukti,modalDetailBuku};

  return(
  <DataIndexContext.Provider value={{...propReducer}}>
    <Breadcrumb items={breadcrumb} />
    <FilterList {...propFilter} />
    <Content {...props}>
      <ListForm {...props} {...propList} filter={dataReducer.filter} model={model} />
    </Content>
    <ModalUploadBukti {...propReducer} />
    <ModalDetailBuku {...propReducer} />
  </DataIndexContext.Provider>
  );
};
const mapStateToProps = ({menuAccess}) => ({menuAccess});
export default connect(mapStateToProps)(TransAdmin);