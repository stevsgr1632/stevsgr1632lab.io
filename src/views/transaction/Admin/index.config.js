import React from 'react';
import { Button, Dropdown,
  Menu,Badge } from 'antd';
import pathName from 'routes/pathName';
import randomString from 'utils/genString';
import { getOneMenu } from 'utils/getMenuAkses';
import ActionTableDropdown from "components/ActionTableDropdown";
import { numberFormat,formDateDisplayValue } from 'utils/initialEndpoint';
/* eslint-disable */
const { initial, utility, transaction, baseUrl } = pathName;

const actionMenu = (row, params) => {
  const [props, modalDelete, menuAksesContext]= params;
  return (
    <Menu>
      <Menu.Item>
        { getOneMenu(menuAksesContext,'Sunting') && <>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => props.history.push(utility.announcement.edit(row.id))} text="Sunting" />
        </>}
      </Menu.Item>
      <Menu.Item>
        { getOneMenu(menuAksesContext,'Hapus') && <>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => modalDelete(row)} text="Hapus" />
        </>}
      </Menu.Item>
    </Menu>
  );
}
const actionMenuDetail = (row, params) => {
  const [props,parentData]= params;
  const {modalUploadBukti,modalDetailBuku}= props;
  
  return (
    <Menu>
      <Menu.Item>
        <Button type="link" onClick={() => modalUploadBukti(row,parentData)}>Upload Bukti</Button>
      </Menu.Item>
      <Menu.Item>        
        <Button type="link" onClick={() => modalDetailBuku(row,parentData)}>Detail</Button>
      </Menu.Item>
    </Menu>
  );
};

const statusColor = status => {
  let color = '';
  switch (status.toLowerCase()) {
    case 'aktivasi buku':
      color = '#ff751a';
      break;
    case 'aktivasi gagal':
      color = '#ff3333';
      break;
    case 'transfer berhasil':
      color = '#52c41a';
      break;
    default:
      color = '#108ee9';
      break;
  }
  return (
    <Badge color={color} text={status} key={randomString(5)}/>
  );
};

export default {
  scope: {
    title: "Transaksi Order Pemesanan",
    subtitle: "Lakukan pilah untuk memudahkan pencarian",
    breadcrumb: [
      { text: "Beranda", to: baseUrl },
      { text: "Transaksi", to: transaction.penerbit.list },
    ]
  },
  model: {
    search: '',
    start_date: '',
    end_date: '',
    status_code:'',
    epustaka_id:''
  },
  schema : {
    tanggal: { label: "Tanggal Order" },
    status_code: { label: "Status" },
    epustaka_id: { label: "ePustaka" },
    search: { label: "Pencarian" },
    transfer_amount: { label: "Total Ditransfer", rules : [
      { required: true, message: 'Silahkan isi total transfer' },
    ]},
    transfer_date: { label: "Tanggal", rules : [
      { required: true, message: 'Silahkan isi tanggal' },
    ]},
    transfer_number: { label: "No. Bukti", rules : [
      { required: true, message: 'Silahkan isi nomor bukti' },
    ]},
    transfer_bank_image: { label: "Bukti Transfer", rules : [
      { required: true, message: 'Silahkan upload bukti transfer' },
    ]},
  },
  breadcrumb: [
    { text: 'Beranda', to: initial },
    { text: 'Transaksi', to: transaction.penerbit.list },
  ],
  config: (...props) => {
    return {
      columns: [
        {
          title: 'Tanggal Order',
          width: 130,
          sorter: true,
          key: 'transaction_date',
          dataIndex: 'transaction_date',
          render: (x, row) => x ? formDateDisplayValue(x,'DD-MM-YYYY') : '-',
        },
        {
          title: 'Nomor Transaksi',
          key:'transaction_number',
          dataIndex : 'transaction_number',
          render: (x) => x,
        },
        {
          title: 'ePustaka',
          sorter: true,
          key : 'epustaka_name',
          dataIndex : 'epustaka_name',
          render: (x) => x,
        },
        {
          title: 'Metode bayar',
          width: 180,
          key: 'transaction_amount',
          dataIndex: 'transaction_amount',
          render : x => '-'
        },
        {
          title: 'Nominal (Rp)',
          width: 180,
          key: 'transaction_amount',
          dataIndex: 'transaction_amount',
          align: 'right',
          render : x => numberFormat(x)
        },
        {
          title: 'Status',
          width: 150,
          key: 'status_name',
          dataIndex: 'status_name',
          render: x => statusColor(x),
        },
        {
          title: 'Aksi',
          key: 'operation',
          width: 80,
          className: 'text-center',
          render: row => (
            <Dropdown overlay={actionMenu(row, props)}>
              <Button type="link" style={{ margin: 0 }} onClick={e => e.preventDefault()}>
                <i className="icon icon-options-vertical" />
              </Button>
            </Dropdown>
          ),
        },
      ]
    }
  },
  configDetail : (...props) => {
    return {
      columns : [
        {
          title: 'Logo',
          key : 'organization_logo',
          width : 150,
          dataIndex : 'organization_logo',
          render: (x) => x ? (<img src={x} alt="no-data" width="50%" />) : '',
        },
        {
          title: 'Penerbit',
          sorter: true,
          key : 'organization_name',
          dataIndex : 'organization_name',
          render: (x) => x,
        },
        {
          title: 'Jumlah',
          align: 'right',
          key : 'number_of_copy',
          dataIndex : 'number_of_copy',
          render: (x) => x ? numberFormat(x) : '',
        },
        {
          title: 'Harga (Rp)',
          align: 'right',
          key : 'catalog_price',
          dataIndex : 'catalog_price',
          render: (x) => x ? numberFormat(x) : '',
        },
        {
          title: 'Diskon (%)',
          align: 'right',
          key : 'discount_amount',
          dataIndex : 'discount_amount',
          render: (x) => x ? x : 0,
        },
        {
          title: 'Total (Rp)',
          align: 'right',
          key : 'total_amount',
          render: (row) => row.total_amount ? numberFormat(row.total_amount) : 0,
        },
        {
          title: 'Status',
          key : 'status_name',
          width: 100,
          dataIndex : 'status_name',
          render: x => x ? statusColor(x) : '',
        },
        {
          title: 'Aksi',
          key: 'operation',
          width: 80,
          className: 'text-center',
          render: (row) => (
            <Dropdown overlay={actionMenuDetail(row, props)}>
              <Button type="link" style={{ margin: 0 }} onClick={e => e.preventDefault()}>
                <i className="icon icon-options-vertical" />
              </Button>
            </Dropdown>
          ),
        },
      ]
    }
  }
}