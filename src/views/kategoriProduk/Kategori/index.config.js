import React from 'react';
import { Tag,Dropdown,
  Button,Menu } from 'antd';
import pathName from 'routes/pathName';
import randomString from 'utils/genString';
import {PlusOutlined} from '@ant-design/icons';
import { getOneMenu } from 'utils/getMenuAkses';
import { noImagePath } from "utils/initialEndpoint";
import ActionTableDropdown from "components/ActionTableDropdown";
/* eslint-disable */

const { initial, kategori: kategoriUrl } = pathName;

const actionMenu = (row, params) => {
  const [props, modalDelete, viewDetail ,menuAksesContext] = params;
  const sunting = () => {
    props.history.push(kategoriUrl.edit(`${row.id}`));
  };

  return (
    <Menu>
      <Menu.Item>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => viewDetail(row)} text="Pratinjau" />
      </Menu.Item>
      <Menu.Item>
        { getOneMenu(menuAksesContext,'Sunting') && <>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={sunting} text="Sunting" />
        </>}
      </Menu.Item>
      <Menu.Item>
        { getOneMenu(menuAksesContext,'Hapus') && <>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => modalDelete(row, props.history)} text="Hapus" />
        </>}
      </Menu.Item>
    </Menu>
  );
};

export default {
  list: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Kategori Produk' },
    ],
    content: {
      title: 'Pilah Kategori',
      subtitle: 'Lakukan pilah untuk memudahkan pencarian dan pembaruan',
      toolbars: [
        {
          text: '', type: 'primary', icon: <PlusOutlined/>,
          onClick: (e) => {
            e.history.push(kategoriUrl.add);
          }
        },
      ]
    },
    config: (...props) => {
      return {
        columns: [
          {
            title: 'Logo',
            width: 40,
            align: 'center',
            dataIndex: 'category_image',
            render: logo => (
              <div style={{ width: "4em" }}>
                <img src={logo || noImagePath} alt="no-data" style={{ width: "100%" }} />
              </div>
            )
          },
          {
            title: 'Nama',
            dataIndex: 'category_name',
            sorter: true
          }, {
            title: 'Tipe Kategori',
            dataIndex: 'content_media_name',
            width: 250
          }, {
            title: 'Turunan',
            dataIndex: 'parent_name',
            width: 250
          }, {
            title: 'Status',
            key: 'category_isactive',
            dataIndex: 'category_isactive',
            width: 130,
            render: status => {
              let color = status === 1 ? 'green' : 'volcano';
              let nameStatus = status === 1 ? 'Aktif' : 'Tidak Aktif';
              return (
                <Tag color={color} key={randomString(5)}>
                  {nameStatus.toUpperCase()}
                </Tag>
              );
            },
          },
          {
            title: 'Aksi',
            key: 'operation',
            fixed: 'right',
            width: 100,
            render: (row) => (
            <span>
              <Dropdown overlay={actionMenu(row, props)}>
                <Button
                  type="link"
                  style={{ margin: 0 }}
                  onClick={e => e.preventDefault()}
                >
                  <i className="icon icon-options-vertical" />
                </Button>
              </Dropdown>
            </span>
            ),
          },
        ]
      }
    }
  },
  add: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Kategori Produk', to: kategoriUrl.list },
      { text: 'Kategori Baru' },
    ],
    content: {
      title: 'Buat Kategori Baru',
    },
  },
  edit: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Kategori Produk', to: kategoriUrl.list },
      { text: 'Pembaruan Kategori' },
    ],
    content: {
      title: 'Pembaruan Kategori',
      subtitle: 'Ubah Data Sesuai Kebutuhan Dan Teliti Kembali Sebelum Menyelesaikan',
    },
  },
  schema: {
    category_parent_id: {
      label: 'Induk Kategori',
    },
    category_name: {
      label: 'Nama Kategori',
      rules: [
        { required: true, message: 'Silahkan input nama kategori' },
      ]
    },
    content_media_id: {
      label: 'Tipe Kategori',
      rules: [
        { required: true, message: 'Silahkan type kategori' },
      ]
    },
    category_image: {
      label: 'Logo',
      // rules: [
      //   { required: true, message: 'Silahkan input upload image' },
      // ]
    },
    category_isactive: {
      label: 'Status Aktif',
      rules: [
        { required: true, message: 'Silahkan pilih status' },
      ]
    },
  },
  model: {
    status: '',
    category_parent_id: '',
    content_media_id: '',
    search: ''
  },
  data: {
    status: [
      { value: '', text: '-- Semua Status --'},
      { value: 1, text: 'Aktif' },
      { value: 0, text: 'Tidak Aktif' },
    ]
  }
};