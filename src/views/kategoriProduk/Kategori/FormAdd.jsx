import React from 'react';
import FormControl from './Form';
import config from './index.config';
import Message from 'common/message';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import { Breadcrumb, Content } from 'components';
/* eslint-disable */

const { kategori: kategoriUrl } = pathName;
const resource = 'category';

const App = (props) => {
  const { schema } = config;
  const { breadcrumb, content } = config.add;

  const backToList = () => {
    props.history.push(kategoriUrl.list);
  };

  const handleOtherSubmit = async (e) => {
    let fileData = e?.category_image?.file_url_download;
    e = { ...e, category_image: fileData };
    if (!e.category_parent_id) {
      e = { ...e, category_parent_id: null };
    }
    console.log(e);
    try {
      // inserting to API
      const response = await oProvider.insert(`${resource}`,  e);
      Notification({ response });
      backToList();
    } catch (error) {
      Message({type:'error',text:'Data tidak valid, Silahkan cek kembali '});
      return false;
    }
  }

  return (<>
    <Breadcrumb items={breadcrumb} />
    <Content {...content}>
      <FormControl
        otherSubmit={handleOtherSubmit}
        onCancel={backToList}
        schema={schema}
        type="add"
      />
    </Content>
  </>);
}

export default App;