import React from 'react';
import axios from 'axios';
import { Col, Row,Form, 
Tag , Button,Modal } from 'antd';
import ListForm from './FormList';
import config from './index.config';
import storeRedux from 'redux/store';
import pathName from 'routes/pathName';
import randomString from "utils/genString";
import oProvider from 'providers/oprovider';
import {MessageContent} from "common/message";
import getMenuAkses from "utils/getMenuAkses";
import { Breadcrumb, Content } from 'components';
import { DownloadOutlined } from '@ant-design/icons';
import InputSearch from 'components/ant/InputSearch';
import InputSelect from 'components/ant/InputSelect';
import LocalStorageService from "utils/localStorageService";
import { initialEndpoint,noImagePath } from 'utils/initialEndpoint';
/* eslint-disable */

const url = `${process.env.REACT_APP_BASE_URL}/${initialEndpoint}`;

export const MenuAksesContext = React.createContext([]);

export default (props) => {
  const [form] = Form.useForm();
  const { kategori : kategoriPath } = pathName;
  const { list, data } = config;
  const [model, setModel] = React.useState(config.model);
  const [selectData,setselectData] = React.useState({});
  const [menuAkses, setmenuAkses] = React.useState([]);
  const [modalVisible, setModalVisible] = React.useState(false);
  const [previewState, setPreviewState ] = React.useState({});

  const handleChange = (name, value) => {
    setModel({ ...model, [name]: value });
  }
  
  const handleChangeSearch = (name, value) => {
    setModel({ ...model, [name]: value });
  }

  const viewDetail = async (params) => {
    try {
      const response = await oProvider.list(`category-get-one?id=${params.id}`);
      if(response){
        setPreviewState(response.data);
        setModalVisible(true);
      }
    } catch (error){
      console.log('error',error);
    }
  }

  const getHeaders = () => {
    const { attributes } = LocalStorageService.state_auth.getAccessToken();
    const headers = {
        'Accept': 'application/vnd.api+json',
        'Content-Type': 'application/vnd.api+json',
        'Access-Control-Allow-Origin': true
    }

    if (attributes && attributes.accessToken) {
        headers.Authorization = `Bearer ${attributes.accessToken}`;
    }

    return headers;
}

  const downloadExcel = async () => {
    try {
      let key = randomString(17);
      MessageContent({ type :'loading', content:'Mengunduh ... ',key, duration: 0 });

      const response = await axios({
        headers: getHeaders(),
        url: `${url}/categories-download-excel`,
        method: 'GET',
        responseType: 'blob', // important
    });

    if(response){
      MessageContent({ type :'success', content: 'Selesai!', key, duration: 1 });
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'Catalog-category.xls'); //or any other extension
      document.body.appendChild(link);
      link.click();
  }

    } catch (error) {
      if(error.response){
        const { data, status } = error.response;
        const errorResponse = {
          code : status,
          message : (data.message) ? data.message : data
        }
        Notification({response:errorResponse, type : 'error'});
      }
      return false;
    }
  }

  const fetchData = async () => {
    try {
      const [
        {data : categoryParent},
        {data : contentMedia}
      ] = await Promise.all([
        oProvider.list(`categories-parent-dropdown`),
        oProvider.list(`content-media-dropdown`)
      ])
      categoryParent.unshift({value: '', text: '-- Semua Turunan --'});
      contentMedia.unshift({value: '', text: '-- Semua Kategori --'});

      setselectData(prev => ({...prev, categoryParent, contentMedia}));
    } catch (error) {
      console.log('error : ', error);
    }
  }
  
  React.useEffect(() => {
    if(storeRedux.getState().menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(kategoriPath.list);
      setmenuAkses(menuAccessFromLocalStorage);
    }    

    fetchData();
  },[storeRedux.getState().menuAccess]);

  return (
    <React.Fragment>
      <MenuAksesContext.Provider value={menuAkses}>
        <Breadcrumb items={list.breadcrumb} />
        <Content {...{...props,...list.content, menuAkses}}>
          <Form form={form}>
            <Row gutter={8}>
              <Col span={8}>
                <InputSelect name="status" text="Status" placeholder="Pilih Status" data={data.status} onChange={handleChange} />
              </Col>
              <Col span={8}>
                <InputSelect name="category_parent_id" text="Turunan" placeholder="Pilih Turunan" data={selectData?.categoryParent} onChange={handleChange} others={{showSearch:true}}/>
              </Col>
              <Col span={8}>
                <InputSelect name="content_media_id" text="Tipe Kategori" placeholder="Pilih Tipe Kategori" data={selectData?.contentMedia} onChange={handleChange} others={{showSearch: true}} />
              </Col>
              <Col span={20}>
                <InputSearch name="search" text="Pencarian" placeholder="Pencarian berdasarkan nama" onSearch={handleChangeSearch} />
              </Col>
              <Col span={4}>
                <div style={{width:'100%',height:'100%'}}>
                  <Button type='primary' icon={<DownloadOutlined/>} style={{marginTop:'30px',background:'#4bba72'}} onClick={downloadExcel}>Download</Button>
                </div>
              </Col>
            </Row>
          </Form>
          <hr />
          <ListForm {...props} viewDetail={viewDetail} model={model} />
          <Modal title="Detail Kategori" visible={modalVisible} onOk={() => setModalVisible(false)} footer={null} onCancel={() => setModalVisible(false)}>
              <Row gutter={[32,32]}>
                  <Col span={8} style={{justifyContent:'center'}}>
                    <div style={{width:'100%',border:'1px solid #DEDEDE', borderRadius:'5px'}}>
                        <img alt='logo' src={(previewState.category_image) ? previewState.category_image : noImagePath } width='100%'/>
                    </div>
                  </Col>
                  <Col span={16} style={{padding:'10px'}}>
                      <div style={{fontWeight:'bold'}}> {(previewState.category_name) ? previewState.category_name : '-'}</div>
                      <div> Turunan : {(previewState.parent_name) ? previewState.parent_name : '-'}</div>
                      <div style={{marginBottom:'20px'}}> Tipe kategori : {(previewState.content_media_name) ? previewState.content_media_name : '-'}</div>
                      <div> Status : {(previewState.category_isactive) ? 
                          <Tag color="#87d068" style={{borderRadius:'10px'}}>
                            <strong>AKTIF</strong>
                          </Tag> 
                        : <Tag color="#D44141" style={{borderRadius:'10px'}}>
                            <strong>TIDAK AKTIF</strong>
                          </Tag> }
                      </div>
                        
                  </Col>
              </Row>
          </Modal>
        </Content>
      </MenuAksesContext.Provider>
    </React.Fragment>
  );
};