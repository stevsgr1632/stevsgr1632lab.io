import React from 'react';
import { Button,Form, 
  Row, Col,Modal } from 'antd';
import oProvider from 'providers/oprovider';
import LoadingSpin from 'components/LoadingSpin';
import InputText from 'components/ant/InputText';
import InputFile from 'components/ant/InputFile3';
import { noImagePath } from "utils/initialEndpoint";
import InputSelect from 'components/ant/InputSelect';
import InputSwitch from 'components/ant/InputSwitch';
import useCustomReducer from "common/useCustomReducer";
/* eslint-disable */

const initialData = {
  loading : false,
  selectData : {},
  dataSubmit : { category_isactive : 1 },
  imgPreview : { src: noImagePath, visible : false, title: '' }
};
const Edit = (props) => {
  const [form] = Form.useForm();
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);
  const { otherSubmit, type, onCancel, schema, paramsId = '' } = props;

  React.useEffect(() => {
    const fetchData = async () => {
      reducerFunc('loading',true,'conventional');
      try {
        const [
          { data : catParent},
          { data : catType}
        ] = await Promise.all([
          oProvider.list(`categories-dropdown`),
          oProvider.list(`content-media-dropdown`),
        ]);
        catType.unshift({text : '-- Pilih Jenis Kategori', value:''});
        catParent.unshift({text : '-- Pilih Kategori Induk', value:''});

        reducerFunc('selectData',{catParent,catType});
  
        if(type === 'edit'){
          const { data } = await oProvider.list(`category-get-one?id=${paramsId}`);
          reducerFunc('imgPreview',{src : data.category_image});
          reducerFunc('dataSubmit',data);
          form.setFieldsValue(data);
        } else {
          form.setFieldsValue({category_isactive : true});
        }
        reducerFunc('loading',false,'conventional');
      } catch (error) {
        reducerFunc('loading',false,'conventional');
        console.log(error?.message);
        return false;
      }
    }
    fetchData();
  },[]);

  const handleUploadFile = async (name,file,data) => {
    reducerFunc('imgPreview',{src : data.file_url_download, title : file.name});
    reducerFunc('dataSubmit',{ [name] : data});
  };

  const onChangeSwitch = (name,value,e) => {
    reducerFunc('dataSubmit',{ [name]: value ? 1 : 0});
  };

  const handleChangeForm = (name,value,e) => {
    switch (name) {
      case 'content_media_id':
        reducerFunc('dataSubmit',{ [name]:value, content_media_name : e?.children });
        break;
      case 'category_parent_id':
        reducerFunc('dataSubmit',{ [name]:value, parent_name : e?.children });
        break;
      default:
        reducerFunc('dataSubmit',{ [name]: value});
        break;
    }
  };

  const handleSubmit = e => {
    e.preventDefault();
    form.validateFields().then(value => {
      otherSubmit(dataReducer?.dataSubmit);
    }).catch(error => {
      console.log(error);
    });
  }

  const handlePreview = () => reducerFunc('imgPreview',{visible : true});
  const handleCancel = () => reducerFunc('imgPreview',{src : false});

  return (
    <LoadingSpin loading={dataReducer?.loading}>
      <>
      <Form form={form}>
        <Row gutter={16}>
          <Col lg={4} md={24}>
            <InputFile 
              name="category_image" 
              accept=".png,.jpeg,.jpg" 
              schema={schema} 
              onChange={handleUploadFile} 
              imgUrl={(dataReducer?.imgPreview.src) ? dataReducer?.imgPreview.src : ''} 
              handlePreview={handlePreview}
            />
          </Col>
          <Col lg={20} md={24}>
              <Row gutter={16}>
                <Col lg={12} md={24}>
                  <InputSelect name="category_parent_id" data={dataReducer?.selectData?.catParent} schema={schema} onChange={handleChangeForm} others={{showSearch:true}}/>
                </Col>
                <Col lg={12} md={24}>
                  <InputText name="category_name" schema={schema} onBlur={handleChangeForm} />
                </Col>
                <Col lg={12} md={24}>
                  <InputSelect name="content_media_id" data={dataReducer?.selectData?.catType} schema={schema} onChange={handleChangeForm} />
                </Col>
              </Row>
              <Row gutter={16}>
                <Col lg={12} md={24}>
                  <InputSwitch 
                    name="category_isactive" 
                    schema={schema} 
                    onChange={onChangeSwitch} />
                </Col>
              </Row>
          </Col>
        </Row>
        <hr />
        <Button 
          className="ml-1" 
          type="primary" 
          onClick={handleSubmit} 
          style={{float:'right'}}>
            {type === 'edit' ? 'Perbaharui' : 'Simpan'}
        </Button>
        <Button onClick={onCancel} style={{float:'right'}}>Batal</Button>
      </Form>
      
      <Modal
        visible={dataReducer?.imgPreview.visible}
        title={dataReducer?.imgPreview.title}
        footer={null}
        centered
        onCancel={handleCancel}
        >
        <img alt="example" style={{ width: '100%' }} src={dataReducer?.imgPreview.src} />
      </Modal>
      </>
    </LoadingSpin>
  );
}

export default Edit;