import React from 'react';
import { Modal } from 'antd';
import config from './index.config';
import queryString from "query-string";
import { MenuAksesContext } from './index';
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
import {DeleteOutlined} from '@ant-design/icons';
/* eslint-disable */

const resource = 'category';
const { confirm } = Modal;

const List = (props) => {
  const menuAksesContext = React.useContext(MenuAksesContext);
  const {model,viewDetail} = props;
  const [limit, setLimit] = React.useState(10);
  const [state, setState] = React.useState({
    loading : false,
    pagination: {
      defaultCurrent: 1,
      pageSize: 10,
      total: 0,
      showSizeChanger: true, 
    },
    dataSource: []
  });
  const [url, setUrl] = React.useState('');

  const modalDelete = (props) => {
  
    confirm({
      title: 'Do you want to delete these items?',
      icon: <DeleteOutlined/>,
      content: 'When clicked the OK button, this dialog will be closed',
      onOk() {
        return new Promise(async (resolve, reject) => {
          try {
            await oProvider.delete(`category?id=${props.id}`);
            refresh('',{status:'',search:''});
            // await oProvider.list(`${resource}?limit=5`);
            setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
          } catch (error) {
            console.log(error?.message);
          }
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {},
    });
  };
  
  const onShowSizeChange = (current, pageSize) => {
    setLimit(pageSize);
  };

  const refresh = async (url,model) => {
    setState({...state, loading:true});
    let paramsObj = {};
    paramsObj['limit'] = limit;

    Object.keys(model).forEach(x => {
      switch (x) {
        case 'search':
          paramsObj['category_name'] = model[x];
          break;
        case 'status':
          paramsObj['isactive'] = model[x];
          break;
        default:
          paramsObj[x] = model[x];
          break;
      }
    });

    try {
      let params = queryString.stringify(paramsObj,{skipEmptyString : true});
      const response = await oProvider.list(`${resource}?${params}${url}`);

      if(response){
        const { meta, data } = response;
        const pagination = {...state.pagination,
          current:meta.page, 
          pageSize: limit, 
          total: parseInt(meta.total), 
          onShowSizeChange:onShowSizeChange 
        };
        setState(prev => ({ ...prev, dataSource: [...data], pagination, loading:false }));
      }
    } catch (error) {
      setState(prev => ({...prev, loading:false}));
      console.log(error?.message);
      return false;
    }
  }

  const handleTableChange = (pagination, filters, sorter) => {
    let url = '';
    if (pagination) url += `&page=${pagination.current}`;
    if (sorter && sorter.field) {
      url += `&sortBy=${sorter.field}`;
      url += `&sortDir=${sorter.order === 'ascend' ? 'asc' : 'desc'}`;
    }
    
    setUrl(url);
  }

  React.useEffect(() => {
    refresh(url,model);
  }, [url,Object.values(model).join(),limit]);
  
  return (<>
    <MocoTable
      {...config.list.config(props,modalDelete,viewDetail,menuAksesContext)}
      {...state}
      onChange={handleTableChange}
    />
  </>);
}

export default List;