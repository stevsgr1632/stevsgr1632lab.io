import React from 'react';
import { Row, Col } from 'antd';
import oProvider from 'providers/oprovider';
import LoadingSpin from 'components/LoadingSpin';
import { noImagePath } from "utils/initialEndpoint";
import '../dashboardDinas/styles/dashboarddinas.scss';
import useCustomReducer from 'common/useCustomReducer';
import CardView from '../dashboardDinas/components/CardView';
import ListForm from '../dashboardDinas/components/ListForm';
import BarChartForm from '../dashboardDinas/components/BarChartForm';
import PieChartForm from '../dashboardDinas/components/epustakaContent';
import BarChartTraffic from '../dashboardDinas/components/BarChartTraffic';
import DashboardContent from '../dashboardDinas/components/DashboardContainer';
/* eslint-disable */

const images = {
  noImagePath,
  catalogQty: '/assets/dashboard/buku.png',
  epustaka:'/assets/dashboard/dipinjam.png',
  publisher:'/assets/dashboard/layanan.png',
  procurement:'/assets/dashboard/access.png',
  catalogCopy:'/assets/dashboard/salinan.png',
  membership:'/assets/dashboard/pemustaka.png',
};
const initialData = {
  loading : false,
  catalogStat : {},
  classList : [],
  barData : {},
  pieChart : [],
  trafficData : {},
  dinasData : {
    name:'-',
    logo:images.noImagePath
  } 
};

const defColCardView = {xs : 12, sm : 12, md : 8, lg : 4, xl : 4};
const defColCardChart = {xs : 24, sm : 24, md : 24, lg : 12, xl : 12};

export default (props) => {
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);
  
  const fetchData = async () => {
    reducerFunc('loading',true,'conventional');
    try {
      const [
        { data : dataStat },
        { data: profile},
        classStat,traffic,
        { chartProcurement : barChart , chartePustaka : pieChart }
      ] = await Promise.all([
        oProvider.list(`dashboard-catalog-instansi`),
        oProvider.list(`organization-group-profile`),
        oProvider.list(`dashboard-catalog-class-instansi`),
        oProvider.list(`dashboard-chart-catalog-instansi`),
        oProvider.list(`dashboard-chart-instansi`)
      ]);

      reducerFunc('classList', classStat,'conventional');
      reducerFunc('dinasData', {
        ...profile[0],
        name: profile[0].organization_group_name,
        logo: profile[0].organization_group_logo
      });
      reducerFunc('barData', barChart);
      reducerFunc('catalogStat', dataStat);
      reducerFunc('pieChart', pieChart.data,'conventional');
      reducerFunc('trafficData', traffic);
      reducerFunc('loading',false,'conventional');
    } catch (error) {
      console.log(error?.message);
      reducerFunc('loading',false,'conventional');
    }
  }

  React.useEffect(()=>{
    fetchData()
  },[]);
  
  return (
    <LoadingSpin loading={dataReducer?.loading}>
      <DashboardContent header={dataReducer?.dinasData}>
        <Row gutter={[20, 20]}>
          <Col {...defColCardView}>
            <CardView 
              title={(dataReducer.catalogStat.catalogQty) ? dataReducer.catalogStat.catalogQty.text : '-' } 
              value={(dataReducer.catalogStat.catalogQty) ? dataReducer.catalogStat.catalogQty.value : '0' }  
              image={images.catalogQty}
            />
          </Col>
          <Col {...defColCardView}>
            <CardView 
              title={(dataReducer.catalogStat.catalogCopy) ? dataReducer.catalogStat.catalogCopy.text : '-' } 
              value={(dataReducer.catalogStat.catalogCopy) ? dataReducer.catalogStat.catalogCopy.value: '0' } 
              image={images.catalogCopy}
            />
          </Col>
          <Col {...defColCardView}>
            <CardView 
              title={(dataReducer.catalogStat.membership) ? dataReducer.catalogStat.membership.text : '-' } 
              value={(dataReducer.catalogStat.membership) ? dataReducer.catalogStat.membership.value : '0' } 
              image={images.membership}
            />
          </Col>
          <Col {...defColCardView}>
            <CardView 
              title={(dataReducer.catalogStat.epustaka)? dataReducer.catalogStat.epustaka.text : '-' } 
              value={(dataReducer.catalogStat.epustaka) ? dataReducer.catalogStat.epustaka.value : '0' } 
              image={images.epustaka}/>
          </Col>
          <Col {...defColCardView}>
            <CardView 
              title={(dataReducer.catalogStat.publisher) ? dataReducer.catalogStat.publisher.text : '-'} 
              value={(dataReducer.catalogStat.publisher) ? dataReducer.catalogStat.publisher.value:'0'} 
              image={images.publisher}/>
          </Col>
          <Col {...defColCardView}>
            <CardView 
              title={(dataReducer.catalogStat.procurement) ? dataReducer.catalogStat.procurement.text : '-' } 
              value={(dataReducer.catalogStat.procurement) ? dataReducer.catalogStat.procurement.value : '0' } 
              image={images.procurement}/>
          </Col>
        </Row>
        <Row gutter={[16, 16]}>
          <Col {...defColCardChart}>
            <BarChartForm dataBar={dataReducer?.barData} {...props}/>
          </Col>
          <Col {...defColCardChart}>
            <BarChartTraffic dataBar={dataReducer?.trafficData}/>
          </Col>
        </Row>
        <Row gutter={[16, { xs: 8, sm: 16, md: 24, lg: 32 }]}>
          <Col {...defColCardChart}>
            <PieChartForm listData={dataReducer?.pieChart}/>   
          </Col>
          <Col {...defColCardChart}>
            <ListForm data={dataReducer?.classList} />
          </Col>
        </Row>
      </DashboardContent>
    </LoadingSpin>
  )
};