import React from 'react';
import { Container } from 'reactstrap';
import {Breadcrumb} from 'components';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import {Row,Col,Card,Avatar,List } from 'antd';
/* eslint-disable */

const {initial,dashboard} = pathName;
const {Meta} = Card;

const DashboardPublisher = props => {
  const [state,setState] = React.useState({});
  const [loading,setloading] = React.useState(true);
  const breadcrumb = [
    { text: 'Home', to: initial },
    { text: 'Dashboard', to: dashboard },
  ];

  const gridStyle = {
    width: '25%',
  };

  const generateColor = () => {
    return '#' +  Math.random().toString(16).substr(-6);
  }

  React.useEffect(() => {
    async function fetchData(){
      setloading(true);
      try {
        const [
          { data: dashboard },
          { data: organization },
          { data: kategori }] =await Promise.all([
            oProvider.list('organization-group-profile'),
            oProvider.list('dashboard-catalog-publisher'),
            oProvider.list('dash-category-publisher'),
          ]);
        setState({...state,organization, dashboard,kategori});
        setloading(false);
      } catch (error) {
        console.log(error?.message);
        setloading(false);
      }
      
    }
    fetchData();
    return () => {
      /**
       * Add cleanup code here
       */
    };
  },[]);

  React.useEffect(() => {
    console.log(state);
  },[state]);

  return (<>
  <Breadcrumb items={breadcrumb} />
  <Card style={{marginTop:'-2em'}} loading={loading}>
    <h3>Profile Perusahaan</h3>
    {!loading && state?.dashboard.map(x => {
      return (
        <Meta key={x?.id} style={{margin:0}}
          avatar={<Avatar style={{ backgroundColor: generateColor(), verticalAlign: 'middle' }}>{x.organization_group_name.substr(0,1)}</Avatar>}
          title={<>
            <h5>{x.organization_group_name}</h5>
            <h5>{x.organization_group_head}</h5>
            <h5>{x.organization_group_phone}</h5>
          </>}
        />
      )
    })}
  </Card>
  <br />
  <Row gutter={[0,24]} style={{padding:'0 1em 0 1em'}}>
    <Col span={17}>
      <Container>
        <Card title={<strong>Penerbit</strong>} loading={loading} bordered={false}>
          {!loading && state?.organization.map(x => {
            return(
            <Card.Grid key={x.id} style={gridStyle}>
              <h5>{x.publisher_name}</h5>
              <Meta
                avatar={
                  <Avatar src={x.publisher_logo} />
                }
                title={`${x.total} Judul`}
                description={<i>Detail</i>}
                style={{margin:0}}
              />
            </Card.Grid>);
          })}
        </Card>
      </Container>
    </Col>
    <Col span={7}>
      <Container>
        <Card title={<strong>Kategori</strong>} loading={loading} bordered={false}>
          <List
            style={{height:'35vh',overflowY:'scroll'}}
            itemLayout="horizontal"
            dataSource={state?.kategori}
            renderItem={item => (
              <List.Item>
                <List.Item.Meta
                  avatar={<Avatar style={{ backgroundColor: generateColor(), verticalAlign: 'middle' }}>{item.category_name.substr(0,1)}</Avatar>}
                  title={item.category_name}
                  description={<a href="https://aksaramaya.com">{item.total} Judul</a>}
                />
              </List.Item>
            )}
          />
        </Card>
      </Container>
    </Col>
  </Row>
  </>);
}

export default DashboardPublisher;