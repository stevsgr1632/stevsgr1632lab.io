import React from 'react';
import './morecss/frontpage.scss';
import pathName from 'routes/pathName';
import useCustomReducer from 'common/useCustomReducer';
import logo from 'layouts/moco/img/frontpage/logo_red.png';
import { PlusOutlined,BarsOutlined} from '@ant-design/icons';
import { Layout,Button,Modal, Row,Col,Card,Anchor} from 'antd';
import mccpplus1 from 'layouts/moco/img/frontpage/mccpplus1.svg';
import mccpplus2 from 'layouts/moco/img/frontpage/mccpplus2.svg';
import mccpplus3 from 'layouts/moco/img/frontpage/mccpplus3.svg';
import maintsvg from 'layouts/moco/img/frontpage/icon_cp2@1x.svg';
import backGround from 'layouts/moco/img/frontpage/banner@2x.png';
import Bitmap1 from 'layouts/moco/img/frontpage/mitra/Bitmap1.png';
import Bitmap2 from 'layouts/moco/img/frontpage/mitra/Bitmap2.png';
import Bitmap3 from 'layouts/moco/img/frontpage/mitra/Bitmap3.png';
import Bitmap4 from 'layouts/moco/img/frontpage/mitra/Bitmap4.png';
import Bitmap5 from 'layouts/moco/img/frontpage/mitra/Bitmap5.png';
import Bitmap6 from 'layouts/moco/img/frontpage/mitra/Bitmap6.png';
import contactsvg from 'layouts/moco/img/frontpage/icon_cp_1@1x.svg';
/* eslint-disable */

const { Footer } = Layout;
const { Link } = Anchor;
// extra={<img src={logo} alt="aksaramaya" />}
const colorPrimary1 = '#C92036';
const colorPrimary2 = '#f0f2f5';
const initialData = {
  modal : {visible:false, data:''},
  bgHeader : colorPrimary2,
  toggleNav : false,
  targetOffset : undefined
};
const dataGambar = {
  portfolio : [Bitmap1,Bitmap2,Bitmap3,Bitmap4,Bitmap5,Bitmap6],
  services : [
    {
      title : 'Unggah',
      subtitle : 'Our themes are updated regularly to keep them bug free!',
      src : mccpplus1,
    },
    {
      title : 'Atur',
      subtitle : 'Our themes are updated regularly to keep them bug free!',
      src : mccpplus2,
    },
    {
      title : 'Pantau',
      subtitle : 'Our themes are updated regularly to keep them bug free!',
      src : mccpplus3,
    },
  ]
};
const FrontPage = props => {
  const {history} = props;
  const {login,register} = pathName;
  const [stateReducer, reducerFunc] = useCustomReducer(initialData);

  const styleLayout = {
    background:`linear-gradient(rgba(242, 67, 67, 0.8) 0%, rgba(64, 1, 1, 0.82) 100%),url(${backGround})`,
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundAttachment: 'fixed',
  };
  const handleLoginPage = e => {
    e.preventDefault();
    history.push(login);
  };
  const handleToggle = e => {
    e.preventDefault();
    reducerFunc('toggleNav',!stateReducer?.toggleNav,'conventional');
  };
  window.addEventListener("resize", () => {
    let width = window.screen.width;
    if(width > 600) {
      reducerFunc('toggleNav',false,'conventional');
      if(document.querySelector('.ant-anchor')){
        document.querySelector('.ant-anchor').style.display = 'flex';
      }
    }
  });
  const handleSignUpPage = e => {
    e.preventDefault();
    history.push(register);
  };
  const onVisible = (params) => {
    reducerFunc('modal',{visible:true,data:params});
  };
  const onCancel = () => {
    reducerFunc('modal',{visible:false,data:''});
  };
  const handleChangeAnchor = idLink => {
    if(idLink !== 'header'){
      reducerFunc('bgHeader',colorPrimary2,'conventional');
    }else{
      reducerFunc('bgHeader','transparent','conventional');
    }
  };
  
  // Toggle menu bar
  React.useEffect(() => {
    let navbar = document.querySelector('.header__navbar');
    let antAnchor = document.querySelector('.ant-anchor');
    if(stateReducer.toggleNav){
      navbar.style.display = 'block';
      antAnchor.style.display = 'block';
      antAnchor.classList.add("toggleNav");
    }else{
      if(window.screen.width < 600) {
        antAnchor.style.display = 'none';
      }else{
        antAnchor.style.display = 'flex';
      }
      antAnchor.classList.remove("toggleNav");
    }
  },[stateReducer.toggleNav]); 
  //== End Toggle menu bar

  React.useEffect(() => {
    const data = window.innerHeight / 17;
    reducerFunc('targetOffset',data,'conventional');
  }, []);

  return(<>
    <div className="header__navigation" style={{backgroundColor:stateReducer.bgHeader}}>
      <figure>
        <a href="#header">
          <img src={logo} height={30} alt="logo-company" />
        </a>
      </figure>
      <div className="header__navbar">
        <Anchor affix={false} targetOffset={stateReducer?.targetOffset} onChange={() => handleChangeAnchor}>
          <Link href="#services" title="Services"/>
          <Link href="#about" title="About" />
          <Link href="#portfolio" title="Portfolio"/>
          <Link href="#contact" title="Contact"/>
          <Button className="header__navbar--signin" type="link" shape="round" onClick={handleLoginPage}>Masuk</Button>
          <Button className="header__navbar--signup" shape="round" icon={<PlusOutlined />} onClick={handleSignUpPage}>Daftar</Button>
        </Anchor>
      </div>
      <Button type="link" className="toggleMenuBar" onClick={handleToggle}><BarsOutlined /></Button>
    </div>
    
    <div id="header" className="header__master" style={styleLayout}>
      <div className="header__wrapper">
        <div className="header__content">
          <div className="header__content--1">
            <h1>LAYANAN PUBLIKASI KATALOG KONTEN TERPERCAYA</h1>
            <hr className="header__content--divider" />
          </div>
          <div className="header__content--2">
            <p>Dengan menggunakan Moco Content Catalogue Platform kami membantu Anda untuk melakukan integrasi konten ke arah digital! Hanya dalam hitungan menit tanpa banyak embel-embel.</p>
          </div>
        </div>
      </div>
    </div>
    
    <section id="services" className="services__master">
      <div className="services__wrapper">
        <div className="services__content">
          <div className="services__content--1">
            <h2 className="services__content--heading" style={{color:colorPrimary1}}>Langkah Mudah Sederhana</h2>
            <hr className="services__content--divider" />
          </div>
          <Row gutter={[24,24]}>
            {dataGambar.services.map((x,index) => (
              <Col key={index} xs={{ span: 24 }} md={{ span: 8 }}>
                <Card bordered={false}
                  cover={
                    <img
                      alt="example"
                      src={x.src}
                    />}
                  className="services__content--card"
                >
                  <h3 className="services__content--heading">{x.title}</h3>
                  <p>{x.subtitle}</p>
                </Card>
              </Col>
            ) )}
          </Row>
        </div>
      </div>
    </section>
    <section id="about" className="about__master">
      <div className="about__wrapper">
        <div className="about__content">
          <div className="about__content--1">
              <h2 className="about__content--heading">KAMI PAHAM DENGAN APA YANG ANDA BUTUHKAN!</h2>
              <hr className="about__content--divider" />
              <p>Tunggu Apalagi, Daftarkan diri Anda, masukan katalog dan lengkapi informasi hanya dalam hitungan menit!</p>
              <Button type="link" className="about__content--button" size="large" shape="round" onClick={handleSignUpPage}>Daftar Sekarang</Button>
          </div>
        </div>
      </div>
    </section>
    <section id="portfolio" className="portfolio__master">
      <div className="services__wrapper">
        <div className="services__content">
          <div className="services__content--1">
            <h2 className="about__content--heading" style={{color:colorPrimary1}}>Our Partner</h2>
            <hr className="services__content--divider" />
          </div>
        </div>
      </div>
      <div className="row">
        <div className="row__inner">
          {dataGambar.portfolio.map((x,index) => (
            <div key={index} className="tile" onClick={() => onVisible(x)}>
              <div className="tile__media">
                <img className="tile__img" src={x} alt={`Bitmap${index}`}  />
              </div>
              <div className="tile__details">
                <div className="tile__title">
                  
                </div>
              </div>
            </div>
          ) )}
        </div>
      </div>

    </section>
    <section id="contact" className="contact__master">
      <div className="contact__wrapper">
        <div className="contact__content">
          <div className="contact__content--1">
            <h2 className="contact__content--heading">MARI BERKENALAN</h2>
            <hr className="contact__content--divider" />
            <p>Siap untuk memulai projek bersama kami? Silahkan membuat panggilan atau mengirimkan email kepada kami dan secepat mungkin kami respon !</p>
            <Row gutter={[0,0]} style={{margin:'1em 0 0 0'}}>
              <Col span={12}>
                <figure>
                  <img className="contact__content--img" alt="example" src={contactsvg} />
                </figure>
                <h5 className="contact__content--telp">+6221 - 7235124</h5>
              </Col>
              <Col span={12}>
                <figure>
                  <img className="contact__content--img" alt="example" src={maintsvg} />
                </figure>
                <h5><a href="https://aksaramaya.com" className="contact__content--anchor">contact@aksaramaya.com</a></h5>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    </section>
    <Footer style={{backgroundColor:'#292424'}}>
      <center>
        <h6 style={{color:colorPrimary2,fontWeight:'bold'}}>Copyright © <a href="https://aksaramaya.com" style={{color:colorPrimary2}}>Aksaramaya</a> | All Right Reserved</h6>
      </center>
    </Footer>

    <Modal
      centered
      visible={stateReducer?.modal?.visible}
      onCancel={onCancel}
      footer={null}
      width="50vw"
    >
      <img src={stateReducer?.modal?.data} alt="example" width="100%"/>
    </Modal>
  </>);
}

export default FrontPage;