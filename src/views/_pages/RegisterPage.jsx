import React from 'react';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import ModalSK from './morecomponent/ModalSK';
import Notification from 'common/notification';
import { LockOutlined } from '@ant-design/icons';
import LoadingSpin from 'components/LoadingSpin';
import InputText from 'components/ant/InputText';
import {newDropDown} from 'providers/dropdownapi';
import InputSelect from 'components/ant/InputSelect';
import useCustomReducer from 'common/useCustomReducer';
import logo from 'layouts/moco/img/brand/logo_text.png';
import InputCheckbox from 'components/ant/InputCheckbox';
import {Layout,Form,Input,Alert,Row, Col,Button} from 'antd';
/* eslint-disable */

const { login,base } = pathName;
const initialData = {
  loading : false,
  visible : false,
  messageError : '',
  state : {},
  jenisComp : []
};
const RegisterPage = (props) => {
  const [form] = Form.useForm();
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  const handleSubmit = async (e) => {
    reducerFunc('loading',true,'conventional');
    try {
      let response = await oProvider.insertNoToken('registration',dataReducer?.state);
      if(response){
        Notification({
          response: {
            code : response.code, 
            message : response.message
          }, 
          type:'info', 
          placement:'topRight'
        });
        reducerFunc('loading',false,'conventional');
        props.history.push(login);
      }
    } catch (err) {
      if(err.response){
        reducerFunc('messageError',err.response.data?.message,'conventional');
        reducerFunc('loading',false,'conventional');
      }
      console.log(err.message);
    }
  };

  const handleChange = (name,value,e) => {
    reducerFunc('state',{ [name]: value });
  };
  const handleChangePass = (e) => {
    reducerFunc('state',{ password: e.target.value });
  };
  const handleCheckbox = (name,checked,e) => {
    reducerFunc('state',{ [name]: checked });
  };
  const handleClickLogo = (e) => {
    props.history.push(base);
  };

  const schema = {
    user_admin_email: { label: "Email Akun", rules :[
      { required: true, message: 'Silahkan isi email terlebih dahulu' },
    ]},
    user_admin_name: { label: "Nama Lengkap", rules :[
      { required: true, message: 'Silahkan isi nama akun terlebih dahulu' },
    ]},
    user_admin_phone: { label: "Nomor Telepon", rules :[
      { required: true, message: 'Silahkan isi nomor telepon terlebih dahulu' },
    ]},
    org_type_id: { label: "Jenis", rules :[
      { required: true, message: 'Silahkan pilih jenis perusahaan terlebih dahulu' },
    ]},
    organization_name: { label: "Nama Organisasi", rules :[
      { required: true, message: 'Silahkan isi nama perusahaan terlebih dahulu' },
    ]},
    checksk: { rules :[
      { required: true, message: 'Silahkan checklist terlebih dahulu' },
    ]},
  };

  const colorCode = {
    color1 : '#C92036',
    color2 : '#f0f2f5',
  };

  React.useEffect(() => {
    async function fetchData(){
      let resp = await newDropDown('organization-type-dropdown');
      reducerFunc('jenisComp',resp,'conventional');
    }
    fetchData();
  },[]);
  
  React.useEffect(() => {
    setTimeout(() => {
      reducerFunc('messageError','','conventional');
    },15000);
  },[dataReducer?.messageError]);

  return (
    <Layout>
      <div style={{display:'grid',gridTemplateColumns:'repeat(auto-fit,minmax(300px,1fr))',height:'100vh'}}>
        <div style={{backgroundColor:colorCode.color1,display:'grid',justifyItems:'center',alignContent:'center'}}>
          <figure onClick={handleClickLogo} style={{cursor:'pointer'}}>
            <img src={logo} alt="logo-company" />
          </figure>
          <center><h5 style={{color:colorCode.color2}}><b>Lengkapi data untuk mendaftar di Moco Catalog</b></h5></center>
        </div>
        <div style={{display:'grid'}}>
          <LoadingSpin loading={dataReducer?.loading}>
            <Form
              form={form}
              onFinish={handleSubmit}
            >
              <Row gutter={[16,0]} style={{padding:'3em'}}>
                <Col span={24}>
                  <div style={{height:'50px',margin:'5px 0px'}}>
                  {dataReducer?.messageError && (
                    <Alert type="error" message={dataReducer?.messageError} showIcon closable />
                  )}
                  </div>
                </Col>
                <Col span={24}>
                  <h5 style={{color:colorCode.color1}}>Data Organisasi</h5>
                </Col>
                <Col span={8}>
                  <InputSelect name="org_type_id" data={dataReducer?.jenisComp} schema={schema} onChange={handleChange} placeholder="Pilih Jenis Perusahaan" others={{showSearch:true}} />
                </Col>
                <Col span={16}>
                  <InputText name="organization_name" autoFocus schema={schema} onChange={handleChange} placeholder="Input Nama Perusahaan" />
                </Col>
                <hr/>
                <Col span={24}>
                  <h5 style={{color:colorCode.color1}}>Data Akun</h5>
                  {/* <span>Kami menghubungi kontak berikut untuk verifikasi pendaftaran.</span> */}
                </Col>
                <Col span={24}>
                  <InputText name="user_admin_name" schema={schema} onChange={handleChange} placeholder="Input Nama Akun" />
                  <InputText name="user_admin_email" type="email" schema={schema} onChange={handleChange} placeholder="Input Email Akun" />
                  <InputText name="user_admin_phone" title="081212341234" schema={schema} onChange={handleChange} placeholder="Input No. Telp. Akun" />
                  <Form.Item name="password" autoComplete="current-password" label='Password' labelCol={{ span: 24 }} style={{marginBottom:'0'}}>
                    <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$" title="contoh : Asd12312" onChange={handleChangePass} placeholder="Input password" />
                  </Form.Item>
                  <small>Terdiri atas min. 8 karakter, kombinasi angka, huruf besar dan huruf kecil</small>
                  <InputCheckbox name="checksk" schema={schema} onChange={handleCheckbox} >
                  <small>Dengan melakukan pendaftaran Anda setuju dengan <a href="#" onClick={() => reducerFunc('visible',true,'conventional')} style={{color:colorCode.color1}}>Syarat dan Kententuan</a> Aksaramaya.com</small>
                  </InputCheckbox>
                  <Button danger type="primary" htmlType="submit" block>Simpan dan Lanjutkan</Button>
                </Col>
              </Row>
            </Form>
          </LoadingSpin>
        </div>
      </div>
      <ModalSK visible={dataReducer?.visible} setvisible={reducerFunc} />
    </Layout>
  );

}

export default RegisterPage;