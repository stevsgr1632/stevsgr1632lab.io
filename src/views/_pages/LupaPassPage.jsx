import React from 'react';
import { Button, Modal, Form } from 'antd';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import InputText from 'components/ant/InputText';
import LoadingSpin from 'components/LoadingSpin';
import logo from 'layouts/moco/img/brand/logo_red.png';
/* eslint-disable */

const LupaPassPage = props => {
  const [form] = Form.useForm();
  const {visible,setvisible,schema} = props;
  const [state,setState] = React.useState({email:'',loading : false});

  const handleChange = (name,value,e) => {
    setState(prev => ({ ...prev, [name]: value }));
  };

  const handleSubmit = async e => {
    setState(prev => ({ ...prev, loading : true }));
    try {
      let response = await oProvider.insertNoToken('forgot-password',e);
      console.log(response);
      setState(prev => ({ ...prev, loading : false }));
      setvisible('visible',false,'conventional');
      Notification({
        response: {
          code : response.code, 
          message : 'Silahkan cek EMAIL untuk login dengan password baru'
        }, 
        type:'info', 
        placement:'topRight'
      });
    } catch (error) {
      console.log(error);
      setState(prev => ({ ...prev, loading : false }));
    }
  };

  return(<>
    <Form form={form}></Form>
    <Modal
      width="50vw"
      centered
      visible={visible}
      onCancel={() => setvisible('visible',false,'conventional')}
      footer={null}
    >
      <center>
        <img src={logo} alt="logo-company" width="50%" />
      </center>
      <br/>
      <h3 style={{color:'#C92036'}} className="lostpass__heading-3">LUPA PASSWORD ?</h3>
      <LoadingSpin loading={state.loading}>
        <Form form={form} onFinish={handleSubmit}>
          <InputText name="email" 
            autoFocus type="email" 
            schema={schema} 
            onChange={handleChange} 
            text={<><b>Masukkan Email Kantor yang Terdaftar</b></>} 
            placeholder="Input Email" 
          />
          <Button danger type="primary" htmlType="submit" block>Kirim Tautan Pengganti Password</Button>
        </Form>
      </LoadingSpin>
    </Modal>
  </>);
}

export default LupaPassPage;