import React from 'react';
import { Result } from 'antd';
import logo from 'layouts/moco/img/brand/logo_red.png';

// extra={<img src={logo} alt="aksaramaya" />}
const Initial = props => {
  return(<>
    <Result
      icon={<img src={logo} alt="moco-catalog" />}
      title="Halo, Selamat Datang !"
    />
  </>);
}

export default Initial;