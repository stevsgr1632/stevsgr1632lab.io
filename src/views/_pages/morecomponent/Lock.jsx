import React, { useEffect } from 'react';
/* eslint-disable */
const Lock = (props) => {
    const interval = 500;

    const generatePosition = () => {
        const x = Math.round((Math.random() * 100) - 10) + '%';
        const y = Math.round(Math.random() * 100) + '%';
        return [x,y];
      }
    const generateLocks = () => {
        const lock = document.createElement('div');
        const position = generatePosition();
        lock.innerHTML = '<div class="top"></div><div class="bottom"></div>';
        lock.style.top = position[0];
        lock.style.left = position[1];
        lock.classList = 'lock'// generated';
        const domComp = document.getElementById('lock-move-container');
        if(domComp){
            domComp.appendChild(lock);
        }
        lock.style.opacity = '1';
        lock.classList.add('generated');
        setTimeout(()=>{
          lock.parentElement.removeChild(lock);
        }, 2000);
    }

    useEffect(()=>{
        const moveInterval = setInterval(generateLocks,interval);
        generateLocks();
        return ()=>{
            clearInterval(moveInterval);
        }
    },[])
    return (
        <div id='lock-move-container'>
        </div>
    )
    
}

export default Lock;
