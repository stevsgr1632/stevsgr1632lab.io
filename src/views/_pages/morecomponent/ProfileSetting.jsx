import React from 'react';
import {Row, Col,Modal} from 'antd';
import schema from './schemasetting';
import { DataSettings } from "../SettingsPage";
import InputText from 'components/ant/InputText';
import InputFile from 'components/ant/InputFile3';
import InputSelect from 'components/ant/InputSelect';
import InputTextArea from 'components/ant/InputTextArea';
/* eslint-disable */
const ProfileCompany = () => {
  const {
    dataReducer : { dataSubmit, region, resources},
    handleChangeLogo,
    handleChangeInput,
    handleChangeCombo,
  } = React.useContext(DataSettings);
  const [previewVisible, setpreviewVisible] = React.useState(false);
  const [previewTitle, ] = React.useState('Preview');
  const handlePreview = () => setpreviewVisible(true);
  const handleCancel = () => setpreviewVisible(false);

  const choicesPajak = [
    {
      text : 'Ditanggung Penerbit',
      value :1
    },
    {
      text : 'Ditanggung MOCO',
      value :2
    }
  ];
  const choicesStatus = [
    { value: 1, text: 'Aktif' },
    { value: 0, text: 'Tidak Aktif' },
  ];

  return(<>
  <Row gutter={[16, 24]}>
    <Col span={4}>
      <InputFile name="organization_group_logo" accept=".png,.jpeg,.jpg" schema={schema} onChange={handleChangeLogo} imgUrl={(dataSubmit?.organization_group_logo) ? dataSubmit?.organization_group_logo : ''} handlePreview={handlePreview}/>    
    </Col>
    <Col lg={20} md={24}>
      <Row gutter={16}>
        <Col span={12}>
          <InputText name="organization_group_name" onBlur={handleChangeInput} schema={schema} />
        </Col>
        <Col span={12}>
          <InputText name="organization_group_email" type="email" onBlur={handleChangeInput} schema={schema} />
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={12}>
          <InputText name="organization_group_website" type="url" onBlur={handleChangeInput} schema={schema} />
        </Col>
        <Col span={6}>
          <InputSelect name="org_type_id" data={resources?.organizationType} onChange={handleChangeInput} schema={schema} others={{showSearch:true}} />
        </Col>
        <Col span={6}>
          <InputSelect name="organization_group_status" data={choicesStatus} onChange={handleChangeInput} schema={schema} />
        </Col>
      </Row>
      
      <Row gutter={16}>
        <Col span={12}>
          <InputText name="organization_group_phone" pattern="\(?(?:\+62|62|0)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}" onBlur={handleChangeInput} schema={schema} />
        </Col>
        <Col span={12}>
          <InputText name="organization_group_head" onBlur={handleChangeInput} schema={schema} />
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={12}>
          <InputSelect name="province_id" schema={schema} loading={region?.province?.loading} data={region?.province?.list} onChange={handleChangeCombo} />
        </Col>
        <Col span={12}>
          <InputSelect name="regency_id" schema={schema} loading={region?.city?.loading} data={region?.city?.list} onChange={handleChangeCombo} />
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={12}>
          <InputSelect name="district_id" schema={schema} loading={region?.district?.loading} data={region?.district?.list} onChange={handleChangeCombo} />
        </Col>
        <Col span={12}>
          <InputSelect name="village_id" schema={schema} loading={region?.village?.loading} data={region?.village?.list} onChange={handleChangeInput} />
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={24}>
          <label>Alamat Organisasi Group</label>
          <InputTextArea name="organization_group_address" onBlur={handleChangeInput} schema={schema} placeholder="Alamat Organisasi Group" />
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={24}>
          <label>Tentang Organisasi Group</label>
          <InputTextArea name="organization_group_about" onBlur={handleChangeInput} schema={schema} placeholder="Deskripsi Organisasi Group" />
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={12}>
          <InputSelect name="organization_group_ppn" data={choicesPajak} onChange={handleChangeInput} schema={schema} others={{showSearch:true}} />
        </Col>
        <Col span={12}>
          <InputText name="organization_group_npwp" pattern="[0-9]+" title="please enter number only" onBlur={handleChangeInput} schema={schema} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <InputTextArea name="organization_group_npwp_address" onBlur={handleChangeInput} schema={schema} placeholder="Alamat NPWP Organisasi Group" />
        </Col>
      </Row>
    </Col>
  </Row>
  <Modal
      visible={previewVisible}
      title={previewTitle}
      footer={null}
      centered
      onCancel={handleCancel}
    >
      <img alt="example" style={{ width: '100%' }} src={dataSubmit?.organization_group_logo} />
    </Modal>
  </>);
};

export default ProfileCompany;