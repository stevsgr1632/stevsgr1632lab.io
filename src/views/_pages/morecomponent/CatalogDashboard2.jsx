import React, { useState, useEffect } from 'react';
import axios from "axios";
import { Spin, Alert } from 'antd';
import {
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    ResponsiveContainer,
    ComposedChart,
    Legend,
    LabelList,
    Bar
} from 'recharts';
import oWebhook from 'providers/webhook';
/* eslint-disable */
const styles = {
    title: {
        marginTop: -10,
        marginBottom: 10,
        fontSize: 22,
    },
    content: {
        minHeight: 200
    }
}

const loadData = async (url) => {
    return oWebhook.get(url);
}

const formatXAxis = (tickItem) => {
    return tickItem.toString() + '%';
}

const Chart1 = ({ data, err }) => {
    if (data) {
        return (
            <div>
                <h5 className="text-center">Content Data Traffic - Repository</h5>
                <ResponsiveContainer height={400}>
                    <ComposedChart height={400} data={data} >
                        <CartesianGrid stroke='#bdc3c7' />
                        <XAxis dataKey="name" />
                        <YAxis yAxisId="left" dataKey="Total Judul" />
                        <YAxis yAxisId="right" dataKey="percent" tickFormatter={formatXAxis} orientation="right" />
                        <Legend />
                        <Bar yAxisId="left" dataKey="Total Judul" barSize={32} fill="#f39c12" >
                            <LabelList dataKey="Total Judul" position="top" style={{ fontSize: 12 }} />
                        </Bar>
                        <Bar yAxisId="left" dataKey="Pertumbuhan per Tahun" barSize={32} fill="#34495e">
                            <LabelList dataKey="Pertumbuhan per Tahun" position="top" style={{ fontSize: 12 }} />
                        </Bar>
                        <Line yAxisId="right" name="% Penambahan" dataKey="percent" fill="#f39c12">
                            <LabelList dataKey="percent" position="bottom" style={{ fontSize: 12 }} />
                        </Line>
                        {/* <LabelList dataKey="percent" position="top" style={{ fontSize: 17 }} /> */}
                    </ComposedChart>
                </ResponsiveContainer>
            </div>
        )
    }
    if (err) {
        return (
            <div>
                <Alert
                    message="Error when fetch data"
                    description={err.message}
                    type="error"
                />
            </div>
        )
    }

    return (
        <Spin tip="Loading...">
            <h5>Resume</h5>
            <p>Loading data resume...</p>
        </Spin>
    )
}

const App = () => {
    const [data, setData] = useState();
    // const [data2, setData2] = useState();
    const signal = React.useMemo(() => axios.CancelToken.source(), []);

    useEffect(() => {
        loadData('dashboard-chart')
            .then(({ data }) => {
                // console.log(data)

                let max = 0;
                const items = [];

                data.forEach(m => {
                    const item = {
                        name: m.year,
                        percent: parseInt(100 * (m.total / m.totalAll)),
                        'Total Judul': m.totalAll,
                        'Pertumbuhan per Tahun': m.total,
                        'Persentase': m.total / m.totalAll,
                    };
                    items.push(item);
                    if (max < m.totalAll) max = m.totalAll;
                });

                items.forEach(m => { m.Persentase = m.Persentase * max; });

                setData({ data: items });
            })
            .catch((err) => {
                setData({ err });
            });

        return () => signal.cancel();
    }, [])

    return (
        <div>
            <h3 style={styles.title}>Pertumbuhan Konten Masuk</h3>
            <div style={styles.content}>
                <Chart1 {...data} />
            </div>
        </div>
    );
}

export default App;