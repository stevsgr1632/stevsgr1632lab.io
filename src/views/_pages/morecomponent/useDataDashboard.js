import React from 'react';
import axios from "axios";
import oWebhook from 'providers/webhook';
/* eslint-disable */

export default (endpoint) => {
  const signal = React.useMemo(() => axios.CancelToken.source(), []);
  const [results, setResults] = React.useState({});

  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const { data = {} } = await oWebhook.get(endpoint);
        setResults({data});
      } catch (err) {
        setResults({err});
      }
    };
    fetchData();
    return () => signal.cancel();
  }, [endpoint]);

  return { results };

}; 