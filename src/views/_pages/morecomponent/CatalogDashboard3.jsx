import React from 'react';
import { Row, Col, Spin, Alert, List, Avatar } from 'antd';
import useDataDashboard from "./useDataDashboard";
/* eslint-disable */
const styles = {
  title: {
    marginTop: -10,
    marginBottom: 10,
    fontSize: 22
  },
  content: {
    minHeight: 200
  }
};

const Chart1 = ({ data, err }) => {
    if (data) {
        return (
            <div>
                <h5>Berdasarkan Judul</h5>
                <List dataSource={data} renderItem={item => (
                    <List.Item>
                        <List.Item.Meta
                            avatar={<Avatar 
                              style={{
                                backgroundColor: '#C92036',
                                verticalAlign: 'middle',
                              }}
                              size="large">{item.name.charAt(0).toUpperCase()}</Avatar>}
                            title={`${item.name}`}
                            description={`judul: ${item.catalog}, total: ${item.total}`}
                        />
                    </List.Item>
                )} />
            </div>
        )
    }
    if (err) {
        return (
            <div>
                <Alert
                    message="Error when fetch data"
                    description={err.message}
                    type="error"
                />
            </div>
        )
    }

    return (
        <Spin tip="Loading...">
            <h5>Berdasarkan Judul</h5>
            <p>Loading data...</p>
        </Spin>
    )
};

const Chart2 = ({ data, err }) => {
    if (data) {
        return (
            <div>
                <h5>Berdasarkan Penjualan</h5>
                <List dataSource={data} renderItem={item => (
                    <List.Item>
                        <List.Item.Meta
                            avatar={<Avatar size="large"></Avatar>}
                            title={`${item.name}`}
                            description={`terjual: ${item.count}, total: ${item.total}`}
                        />
                    </List.Item>
                )} />
            </div>
        )
    }
    if (err) {
        return (
            <div>
                <Alert
                    message="Error when fetch data"
                    description={err.message}
                    type="error"
                />
            </div>
        )
    }

    return (
        <Spin tip="Loading...">
            <h5>Berdasarkan Penjualan</h5>
            <p>Loading data...</p>
        </Spin>
    )
};

const App = () => {
    const { results: resultsdata1 } = useDataDashboard('dashboard-top5-publisher?type=catalog');
    const { results: resultsdata2 } = useDataDashboard('dashboard-top5-publisher?type=procurement');

    return (
        <div>
            <h3 style={styles.title}>Top 5 Penerbit</h3>
            <div style={styles.content}>
                <Row gutter={24}>
                    <Col span={12}>
                        <Chart1 {...resultsdata1} />
                    </Col>
                    <Col span={12}>
                        <Chart2 {...resultsdata2} />
                    </Col>
                </Row>
            </div>
        </div>
    );
}

export default App;