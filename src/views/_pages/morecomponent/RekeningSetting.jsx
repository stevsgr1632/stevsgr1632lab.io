import React from 'react';
import {Row, Col} from 'antd';
import schema from './schemasetting';
import { DataSettings } from "../SettingsPage";
import InputText from 'components/ant/InputText';
import InputSelect from 'components/ant/InputSelect';
/* eslint-disable */
const RekeningSetting = () => {
  const {
    dataReducer : { resources},
    handleChangeInput
  } = React.useContext(DataSettings);
return(<>
  <Row gutter={16}>
    <Col span={12}>
      <InputSelect name="bank_id" data={resources?.bankChoices} onChange={handleChangeInput} schema={schema} others={{showSearch:true}} />
    </Col>
    <Col span={12}>
      <InputText name="bank_account" pattern="[0-9]+" title="please enter number only" onBlur={handleChangeInput} schema={schema} />
    </Col>
  </Row>
  <Row gutter={16}>
    <Col span={24}>
      <InputText name="bank_account_name" onBlur={handleChangeInput} schema={schema} />
    </Col>
  </Row>
</>);
};

export default RekeningSetting;