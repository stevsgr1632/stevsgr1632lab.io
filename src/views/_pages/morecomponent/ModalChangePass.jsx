import React from 'react';
import oProvider from 'providers/oprovider';
import { LockOutlined } from '@ant-design/icons';
import LoadingSpin from 'components/LoadingSpin';
import {Modal,Button,Form,Input,Typography} from 'antd';
/* eslint-disable */
const { Text } = Typography;

const ModalChangePass = props => {
  const {visible,setvisible} =  props;
  const [form] = Form.useForm();
  const [state,setState] = React.useState({loading : false, password:'',confirmpass:'',});

  const handleCancel = e => {
    setvisible('visible',false,'conventional');
  }

  const handleSubmit = async e => {
    setState(prev => ({...prev, loading: true }));
    try {
      let response = await oProvider.insert('change-password',state);
      setState(prev => ({...prev, loading: false }));
      Notification({
        response: {
          code : response.code, 
          message : 'Silahkan cek EMAIL untuk login dengan password baru'
        }, 
        type:'info', 
        placement:'topRight'
      });
      setvisible('visible',false,'conventional');
    } catch (error) {
      console.log(error);
      setState(prev => ({...prev, loading: false }));
    }
  };

  const validationPass = (params,type) => {
    let lowerCaseLetters = /[a-z]/g;
    let upperCaseLetters = /[A-Z]/g;
    let numbers = /[0-9]/g;
    let lengthParams = 8;

    if(!params.match(lowerCaseLetters)){
      setState(prev => ({...prev,[type]:'setidaknya ada 1 karakter huruf kecil'}));
    }else if(!params.match(upperCaseLetters)){
      setState(prev => ({...prev,[type]:'setidaknya ada 1 karakter huruf BESAR'}));
    }else if(!params.match(numbers)){
      setState(prev => ({...prev,[type]:'setidaknya ada 1 karakter angka'}));
    }else if(!params.length >= lengthParams){
      setState(prev => ({...prev,[type]:'setidaknya minimal 8 karakter atau lebih'}));
    }else{
      setState(prev => ({...prev,[type]:''}));
    }
  }

  const handleChangePass = (e) => {
    if(e.target.name === 'newPassword'){
      validationPass(e.target.value,'password');
    }
    if(e.target.name === 'newPasswordConfirm'){
      validationPass(e.target.value,'confirmpass');
    }
    setState(prev => ({ ...prev, [e.target.name]: e.target.value }));
  };

  const schema = {
    oldPassword: { label: "Password Lama", rules :[
      { required: true, message: 'Silahkan isi password lama terlebih dahulu' },
    ]},
    newpassword: { label: "Password Baru", rules :[
      { required: true, message: 'Silahkan isi password baru terlebih dahulu' },
      ({ getFieldValue }) => ({
        validator(rule, value) {
          validationPass(value,'password');
          return Promise.resolve();
        },
      }),
    ]},
    newpasswordconfirm: { label: "Konfirmasi Password Baru", rules :[
      { required: true, message: 'Silahkan isi konfirmasi password terlebih dahulu' },
      ({ getFieldValue }) => ({
        validator(rule, value) {
          validationPass(value,'confirmpass');
          if (!value || getFieldValue('newpassword') === value) {
            return Promise.resolve();
          }
          return Promise.reject('Konfirmasi password harus sama dengan password baru');
        },
      }),
    ]},
  };

  return(<>
  <Modal 
    centered
    title={<><b>Ubah Password</b></>}
    visible={visible}
    onCancel={handleCancel}
    footer={[
      <Button type="primary" key="back" onClick={handleCancel}>
        Kembali
      </Button>,
    ]}
    width="60vw"
  >
    <LoadingSpin loading={state.loading}>
      <Form form={form} onFinish={handleSubmit}>
        <Form.Item 
          name="oldPassword" 
          autoComplete="current-password" 
          label={schema['oldPassword'].label} 
          rules={schema['oldPassword'].rules} 
          labelCol={{ span: 24 }}
          style={{margin:'0'}}
        >
          <Input type="password" name="oldPassword" prefix={<LockOutlined className="site-form-item-icon" />} onChange={handleChangePass} placeholder="Input password lama" />
        </Form.Item>
        <Form.Item 
          name="newpassword" 
          hasFeedback
          label={schema['newpassword'].label} 
          rules={schema['newpassword'].rules} 
          labelCol={{ span: 24 }}
          style={{margin:'.5em 0 0 0'}}
        >
          <Input.Password name="newPassword" prefix={<LockOutlined className="site-form-item-icon" />} placeholder="Input password baru" onChange={handleChangePass} />
        </Form.Item>
        <Text type="danger">{state.password && state.password}</Text>
        <Form.Item 
          name="newpasswordconfirm" 
          hasFeedback
          dependencies={['newpassword']}
          label={schema['newpasswordconfirm'].label} 
          rules={schema['newpasswordconfirm'].rules} 
          labelCol={{ span: 24 }}
          style={{margin:'.5em 0 0 0'}}
        >
          <Input.Password name="newPasswordConfirm" prefix={<LockOutlined className="site-form-item-icon" />} placeholder="Input lagi password baru" onChange={handleChangePass} />
        </Form.Item>
        <Text type="danger">{state.confirmpass && state.confirmpass}</Text>
        <hr/>
        <Button danger type="primary" htmlType="submit" block>Simpan</Button>
      </Form>
    </LoadingSpin>
  </Modal>
  </>);
}

export default ModalChangePass;