import React from 'react';
import {Modal,Button,Typography} from 'antd';
/* eslint-disable */
const { Paragraph, Text } = Typography;

const ModalSK = props => {
  const {visible,setvisible} = props;

  const handleCancel = e => {
    setvisible('visible',false,'conventional');
  }

  return(<>
  <Modal 
    centered
    title={<><h4>Syarat dan Kententuan</h4></>}
    visible={visible}
    onCancel={handleCancel}
    footer={[
      <Button key="back" onClick={handleCancel}>
        Kembali
      </Button>,
    ]}
    width="80vw"
  >
    <div style={{width:'100%',height:'55vh',overflowY:'scroll'}}>
      <Paragraph>Anda diwajibkan untuk membaca Syarat &amp; Ketentuan Penggunaan ini dengan seksama
    karena dapat berdampak secara hukum kepada hak dan kewajiban Anda.</Paragraph>
      <Paragraph>Syarat &amp; Ketentuan Penggunaan ini mengatur penggunaan dan akses Platform
    (didefinisikan di bawah) dan penggunaan Layanan (didefinisikan di bawah).</Paragraph>
      <Paragraph>
        <Text strong>Dengan mengakses Platform dan/atau menggunakan Layanan, maka Anda telah
      membaca, mengerti, memahami dan menyetujui untuk terikat dengan Syarat &amp;
      Ketentuan Penggunaan ini. Jika Anda tidak menyetujui Syarat &amp; Ketentuan
      Penggunaan ini, maka Anda tidak diperbolehkan untuk menggunakan Platform
      atau Layanan ini.</Text>
      </Paragraph>
      <Paragraph>Akses dan penggunaan password dan/atau area tertentu yang dilindungi pada Platform
      dan/atau penggunaan Layanan dibatasi semata-mata hanya bagi Pelanggan yang telah
      memiliki Akun. Perolehan akses tidak sah ke area Platform dan/atau Layanan ini, atau ke
      area informasi lain yang dilindungi, dengan cara apapun yang tanpa izin penggunaan
      khusus oleh Kami adalah tidak diperbolehkan. Pelanggaran terhadap ketentuan ini
      merupakan pelanggaran yang didasarkan pada hukum Indonesia dan/atau peraturan
      perundang-undangan yang berlaku dan Kami memiliki hak untuk mempertahankan hak-
      hak Kami atas hal tersebut dan mengajukan pergantian kerugian, mengambil tindakan
      hukum dan/atau melaporkan hal tersebut kepada pihak yang berwajib sesuai dengan
      hukum Indonesia dan/atau peraturan perundang-undangan yang berlaku.</Paragraph>
      <Paragraph>
        <Text strong>Jika Anda dalam hal ini bertindak untuk dan atas nama perusahaan anda: </Text>
        <Text>Anda
    harus memperoleh persetujuan-persetujuan yang diperlukan dari perusahaan anda dan
    pihak ketiga lainnya yang relevan sehubungan dengan penggunaan dan tanggung jawab
    atas (i) tindakan Anda; (ii) biaya yang terkait dengan penggunaan setiap Layanan atau
    pembelian produk; dan (iii) penerimaan dan kepatuhan Anda sesuai dengan Syarat &amp;
    Ketentuan Penggunaan ini. </Text><Text strong>Jika Anda tidak memiliki persetujuan-persetujuan
    tersebut, Anda harus berhenti menggunakan dan/atau mengakses Platform, serta
    berhenti menggunakan Layanan ini.</Text>
      </Paragraph>
      <Paragraph>
        <Text strong>Pasal 1</Text><br/>
        <Text strong>Ruang Lingkup</Text><br/>
        <Text>Dalam menjalankan kegiatan usaha Kami, Kami menyediakan platform sistem elektronik
    dalam bidang marketplace atas produk dan/atau layanan jasa pada Situs (sebagaimana
    didefinisikan dibawah ini) yang terkoneksi dengan internet, dengan ruang lingkup usaha
    mempertemukan Penerbit Kami dan Pelanggan untuk melaksanakan Layanan. Dalam
    Layanan ini baik Kami, Vendor Kami dan Pelanggan sepakat untuk saling mengikatkan
    diri berdasarkan Syarat &amp; Ketentuan Penggunaan ini untuk mempertemukan Vendor
    Kami dan Pelanggan guna melaksanakan Layanan, memenuhi Pesanan dan membayar
    Harga Tercatat, termasuk tapi tidak terbatas pada penyediaan fasilitas promosi dan
    sistem pembayaran elektronik (e-payment).</Text>
      </Paragraph>
    
    
      <Paragraph>
        <Text strong>Pasal 2</Text><br/>
        <Text strong>Definisi dan Interpretasi</Text>
        <ol>
          <li><Text strong>Definisi: </Text><Text>Kecuali ditentukan lain, istilah berikut akan memiliki arti sebagai berikut
    dalam Syarat &amp; Ketentuan Penggunaan ini:</Text><br/>
    <Text strong>”Akun”</Text> berarti akun elektronik yang dibuat di Platform dan merupakan suatu
    identitas atas diri Anda atau Pengguna. <br/>
    <Text strong>“Anda”</Text> atau <Text strong>“Pengguna”</Text> berarti setiap orang yang melakukan akses pada
    Platform. <br/>
    <Text strong>“Barang dan/atau Jasa”</Text> adalah produk (termasuk angsuran produk atau bagian
    dari pada itu) dan/atau layanan yang tersedia untuk dijual dan/atau</li>
        </ol>
        
      </Paragraph>
      <Paragraph>
        <Text strong>Pasal 3</Text><br/>
        <Text strong>Penggunaan/Akses atas Platform</Text>
        <ol>
          <li>
            <Text strong>Penggunaan Umum Layanan</Text><br/>
            <Text>Kami setuju untuk menyediakan Layanan kepada Vendor Kami dan Pelanggan
    sama halnya dengan Vendor Kami dan Pelanggan sepakat untuk menggunakan
    Layanan. Penyediaan dan penggunaan Layanan ini dilakukan dengan ketentuan,
    Vendor Kami dan Pelanggan dengan ini memberikan kuasa kepada Aksaramaya
    untuk:</Text>
            <ol style={{listStyleType: 'lower-latin'}}>
              <li>
                <Text>Mempertemukan, merekomendasikan, meneruskan dan/atau tidak
    meneruskan Pesanan antara Pelanggan dan Vendor Kami.</Text>
              </li>
              <li>
                <Text>Melakukan promosi Barang dan/atau Jasa antara Vendor Kami dan
    Pelanggan dan di dalam sistem elektronik lainnya baik yang terafiliasi
    maupun yang tidak terafiliasi dengan Aksaramaya</Text>
              </li>
              <li>
                <Text>Mengelola, memperbarui (update) sistem Layanan dan menentukan
    Syarat &amp; Ketentuan Penggunaan.</Text>
              </li>
            </ol>
          </li>
          <li>
            <Text strong>Perubahan Syarat &amp; Ketentuan Penggunaan</Text><br/>
            <Text>Dari waktu ke waktu, Kami berhak untuk melakukan perubahan atas Syarat &amp;
    Ketentuan Pengunaan (termasuk tetapi tidak terbatas pada pengurangan,
    penambahan, penghapusan, dan/atau tindakan-tindakan lainnya yang dianggap
    diperlukan), dengan menampilkannya pada Situs. Atas hal tersebut, Anda setuju
    bahwa perubahan tersebut akan berlaku efektif dan Anda akan tunduk atas
    setiap perubahan tersebut. Dalam hal Anda tidak setuju dengan perubahan
    tersebut, maka Anda harus menghentikan akses penggunakan Platform atau
    Layanan ini. Apabila Anda tetap mengakses Situs, menggunakan Platform atau
    Layanan ini, maka Anda akan dianggap tunduk dan setuju atas perubahan
    tersebut.</Text>
          </li>
          <li>
            <Text strong>Larangan</Text>
            <ol style={{listStyleType: 'lower-roman'}}>
              <li>
                <Text strong>Tindakan yang dilarang</Text><br/>
                <Text>Anda tidak boleh menyalahgunakan Platform ini dengan alasan apapun
    maupun untuk tujuan apapun. Anda, baik sengaja dan/atau tanpa
    disengaja, dilarang untuk dan karenanya mengikatkan diri bahwa Anda
    tidak akan (a) mencoba, melakukan atau mendukung suatu tindakan
    pelanggaran hukum atau tindakan lain yang bertentangan dengan
    ketentuan peraturan perundang-undangan yang berlaku (termasuk dan
    tidak terbatas pada transaksi yang bersifat pencucian uang (money
    laundering), pendanaan bagi kegiatan terorisme, transaksi narkotika dan
    obat-obatan terlarang dan/atau kegiatan lainnya yang melanggar hukum
    dan peraturan perundang-undangan); (b) mencoba, melakukan atau
    mendukung suatu tindak pidana; (c) mengirimkan, mengunggah,
    menyebarkan, menambahkan dan/atau mendistribusikan virus dan/atau
    perangkat lunak lainnya ke dalam atau melalui Platform dalam bentuk
    apapun termasuk namun tidak terbatas pada Trojan horse, worm,
    dan/atau logic bomb, (d) mengirimkan, mengunggah atau memasukkan
    materi dan/atau teknologi berbahaya dalam bentuk apapun ke dalam
    atau melalui Platform, (e) melakukan tindakan pelanggaran hak apapun
    atas pihak manapun termasuk hak atas kekayaan intelektual Kami atau
    pihak lainnya, (f) melakukan tindakan berpura-pura atau mengaku
    sebagai orang lain atau entitas lain atau pihak tertentu, memberikan
    keterangan, identitas atau informasi atau yang salah, tidak benar atau
    palsu, (g) dengan cara apapun melakukan tindakan, menuliskan atau
    menyebarkan hal-hal yang bersifat ofensif, sensitif, melecehkan, SARA,
    kebencian, melanggar kesusilaan atau menyebabkan gangguan pada
    ketertiban umum, (h) memasuki atau mendapatkan akses secara tidak
    sah dan/atau mengganggu atau mengacaukan sistem komputer atau
    jaringan yang menghubungkan atau terhubung dengan Layanan dan/atau
    Platform, (i) merusak dan/atau melakukan penyerangan cyber ke
    Platform dan/atau database Kami, (j) mengganggu Pengguna lain, (k)
    melanggar asusila, (l) melanggar hak kesopanan orang lain, (m)
    mengirim iklan atau materi promosi yang tidak diminta ke dalam Platform,
    (n) mencoba untuk mempengaruhi kinerja atau fungsi dari setiap fasilitas
    komputer atau akses terhadap seluruh Platform, dan/atau (o)
    mengganggu, melakukan intersepsi dan/atau tindakan-tindakan lain yang
    merugikan atau menyebabkan Platform dan/atau server Platform
    dan/atau hosting milik Platform menjadi tidak berjalan dengan baik.</Text>
              </li>
              <li>
                <Text strong>Konten yang dilarang</Text><br/>
                <Text>Vendor Kami sepakat tidak akan mengunggah dalam Platform setiap
    materi yang dilarang termasuk tetapi tidak terbatas pada (a) barang
    dan/atau jasa yang memuat konten negatif (antara lain pornografi,
    perjudian, kebencian, kengerian, melanggar hak kekayaan intelektual,
    memuat jasa peretasan (hacking / cracking) atau menyediakan akses
    tanpa hak atau melawan hukum atas sistem elektronik, narkoba dan zat
    adiktif sejenis lainnya, ketidakjujuran, kecurangan, mistis/takhayul,
    penipuan, pencucian uang, pemalsuan dokumen, skema piramida,
    perdagangan manusia (human trafficking) atau organ manusia, rokok,
    dan kebencian); (b) barang dan/atau jasa yang tidak memiliki perizinan
    untuk diperdagangkan (antara lain senjata, militer, peledak, obat-obatan,
    makanan dan/atau minuman tertentu (termasuk obat yang memerlukan
    resep dokter, tidak mempunyai izin edar, dan minuman beralkohol),
    tanaman dan/atau binatang yang dilindungi negara, alat dan/atau
    perangkat telekomunikasi yang dilarang dan/atau tidak tersertifikasi oleh
    Kementerian Komunikasi dan Informasi Republik Indonesia, bahan kimia
    yang beracun dan berbahaya, rumah subsidi pemerintah, jasa
    pernikahan siri, kesehatan yang tidak sesuai peraturan), dan (c)
    membahayakan sistem elektronik dalam Platform dan/atau merugikan
    Aksaramaya (seluruhnya disebut sebagai “Konten Yang Dilarang”).</Text>
              </li>
            </ol>
            <Text>Setiap pelanggaran atas Syarat dan Ketentuan Penggunaan ini dapat
    dikategorikan sebagai tindak pidana berdasarkan Kitab Undang-Undang Hukum
    Pidana, Undang-Undang Nomor 11 Tahun 2008 sebagaimana diubah oleh
    Undang-Undang No. 19 tahun 2016 tentang Informasi dan Transaksi Elektronik
    (ITE) termasuk peraturan penggantinya sebagaimana diubah dari waktu ke waktu
    dan/atau peraturan perundangan-undangan yang berlaku. Jika hal tersebut
    terjadi, Kami, tanpa pemberitahuan terlebih dahulu kepada Pengguna, berhak
    melakukan tindakan yang perlu atas pelanggaran tersebut termasuk melaporkan
    pelanggaran tersebut kepada pihak penegak hukum yang berwenang dan akan
    mengambil tindakan hukum sesuai dengan ketentuan hukum yang berlaku.</Text>
          </li>
          <li>
            <Text strong>Ketersediaan Platform dan Layanan</Text><br/>
            <Text>Kami berhak untuk memperbaiki, menghilangkan meningkatkan, menambah,
    memodifikasi, menghentikan sementara, menghentikan penyediaan, menghapus,
    baik secara sebagian maupun keseluruhan dari Platform atau Layanan, termasuk
    perubahan atas kebijakan Data Pribadi, tanpa berkewajiban untuk memberikan
    alasan &amp; pemberitahuan sebelumnya kepada Anda. Kami tidak bertanggung
    jawab jika peningkatan, modifikasi, suspensi atau penghapusan tersebut
    mencegah Anda dalam mengakses Platform atau bagian dari Layanan lainnya.</Text>
          </li>
          <li>
            <Text strong>Lingkup Kerjasama</Text><br/>
            <Text>Vendor Kami dan Pelanggan sepakat, bahwa masing-masing hanya
    menawarkan dan menerima Pesanan terbatas pada ruang lingkup Barang
    dan/atau Jasa yang terdapat dalam Platform.</Text>
          </li>
          <li>
            <Text strong>Hak atas Platform</Text><br/>
            <Text>Setiap saat, Anda setuju dan memberikan kewenangan kepada Kami, sehingga
    Kami berhak dan tidak wajib untuk:</Text>
            <ol style={{listStyleType: 'lower-latin'}}>
              <li><Text>Memantau, menyaring, menyimpan atau mengatur setiap kegiatan, isi
    atau materi pada Platform dan/atau melalui Layanan. Atas kebijakan
    Kami sendiri, Kami dapat menyelidiki setiap pelanggaran terhadap Syarat
    &amp; Ketentuan Penggunaan pada Platform atau Layanan ini dan dapat
    mengambil tindakan apapun yang dianggap sesuai atau tepat untuk
    dapat dilakukan;</Text></li>
              <li><Text>Mencegah atau membatasi akses Pelanggan dalam mengakses Platform
    dan/atau jasa lainnya;</Text></li>
              <li><Text>Melakukan perekaman serta melaporkan kegiatan yang dicurigai sebagai
    pelanggaran terhadap hukum yang berlaku, undang-undang atau
    peraturan yang dikeluarkan oleh pihak yang berwenang, serta bekerja
    sama dengan pihak berwenang yang bersangkutan untuk memberikan
    informasi maupun tindakan lainnya yang dianggap diperlukan dalam
    menindaklanjuti laporan tersebut; dan/atau</Text></li>
              <li><Text>Meminta informasi dan data yang diperlukan dari Anda sehubungan
    dengan penggunaan Layanan dan/atau akses Platform setiap saat dan
    sebagai pelaksanaan hak Kami apabila (i) Anda tidak memberikan dan
    mengungkapkan informasi atau data tersebut; (ii) Anda memberikan
    informasi yang tidak akurat, menyesatkan, penipuan data dan/atau
    informasi; atau (iii) Kami memiliki alasan dan bukti yang cukup untuk
    mencurigai Anda bahwa Anda telah menyediakan informasi yang tidak
    akurat, menyesatkan maupun penipuan data dan/atau informasi.</Text></li>
            </ol>
          </li>
        </ol>
      </Paragraph>
      <Paragraph>
        <Text strong>Pasal 4</Text><br/>
        <Text strong>Penggunaan Data Pribadi</Text><br/>
        <Text>Panduan mengenai Penggunaan Data Pribadi diatur dan tunduk pada segala ketentuan
    sebagaimana tercantum pada ketentuan Kebijakan Privasi di Platform.</Text>
      </Paragraph>
      <Paragraph>
        <Text strong>Pasal 5</Text><br/>
        <Text strong>Ketentuan Umum</Text><br/>
        <ol style={{listStyleType:'decimal'}}>
          <li>
            <Text strong>Hak kumulatif dan penggantian</Text><br/>
            <Text>Kecuali ditentukan lain dalam Syarat &amp; Ketentuan Penggunaan ini, segala pasal
    yang berkaitan dengan Syarat &amp; Ketentuan Penggunaan ini merupakan hak Kami
    yang bersifat kumulatif dan merupakan hak penggantian berdasarkan hukum
    atau keadilan. Tidak ada ketentuan yang akan menghalangi, membatasi atau
    mencegah Kami untuk mempertahankan dan meminta perlindungan atas hak
    yang Kami miliki dan melakukan upaya hukum atau upaya penggantian atas hal
    tersebut berdasarkan peraturan perundang-undangan yang berlaku.</Text>
          </li>
          <li>
            <Text strong>Tidak ada Pengesampingan</Text><br/>
            <Text>Kegagalan Kami untuk menerapkan Syarat &amp; Ketentuan Penggunaan tidak dapat
    diartikan sebagai dapat diabaikannya sebagian maupun seluruh ketentuan ini,
    dan kegagalan tersebut tidak akan mempengaruhi hak Kami selanjutnya untuk
    menerapkan Syarat &amp; Ketentuan Penggunaan yang bersangkutan. Kami akan
    tetap berhak mempertahankan hak dan meminta penggantian atau melakukan
    upaya hukum dalam setiap kondisi dimana Anda melanggar Syarat &amp; Ketentuan
    Penggunaan ini.</Text>
          </li>
          <li>
            <Text strong>Pemisahan</Text><br/>
            <Text>Apabila di kemudian hari terdapat ketentuan pada Syarat &amp; Ketentuan
    Penggunaan ini yang menjadi ilegal, tidak sah atau tidak dapat diterapkan dalam
    hal apapun, maka legalitas, validitas dan berlakunya ketentuan lainnya dalam
    Syarat &amp;Ketentuan Penggunaan ini tidak akan terpengaruh atau berkurang
    karenanya, dan akan terus berlaku.</Text>
          </li>
          <li>
            <Text strong>Hak Pihak Ketiga</Text><br/>
            <Text>Seseorang atau pihak yang bukan merupakan pihak yang tunduk pada Syarat &amp;
    Ketentuan Penggunaan ini, tidak memiliki hak dan kewajiban apapun
    berdasarkan hukum dan yurisdiksi yang berlaku untuk menjalankan ketentuan
    dari Syarat &amp;Ketentuan Penggunaan ini. Untuk menghindari keraguan, tidak ada
    ketentuan dalam Pasal ini yang akan mempengaruhi hak-hak dari setiap
    pengalihan yang diizinkan atas Syarat &amp; Ketentuan Penggunaan ini.</Text>
          </li>
          <li>
            <Text strong>Hukum yang Berlaku</Text><br/>
            <Text>Penggunaan Platform dan/atau Layanan dan Syarat &amp; Ketentuan Penggunaan ini
    akan diatur oleh dan ditafsirkan sesuai dengan hukum Republik Indonesia serta
    yurisdiksi eksklusif pengadilan Jakarta Selatan.</Text>
          </li>
          <li>
            <Text strong>Bantuan Hukum</Text><br/>
            <Text>Kami dapat mencari atau menggunakan bantuan hukum dengan segera
    berdasarkan itikad baik untuk menangani dan menyelesaikan segala
    pelanggaran atau tidak dipatuhinya Syarat &amp; Ketentuan Pengunaan ini
    sedemikian rupa sehingga perintah penahanan sementara atau langsung ganti-
    rugi lainnya adalah satu-satunya yang sesuai atau memadai.</Text>
          </li>
          <li>
            <Text strong>Koreksi kesalahan</Text><br/>
            <Text>Setiap kesalahan ketik, administrasi atau kesalahan lainnya atau kelalaian dalam
    penerimaan, faktur atau dokumen lainnya yang terjadi pada sisi Kami akan Kami
    perbaiki berdasarkan kebijakan yang Kami miliki.</Text>
          </li>
          <li>
            <Text strong>Mata uang</Text><br/>
            <Text>Mata uang yang digunakan untuk segala hal yang berkaitan dengan Syarat &amp;
    Ketentuan Penggunaan termasuk transaksi di dalam Platform adalah mata uang
    Rupiah yang merupakan mata uang Negara Republik Indonesia.</Text>
          </li>
          <li>
            <Text strong>Keseluruhan</Text><br/>
            <Text>Kecuali ditentukan lain oleh Syarat &amp; Ketentuan Penjual dan/atau konfirmasi
    Pesanan dari Kami, Syarat &amp; Ketentuan Penggunaan harus merupakan
    keseluruhan perjanjian antara Kami dengan Anda dan berkaitan dengan materi
    pokok dalam perjanjian dan menggantikan secara penuh seluruh pemahaman,
    komunikasi dan perjanjian sebelumnya sehubungan dengan materi pokok dalam
    perjanjian.</Text>
          </li>
          <li>
            <Text strong>Mengikat dan konklusif</Text><br/>
            <Text>Anda mengakui dan menyetujui bahwa setiap catatan (termasuk catatan dari
    setiap percakapan telepon terkait dengan Layanan jika ada) dikelola oleh Kami
    atau penyedia Layanan Kami yang berhubungan dengan atau yang berhubungan
    dengan Platform dan Layanan bersifat mengikat dan konklusif pada Anda untuk
    tujuan apapun dan menjadi bukti dari setiap informasi dan/atau data yang
    dikirimkan antara Kami dan Anda. Anda dengan ini menyutujui bahwa semua
    catatan tersebut akan diterima sebagai bukti dan bahwa Anda tidak akan
    menentang atau membantah diterimanya, akurasi atau keaslian catatan tersebut,
    baik dengan alasan karena catatan tersebut dibuat dalam bentuk elektronik atau
    output dari sistem komputer, maupun alasan lainnya.</Text>
          </li>
          <li>
            <Text strong>Sub-kontraktor dan delegasi</Text><br/>
            <Text>Kami berhak untuk mendelegasikan atau sub-kontrak kinerja dari setiap fungsi
    yang Kami miliki sehubungan dengan Platform dan/atau jasa dan berhak untuk
    menggunakan jasa penyedia Layanan, sub-kontraktor dan/atau agen pada
    segala hal sebagaimana yang Kami anggap pantas.</Text>
          </li>
          <li>
            <Text strong>Pengalihan</Text><br/>
            <Text>Anda tidak dapat mengalihkan hak dan kewajiban yang dimiliki atas Syarat &amp;
    Ketentuan Penggunaan tanpa terdapat izin tertulis yang Kami berikan
    sebelumnya.</Text>
          </li>
          <li>
            <Text strong>Force Majeure</Text><br/>
            <Text>Anda menyetujui bahwa Kami akan dilepaskan dari segala pertanggungjawaban
    atas wanprestasi, kesalahan, gangguan atau keterlambatan dalam pelaksanaan
    kewajiban atau untuk setiap ketidakakuratan, atau ketidaksesuaian informasi
    yang terdapat pada Platform dan/atau konten Layanan, apabila hal tersebut
    disebabkan, baik secara sebagian maupun keseluruhan, langsung maupun tidak
    langsung, oleh suatu peristiwa atau kegagalan yang disebabkan force majeur
    atau keadaan memaksa di luar kuasa dan kehendak Kami yang berlaku secara
    umum untuk bidang usaha Kami.</Text>
          </li>
        </ol>
      </Paragraph>
    <i>Syarat &amp; Ketentuan Penggunaan ini terakhir diperbaharui tanggal 1 Juni 2020.</i>
    </div>
    
  </Modal>
  </>)
}

export default ModalSK;