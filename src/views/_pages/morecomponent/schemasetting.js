export default {
  organization_group_logo: {
    label: 'Logo',
    rules: [
      { required: true, message: 'Silahkan input logo organisasi' },
    ]
  },
  organization_group_name: {
    label: 'Nama Organisasi',
    rules: [
      { required: true, message: 'Silahkan input nama organisasi' },
    ]
  },
  organization_group_phone: {
    label: 'Phone Organisasi',
    rules: [
      { required: true, message: 'Silahkan input nama organisasi' },
    ]
  },
  organization_group_email: {
    label: 'Email Organisasi',
    rules: [
      { required: true, message: 'Silahkan input email organisasi' },
    ]
  },
  organization_group_npwp: {
    label: 'NPWP',
    rules: [
      { required: true, message: 'Silahkan input nomor NPWP' },
    ]
  },
  organization_group_website: {
    label: 'Website Organisasi',
  },
  organization_group_head: {
    label: 'PIC Organisasi',
  },
  organization_group_status: {
    label: 'Status Organisasi',
    rules: [
      { required: true, message: 'Silahkan pilih status organisasi' },
    ]
  },
  organization_group_address: {
    // label: 'Alamat Organisasi',
  },
  organization_group_about: {
    // label: 'Tentang Group Organisasi',
  },
  org_type_id: {
    label: 'Jenis Organisasi',
    rules: [
      { required: true, message: 'Silahkan pilih jenis organisasi' },
    ]
  },
  province_id: {
    label: 'Province',
    rules: [
      { required: true, message: 'Silahkan pilih propinsi' },
    ]
  },
  regency_id: {
    label: 'Kota/Kab',
    rules: [
      { required: true, message: 'Silahkan pilih kabupaten/kota' },
    ]
  },
  district_id: {
    label: 'Kecamatan',
    rules: [
      { required: true, message: 'Silahkan pilih kecamatan' },
    ]
  },
  village_id: {
    label: 'Kelurahan',
    rules: [
      { required: true, message: 'Silahkan pilih kelurahan' },
    ]
  },
  postalcode: {
    label: 'Postal Code'
  },
  organization_group_content: {
    label: 'Content Organisasi',
  },
  organization_group_ppn: {
    label: 'Jenis Pajak',
  },
  organization_group_npwp_address: {
    label: 'Alamat NPWP',
    rules: [
      { required: true, message: 'Silahkan masukkan alamat NPWP Anda' },
    ]
  },
  bank_id: {
    label: 'Nama Bank',
  },
  bank_account: {
    label: 'Nomor Rekening',
  },
  bank_account_name: {
    label: 'Nama Pemilik',
  },
  detailSchema: {
    contract_number: {
      label: 'No. Perjanjian Kerjasama',
      rules: [
        { required: true, message: 'Silahkan input No. Perjanjian Kerjasama' },
      ]
    },
    contract_time: {
      label: 'Masa Kerjasama',
      rules: [
        { required: true, message: 'Silahkan input Masa Kerjasama' },
      ]
    },
    contract_start_date : {
      label: 'Tanggal mulai Kerjasama',
      rules: [
        { required: true, message: 'Silahkan pilih tanggal mulai Kerjasama' },
      ]
    },
    contract_end_date : {
      label: 'Tanggal Kerjasama berakhir',
      rules: [
        { required: true, message: 'Silahkan pilih tanggal berakhirnya Kerjasama' },
      ]
    },
    sharing_am:{
      label: 'Bagi hasil AM',
      rules: [
        { required: true, message: 'Silahkan input profit AM' },
      ]
    },
    sharing_publisher:{
      label: 'Bagi hasil Penerbit',
      rules: [
        { required: true, message: 'Silahkan input profit Penerbit' },
      ]
    },
    agreement_file:{
      label: 'File kerjasama',
      rules: [
        { required: true, message: 'Silahkan upload file kerjasama' },
      ]
    },
    doc_profile_url : {
      label: 'Profile Perusahaan',
      rules: [
        { required: true, message: 'Silahkan upload file profile perusahaan' },
      ]
    },
    doc_siup_url : {
      label: 'SIUP',
      rules: [
        { required: true, message: 'Silahkan upload file SIUP' },
      ]
    },
    doc_npwp_url : {
      label: 'NPWP',
      rules: [
        { required: true, message: 'Silahkan upload file NPWP' },
      ]
    },
    doc_tdp_url : {
      label: 'TDP',
      rules: [
        { required: true, message: 'Silahkan upload file TDP' },
      ]
    },
    doc_ktp_url : {
      label: 'KTP Direktur',
      rules: [
        { required: true, message: 'Silahkan upload file KTP Direktur' },
      ]
    },
    doc_akta_url : {
      label: 'Akta Pendirian',
      rules: [
        { required: true, message: 'Silahkan upload file Akta Pendirian' },
      ]
    },
    doc_domisili_url : {
      label: 'Surat Keterangan Domisili',
      rules: [
        { required: true, message: 'Silahkan upload file surat keterangan domisili' },
      ]
    },
    doc_tax_url : {
      label: 'bayar pajak 3 Bulan terakhir',
      rules: [
        { required: true, message: 'Silahkan upload file bukti bayar pajak' },
      ]
    },
    doc_tax_expired : {
      label: 'Tentukan tanggal & unggah bukti ',
      rules: [
        { required: true, message: 'Silahkan upload file bukti bayar pajak' },
      ]
    },
    doc_listtax_url : {
      label: 'Surat Keterangan Daftar Pajak',
      rules: [
        { required: true, message: 'Silahkan upload file keterangan daftar pajak' },
      ]
    },
    doc_pkp_url : {
      label: 'Surat Pengukuhan Pengusaha Kena Pajak',
      rules: [
        { required: true, message: 'Silahkan upload file surat pengukuhan kena pajak' },
      ]
    },
    organization_file_rekening : {
      label: 'Rekening Koran / Halaman Pertama Buku Bank',
      rules: [
        { required: true, message: 'Silahkan upload file Rekening Koran / Halaman Pertama Buku Bank' },
      ]
    }
  },
}