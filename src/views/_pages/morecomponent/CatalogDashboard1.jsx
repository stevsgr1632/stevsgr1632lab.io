import React, { useState, useEffect } from 'react';
import axios from "axios";
import oWebhook from 'providers/webhook';
import useDataDashboard from "./useDataDashboard";
import { Row, Col, Spin, Alert, List, Avatar, Progress } from 'antd';
/* eslint-disable */
const styles = {
    title: {
        marginTop: -10,
        marginBottom: 10,
        fontSize: 22,
    },
    content: {
        minHeight: 200
    }
}

const Chart1 = ({ data, err }) => {
    if (data) {
        return (
            <div>
                <h5>Resume</h5>
                <List dataSource={data} renderItem={item => (
                    <List.Item>
                        <List.Item.Meta
                            avatar={<Avatar 
                              style={{
                                backgroundColor: '#C92036',
                                verticalAlign: 'middle',
                              }}                      
                              size="small">{item.name.charAt(0).toUpperCase()}</Avatar>}
                            title={`${item.total} - ${item.name}`}
                        />
                    </List.Item>
                )} />
            </div>
        )
    }
    if (err) {
        return (
            <div>
                <Alert
                    message="Error when fetch resume"
                    description={err.message}
                    type="error"
                />
            </div>
        )
    }

    return (
        <Spin tip="Loading...">
            <h5>Resume</h5>
            <p>Loading data resume...</p>
        </Spin>
    )
}

const Chart2 = ({ data, err }) => {
    if (data) {
        return (
            <div>
                <h5>Status Katalog</h5>
                <List dataSource={data} renderItem={item => (
                    <List.Item>
                        <List.Item.Meta
                            title={item.status_name}
                            description={<Progress percent={item.percentValue} />}
                        />
                    </List.Item>
                )} />
            </div>
        )
    }

    if (err) {
        return (
            <div>
                <Alert
                    message="Error when fetch status"
                    description={err.message}
                    type="error"
                />
            </div>
        )
    }

    return (
        <Spin tip="Loading...">
            <div>
                <h5>Status Katalog</h5>
            </div>
        </Spin>
    )
}

const loadData = async (url) => {
  return oWebhook.get(url);
}

const App = () => {
    const signal = React.useMemo(() => axios.CancelToken.source(), []);
    const [data2, setData2] = useState();
    const { results: resultsdata1 } = useDataDashboard('dashboard-summary-one');

    useEffect(() => {
        loadData('dashboard-catalog-status')
            .then(({ data = {} }) => {
                setData2({ data: data.status });
            })
            .catch((err) => {
                setData2({ err });
            })
        return () => signal.cancel();
    }, [])

    return (
        <div>
            <h3 style={styles.title}>Dashboard MCCP</h3>
            <div style={styles.content}>
                <Row gutter={24}>
                    <Col span={12}>
                        <Chart1 {...resultsdata1} />
                    </Col>
                    <Col span={12}>
                        <Chart2 {...data2} />
                    </Col>
                </Row>
            </div>
        </div>
    );
}

export default App;