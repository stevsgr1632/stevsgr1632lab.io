import React from 'react';
import { Button } from 'antd';
import './morecss/notallowedpage.scss';
import Lock from './morecomponent/Lock';
import { useHistory } from 'react-router-dom';

const NotAllowed = (props) => {
  const history = useHistory();

  return (
    <div className="container-notallow">
      <div className='notallow-sub-continer'>
        <h1>4<div className="lock"><div className="top"></div><div className="bottom"></div>
        </div>3</h1>
        <p className="ant-result-subtitle">Anda tidak mempunyai hak akses untuk halaman ini</p>
        <Button type="primary" danger onClick={() => history.goBack()}>Kembali</Button>
      </div>
      <Lock/>
    </div>
  );
}

export default NotAllowed;