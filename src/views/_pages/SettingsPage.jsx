import React from 'react';
import { Form, Tabs, Button } from 'antd';
import Message from 'common/message';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import {Breadcrumb,Content} from 'components';
import Notification from 'common/notification';
import LoadingSpin from 'components/LoadingSpin';
import useCustomReducer from 'common/useCustomReducer';
import DocumentPage from './morecomponent/DocumentPage';
import ProfileSetting from './morecomponent/ProfileSetting';
import RekeningSetting from './morecomponent/RekeningSetting';
import { ProfileOutlined,BankOutlined,FileTextOutlined } from '@ant-design/icons';
/* eslint-disable */

const {initial,dashboard} = pathName;
const initialData = { 
  dataSubmit : {}, 
  region : {
    province: { loading: false, list: [] },
    city: { loading: false, list: [] },
    district: { loading: false, list: [] },
    village: { loading: false, list: [] }
  }, 
  loadingForm : false,
  resources : {}
};
export const DataSettings = React.createContext({});

const SettingsPage = () => {
  const [form] = Form.useForm();
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);
  const userInfo = JSON.parse(localStorage.getItem('userInfo')) || {};
  const handleChangeLogo = (name, value,data) => {
    reducerFunc('dataSubmit',{organization_group_logo : data.file_url_download });
  };
  const handleChangeFile = (name, name2, file) => {
    reducerFunc('dataSubmit',{[name]: file.file_url_download, [name2] : file.originalFilename});
  }
  const handleStepChange = current => {
    console.log(current);
  };
  const handleClickSave = (e) => {
    e.preventDefault();
    reducerFunc('loadingForm',true,'conventional');
    form.validateFields().then( async values => {
      // console.log(values);
      let dataUpdate = {
        ...dataReducer?.dataSubmit, 
        organization_group_content : dataReducer?.dataSubmit?.organization_group_content ? dataReducer?.dataSubmit?.organization_group_content?.join() : 'Tidak Ada Konten',
        organization_group_status : dataReducer?.dataSubmit?.organization_group_status ?? 1
      };
      try {
        let response = await oProvider.update(`organization-group?id=${dataUpdate.id}`, dataUpdate);
        Notification({
          response, 
          type:'success', 
          placement:'bottomRight'
        });
        fetchData();
        reducerFunc('loadingForm',false,'conventional');
      } catch (error) {
        Message({type:'error',text:`Kesalahan, ${error.message}`});
        reducerFunc('loadingForm',false,'conventional');
      }
    })
    .catch(errorInfo => {
      console.log(errorInfo);
      errorInfo.errorFields.map(x => {
        Message({type:'error',text:`Kesalahan, ${x.name.toString()} : ${x.errors.toString()}`});
        reducerFunc('loadingForm',false,'conventional');
        return false;
      });
    });  
  };

  const handleChangeInput = (name, value,e) => {
    if(value){
      reducerFunc('dataSubmit',{[name] : value });
    }
  };

  const handleChangeCombo = async (name, value) => {
    try {
      reducerFunc('dataSubmit',{[name] : value });
      switch (name) {
        case 'province_id':
          reducerFunc('region',{ city:  { loading: true, list: [] } });
          const { data : city } = await oProvider.list(`regencies-dropdown?id=${value}`);
          reducerFunc('region',{
            city: { loading: false, list: city }, 
            district: [], 
            village: []
          });
          break;

        case 'regency_id':
          reducerFunc('region',{ district: { loading: true, list: [] } });
          const { data : district }= await oProvider.list(`districts-dropdown?id=${value}`);
          reducerFunc('region',{ district: { loading: false, list: district } });
          break;

        case 'district_id':
          reducerFunc('region',{ village: { loading: true, list: [] } });
          const { data : village } = await oProvider.list(`village-dropdown?id=${value}`);
          reducerFunc('region',{ village: { loading: false, list: village } });
          break;
        default:
          break;
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  const fetchData = async() => {
    reducerFunc('loadingForm',true,'conventional');
    try {
      reducerFunc('region',{ province: { loading: true, list: [] } });
      const [
        {data : province},
        {data : dataSetting},
        {data : bankChoices},
        {data : organizationType}
      ] = await Promise.all([
        oProvider.list('province-dropdown'),
        oProvider.list('setting-organization'),
        oProvider.list('bank-dropdown'),
        oProvider.list('organization-type-dropdown'),
      ]);
      form.setFieldsValue({...dataSetting,organization_group_status:1});
      reducerFunc('dataSubmit', dataSetting);

      const regencydata = await oProvider.list(`regencies-dropdown?id=${dataSetting?.province_id}`);
      const districtdata = await oProvider.list(`districts-dropdown?id=${dataSetting?.regency_id}`);
      const villagedata = await oProvider.list(`village-dropdown?id=${dataSetting?.district_id}`);
      reducerFunc('region',{ 
        province: { loading: false, list: province },
        city: { loading: false, list: regencydata?.data },
        district: { loading: false, list: districtdata?.data },
        village: { loading: false, list: villagedata?.data }
      });
      
      reducerFunc('loadingForm',false,'conventional');
      reducerFunc('resources',{ bankChoices, organizationType });
    } catch (error) {
      console.clear();
      console.log(error?.message);
      reducerFunc('loadingForm',false,'conventional');
    }
  }

  React.useEffect(() => {
    fetchData();
  }, []);
  
  const breadcrumb = [
    { text: 'Home', to: initial },
    { text: 'Dashboard', to: dashboard },
  ];

  const content = {
    title: 'Settings',
    // subtitle: 'Update Perubahan data pengguna',
  };

  return (
    <DataSettings.Provider 
      value={{
        dataReducer,
        handleChangeLogo,
        handleChangeInput,
        handleChangeCombo,
        handleChangeFile,
      }}
    >
    <Breadcrumb items={breadcrumb} />
    <Content {...content} >
      <LoadingSpin loading={dataReducer?.loadingForm}>
        <Form form={form}>
          <Tabs defaultActiveKey="1" onChange={handleStepChange}>
            <Tabs.TabPane
              tab={
                <span>
                  <ProfileOutlined />
                  Profile
                </span>
              }
              key="1"
            >
              <ProfileSetting/>
            </Tabs.TabPane>
            {(userInfo.role_name === 'Penerbit') ?  
              <Tabs.TabPane
              tab={
                <span>
                  <FileTextOutlined />
                  Dokumen
                </span>
              }
              key="2"
            >
              <DocumentPage form={form}/>
            </Tabs.TabPane>
            : null}
            <Tabs.TabPane
              tab={
                <span>
                  <BankOutlined />
                  Rekening
                </span>
              }
              key="3"
            >
              <RekeningSetting/>
            </Tabs.TabPane>
          </Tabs>
          <Button danger type="primary" onClick={handleClickSave} style={{float:'right'}}>Update</Button>
        </Form>
      </LoadingSpin>
    </Content>
    </DataSettings.Provider>
  );
}

export default SettingsPage;