import React, { Component } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from 'reactstrap';
import oAuth from 'providers/Auth';
import pathName from 'routes/pathName';

const { base,home } = pathName;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: 'admin@mylib.id',
      // email: 'dinas@mail.com',
      // email : 'penerbit@mail.com',
      password: '12QWaszx'
    }
  }

  handleSubmit = async (e) => {
    e.preventDefault();

    try {
      await oAuth.login(this.state);
      this.props.history.push(home);
    } catch (err) {
      console.log(err.message);
    }
  }

  handleChange = (e) => {
    this.setState({ ...this.state, [e.target.name]: e.target.value });
  };

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col lg="6">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={this.handleSubmit}>
                      <h1>Login - MCCP</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Email" autoComplete="email" onChange={this.handleChange} name="email" value={this.state.email} />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" autoComplete="current-password" onChange={this.handleChange} name="password" value={this.state.password} />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="danger" onSubmit={this.handleSubmit} className="px-4">Login</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Lupa password?</Button>
                        </Col>
                      </Row>
                    </Form>
                    <p>email: admin@mylib.id , dinas@mail.com , penerbit@mail.com , password: 12QWaszx</p>
                    <b>Aplikasi demo Moco Catalog yg current berjalan : http://mccpdev.mocogawe.com/</b>
                    <p>user dinas: tesakundinas@gmail.com , Password: Asd12345</p>
                    <p>User Super Admin: backend@dashboard.com , password: P4zzw0rD</p>
                    <p>User Project Manager: pm-dev@yopmail.com , password: p8mn791i</p>
                    <p>User Penerbit: penerbittest@yopmail.com , password: OsTBHH2J</p>                 
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
