import React, { Fragment } from 'react';
import {Breadcrumb} from '../../components';
import { DashboardStat,ChartContent,ChartSell,ChartSellCategory,ListBuy } from '../dashboardPenerbit';
import { Card, Row, Col } from 'antd';
import config from '../dashboardPenerbit/index.config';
import '../dashboardPenerbit/style.scss';
/* eslint-disable */

const DashboardPenerbit = (props) => {
    const { breadcrumb } = config;

    return (
        <Fragment>
            <Breadcrumb items={breadcrumb} />
            <Card style={{background:'transparent'}} bodyStyle={{padding:'10px'}}>
                <Card className='container-left' style={{height:'auto'}}>
                    <DashboardStat />
                </Card>
                <Row gutter={[2,2]}>
                    <Col xs={24} sm={24} md={12} lg={12} xl={12} >
                        <Card className='container-left' >
                            <ChartContent/>
                        </Card>
                    </Col>
                    <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                        <Card className='container-right' >
                            <ChartSell/>
                        </Card>
                    </Col>
                </Row>
                <Row gutter={[2,2]}>
                    <Col xs={24} sm={24} md={12} lg={12} xl={12} >
                        <Card className='container-left' >
                            <ChartSellCategory/>
                        </Card>
                    </Col>
                    <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                        <Card className='container-right' >
                            <ListBuy/>
                        </Card>
                    </Col>
                </Row>
            </Card>
        </Fragment>
    )
}

export default DashboardPenerbit;
