import React from 'react';
import Axios from 'axios';
import './morecss/loginpage.scss';
import oAuth from 'providers/Auth';
import { connect } from 'react-redux';
import pathName from 'routes/pathName';
import { Link } from "react-router-dom";
import LupaPassPage from './LupaPassPage';
import { getHeadersBase } from "utils/request";
import InputText from 'components/ant/InputText';
import LoadingSpin from 'components/LoadingSpin';
import userInfoAction from 'redux/userInfo/action';
import menuAccessReduxAction from 'redux/rbac/action';
import useCustomReducer from "common/useCustomReducer";
import logo from 'layouts/moco/img/brand/logo_text.png';
import {Layout,Form,Input,Alert,Row, Col,Button} from 'antd';
import { MailOutlined,LockOutlined } from '@ant-design/icons';
import { initURL,initialEndpoint,tokenCompareTimeNow } from "utils/initialEndpoint";
/* eslint-disable */

const axiosNewRequest = Axios.create({});
const { base } = pathName;

const callingUserRequestAfterLogin = async (url,token) => {
  const { data } = await axiosNewRequest.request({
    method : 'GET',
    url : `${initURL}/${initialEndpoint}/${url}`,
    headers : {
      ...getHeadersBase,
      "Authorization" : `Bearer ${token}`
    }
  });
  return data;
}

const initialData = {
  loading : false,
  visible : false,
  messageError : '',
  state : {
    email: 'admin@mylib.id',
    // email: 'dinas@mail.com',
    // email : 'penerbit@mail.com',
    password: '12QWaszx'
  },
  redirectPage : {}
};
const LoginPage = (props) => {
  const [form] = Form.useForm();
  const {state_auth,history,historyRedux} = props;
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  const handleSubmit = async (e) => {
    reducerFunc('loading',true,'conventional');
    try {
      localStorage.clear();
      sessionStorage.clear();
      let resp = await oAuth.login(dataReducer.state);
      if(resp){
        const token = resp?.data?.attributes?.accessToken;
        const [
          { data : userRBAC },
          { data : userInfo },
        ] = await Promise.all([
          callingUserRequestAfterLogin('user-rbac',token),
          callingUserRequestAfterLogin('user-info',token)
        ]);
        menuAccessReduxAction.setMenuAccess(userRBAC);
        userInfoAction.setUserInfo(userInfo);
        reducerFunc('redirectPage',userInfo);
      }
      reducerFunc('loading',false,'conventional');
    } catch (err) {
      if(err.message || err.response.data){
        reducerFunc('messageError','Email atau password yang anda masukkan salah !','conventional');
        reducerFunc('loading',false,'conventional');
      }
      console.log(err?.message);
    }
  };
  const handleClickLogo = (e) => {
    history.push(base);
  };

  const handleChange = (name,value,e) => {
    reducerFunc('state',{ [name] : value });
  };
  const handleChangePass = (e) => {
    reducerFunc('state',{ password: e.target.value });
  };

  const schema = {
    email: { label: "Email", rules :[
      { required: true, message: 'Silahkan isi email terlebih dahulu' },
    ]},
    password: { label: "Password", rules :[
      { required: true, message: 'Silahkan isi password terlebih dahulu' },
    ]},
  };

  const colorCode = {
    color1 : '#C92036',
    color2 : '#f0f2f5',
  };

  React.useEffect(() => {
    form.setFieldsValue({...dataReducer.state});
  },[]);

  // user cant access login page after logged in
  React.useEffect(() => {
    const { attributes } = state_auth || {};
    if(historyRedux){
      if(attributes){
        if(!tokenCompareTimeNow(attributes?.refreshToken)){
          history.push(historyRedux);
        }
      }      
    }
  },[historyRedux]);

  React.useEffect(() => {
    if(dataReducer?.redirectPage?.role_first_url){
      if(historyRedux){
        window.location.replace(historyRedux);
      }else{
        window.location.replace(dataReducer?.redirectPage?.role_first_url);
      }
    }
  },[dataReducer?.redirectPage?.email]);

  return (
    <Layout>
      <div className="login__page--wrapper">
        <div className="login__page--title" style={{backgroundColor:colorCode.color1}}>
          <figure onClick={handleClickLogo}>
            <img src={logo} alt="logo-company" />
          </figure>
            <h1 style={{color:colorCode.color2}}><b>Selamat Datang</b></h1>
            <center><h5 style={{color:colorCode.color2}}>Kami mengucapkan terima kasih atas kepercayaan Anda</h5></center>
        </div>
        <div className="login__page--content">
          <Row gutter={[0,24]} className="login__page--content-wrap">
            <Col span={24}>
              <h3 style={{color:colorCode.color1}}>Masuk</h3>
              {dataReducer.messageError && (
                <Alert type="error" message={dataReducer.messageError} showIcon closeable />
              )}
              <LoadingSpin loading={dataReducer.loading}>
                <Form
                  form={form}
                  onFinish={handleSubmit}
                  >
                  <InputText name="email" prefix={<MailOutlined />} autoFocus type="email" schema={schema} onChange={handleChange} placeholder="Input Email" />
                  <Form.Item name="password" autoComplete="current-password" label={schema['password'].label} rules={schema['password'].rules} labelCol={{ span: 24 }}>
                    <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} onChange={handleChangePass} placeholder="Input password" />
                  </Form.Item>
                  <Button danger type="primary" htmlType="submit" block>Submit</Button>
                  <div style={{float:'right'}}>
                    <Button danger type="link" onClick={() => reducerFunc('visible',true,'conventional')}><b>Lupa Password</b></Button>
                  </div>
                </Form>
              </LoadingSpin>
            </Col>

            <Col span={24}>
              <Alert
                message=""
                description={<>Belum daftar ? <Link to="/register" style={{color:colorCode.color1}}><b>Daftarkan Organisasi Anda</b></Link></>}
                type="info"
              />
            </Col>
          </Row>
        </div>
      </div>
      <LupaPassPage visible={dataReducer.visible} setvisible={reducerFunc} schema={schema} />
    </Layout>
  );
}
const mapStateToProps = ({auth,historyRedux}) => ({ 
  state_auth : auth, 
  historyRedux,
});

export default connect(mapStateToProps)(LoginPage);
