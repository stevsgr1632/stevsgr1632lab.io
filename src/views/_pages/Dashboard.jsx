import React from 'react';
import oProvider from 'providers/oprovider';
import {Breadcrumb,Content} from 'components';
import { useHistory } from 'react-router-dom';
import {Row,Col,Card,Divider,Avatar} from 'antd';
import { numberFormat } from "utils/initialEndpoint";
/* eslint-disable */

const {Meta} = Card;

const App = () => {
  const history = useHistory();
  const [data,setData] = React.useState({});
  const [loading,setLoading] = React.useState(true);

  React.useEffect(() => {
    async function fetchData(){
      setLoading(true);
      try {
        const [
          { data : summary },
          { data : pengadaan },
          { data : publisher },
        ] = await Promise.all([
          oProvider.list('dashboard-summary-one'),
          oProvider.list('dashboard-catalog-budget?id=42b6a5bc-5464-11ea-80ba-aee15e33e3db'),
          oProvider.list('dashboard-catalog-publisher?organization_id=42b6a5bc-5464-11ea-80ba-aee15e33e3db')
        ]);
        setData({ ...data, summary, pengadaan, publisher });
        setLoading(false); 
      } catch (error) {
        console.log(error?.message);
        setLoading(false);
      }
    }
    fetchData();

    return oProvider.cancel();
  },[]);

  const breadcrumb = [
    { text: 'Home' },
    { text: 'Dashboard' },
  ];

  const content = {
    title: 'Selamat Datang',
    subtitle: 'Disini anda dapat memantau statistik, melakukan berbagai aktifitas dan pengaturan (Data dihalaman ini masih sebagai contoh)',
  };

  React.useEffect(() => {
    if(localStorage.getItem('menuAccess')){
      const url = JSON.parse(localStorage.getItem('menuAccess')).map(rbac => rbac.menu_url);
      history.push(url[0]);
    }
  },[localStorage.getItem('menuAccess')]);

  return (<>
    <Breadcrumb items={breadcrumb} />
    <Content {...content} >
      <Row gutter={[16,16]} style={{display:'grid',gridTemplateColumns:'repeat(auto-fit,minmax(250px,1fr))',alignItems:'start'}}>
      <Divider style={{gridColumn:'1/-1'}} orientation="left"><strong>Summary</strong></Divider>
      {data?.summary ? data.summary.map(x => (
        <Col key={x.name}>
          <Card 
            style={{width:'250px',backgroundColor:'#eee'}}
            bordered={true} 
            loading={loading}
          >
            <h6>{x.name}</h6>
            <h3>{numberFormat(x.total)}</h3>
          </Card>
        </Col>
      )) : null}
      <Divider style={{gridColumn:'1/-1'}} orientation="left"><strong>Penjualan</strong></Divider>
      {data?.pengadaan && data.pengadaan.map(x => (
        <Col key={x.id}>
          <Card 
            style={{width:'250px',backgroundColor:'#eee'}}
            bordered={true} 
            loading={loading}
          >
            <h6>{x.budget_name}</h6>
            <h3>Rp. {numberFormat(x.amount)}</h3>
          </Card>
        </Col>
      ))}
      <Divider style={{gridColumn:'1/-1'}} orientation="left"><strong>Katalog Digital</strong></Divider>
      {data?.publisher && data.publisher.map(x => (
        <Col key={x.id}>
          <Card 
            style={{width:'250px',backgroundColor:'#eee'}}
            bordered={true} 
            loading={loading}
          >
            <Meta style={{margin:0}}
              avatar={<Avatar src={x.publisher_logo} />}
              title={<><h5>{x.publisher_name}</h5><h5>{x.total} Judul</h5></>}
            />
          </Card>
        </Col>
      ))}
      </Row>
    </Content>
  </>);

}

export default App;