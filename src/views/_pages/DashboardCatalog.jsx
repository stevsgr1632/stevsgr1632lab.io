import React from 'react';
import { Breadcrumb, Content } from 'components';
import Dashboard1 from './morecomponent/CatalogDashboard1';
import Dashboard2 from './morecomponent/CatalogDashboard2';
import Dashboard3 from './morecomponent/CatalogDashboard3';
/* eslint-disable */

const breadcrumbs = [
    { text: 'Beranda', to: '/' },
    { text: 'Dashboard', to: '/dashboard' },
    { text: 'Dashboard Catalog' },
]

const App = () => {
  return (<>
    <Breadcrumb items={breadcrumbs} />
    <Content><Dashboard1 /></Content>
    <Content><Dashboard2 /></Content>
    <Content><Dashboard3 /></Content>
  </>);
}

export default App;