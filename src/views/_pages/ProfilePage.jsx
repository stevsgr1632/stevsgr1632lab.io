import React from 'react';
import Message from 'common/message';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import {Breadcrumb,Content} from 'components';
import Notification from 'common/notification';
import {Row,Col,Form, Button,Modal} from 'antd';
import InputDate from 'components/ant/InputDate';
import LoadingSpin from 'components/LoadingSpin';
import InputText from 'components/ant/InputText';
import InputFile from 'components/ant/InputFile3';
import useCustomReducer from 'common/useCustomReducer';
import InputTextArea from 'components/ant/InputTextArea';
import { formDateInputValue } from "utils/initialEndpoint";
import ModalChangePass from './morecomponent/ModalChangePass';
/* eslint-disable */

const {initial,dashboard} = pathName;
const initialData = {
  dataImg : '',
  loading: false,
  visible : false,
  previewTitle : '',
  previewVisible : false,
  state : {}
}
export default () => {
  // console.log(props);
  const [form] = Form.useForm();
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  const handleSubmit = e => {
    e.preventDefault();
    reducerFunc('loading',true,'conventional');
    form.validateFields().then(async values => {
      // console.log(values);
      try {
        const response = await oProvider.update(`users?id=${dataReducer?.state?.id}`,dataReducer.state);
        if(response){
          Notification({
            response: {
              code : response.code, 
              message : response.message
            }, 
            type:'success', 
            placement:'bottomRight'
          });
        }
        reducerFunc('loading',false,'conventional');
      } catch (error) {
        console.log(error?.message);
        reducerFunc('loading',false,'conventional');
      }
    })
    .catch(errorInfo => {
      // console.log(errorInfo);
      errorInfo.errorFields.map(x => {
        Message({type:'error',text:`Kesalahan, ${x.name.toString()} : ${x.errors.toString()}`});
        reducerFunc('loading',false,'conventional');
        return false;
      });
    });
    
  }
  const handleChangePass = e => {
    e.preventDefault();
    reducerFunc('visible',true,'conventional');
  }

  const handleChangeForm = (name,value,e) => {
    reducerFunc('state',{ [name]:value });
  }
  const handleUploadFile = async (name,file,data) => {
    console.log('ini file',data);
    const dataImg = data.file_url_download || '';
    reducerFunc('dataImg',dataImg,'conventional');
    reducerFunc('previewTitle',file.name,'conventional');
    reducerFunc('state',{ image:data.file_url_download });
  };

  const headingProps = {
    breadcrumb : [
      { text: 'Home', to: initial },
      { text: 'Dashboard', to: dashboard },
      { text: 'Profile' },
    ],
    content :{
      title: 'Profile Pengguna',
      subtitle: 'Update Perubahan data pengguna',
    }
  }

  const schema = {
    name: { label: "Nama lengkap", rules :[
      { required: true, message: 'Silahkan isi nama lengkap terlebih dahulu' },
    ]},
    email: { label: "Email", rules :[
      { required: true, message: 'Silahkan isi email terlebih dahulu' },
    ]},
    phone: { 
      label: "Telepon", 
      rules :[
        { required: true, message: 'Silahkan isi telepon terlebih dahulu' },
      ]
    },
    image: { 
      label: "Foto", 
      rules :[
        { required: true, message: 'Silahkan isi upload foto terlebih dahulu' },
      ]
    },
    about_me: { 
      label: "Tentang Saya", 
      rules :[
        { required: true, message: 'Silahkan isi keterangan diri Anda' },
      ]
    },
    tanggal_lahir: { 
      label: "Tanggal Lahir", 
      rules :[
        { required: true, message: 'Silahkan pilih tanggal lahir Anda' },
      ]
    },
    alamat: { 
      label: "Alamat Lengkap", 
      rules :[
        { required: true, message: 'Silahkan isi alamat lengkap Anda' },
      ]
    },
  };

  const handlePreview = () => reducerFunc('previewVisible',true,'conventional');
  const handleCancel = () => reducerFunc('previewVisible',false,'conventional');

  React.useEffect(() => {
    const fetchData = async() => {
      reducerFunc('loading',true,'conventional');
      const { data } = await oProvider.list('user-info');
      data.tanggal_lahir = formDateInputValue(data.tanggal_lahir);
      
      reducerFunc('state', data);
      data.image && reducerFunc('dataImg',data.image,'conventional');
      form.setFieldsValue({...data});
      reducerFunc('loading',false,'conventional');
    }
    fetchData();
  },[]);

  return(<>
  <Breadcrumb items={headingProps.breadcrumb} />
  <Content {...headingProps.content} >
    <LoadingSpin loading={dataReducer.loading}>
      <Form form={form} onFinish={handleSubmit}>
        <Row gutter={[16,0]}>
          <Col span={3}>
            <InputFile name="image" accept=".png,.jpeg,.jpg" schema={schema} onChange={handleUploadFile} imgUrl={(dataReducer.dataImg) ? dataReducer.dataImg : ''} handlePreview={handlePreview}/>        
          </Col>
          <Col span={21}>
            <Row gutter={[16,0]}>
              <Col span={12}>
                <InputText name="name" schema={schema} onBlur={handleChangeForm} />
              </Col>
              <Col span={12}>
                <InputText name="email" schema={schema} onBlur={handleChangeForm} />
              </Col>
              <Col span={12}>
                <InputText name="phone" 
                  pattern="\(?(?:\+62|62|0)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}" 
                  title="081212341234" schema={schema} onBlur={handleChangeForm} />
              </Col>
              <Col span={12}>
                <InputDate name="tanggal_lahir" schema={schema} onChange={handleChangeForm} width="100%" />
              </Col>
              <Col span={12}>
                <InputTextArea name="about_me" schema={schema} onBlur={handleChangeForm} />
              </Col>
              <Col span={12}>
                <InputTextArea name="alamat" schema={schema} onBlur={handleChangeForm} />
              </Col>
            </Row>
          </Col>
        </Row>
      <hr/>
      <div style={{display:'flex',justifyContent:'space-between'}}>
        <Button type="primary" onClick={handleChangePass}>Ubah Password</Button>
        <Button type="primary" htmlType="submit" onClick={handleSubmit}>Simpan</Button>
      </div>
      </Form>
    </LoadingSpin>
  </Content>
  <ModalChangePass visible={dataReducer?.visible} setvisible={reducerFunc} />
  <Modal
    visible={dataReducer?.previewVisible}
    title={dataReducer?.previewTitle}
    footer={null}
    centered
    onCancel={handleCancel}
    >
    <img alt="example" style={{ width: '100%' }} src={dataReducer?.dataImg} />
  </Modal>
  </>);
};