import React, { useState } from "react";
import FormControl from "./Form";
import config from "./index.config";
import oProvider from "providers/oprovider";
import Notification from "common/notification";
import { Breadcrumb, Content } from "components";
/* eslint-disable */
const resource = 'users';

const App = (props) => {
  const { schema, indexUrl } = config;
  const { breadcrumb, content } = config.edit;
  const userId = props.match.params.id;
  const [imageUrl, setImageUrl] = useState('');

  const backToList = () => {
    props.history.push(indexUrl);
  };

  const openNotification = params => {
    Notification({...params});
  };

  const handleSubmit = async (values) => {
    if (imageUrl) {
      values.image = imageUrl;
    } else {
      openNotification({type : 'error', response : {
        code: 'Data tidak berhasil disimpan',
        message: 'Silahkan unggah ulang gambar atau muat ulang halaman dan coba lagi',
      }});
      return false;
    }

    try {
      // inserting to API
      const response = await oProvider.update(`${resource}?id=${userId}`, values );
      if(response){
        openNotification({type : 'success', response});
        backToList();
      }
    } catch (error) {
      const isShowMessage = errorHandling(error);
      if(!isShowMessage){
        const errorResponse = {
          code : "Data tidak berhasil disimpan",
          message : 'Terjadi kesalahan saat proses data'
        }
        Notification({response:errorResponse, type : 'error',placement : 'topRight'});
      }
      console.log(error.message);
      return false;
    }
  };

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <FormControl
          onSubmit={handleSubmit}
          onCancel={backToList}
          imageUrl={imageUrl}
          setImageUrl={setImageUrl}
          schema={schema}
          type="edit"
          paramsId={userId}
        />
      </Content>
    </React.Fragment>
  );
};

export default App;
