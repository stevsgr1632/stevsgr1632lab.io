import React from "react";
import config from "./index.config";
import queryString from "query-string";
import { Modal, Typography } from 'antd';
import { MenuAksesContext } from './index';
import oProvider from "providers/oprovider";
import MocoTable from "components/MocoTable";
import Notification from 'common/notification';
import { DeleteOutlined } from '@ant-design/icons';
import useCustomReducer from 'common/useCustomReducer';
import ModalViewDetail from "./components/ModalViewDetail";
import errorHandling from "utils/errorHandling";
/* eslint-disable */

const resource = "user";
const { confirm } = Modal;
const { Text } = Typography;

const initialData = {
  state : {
    loading : false,
  },
  url : '',
  modalDetail : {
    visible: false, data : {}
  }
};

const List = (props) => {
  const menuAksesContext = React.useContext(MenuAksesContext);
  const { model } = props;
  const [dataReducer, reducerFunc] = useCustomReducer(initialData);

  const showConfirmDelete = (props) => {
    confirm({
      title: 'Hapus Pengumuman',
      icon: <DeleteOutlined />,
      content: <React.Fragment>
        <Text>Anda yakin ingin menghapus data ini (<b>{props.name}</b>) dan semua data terkait?</Text>
        <br />
        <Text type="danger">Tindakan ini tidak bisa dibatalkan</Text>
      </React.Fragment>,
      onOk: async () => {
        try {
          const response = await oProvider.delete(`${resource}-delete?id=${props.id}`);
          if (!response) {
            Notification({ type : 'error', response : {
                message: 'Data gagal dihapus',
                description: 'Silahkan muat ulang halaman dan coba lagi',
              } 
            });
            return;
          }
          await refresh(dataReducer?.url, model);
          Notification({ type : 'success', response : {
              message: 'Data berhasil dihapus',
              description: response.message,
            } 
          });
        } catch (error) {
          console.log(error?.message);
          return false;
        }
      },
      onCancel() { },
    });
  };

  const refresh = async (url, model) => {
    reducerFunc('state',{ loading: true });
    const paramsObject = {};
    
    Object.keys(model).forEach(x => {
      if(model[x]) {
        paramsObject[x] = model[x];
      }
    });
    try {
      const params = queryString.stringify(paramsObject, {skipEmptyString : true});
      const { meta, data } = await oProvider.list(`${resource}-search?${params}${url}`);
      reducerFunc('state',{
        loading: false,
        pagination: { pageSize: meta.limit, total: parseInt(meta.total) },
        dataSource: data,
      });
    } catch (error) {
      const isShowMessage = errorHandling(error);
      if(!isShowMessage){
        const errorResponse = {
          code : error.response.status || "Error",
          message : 'Terjadi kesalahan saat proses list data'
        }
        Notification({response:errorResponse, type : 'error',placement : 'topRight'});
      }
      console.log(error?.message);
      return false;
    }
  };

  const handleTableChange = (pagination, _, sorter) => {
    let paramsObject = {};
    if (pagination) paramsObject["page"] = pagination.current;
    if (sorter && sorter.field) {
      paramsObject["sortBy"] = sorter.field;
      paramsObject["sortDir"] = sorter.order === "ascend" ? "asc" : "desc";
    }
    if (model.search) paramsObject["searching"] = model.search;
    reducerFunc('url',new URLSearchParams(paramsObject).toString(),'conventional');
  };
  const viewDetail = async (row) => {
    try {
      const { data } = await oProvider.list(`user-get-one?id=${row.id}`);
      reducerFunc('modalDetail',{ data, visible : true});
    } catch (error) {
      reducerFunc('modalDetail',{visible : false});
      console.log(error?.message);
      return false;
    }
  };

  const propsModalDetail = {dataReducer, reducerFunc};

  React.useEffect(() => {
    refresh(dataReducer?.url, model);
  }, [dataReducer?.url, Object.values(model).join()]);

  return (
  <>
    <MocoTable
      {...config.list.config(props, showConfirmDelete, viewDetail, menuAksesContext)}
      {...dataReducer?.state}
      onChange={handleTableChange}
    />
    <ModalViewDetail {...propsModalDetail} />
  </>
  );
};

export default List;
