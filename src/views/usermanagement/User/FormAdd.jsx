import React, { useState } from "react";
import FormControl from "./Form";
import config from "./index.config";
import oProvider from "providers/oprovider";
import Notification from 'common/notification';
import { Breadcrumb, Content } from "components";
import errorHandling from "utils/errorHandling";
/* eslint-disable */
const resource = 'users';

const App = (props) => {
  const { schema, indexUrl } = config;
  const { breadcrumb, content } = config.add;
  const [imageUrl, setImageUrl] = useState('');

  const backToList = () => {
    props.history.push(indexUrl);
  };

  const openNotification = (params) => {
    Notification({...params});
  };
  const codeNotif = 'Data tidak berhasil disimpan';
  const handleSubmit = async (values) => {
    if (imageUrl) {
      values.image = imageUrl;
    } else {
      openNotification({type: 'error', response : {
        code : codeNotif,
        message : 'Silahkan unggah ulang gambar atau muat ulang halaman dan coba lagi',
      } });
      return false;
    }

    try {
      // inserting to API
      const response = await oProvider.insert(`${resource}`, values);
      if(response){
        openNotification({type: 'success', response});
        backToList();
      }
    } catch (error) {
      const isShowMessage = errorHandling(error);
      if(!isShowMessage){
        const errorResponse = {
          code : 'Data tidak berhasil disimpan',
          message : 'Terjadi kesalahan saat proses data'
        }
        Notification({response:errorResponse, type : 'error',placement : 'topRight'});
      }
      console.log(error.message);
      return false;
    }
  };

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <FormControl
          onSubmit={handleSubmit}
          onCancel={backToList}
          imageUrl={imageUrl}
          setImageUrl={setImageUrl}
          schema={schema}
        />
      </Content>
    </React.Fragment>
  );
};

export default App;
