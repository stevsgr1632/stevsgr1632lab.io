import React from 'react';
import { Button, Form, Row, 
  Col, Space, Modal } from 'antd';
import './Form.css';
import MessageMod from 'common/message';
import oProvider from 'providers/oprovider';
import InputText from 'components/ant/InputText';
import LoadingSpin from 'components/LoadingSpin';
import InputFile from 'components/ant/InputFile3';
import InputSelect from 'components/ant/InputSelect';
import useCustomReducer from 'common/useCustomReducer';
/* eslint-disable */
const initialData = {
  loading : false,
  previewVisible : false,
  previewTitle : 'Preview',
  statusUser : [
    {
      text : 'Email belum terverifikasi',
      value : 1,
    },
    {
      text : 'Email Terverifikasi',
      value : 2,
    },
    {
      text : 'Block',
      value : 3,
    },
  ],
  organizations : [],
  role : [],
  datasubmit : {},
};

const FormBase = (props) => {
  const [form] = Form.useForm();
  const { onSubmit, onCancel, schema, imageUrl, setImageUrl, paramsId } = props;
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  const handleSubmit = () => {
    form.validateFields()
    .then((result) => {
      console.log('result',result);
      onSubmit(dataReducer.datasubmit);
    }).catch((err) => {
      err.errorFields.forEach(x => {
        MessageMod({type : 'error', text : `Kesalahan, ${x.name.toString()} : ${x.errors.toString()}`});
      });
      return false;
    });
    
  }
  const handleInputChange = (name,value,e) => {
    reducerFunc('datasubmit',{ [name] : value });
  };

  const handlePreview = () => reducerFunc('previewVisible',true,'conventional');
  const handleCancel = () => reducerFunc('previewVisible',false,'conventional');

  const handleUploadFile = (name, file, data) => {
    reducerFunc('datasubmit',{ [name] : data.file_url_download });
    setImageUrl(data.file_url_download || '');
  };

  React.useEffect(() => {
    if(dataReducer.role.length > 0){
      if (props.type === 'edit') {
        let datasubmitBaru = {...dataReducer?.datasubmit};
        const role_id = dataReducer?.role?.filter(r => r.text === datasubmitBaru?.role_name)[0] ?? '';
        if (role_id?.value) {
          datasubmitBaru.role_id = role_id.value;
        }
        reducerFunc('datasubmit',datasubmitBaru);
        form.setFieldsValue(datasubmitBaru);
      }
    }
  },[dataReducer?.datasubmit?.role_name]);

  React.useEffect(() => {
    const fetchData = async () => {
      reducerFunc('loading',false,'conventional');
      try {
        const [
          {data : roles},
          {data : organizations},
        ] = await Promise.all([
          oProvider.list('role-dropdown'),
          oProvider.list('organization-group-dropdown')
        ]);
        reducerFunc('role',roles,'conventional');
        reducerFunc('organizations',organizations,'conventional');
        if (props.type === 'edit') {
          const { data: formData } = await oProvider.list(`user-get-one?id=${paramsId}`);
          if (formData.image) setImageUrl(formData.image);
          reducerFunc('datasubmit',formData);
          form.setFieldsValue(formData);
        }
        reducerFunc('loading',false,'conventional');
      } catch (error) {
        reducerFunc('loading',false,'conventional');
        console.log(error?.message);
        return false;
      }
    }
    fetchData();
  }, []);

  return (
    <LoadingSpin loading={dataReducer?.loading}>
      <Form form={form} labelCol={{ span: 24 }}>
        <Row gutter={16}>
          <Col lg={4} md={24}>
            <InputFile name="image" accept=".png,.jpeg,.jpg" schema={schema} onChange={handleUploadFile} imgUrl={(imageUrl) ? imageUrl : ''} handlePreview={handlePreview}/>        
          </Col>
          <Col lg={20} md={24}>
            <Row gutter={16}>
              <Col lg={12} md={24}>
                <InputText name="name" schema={schema} onBlur={handleInputChange}/>
              </Col>
              <Col lg={12} md={24}>
                <InputText name="email" schema={schema} onBlur={handleInputChange}/>
              </Col>
              <Col lg={12} md={24}>
                <InputText name="phone" schema={schema} onBlur={handleInputChange}/>
              </Col>
              <Col lg={12} md={24}>
                <InputSelect name="user_status" schema={schema} onChange={handleInputChange}
                  data={dataReducer?.statusUser} placeholder="Pilih Status" others={{showSearch: true}} />
              </Col>
              <Col lg={12} md={24}>
                <InputSelect name="role_id" schema={schema}
                  data={dataReducer?.role} placeholder="Pilih level pengguna" onChange={handleInputChange} />
              </Col>
              <Col lg={12} md={24}>
                <InputSelect name="organization_id" schema={schema} onChange={handleInputChange}
                  data={dataReducer?.organizations} placeholder="Pilih Instansi" others={{showSearch: true}}/>
              </Col>
            </Row>
          </Col>
        </Row>
        <hr />
        <Space style={{float:'right'}}>
          <Button type="primary" danger onClick={onCancel}>Batal</Button>
          <Button type="primary" htmlType="button" onClick={handleSubmit}>Simpan</Button>
        </Space>
      </Form>
      <Modal
        visible={dataReducer?.previewVisible}
        title={dataReducer?.previewTitle}
        footer={null}
        centered
        onCancel={handleCancel}
        >
        <img alt="example" style={{ width: '100%' }} src={imageUrl} />
      </Modal>
    </LoadingSpin>
  );
}

export default FormBase;