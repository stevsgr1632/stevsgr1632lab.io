import React from "react";
import "../styles/modalviewdetail.scss";
import { noImagePath } from "utils/initialEndpoint";
import { Col,Modal,Row,Typography,Space } from "antd";
/* eslint-disable */

export default (props) => {
  const { dataReducer : { modalDetail :  { data,visible }} , reducerFunc } = props;
  return(
  <Modal 
    centered
    title="Detail Pengguna" 
    visible={visible} 
    onCancel={() => reducerFunc('modalDetail',{visible : false})} 
    footer={null} 
  >
    <Row gutter={[32,32]} className="modal__detail">
      <Col span={8} className="modal__detail--wrap">
        <div className="modal__detail--img">
          <img alt='logo' src={(data?.image) ? data?.image : noImagePath } width='100%'/>
        </div>
      </Col>
      <Col className="modal__detail--content" span={16}>
        <Space direction="vertical">
          <Typography.Text strong> Nama : {(data?.name) ? data?.name : '-'}</Typography.Text>
          <Typography.Text> Email : {(data?.email) ? data?.email : '-'}</Typography.Text>
          <Typography.Text> Telepon : {(data?.phone) ? data?.phone : '-'}</Typography.Text>
          <Typography.Text> Kelompok Pengguna : {(data?.role_name) ? data?.role_name : '-'}</Typography.Text>
        </Space>
      </Col>
    </Row>
  </Modal>
  )
};