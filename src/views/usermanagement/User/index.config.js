import React from "react";
import { 
  PlusOutlined,
  FileExcelOutlined 
} from '@ant-design/icons';
import regexList from "utils/regex";
import pathName from "routes/pathName";
import { Button, Dropdown, Menu } from "antd";
import { getOneMenu } from 'utils/getMenuAkses';
import ActionTableDropdown from "components/ActionTableDropdown";
/* eslint-disable */
const { initial, usermanagement } = pathName;

const actionMenu = (row, params) => {
  const [props,showConfirmDelete,viewDetail, menuAksesContext] = params;
  return (
    <Menu>
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => viewDetail(row)} text="Pratinjau" />
      </Menu.Item>
      <Menu.Item>
        { getOneMenu(menuAksesContext,'Sunting') && <>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => props.history.push(usermanagement.user.edit(row.id))} text="Sunting" />
        </>}
      </Menu.Item>
      <Menu.Item>
        { getOneMenu(menuAksesContext,'Hapus') && <>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => showConfirmDelete(row)} text="Hapus" />
        </>}
      </Menu.Item>
    </Menu>
  );
};

export default {
  indexUrl: usermanagement.user.list,
  add: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Pengguna', to: usermanagement.user.list },
      { text: 'Tambah Pengguna', to: usermanagement.user.add },
    ],
    content: {
      title: 'Tambah Pengguna',
      subtitle: 'Tambah Data Sesuai Kebutuhan dan Teliti Kembali Sebelum Menyelesaikan',
    },
  },
  edit: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Pengguna', to: usermanagement.user.list },
      { text: 'Sunting Pengguna' },
    ],
    content: {
      title: 'Pembaruan Pengguna',
      subtitle: 'Memperbarui Data Sesuai Kebutuhan dan Teliti Kembali Sebelum Menyelesaikan',
    },
  },
  model: {
    search: "",
    roleId: "",
  },
  list: {
    breadcrumb: [
      { text: "Beranda", to: initial },
      { text: "Pengguna", to: usermanagement.user.list },
    ],
    content: {
      title: "Pengguna",
      subtitle: "Lakukan pilah untuk memudahkan pencarian dan pembaruan",
      toolbars: [
        {
          text: '', type: 'primary', icon: <FileExcelOutlined />,
          onClick: (e) => {
            e.donwloadExcel();
          },
          tooltip: 'Download data'
        },
        {
          text: '', type: 'primary', icon: <PlusOutlined />,
          onClick: (e) => {
            e.history.push(usermanagement.user.add);
          }
        },
      ]
    },
    config: (...props) => {
      return {
        columns: [
          {
            title: "Nama Pengguna",
            dataIndex: "name",
            key: "name",
            sorter: true,
          },
          {
            title: "Kelompok Pengguna",
            dataIndex: "role_name",
            key: "role_name",
          },
          {
            title: "Organisasi",
            dataIndex: "organization_name",
            key: "organization_name",
          },
          {
            title: "Email",
            dataIndex: "email",
            key: "email",
          },
          {
            title: "Aksi",
            key: "operation",
            width: 80,
            className: "text-center",
            render: (row) => (
              <Dropdown overlay={actionMenu(row, props)}>
                <Button
                  type="link"
                  style={{ margin: 0 }}
                  onClick={(e) => e.preventDefault()}
                >
                  <i className="icon icon-options-vertical" />
                </Button>
              </Dropdown>
            ),
          },
        ],
      };
    },
  },
  schema: {
    image: {
      label: "Foto",
      rules: [
        { required: true, message: "Silahkan masukkan foto" }
      ]
    },
    organization_id: {
      label: "Instansi",
      rules: [
        { required: true, message: "Silahkan pilih Instansi" },
      ]
    },
    user_status: {
      label: "Status Pengguna",
      rules: [
        { required: true, message: "Silahkan pilih status pengguna" },
      ]
    },
    name: {
      label: "Nama Lengkap",
      rules: [
        { required: true, message: "Silahkan masukkan nama" },
        { min: 3, message: "Nama minimal 3 karakter" },
        { max: 100, message: "Nama maksimal 100 karakter" },
      ]
    },
    email: {
      label: "Email",
      rules: [
        { required: true, message: "Silahkan masukkan email" },
        { pattern: regexList('email') , message: "Email harus valid" },
        // () => ({
        //   validator(_, value) {
        //     if (!regexList('email').test(value)) {
        //       return Promise.reject('Masukan harus email yang valid');
        //     }
        //     return Promise.resolve();
        //   },
        // }),
      ]
    },
    phone: {
      label: "Nomor Telepon",
      rules: [
        { required: true, message: "Silahkan masukkan nomor telepon" },
        { pattern: /\d/g , message: "Nomor telepon hanya terdiri dari angka/numerik" },
        // () => ({
        //   validator(_, value) {
        //     if (new RegExp(/\D/g).test(value)) {
        //       return Promise.reject('Nomor telepon hanya terdiri dari angka/numerik');
        //     }
        //     return Promise.resolve();
        //   },
        // }),
      ]
    },
    role_id: {
      label: "Kelompok Pengguna",
      rules: [
        { required: true, message: "Silahkan Pilih Kelompok Pengguna" },
      ]
    },
  }
};
