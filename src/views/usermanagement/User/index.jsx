import React from "react";
import ListForm from "./FormList";
import config from "./index.config";
import { Col, Row, Form } from "antd";
import { connect } from 'react-redux';
import queryString from "query-string";
import pathName from 'routes/pathName';
import randomString from "utils/genString";
import oProvider from "providers/oprovider";
import getMenuAkses from "utils/getMenuAkses";
import { MessageContent } from "common/message";
import { Breadcrumb, Content } from "components";
import InputSearch from "components/ant/InputSearch";
import InputSelect from "components/ant/InputSelect";
import useCustomReducer from "common/useCustomReducer";
/* eslint-disable */

export const MenuAksesContext = React.createContext([]);
const initialData = {
  model : {
    search: "",
    roleId: "",
  },
  role : [],
  menuAkses : []
};

const App = (props) => {
  const { list } = config;
  const [form] = Form.useForm();
  const { usermanagement : { user : userPath } } = pathName;
  const [dataReducer, reducerFunc] = useCustomReducer(initialData);

  const handleChange = (name, value) => {
    reducerFunc('model',{ [name] : value });
  };

  const donwloadExcel = async () => {
    let key = randomString(17);
    MessageContent({ type :'loading', content:'Mengunduh ... ',key, duration: 0 });
    try {
      let paramsObject = {};
      Object.keys(dataReducer.model).forEach(x => {
        if(dataReducer.model[x]) {
          if(x === 'search'){
            paramsObject['role_name'] = dataReducer.model[x];
          }else {
            paramsObject[x] = dataReducer.model[x];
          }
        }
      });
      const paramsUrl = queryString.stringify(paramsObject, {skipEmptyString : true});
      const params = (paramsUrl) ? `?${paramsUrl}` : '' ;
      const response = await oProvider.downloadfile(`user-download-excel${params}`);
      if(response){
        MessageContent({ type :'success', content: 'Selesai!', key, duration: 2 });
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'daftar-pengguna-MCCP.xls'); //or any other extension
        document.body.appendChild(link);
        link.click();
      }
    } catch (error) {
      MessageContent({ type :'error', content: 'Terjadi kesalahan saat download !', key, duration: 2 });
      console.log(error?.message);
      return false;
    }
  }

  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const roleResponse = await oProvider.list("role-dropdown");
        roleResponse.data.unshift({ text: "Pilih Kelompok Pengguna", value: "" });
        reducerFunc('role',roleResponse.data,'conventional');
      } catch (error) {
        console.log(error?.message);
        return false;
      }
    };
    fetchData();
  }, []);

  React.useEffect(() => {
    if(props.menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(userPath.list);
      reducerFunc('menuAkses',menuAccessFromLocalStorage,'conventional');
    }    
  },[props.menuAccess]);

  return (
    <MenuAksesContext.Provider value={dataReducer?.menuAkses}>
      <Breadcrumb items={list.breadcrumb} />
      <Content {...{ ...props, ...list.content, menuAkses : dataReducer?.menuAkses,donwloadExcel }}>
        <Form form={form}>
          <Row gutter={16}>
            <Col sm={24} md={12}>
              <InputSelect
                name="roleId"
                text="Kelompok Pengguna"
                data={dataReducer?.role}
                onChange={handleChange}
                others={{showSearch:true}}
              />
            </Col>
            <Col sm={24} md={12}>
              <InputSearch
                name="search"
                text="Pencarian"
                placeholder="Pencarian berdasarkan nama"
                onSearch={handleChange}
                style={{ marginTop: "0.7em" }}
              />
            </Col>
          </Row>
        </Form>
        <hr />
        <ListForm {...props} model={dataReducer?.model} />
      </Content>
    </MenuAksesContext.Provider>
  );
};
const mapStateToProps = ({menuAccess}) => ({menuAccess});
export default connect(mapStateToProps)(App);
