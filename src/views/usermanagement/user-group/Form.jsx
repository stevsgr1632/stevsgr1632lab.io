import React from 'react';
import config from './index.config';
import MessageMod from 'common/message';
import oProvider from 'providers/oprovider';
import LoadingSpin from 'components/LoadingSpin';
import InputText from 'components/ant/InputText';
import { Button,Form, Row, Col,Space } from 'antd';
import InputSelect from 'components/ant/InputSelect';
import InputSwitch from 'components/ant/InputSwitch';
import useCustomReducer from 'common/useCustomReducer';
/* eslint-disable */
const initialData = {
  organizationType : [],
  dataSubmit : {},
  loading : {form : false, submit: false}
};

export default (props) => {
  const { schema } = config;
  const [form] = Form.useForm();
  const { onSubmit, onCancel, paramsId = '' } = props;
  const [dataReducer,refucerFunc] = useCustomReducer(initialData);

  const handleChange = (name,value) => {
    if(name === 'role_isactive') value = value ? 1 : 0;
    refucerFunc('dataSubmit',{ [name] : value });
  };

  const handleSimpanValid = (e) => {
    e.preventDefault();
    form.validateFields()
    .then(async values => {
      // console.log(values);
      refucerFunc('loading',{ submit : true });
      
      const role_isactive = dataReducer?.dataSubmit.role_isactive ? 1 : 0;
      const newDataSubmit = {...dataReducer?.dataSubmit, role_isactive};
      await onSubmit(newDataSubmit);

      refucerFunc('loading',{ submit : false });
    })
    .catch(errorInfo => {
      console.log(errorInfo);
      MessageMod({type : 'error', text : `Kesalahan, check form input kembali `});
      return false;
    });
  };

  React.useEffect(() => {
    const fetchData = async () => {
      refucerFunc('loading',{ form : true });
      try {
        const [
          {data : organizationType },
        ] = await Promise.all([
          oProvider.list(`organization-type-dropdown`),
        ]);
        refucerFunc('organizationType', organizationType,'conventional');
  
        if(props.type === 'edit'){
          let { data : dataedit } = await oProvider.list(`role-get-one?id=${paramsId}`);
          form.setFieldsValue(dataedit);
          refucerFunc('dataSubmit', dataedit);
        } else {
          form.setFieldsValue({role_isactive: true});
          refucerFunc('dataSubmit', {role_isactive: true});
        }
        refucerFunc('loading',{ form : false });
      } catch (error) {
        refucerFunc('loading',{ form : false });
        console.log(error?.message);
        return false; 
      }
    }
    fetchData();
  },[]);

  return(
  <LoadingSpin loading={dataReducer?.loading?.form}>
    <Form form={form}>
    <Row gutter={[16,0]}>
      <Col span={12}>
        <InputText name="role_name" schema={schema} onBlur={handleChange} />
      </Col>
      <Col span={12}>
        <InputSelect 
          name="org_type_id"
          schema={schema}
          data={dataReducer?.organizationType}
          others={{showSearch : true }}
          onChange={handleChange} 
        />
      </Col>
      <Col span={5}>
        <InputSwitch name="role_isactive" schema={schema} onChange={handleChange} />
      </Col>
    </Row>
      <Space direction="horizontal" style={{float:'right'}}>
        <Button type="primary" danger htmlType="button" onClick={onCancel}>Batal</Button>
        <Button type="primary" 
          loading={dataReducer?.loading?.submit} 
          htmlType="button" 
          onClick={handleSimpanValid}
        >{props.type === 'edit' ? 'Perbaharui' : 'Simpan'}</Button>
      </Space>
    </Form>
  </LoadingSpin>
  );
};