import React from 'react';
import FormControl from './Form';
import config from './index.config';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import { Breadcrumb, Content } from 'components';
/* eslint-disable */
const { usermanagement : { role } } = pathName;
const resource = 'role';

const FormEdit = (props) => {
  const { breadcrumb, content } = config.edit;
  const paramsId = props.match.params.id;
  const backToList = () => {
    props.history.push(role.list);
  };

  const openNotification = resp => {
    Notification({response : resp });
  };

  const handleSubmit = async (e) => {
    try {
      let response = await oProvider.update(`${resource}?id=${paramsId}`, e);
      if(response) {
        openNotification(response);
        backToList();
      }
    } catch (error) {
      console.log(error?.message);
      if(error.response){
        Notification({ type: 'error', response : {code : error.response.statusText, message:error.response.data}})
      }
      return false;
    }
  };

  return (<>
    <Breadcrumb items={breadcrumb} />
    <Content {...content}>
      <FormControl
        onSubmit={handleSubmit}
        onCancel={backToList}
        paramsId={paramsId}
        type="edit"
      />
    </Content>
  </>);
}

export default FormEdit;