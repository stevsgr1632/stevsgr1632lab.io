import React, { useState,Fragment } from 'react';
import config from './index.config';
import MessageMod from 'common/message';
import { Modal,Typography } from 'antd';
import { MenuAksesContext } from './index';
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
/* eslint-disable */
const resource = 'role';
const { confirm } = Modal;

const List = (props) => {
  const menuAksesContext = React.useContext(MenuAksesContext);
  const {model} = props;
  const [state, setState] = useState({loading : false});
  const [url, setUrl] = useState('');

  const { Text } = Typography;
  const modalDelete = (row,dataone) => {
    if(dataone?.role_access && dataone?.role_access?.length){
      confirm({
        title: 'Hapus User Role',
        content: <Fragment>
        <Text>Anda yakin ingin menghapus data ini (<b>{row.role_name}</b>) dan semua data terkait?</Text>
        <br />
        <Text type="danger">Tindakan ini tidak bisa dibatalkan</Text>
      </Fragment>,
        onOk() {
          return new Promise(async (resolve, reject) => {
            await oProvider.delete(`${resource}?id=${row.id}`);
            refresh(url, model);
            setTimeout(Math.random() > 0.5 ? resolve() : reject(), 1000);
          }).catch(() => console.log('Oops errors!'));
        },
        onCancel() {},
      });
    }else{
      return MessageMod({type: 'error', text : 'Maaf Anda tidak bisa menghapus data user role yang sudah ada akses'});
    }
  }

  const refresh = async (url,model) => {
    setState(prev => ({...prev,loading : true}));
    if (model.search) {
      let paramsObject = {};
      if (model.search) paramsObject['role_name'] = model.search;
      url += (url ? '&' : '') + new URLSearchParams(paramsObject).toString();
      console.log(url);
    }
    try {
      const { meta, data } = await oProvider.list(`${resource}${url ? '?' : ''}${url}`);
      setState({ loading : false, pagination: { pageSize: meta.limit, total: parseInt(meta.total) }, dataSource: data });
    } catch (error) {
      setState(prev => ({ ...prev, loading : false }));
      console.log(error?.message);
      return false;
    }
  }

  const handleTableChange = (pagination, filters, sorter) => {
    let paramsObject = {};
    let url = '';
    if (pagination) url += `&page=${pagination.current}`;
    if (sorter && sorter.field) {
      url += `&sortBy=${sorter.field}`;
      url += `&sortDir=${sorter.order === 'ascend' ? 'asc' : 'desc'}`;
    }
    if (model.search) paramsObject['role_name'] = model.search;
    setUrl(url);
  }

  const modalDetail = async row => {
    console.log(row);
  }

  React.useEffect(() => {
    refresh(url,model);
  }, [url,Object.values(model).join()]);

  return (
    <React.Fragment>
      <MocoTable
        {...config.list.config(props,modalDelete,modalDetail,menuAksesContext)}
        {...state}
        onChange={handleTableChange}
      />
    </React.Fragment>
  );
}

export default List;