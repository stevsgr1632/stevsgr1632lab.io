import React from 'react';
import { Collapse,List,Switch } from 'antd';
import { CloseOutlined, CheckOutlined,
  SaveOutlined 
} from '@ant-design/icons';
import './style/buttonfloat.style.scss';
import pathName from 'routes/pathName';
import MessageComp from 'common/message';
import oProvider from 'providers/oprovider';
import {Breadcrumb,Content} from 'components';
import ButtonFloat from './component/ButtonFloat';
/* eslint-disable */

const { initial, usermanagement } = pathName;

const RoleAkses = props => {
  const [state, setState] = React.useState({});
  const [dataSubmit, setdataSubmit] = React.useState([]);
  const [btnstate,setbtnstate] = React.useState({loading : false, bounce : false});
  const getParams = props.match.params.id;

  const roleAkses = (...params) => {
    return {
      breadcrumb: [
        { text: 'Beranda', to: initial },
        { text: 'Kelompok Pengguna', to: usermanagement.role.list },
        { text: `Pembaharuan Kelompok Pengguna: ${state?.role_name ?? ''}` },
      ],
      content: {
        title: `Pembaharuan Kelompok Pengguna: ${state?.role_name ?? ''}`,
        subtitle: `Ubah Data Sesuai Kebutuhan Dan Teliti Kembali Sebelum Menyelesaikan`,
      },
    }
  };

  const handleCallback = (key) => {
    // console.log(key);
  }
  
  const handleClickFloat = async e => {
    e.preventDefault();
    setbtnstate(prev => ({...prev, loading : true}));
    try {
      let resp = await oProvider.insertCustom('role-access', dataSubmit);
      MessageComp({type:'success',text : resp.message });
      setbtnstate({ loading: false, bounce : false});
    } catch (error) {
      MessageComp({type:'error',text : error.message.substr(0,50) });
      setbtnstate({ loading: false, bounce : false});
      return false;
    }
  };

  const handleChangeSwitch = (...params) => {
    const [oneMenu,item,checked,children = null,event] = params;
    let newAccess = dataSubmit.map(x => {
      // checking menu_id for one parent akses 
      if(x.menu_id === oneMenu.menu_id){
        // checking there children obj
        if(x.children){
          x.children.map(child => {
            // checking menu_id for one children akses 
            if(child.menu_id === children.menu_id){
              child.menu_access.map(y => {
                // checking access_name for 1 item access_name
                if(y.access_name === item.access_name){
                  y.access_role = checked;
                }
              });
            }
          });
        }else{
          x.menu_access.map(y => {
            if(y.access_name === item.access_name){
              y.access_role = checked;
            }
          });
        }
      }
      return x;
    });

    let newAccessPayload = newAccess.map(x => {
      return {...x,role_id : state?.id};
    });

    setdataSubmit(newAccessPayload);
    setbtnstate(prev => ({...prev, bounce : true}));
    setState(prev => ({...prev,role_access: newAccess }));
  }
  
  React.useEffect(() => {
    const fetchData = async () => {
      try {
        let {data} = await oProvider.list(`role-get-one?id=${getParams}`);
        setdataSubmit(data.role_access);
        setState(prev => ({...prev,...data}));
      } catch (error) {
        console.log(error?.message);
        return false;
      }
    }
    fetchData();
  },[]);
  
  React.useEffect(() => {
    console.log(state);
  },[state]);  

  return(<>
    <Breadcrumb items={roleAkses().breadcrumb} />
    <Content {...roleAkses().content}>
      <Collapse onChange={handleCallback}>
        {state?.role_access?.length && state?.role_access.map((x,idx) => {
          return(
          <Collapse.Panel header={<React.Fragment key={idx}>
            <h6 style={{margin:0}}>
              <b>{x.menu_name}</b>
            </h6>
            <small>{x.menu_description}</small>
          </React.Fragment>} key={idx}>
            {x.children ? x.children.map((child,index) => {
              return(<React.Fragment key={index}>
                <Collapse onChange={handleCallback}>
                  <Collapse.Panel header={<React.Fragment key={index}>
                      <h6 style={{margin:0}}>
                        <b>{child.menu_name}</b>
                      </h6>
                    <small>{child.menu_description}</small>
                    </React.Fragment>} key={index}>
                    <List
                      itemLayout="horizontal"
                      dataSource={child.menu_access}
                      renderItem={item => (
                        <List.Item>
                          <List.Item.Meta title={<a href="https://aksaramaya.com">{item.access_text}</a>} />
                          <Switch
                            checkedChildren={<CheckOutlined />}
                            unCheckedChildren={<CloseOutlined />}
                            defaultChecked={item.access_role}
                            onChange={(checked,e) => handleChangeSwitch(x,item,checked,child,e)}
                          />
                        </List.Item>
                      )}
                    />
                  </Collapse.Panel>
                </Collapse>
                <br />
              </React.Fragment>)
            }) : 
            <List
              itemLayout="horizontal"
              dataSource={x.menu_access}
              renderItem={item => (
                <List.Item>
                  <List.Item.Meta title={<a href="https://aksaramaya.com">{item.access_text}</a>} />
                  <Switch
                    checkedChildren={<CheckOutlined />}
                    unCheckedChildren={<CloseOutlined />}
                    defaultChecked={item.access_role}
                    onChange={(checked,e) => handleChangeSwitch(x,item,checked,e)}
                  />
                </List.Item>
              )}
            />}
          </Collapse.Panel>
          );
        })}
      </Collapse>
      <ButtonFloat text="Simpan" onClick={handleClickFloat} options={{
          style: {
            borderColor:'transparent',
            position:'fixed',
            right:'4em',
            bottom:'4em',
            boxShadow: `0 3px 6px -4px rgba(0,0,0,.12),0 6px 16px 0 rgba(0,0,0,.08),0 9px 28px 8px rgba(0,0,0,.05)`,
            animation: btnstate.bounce ? 'bounce .3s infinite alternate' : '',
            backgroundColor : btnstate.bounce ? 'rgb(77, 189, 116)' : '#1890ff',
          },
          shape : "round",
          className : 'buttonfloat',
          icon : <SaveOutlined/>,
          loading : btnstate.loading
        }} 
      />
    </Content>
  </>);
};

export default RoleAkses;