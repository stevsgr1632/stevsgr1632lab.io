import React from 'react';
import {Button} from 'antd';
/* eslint-disable */

const ButtonFloat = props => {
  const { options = {}, text = '',onClick } = props;
  const optionDefault = {
    type: 'primary',
    size: 'large',
    onClick,
    ...options
  }

  return (<>
    <Button {...optionDefault}>
      {text}
    </Button>
  </>);
};

export default ButtonFloat;