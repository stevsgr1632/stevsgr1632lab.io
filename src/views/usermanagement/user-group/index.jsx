import React from 'react';
import ListForm from './FormList';
import config from './index.config';
import { Col, Row,Form } from 'antd';
import { connect } from 'react-redux';
import pathName from 'routes/pathName';
import queryString from "query-string";
import randomString from 'utils/genString';
import oProvider from 'providers/oprovider';
import getMenuAkses from "utils/getMenuAkses";
import { Breadcrumb,Content } from 'components';
import { MessageContent } from 'common/message';
import InputSearch from 'components/ant/InputSearch';
/* eslint-disable */
export const MenuAksesContext = React.createContext([]);

const App = (props) => {
  const { usermanagement : { role : rolePath } } = pathName;
  const { list } = config;
  const [form] = Form.useForm();
  const [model, setModel] = React.useState(config.model);
  const [menuAkses, setmenuAkses] = React.useState([]);

  const handleChangeSearch = (name, value) => {
    setModel(prev => ({ ...prev, [name]: value }));
  }

  const donwloadExcel = async () => {
    let key = randomString(17);
    MessageContent({ type :'loading', content:'Mengunduh ... ',key, duration: 0 });
    try {
      let paramsObject = {};
      Object.keys(model).forEach(x => {
        if(model[x]) {
          if(x === 'search'){
            paramsObject['role_name'] = model[x];
          }else {
            paramsObject[x] = model[x];
          }
        }
      });
      const paramsUrl = queryString.stringify(paramsObject, {skipEmptyString : true});
      const params = (paramsUrl) ? `?${paramsUrl}` : '' ;
      const response = await oProvider.downloadfile(`role-download-excel${params}`);
      if(response){
        MessageContent({ type :'success', content: 'Selesai!', key, duration: 2 });
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'kelompok-pengguna-MCCP.xls'); //or any other extension
        document.body.appendChild(link);
        link.click();
      }
    } catch (error) {
      MessageContent({ type :'error', content: 'Terjadi kesalahan saat download !', key, duration: 2 });
      console.log(error?.message);
      return false;
    }
  }

  React.useEffect(() => {
    if(props.menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(rolePath.list);
      setmenuAkses(menuAccessFromLocalStorage);
    }    
  },[props.menuAccess]);

  return (
    <React.Fragment>
    <MenuAksesContext.Provider value={menuAkses}>
      <Breadcrumb items={list.breadcrumb} />
      <Content {...{...props,...list.content, menuAkses,donwloadExcel}}>
        <Form form={form}>
        <Row gutter={16} style={{marginTop:8}}>          
          <Col span={24} style={{marginTop:'0.6em'}}>
            <InputSearch name="search" text="Pencarian" placeholder="Pencarian" onSearch={handleChangeSearch} />
          </Col>
        </Row>
        </Form>
        <hr />
        <ListForm {...props} model={model} />
      </Content>
    </MenuAksesContext.Provider>
    </React.Fragment>
  );
};
const mapStateToProps = ({menuAccess}) => ({menuAccess});
export default connect(mapStateToProps)(App);