import React from 'react';
import { Button,Menu, Dropdown } from 'antd';
import {
  PlusOutlined,
  FileExcelOutlined
} from '@ant-design/icons';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import { getOneMenu } from 'utils/getMenuAkses';
import ActionTableDropdown from "components/ActionTableDropdown";
/* eslint-disable */
const { initial,usermanagement } = pathName;
const actionMenu = (row, params) => {
  const [props,modalDelete,modalDetail,menuAksesContext] = params;
  const { history } = props; // props == Object { history: {…}, location: {…}, match: {…}, staticContext: undefined, model: {…} }

    const sunting = async (type) => {
      switch (type){
        case "first":
          history.push(usermanagement.role.edit(row.id));
          break;
        case "second":
          const fetchData = async () => {
            const { data } = await oProvider.list(`role-get-one?id=${row.id}`);
            modalDelete(row,data);
          };
          fetchData();
          break;
        case "third":
          history.push(usermanagement.role.roleAkses(row.id));
          break;
        case "detail":
          modalDetail(row);
          break;
        default:
          props.viewDetail(row);
      }
    };
  
    return (
      <Menu>
        <Menu.Item>
          <Button type="link" style={{ margin: 0 }} onClick={() => modalDetail(row)}>Detail</Button>
        </Menu.Item>
        <Menu.Item>
          <Button type="link" style={{ margin: 0 }} onClick={() => sunting('third')}>Role Akses</Button>
        </Menu.Item>
        <Menu.Item>
          { getOneMenu(menuAksesContext,'Sunting') && <>
            <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => sunting('first')} text="Sunting" />
          </>}
        </Menu.Item>
        <Menu.Item>
          { getOneMenu(menuAksesContext,'Hapus') && <>
            <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => sunting('second')} text="Hapus" />
          </>}
        </Menu.Item>
      </Menu>
    )
  };
export default {
  list: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Kelompok Pengguna', to: usermanagement.role.list },
    ],
    content: {
      title: 'Kelompok Pengguna',
      subtitle:'Lakukan pilah untuk memudahkan pencarian dan pembaruan',
      toolbars: [
        {
          text: '', type: 'primary', icon: <FileExcelOutlined/>,
          onClick: (e) => {
            e.donwloadExcel();
          },
          tooltip: 'Download data'
        },
        {
          text: '', type: 'primary', icon: <PlusOutlined/>,
          onClick: (e) => {
            e.history.push(usermanagement.role.add);
          }
        }
      ]
    },
    config: (...props) => {
      return {
        columns: [
          {
            title: 'Nama Kelompok Pengguna',
            dataIndex: 'role_name',
            key: 'role_name',
            render: x => x ? x : '-'
          },
          {
            title: 'Jenis Instansi',
            width: 200,
            dataIndex: 'org_type_name',
            key: 'org_type_name',
            render: x => x ? x : '-'
          },
          {
            title: 'Jumlah Pengguna',
            width: 120,
            dataIndex: 'role_qty',
            key: 'role_qty',
            align: 'right',
            render: x => x ? x : '-'
          },
          {
            title: 'Aksi',
            key: 'aksi',
            width: 80,
            className: 'text-center',
            render: (row) => (
              <span>
                <Dropdown overlay={actionMenu(row, props)}>
                  <Button type="link" style={{ margin: 0 }} onClick={e => e.preventDefault()}>
                    <i className="icon icon-options-vertical" />
                  </Button>
                </Dropdown>
              </span>
            ),
          },
        ]
      }
    }
  },
  add: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Roles', to: usermanagement.role.list },
      { text: 'Tambah Kelompok Pengguna' },
    ],
    content: {
      title: 'Tambah Kelompok Pengguna',
    },
  },
  edit: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Roles', to: usermanagement.role.list },
      { text: 'Ubah Kelompok Pengguna' },
    ],
    content: {
      title: 'Perbaruan Kelompok Pengguna',
    },
  },
  schema: {
    role_name : {
      label: 'Nama Kelompok Pengguna',
      rules: [
        { required: true, message: 'Silahkan input nama kelompok pengguna' },
      ]
    },
    role_isactive : {
      label: 'Status Aktif',
    },
    org_type_id : {
      label: 'Jenis',
      rules: [
        { required: true, message: 'Silahkan pilih jenis institusi' },
      ]
    },
  },
  model: {
    status: '',
    search: ''
  },
};