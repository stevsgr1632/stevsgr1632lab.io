import React from 'react';
import FormControl from './Form';
import config from './index.config';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import { Breadcrumb, Content } from 'components';
/* eslint-disable */
const { usermanagement : { role } } = pathName;
const resource = 'role';

const FormAdd = (props) => {
  const { breadcrumb, content } = config.add;

  const backToList = () => {
    props.history.push(role.list);
  };

  const openNotification = resp => {
    Notification({response : resp });
  };

  const handleSubmit = async (e) => {
    try {
      let response = await oProvider.insert(`${resource}`, e);
      if(response) {
        openNotification(response);
        backToList();
      }
    } catch (error) {
      console.log(error?.message);
      return false;
    }
  };

  return (<>
    <Breadcrumb items={breadcrumb} />
    <Content {...content}>
      <FormControl
        onSubmit={handleSubmit}
        onCancel={backToList}
        type="add"
      />
    </Content>
  </>);
}

export default FormAdd;