import React from 'react';
/* eslint-disable */

const App = (props) => {

  return (
    <div onClick={props.onClick} style={{display:'grid',alignContent:'center',backgroundColor:'ghostwhite',border:'1px dashed gray', width:'100%',height:'200px',cursor:'pointer'}}> 
      <center><i className={props.icon}></i><br/> {props.description}</center>
    </div>
  );
}

export default App;