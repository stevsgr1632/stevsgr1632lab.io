import React from 'react';
import {Row,Col,Divider} from 'antd';
import InputText from '../../../../../components/ant/InputText';
/* eslint-disable */

const App = props => {
	let { handleChangeInput,schema } = props;

	return(<>
	<br/><br/>
  <Divider orientation="left"><strong>Detail Kriteria</strong></Divider>
  <Row gutter={[16,16]}>
    <Col span={12}>
      <InputText type="number" name="budget_number_of_title" min="0" placeholder="Masukkan Jumlah Judul Buku" onChange={handleChangeInput} schema={schema} />
    </Col>
    <Col span={12}>
      <InputText type="number" name="budget_number_of_copy" min="0" placeholder="Masukkan Jumlah Salinan Buku" onChange={handleChangeInput} schema={schema} />
    </Col>
    <Col span={12}>
      <InputText type="number" name="budget_min_price" min="0" placeholder="Masukkan Harga Minimal" onChange={handleChangeInput} schema={schema} />
    </Col>
    <Col span={12}>
      <InputText type="number" name="budget_max_price" min="0" placeholder="Masukkan Harga Maximal" onChange={handleChangeInput} schema={schema} />
    </Col>
  </Row>
	</>);
}

export default App;