import React from 'react';
import { Modal } from 'antd';
/* eslint-disable */

const App = (props) => {
  const [data,setData] = React.useState(props.data.filter(x=> props.selected.includes(x.value)).map(y => y.text));

  const info = params => {
    Modal.info({
      centered:true,
      title: 'Semua Jenis Produk',
      content: (
        <dl style={{overflowY:'scroll',height:'30vh'}}>
        {
          params.map(x => (
            <dd key={x}>{x}</dd>
          ))
        }
        </dl>
      ),
      onOk() {},
    });
  }

  React.useEffect(() => {
    let storeData = props.data.filter(x=> props.selected.includes(x.value)).map(y => y.text);
    setData(storeData);
  },[props]);

  return (<>
    {(props.selected && props.selected.length < 4) ? (
    <p>{props.data.filter(x=> props.selected.includes(x.value)).map(y => y.text).join(', ')}</p>
    ) : (
      <p>
        {props.data.filter(x=> props.selected.slice(0,4).includes(x.value)).map(y => y.text).join(', ')} 
        <a className="text-primary" href={process.env.REACT_APP_BASE_URL} key="list-loadmore-more" onClick={() => info(data)}>...more</a>
      </p>
      ) 
    }
  </>);
}

export default React.memo(App);