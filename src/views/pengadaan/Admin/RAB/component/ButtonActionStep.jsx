import React from 'react';
import { Button } from 'antd';
/* eslint-disable */

const App = props => {
	const { steps, current, prev, next, otherSubmit, dataSubmit = {} } = props;

	return (
		<div style={{ display: 'flex', justifyContent: 'flex-end' }}>
			{current > 0 && (
				<Button style={{ margin: 8 }} onClick={() => prev()}>Sebelumnya</Button>
			)}

			{current < steps.length - 1 && (
				<Button style={{ margin: 8 }} type="primary" onClick={() => next()}>Berikut</Button>
			)}
			{current === steps.length - 1 && (
				<Button style={{ margin: 8 }} htmlType="button" type="primary" onClick={() => otherSubmit(dataSubmit)}>Save</Button>
			)}
		</div>
	);
}

export default App;