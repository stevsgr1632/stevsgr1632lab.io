import React from 'react';
import { numberFormat } from "utils/initialEndpoint";
/* eslint-disable */

const App = props => {
	const {dataSubmit,budgetStatus,typeOrg,fiscalYear,sourceFund,jenisProduk,kategori,publisher} = props;

	return (<>
		<br/>
    <h5>Detail Pengadaan</h5>
    {dataSubmit.budget_name && (
      <p>Pesanan : {dataSubmit.budget_name}</p>
    )}
    {dataSubmit.budget_status && (
      <p>Status : {budgetStatus.find(x => x.value === dataSubmit.budget_status).text}</p>
    )}
    {dataSubmit.budget_name && (
      <p>Nama Pengadaan : {dataSubmit.budget_name}</p>
    )}
    {dataSubmit.budget_name && (
      <p>Di proses untuk iLibrary : {dataSubmit.budget_name}</p>
    )}
    {dataSubmit.budget_name && (
      <p>Di proses untuk iLibrary : {dataSubmit.budget_name}</p>
    )}
    {dataSubmit.procurement_type_id && (
      <p>Jenis Pengadaan : {typeOrg.find(x => x.id === dataSubmit.procurement_type_id).text }</p>
    )}
    {dataSubmit.fiscal_year_id && (
      <p>Tahun Anggaran : {fiscalYear.find(x => x.value === dataSubmit.fiscal_year_id).text}</p>
    )}
    {dataSubmit.source_fund_id && (
      <p>Sumber Dana : {sourceFund.find(x => x.value === dataSubmit.source_fund_id).text}</p>
    )}
    {dataSubmit.budget_amount && (
      <p>Dana Anggaran : Rp. {numberFormat(dataSubmit.budget_amount)}</p>
    )}
    {dataSubmit.budget_start_date && (
      <p>Tanggal Mulai Pengadaan : {dataSubmit.budget_start_date}</p>
    )}
    {dataSubmit.budget_end_date && (
      <p>Batas Akhir Pengadaan : {dataSubmit.budget_end_date}</p>
    )}
    
    {dataSubmit.budget_content_media && (<>
      <br/>
      <h5>Jenis Produk dan Kategori</h5>
      <p>Jenis Produk : {jenisProduk.filter(x=> dataSubmit.budget_content_media.includes(x.id)).map(y => y.text).join(', ')}</p>
    </>)}
    {dataSubmit.books_category && (
      <p>Kategori : {kategori.filter(x=> dataSubmit.books_category.includes(x.value)).map(y => y.text).join(', ')}</p>
    )}
    
    {dataSubmit.budget_number_of_title && (<>
      <br/>
      <h5>Jumlah &amp; Harga</h5>
      <p>Judul Buku : {dataSubmit.budget_number_of_title}</p>
    </>)}
    {dataSubmit.budget_number_of_copy && (
      <p>Salinan Per Judul : {dataSubmit.budget_number_of_copy}</p>
    )}
    {dataSubmit.budget_max_price && (
      <p>Harga Tertinggi : {numberFormat(dataSubmit.budget_max_price)}</p>
    )}
    {dataSubmit.budget_min_price && (
      <p>Harga Terendah : {numberFormat(dataSubmit.budget_min_price)}</p>
    )}
    
    {dataSubmit.budget_publishers && (<>
      <br/>
      <h5>Penyedia Produk</h5>
      <p>Penerbit : {publisher.filter(x => dataSubmit.budget_publishers.includes(x.value)).map(y => y.text).join(', ')}</p>
    </>)}

    {dataSubmit.budget_name && (<>
      <br/>
      <h5>Detail Rencana Pelaksanaan</h5>
      <p>Persiapaan  : {dataSubmit.budget_name}</p>
    </>)}

	</>);
}

export default App