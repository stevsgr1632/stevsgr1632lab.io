import React from 'react';
import {Row,Col,List,Avatar} from 'antd';
// import AddNew from './AddNew.jsx';
import '../styles/perusahaan.style.scss';
// const { Meta } = Card;
/* eslint-disable */

const App = ({dataSource,dataPub}) => {
  const handleAddNew = e => {
    e.preventDefault(); 
    console.log('variabel e : ',e);
    console.log('variabel dataPub : ',dataPub);
  }

  return (
    <>
      <Row gutter={[16,16]}>
        {dataPub.length > 0 && (<>
          <Col>
            <h4>List Daftar Penerbit</h4>
            <List
              itemLayout="horizontal"
              dataSource={dataPub}
              renderItem={item => (
                <List.Item
                  actions={[
                    <a className="text-info" key="list-loadmore-edit">edit</a>, 
                    <a className="text-primary" key="list-loadmore-more">more</a>
                  ]}
                >
                  <List.Item.Meta
                    avatar={<Avatar src={item.organization_logo} />}
                    title={<a href="https://ant.design">{item.organization_name}</a>}
                    description={item.organization_email}
                  />
                </List.Item>
              )}
            />
            </Col>
            {/* {dataPub.map(x => {
              return (
                <Col span={4}>
                  <Card
                    title={<Button type="primary" size="small">Berjalan</Button>}
                    hoverable
                    cover={<img alt="example" src={x.organization_logo} />}
                    extra={<a href="https://www.google.com">More</a>}
                  >
                    <Meta title={x.organization_name} description={<><i className="icon-user"></i> <span>{x.organization_email}</span></>} />
                  </Card>
                </Col>
              );
            }) 
            } */}
          </>)}
        
        {/* <Col span={4}>
          <AddNew onClick={handleAddNew} description='Buat pengadaan baru' icon='icon-plus' />
        </Col> */}


      </Row>
    </>
  )
}

export default App;