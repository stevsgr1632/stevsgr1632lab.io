import React, { useState } from 'react';
import moment from 'moment';
import { Row, Col, Input, DatePicker } from 'antd';
/* eslint-disable */

const dateFormat = 'YYYY-MM-DD';

const App = (plan) => {
    const defaultValue = ['', ''];
    const days = moment(plan.budget_plan_end_date).diff(moment(plan.budget_plan_start_date), 'days');
    const [model, setModel] = useState({ days: plan.budget_plan_start_date ? `${days} Hari` : '-' });

    if (plan.budget_plan_start_date) defaultValue[0] = moment(plan.budget_plan_start_date);
    if (plan.budget_plan_end_date) defaultValue[1] = moment(plan.budget_plan_end_date);

    return (
        <div style={{ marginBottom: 15 }}>
            <Row gutter={12}>
                <Col span={18}>
                    <div style={{ fontSize: 14 }}>{plan.budget_plan_name}</div>
                    <DatePicker.RangePicker
                        style={{ width: '100%' }}
                        defaultValue={defaultValue}
                        format={dateFormat}
                        onChange={(date, dateString) => {
                            const days = moment(dateString[1], dateFormat).diff(moment(dateString[0], dateFormat), 'days');
                            setModel({ ...model, days: `${days} Hari` });
                            console.log(`${days} Hari`);
                        }}
                        onPanelChange={(value, mdoe) => {
                            console.log(value, mdoe)
                        }}
                    />
                </Col>
                <Col span={6}>
                    <div style={{ fontSize: 14 }}>Total Hari</div>
                    <Input value={model.days} disabled />
                </Col>
            </Row>
        </div >
    );
}

export default App;