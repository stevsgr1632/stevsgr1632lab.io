import React from 'react';
import Icon from '@ant-design/icons';
import { Tabs, Button, Popconfirm, message } from 'antd';
import MocoTable from '../../../../../components/MocoTable';
import messageConfig from './config';
/* eslint-disable */

const { TabPane } = Tabs;
message.config(messageConfig.message);

const App = props => {
  const { state, columns, loading, handleConfirm, selectedRowKeys, rowSelection, handleTableChange, handleChangeTabs, tabKey } = props;
  return (<>
    <Tabs type="card" defaultActiveKey={`${tabKey}`} onChange={handleChangeTabs}>
      <TabPane tab="HASIL SELEKSI" key="1">
        {selectedRowKeys.length > 0 && (
          <Popconfirm
            title={<p>Apakah Anda yakin <b>menyetujui</b> katalog?</p>}
            icon={<Icon type="question-circle-o" style={{ color: 'blue' }} />}
            onConfirm={handleConfirm}
            onCancel={(e) => message.error('Gagal menyetujui !')}
            okText="Yes"
            cancelText="No"
          >
            <Button style={{ float: 'right', zIndex: 1 }} type="primary" onClick={e => e.preventDefault()} disabled={selectedRowKeys.length > 0 ? false : true} loading={loading}>
              <span>Setujui <i className="icon-check"></i> </span>
            </Button>
          </Popconfirm>
        )}
        <MocoTable
          columns={columns}
          rowSelection={rowSelection}
          {...state}
          onChange={handleTableChange}
        />
      </TabPane>
      <TabPane tab="DISETUJUI" key="2">
        <MocoTable
          columns={columns}
          {...state}
          onChange={handleTableChange}
        />
      </TabPane>
    </Tabs>
  </>);
}

export default App;