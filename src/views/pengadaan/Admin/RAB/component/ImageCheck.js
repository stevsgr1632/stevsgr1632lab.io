import React, { useState, useEffect } from 'react';
import { Row, Col } from 'antd';

import '../styles/ImageCheck.scss';
/* eslint-disable */

const App = (props) => {
  // console.log(props.items);
  const [state, setState] = useState({
    items: props.items.map((m, idx) => ({ value: m.id, ...m }))
  })

  const setActive = (idx) => {
    const { items } = state;
    if (props.type === 'radio') {
      items.forEach((item, id) => {
        item.active = idx === id;
        if (props.onChange) props.onChange(items[idx].value);
      });
    } else {
      items[idx].active = items[idx].active ? false : true;
      if (props.onChange) props.onChange(items.filter(m => m.active).map(m => m.value));
    }

    setState({ ...state, items });
  }

  useEffect(() => {
    if (props.defaultValue) {
      const { items } = state;
      items.forEach((item) => {
        item.active = props.defaultValue.indexOf(item.value) >= 0;
      });
      setState({ ...state, items });
    }
  }, []);

  return (
    <div className="checkbox-image">
      <Row gutter={16}>
        {state.items.map((m, idx) => (
          <Col sm={4} style={{ padding: 10 }} key={idx}>
            <div className={`image-box ${m.active ? 'active' : ''}`} onClick={() => setActive(idx)}>
              <img src={m.src || 'https://via.placeholder.com/100'} alt={'no-data'} width={'60%'} />
              <div style={{ height: 40, fontSize: 12, marginTop: 5 }}>{m.text}</div>
            </div>
          </Col>
        ))}
      </Row>
    </div >
  );
}

export default App;