import React, { useEffect, useState } from 'react';
import { Modal } from 'antd';
import config from './index.config';
import { MenuAksesContext } from './index';
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
import {DeleteOutlined} from '@ant-design/icons';
/* eslint-disable */

const { confirm } = Modal;

const List = (props) => {
  const menuAksesContext = React.useContext(MenuAksesContext);
  const {model} = props;
  const resource = 'budget-execution?limit=5&budgetStatus='+ model.type.toUpperCase();
  const [state, setState] = useState({});
  const [url, setUrl] = useState('');

  const showConfirm = (props) => {
  
    confirm({
      title: 'Do you want to delete these items?',
      icon: <DeleteOutlined/>,
      content: 'When clicked the OK button, this dialog will be closed',
      onOk() {
        return new Promise(async (resolve, reject) => {
          await oProvider.delete(`budget-execution?id=${props.id}`);
          refresh('',{status:'',search:''});
          // await oProvider.list(`${resource}?limit=5`);
          setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {},
    });
  }

  const refresh = async (url,model) => {
    if(model.fiscalyear !== ''){
      url += `&fiscal_year_name=${model.fiscalyear}`;
    }else{
      url += ``;
    }
    // console.log(model)
    if(model.search !== ''){
      url += `&budget_name=${model.search}`;
    }else{
      url += ``;
    }
    const { meta, data } = await oProvider.list(`${resource}${url}`);
    setState({ pagination: { pageSize:5,total: parseInt(meta.total) }, dataSource: data })
  }

  const viewDetail = (row) => {
    console.log(row);
  }

  const handleTableChange = (pagination, filters, sorter) => {
    let url = '';
    if (pagination) url += `&page=${pagination.current}`;
    if (sorter && sorter.field) {
      url += `&sortBy=${sorter.field}`;
      url += `&sortDir=${sorter.order === 'ascend' ? 'asc' : 'desc'}`;
    }
    setUrl(url);
  }

  useEffect(() => {
    if(model.type !== '') refresh(url,model);
  }, [url,model]);

  // {...config.list.config(props,showConfirm)}
  return (
    <React.Fragment>
      <MocoTable
        columns={config.list.config({...props,viewDetail,menuAksesContext}).columns}
        {...state}
        onChange={handleTableChange}
      />
    </React.Fragment>
  );
}

export default List;