import React, { useState } from 'react';
import ListForm from './FormList';
import config from './index.config';
import { Col, Row,Form } from 'antd';
import storeRedux from 'redux/store';
import pathName from 'routes/pathName';
import getMenuAkses from "utils/getMenuAkses";
import { Breadcrumb, Content } from 'components';
import {newDropDown} from 'providers/dropdownapi';
import InputSelect from 'components/ant/InputSelect';
import InputSearch from 'components/ant/InputSearch';
/* eslint-disable */

export const MenuAksesContext = React.createContext([]);

export default (props) => {
  const [form] = Form.useForm();
  const { rab : { admin : adminPath} } = pathName;
  const [fiscalYear, setfiscalYear] = useState([]);
  const { list } = config;
  const [model, setModel] = useState(config.model);
  const [menuAkses, setmenuAkses] = React.useState([]);

  const handleChange = (name, value) => {
    setModel({ ...model, [name]: value });
  }
  const handleSubmit = values => {
    console.log('Success:', values);
  }

  React.useEffect(() => {
    async function fetchData(){
      const data = await newDropDown(`fiscal-year-dropdown`);
      data.unshift({text:'Pilih Tahun Anggaran',value:''});
      setfiscalYear(data);
    }
    fetchData();
    setModel({ ...model, type: 'planning' });
  },[]);

  React.useEffect(() => {
    if(storeRedux.getState().menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(adminPath.list);
      setmenuAkses(menuAccessFromLocalStorage);
    }    
  },[storeRedux.getState().menuAccess]);

  return (
    <React.Fragment>
    <MenuAksesContext.Provider value={menuAkses}>
      <Breadcrumb items={list.breadcrumb} />
      <Content {...{...props,...list.planning, menuAkses}}>
        <Form form={form} onFinish={handleSubmit} >
          <Row gutter={16}>
            <Col span={8}>
              <InputSelect name="fiscalyear" defaultVal={model.fiscalyear || ''} text="Tahun Anggaran" data={fiscalYear} onChange={handleChange} />
            </Col>
            <Col span={8} style={{marginTop:'0.6em'}}>
              <InputSearch name="search" text="Pencarian" placeholder="Cari Berdasarkan Nama" onSearch={handleChange} />
            </Col>
          </Row>
        </Form>
        <hr />
        <ListForm {...props} model={model} />
      </Content>
    </MenuAksesContext.Provider>
    </React.Fragment>
  );
};