import React from 'react';
import { Row, Col, Button, Menu, Dropdown,message} from 'antd';
import moment from 'moment';

import config from './index.config';
import pathName from 'routes/pathName';
import Lelang from './component/Lelang';
import PopupAdd from './component/Modal';
import oProvider from 'providers/oprovider';
import Loading from 'components/LoadingPage';
import Perusahaan from './component/Perusahaan';
import { Breadcrumb, Content } from 'components';
import LoadingSpin from 'components/LoadingSpin';
import {newDropDown} from 'providers/dropdownapi';
import { numberFormat } from "utils/initialEndpoint";
import KategoriSelected from './component/KategoriSelected';

/* eslint-disable */
message.config({
  top: 100,
  duration: 2,
  maxCount: 3,
  rtl: true,
});

const {rab:{dinas}} = pathName;
const resource = 'budget-get-info';
const App = (props) => {
  const { breadcrumb, content } = config.detail;
  const passId = props.match.params.id;
  
  const [state,setState] = React.useState({});
  const [dataRow,setDataRow] = React.useState({});
  const [kategori,setkategori] = React.useState([]);
  const [budgetStatus,setbudgetStatus] = React.useState([]);
  const [dataPub,setdataPub] = React.useState([]);
  const [dataTeam,setdataTeam] = React.useState([]);
  const [loading,setloading] = React.useState(false);
  const [visible, setvisible] = React.useState(false);

  const goToProjectSelection = () => {
    props.history.push(dinas.projectSelect(`${passId}`));
  };

  const getDetail = async id => {
    setloading(true);
    const sample      = await oProvider.list(`${resource}?id=${id}`);
    let kategori      = await oProvider.list('categories-dropdown');
    const budgetTeam  = await oProvider.list(`budget-teams?budgetId=${id}`);
    const sample2     = await oProvider.list(`budget-publisher?budget_id=${id}&filter=perusahaan`);
    let budgetStatus  = await newDropDown(`status-dropdown`);
    setDataRow(sample.data);
    setdataPub(sample2.data);
    setdataTeam(budgetTeam.data);
    setState({ pagination: { pageSize:5,total: parseInt(budgetTeam.meta.total) }, dataSource: budgetTeam.data });
    setkategori(kategori.data);
    setbudgetStatus(budgetStatus);
    setloading(false);
  }

  const refreshTeam = async () => {
    const budgetTeam  = await oProvider.list(`budget-teams?budgetId=${passId}`);
    setState({ pagination: { pageSize:5,total: parseInt(budgetTeam.meta.total) }, dataSource: budgetTeam.data });
    setdataTeam(budgetTeam.data);
  } 

  React.useEffect(() => {
    getDetail(passId);
  },[passId]);

  const actionMenu = (row, props) => {

    const sunting = async () => {
      let status = null;
      if(row.team_budget_isblock) {
        status = false;
      }else{
        status = true;
      }
      try {
        await oProvider.update(`budget-team-update?id=${row.id}`, {status});
        message.success('Success mengupdate data');
        refreshTeam();
      } catch (error) {
        console.error(error);
      }
    };
  
    return (
      <Menu>
        <Menu.Item>
          <Button type="link" style={{ margin: 0 }} onClick={sunting}>
            {row.team_budget_isblock ? 'Unblock' : 'Block'}
          </Button>
        </Menu.Item>
      </Menu>
    );
  };

  const columns = [
    {
      title : 'Anggota',
      key : 'team_name',
      dataIndex : 'team_name',
      render : (x,row) => {
        return (
          <div style={{display:'grid',gridTemplateColumns:'repeat(2,max-content)',gridColumnGap:'1em'}}>
            <img src={row.team_logo || 'https://via.placeholder.com/50'} alt={row.team_logo || 'no-data'} />
            <div>
              <small>{x}</small>
              <h4>{row.team_position}</h4>
            </div>
          </div>
        )
      }
    },
    {
      title : 'Status',
      key : 'team_budget_isblock',
      dataIndex : 'team_budget_isblock',
      render : x => {
        return x ? 'Blocked' : 'Unblocked';
      }
    },
    {
      title: "Action",
      key: 'action',
      width: 80,
      className: "text-center",
      render: row => (
        <span>
          <Dropdown overlay={actionMenu(row, props)}>
            <Button
              type="link"
              style={{ margin: 0 }}
              onClick={e => e.preventDefault()}
            >
              <i className="icon icon-options-vertical" />
            </Button>
          </Dropdown>
        </span>
      )
    },
  ];

  const handleModalAdd = e => {
    e.preventDefault();
    setvisible(true);
  }
  const handleModalClose = () => {
    setvisible(false);
  }

  const styleItem = {
    display:'grid',
    gridTemplateColumns:'max-content 1fr',
    gridColumnGap:'1em',
    justifyItems:'start',
    alignItems:'center',
  };

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <LoadingSpin loading={loading}>
        <>
        <Row gutter={[16,16]}>
        
          <Col span={8}>
            <p>PENGADAAN INI DIPROSES UNTUK</p>
            
            <Row>
              <div style={styleItem}>
                <img src="https://via.placeholder.com/50" alt={dataRow.ilibrary_name} />
                <div>
                  <span>iLibrary</span>
                  <p>{dataRow.ilibrary_name}</p>
                </div>
              </div>
            </Row>
            
            <Row>
              <div style={styleItem}>
                <img src="https://via.placeholder.com/50" alt={dataRow.epustaka_name} />
                <div>
                  <span>ePustaka</span>
                  <p>{dataRow.epustaka_name}</p>
                </div>
              </div>
            </Row>

            <br/>
            <p style={{marginBottom:'0.4em'}}>JENIS PRODUK</p>
            <Row>
              <div style={styleItem}>
                <img src="https://via.placeholder.com/50" alt={`dataRow.books_category`} />
                <div>
                  <span></span>
                  <KategoriSelected data={kategori} selected={dataRow.books_category} />
                </div>
              </div>
            </Row>

          </Col>

          <Col span={8}>
            <p>JENIS PENGADAAN</p>
            
            <Row>
              <div style={styleItem}>
                <img src="https://via.placeholder.com/50" alt={dataRow.procurement_type_name} />
                <div>
                  <span></span>
                  <p>{dataRow.procurement_type_name}</p>
                </div>
              </div>
            </Row>
            
            <br/>
            <p>NO PESANAN</p>
            <p style={{marginTop:'-1em'}}>{dataRow.budget_order_number || '-'}</p>

            <p>SUMBER DANA</p>
            <Row>
              <div style={styleItem}>
                <img src="https://via.placeholder.com/50" alt={dataRow.source_fund_name} />
                <div>
                  <p>{dataRow.source_fund_name}</p>
                </div>
              </div>
            </Row>

          </Col>

          <Col span={8}>
            <p>TANGGAL PENGADAAN</p>
            
            <Row>
              <div style={{display:'grid',gridTemplateColumns:'repeat(2,max-content)',gridColumnGap:'1em',justifyItems:'start'}}>
                <img src="https://via.placeholder.com/50" alt={dataRow.budget_start_date} />
                <div>
                  <span>Tanggal Mulai</span>
                  <p>{moment(dataRow.budget_start_date).format("YYYY-MM-DD")}</p>
                </div>
              </div>
            </Row>
            
            <Row>
              <div style={{display:'grid',gridTemplateColumns:'repeat(2,max-content)',gridColumnGap:'1em',justifyItems:'start'}}>
                <img src="https://via.placeholder.com/50" alt={dataRow.budget_end_date} />
                <div>
                  <span>Tanggal Jatuh Tempo</span>
                  <p>{moment(dataRow.budget_end_date).format("YYYY-MM-DD")}</p>
                </div>
              </div>
            </Row>

            <br/>
            <p>STATUS PENGADAAN</p>
            <p style={{marginTop:'-1em'}}>{budgetStatus.length > 0 && budgetStatus.find(x => x.value === dataRow.budget_status).text}</p>

          </Col>

        </Row>
        <Row gutter={[16,16]}>
          <Col span={8}>
            <span>Rencana Dana Anggaran (Rp)</span>
            <p>{numberFormat(dataRow.budget_amount)} <small>(Belum termasuk PPN dan diskon)</small></p>
          </Col>

          <Col span={8}>
            <span>Anggaran Yang Terpakai (Rp)</span>
            <p>{numberFormat(0)} </p>
          </Col>
          <Col span={8}>
            <span>Anggaran Yang Tersisa (Rp)</span>
            <p>{numberFormat(dataRow.budget_amount)} </p>
          </Col>
        </Row>
        {
          (!loading && Object.keys(dataRow).length > 0) && 
          dataRow.procurement_type_name.toLowerCase() === 'lelang' ? (<>
            <div style={{...styleItem,alignContent:'start',justifyContent:'end',gridColumnGap:'1em',gridTemplateColumns:'min-content auto'}}>
              <Button type="primary" onClick={handleModalAdd}><i className="icon-plus"></i></Button>
              <Button type="primary" onClick={goToProjectSelection}>Halaman Seleksi</Button>
            </div>
            <Lelang state={state} columns={columns}  dataSource={dataTeam} dataRow={dataRow} />
          </>) : (
            <React.Suspense fallback={<Loading />}>
              <Perusahaan dataSource={dataRow} dataPub={dataPub} />
            </React.Suspense>
          )
        }

        <PopupAdd refreshTeam={refreshTeam} visible={visible} data={dataTeam} budgetId={passId} close={handleModalClose} />
        </>
        </LoadingSpin>
      </Content> 
    </React.Fragment>
  );
}

export default App;