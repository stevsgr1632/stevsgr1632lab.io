import React from 'react';
import { message } from 'antd';
import moment from 'moment';
import config from './index.config';
import FormControl from './Form.jsx';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import messageConfig from './component/config';
import Notification from 'common/notification';
import { Breadcrumb, Content } from 'components';
/* eslint-disable */

const { rab: { dinas } } = pathName;
const resource = 'budget-execution';
message.config(messageConfig.message);

const App = (props) => {
  const { schema } = config;
  const { breadcrumb, content } = config.add;

  const backToList = () => {
    props.history.push(dinas.list.rencana);
  };

  const handleSubmit = (e) => {
    console.log(e);
    e.preventDefault();
    // props.form.validateFields((errors, values) => {
    //   if (!errors) {
    //     console.log(values);
    //     backToList();
    //   }
    // })
  };

  const openNotification = resp => {
    Notification({response : resp });
  };

  const handleOtherSubmit = async (e) => {
    console.log('sebelum tombol save ditekan : ', e);
    if (Object.keys(e).length > 2) {
      let plans = [
        {
          "text": "Persiapan Panitia Pelaksana",
          "value": "12b47230-5b0f-11ea-9b44-a2eedb660190"
        },
        {
          "text": "Seleksi Katalog",
          "value": "12b472bb-5b0f-11ea-9b44-a2eedb660190"
        },
        {
          "text": "Persiapan Berkas",
          "value": "12b472fd-5b0f-11ea-9b44-a2eedb660190"
        },
        {
          "text": "Penerbitan Berkas",
          "value": "12b47321-5b0f-11ea-9b44-a2eedb660190"
        },
        {
          "text": "Aktivasi",
          "value": "12b47354-5b0f-11ea-9b44-a2eedb660190"
        },
        {
          "text": "Pengecekan",
          "value": "12b4737b-5b0f-11ea-9b44-a2eedb660190"
        }
      ];
      e.budget_plans = plans.map(x => {
        let dataSave = {};
        dataSave.budget_plan_status = x.value;
        dataSave.budget_plan_start_date = moment(new Date()).format('DD-MM-YYYY');
        dataSave.budget_plan_end_date = moment(new Date()).format('DD-MM-YYYY');
        return dataSave;
      });

      try {
        let response = await oProvider.insert(`${resource}`, { ...e });
        openNotification(response);
        backToList();
      } catch (errors) {
        message.error('Data Anda ada yang salah atau kurang, Silahkan check kembali !');
      }
    } else {
      message.error('Data tidak lengkap !');
    }

    console.log('setelah tombol save ditekan : ', e);
  }

  const budget_plans = [
    {
      "id": "dd131c56-ac80-4b2c-9769-de06100c0343",
      "budget_plan_status": "12b47230-5b0f-11ea-9b44-a2eedb660190",
      "order_number": 1,
      "budget_plan_name": "Persiapan Panitia Pelaksana",
    },
    {
      "id": "5683e0e4-f05e-4f4c-b5b2-75898e09eab2",
      "budget_plan_status": "12b472bb-5b0f-11ea-9b44-a2eedb660190",
      "order_number": 2,
      "budget_plan_name": "Seleksi Katalog",
    },
    {
      "id": "c3143c1f-2235-44de-b9a5-020164482147",
      "budget_plan_status": "12b472fd-5b0f-11ea-9b44-a2eedb660190",
      "order_number": 3,
      "budget_plan_name": "Persiapan Berkas",
    },
    {
      "id": "bb9d95b2-3849-4266-962f-595da73b459e",
      "budget_plan_status": "12b47321-5b0f-11ea-9b44-a2eedb660190",
      "order_number": 4,
      "budget_plan_name": "Penerbitan Berkas",
    },
    {
      "id": "fb797496-c227-4090-86a1-e3ee06fe9acb",
      "budget_plan_status": "12b47354-5b0f-11ea-9b44-a2eedb660190",
      "order_number": 5,
      "budget_plan_name": "Aktivasi",
    },
    {
      "id": "53e9e5e6-db2d-4da3-a7fa-7c56f8eec7cd",
      "budget_plan_status": "12b4737b-5b0f-11ea-9b44-a2eedb660190",
      "order_number": 6,
      "budget_plan_name": "Pengecekan",
    }
  ]

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <FormControl
          onSubmit={handleSubmit}
          onCancel={backToList}
          schema={schema}
          otherSubmit={handleOtherSubmit}
          type="add"
          data={{ budget_plans }}
        />
      </Content>
    </React.Fragment>
  );
}

export default App;