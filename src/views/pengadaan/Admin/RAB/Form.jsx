import React from 'react';
import moment from 'moment';
import { Button, Form, Steps, Divider } from 'antd';
import oProvider from '../../../../providers/oprovider';
import LoadingSpin from '../../../../components/LoadingSpin';
import { newDropDown } from '../../../../providers/dropdownapi';
// import DetailPlanningForm from './component/DetailPlanningForm';
import InputControl from './component/InputControl';
import PreviewForm from './component/PreviewForm.jsx';
import DetailPlanningPlan from './component/DetailPlanningPlan';
import DetailBudgetForm from './component/DetailBudgetForm.jsx';
import ButtonActionStep from './component/ButtonActionStep.jsx';
import DetailKriteriaForm from './component/DetailKriteriaForm.jsx';
import DetailPublisherForm from './component/DetailPublisherForm.jsx';
/* eslint-disable */

moment.locale('id');

const App = (props) => {
  // console.log(props);
  const { Step } = Steps;
  const [current, setCurrent] = React.useState(0);
  const [loading, setloading] = React.useState(true);
  const [kategori, setkategori] = React.useState([]);
  const [publisher, setpublisher] = React.useState([]);
  const [fiscalYear, setfiscalYear] = React.useState([]);
  const [budgetStatusPlan, setbudgetStatusPlan] = React.useState([]);
  const [budgetStatus, setbudgetStatus] = React.useState([]);
  const [sourceFund, setsourceFund] = React.useState([]);

  const [dataSubmit, setdataSubmit] = React.useState({ budget_ispartial: 0 });

  const [typeOrg, setTypeOrg] = React.useState([]);
  const [typeOrgSelect, setTypeOrgSelect] = React.useState('');
  const [choicePengadaan, setchoicePengadaan] = React.useState('');
  const [jenisProduk, setJenisProduk] = React.useState([]);
  const [jenisProductSelect, setjenisProductSelect] = React.useState([]);
  // const [dateStart,setdateStart] = React.useState([]);
  // const [dateEnd,setdateEnd] = React.useState([]);

  const { onSubmit, otherSubmit, onCancel, schema } = props;
  const [form] = Form.useForm();
  const handleImageboxChange = (value) => {
    setdataSubmit({ ...dataSubmit, procurement_type_id: value });
    setTypeOrgSelect(value);
  }
  const handleImageboxChange2 = (value) => {
    setdataSubmit({ ...dataSubmit, budget_content_media: value.map(x => x).join(',') });
    setjenisProductSelect([...value]);
  }

  const handleStepChange = current => {
    setCurrent(current);
  }

  const steps = [
    { title: 'Jenis Pengadaan', },
    { title: 'Jenis Produk', },
    { title: 'Detail Pengadaaan', },
    { title: 'Pratinjau', },
  ];

  const next = () => {
    setCurrent(current + 1);
  }

  const prev = () => {
    setCurrent(current - 1);
  }

  const handleChangeInput = (name, value, e) => {
    let arr = ['budget_amount', 'budget_number_of_title', 'budget_number_of_copy', 'budget_min_price', 'budget_max_price'];
    if (arr.includes(name)) {
      value = parseFloat(value);
    }
    if (name === 'books_category') {
      value = value.join();
    }
    if (name === 'budget_publishers') {
      value = value.join();
    }
    if (name === 'budget_start_date') {
      value = moment(value).format('DD-MM-YYYY');
    }
    if (name === 'budget_end_date') {
      value = moment(value).format('DD-MM-YYYY');
    }
    // console.log(name,value,e);
    setdataSubmit({ ...dataSubmit, [name]: value });
  }

  const handleChangeInputPlan = (...data) => {
    console.log(data);
    let arrCompare = ['12b47230-5b0f-11ea-9b44-a2eedb660190', '12b472bb-5b0f-11ea-9b44-a2eedb660190', '12b472fd-5b0f-11ea-9b44-a2eedb660190', '12b47321-5b0f-11ea-9b44-a2eedb660190', '12b47354-5b0f-11ea-9b44-a2eedb660190', '12b4737b-5b0f-11ea-9b44-a2eedb660190'];
    let parseData = {}, budget_plans = [];
    if (arrCompare.includes(data[0])) {
      parseData.budget_plan_status = data[0];
      if (data[1] === 'budget_plan_start_date') {
        parseData.budget_plan_start_date = data[1];
      } else {
        parseData.budget_plan_end_date = data[1];
      }
    }
    if (budget_plans.find(x => x.budget_plan_status)) {
      budget_plans.map(x => (x.budget_plan_status === data[0]) && { x, ...parseData });
    } else {
      budget_plans.push(parseData);
    }
    console.log(budget_plans);
    setdataSubmit({ ...dataSubmit, budget_plans });
  }

  // React.useEffect(() => {
  //   console.log(dateStart);
  //   console.log(dateEnd);
  // },[dateEnd,dateStart]);

  // const resultingDiffDate = () => {
  //   if(dateStart && dateEnd){
  //     let date1     = moment(dateStart);
  //     let date2     = moment(dateEnd);
  //     let duration   = moment.duration(date2.diff(date1))/(1000 * 60 * 60 * 24);
  //     return duration;
  //   }else{
  //     console.log('ada error');
  //   }
  // }
  // const handleChangeInputPlan2 = (...data) => {
  //   console.log(data);
  // }

  React.useEffect(() => {
    async function fetchData() {
      setloading(true);
      const [
        { data: typeProcurement },
        { data: jenisProduk },
        { data: kategori },
        { data: publisher }
      ] = await Promise.all([
        oProvider.list('procurement-type'),
        oProvider.list('content-media-list'),
        oProvider.list('categories-dropdown'),
        oProvider.list('organization-dropdown')
      ]);
      let fiscalyear = await newDropDown('fiscal-year-dropdown');
      let sourceFund = await newDropDown('source-of-funds-dropdown');
      let budgetStatus = await newDropDown(`status-dropdown`);
      let budgetStatusPlan = await newDropDown(`status-dropdown?process_name=RAB-RUN`);
      setTypeOrg(typeProcurement.map(({ procurement_type_name: text, procurement_type_image: src, id }) => ({ text, src, id })));
      setJenisProduk(jenisProduk.map(({ content_media_name: text, content_media_logo: src, id }) => ({ id, text, src:`/assets/general/${src}` })));
      setkategori(kategori);
      setpublisher(publisher);
      setfiscalYear(fiscalyear);
      setsourceFund(sourceFund);
      setbudgetStatus(budgetStatus);
      setbudgetStatusPlan(budgetStatusPlan);
      setloading(false);
    }
    fetchData();
  }, []);

  React.useEffect(() => {
    if (Object.keys(props.data).length > 0) {
      console.log('effect triggered', props.data);
      setdataSubmit({ ...dataSubmit, budget_publishers: props.data.catalog_publishers, ...props.data });
    }
  }, [props.data]);

  React.useEffect(() => {
    let dataValue = {};
    if (current === 2) {
      if (props.type === 'edit') {
        dataValue.budget_start_date = moment(props.data.budget_start_date).format('YYYY-MM-DD');
        dataValue.budget_end_date = moment(props.data.budget_end_date).format('YYYY-MM-DD');
        dataValue.budget_publishers = props.data.catalog_publishers;
        form.setFieldsValue({ ...props.data, ...dataValue });
      }
      setdataSubmit({ ...dataSubmit, budget_publishers: props.data.catalog_publishers, ...props.data });
    }

  }, [current]);

  React.useEffect(() => {
    async function changeData() {
      let choicePengadaan = await typeOrg.find(x => {
        return x.id === typeOrgSelect;
      }).text;
      console.log(choicePengadaan);
      setchoicePengadaan(choicePengadaan);
    }
    if (typeOrgSelect !== '') {
      changeData();
    }

  }, [typeOrgSelect]);

  let budgetDataForm = { handleChangeInput, schema, fiscalYear };
  let publisherDataForm = { handleChangeInput, schema, publisher, kategori };
  let kriteriaDataForm = { handleChangeInput, schema };
  let previewDataForm = { dataSubmit, budgetStatus, typeOrg, fiscalYear, sourceFund, jenisProduk, kategori, publisher };
  let buttonNavDataForm = { prev, next, steps, current, otherSubmit, dataSubmit };

  return (
    <>
      <Steps current={current} onChange={handleStepChange}>
        {steps.map(x => <Step key={x.title} title={x.title} />)}
      </Steps>
      <br />
      <Form form={form} onFinish={onSubmit}>
        <LoadingSpin loading={loading}>
          <>
            {(current === 0 && typeOrg.length > 0) && (
              <InputControl type="radio-image" items={typeOrg} onChange={handleImageboxChange} defaultValue={((props.type === 'edit') && (typeOrgSelect === '')) ? props.data.procurement_type_id : typeOrgSelect} />
            )}
            {(current === 1 && jenisProduk.length > 0) && (
              <InputControl type="checkbox-image" items={jenisProduk} onChange={handleImageboxChange2} defaultValue={((props.type === 'edit') && (jenisProductSelect.length === 0)) ? props.data.budget_content_media : jenisProductSelect} />
            )}
            {current === 2 && (<>

              <DetailBudgetForm {...budgetDataForm} />

              {!choicePengadaan.toLowerCase().includes('langsung') && (<>
                <DetailPublisherForm {...publisherDataForm} />
                <DetailKriteriaForm {...kriteriaDataForm} />

                <br /><br />
                <Divider orientation="left"><strong>Detail Rencana Pelaksanaan</strong></Divider>
                {/* <pre>{JSON.stringify(props.data.budget_plans, null, '  ')}</pre> */}
                {props.data.budget_plans.map(plan => (<DetailPlanningPlan key={plan.id} {...plan} />))}
              </>)}

            </>)}
            {(current === 3 && Object.keys(dataSubmit).length > 1) && <PreviewForm {...previewDataForm} />}

            <hr />

            <ButtonActionStep {...buttonNavDataForm} />

            {/* <Button htmlType="submit">Save</Button> */}
            <Button className="ml-1" style={{ marginTop: -40, position: 'absolute' }} onClick={onCancel}>Cancel</Button>
          </>
        </LoadingSpin>
      </Form>
    </>
  );
}

export default App;