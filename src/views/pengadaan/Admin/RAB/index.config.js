import React from 'react';
import pathName from 'routes/pathName';
import { Button,Menu, Dropdown } from 'antd';
import {PlusOutlined} from '@ant-design/icons';
import { getOneMenu } from 'utils/getMenuAkses';
import { numberFormat } from 'utils/initialEndpoint';
import ActionTableDropdown from "components/ActionTableDropdown";
/* eslint-disable */

const {initial,rab:{dinas}} = pathName;

const actionMenu = (row, props) => {
  const {menuAksesContext} = props;
  const sunting = (type) => {
    switch (type){
      case "first":
        props.history.push(dinas.edit(`${row.id}`));
        break;
      case "second":
        console.log('second');
        props.viewDetail(row);
        break;
      case "third":
        console.log('third');
        props.history.push(dinas.detail(`${row.id}`));
        break;
      default:
        console.log('default');
        props.viewDetail(row);
    }

    // console.log(props);
  };

  return (
    <Menu>
      {(props.model.type === 'planning' || props.model.type === 'berjalan') && (
        <Menu.Item>
          { getOneMenu(menuAksesContext,'Sunting') && <>
            <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => sunting('first')} text="Sunting" />
          </>}
        </Menu.Item>
      )}
      {props.model.type === 'planning' && (
        <Menu.Item>
          <Button type="link" style={{ margin: 0 }} onClick={() => sunting('second')}>Jadikan Berjalan</Button>
        </Menu.Item>
      )}
      <Menu.Item>
        <Button type="link" style={{ margin: 0 }} onClick={() => sunting('third')}>Detail</Button>
      </Menu.Item>
    </Menu>
  )
};

export default {
  list: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Rencana Anggaran Biaya', to: dinas.list },
    ],
    planning: {
      title: 'Filter Rencana Anggaran Biaya (RAB)',
      subtitle: 'Lakukan filter untuk memudahkan pencarian',
      toolbars: [
        {
          text: '', type: 'primary', icon:<PlusOutlined />,
          onClick: (e) => {
            e.history.push(dinas.add);
          }
        },
      ]
    },
    berjalan: {
      title: 'Filter Rencana Anggaran Biaya Berjalan',
      subtitle: 'Lakukan filter untuk memudahkan pencarian',
    },
    selesai: {
      title: 'Filter Rencana Anggaran Biaya Selesai',
      subtitle: 'Lakukan filter untuk memudahkan pencarian',
    },
    config: (props) => {
      // const { history } = props;
      return {
        columns: [
          {
            title: 'Nama Pengadaan',
            dataIndex: 'budget_name',
            sorter: true
          },
          {
            title: 'No Pesanan',
            dataIndex: 'budget_order_number',
          },
          {
            title: 'Jenis Pengadaan',
            dataIndex: 'procurement_type_name',
          },
          {
            title: 'Tahun Anggaran',
            dataIndex: 'fiscal_year_name',
          },
          {
            title: 'Sumber Dana',
            dataIndex: 'source_fund_name',
            render: (x,y) => {
              return (
              <>
                <span>{x}</span>
                <p>Total Dana : {numberFormat(y.budget_amount)}</p>
              </>
              )
            }
          },
          {
            title: 'Tahap Persiapan',
            dataIndex: 'status_name',
          },
          {
            title: 'Total Judul Pengadaan / Salinan',
            dataIndex: 'budget_number_of_title',
            render: (x,y) => {
              return (
              <>
                <span>{`Judul : ${x}`}</span>
                <p>{`Salinan : ${y.budget_number_of_copy}`}</p>
              </>
              )
            }
          },
          {
            title: 'Action',
            key: 'action',
            width: 80,
            className: 'text-center',
            render: (row) => (
              <span>
                <Dropdown overlay={actionMenu(row, props)}>
                  <Button type="link" style={{ margin: 0 }} onClick={e => e.preventDefault()}>
                    <i className="icon icon-options-vertical" />
                  </Button>
                </Dropdown>
              </span>
            ),
          },
        ]
      }
    }
  },
  add: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Rencana Anggaran Biaya (RAB)', to: dinas.list.rencana },
      { text: 'Create New' },
    ],
    content: {
      title: 'Buat Rencana Anggaran Biaya Baru',
    },
  },
  edit: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Rencana Anggaran Biaya (RAB)', to: dinas.list },
      { text: 'Edit RAB' },
    ],
    content: {
      title: 'Update Rencana Anggaran Biaya',
    },
  },
  detail: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Rencana Anggaran Biaya (RAB)', to: dinas.list },
      { text: 'Detail' },
    ],
    content: {
      title: 'Detail Rencana Anggaran Biaya',
    },
  },
  schema: {
    procurement_type_id: {label: 'Pilih salah satu jenis pengadaan yang akan digunakan',},
    budget_content_media: {label: 'Produk Digital (Buku Elektronik, VIdeo Elektronik dll)',},
    source_fund_id: {label: 'Sumber Dana',rules : [{ required: true, message: 'Pilih Sumber Dana' }]},
    fiscal_year_id: {label: 'Tahun Anggaran',rules : [{ required: true, message: 'Pilih Tahun Anggaran' }]},
    budget_amount: {label: 'Rencana Dana Anggaran',rules : [{ required: true, message: 'Silahkan Input Jumlah Anggaran' }]},
    budget_start_date: {label: 'Tanggal Mulai', rules : [{ required: true, message: 'Pilih Tanggal Awal' },
      ({ getFieldValue }) => ({
        validator(rule, value) {
          console.log(getFieldValue('budget_end_date'));
          if (!value || getFieldValue('budget_end_date') < value) {
            console.log(value);
            return Promise.reject('Tanggal mulai harus lebih kecil dari tanggal jatuh tempo !');
          }
          return Promise.resolve();
        },
      }),
    ]},
    budget_end_date: {label: 'Tanggal Jatuh Tempo', rules : [{ required: true, message: 'Pilih tanggal jatuh tempo' },
      ({ getFieldValue }) => ({
        validator(rule, value) {
          console.log(getFieldValue('budget_start_date'));
          if (!value || getFieldValue('budget_start_date') > value) {
            console.log(value);
            return Promise.reject('Tanggal jatuh tempo harus lebih besar dari tanggal mulai !');
          }
          return Promise.resolve();
        },
      }),
    ]},
    budget_status: {label: 'Status', rules : [{ required: true, message: 'Pilih Status' }]},
    budget_publishers: {label: 'Penerbit', rules : [{ required: true, message: 'Pilih Penerbit' }]},
    books_category: {label: 'Kategory', rules : [{ required: true, message: 'Pilih Kategori' }]},
    budget_number_of_title: {label: 'Jumlah Judul', rules : [{ required: true, message: 'Silahkan Input Jumlah Judul Buku'}]},
    budget_number_of_copy: {label: 'Jumlah Salinan per Judul', rules : [{ required: true, message: 'Silahkan Input Jumlah Salinan'}]},
    budget_min_price: {label: 'Harga Terendah', rules : [{ required: true, message: 'Silahkan Input Harga Terendah'}] },
    budget_max_price: {label: 'Harga Tertinggi',
      rules : [
        { required: true, message: 'Silahkan Input Harga Tertinggi' },
        ({ getFieldValue }) => ({
          validator(rule, value) {
            console.log(getFieldValue('budget_min_price'));
            if (!value || getFieldValue('budget_min_price') > value) {
              console.log(value);
              return Promise.reject('Harga tertinggi harus lebih besar dari harga terendah !');
            }
            return Promise.resolve();
          },
        }),
      ]
    },
    budget_name: {
      label: 'Nama Pengadaan',
      rules: [ 
        { required: true, message: 'Silahkan input nama' },
     ]
    },
    budget_plan_start_date:{label: ''},
    budget_plan_end_date:{label: ''},
    budget_totalhari_0:{label: ''},
    budget_totalhari_1:{label: ''},
    budget_totalhari_2:{label: ''},
    budget_totalhari_3:{label: ''},
    budget_totalhari_4:{label: ''},
    budget_totalhari_5:{label: ''},
    budget_plan_status:{label: ''},
  },
  model: {
    fiscalyear: '', search: '', type:""
  },
  data: {
    status: [
      { value: '', text: 'Pilih Status', disabled: true },
      { value: 'rencana', text: 'Rencana' },
      { value: 'berjalan', text: 'Berjalan' },
    ],
  }
};