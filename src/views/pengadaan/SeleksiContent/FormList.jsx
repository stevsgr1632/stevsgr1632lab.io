import React from 'react';
import { Modal } from 'antd';
import config from './index.config';
import queryString from "query-string";
import { MenuAksesContext } from './index';
import randomString from "utils/genString";
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
import {MessageContent} from "common/message";
import Notification from "common/notification";
import {DeleteOutlined} from '@ant-design/icons';
import useCustomReducer from "common/useCustomReducer";
import ButtonDeleteBulk from "./component/ButtonDeleteBulk";
/* eslint-disable */

const resource = 'selection-planning-list';
const { confirm } = Modal;
const initialData = {
  url : '',
  limit : 10,
  state : {
    loading:false, 
    pagination: { 
      pageSize: 10, 
      defaultCurrent: 1,
      showSizeChanger: true,
      hideOnSinglePage : true,
    }, 
    dataSource: []
  },
  loadingBtnBulk : false,
  selectedRowKeys : []
};
const List = (props) => {
  const menuAksesContext = React.useContext(MenuAksesContext);
  const {model} = props;
  const [reducerCatalog,reducerFunc] = useCustomReducer(initialData);

  const downloadSelectContent = async row => {
    try {
      let key = randomString(17);
      MessageContent({ type :'loading', content:'Mengunduh ... ',key, duration: 0 });
      const {data : file} = await oProvider.downloadfile(`selection-planning-download?id=${row.id}`);
      if(file){
        MessageContent({ type :'success', content: 'Selesai!', key, duration: 1 });
        const url = window.URL.createObjectURL(new Blob([file]));
        const link = window.document.createElement('a');
        link.href = url;
        link.setAttribute('download', `Seleksi-Katalog_${new Date().getTime()}.xlsx`); //or any other extension
        document.body.appendChild(link);
        link.click();
      }
    } catch (error) {
      console.log(error?.message);
    }
  };
  const modalDelete = (props) => {
    confirm({
      title: 'Do you want to delete these items?',
      icon: <DeleteOutlined/>,
      content: 'When clicked the OK button, this dialog will be closed',
      centered : true,
      onOk() {
        return new Promise(async (resolve, reject) => {
          try {
            await oProvider.delete(`selection-planning-delete?id=${props.id}`);
            setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
            refresh(model);
          } catch (error) {
            Modal.destroyAll();
            console.log(error?.message);
          }
        }).catch((error) => {
          console.log(error);
        });
      },
      onCancel() {},
    });
  };
  const onShowSizeChange = (current, pageSize) => {
    reducerFunc('limit',pageSize,'conventional');
  };
  const refresh = async (model) => {
    reducerFunc('state',{ loading:true });

    let paramsObj = {};
    paramsObj['limit'] = reducerCatalog?.limit;

    Object.keys(model).forEach(x => {
      paramsObj[x] = model[x];
    });

    try {
      let queryStringData = queryString.stringify(paramsObj,{ skipEmptyString : true});
      const { meta, data } = await oProvider.list(`${resource}?${queryStringData}${reducerCatalog?.url}`);
      reducerFunc('state',{ 
        loading:false, pagination: { 
          ...reducerCatalog?.state?.pagination,
          pageSize:reducerCatalog?.limit, 
          total: meta.total,
          onShowSizeChange 
        },
        dataSource: data 
      });
    } catch (error) {
      reducerFunc('state',{ loading:false });
      console.log(error?.message);
      return false;
    }
  };

  const onSelectChange = (rowKeys,rowArrays) => {
    reducerFunc('selectedRowKeys',rowKeys,'conventional');
  };

  const rowSelection = {
    selectedRowKeys : reducerCatalog.selectedRowKeys,
    onChange: onSelectChange,
  };

  const handleBulkDelete = async () => {
    try {
      reducerFunc('loadingBtnBulk',true,'conventional');
      let resp = await oProvider.delete(`selection-planning-delete?id=${reducerCatalog?.selectedRowKeys.join()}`);
      if(resp){
        reducerFunc('loadingBtnBulk',false,'conventional');
        Notification({type: 'success', response : resp});
        reducerFunc('selectedRowKeys',[],'conventional');
        refresh(model,reducerCatalog?.url);
      }
    } catch (error) {
      reducerFunc('loadingBtnBulk',false,'conventional');
      console.log(error?.message);
    }
  };

  const handleTableChange = (pagination, filters, sorter) => {
    let url = '';
    if (pagination) url += `&page=${pagination.current}`;
    if (sorter && sorter.field) {
      url += `&sortBy=${sorter.field}`;
      url += `&sortDir=${sorter.order === 'ascend' ? 'asc' : 'desc'}`;
    }
    reducerFunc('url',url,'conventional');
  }

  React.useEffect(() => {
    refresh(model,reducerCatalog?.url);
  }, [reducerCatalog?.url,
    model?.search,
    model?.status,
    model?.end_date,
  ]);

  return (
    <React.Fragment>
      {reducerCatalog?.selectedRowKeys.length > 0 && <ButtonDeleteBulk loading={reducerCatalog?.loadingBtnBulk} handleBulkDelete={handleBulkDelete} />}
      <MocoTable
        {...config.list.config(props,modalDelete,downloadSelectContent,menuAksesContext)}
        {...reducerCatalog?.state}
        onChange={handleTableChange}
        rowSelection={rowSelection}
      />
    </React.Fragment>
  );
}

export default List;