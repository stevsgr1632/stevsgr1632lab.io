import React from 'react';
import { useHistory } from 'react-router-dom';
import { Form, Row, Col, 
  Typography,Popconfirm,
  Button, Divider, Space } from "antd";
import pathName from "routes/pathName";
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import { ubahFormatAngka } from "./index.config";
import FormInputComp from './component/FormInput';
import TableInvoice from './component/TableInvoice';
import TableKategori from './component/TableKategori';
import TablePenerbit from './component/TablePenerbit';
import DaftarKatalog from './component/DaftarKatalog';
import { DataSubmitContext } from "./FormDataContainer";
/* eslint-disable */
const colorText = {color:'#0E990F'};
const CheckPriceCatalog = ({dataSubmit}) => {
  const sisa = dataSubmit?.transaction_amount - dataSubmit?.catalog_selection_amount;
  return(<>
    {!isNaN(sisa) ? <Typography.Title level={3} style={colorText}>Sisa Anggaran(Rp) : {ubahFormatAngka(sisa)}
    </Typography.Title> : 
    <Typography.Title level={3} style={colorText}>Sisa Anggaran(Rp) : {dataSubmit?.transaction_amount ? 
      ubahFormatAngka(dataSubmit?.transaction_amount) : 0}
    </Typography.Title>    
    }
  </>);
};

const FormWrapper = () => {
  const [form] = Form.useForm();
  const history = useHistory();
  const { rab : { selectcontent } } = pathName;
  const [visible,setvisible] = React.useState(false);
  const [simpanAble,setsimpanAble] = React.useState(false);
  const { dataReducer : { dataSubmit }, propsContainer } = React.useContext(DataSubmitContext);

  const handleTambah = (e) => {
    e.preventDefault();
    form.validateFields().then(value => {
      if(value){
        setvisible(true);
      }
    }).catch(error => {
      console.log(error);
    });
  };

  const notif = async (message,data) => {
    if(data){
      localStorage.removeItem('selectedcontent');
      localStorage.removeItem('arraycatalog');
      history.push(selectcontent.list);
      Notification({
        type:'success',
        response : {
          code : message,
          message : 'Success Seleksi Kontent',
        }
      });
    }
  };

  const handleSubmitToAPI = async e => {
    e.preventDefault();
    try {
      setsimpanAble(true);
      if(propsContainer.typeForm === 'edit'){
        const { message,data } = await oProvider.update(`selection-planning-update?id=${propsContainer.match.params.id}`, dataSubmit);
        notif(message,data);
      }else{
        const { message,data } = await oProvider.insert('selection-planning-insert', dataSubmit);
        notif(message,data);
      }
    } catch (error) {
      setsimpanAble(false);
      Notification({
        type:'error',
        response : {
          code : error?.message,
          message : 'GAGAL Seleksi Kontent',
        },
        placement:'topRight'
      });
      console.log(error?.message);
    }
  };

  React.useEffect(() => {
    if(dataSubmit?.catalog_selection_amount){
      let sisaAnggaran = parseInt(dataSubmit?.transaction_amount) - parseInt(dataSubmit?.catalog_selection_amount);
      if(sisaAnggaran <= 0) {
        setsimpanAble(true);
      }else{
        setsimpanAble(false);
      }
    }
    form.setFieldsValue(dataSubmit);
  },[dataSubmit,propsContainer]);

  const propsDaftarKatalog = { visible,setvisible };

  return(<>
    <Form form={form} initialValues={{transaction_amount : 0}}>
      <Row gutter={16}>
          <FormInputComp />
        <Divider />
        <Col span={24} style={{display:'flex',justifyContent:'space-between'}}>
          <CheckPriceCatalog dataSubmit={dataSubmit} />
          <Space>
            {(dataSubmit?.catalogs?.length && !simpanAble) ?
              <Popconfirm
                title="Apakah Anda yakin ?"
                onConfirm={handleSubmitToAPI}
                onCancel={() => Boolean(false)}
                okText="Ya"
                cancelText="Batal"
              >
                <Button 
                  type="primary" 
                  htmlType="button" 
                  danger 
                  disabled={simpanAble}
                >{propsContainer.typeForm === 'edit' ? 'PERBAHARUI' : 'SIMPAN'}</Button> 
              </Popconfirm>
              : null}
            <Button type="primary" onClick={handleTambah} style={{float:'right'}}>Tambah Katalog</Button>
          </Space>
        </Col>
        <Divider />
      </Row>
      <TableInvoice />
      <Row gutter={16}>
        <Col span={12}>
          <TablePenerbit />
        </Col>
        <Col span={12}>
          <TableKategori />
        </Col>
      </Row>
    </Form>
    <DaftarKatalog {...propsDaftarKatalog} />
  </>);
};

export default FormWrapper;
