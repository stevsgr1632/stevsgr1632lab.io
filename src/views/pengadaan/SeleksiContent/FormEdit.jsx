import React from 'react';
import FormDataContainer from "./FormDataContainer";

const FormEdit = props => {
  return(<>
    <FormDataContainer
      {...props}
      typeForm="edit"
    />
  </>);
};

export default FormEdit;