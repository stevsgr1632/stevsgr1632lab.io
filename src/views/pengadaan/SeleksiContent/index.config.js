import React from 'react';
import moment from 'moment';
import { Dropdown,Button,Menu,
  Badge, Typography, Space } from 'antd';
import pathName from 'routes/pathName';
import randomString from 'utils/genString';
import {PlusOutlined} from '@ant-design/icons';
import { getOneMenu } from 'utils/getMenuAkses';
import { numberFormat } from "utils/initialEndpoint";
import ActionTableDropdown from "components/ActionTableDropdown";
/* eslint-disable */

export const initialColor = {orange : '#ff751a',red : '#ff3333',green : '#52c41a', blue : '#108ee9' };
export const textAlign = {textAlign:'right'};
export const hideSuntingBerjalan = (params) => {
  let modString = params.toLowerCase();
  switch (modString) {
    case 'revisi':
      return true;
    case 'select by team':
      return true;
    default:
      return false;
  }
};

const { initial, rab : { selectcontent } } = pathName;
const actionMenu = (row, params) => {
  const [props,modalDelete,downloadSelectContent,menuAksesContext] = params;
  
  const sunting = () => {
    props.history.push(selectcontent.edit(row.id));
  };
  const jadikanBerjalan = () => {
    props.history.push(selectcontent.buatBerjalan(row.id));
  };

  return (
    <Menu>
      { (getOneMenu(menuAksesContext,'Sunting') && hideSuntingBerjalan(row.status_name)) &&
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={sunting} text="Sunting" />
      </Menu.Item>
      }
      {hideSuntingBerjalan(row.status_name) && 
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={jadikanBerjalan} text="Jadikan Berjalan"/>
      </Menu.Item>
      }
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => downloadSelectContent(row)} text="Download" />
      </Menu.Item>
      { getOneMenu(menuAksesContext,'Hapus') &&
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => modalDelete(row, props.history)} text="Hapus" />
      </Menu.Item>
      }
    </Menu>
  );
};

export const ubahFormatAngka = (x, type = 'biasa') => {
  let formatting = numberFormat(parseInt(x));
  switch (type) {
    case 'uang':
      return `Rp${formatting},00`;
    default:
      return formatting;
  }
};

const statusColor = status => {
  let color = '';
  switch (status.toLowerCase()) {
    case 'revisi':
      color = initialColor.orange;
      break;
    case 'aktivasi gagal':
      color = initialColor.red;
      break;
    case 'approved':
      color = initialColor.green;
      break;
    default:
      color = initialColor.blue;
      break;
  }
  return (
    <Badge color={color} text={status} key={randomString(5)}/>
  );
};

export default {
  list: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Seleksi Konten'},
    ],
    content: {
      title: 'Daftar Seleksi Konten',
      subtitle: 'Modul Seleksi Konten',
      toolbars: [
        {
          text: '', type: 'primary', icon: <PlusOutlined/>,
          onClick: (e) => {
            e.history.push(selectcontent.add);
          }
        },
      ]
    },
    config: (...props) => {
      return {
        columns: [
          {
            title: 'Tanggal',
            dataIndex: 'transaction_date',
            render : x => moment(x).format('DD-MM-YYYY'),
            defaultSortOrder: 'ascend',
            sorter: (a, b) => b.transaction_date - a.transaction_date,
          },
          {
            title: 'Keterangan',
            key: 'transaction_note',
            render : x => {
              return(<Space direction='vertical'>
                <Typography.Text>{x.transaction_number}</Typography.Text>
                <Typography.Text>{x.transaction_note}</Typography.Text>
                <Typography.Text strong>{x.source_fund_name} - {ubahFormatAngka(x.budget_amount,'uang')}</Typography.Text>
              </Space>);
            },
          },
          {
            title: 'iLibrary',
            dataIndex: 'ilibrary_name',
            key: 'ilibrary_name',
            render : x => x ? x : 'None'
          },
          {
            title: 'Jumlah Buku',
            dataIndex: 'catalog_qty',
            key: 'catalog_qty',
            render : x => ubahFormatAngka(x)
          },
          {
            title: 'Jumlah Kategori',
            dataIndex: 'category_qty',
            key: 'category_qty',
            render : x => ubahFormatAngka(x)
          },
          {
            title: 'Total Seleksi',
            dataIndex: 'catalog_selection_amount',
            key: 'catalog_selection_amount',
            render : x => ubahFormatAngka(x,'uang')
          },
          {
            title: 'Status',
            width: 100,
            key: 'status_name',
            dataIndex: 'status_name',
            render: x => statusColor(x),
          },
          {
            title: 'Aksi',
            key: 'operation',
            fixed: 'right',
            width: 100,
            render: (row) => {
              return (
              <span>
                <Dropdown overlay={actionMenu(row, props)}>
                  <Button
                    type="link"
                    style={{ margin: 0 }}
                    onClick={e => e.preventDefault()}
                  >
                    <i className="icon icon-options-vertical" />
                  </Button>
                </Dropdown>
              </span>
              )
            },
          },
        ]
      }
    }
  },
  add: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Seleksi Konten', to: selectcontent.list },
      { text: 'Masukkan Seleksi Konten' },
    ],
    content: {
      title: 'Masukkan Seleksi Konten Baru',
    },
  },
  edit: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Seleksi Konten', to: selectcontent.list },
      { text: 'Perbaharui Seleksi Konten' },
    ],
    content: {
      title: 'Perbaharui Seleksi Konten',
    },
  },
  buatberjalan: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Seleksi Konten', to: selectcontent.list },
      { text: 'Jadikan Berjalan' },
    ],
    content: {
      title: 'Seleksi Pengadaan Katalog',
      subtitle: 'Persetujuan Pengadaan Katalog',
    },
  },
  schema: {
    status: {
      label: 'Status',
    },
    tanggal: {
      label: 'Tanggal',
    },
    search: {
      label: 'Pencarian',
    },
    ilibrary_id: {
      label: 'iLibrary',
      rules: [
        { required: true, message: 'Silahkan pilih iLibrary' },
      ]
    },
    procurement_type_id: {
      label: 'Jenis Pengadaan',
      rules: [
        { required: true, message: 'Silahkan pilih jenis pengadaan' },
      ]
    },
    source_of_funds_id: {
      label: 'Sumber Pendanaan',
      rules: [
        { required: true, message: 'Silahkan pilih sumber dana' },
      ]
    },
    transaction_amount: {
      label: 'Nilai Anggaran (Rp)',
      rules: [
        { required: true, message: 'Silahkan input nilai anggaran' },
        ({getFieldValue}) => ({
          validator(rule, value) {
            if (value > 0) {
              return Promise.resolve();
            } else {
              return Promise.reject('Nilai anggaran tidak boleh kosong');
            }
          },
        }),
      ]
    },
    transaction_note: {
      label: 'Keterangan',
      rules: [
        { required: true, message: 'Silahkan input keterangan' },
      ]
    },
  },
  schemaBerjalan : {
    transaction_number: {
      label: 'Nomor SPK',
      rules: [
        { required: true, message: 'Silahkan input nomor SPK' },
      ]
    },
    transaction_date: {
      label: 'Tanggal SPK',
      rules: [
        { required: true, message: 'Silahkan pilih tanggal SPK' },
      ]
    },
    document_spk: {
      label: 'Upload Dokumen SPK',
      rules: [
        { required: true, message: 'Silahkan upload dokumen SPK' },
      ]
    },
  },
  model: {
    status: '',
    start_date: '',
    end_date: '',
    search: ''
  }
};