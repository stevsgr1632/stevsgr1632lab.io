import React from 'react';
import pathName from "routes/pathName";
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import { ubahFormatAngka } from "../index.config";
import TableInvoice from './TableInvoiceBuatBerjalan';
import { DataSubmitContext } from "../JadikanBerjalan";
import FormInputComp from './FormInputJadikanBerjalan';
import TableKategori from './TableKategoriBuatBerjalan';
import TablePenerbit from './TablePenerbitBuatBerjalan';
import { Form, Row, Col, Typography, Divider } from "antd";
import { formDateDisplayValue, formDateInputValue } from "utils/initialEndpoint";
/* eslint-disable */

export default () => {
  const [form] = Form.useForm();
  const { rab : { selectcontent } } = pathName;
  const { dataReducer : { dataSubmit,sisa,terpakai }, propsContainer,textAlign } = React.useContext(DataSubmitContext);
  const [simpanable,setsimpanAble] = React.useState({loading : false, disabled : false});
  const selectContentId = propsContainer.match.params.id;
  
  const notif = (message,data) => {
    if(data){
      propsContainer.history.push(selectcontent.list);
      Notification({
        type:'success',
        response : {
          code : message,
          message : 'Success Seleksi Kontent',
        }
      });
    }
  };

  const handleSubmitToAPI = async e => {
    e.preventDefault();
    form.validateFields().then(async value => {
      try {
        setsimpanAble(prev => ({...prev, loading : true}) );
        const { message } = await oProvider.update(`selection-planning-approve?id=${selectContentId}`, {
          transaction_number: dataSubmit?.transaction_number,
          transaction_date: dataSubmit?.transaction_date_new
        });
        notif(message,message);
      } catch (error) {
        setsimpanAble(prev => ({...prev, loading : false}) );
        Notification({
          type:'error',
          response : {
            code : error?.message,
            message : 'GAGAL Seleksi Kontent',
          },
          placement:'topRight'
        });
        console.log(error?.message);
      }
    }).catch(error => {
      console.log(error);
    }); 
  }
    
  React.useEffect(() => {
    if(sisa){
      if(sisa < 0) {
        setsimpanAble(prev => ({...prev, disabled : true}) );
      }else{
        setsimpanAble(prev => ({...prev, disabled : false}) );
      }
    }
  },[sisa]);

  const propsFormInput = {simpanable,setsimpanAble,handleSubmitToAPI};

  return(<>
    <Form form={form} initialValues={{transaction_date : formDateInputValue(new Date, 'DD-MM-YYYY') }}>
      <Row gutter={16}>
        <FormInputComp {...propsFormInput} />
      </Row>
    </Form>
    <Divider />
    <Row>
      <Col span={12}>
        <Row gutter={4}>
          <Col span={4}>
            <Typography.Text strong>Nama iLibrary</Typography.Text>
          </Col>
          <Col span={1}>:</Col>
          <Col span={12}>
            <Typography.Text strong>{dataSubmit?.ilibrary_name}</Typography.Text>
          </Col>
        </Row>
        
        <Row gutter={4}>
          <Col span={4}>
            <Typography.Text>Tanggal</Typography.Text>
          </Col>
          <Col span={1}>:</Col>
          <Col span={12}>
            <Typography.Text>{formDateDisplayValue(dataSubmit?.transaction_date,'DD-MM-YYYY')}</Typography.Text>
          </Col>
        </Row>

        <Row gutter={4}>
          <Col span={4}>
            <Typography.Text>Sumber Dana</Typography.Text>
          </Col>
          <Col span={1}>:</Col>
          <Col span={12}>
            <Typography.Text>{dataSubmit?.sumber_of_fund}</Typography.Text>
          </Col>
        </Row>
        
        <Row gutter={4}>
          <Col span={4}>
            <Typography.Text>Keterangan</Typography.Text>
          </Col>
          <Col span={1}>:</Col>
          <Col span={12}>
            <Typography.Text>{dataSubmit?.transaction_note}</Typography.Text>
          </Col>
        </Row>

      </Col>
      <Col span={12}>
          <Row></Row>
          <Row gutter={4}>
            <Col span={4}>
              <Typography.Text strong>Anggaran (Rp)</Typography.Text>
            </Col>
            <Col span={1}>:</Col>
            <Col span={3} style={textAlign}>
              <Typography.Text strong>{dataSubmit?.transaction_amount ? ubahFormatAngka(dataSubmit?.transaction_amount) : 0}</Typography.Text>
            </Col>
          </Row>

          <Row gutter={4}>
            <Col span={4}>
              <Typography.Text>Terpakai (Rp)</Typography.Text>
            </Col>
            <Col span={1}>:</Col>
            <Col span={3} style={textAlign}>
              <Typography.Text>{ubahFormatAngka(terpakai)}</Typography.Text>
            </Col>
          </Row>
          
          <Row gutter={4}>
            <Col span={4}>
              <Typography.Text strong>Sisa (Rp)</Typography.Text>
            </Col>
            <Col span={1}>:</Col>
            <Col span={3} style={textAlign}>
              <Typography.Text strong>{ubahFormatAngka(sisa)}</Typography.Text>
            </Col>
          </Row>
        </Col>
      <Divider />
    </Row>
    <Row gutter={16}>
      <Col span={12}>
        <TableKategori />
      </Col>
      <Col span={12}>
        <TablePenerbit />
      </Col>
    </Row>
    <TableInvoice />
  </>);
};