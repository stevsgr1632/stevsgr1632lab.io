import React from 'react';
import "../styles/tableinvoice.scss";
import { Form,Table,Typography } from "antd";
import MocoTable from 'components/MocoTable';
import { ubahFormatAngka } from "../index.config";
import ButtonDeleteBulk from "./ButtonDeleteBulk";
import { EditableCell } from "./TableInvoiceEditCell";
import TableInvoiceColumn from './TableInvoiceColumn.js';
import { DataSubmitContext } from "../FormDataContainer";
/* eslint-disable */

export const EditableContext = React.createContext();

const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

const TableInvoice = () => {
  const { dataReducer,dipatchDataReducer, dataAfterSubmit, setdataAfterSubmit,textAlign } = React.useContext(DataSubmitContext);
  const [state,setState] = React.useState({
    loading : false,
    dataSource : [],
  });
  const [selectedRowKeys,setselectedRowKeys] = React.useState([]);

  const components = {
    body: {
      row: EditableRow,
      cell: EditableCell,
    },
  };

  React.useEffect(() => {
    if(dataAfterSubmit?.catalogs){
      const dataSource = [...dataAfterSubmit?.catalogs];
      setState(state => ({...state, 
        dataSource,
        pagination : { 
          pageSize: 10, 
          total: dataSource.length,
          hideOnSinglePage : true,
        }
      }));
    }
  },[dataAfterSubmit]);

  const handleSave = row => {
    const newData = [...state.dataSource].map(item => {
      if(item.id === row.id){
        return {...item, 
          catalog_total_amount : parseInt(row.catalog_copy) * parseInt(row.catalog_price),
          catalog_copy : row.catalog_copy,
        };
      }
      return {...item, 
        catalog_total_amount : parseInt(item.catalog_copy) * parseInt(item.catalog_price)
      };
    });
    const selectionAMount = newData.reduce((prev,current) => (prev + parseInt(current.catalog_total_amount)),0);
    dipatchDataReducer({
      type:'datasubmit',
      payload : { 
        catalogs : newData,
        catalog_selection_amount : selectionAMount,
      }
    });
    setdataAfterSubmit(dataAfterSubmit => ({ ...dataAfterSubmit, catalogs: newData }));
  };

  const onSelectChange = (rowKeys,rowArrays) => {
    setselectedRowKeys(rowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  const handleBulkDelete = () => {
    const databaru = dataAfterSubmit?.catalogs?.filter((dt,indx) => !selectedRowKeys.includes(dt.id));
    const selectionAMount = databaru.reduce((prev,current) => {
      return (prev + (parseInt(current.catalog_copy) * parseInt(current.catalog_price)))
    },0);
    setdataAfterSubmit(prev => ({ 
      ...prev, 
      catalogs : databaru,
    }));
    
    dipatchDataReducer({
      type: 'datasubmit',
      payload : {
        catalogs : databaru,
        catalog_selection_amount : selectionAMount
      }
    });
    setselectedRowKeys([]);
  };

  const columnsTable = TableInvoiceColumn(dataReducer,dipatchDataReducer, dataAfterSubmit, setdataAfterSubmit).columns.map(col => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: record => ({
        record,
        editable: col.editable,
        dataIndex: col.dataIndex,
        title: col.title,
        handleSave,
      }),
    };
  });

  return(<>
    {selectedRowKeys.length > 0 && <ButtonDeleteBulk handleBulkDelete={handleBulkDelete} />}
    <MocoTable
      {...state}
      components={components}
      rowClassName={() => 'editable-row'}
      bordered
      rowSelection={rowSelection}
      columns={columnsTable}
      summary={pageData => {
        let totalHarga = dataReducer?.dataSubmit?.catalogs?.reduce((tot,num) => tot + (parseInt(num.catalog_copy)*parseInt(num.catalog_price)),0);
        return(<>
          <Table.Summary.Row style={textAlign}>
            <Table.Summary.Cell colSpan={9}>
              <Typography.Text strong>Total (Rp)</Typography.Text>
            </Table.Summary.Cell>
            <Table.Summary.Cell>
              <Typography.Text strong>{ubahFormatAngka(totalHarga)}</Typography.Text>
            </Table.Summary.Cell>
          </Table.Summary.Row>
        </>)
      }}
    />
  </>);
};

export default TableInvoice;