import React from 'react';
import config from '../index.config';
import InputText from "components/ant/InputText";
import InputFile from "components/ant/InputFile";
import InputDate from "components/ant/InputDate";
import {FileProtectOutlined} from '@ant-design/icons';
import { DataSubmitContext } from '../JadikanBerjalan';
import { Button, Col, Popconfirm,Input,Progress } from "antd";
import { formDateDisplayValue } from "utils/initialEndpoint";
/* eslint-disable */

const FormInput = (props) => {
  const { schemaBerjalan : schema } = config;
  const {simpanable,handleSubmitToAPI} = props;
  const { dipatchDataReducer} = React.useContext(DataSubmitContext);
  const [dataFile,setdataFile] = React.useState({ progress : 0, filename : '' });
  
  const handleChange = (name,value,e) => {
    if(name === 'transaction_date') {
      value = formDateDisplayValue(value,'DD-MM-YYYY');
      dipatchDataReducer({type : 'datasubmit', payload : { transaction_date_new : value } });
    }else{
      dipatchDataReducer({type : 'datasubmit', payload : { [name] : value } });
    }
  };
  const handleUploadFile = async (name,file,data) => {
    // console.log(name,file,data); // https://hastebin.com/iqulohavef.pl
    setdataFile(prev => ({...prev, filename : data.originalFilename || ''}));
    dipatchDataReducer({type : 'datasubmit', payload : { [name] : data.file_url_download } });
  };
  const fileMaxSize = 5;
  const styleButton = {
    backgroundColor:'#4dbd74',color:'#f0f3f5',borderColor:'#4dbd74'
  };
  const styleContainer = {
    display:'grid',alignContent:'start'
  };

  const inputFileComp = () => {
    return(<>
      <InputFile name='document_spk'
        listType="picture"
        uploadURL="upload-content"
        fileMaxSize={fileMaxSize}
        accept=".pdf"
        schema={schema}
        onChange={handleUploadFile}
        onProgress={({ percent }) => {
                if (percent === 100) {
                  setTimeout(() => setdataFile(prev=> ({...prev, progress : 0})), 1000);
                }
                return setdataFile(prev=> ({...prev, progress : Math.floor(percent)}));
              }}
        otherComponent={<Input type="text" readOnly value={dataFile.filename && dataFile.filename} />}
      >
        <Button style={styleButton}>
          <FileProtectOutlined /> Pilih Berkas
        </Button>
      </InputFile>
      <small>Berkas Konten : PDF, Maks {fileMaxSize}MB</small> 
      {dataFile?.progress > 0 ? <Progress percent={dataFile?.progress} /> : null}
    </>)
  };

  return(<>
  <Col span={8}>
    <InputText name="transaction_number" schema={schema} onChange={handleChange} />
  </Col>
  <Col span={12} style={styleContainer}>
    {inputFileComp()}
  </Col>
  <Col>
    <InputDate style={{width:'100%'}} name="transaction_date" schema={schema} onChange={handleChange} />
  </Col>
  <Col style={{marginTop:'2.8em'}}>
    <Popconfirm
      title="Apakah Anda yakin ingin memproses ?"
      onConfirm={handleSubmitToAPI}
      onCancel={(e) => {return false;}}
      okText="Ya"
      cancelText="Batal"
    >
      <Button
        type="primary"
        disabled={simpanable?.disabled}
        loading={simpanable?.loading}
        style={{ margin: 0 }}
        onClick={e => {}}
      >Proses</Button>
    </Popconfirm>
  </Col>
  </>);
};

export default FormInput;