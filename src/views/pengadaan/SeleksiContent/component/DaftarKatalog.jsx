import React from 'react';
import { Modal, Form } from "antd";
import FormFilter from './FormFilter';
import queryString from "query-string";
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
import ModalDetailCatalog from "./ModalDetail";
import columnTable from "./DaftarKatalogColumn";
import useCustomReducer from "common/useCustomReducer";
import { DataSubmitContext } from "../FormDataContainer";
import DaftarKatalogEpustaka from "./DaftarKatalogEpustaka";
/* eslint-disable */

const resourceAPI = 'selection-catalog-list';
const initialData = {
  limit : 10,
  filter : {},
  state : {
    loading : false,
    dataSource: [],
    pagination : {
      pageSize : 10,
      defaultCurrent: 1,
      showLessItems: true,
      showSizeChanger: true,
    }
  },
  modalDetail : {
    loading: false, visible : false, model : {}
  },
  isAllocated : { 
    loading : false, disabled : false 
  },
  selectedRow : {
    selectedId : [], selectedArrays : []
  }
};

export default props => {
  const [form] = Form.useForm();
  const [formEpustaka] = Form.useForm();
  const { visible, setvisible } = props;
  const [reducerCatalog,reducerFunc] = useCustomReducer(initialData);
  const { setdataAfterSubmit,dataReducer,dipatchDataReducer } = React.useContext(DataSubmitContext);

  const handleModalDetail = async row => {
    try {
      reducerFunc('modalDetail',{loading : true, visible : true});
      const { data } = await oProvider.list(`catalog-view-detail?id=${row.id}`);
      reducerFunc('modalDetail',{loading : false, model : data});
    } catch (error) {
      console.log(error?.message);
    }
  }
  const handleTableChange = (pagination, filter, sorter) => {
    reducerFunc('state',{pagination, filter, sorter});
  };

  // selection table
  const onSelectChange = async (selectedId,selectedArrays) => {
    if(!dataReducer.dataSubmit.epustaka_id){
      alert('Silahkan pilih Epustaka terlebih dahulu !');
      formEpustaka.validateFields().then(async value => {
        // console.log(value);
      }).catch(e => console.log(e?.message));
    }else{
      reducerFunc('selectedRow',{ selectedId, selectedArrays });
      reducerFunc('isAllocated',{ disabled : false });
    }
  };
  const rowSelection = {
    selectedRowKeys: reducerCatalog?.selectedRow?.selectedId,
    onChange: onSelectChange,
  };
  const onShowSizeChange = (current, pageSize) => {
    reducerFunc('limit',pageSize,'conventional');
  };
  
  // filter not duplicate object by key in this case id
  const removeDuplicateObjectFromArray = (array, key) => {
    return array.filter((obj, index, self) =>
      index === self.findIndex((el) => (
          el[key] === obj[key]
      ))
    )
  }
  /* 
  merging all array become 1 array object
  filter for just not null object
  */
  const flatten = (arr) => {
    let merginArr = arr.reduce(function (flat, toFlatten) {
      return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
    }, []).filter(x => Boolean(x));
    return removeDuplicateObjectFromArray(merginArr,'id');
  };

  const handleAlokasi = (e) => {
    e.preventDefault();
    formEpustaka.validateFields().then( async value => {
      if(value){
        try {
          reducerFunc('isAllocated',{ loading : true });
          // modify selected data and insert to API
          let arrayFlatten;
          if(dataReducer.dataSubmit.catalogs){
            arrayFlatten = flatten([...dataReducer.dataSubmit.catalogs, ...flatten(getArrayCatalog())]);
          }else{
            arrayFlatten = flatten(getArrayCatalog());
          }
          const newCatalog = {
            ...dataReducer.dataSubmit,
            catalogs : arrayFlatten.map(x => ({
              ...x, 
              catalog_copy : x?.catalog_copy ?? 1, 
              catalog_id : x.id,
              epustaka_id : dataReducer.dataSubmit.epustaka_id,
              epustaka_name : dataReducer.dataSubmit.epustaka_name
            })), 
            catalog_selection_amount : arrayFlatten.reduce((prev,current) => (prev + current.catalog_price),0),
          };
          
          await oProvider.insert('selection-planning-cart',newCatalog);
          let { data } = await oProvider.list('selection-planning-cart-getone');
          dipatchDataReducer({type : 'datasubmit', payload : data });
          setdataAfterSubmit(prev => ({ ...prev, ...data }));
          reducerFunc('isAllocated',{ loading : false, disabled : true });
        } catch (error) {
          console.log(error?.message);
        }
      }
    }).catch(error => {
      console.log(error?.message);
      return false;
    });
  };

  // refresh datatable
  const refreshCatalog = async () => {
    reducerFunc('state',{loading: true});
    let page = reducerCatalog?.state?.pagination?.current ?? 1;
    let paramsObj = {};
    paramsObj['page'] = page; 
    paramsObj['limit'] = reducerCatalog?.limit;
    paramsObj['sortBy'] = 'catalog_epublish_date';
    paramsObj['sortDir'] = 'DESC';
    paramsObj['ilibrary_id'] = dataReducer?.dataSubmit?.ilibrary_id;

    Object.keys(reducerCatalog?.filter).forEach(x => {
      if(reducerCatalog?.filter[x]){
        paramsObj[x] = reducerCatalog?.filter[x];
      }
    });
    
    const params = queryString.stringify(paramsObj,{skipEmptyString : true});
    try {
      const { meta, data } = await oProvider.list(`${resourceAPI}?${params}`);
      const pagination = { 
        ...reducerCatalog?.state?.pagination,
        pageSize: reducerCatalog?.limit,
        total: parseInt(meta.total),
        onShowSizeChange,
        position: ['bottomRight']
      };
      reducerFunc('state',{pagination, dataSource: data, loading: false});
      let selectedId = getArrayCatalog()[(page-1)].map(x => x.id);
      reducerFunc('selectedRow',{selectedId});
    } catch (error) {
      reducerFunc('state',{ loading: false});
      console.log(error?.message);
      return false;
    }
  };

  const getArrayCatalog = () => {
    const existing = localStorage.getItem('arraycatalog');
    if(existing) {
      return JSON.parse(existing);
    }
    return [];
  };

  React.useEffect(() => {
    if(dataReducer?.dataSubmit?.ilibrary_id){
      refreshCatalog();
    }
  }, [
    reducerCatalog?.limit,
    reducerCatalog?.state.pagination.current,
    Object.values(reducerCatalog?.filter).join(),
    dataReducer?.dataSubmit?.ilibrary_id
  ]);

  React.useEffect(() => {
    let page = (reducerCatalog?.state?.pagination?.current ?? 1) - 1;
    let prevArray = getArrayCatalog();
    prevArray[page] = reducerCatalog?.selectedRow?.selectedArrays;
    
    dipatchDataReducer({type : 'arraycatalog', payload : prevArray });
  },[reducerCatalog?.selectedRow.selectedArrays]);

  const propsFormFilter = { reducerCatalog,reducerFunc };
  const handleCloseDaftarKatalog = () => setvisible(false);
  const propsEpustaka = { 
    ...propsFormFilter,
    handleCloseDaftarKatalog,handleAlokasi,
    arraycatalog:flatten(getArrayCatalog()),
  };

  return (<>
  <Modal
    width="80vw"
    title={<h3>Daftar Katalog</h3>}
    visible={visible}
    onCancel={handleCloseDaftarKatalog}
    footer={null}
  >
    <Form form={form} initialValues={{harga : 0, harga_2 : 0}}>
      <FormFilter {...propsFormFilter} />
    </Form>
    <DaftarKatalogEpustaka {...propsEpustaka} />
    <MocoTable 
      {...reducerCatalog?.state}
      scroll={{ y: 500 }}
      onChange={handleTableChange}
      columns={columnTable(handleModalDetail).columns}
      rowSelection={rowSelection}
    />
  </Modal>
  <ModalDetailCatalog {...reducerCatalog?.modalDetail} setModal={reducerFunc} />
  </>);
};