import React from 'react';
import '../styles/daftarkatalog.scss';
import { Form, Space, Button } from "antd";
import EpustakaSelection from './EpustakaSelection';
import { DataSubmitContext } from "../FormDataContainer";
/* eslint-disable */

export default (props) => {
  const [form] = Form.useForm();
  const {dataReducer, dipatchDataReducer} = React.useContext(DataSubmitContext);
  const {handleCloseDaftarKatalog,reducerCatalog,handleAlokasi,reducerFunc,arraycatalog} = props;
  const handleReset = () => {
    reducerFunc('selectedRow',{selectedId : [], selectedArrays : []});
    dipatchDataReducer({type : 'arraycatalog', payload : [] });
  };
  
  React.useEffect(() => {
    if(dataReducer?.dataSubmit?.epustaka_id){
      form.setFieldsValue({epustaka_id : dataReducer?.dataSubmit?.epustaka_id});
    }
  },[dataReducer?.dataSubmit?.epustaka_id]);

  return(
    <Form form={form} className="form__epustaka">
      <div className="form__epustaka--wrapper">
        <Space direction="horizontal">
          <Button type="dashed" danger onClick={handleCloseDaftarKatalog}>Tutup</Button>
          {
            reducerCatalog?.selectedRow?.selectedId.length > 0 && <>
            <Button 
              type="primary" 
              danger 
              loading={reducerCatalog?.isAllocated.loading} 
              disabled={reducerCatalog?.isAllocated.disabled} 
              onClick={handleAlokasi}
              >Alokasi</Button>
            <span className="form__epustaka--help">* Silahkan alokasikan sebelum beralih halaman</span>
            </>
          }
        </Space>
        <EpustakaSelection />
      </div>
      <Space direction="horizontal">
        <span><strong>{arraycatalog.length ? arraycatalog.length : 0}</strong> item dipilih</span>
        <Button type="default" size="small" danger onClick={handleReset}>Reset</Button>
      </Space>
    </Form>
  );
}
