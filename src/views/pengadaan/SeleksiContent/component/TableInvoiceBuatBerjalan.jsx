import React from 'react';
import "../styles/tableinvoice.scss";
import { Table,Typography } from "antd";
import MocoTable from 'components/MocoTable';
import { ubahFormatAngka } from "../index.config";
import { DataSubmitContext } from "../JadikanBerjalan";
import TableInvoiceColumn from './TableInvoiceColumnBuatBerjalan';
/* eslint-disable */

const TableInvoice = () => {
  const { dataReducer : { dataSubmit }, textAlign } = React.useContext(DataSubmitContext);
  const [state,setState] = React.useState({
    loading : false,
    dataSource : [],
  });

  React.useEffect(() => {
    if(dataSubmit?.catalogs){
      setState(state => ({...state, 
        dataSource : dataSubmit?.catalogs,
        pagination : { 
          pageSize: 10, 
          total: dataSubmit?.catalogs?.length,
          hideOnSinglePage : true,
        }
      }));
    }
  },[dataSubmit?.catalogs]);

  return(<>
    <MocoTable
      {...state}
      bordered
      columns={TableInvoiceColumn().columns}
      summary={pageData => {
        let totalHarga = dataSubmit?.catalogs?.reduce((tot,num) => tot + (parseInt(num.catalog_copy)*parseInt(num.catalog_price)),0);
        return(<>
          <Table.Summary.Row style={textAlign}>
            <Table.Summary.Cell colSpan={8}>
              <Typography.Text strong>Total (Rp)</Typography.Text>
            </Table.Summary.Cell>
            <Table.Summary.Cell>
              <Typography.Text strong>{isNaN(totalHarga) ? 0 : ubahFormatAngka(totalHarga)}</Typography.Text>
            </Table.Summary.Cell>
          </Table.Summary.Row>
        </>)
      }}
    />
  </>);
};

export default TableInvoice;