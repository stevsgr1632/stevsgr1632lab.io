import React from 'react';
import MocoTable from 'components/MocoTable';
import { Card,Typography,Table } from "antd";
import { ubahFormatAngka } from "../index.config";
import { DataSubmitContext } from "../JadikanBerjalan";
/* eslint-disable */

const TableKategori = () => {
  const { dataReducer : { dataSubmit },textAlign } = React.useContext(DataSubmitContext);
  const [state,setState] = React.useState({
    loading : false,
    dataSource : []
  });

  const columnsRender = [
    {
      title: () => <Typography.Text strong>Penerbit</Typography.Text>,
      dataIndex: 'organization_name',
      key: 'organization_name',
    },
    {
      title: () => <Typography.Text strong>Copy</Typography.Text>,
      dataIndex: 'catalog_copy',
      key: 'catalog_copy',
      render : x => <div style={textAlign}>{x}</div>
    },
    {
      title: () => <Typography.Text strong>Nominal (Rp)</Typography.Text>,
      align : 'right',
      dataIndex: 'transaction_amount',
      key: 'transaction_amount',
      render : (x,row,index) => ubahFormatAngka(x)
    },
  ];

  React.useEffect(() => {
    if(dataSubmit?.publishers){

      // merging object dengan nama organisasi yang sama dan menggabungkan value tiap2 key
      const mergeArray = dataSubmit?.publishers.reduce(function(o, cur) {

        // Get the index of the key-value pair.
        var occurs = o.reduce(function(n, item, i) {
          return (item.organization_name === cur.organization_name) ? i : n;
        }, -1);
      
        // If the name is found,
        if (occurs >= 0) {
      
          // append the current value to its list of values.
          o[occurs].transaction_amount = parseInt(o[occurs].transaction_amount) + parseInt(cur.transaction_amount);
          o[occurs].catalog_qty = parseInt(o[occurs].catalog_qty) + parseInt(cur.catalog_qty);
          o[occurs].catalog_copy = parseInt(o[occurs].catalog_copy) + parseInt(cur.catalog_copy);
          // Otherwise,
        } else {
      
          // add the current item to o (but make sure the value is exist).
          var obj = {...cur,
            organization_name: cur?.organization_name,
            transaction_amount: cur?.transaction_amount,
            catalog_qty: cur?.catalog_qty,
            catalog_copy: cur?.catalog_copy,
          };
          o = o.concat([obj]);
        }
      
        return o;
      }, []);

      setState(prev => ({ ...prev, 
        dataSource : mergeArray,
        pagination : { 
          pageSize: 10, 
          total: mergeArray?.length,
          hideOnSinglePage : true,
        }
      }));
    }
  },[dataSubmit?.publishers]);

  return(
  <Card>
    <MocoTable 
      {...state}
      bordered
      columns={columnsRender}
      summary={pageData => {
        let totalHarga = dataSubmit?.publishers?.reduce((total,num) => total+ parseInt(num.transaction_amount),0);
        return(
        <Table.Summary.Row style={textAlign}>
          <Table.Summary.Cell colSpan={2}>
            <Typography.Text strong>Total (Rp)</Typography.Text>
          </Table.Summary.Cell>
          <Table.Summary.Cell>
            <Typography.Text strong>{isNaN(totalHarga) ? 0 : ubahFormatAngka(totalHarga)}</Typography.Text>
          </Table.Summary.Cell>
        </Table.Summary.Row>)
      }}
    />
  </Card>);
};

export default TableKategori;