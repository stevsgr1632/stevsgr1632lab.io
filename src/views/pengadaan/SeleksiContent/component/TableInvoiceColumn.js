import React from 'react';
import { Popconfirm, Button } from 'antd';
import MessageCostm from "common/message";
import { DeleteOutlined } from '@ant-design/icons';
import TableInvoiceColBase from "./TableInvoiceColBase";
/* eslint-disable */

export default (...props) => {
  const [dataReducer,dipatchDataReducer, dataAfterSubmit, setdataAfterSubmit] = props;

  return {
    columns : [
      ...TableInvoiceColBase,
      {
        title: '',
        key: 'operation',
        align : 'center',
        width: 100,
        render: (row,arr,index) => (
            <Popconfirm
              title="Apakah Anda yakin menghapus ?"
              onConfirm={() => {
                let databaru = dataAfterSubmit?.catalogs?.filter((dt,indx) => dt.id !== row.id);
                const selectionAMount = databaru.reduce((prev,current) => {
                  return (prev + (parseInt(current.catalog_copy) * parseInt(current.catalog_price)))
                },0);
                setdataAfterSubmit(prev => ({ 
                  ...prev, 
                  catalogs : databaru,
                }));
                
                dipatchDataReducer({
                  type: 'datasubmit',
                  payload : {
                    ...dataReducer.dataSubmit,
                    catalogs : databaru,
                    catalog_selection_amount : selectionAMount
                  }
                });
                MessageCostm({type:'success',text:'Sukses menghapus catalog'});
              }}
              onCancel={(e) => {return false;}}
              okText="Ya"
              cancelText="Batal"
              icon={<DeleteOutlined />}
            >
              <Button
                type="text"
                style={{ margin: 0 }}
                onClick={e => {}}
                icon={<DeleteOutlined />}
              />
            </Popconfirm>
          )
      },
    ]
  };
}