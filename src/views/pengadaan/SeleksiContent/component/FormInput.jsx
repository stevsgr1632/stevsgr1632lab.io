import React from 'react';
import { Col } from "antd";
import '../styles/forminput.scss';
import config from '../index.config';
import InputText from "components/ant/InputText";
import InputNumber from "components/ant/InputNumber";
import InputSelect from "components/ant/InputSelect";
import { FormAddContext,DataSubmitContext } from '../FormDataContainer';
/* eslint-disable */

const FormInput = () => {
  const { schema } = config;
  const formDataContext = React.useContext(FormAddContext);
  const { dipatchDataReducer} = React.useContext(DataSubmitContext);
  
  const handleChange = (name,value,e) => {
    if(name === 'transaction_amount') value = e.target ? e.target?.attributes['aria-valuenow']['nodeValue'] : 0;
    dipatchDataReducer({type : 'datasubmit', payload : { [name] : value } });
  };
  return(<>
  <Col span={8}>
    <InputSelect name="ilibrary_id" schema={schema} data={formDataContext?.ilibrary} others={{showSearch : true}} onChange={handleChange} />
  </Col>
  <Col span={8}>
    <InputSelect name="source_of_funds_id" schema={schema} data={formDataContext?.sumberdana} others={{showSearch : true}} onChange={handleChange} />
  </Col>
  <Col span={8}>
    <InputNumber name="transaction_amount" schema={schema} onBlur={handleChange} others={{
      min: 0,
      formatter : value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ','),
      parser : value => value.replace(/(,*)/g, ''),
    }} />
  </Col>
  <Col span={8}>
    <InputSelect name="procurement_type_id" schema={schema} data={formDataContext?.jenispengadaan} others={{showSearch : true}} onChange={handleChange}/>
  </Col>
  <Col span={16}>
    <InputText name="transaction_note" schema={schema} onBlur={handleChange} />
  </Col>
  </>);
};

export default FormInput;