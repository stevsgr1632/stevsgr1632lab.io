/* eslint-disable */
export default {
  organization_group_id: {
    label: 'Institusi',
  },
  organization_id: {
    label: 'Penerbit',
  },
  category_id: {
    label: 'Kategori',
  },
  publish_date: {
    label: 'Tahun Terbit',
  },
  harga : {
    label: 'Harga',
    rules: [
      { required: true, message: '' },
      // ({getFieldValue}) => ({
      //   validator(rule, value) {
      //     // console.log(rule,value,params);
      //     let errorMsg = 'Data input yang Anda masukkan harus berupa angka !';
      //     let regex = regexList('numeric');
      //     if (value.match(regex)) {
      //       return Promise.resolve();
      //     } else {
      //       return Promise.reject(errorMsg);
      //     }
      //   },
      // }),
    ]
  },
  harga_2 : {
    label: '',
    rules: [
      // { required: true, message: '' },
      // ({getFieldValue}) => ({
      //   validator(rule, value) {
      //     // console.log(rule,value,params);
      //     let errorMsg = 'Data input yang Anda masukkan harus berupa angka !';
      //     let regex = regexList('numeric');
      //     if (value.match(regex)) {
      //       return Promise.resolve();
      //     } else {
      //       return Promise.reject(errorMsg);
      //     }
      //   },
      // }),
    ]
  },
  search : {
    label: 'Pencarian',
  },
  epustaka_id : {
    label: 'Epustaka',
    rules: [
      { required: true, message: 'Silahkan pilih epustaka dahulu' },
    ]
  },
};