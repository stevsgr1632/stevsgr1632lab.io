import React from 'react';
import { Popconfirm, Button } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
/* eslint-disable */

export default props => (
  <Popconfirm
    title="Apakah Anda yakin menghapus semua ?"
    onConfirm={props.handleBulkDelete}
    onCancel={(e) => {return false;}}
    okText="Ya"
    cancelText="Batal"
    icon={<DeleteOutlined />}
  >
    <Button
      type="text"
      danger
      loading={props.loading ?? false}
      style={{ margin: 0 }}
      onClick={e => {}}
      icon={<DeleteOutlined />}
    >HAPUS</Button>
  </Popconfirm>
);