import React from 'react';
import MocoTable from 'components/MocoTable';
import { Card,Typography,Table } from "antd";
import { ubahFormatAngka } from "../index.config";
import { DataSubmitContext } from "../JadikanBerjalan";
/* eslint-disable */

const TableKategori = () => {
  const { dataReducer : { dataSubmit },textAlign } = React.useContext(DataSubmitContext);
  const [state,setState] = React.useState({
    loading : false,
    dataSource : []
  });

  const columnsRender = [
    {
      title: () => <Typography.Text strong>Kategori</Typography.Text>,
      dataIndex: 'category_name',
      key: 'category_name',
    },
    {
      title: () => <Typography.Text strong>Nominal (Rp)</Typography.Text>,
      align : 'right',
      dataIndex: 'total_amount',
      key: 'total_amount',
      render : (x,row,index) => ubahFormatAngka(x)
    },
  ];

  React.useEffect(() => {
    if(dataSubmit?.categories){
      setState(prev => ({ ...prev, 
        dataSource : dataSubmit?.categories,
        pagination : { 
          pageSize: 10, 
          total: dataSubmit?.categories?.length,
          hideOnSinglePage : true,
        }
      }));
    }
  },[dataSubmit?.categories]);

  return(
  <Card>
    <MocoTable 
      {...state}
      bordered
      columns={columnsRender}
      summary={pageData => {
        let totalHarga = dataSubmit?.categories?.reduce((total,num) => total+ parseInt(num.total_amount),0);
        return(
        <Table.Summary.Row style={textAlign}>
          <Table.Summary.Cell>
            <Typography.Text strong>Total (Rp)</Typography.Text>
          </Table.Summary.Cell>
          <Table.Summary.Cell>
            <Typography.Text strong>{isNaN(totalHarga) ? 0 : ubahFormatAngka(totalHarga) }</Typography.Text>
          </Table.Summary.Cell>
        </Table.Summary.Row>)
      }}
    />
  </Card>);
};

export default TableKategori;