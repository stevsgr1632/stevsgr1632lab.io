import React from 'react';
import MocoTable from 'components/MocoTable';
import { Card, Typography,Table } from "antd";
import { ubahFormatAngka } from "../index.config";
import { DataSubmitContext } from "../FormDataContainer";
/* eslint-disable */

const TablePenerbit = () => {
  const { dataAfterSubmit,tableParseData,textAlign } = React.useContext(DataSubmitContext);
  const [state,setState] = React.useState({
    loading : false,
    dataSource : []
  });

  const columnsRender = (() => {
    return {
    columns : [
      {
        title: () => <Typography.Text strong>Penerbit</Typography.Text>,
        dataIndex: 'organization_name',
        key: 'organization_name',
      },
      {
        title: () => <Typography.Text strong>Nominal (Rp)</Typography.Text>,
        align:'right',
        key: 'catalog_price',
        dataIndex: 'catalog_price',
        render : (x,row,index) => ubahFormatAngka(x)
      },
    ]};
  })();

  React.useEffect(() => {
    if(dataAfterSubmit?.catalogs){
      let dataSource = tableParseData(dataAfterSubmit?.catalogs,'organization_name');
      setState(prev => ({ ...prev, 
        dataSource,
        pagination : { 
          pageSize: 10, 
          total: dataSource.length,
          hideOnSinglePage : true,
        }
      }));
    }
  },[dataAfterSubmit]);

  return(
  <Card title='Pembelian Per Penerbit'>
    <MocoTable 
      {...state} 
      bordered
      columns={columnsRender.columns}
      summary={pageData => {
        let totalHarga = state?.dataSource?.reduce((total,num) => total+parseInt(num.catalog_price),0);
        return(
        <Table.Summary.Row style={textAlign}>
          <Table.Summary.Cell>
            <Typography.Text strong>Total (Rp)</Typography.Text>
          </Table.Summary.Cell>
          <Table.Summary.Cell>
            <Typography.Text strong>{ubahFormatAngka(totalHarga)}</Typography.Text>
          </Table.Summary.Cell>
        </Table.Summary.Row>)
      }}
    />
  </Card>);
};

export default TablePenerbit;