import React from 'react';
import { initialColor } from "../index.config";
/* eslint-disable */

const columnTable = (...params) => {
  const [handleModalDetail] = params;
  return {
    columns : [
      {
        title: "ISBN / eISBN",
        key: "catalog_isbn",
        render: row => <span>{row.catalog_isbn} / {row.catalog_eisbn}</span>,
      },
      {
        title: "Judul Buku",
        key: "catalog_title",
        dataIndex : "catalog_title",
        render: (x,row) => <div onClick={() => handleModalDetail(row)} style={{color:initialColor.blue, cursor:'pointer'}}>{x}</div>
      },
      {
        title: "Tahun",
        key: "catalog_publish_date",
        dataIndex : "catalog_publish_date",
        render: x => x
      },
      {
        title: "Kategori",
        key: "category_name",
        dataIndex : "category_name",
        render: x => x
      },
      {
        title: "Penerbit",
        key: "organization_name",
        dataIndex : "organization_name",
        render: x => x
      },
      {
        title: "Harga",
        key: "catalog_price",
        dataIndex : "catalog_price",
        render: x => (
          <span>
            <div>{parseFloat(x).toLocaleString("id-ID")}</div>
          </span>
        )
      },
    ]
  };
};

export default columnTable;