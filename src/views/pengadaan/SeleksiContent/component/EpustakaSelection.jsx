import React from 'react';
import moment from 'moment';
import { Row, Typography } from 'antd';
import "../styles/epustakaselect.scss";
import schema from './SchemaDaftarKatalog';
import InputSelect from "components/ant/InputSelect";
import { FormAddContext, DataSubmitContext } from "../FormDataContainer";
/* eslint-disable */

const EpustakaSelection = () => {
  const formDataContext = React.useContext(FormAddContext);
  const { dipatchDataReducer } = React.useContext(DataSubmitContext);
  const [epustaka,setepustaka] = React.useState("");

  //pemanggilan api selection-catalog-list : tanyakan key n value number_of_copy
  const handleSelectChange = (name,value,e) => {
    setepustaka(e?.children);
    dipatchDataReducer({
      type:'datasubmit', 
      payload : {
        [name] : value,
        epustaka_name : e?.children,
        transaction_date: moment().format('YYYY-MM-DD HH:mm:ss') 
      } 
    });
  };

  return(<>
  <div id="epustaka_id"></div>
  <Row className="cs__epustaka--wrapper">
    <span className="cs__epustaka--label">Anda memilih epustaka : <Typography.Text strong>{epustaka} &nbsp;</Typography.Text></span>
    <InputSelect 
      name="epustaka_id" 
      labelCol={17}
      schema={schema} 
      onChange={handleSelectChange} 
      data={formDataContext?.epustaka} 
      others={{ getPopupContainer:() => document.getElementById('epustaka_id'),showSearch:true, style: { width : '20em'} }}
    />
  </Row>
  </>)
};

export default EpustakaSelection;