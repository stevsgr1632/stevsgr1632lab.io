import React from 'react';
import { Typography } from 'antd';
import { ubahFormatAngka,textAlign } from "../index.config";
/* eslint-disable */

export default [
  {
    title: () => <Typography.Text strong>eISBN</Typography.Text>,
    dataIndex: 'catalog_eisbn',
    key: 'catalog_eisbn',
  },
  {
    title: () => <Typography.Text strong>ePustaka</Typography.Text>,
    dataIndex: 'epustaka_name',
    key: 'epustaka_name',
    sorter : (a, b) => a.epustaka_name < b.epustaka_name,
  },
  {
    title: () => <Typography.Text strong>Judul Buku</Typography.Text>,
    dataIndex: 'catalog_title',
    key: 'catalog_title',
    sorter : (a, b) => a.catalog_title < b.catalog_title,
  },
  {
    title: () => <Typography.Text strong>Tahun</Typography.Text>,
    dataIndex: 'catalog_publish_date',
    key: 'catalog_publish_date',
    sorter: (a, b) => a.catalog_publish_date - b.catalog_publish_date,
    render : x => <div style={textAlign}>{x}</div>
  },
  {
    title: () => <Typography.Text strong>Kategori</Typography.Text>,
    dataIndex: 'category_name',
    key: 'category_name',
    sorter : (a, b) => a.category_name < b.category_name,
  },
  {
    title: () => <Typography.Text strong>Penerbit</Typography.Text>,
    dataIndex: 'organization_name',
    key: 'organization_name',
    sorter : (a, b) => a.organization_name < b.organization_name,
  },
  {
    title: () => <Typography.Text strong>Copy</Typography.Text>,
    dataIndex: 'catalog_copy',
    key: 'catalog_copy',
    editable: true,
    render : x => <div style={textAlign}>{ubahFormatAngka(x)}</div>
  },
  {
    title: () => <Typography.Text strong>Harga (Rp)</Typography.Text>,
    align : 'right',
    key: 'catalog_price',
    dataIndex: 'catalog_price',
    render : x => <div style={textAlign}>{ubahFormatAngka(x)}</div>
  },
  {
    title: () => <Typography.Text strong>Total Harga (Rp)</Typography.Text>,
    align : 'right',
    key: 'catalog_total_amount',
    render : (x,row,index) => {
      let subharga = parseInt(x.catalog_price)*parseInt(x.catalog_copy);
      return <div style={textAlign}>{ubahFormatAngka(subharga)}</div>
    }
  },
];