import React from 'react';
import { Row, Col, Space } from "antd";
import schema from './SchemaDaftarKatalog';
import oProvider from 'providers/oprovider';
import InputNumber from "components/ant/InputNumber";
import InputSelect from "components/ant/InputSelect";
import InputSearch from "components/ant/InputSearch";
import { FormAddContext } from "../FormDataContainer";
/* eslint-disable */

const FormFilter = props => {
  const formDataContext = React.useContext(FormAddContext);
  const { reducerFunc } = props;
  const [penerbitopt,setpenerbitopt] = React.useState([]);

  const filterFunc = (name,value) => {
    reducerFunc('filter',{ [name] : value });
  };

  const handleChange = async (name,value,e) => {
    switch (name) {
      case 'organization_group_id':
        try {
          oProvider.list(`organization-dropdown?showAll=1&id=${value}`)
          .then(({ data: list }) => {
            list.unshift({ value: '', text: '-- Semua Penerbit --' });
            setpenerbitopt(list);
            filterFunc(name,value);
          });
        } catch (error) {
          console.log(error?.message);
          return false;
        }
        break;
      case 'harga':
        if(!isNaN(parseInt(value))) {
          value = e.target ? e.target?.attributes['aria-valuenow']['nodeValue'] : 0;
          value = parseInt(value);
          filterFunc(name,value);
        }else{
          return false;
        }
        break;
      case 'harga_2':
        if(!isNaN(parseInt(value))) {
          value = e.target ? e.target?.attributes['aria-valuenow']['nodeValue'] : 0;
          value = parseInt(value);
          filterFunc(name,value);
        }else{
          return false;
        }
        break;
      default:
        filterFunc(name,value);
        break;
    }
  };

  return(
    <Row gutter={16}>
      <Col span={8}>
        <div id="organization_group_id"></div>
        <InputSelect 
          schema={schema} 
          name="organization_group_id" 
          onChange={handleChange} 
          data={formDataContext?.organizatiogroup} 
          others={{getPopupContainer:() => document.getElementById('organization_group_id'), showSearch:true}} 
        />
      </Col>
      <Col span={8}>
        <div id="organization_id"></div>
        <InputSelect 
          schema={schema} 
          name="organization_id" 
          onChange={handleChange} 
          data={penerbitopt} 
          others={{getPopupContainer:() => document.getElementById('organization_id'), showSearch:true}} 
        />
      </Col>
      <Col span={8}>
        <div id="category_id"></div>
        <InputSelect 
          schema={schema} 
          name="category_id" 
          onChange={handleChange} 
          data={formDataContext?.categories} 
          others={{getPopupContainer:() => document.getElementById('category_id'), showSearch:true}}
        />
      </Col>
      <Col span={4}>
        <div id="publish_date"></div>
        <InputSelect 
          schema={schema} 
          name="publish_date" 
          onChange={handleChange} 
          data={formDataContext?.thnanggaran} 
          others={{getPopupContainer:() => document.getElementById('publish_date'), showSearch:true}}
        />
      </Col>
      <Col span={8} style={{display:'flex'}}>
        <Space direction="horizontal">
          <InputNumber
            schema={schema} 
            style={{textAlign:'right'}}
            name="harga" 
            onBlur={handleChange} 
            others={{
              min: 0,
              formatter : value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ','),
              parser : value => value.replace(/(,*)/g, ''),
            }} 
          />
          <span> s/d </span>
          <div style={{transform:'translateY(1.4em)'}}>
            <InputNumber
              schema={schema}
              style={{textAlign:'right'}}
              name="harga_2"
              onBlur={handleChange}
              others={{
                min: 0,
                formatter : value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                parser : value => value.replace(/(,*)/g, ''),
              }} 
            />
          </div>
        </Space>
      </Col>
      <Col span={12}>
        <InputSearch 
          schema={schema} 
          name="search"
          style={{transform:'translateY(.68em)'}}
          placeholder="Cari berdasarkan judul"
          onSearch={handleChange}
        />
      </Col>
    </Row>
  )
};

export default FormFilter;