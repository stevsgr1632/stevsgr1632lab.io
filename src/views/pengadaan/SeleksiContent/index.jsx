import React from 'react';
import { Col, Row,
  Form,DatePicker } from 'antd';
import ListForm from './FormList';
import config from './index.config';
import { connect } from 'react-redux';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import { Breadcrumb, Content } from 'components';
import InputSelect from 'components/ant/InputSelect';
import InputSearch from 'components/ant/InputSearch';
import getMenuAkses,{ getOneMenu } from "utils/getMenuAkses";
/* eslint-disable */

export const MenuAksesContext = React.createContext([]);

const App = (props) => {
  const [form] = Form.useForm();
  const defCol = {xs:24, sm:12, md:12, lg:8, xl:8};
  const { rab : { selectcontent } } = pathName;
  const { 
    list : { content, breadcrumb },
    schema } = config;
  const [status,setstatus] = React.useState([]);
  const [menuAkses, setmenuAkses] = React.useState([]);
  const [model, setModel] = React.useState(config.model);

  const handleChange = (name, value) => {
    setModel(model => ({ ...model, [name]: value }));
  };
  const handleDateChange = async (_, newDates) => {
    setModel(model => ({ ...model, 'start_date': newDates[0], 'end_date': newDates[1] }));
  };

  React.useEffect(() => {
    if(props.menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(selectcontent.list);
      setmenuAkses(menuAccessFromLocalStorage);
    }    
  },[props.menuAccess]);

  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const {data : dataku} = await oProvider.list('status-dropdown?process_name=CATALOG_SELECTION');
        dataku.unshift({ value: '', text: '-- Semua Status --'});
        setstatus(dataku);
      } catch(error) {
        console.log(error?.message);
        return false;
      }
    };
    fetchData();
  },[]);

  return (
    <React.Fragment>
    <MenuAksesContext.Provider value={menuAkses}>
      <Breadcrumb items={breadcrumb} />
      <Content {...{...props,...content, menuAkses}}>
      { getOneMenu(menuAkses,'List') && <></>}
        <Form form={form}>
          <Row gutter={16}>
            <Col {...defCol} style={{marginTop:'0.6em'}}>
              <label>Tanggal</label>
              <br />
              <DatePicker.RangePicker style={{ width: '100%' }}
                placeholder={['Tanggal Mulai', 'Tanggal Berakhir']}
                onCalendarChange={handleDateChange} />
            </Col>
            <Col {...defCol}>
              <InputSelect name="status" schema={schema} text="Status" placeholder="Pilih status" data={status} onChange={handleChange} />
            </Col>
            <Col {...defCol} style={{marginTop:'0.6em'}}>
              <InputSearch name="search" schema={schema} text="Pencarian" placeholder="Pencarian berdasarkan nama" onSearch={handleChange} />
            </Col>
          </Row>
        </Form>
        <hr />
        <ListForm {...props} model={model} />
      </Content>
    </MenuAksesContext.Provider>
    </React.Fragment>
  );
};
const mapStateToProps = ({menuAccess}) => ({menuAccess});
export default connect(mapStateToProps)(App);