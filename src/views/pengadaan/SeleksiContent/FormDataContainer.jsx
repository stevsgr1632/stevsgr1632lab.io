import React from 'react';
import FormContainer from './Form';
import oProvider from 'providers/oprovider';
import config,{textAlign} from './index.config';
import { Content, Breadcrumb } from 'components';
import LoadingSpin from "components/LoadingSpin";
import { newDropDown } from 'providers/dropdownapi';
/* eslint-disable */

export const FormAddContext = React.createContext({});
export const DataSubmitContext = React.createContext({});

const initialData = { 
  dataSubmit : {}, 
  dataAfterSubmit : {}, 
  arraycatalog : [], 
  loadingRsc : false,
  state : {}
};
const reducerData = (state,action) => {
  switch (action.type) {
    case 'datasubmit':
      localStorage.setItem('selectedcontent',JSON.stringify({...state.dataSubmit, ...action.payload}));
      return {...state, 
        dataSubmit : {
          ...state.dataSubmit, 
          ...action.payload
        } 
      };
    case 'arraycatalog':
      // localStorage.setItem('arraycatalog', JSON.stringify([...state.arraycatalog,action.payload]));
      localStorage.setItem('arraycatalog', JSON.stringify(action.payload));
      return {...state, arraycatalog : action.payload};
    case 'loadingRsc':
      return {...state,
        [action.type] : action.payload
      };
    default:
      return {...state,
        [action.type] : {
          ...state[action.type],
          ...action.payload
        }
      };
  }
};

const FormDataContainer = props => {
  const { breadcrumb, content } = config[props.typeForm];
  const [dataAfterSubmit,setdataAfterSubmit] = React.useState({});
  const [dataReducer,dipatchDataReducer] = React.useReducer(reducerData, initialData);
  
  const reducerFunc = (type,payload) => {
    dipatchDataReducer({type,payload});
  };
  const tableParseData = (arrayData = [],groupBy,keyAmount = "catalog_price",keyCopy="catalog_copy") => {
    const result = arrayData.reduce((acc,curr,index,array) => {
      let idx = curr[groupBy]; 
        if(!acc[idx]){
          acc[idx] = array.filter(item => item[groupBy] === idx)
        } 
      return acc; 
    },{});
    
    let newArrayData = Object.keys(result).map(x => {
        let objectData = {};
        objectData[groupBy] = x;
        objectData[keyAmount] = result[x].reduce((x,y) => x + (y[keyAmount]*parseInt(y[keyCopy])),0);
        return objectData;
    });

    return newArrayData;
  };

  React.useEffect(() => {
    if(dataReducer.dataSubmit?.ilibrary_id){
      const fetchData = async () => {
        try {
          const {data : epustaka} = await oProvider.list(`epustaka-dropdown?ilibrary_id=${dataReducer.dataSubmit?.ilibrary_id}`);
          epustaka.unshift({ value: '', text: '-- Pilih ePustaka --' });
          reducerFunc('state',{epustaka});
        } catch (error) {
          console.log(error?.message);
        }
      };
      fetchData();
    }
  },[dataReducer.dataSubmit?.ilibrary_id]);

  React.useEffect(() => {
    const fetchData = async () => {
      reducerFunc('loadingRsc',true);
      try {
        const [
          {data : organizatiogroup},
          {data : ilibrary},
          {data : categories},
          {data : sumberdana},
          {data : cartData },
          {data : jenispengadaan },
        ] = await Promise.all([
          oProvider.list('organization-group-dropdown'),
          oProvider.list('ilibrary-dropdown'),
          oProvider.list('categories-dropdown'),
          oProvider.list('source-of-funds-dropdown'),
          oProvider.list('selection-planning-cart-getone'),
          oProvider.list('procurement-types-dropdown'),
        ]);

        const thnanggaran = await newDropDown('fiscal-year-dropdown');

        thnanggaran.unshift({ value: '', text: '-- Semua Tahun --' });
        ilibrary.unshift({ value: '', text: '-- Pilih iLibrary --' });
        categories.unshift({ value: '', text: '-- Semua Kategori --' });
        sumberdana.unshift({ value: '', text: '-- Pilih Sumber Dana --' });
        organizatiogroup.unshift({ value: '', text: '-- Semua Group Penerbit --' });
        jenispengadaan.unshift({ value: '', text: '-- Semua Jenis Pengadaan --' });

        setdataAfterSubmit(prev => ({...prev, ...cartData}));
        dipatchDataReducer({type : 'datasubmit' , payload : cartData});
        if(props.typeForm === 'edit'){
          let selectContentId = props.match.params.id;
          const { data } = await oProvider.list(`selection-planning-getone?id=${selectContentId}`);
          dipatchDataReducer({ type : 'datasubmit', payload : data });
          setdataAfterSubmit(data);
        }
        reducerFunc('state',{organizatiogroup,ilibrary,categories,sumberdana,thnanggaran,jenispengadaan});
        reducerFunc('loadingRsc',false);
      } catch (error) {
        console.log(error?.message);
        reducerFunc('loadingRsc',false);
        return false;
      }
    };
    fetchData();
  },[]);

  return(<>
  <Breadcrumb items={breadcrumb} />
  <Content {...content}>
    <LoadingSpin loading={dataReducer?.loadingRsc}>
      <FormAddContext.Provider value={dataReducer?.state}>
        <DataSubmitContext.Provider 
          value={{
            textAlign,
            dataReducer,
            tableParseData,
            dataAfterSubmit,
            dipatchDataReducer,
            setdataAfterSubmit,
            propsContainer : props }}>
          <FormContainer />
        </DataSubmitContext.Provider>
      </FormAddContext.Provider>
    </LoadingSpin>
  </Content>
  </>);
};

export default FormDataContainer;