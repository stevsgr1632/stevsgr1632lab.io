import React from 'react';
import FormDataContainer from "./FormDataContainer";

const FormAdd = props => {
  return(
  <FormDataContainer
    {...props}
    typeForm="add"
  />);
};

export default FormAdd;