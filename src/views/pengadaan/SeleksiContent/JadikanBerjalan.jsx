import React from 'react';
import oProvider from 'providers/oprovider';
import config,{textAlign} from './index.config';
import { Content, Breadcrumb } from 'components';
import LoadingSpin from "components/LoadingSpin";
import FormContainer from './component/FormJadikanBerjalan';
/* eslint-disable */

export const DataSubmitContext = React.createContext({});

const initialData = { dataSubmit : {}, sisa : 0, terpakai : 0, loadingRsc : true };
const reducerData = (state,action) => {
  switch (action.type) {
    case 'datasubmit':
      let terpakai,sisa;
      if(action?.payload?.transaction_amount){
        let hitungTerpakai = action?.payload?.catalogs?.reduce((total,num) => total + (parseInt(num.catalog_price) * parseInt(num.catalog_copy)),0);
        terpakai = isNaN(hitungTerpakai) ? 0 : hitungTerpakai;
        let hitungSisa = parseInt(action?.payload?.transaction_amount) - parseInt(terpakai); 
        sisa = isNaN(hitungSisa) ? 0 : hitungSisa;
      }else{
        let hitungTerpakai = state.dataSubmit?.catalogs?.reduce((total,num) => total + (parseInt(num.catalog_price) * parseInt(num.catalog_copy)),0);
        terpakai = isNaN(hitungTerpakai) ? 0 : hitungTerpakai;
        let hitungSisa = parseInt(state.dataSubmit?.transaction_amount) - parseInt(terpakai); 
        sisa = isNaN(hitungSisa) ? 0 : hitungSisa;
      }
      return {...state, 
        dataSubmit : {
          ...state.dataSubmit, 
          ...action.payload
        },sisa,terpakai
      };
    case 'loadingRsc':
      return {...state,[action.type] : action.payload};
    default:
      return {...state,
        [action.type]: { ...state[action.type], ...action.payload }
      };
  }
};

export default props => {
  const selectContentId = props.match.params.id;
  const { breadcrumb, content } = config['buatberjalan'];
  const [dataReducer,dipatchDataReducer] = React.useReducer(reducerData, initialData);

  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const [
          {data : viewData},
        ] = await Promise.all([
          oProvider.list(`selection-planning-view?id=${selectContentId}`),
        ]);
        dipatchDataReducer({type : 'datasubmit' , payload : viewData});
        dipatchDataReducer({type : 'loadingRsc', payload : false });
      } catch (error) {
        console.log(error?.message);
        dipatchDataReducer({type : 'loadingRsc', payload : false });
        return false;
      }
    };
    fetchData();
  },[]);

  return(<>
  <Breadcrumb items={breadcrumb} />
  <Content {...content}>
    <LoadingSpin loading={dataReducer?.loadingRsc}>
      <DataSubmitContext.Provider 
        value={{
          textAlign,
          dataReducer,
          dipatchDataReducer,
          propsContainer : props }}>
        <FormContainer />
      </DataSubmitContext.Provider>
    </LoadingSpin>
  </Content>
  </>);
};