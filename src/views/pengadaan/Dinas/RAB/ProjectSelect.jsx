import React, { useEffect } from 'react';
import { Row, Col, message } from 'antd';
import { Card, CardBody } from 'reactstrap';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import { Breadcrumb, Content } from 'components';
import DetailHead from './component/HeaderSeleksi';
import { numberFormat } from "utils/initialEndpoint";
import InputSelect from 'components/ant/InputSelect';
import InputSearch from 'components/ant/InputSearch';
import HalamanSeleksi from './component/HalamanSeleksi';
import SumDetailHead from './component/SumHeaderSeleksi';
/* eslint-disable */

const {base:baseUrl,rab:{dinas}} = pathName;

const App = (props) => {
  const paramsId = props.match.params.id;
  const approveKat = `budget-selected-catalog?limit=5&type=APPROVED&budgetId=${paramsId}`;
  const selectKatalog = `budget-selected-catalog?limit=5&type=SELECTED&budgetId=${paramsId}`;
  
  const [state, setState] = React.useState({});
  const [loading, setloading] = React.useState(false);
  const [selectedRowKeys, setselectedRowKeys] = React.useState([]);
  const [selectedRowSaved, setselectedRowSaved] = React.useState([]);
  const [tabKey, settabKey] = React.useState(1);
  const [url, setUrl] = React.useState('');
  const [,setselectProj] = React.useState([]);
  const [kategori,setkategori] = React.useState([]);
  const [detailHead,setdetailHead] = React.useState([]);
  const [organization,setorganization] = React.useState([]);
  const [kategoriSelect,setkategoriSelect] = React.useState('');
  const [organizationSelect,setorganizationSelect] = React.useState('');
  const [thunSelect,setthunSelect] = React.useState('');

  const pjselection = {
    breadcrumb: [
      { text: 'Beranda', to: baseUrl },
      { text: 'Rencana Anggaran Biaya (RAB)', to: dinas.list },
      { text: 'Detail', to: dinas.detail(`${paramsId}`)},
      { text: 'Project Selection'},
    ],
    content: {
      title: 'Project Selection',
    },
  }

  const onSelectChange = (selectedRowKeys,e) => {
    console.log('data e: ', e);
    let catalog_id = e.map(x => x.catalog_id);
    console.log('selected by catalog_id',catalog_id);
    setselectedRowKeys(selectedRowKeys);
    setselectedRowSaved(catalog_id);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  const refresh = async (url) => {
    setState({...state, loading: true});
    try {
      const { meta, data } = await oProvider.list(`${tabKey > 1 ? approveKat : selectKatalog}${url}`);
      setselectProj(data.filter(x => Boolean(x.selected)).map(y => y.id));
      setState({ loading: false, pagination: { pageSize:5,total: parseInt(meta.total) }, dataSource: data });
    } catch (error) {
      console.log(error?.message);
      setState({...state, loading: false});
      return false;
    }
  }

  const handleTableChange = (pagination, filters, sorter) => {
    let url = '';
    if (pagination) url += `&page=${pagination.current}`;
    if (sorter && sorter.field) {
      url += `&sortBy=${sorter.field}`;
      url += `&sortDir=${sorter.order === 'ascend' ? 'asc' : 'desc'}`;
    }
    setUrl(url);
  }

  const columns = [
    {
      title: 'ISBN / EISBN',
      key: 'catalog_isbn',
      dataIndex: 'catalog_isbn',
    },
    {
      title: 'Judul',
      key: 'catalog_title',
      dataIndex: 'catalog_title',
      render : (x,row) => x ? x : row.catalog_name
    },
    {
      title: 'Cover',
      key: 'catalog_cover',
      dataIndex: 'catalog_cover',
      render : x => {
        return (<img src={x} alt="no-data" width="50%" />)
      } 
    },
    {
      title: 'Pengarang',
      key: 'catalog_authors',
      dataIndex: 'catalog_authors',
    },
    {
      title: 'Kategori',
      key: 'category_name',
      dataIndex: 'category_name',
    },
    {
      title: 'Tanggal Terbit',
      key: 'catalog_publish_date',
      dataIndex: 'catalog_publish_date',
    },
    {
      title: 'Jumlah',
      key: 'subcategory_name',
      dataIndex: 'subcategory_name',
      render : (x,row) => x ? `${row.subcategory_name.length}` : row.catalog_qty
    },
    {
      title: 'Harga Satuan (Rp)',
      key: 'catalog_price',
      dataIndex: 'catalog_price',
      render : x => x ? numberFormat(x) : '-'
    },
    {
      title: 'Total (Rp)',
      key: 'total',
      dataIndex: 'total',
      render : x => x ? numberFormat(x) : '-'
    },
  ];

  const tahunterbit = [
    {
      text:'2010',
      value:'2010'
    },
    {
      text:'2011',
      value:'2011'
    },
    {
      text:'2012',
      value:'2012'
    },
    {
      text:'2013',
      value:'2013'
    },
    {
      text:'2014',
      value:'2014'
    },
    {
      text:'2015',
      value:'2015'
    },
    {
      text:'2016',
      value:'2016'
    },
    {
      text:'2017',
      value:'2017'
    },
    {
      text:'2018',
      value:'2018'
    },
    {
      text:'2019',
      value:'2019'
    },
    {
      text:'2020',
      value:'2020'
    },
    {
      text:'Pilih Tahun Terbit',
      value:''
    },
  ];

  const handleChangeSearch = (name, value) => {
    setUrl(`&search=${value}`);
  }
  const handleKategori = (name, value) => {
    setkategoriSelect(value);
    setUrl(`&category=${value}`);
  }
  const handleOrg = (name, value) => {
    setorganizationSelect(value);
    setUrl(`&organizationId=${value}`);
  }
  const handleTahun = (name, value) => {
    setthunSelect(value);
    setUrl(`&publishDate=${value}`);
  }
  const handleChangeTabs = key => {
    console.log(key);
    settabKey(key);
  }

  const handleConfirm = async e => {
    try {
      setloading(true);
      let payload = {};
      payload.catalogIds = selectedRowSaved.join();
      await oProvider.update(`budget-catalog-approve?budgetId=${paramsId}`,payload);
      let detailHead = await oProvider.list(`budget-detail-header?id=${paramsId}`);
      setdetailHead(detailHead.data);
      message.success('Berhasil menyetujui !');
      setselectedRowKeys([]);
      setselectedRowSaved([]);
      refresh(url);
      setloading(false);
    } catch (error) {
      console.error(error?.message);
      message.error('Gagal menyetujui ! Ada error, cek pada console.');
      setloading(false);
      return false;
    }
  }

  useEffect(() => {
    async function fetchData(){
      try {
        const [
          {data: kategori},
          {data: organization},
          {data: detailHead},
        ] = await Promise.all([
          oProvider.list(`category`),
          oProvider.list(`organization?organization_isactive=1`),
          oProvider.list(`budget-detail-header?id=${paramsId}`)
        ]);
        kategori.unshift({category_name:'Pilih Kategori',id:''});
        organization.unshift({organization_name:'Pilih Penerbit',id:''});
        setkategori(kategori.map( ({category_name:text,id:value}) => ({text,value}) ));
        setorganization(organization.map( ({organization_name:text,id:value}) => ({text,value}) ));
        setdetailHead(detailHead);
      } catch (error) {
        console.log(error?.message);
        return false;
      }
    };
    fetchData();
  }, []);

  useEffect(() => {
    refresh(url);
  }, [url,tabKey]);

  let dataPage = {
    selectedRowKeys,
    handleConfirm,
    rowSelection,
    handleTableChange,
    handleChangeTabs,
    tabKey,
    state,
    columns,
    loading,
  }

  return (<>
    <Breadcrumb items={pjselection.breadcrumb} />
    <Content {...pjselection.content}>
      <DetailHead detailHead={detailHead} />
      <hr/>
      <SumDetailHead detailHead={detailHead} />
    </Content>
    <Content>
      <Row gutter={[16,16]}>
        <Col span={8}>
          <InputSelect name="kategori" defaultVal={kategoriSelect || ''} text="Kategori" data={kategori} onChange={handleKategori} />
        </Col>
        <Col span={8}>
          <InputSelect name="organization" defaultVal={organizationSelect || ''} text="Penerbit" data={organization} onChange={handleOrg} />
        </Col>
        <Col span={8}>
          <InputSelect name="tahunterbit" defaultVal={thunSelect || ''} text="Tahun Terbit" data={tahunterbit.sort((a, b) => b.text - a.text)} onChange={handleTahun} />
        </Col>
        <Col span={24}>
          <InputSearch name="search" text="Pencarian" placeholder="Cari Berdasarkan Judul" onSearch={handleChangeSearch} />
        </Col>
      </Row>

      <Card>
        <CardBody>
          <HalamanSeleksi {...dataPage} />          
        </CardBody>
      </Card>

    </Content>
  </>);
}

export default App;