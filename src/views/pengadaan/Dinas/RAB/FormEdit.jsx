import React, { useEffect } from 'react';
import moment from 'moment';
import { message } from 'antd';

import pathName from '../../../../routes/pathName';
import oProvider from '../../../../providers/oprovider';
import Notification from '../../../../common/notification';
import { Breadcrumb, Content } from '../../../../components';
import FormControl from './Form';
import config from './index.config';
import messageConfig from './component/config';
/* eslint-disable */

const { rab: { dinas } } = pathName;
message.config(messageConfig.message);

const App = (props) => {
  const [data, setData] = React.useState({});
  const { schema } = config;
  const { breadcrumb, content } = config.edit;
  const budgetId = props.match.params.id;
  const resource = `budget-execution?id=${budgetId}`;

  const backToList = () => {
    props.history.push(dinas.list);
  };

  const getBudgetById = async (id) => {
    try {
      const { data } = await oProvider.list(`budget-get-one?id=${id}`);
      setData(data);
    } catch (error) {
      console.log(error?.message);
      return false;
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((errors, values) => {
      if (!errors) {
        console.log(values);
        backToList();
      }
    })
  }

  const openNotification = resp => {
    Notification({ response : resp });
  };

  const handleOtherSubmit = async (e) => {
    console.log('sebelum tombol save ditekan : ', e);
    if (Object.keys(e).length > 2) {
      let plans = [
        {
          "text": "Persiapan Panitia Pelaksana",
          "value": "12b47230-5b0f-11ea-9b44-a2eedb660190"
        },
        {
          "text": "Seleksi Katalog",
          "value": "12b472bb-5b0f-11ea-9b44-a2eedb660190"
        },
        {
          "text": "Persiapan Berkas",
          "value": "12b472fd-5b0f-11ea-9b44-a2eedb660190"
        },
        {
          "text": "Penerbitan Berkas",
          "value": "12b47321-5b0f-11ea-9b44-a2eedb660190"
        },
        {
          "text": "Aktivasi",
          "value": "12b47354-5b0f-11ea-9b44-a2eedb660190"
        },
        {
          "text": "Pengecekan",
          "value": "12b4737b-5b0f-11ea-9b44-a2eedb660190"
        }
      ];
      e.budget_plans = plans.map(x => {
        let dataSave = {};
        dataSave.budget_plan_status = x.value;
        dataSave.budget_plan_start_date = moment(new Date()).format('DD-MM-YYYY');
        dataSave.budget_plan_end_date = moment(new Date()).format('DD-MM-YYYY');
        return dataSave;
      });

      try {
        let responsed = await oProvider.update(`${resource}`, { ...e });
        openNotification(responsed);
        backToList();
      } catch (error) {
        console.log(error?.message);
        return false;
      }
    } else {
      message.error('Data tidak lengkap !');
      return false;
    }

    console.log('setelah tombol save ditekan : ', e);
  }

  useEffect(() => {
    getBudgetById(budgetId);
  }, []);

  schema.form = props.form;

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <FormControl
          onSubmit={handleSubmit}
          onCancel={backToList}
          schema={schema}
          data={data}
          otherSubmit={handleOtherSubmit}
          type="edit"
        />
      </Content>
    </React.Fragment>
  );
}

export default App;