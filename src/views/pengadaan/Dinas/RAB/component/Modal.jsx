import React, { useState } from 'react';
import { Modal,Table,Radio,Checkbox,message } from 'antd';
import oProvider from '../../../../../providers/oprovider';
/* eslint-disable */

const App = (props) => {
  const [confirmLoading, setconfirmLoading] = useState(false);
  const [anggota, setanggota] = useState([]);
  const [state, setState] = useState({});

  const handleOk = async () => {
    console.log(anggota);
    let dataBody = {
      budget_id :props.budgetId,
      librarianIds: anggota.join()
    };
    try{
      setconfirmLoading(true);
      let budgetTeamSimpan = await oProvider.insert('budget-team',dataBody);
      console.log(budgetTeamSimpan);
      message.success('Berhasil mengupdate data');
      props.refreshTeam();
      dataLookup();
      setconfirmLoading(false);
      props.close();
    }catch(error) {
      console.error(error);
    }
  };

  const handleCancel = () => {
    console.log('Clicked cancel button');
    props.close();
  };

  const dataLookup = async () => {
    let {data} = await oProvider.list(`budget-teams-lookup?id=${props.budgetId}`);
    setState({ pagination: { pageSize:5,total: data.length }, dataSource: data });
  }

  React.useEffect(() => {
    async function fetchData(){
      let {data} = await oProvider.list(`budget-teams-lookup?id=${props.budgetId}`);
      setState({ pagination: { pageSize:5,total: data.length }, dataSource: data });
      setanggota(data.filter(y=> Boolean(y.teamSelected)).map(x => x.id));
    }
    fetchData();
  },[]);

  const handleChecked = (e,row) => { 
    if(e.target.checked && (anggota.indexOf(row.id) === -1)){
      setanggota([...anggota,row.id]);
    }else{
      let filtering = anggota.filter(x => {
        return x !== row.id;
      });
      setanggota(filtering);
    }
  }

  const handleRadioInput = e => {
    console.log(`radioinput = ${e.target.checked}`);
  }

  const columns = [
    {
      title: 'Nama Member',
      key:'librarian_name',
      dataIndex: 'librarian_name',
      render: (x,row) => {
        return (
        <>
          <div style={{display:'grid',gridTemplateColumns:'repeat(2,max-content)',gridColumnGap:'1em'}}>
            <img src={row.librarian_logo || 'https://via.placeholder.com/50'} alt={row.librarian_logo || 'no-data'} />
            <div>
              <h5><strong>{x}</strong></h5>
              <small>{row.librarian_position}</small>
            </div>
          </div>
        </>
        )
      }
    },
    {
      title: 'PM',
      key:'inputpm',
      render: row => {
        return (
        <>
          <div style={{display:'grid',gridTemplateColumns:'repeat(2,max-content)',gridColumnGap:'1em'}}>
            {row.teamSelected ? (
              <Checkbox name="choices" defaultChecked onChange={(e) => handleChecked(e,row)} />
            ) : (
              <Checkbox name="choices" onChange={(e) => handleChecked(e,row)} />
            )}
            {(row.teamSelected === 1 && row.librarian_position === 'Project Manager') && (
            <Radio name="radiopm" defaultChecked onChange={e => handleRadioInput(e,row)} />
            )}
          </div>
        </>
        )
      }
    },
  ];

  return (<>
  <Modal
    title={<h4><i className="icon-plus"></i> Tambah Anggota Pengadaan</h4>}
    visible={props.visible}
    onOk={handleOk}
    okButtonProps={{ disabled: anggota.length === 0 ? true : false }}
    centered
    confirmLoading={confirmLoading}
    onCancel={handleCancel}
  >
    <React.Suspense>
    <Table columns={columns} {...state} />
    </React.Suspense>
    
  </Modal>
  </>);
}

export default App;