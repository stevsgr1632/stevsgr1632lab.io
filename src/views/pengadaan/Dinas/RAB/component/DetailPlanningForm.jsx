import React from 'react';
import moment from 'moment';
import {Row,Col} from 'antd';
import InputText from '../../../../../components/ant/InputText';
/* eslint-disable */

const App = props => {
  const {handleChangeInputPlan,text,value} = props;
  // defaultValue={moment(new Date()).format('YYYY-MM-DD')}
  const handleChange = (name,val) => {
    // moment(val).format('YYYY-MM-DD');
    console.log(name,val); //budget_plan_start_date 2020-03-31
    if(handleChangeInputPlan){
      handleChangeInputPlan(value,name,moment(val).format('YYYY-MM-DD'));
    }
  }

  return(<>
    <small>{text}</small>
    <Row gutter={[16,16]}>
      <Col span={8}>
        <InputText type="date" name="budget_plan_start_date"  onChange={handleChange} />
      </Col>
      <Col span={8}>
        <InputText type="date" name="budget_plan_end_date" onChange={handleChange} />
      </Col>
      <Col span={8}>
        <InputText type="number" disabled name="budget_totalhari" min="0" />
      </Col>
    </Row>
  </>);
}

export default App;