import React from 'react';
import { Row, Col } from 'antd';
import { Table } from 'reactstrap';
import '../styles/sumheader.scss';
import { numberFormat } from "utils/initialEndpoint";
/* eslint-disable */

const colorText = '#333';

const App = (props) => {

  return (<>
	<Row>
		<Col span={8}>
		  <h5>Detail Harga</h5>
		  <Table borderless>
		  	<tbody style={{color:colorText}}>
		  		<tr>
		  			<td className="custom-column-style" width="50%">Jumlah Judul</td>
		  			<td className="custom-column-style">:</td>
		  			<td className="custom-column-style">{props.detailHead.catalog_qty}</td>
		  		</tr>
		  		<tr>
		  			<td className="custom-column-style">Jumlah Salinan</td>
		  			<td className="custom-column-style">:</td>
		  			<td className="custom-column-style">{props.detailHead.copy_qty ? props.detailHead.copy_qty : '0'}</td>
		  		</tr>
		  		<tr>
		  			<td className="custom-column-style">Harga Gross (Rp)</td>
		  			<td className="custom-column-style">:</td>
		  			<td className="custom-column-style">{numberFormat(props.detailHead.budget_allocation)}</td>
		  		</tr>
		  		<tr>
		  			<td className="custom-column-style">PPN</td>
		  			<td className="custom-column-style">:</td>
		  			<td className="custom-column-style">{props.detailHead.ppn}%</td>
		  		</tr>
		  		<hr style={{width:"150%",borderColor:colorText}}/>
		  		<tr>
		  			<td className="custom-column-style">Total (Rp)</td>
		  			<td className="custom-column-style">:</td>
		  			<td className="custom-column-style">{numberFormat(props.detailHead.total)}</td>
		  		</tr>
		  	</tbody>
		  </Table>
		</Col>
	</Row>
  </>);
}

export default App;