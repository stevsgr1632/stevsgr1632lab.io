import React from 'react';
import {Table} from 'antd';
/* eslint-disable */

const App = (props) => {
  return (
    <>
      <Table {...props.state} columns={props.columns} dataSource={props.dataSource} />
    </>
  )
}

export default App;