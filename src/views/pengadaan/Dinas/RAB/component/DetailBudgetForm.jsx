import React from 'react';
import {Row,Col,Divider} from 'antd';
import InputText from '../../../../../components/ant/InputText';
import InputSelect from '../../../../../components/ant/InputSelect';
import InputSelectAsync from '../../../../../components/ant/InputSelectAsync';
/* eslint-disable */

const App = props => {
	const { handleChangeInput,schema,fiscalYear } = props;

	return(<>
		<Divider orientation="left"><strong>Detail Pengadaan</strong></Divider>
	  <Row gutter={[16,16]}>
	    <Col span={12}>
	      <InputText name="budget_name" placeholder="Masukkan Nama Pengadaan" onChange={handleChangeInput} schema={schema} />
	    </Col>
	    <Col span={12}>
	      <InputSelectAsync name="source_fund_id" placeholder="Pilih Sumber Dana" addressApi='source-of-funds-dropdown' onChange={handleChangeInput} schema={schema} />
	    </Col>
	    <Col span={12}>
	      <InputSelect name="fiscal_year_id" placeholder="Pilih Tahun Anggaran" data={fiscalYear} onChange={handleChangeInput} schema={schema} />
	    </Col>
	    <Col span={12}>
	      <InputText type="number" min="0" name="budget_amount" placeholder="Masukkan Rencana Dana Anggaran" onChange={handleChangeInput} schema={schema} />
	    </Col>
	    <Col span={12}>
	      <InputText type="date" name="budget_start_date" onChange={handleChangeInput} schema={schema} />
	    </Col>
	    <Col span={12}>
	      <InputText type="date" name="budget_end_date" onChange={handleChangeInput} schema={schema} />
	    </Col>
	    <Col span={12}>
	      <InputSelectAsync name="budget_status" placeholder="Pilih Status" addressApi="status-dropdown" onChange={handleChangeInput} schema={schema} />
	    </Col>
	  </Row>
	</>);
}

export default App;