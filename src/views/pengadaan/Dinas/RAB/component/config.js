export default {
  message : {
    top: 100,
    duration: 2,
    maxCount: 3,
    rtl: true,
  }
}