import React from 'react';
import { Row, Col,Card } from 'antd';
import { numberFormat } from "utils/initialEndpoint";
/* eslint-disable */

const App = (props) => {

  return (<>
    <Row gutter={[16,16]} style={{display:'grid',gridTemplateColumns:'repeat(auto-fit,minmax(22em,1fr))',alignItems:'start'}}>
		<h5 style={{margin:'0 .4em',gridColumn:'1/-1'}}>Detail Pengadaan</h5>
		<Col>
      <Card 
        style={{width:'22em'}}
        bordered={true} 
      >
        <small style={{color:'#777'}}>RENCANA DANA ANGGARAN (Rp)</small>
        <h4>{numberFormat(props.detailHead.budget_amount)}</h4>
      </Card>
		</Col>
		<Col>
      <Card 
        style={{width:'22em'}}
        bordered={true} 
      >
        <small style={{color:'#777'}}>ANGGARAN YANG TERPAKAI (Rp)</small>
		    <h4>{numberFormat(props.detailHead.total)}</h4>
      </Card>
		</Col>
		<Col>
      <Card 
        style={{width:'22em'}}
        bordered={true} 
      >
        <small style={{color:'#777'}}>ANGGARAN YANG TERSISA (Rp)</small>
		    <h4>{numberFormat(props.detailHead.budget_remaining)}</h4>
      </Card>
		</Col>
	</Row>
  </>);
}

export default App;