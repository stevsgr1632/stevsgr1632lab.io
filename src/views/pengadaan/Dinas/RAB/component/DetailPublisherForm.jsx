import React from 'react';
import {Row,Col,Divider} from 'antd';
import InputSelect from '../../../../../components/ant/InputSelect';
/* eslint-disable */

const App = props => {
	let { handleChangeInput,schema,publisher,kategori } = props;

	return(<>
	<br/><br/>
  <Divider orientation="left"><strong>Detail Publishers</strong></Divider>
  <Row gutter={[16,16]}>
    <Col span={12}>
      <InputSelect mode="multiple" name="budget_publishers" data={publisher} placeholder="Pilih Publisher" onChange={handleChangeInput} schema={schema} />
    </Col>
    <Col span={12}>
      <InputSelect mode="multiple" name="books_category" data={kategori} placeholder="Pilih Kategori" onChange={handleChangeInput} schema={schema} />
    </Col>
  </Row>
	</>);
}

export default App;