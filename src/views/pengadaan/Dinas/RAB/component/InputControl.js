import React from 'react';

import ImageCheck from './ImageCheck';
import InputText from '../../../../../components/ant/InputText';
/* eslint-disable */

export default (props) => {
  const { type } = props;
  const prop = { ...props };

  delete prop.type;

  switch (type) {
    case 'checkbox-image':
      return <ImageCheck {...prop} />
    case 'radio-image':
      return <ImageCheck type="radio" {...prop} />
    default:
      return <InputText {...prop} />
  }
};