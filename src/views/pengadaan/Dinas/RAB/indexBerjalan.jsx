import React, { useState } from 'react';
import { Col, Row,Form } from 'antd';
import { Breadcrumb, Content } from '../../../../components';
import {newDropDown} from '../../../../providers/dropdownapi';
import InputSelect from '../../../../components/ant/InputSelect';
import InputSearch from '../../../../components/ant/InputSearch';
import ListForm from './FormList';
import config from './index.config';
/* eslint-disable */

export default (props) => {
  const [form] = Form.useForm();
  const [fiscalYear, setfiscalYear] = useState([]);
  const { list } = config;
  const [model, setModel] = useState(config.model);

  const handleChange = (name, value) => {
    setModel({ ...model, [name]: value });
  }  

  React.useEffect(() => {
    async function fetchData(){
      try{
        const data = await newDropDown(`fiscal-year-dropdown`);
        data.unshift({text:'Pilih Tahun Anggaran',value:''});
        setfiscalYear(data);
      } catch (error) {
        console.log(error?.message);
        return false;
      }
    }
    fetchData();
    setModel({ ...model, type: 'execution' });
  },[]);

  return (
    <React.Fragment>
      <Breadcrumb items={list.breadcrumb} />
      <Content {...{...props,...list.berjalan}}>
        <Form form={form}>
          <Row gutter={16}>
            <Col span={8}>
              <InputSelect name="fiscalyear" defaultVal={model.fiscalyear || ''} text="Tahun Anggaran" data={fiscalYear} onChange={handleChange} />
            </Col>
            <Col span={8} style={{marginTop:'0.6em'}}>
              <InputSearch name="search" text="Pencarian" placeholder="Cari Berdasarkan Nama" onSearch={handleChange} />
            </Col>
          </Row>
        </Form>
        <hr />
        <ListForm {...props} model={model} />
      </Content>
    </React.Fragment>
  );
};