import React from 'react';
import pathName from 'routes/pathName';
import {Row,Col,Typography} from 'antd';
import oProvider from 'providers/oprovider';
import FormComp from './component/FormComp';
import TableComp from './component/TableAdd';
import {Content,Breadcrumb} from 'components';
import Notification from 'common/notification';
import useCustomReducer from 'common/useCustomReducer';
import TableRekapPustaka from './component/TableRekapPustaka';
import TableRekapPenerbit from './component/TableRekapPenerbit';
/* eslint-disable */

const { initial, rab } = pathName;
export const FormAddContext = React.createContext({});
const initialData = {
  dataSubmit : {},
  data : {},
  context : {}
};

const FormAdd = props => {
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  const content = {
    title : 'Upload Daftar Pengadaan',
    subtitle : 'Transaksi Pengadaan Buku'
  };

  const breadcrumb = [
    { text: 'Beranda', to: initial },
    { text: 'Daftar Pengadaan', to: rab.catalog.list },
    { text: 'Upload Daftar Pengadaan' },
  ];

  const handleSimpan = async (e) => {
    try {
      const resp = await oProvider.insert('purchasing-upload-save', dataReducer?.context?.data);
      if(resp){
        Notification({type: 'success', response : {
            code: resp?.message,
            message : 'Success menyimpan data' 
          } 
        });
        props.history.push(rab.catalog.list);
      }
    } catch (error) {
      Notification({type: 'error', 
        response : {
          code: 'Error Message',
          message : "Data sudah ada atau error lain. Silahkan hubungi administrator"
        } 
      });
    }
  };

  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const [
          {data : organization},
        ] = await Promise.all([
          oProvider.list('institusi-dropdown'),
        ]);
        organization.unshift({ value: '', text: '-- Semua Institusi --' });
        reducerFunc('data',{ organization });
      } catch (error) {
        console.log(error.message);
      }
    };
    fetchData();
  },[]);

  const propsForm = {props,handleSimpan};

  return(<>
  <Breadcrumb items={breadcrumb} />
  <Content {...content}>
    <FormAddContext.Provider value={{dataReducer,reducerFunc}}>
      <FormComp {...propsForm} />
      <TableComp {...props} />
      <Row gutter={24}>
        <Col span={12}>
          <Typography.Title level={4}>Rekap Per Penerbit</Typography.Title>
          <TableRekapPenerbit/>
        </Col>
        <Col span={12}>
          <Typography.Title level={4}>Rekap Per Pustaka</Typography.Title>
          <TableRekapPustaka />
        </Col>
      </Row>
    </FormAddContext.Provider>
  </Content>
  </>);
};

export default FormAdd;