import React from 'react';
import { Button, 
  Dropdown, Menu } from 'antd';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import { getOneMenu } from 'utils/getMenuAkses';
import { MessageContent } from 'common/message';
import { PlusOutlined } from '@ant-design/icons';
import ActionTableDropdown from "components/ActionTableDropdown";
import { numberFormat,formDateDisplayValue } from 'utils/initialEndpoint';

/* eslint-disable */
const { initial, rab } = pathName;

const actionMenu = (row, params) => {
  const [, modalDelete,reducerlist,menuAksesContext]= params;

  const handleAktivasi = async () => {
    try {
      MessageContent({ type :'loading', content:'Memuat informasi aktivasi... ',key : 'aktivasi', duration: 0 });
      let { data } = await oProvider.list(`purchasing-catalog-getone?id=${row.id}`);
      reducerlist('stateAktivasi', data);
      MessageContent({ type :'success', content: 'Selesai!', key : 'aktivasi', duration: 1 });
    } catch (error) {
      console.log(error.message);
    }
  }

  return (
    <Menu>
      <Menu.Item>
        <Button type="link" style={{ margin: 0 }} onClick={handleAktivasi}>Aktivasi</Button>
      </Menu.Item>
      { getOneMenu(menuAksesContext,'Hapus') &&
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => modalDelete(row)} text="Hapus" />
      </Menu.Item>
      }
    </Menu>
  );
}

export default {
  scope: {
    title: "Daftar Pengadaan",
    subtitle: "Lakukan pilah untuk memudahkan pencarian",
    toolbars: [
      {
        text: '', type: 'primary', icon: <PlusOutlined/>,
        onClick: (e) => {
          e.history.push(rab.catalog.add);
        }
      },
    ]
  },
  model: {
    search: '',
    start_date: '',
    end_date: '',
    organization_id:'',
    epustaka_id:''
  },
  schema : {
    tanggal: { label: "Tanggal Order" },
    organization_id: { label: "Instansi" },
    epustaka_id: { label: "iPustaka" },
    search: { label: "Pencarian" },
    spk_date: { label: "Tanggal SPK",rules : [
      {required: true, message: 'Tanggal SPK harus dipilih'}
    ] },
    last_activite_date: { label: "Tanggal Akhir Aktivasi",rules : [
      {required: true, message: 'Tanggal Akhir Aktivasi harus dipilih'}
    ] },
    org_id: { label: "Institusi",rules : [
      {required: true, message: 'Institusi harus dipilih'}
    ] },
    spk_number: { label: "Nomor SPK" ,rules : [
      {required: true, message: 'Nomor SPK harus diisi'}
    ]},
    discount: { label: "Diskon" , rules : [
      {required: true, message: 'Diskon harus diisi'}
    ]},
  },
  breadcrumb: [
    { text: 'Beranda', to: initial },
    { text: 'Daftar Pengadaan', to: rab.catalog.list },
  ],
  config: (...props) => {
    return {
      columns: [
        {
          title: 'Tanggal Order',
          width: 130,
          key: 'transaction_date',
          dataIndex: 'transaction_date',
          render: (x, row) => x ? formDateDisplayValue(x,'DD-MM-YYYY') : '-',
        },
        {
          title: 'Nomor Transaksi',
          sorter: true,
          key:'transaction_number',
          dataIndex : 'transaction_number',
          render: (x) => x ? x : '-',
        },
        {
          title: 'ePustaka',
          sorter: true,
          key : 'epustaka_name',
          render: (row) => {
            if(row.epustaka_name){
              if(Array.isArray(row.epustaka_name)){
                return row.epustaka_name.map(x => x.epustaka_name).join();
              }
              if(row.epustaka_name.length == 0) return '-';
              return row.epustaka_name;
            }
            return '-';
          },
        },
        {
          title: 'Instansi',
          width: 180,
          key: 'organization_name',
          dataIndex: 'organization_name',
          render : x => x ? x : '-'
        },
        {
          title: 'Nominal (Rp)',
          width: 180,
          align : 'right',
          key: 'transaction_amount',
          dataIndex: 'transaction_amount',
          render : x => x ? numberFormat(x) : 0,
        },
        {
          title: 'Aksi',
          key: 'operation',
          width: 80,
          className: 'text-center',
          render: row => (
            <Dropdown overlay={actionMenu(row, props)}>
              <Button type="link" style={{ margin: 0 }} onClick={e => e.preventDefault()}>
                <i className="icon icon-options-vertical" />
              </Button>
            </Dropdown>
          ),
        },
      ]
    }
  }
}