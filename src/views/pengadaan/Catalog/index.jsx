import React from 'react';
import ListForm from './FormList';
import config from './index.config';
import { connect } from 'react-redux';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import getMenuAkses from "utils/getMenuAkses";
import FilterList from './component/FilterList';
import { Breadcrumb, Content } from 'components';
import useCustomReducer from 'common/useCustomReducer';
/* eslint-disable */

export const MenuAksesContext = React.createContext([]);
export const DataIndexContext = React.createContext({});

const initialData = {
  filter : config.model,
  data : {},
  menuAkses : []
};
const TransPenerbit = props => {
  const { rab : { catalog } } = pathName;
  const {breadcrumb,schema, scope } = config;
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  React.useEffect(() => {
    if(props.menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(catalog.list);
      reducerFunc('menuAkses',menuAccessFromLocalStorage,'conventional');
    }
  },[props.menuAccess]);

  React.useEffect(() => {
    const fetchData = async() => {
      const [
        {data : organization},
        {data : epustaka},
      ] = await Promise.all([
        oProvider.list('procurement-instansi-dropdown'),
        oProvider.list('procurement-epustaka-dropdown'),
      ]);
      organization.unshift({ value: '', text: '-- Semua Instansi --' });
      epustaka.unshift({ value: '', text: '-- Semua ePustaka --' });
      reducerFunc('data',{organization,epustaka});
    }
    fetchData();
  },[]);

  const propFilter = {schema,scope,history:props.history};
  
  return(
  <DataIndexContext.Provider value={{dataReducer,reducerFunc}}>
    <Breadcrumb items={breadcrumb} />
    <FilterList {...propFilter} />
    <Content {...props}>
      <ListForm {...props} />
    </Content>
  </DataIndexContext.Provider>
  );
};
const mapStateToProps = ({menuAccess}) => ({menuAccess});
export default connect(mapStateToProps)(TransPenerbit);