import React from 'react';
import config from './index.config';
import { stringify } from "query-string";
import { DataIndexContext } from './index';
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
import Notification from 'common/notification';
import { Modal, Space, Typography } from 'antd';
import useCustomReducer from 'common/useCustomReducer';
import ContenModalAktivasi from './component/ContenModalAktivasi';
import { InteractionTwoTone,DeleteTwoTone } from '@ant-design/icons';
/* eslint-disable */

const resource = 'procurement-list';
const { confirm } = Modal;
const initialData = {
  url : '',
  limit: 10,
  state : {
    loading:false,
    dataSource : [],
    pagination : {
      pageSize : 10,
      showSizeChanger: true,
      hideOnSinglePage : true,
    }
  },
  stateAktivasi : {},
};
const List = (props) => {
  const {dataReducer} = React.useContext(DataIndexContext);
  const [datalist,reducerlist] = useCustomReducer(initialData);

  const modalDelete = (props) => {
    confirm({
      title: 'Hapus Pengumuman',
      icon: <DeleteTwoTone />,
      content: <Space direction="vertical">
        <Typography.Text>Anda yakin ingin menghapus data ini (<b>{props.organization_name}</b>) dan semua data terkait?</Typography.Text>
        <Typography.Text type="danger">Tindakan ini tidak bisa dibatalkan</Typography.Text>
      </Space>,
      onOk: async () => {
        const response = await oProvider.delete(`purchasing-upload-delete?id=${props.id}`);
        if (!response) {
          Notification({type : 'error', 
            response : {
              message: 'Data gagal dihapus',
              description: 'Silahkan muat ulang halaman dan coba lagi',
            } 
          });
          return;
        }
        await refresh(datalist?.url);
        Notification({type : 'success', response });
      },
      onCancel() { },
    });
  };

  // Modal Konten Aktivasi
  React.useEffect(() => {
    if(Object.keys(datalist?.stateAktivasi).length){
      modalAktivasi(datalist?.stateAktivasi);
    }
  },[datalist?.stateAktivasi]);

  const modalAktivasi = (props) => {
    confirm({
      title : 'Konten Aktivasi',
      width :'60vw',
      icon  : <InteractionTwoTone />,
      okText : 'Proses Aktivasi',
      cancelText : 'Keluar',
      content: <ContenModalAktivasi {...props} />,
      onOk: () => {
        return new Promise(async (resolve, reject) => {
          const response = await oProvider.update(`catalog-activation?id=${props.id}`);
          if (!response) reject();
          if(response?.code){
            Notification({type: response?.code === 'already_activated' ? 'warning' : 'success', 
              response : {
                message : response.message,
                code    : response.code,
              }
            });
          }
          refresh(datalist?.url);
          resolve();
        }).catch((error) => {
          Notification({type: 'error', response : {
              message : 'Konten gagal diaktivasi',
              code    : 'Silahkan muat ulang halaman dan coba lagi',
            } 
          });
          console.log(error?.message);
          return;
        });
      },
      onCancel() { },
    });
  };
  // END Modal Konten Aktivasi
  const onShowSizeChange = (current, pageSize) => {
    reducerlist('limit',pageSize,'conventional');
  };
  const refresh = async (url) => { 
    reducerlist('state',{ loading:true });
    const paramsObject = {};
    paramsObject['limit'] = datalist?.limit;
    Object.keys(dataReducer?.filter).forEach(x => {
      paramsObject[x] = dataReducer?.filter[x];
    });
    const stringifyUrl = stringify(paramsObject,{skipEmptyString : true});
    try {
      const { meta, data } = await oProvider.list(`${resource}?${stringifyUrl}&${url}`);
      reducerlist('state',{ 
        loading:false, 
        pagination: { 
          ...datalist?.state?.pagination,
          pageSize: datalist?.limit, 
          total: meta.total,
          onShowSizeChange
        }, 
        dataSource: data
      });
    } catch (error) {
      reducerlist('state',{ loading:false });
      console.log(error?.message);
    }
  };

  const handleTableChange = (pagination, filters, sorter) => {
    const paramsObject = {};
    if (pagination) paramsObject['page'] = pagination.current;
    if (sorter && sorter.field) {
      paramsObject['sortBy'] = sorter.field;
      paramsObject['sortDir'] = sorter.order === 'ascend' ? 'asc' : 'desc';
    }
    if (dataReducer?.filter?.search) paramsObject['search'] = dataReducer?.filter?.search;
    reducerlist('url',new URLSearchParams(paramsObject).toString(),'conventional');
  };

  React.useEffect(() => {
    refresh(datalist?.url);
  }, [datalist?.url,datalist?.limit]);

  React.useEffect(() => {
    const loaded = datalist?.state?.loading;
    reducerlist('state',{ loading:true });
    const search1 = dataReducer?.filter?.search;
    const handler = setTimeout(() => {
      const search2 = dataReducer?.filter?.search;
      if (search1 === search2 && loaded) {
        refresh(datalist?.url);
      }
    }, 900);

    return () => { clearTimeout(handler) };
  }, [dataReducer?.filter?.search]);

  React.useEffect(() => {
    refresh(datalist?.url);
  }, [Object.values(dataReducer?.filter).join()]);

  return (
    <MocoTable
      {...config.config(props, modalDelete, reducerlist, dataReducer?.menuAkses)}
      {...datalist?.state}
      onChange={handleTableChange}
    />
  );
}

export default List;