import React from 'react';
import { Form } from 'antd';
import {DataIndexContext} from '../index';
import InputForm from 'components/ant/InputForm';
// import { FileExcelOutlined } from '@ant-design/icons';
// import oProvider from 'providers/oprovider';
/* eslint-disable */

const FilterList = props => {
  const [form] = Form.useForm();
  const {dataReducer,reducerFunc} = React.useContext(DataIndexContext);
  
  const onChange = (name, value) => {
    switch (name) {
      case 'action':
        if (value === 'clear') {
          clearFilter();
        }
        break;
      case 'tanggal':
        console.log(name,'==>',value);
        reducerFunc('filter',{ start_date: value[0], end_date: value[1] });
      default:{
        console.log(name,'==>',value);
        if(!Array.isArray(value)){
          reducerFunc('filter',{ [name]: value });
        }
        break;
      }
    }
  }

  const clearFilter = () => {
    const clearData = { epustaka_id: '', organization_id : '', search: '', start_date: '', end_date: '' };
    form.setFieldsValue({...clearData,tanggal: ['','']});
    reducerFunc('filter',clearData);
  }

  return (<div>
    <InputForm {...props}
      form={form}
      onChange={onChange}
      defCol={{ md: 12, lg: 12, xl: 8 }}
      controls={[
        { type: 'rangepicker', name: 'tanggal', placeholder:['Tanggal Awal','Tanggal Akhir'], style:{width:'100%'} },
        { type: 'select', name: 'organization_id', data: dataReducer?.data?.organization },
        { type: 'select', name: 'epustaka_id', data: dataReducer?.data?.epustaka },
        { type: 'text', name: 'search', defcol: { md: 16 } },
        {
          type: 'action', name: 'action', defcol: { md: 8 }, useLabel: true, actions: [
            { text: 'Pencarian', type: 'primary', action: 'search' },
            { text: 'Reset', type: 'danger', action: 'clear' },
            // { text: 'Excel', type: 'primary', action: 'uploadexcel', icon: <FileExcelOutlined /> }
          ]
        },
      ]}>
      {props.children}
    </InputForm>
  </div>);
};

export default FilterList;