import React from 'react';
import {FormAddContext} from '../FormAdd';
import MocoTable from 'components/MocoTable';
import { numberFormat } from "utils/initialEndpoint";
/* eslint-disable */

const TableRekapPustaka = () => {
  const {dataReducer} = React.useContext(FormAddContext);
  const [state,setState] = React.useState({
    loading:false,
    dataSource : [],
    pagination : {
      hideOnSinglePage : true,
    }
  });

  const columnIndex = () => {
    return {
      columns : [
        {
          title:'ePustaka',
          dataIndex:'epustaka_name',
          render : x => x
        },
        {
          title:'Qty',
          align : 'right',
          dataIndex:'catalog_qty',
          render : x => x ? x : 0
        },
        {
          title:'Copy',
          align : 'right',
          dataIndex:'catalog_copy',
          render : x => x ? x : 0
        },
        {
          title:'Total (Rp)',
          align : 'right',
          dataIndex:'total',
          render : x => x ? numberFormat(x) : 0
        },
      ]
    };
  };

  React.useEffect(() => {
    setState(prev => ({...prev,loading:true}));
    if(Object.keys(dataReducer?.context).length){
      // set state for data epustaka
      setState(prev => ({...prev,
        loading : false, 
        dataSource : dataReducer?.context?.data?.epustaka,
        pagination : {
          ...state.pagination,
          total : dataReducer?.context?.data?.epustaka?.length
        }
      }));
    }else{
      setState(prev => ({...prev,loading:false}));
    }
  },[dataReducer?.context]);

  return(
    <MocoTable 
      {...state}
      columns={columnIndex().columns}
    />
  );
};

export default TableRekapPustaka;