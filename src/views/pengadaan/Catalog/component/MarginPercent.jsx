import React from 'react';
import { Button,Row,Col,
  Space,InputNumber,Form } from 'antd';
// import InputNumberMod from 'components/ant/InputNumber';
/* eslint-disable */

const MarginPercent = props => {
  const {handleCancel,handleSimpan,record,handleChange} = props;
  const rules = {
    margin_publisher : [
      {required: true, message: 'Margin publisher harus di isi'},
      ({getFieldValue}) => ({
        validator(rule, value) {
          let totalMargin = parseInt(value) + parseInt(getFieldValue(`margin_am-${record.publisher_id}`));
          let errorMsg = `Margin publisher : ${value}, Margin AM : ${getFieldValue(`margin_am-${record.publisher_id}`)}, total margin lebih dari 100% !`;
          if (totalMargin <= 100) {
            return Promise.resolve();
          } else {
            return Promise.reject(errorMsg);
          }
        },
      }),
    ],
    margin_am : [
      {required: true, message: 'Margin Aksaramaya harus di isi'},
      ({getFieldValue}) => ({
        validator(rule, value) {
          let totalMargin = parseInt(value) + parseInt(getFieldValue(`margin_publisher-${record.publisher_id}`));
          let errorMsg = `Margin AM : ${value}, Margin publisher : ${getFieldValue(`margin_publisher-${record.publisher_id}`)}, total margin lebih dari 100% !`;
          if (totalMargin <= 100) {
            return Promise.resolve();
          } else {
            return Promise.reject(errorMsg);
          }
        },
      }),
    ]
  };

  return( <Row gutter={16}>
    <Col span={12}>
      <Form.Item name={`margin_publisher-${record.publisher_id}`} label="Margin Publisher (%) :" rules={rules.margin_publisher}>
        <InputNumber 
          min={0} max={100} 
          onBlur={(e) => handleChange(record.publisher_id,'margin_publisher',e.target.value)} 
        />
      </Form.Item>
    </Col>
    <Col span={12}>
      <Form.Item name={`margin_am-${record.publisher_id}`} label="Margin AM (%) :" rules={rules.margin_am}>
        <InputNumber 
          min={0} max={100} 
          onBlur={(e) => handleChange(record.publisher_id,'margin_am',e.target.value)} 
        />
      </Form.Item>
    </Col>
    <Col span={24} style={{display:'flex',justifyContent:'end'}}>
      <Space>
        <Button type="primary" danger onClick={handleCancel}>Cancel</Button>
        <Button type="primary" onClick={handleSimpan}>Simpan</Button>
      </Space>
    </Col>
  </Row>);
};

export default MarginPercent;