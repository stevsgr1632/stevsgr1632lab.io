import React from 'react';
import { Form,Typography } from "antd";
import DiskonPercent from './DiskonPercent';
import MarginPercent from './MarginPercent';
import { numberFormat } from 'utils/initialEndpoint';

export default props => {
  const [form] = Form.useForm();
  const {record, propsDiskonPercent,propsMarginPercent,total} = props;
  return (<React.Fragment key={record?.publisher_id}>
    <Form 
      form={form} 
      initialValues={{ 
        [`publisher_disc1-${record.publisher_id}`] : record.publisher_disc1,
        [`publisher_disc2-${record.publisher_id}`] : record.publisher_disc2,
        [`margin_publisher-${record.publisher_id}`] : record.margin_publisher,
        [`margin_am-${record.publisher_id}`] : record.margin_am,
      }}
    >
      <Typography.Text strong>Total (Rp): {numberFormat(total)}</Typography.Text>
      <DiskonPercent {...propsDiskonPercent} record={record} />
      <MarginPercent {...propsMarginPercent} record={record} />
    </Form>
  </React.Fragment>
  );
}