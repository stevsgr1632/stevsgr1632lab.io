import React from 'react';
import { Form,Row,Col,Input,
  Button,Progress,Upload } from "antd";
import config from '../index.config';
import Message from 'common/message';
import { FormAddContext } from "../FormAdd";
import oProvider from 'providers/uploadfile';
import InputDate from 'components/ant/InputDate';
import InputText from 'components/ant/InputText';
import { UploadOutlined } from '@ant-design/icons';
import InputNumber from 'components/ant/InputNumber';
import InputSelect from 'components/ant/InputSelect';
import useCustomReducer from 'common/useCustomReducer';

/* eslint-disable */
const initialData = {
  previewUpload : {visible:false,title:'',src:''},
  loading : {save : false, upload : false},
  progress : 0,
  colorGreen : '#4dbd74',
};
const FormComp = props => {
  const {handleSimpan} = props;
  const [form] = Form.useForm();
  const { schema } = config;
  const { dataReducer,reducerFunc } = React.useContext(FormAddContext);
  const [ dataForm, reducerForm ] = useCustomReducer(initialData);
  
  const styleButton = {
    color:'#f0f3f5',
    borderColor:dataForm.colorGreen,
    backgroundColor:dataForm.colorGreen,
  };

  const handleChange = (name,value) => {
    if (name === 'org_id'){
      reducerFunc('dataSubmit',{organization_id : value});
    }else{
      reducerFunc('dataSubmit',{[name] : value});
    }
  };

  const loadingSimpan = (e) =>{
    e.preventDefault();
    reducerForm('loading', {save : true});
    form.validateFields()
    .then(values => {
      setTimeout(() => {
        handleSimpan();
        reducerForm('loading', {save : false});
      },1000);
      // console.log(values);
    })
    .catch(errorInfo => {
      console.log(errorInfo);
      reducerForm('loading', {save : false});
      errorInfo.errorFields.forEach(x => {
        Message({type:'error',text:`Kesalahan, ${x.name.toString()} : ${x.errors.toString()}`});
      });
      return false;
    });
  };

  const beforeUpload = file => {
    let isAccept = null;
    let allowedFileType = '.xls,.xlsx';
    if(allowedFileType !== "*"){
      let isAcceptFile = allowedFileType.split(',');
      let typeFile = '.' + file.name.split('.').pop();
      isAccept = isAcceptFile.includes(typeFile);
      if (!isAccept) {
        Message({type:'error',text:`You can only upload ${isAcceptFile.map(x => '.' + x.substring(1).toUpperCase()).join(', ')} file`});
      }
    };
    const isLt2M = file.size / 1024 / 1024 < 11;
    if (!isLt2M) {
      Message({type:'error',text:`Image must smaller than 10MB`});
    };
    
    form.validateFields()
    .then(values => {
      // console.log(values);
    }).catch(async errorInfo => {
      // console.log(errorInfo);
      if(!errorInfo.errorFields.length){
        // console.log(dataReducer?.dataSubmit);
        reducerForm('loading', {upload : true});
        let payloadReq = {...dataReducer?.dataSubmit,filename:file};
        let responseData = await oProvider.insertnew('purchasing-upload',payloadReq);
        reducerFunc('context',responseData);
        reducerForm('previewUpload',{src:file.name});
        reducerForm('loading', {upload : false});
      }else{
        errorInfo.errorFields.forEach(x => {
          Message({type:'error',text:`Kesalahan, ${x.name.toString()} : ${x.errors.toString()}`});
        });
      }
      return false;
    });
    return false;
  };

  return(
    <Form form={form} initialValues={{discount: 0}}>
      <Row gutter={16}>
        <Col span={4}>
          <InputDate name="spk_date" schema={schema} onChange={handleChange} style={{width:'100%'}} />
        </Col>
        <Col span={8}>
          <InputText name="spk_number" schema={schema} onBlur={handleChange} />
        </Col>
        <Col span={8}>
          <InputSelect name="org_id" schema={schema} onChange={handleChange} data={dataReducer?.data?.organization} />
        </Col>
        <Col span={4}>
          <InputDate name="last_activite_date" schema={schema} onChange={handleChange} style={{width:'100%'}} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={8}>
          <InputNumber name="discount" schema={schema} max={100} min={0} onBlur={handleChange} />
        </Col>
        <Col span={12}>
          <Form.Item 
            style={{ marginBottom: 5 }} 
            name="filename" label="Upload" 
            rules={[]}
            labelCol={{ span: 24 }}
          >
            <div style={{display:'flex'}}>
              <Input type="text" readOnly value={dataForm?.previewUpload?.src && dataForm?.previewUpload?.src} />
              <Upload
                accept=".xls,.xlsx"
                className="avatar-uploader"
                showUploadList={false}
                schema={schema}
                beforeUpload={beforeUpload}
                // onChange={handleUploadImage}
                onProgress={({ percent }) => {
                  if (percent === 100) {
                    setTimeout(() => reducerForm('loading',0,'coventional'), 1000);
                  }
                  return reducerForm('progress',(~~(percent)),'coventional');
                }}
              >
                <Button style={styleButton} loading={dataForm?.loading?.upload}>
                  <UploadOutlined /> Proses
                </Button>
              </Upload>
            </div>
          </Form.Item>
          <small>Berkas File : xls atau xlsx, Maks. upload 10MB</small> 
          {dataForm?.progress > 0 ? <Progress percent={dataForm?.progress} /> : null}
        </Col>
        <Col span={4} style={{transform:'translate(1em,2.82em)'}}>
          <Button type="primary" onClick={loadingSimpan} loading={dataForm?.loading?.save}>Simpan</Button>
        </Col>
      </Row>
    </Form>
  );
};

export default FormComp;