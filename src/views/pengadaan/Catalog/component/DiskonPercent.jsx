import React from "react";
import {Row,Col,Form,Typography,InputNumber,Space} from 'antd';
/* eslint-disable */

const DiskonPercent = props => {
  const { datapenerbit,penerbitFunc, numberFormmater, record, handleChange } = props;
  const { publisher_disc1, publisher_disc2 } = record;
  
  const handleBlurNum = (record,name,value) => {
    handleChange(record.publisher_id,name,value);
    penerbitFunc('detailDiskon',{ [`${name}-${record.publisher_id}`] : parseInt(value) });
  };

  return(<Row>
    <Col span={24}>
      <Form.Item name={`publisher_disc1-${record.publisher_id}`} label="Discount 1 (%) :" rules={[]}>
        <Space direction="horizontal">
          <InputNumber 
            min={0} 
            max={100} 
            defaultValue={publisher_disc1} 
            onBlur={(e) => handleBlurNum(record,'publisher_disc1',e.target.value)} 
          />
          <Typography.Text>Rp {datapenerbit?.detailDiskon[`${publisher_disc1}-${record.publisher_id}`] ? numberFormmater(datapenerbit?.detailDiskon[`${publisher_disc1}-${record.publisher_id}`],record.total,'percent') : numberFormmater(publisher_disc1,record.total,'percent')}</Typography.Text>
        </Space>
      </Form.Item>
    </Col>
    <Col span={24}>
      <Form.Item name={`publisher_disc2-${record.publisher_id}`} label="Discount 2 (%) :" rules={[]}>
        <Space direction="horizontal">
          <InputNumber 
            min={0} 
            max={100} 
            defaultValue={publisher_disc2} 
            onBlur={(e) => handleBlurNum(record,'publisher_disc2',e.target.value)} 
          />
          <Typography.Text>Rp {datapenerbit?.detailDiskon[`${publisher_disc2}-${record.publisher_id}`] ? numberFormmater(datapenerbit?.detailDiskon[`${publisher_disc2}-${record.publisher_id}`],record.total,'percent') : numberFormmater(publisher_disc2,record.total,'percent')}</Typography.Text>
        </Space>
      </Form.Item>
    </Col>
  </Row>);
};

export default DiskonPercent;