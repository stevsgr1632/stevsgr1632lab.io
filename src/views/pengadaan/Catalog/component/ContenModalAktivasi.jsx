import React from 'react';
import { Typography, Row, Col } from 'antd';
import MocoTable from 'components/MocoTable';
import { numberFormat,formDateDisplayValue } from 'utils/initialEndpoint';
/* eslint-disable */

const { Text } = Typography;

const ContenModalAktivasi = props => {
  const columnTabelRekapPenerbit = [
    {
      title: 'Penerbit',
      key: 'organization_name',
      dataIndex: 'organization_name',
      render: x => x,
    },  
    {
      title: 'Judul',
      key: 'catalog_qty',
      dataIndex: 'catalog_qty',
      render: (x) => x ? numberFormat(x) : 0,
    },  
    {
      title: 'Copy',
      key: 'catalog_copy',
      dataIndex: 'catalog_copy',
      render: (x) => x ? numberFormat(x) : 0,
    },  
  ];

  return(<>
    <hr/>
    <Row gutter={24}>
      <Col span={12}>
        <Row gutter={[0,8]}>
          <Col span={8}>
            <Text>Tanggal</Text>
          </Col>
          <Col span={16}>
            <Text>: {formDateDisplayValue(props.transaction_date,'DD MMMM YYYY')}</Text>
          </Col>
          <Col span={8}>
            <Text>No. Transaksi</Text>
          </Col>
          <Col span={16}>
            <Text>: {props.transaction_number ?? '-'}</Text>
          </Col>
          <Col span={8}>
            <Text>Jumlah Katalog</Text>
          </Col>
          <Col span={16}>
            <Text>: {numberFormat(props.total_qty)} Buku</Text>
          </Col>
          <Col span={8}>
            <Text>Copy</Text>
          </Col>
          <Col span={16}>
            <Text>: {numberFormat(props.total_copy)} Buku</Text>
          </Col>
        </Row>
      </Col>

      <Col span={12}>
        <MocoTable 
          columns={columnTabelRekapPenerbit} 
          dataSource={props.publisher} 
          pagination={{
            pageSize: 5, 
            total: props.publisher?.length,
            hideOnSinglePage : true,
          }}
        />
      </Col>           
    </Row>
  </>);
};

export default ContenModalAktivasi;