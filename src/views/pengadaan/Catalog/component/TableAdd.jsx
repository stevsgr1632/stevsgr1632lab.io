import React from 'react';
import { FormAddContext } from '../FormAdd';
import MocoTable from 'components/MocoTable';
import { Typography,Space,Table } from "antd";
import { numberFormat } from "utils/initialEndpoint";
/* eslint-disable */

const TableAdd = () => {
  const { dataReducer } = React.useContext(FormAddContext);
  const [state,setState] = React.useState({
    loading:false,
    dataSource : [],
    pagination : {
      hideOnSinglePage : true
    }
  });

  const columnIndex = () => {
    return {
      columns : [
        {
          title:'Nama Katalog',
          render : row => {
            return(<Space direction="vertical">
              <Typography.Text strong>{row?.catalog_isbn}</Typography.Text>
              <Typography.Text>{row?.catalog_title}</Typography.Text>
              <Typography.Text>{row?.publisher_name}</Typography.Text>
            </Space>);
          }
        },
        {
          title:'ePustaka',
          dataIndex:'epustaka_name',
          render : x => x
        },
        {
          title:'Harga (Rp)',
          dataIndex:'catalog_price',
          align : 'right',
          render : x => x ? numberFormat(parseInt(x)) : 0
        },
        {
          title:'Copy',
          dataIndex:'catalog_qty',
          align : 'right',
          render : x => x ? x : 0
        },
        {
          title:'Total (Rp)',
          dataIndex:'total',
          align : 'right',
          render : x => x ? numberFormat(parseInt(x)) : 0
        },
      ]
    };
  };

  React.useEffect(() => {
    // console.log(formDataContext);
    setState(prev => ({...prev,loading:true}));
    if(Object.keys(dataReducer?.context).length){
      setState(prev => ({...prev,
        loading : false, 
        dataSource : dataReducer?.context?.data?.catalogs,
        pagination : { ...state.pagination,
          total : dataReducer?.context?.meta?.totalData,
        }
      }));
    }else{
      setState(prev => ({...prev,loading:false}));
    }
  },[dataReducer?.context]);

  return(
    <MocoTable 
      {...state}
      columns={columnIndex().columns}
      summary={pageData => {
        let totalHarga = state?.dataSource?.reduce((total,num) => total+parseInt(num.total ? num.total : 0),0);
        return(
        <Table.Summary.Row style={{textAlign:'right'}}>
          <Table.Summary.Cell colSpan={4}>
            <Typography.Text strong>Total (Rp)</Typography.Text>
          </Table.Summary.Cell>
          <Table.Summary.Cell>
            <Typography.Text strong>{numberFormat(totalHarga)}</Typography.Text>
          </Table.Summary.Cell>
        </Table.Summary.Row>)
      }}
    />
  );
};

export default TableAdd;