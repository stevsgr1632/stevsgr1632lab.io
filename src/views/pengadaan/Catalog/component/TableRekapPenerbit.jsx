import React from 'react';
import { Table } from 'antd';
import Message from 'common/message';
import {FormAddContext} from '../FormAdd';
import randomString from 'utils/genString';
import { numberFormat } from 'utils/initialEndpoint';
import useCustomReducer from "common/useCustomReducer";
import FormDiskonMarginWrap from "./FormDiskonMarginWrap";
/* eslint-disable */

const initialData = {
  detailMargin : [],
  detailDiskon : {},
  state : {
    loading:false,
    dataSource : [],
    pagination : {
      hideOnSinglePage : true,
    }
  }
};
const TableRekapPenerbit = () => {
  const [ datapenerbit,penerbitFunc ] = useCustomReducer(initialData);
  const { dataReducer,reducerFunc } = React.useContext(FormAddContext);

  const columns = [
    {
      title:'Penerbit',
      key : 'publisher_name',
      dataIndex:'publisher_name',
      render : x => x
    },
    {
      title:'Qty',
      align : 'right',
      key : 'catalog_qty',
      dataIndex:'catalog_qty',
      render : x => x ? x : 0
    },
    {
      title:'Copy',
      align : 'right',
      key : 'catalog_copy',
      dataIndex:'catalog_copy',
      render : x => x ? x : 0
    },
    {
      title:'Total (Rp)',
      align : 'right',
      key : 'catalog_total',
      dataIndex:'total',
      render : x => x ? numberFormat(x) : 0
    }
  ];

  const handleChange = (id,name,value) => {
    let dataChange = datapenerbit?.detailMargin.map(x => {
      if(x.publisher_id === id){
        x[name] = parseInt(value);
      }
      return x;
    });
    penerbitFunc('detailMargin',dataChange,'conventional');
  };
  const numberFormmater = (number,total = 0,type = 'default') => {
    switch (type) {
      case 'percent':
        return numberFormat(total * number / 100);
      default:
        return numberFormat(number);
    }
  };

  const handleCancel = e => {
    e.preventDefault();
    penerbitFunc('detailMargin',dataReducer?.context?.data?.publishers,'conventional');
  };
  const handleSimpan = e => {
    e.preventDefault();
    penerbitFunc('state',{ loading:true });
    const newDataContext = {...dataReducer?.context?.data,publishers : datapenerbit?.detailMargin };
    reducerFunc('context',{ data : newDataContext });
    Message({type:'success',text:'Sukses simpan data presentase'});
  };
  
  React.useEffect(() => {
    if(Object.keys(dataReducer?.context).length){
      // set state for data publisher
      penerbitFunc('state',{ 
        loading : false, 
        dataSource : dataReducer?.context?.data?.publishers,
        pagination : {
          ...datapenerbit?.state?.pagination,
          total : dataReducer?.context?.data?.publishers?.length
        }
      });
      penerbitFunc('detailMargin',dataReducer?.context?.data?.publishers,'conventional');
    }
  },[dataReducer?.context]);
  const propsDiskonPercent = { datapenerbit,penerbitFunc, numberFormmater,handleChange };
  const propsMarginPercent = { handleCancel,handleSimpan,handleChange };
  
  return(
  <Table 
    rowKey={row => row.publisher_id}
    {...datapenerbit?.state}
    columns={columns}
    expandable={{
      expandedRowRender: record => {
        let total = parseInt(record.total);
      return (<React.Fragment key={randomString(10)}>
        <FormDiskonMarginWrap 
          record={record} 
          total={total}
          propsDiskonPercent={propsDiskonPercent} 
          propsMarginPercent={propsMarginPercent} 
        />
      </React.Fragment>)},
    }}
  />);
};

export default TableRekapPenerbit;