import React from 'react';
import i18next from "i18next";
import { Button } from 'reactstrap';
import oAuth from '../providers/Auth';
import { connect } from 'react-redux';
import Content from '../components/coreui/Content';

const App = (props) => {
  const doRefreshToken = async () => {
    await oAuth.refreshToken();
  };

  return (
    <div className="mt-4">
      <Content>
        <pre>
          {JSON.stringify(props.auth, null, 4)}
        </pre>
        <br />
        <pre>
          {i18next.t('page.dashboard')}
        </pre>
        <Button color="success" onClick={doRefreshToken}>Refresh Token</Button>
      </Content>
    </div>
  );
}

const mapState = (state) => ({ auth: state.auth });

export default connect(mapState)(App);