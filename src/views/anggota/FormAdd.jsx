import React from 'react';
import FormControl from './Form';
import config from './index.config';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import { Breadcrumb, Content } from 'components';
/* eslint-disable */

const { anggota } = pathName;
const resource = 'librarian';

const App = (props) => {
  const { schema } = config;
  const { breadcrumb, content } = config.add;

  const backToList = () => {
    props.history.push(anggota.list);
  };
  
  const openNotification = resp => {
    Notification({ response : resp });
  };

  const handleSubmit = async (e) => {
    if (!e.librarian_type) {
      e = { ...e, librarian_type: null };
    }
    console.log(e);
    // inserting to API
    try {
      const response = await oProvider.insert(`${resource}`, { ...e });
      openNotification(response)
      backToList();
    } catch (error) {
      console.log(error?.message);
      return false;
    }
  }

  return (<>
    <Breadcrumb items={breadcrumb} />
    <Content {...content}>
      <FormControl
        onSubmit={handleSubmit}
        onCancel={backToList}
        schema={schema}
        type="add"
      />
    </Content>
  </>);
}

export default App;