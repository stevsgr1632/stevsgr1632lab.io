import React from 'react';
import pathName from 'routes/pathName';
import randomString from 'utils/genString';
import {PlusOutlined} from '@ant-design/icons';
import { Tag,Dropdown,Button,Menu } from 'antd';
import { getOneMenu } from 'utils/getMenuAkses';
import ActionTableDropdown from "components/ActionTableDropdown";
/* eslint-disable */

const { initial, anggota } = pathName;

const actionMenu = (row, params) => {
  const [props,modalDelete,menuAksesContext] = params;
  const sunting = () => {
    props.history.push(anggota.edit(`${row.id}`));
  };

  return (
    <Menu>
      <Menu.Item>
        { getOneMenu(menuAksesContext,'Sunting') && <>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={sunting} text="Sunting" />
        </>}
      </Menu.Item>
      <Menu.Item>
        { getOneMenu(menuAksesContext,'Hapus') && <>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => modalDelete(row, props.history)} text="Hapus" />
        </>}
      </Menu.Item>
    </Menu>
  );
};

export default {
  list: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Anggota', to: anggota.list },
    ],
    content: {
      title: 'Pustakawan - PM',
      toolbars: [
        {
          text: '', type: 'primary', icon: <PlusOutlined/>,
          onClick: (e) => {
            e.history.push(anggota.add);
          }
        },
      ]
    },
    config: (...props) => {
      return {
        columns: [
          {
            title: 'Nama',
            dataIndex: 'librarian_name',
            key: 'librarian_name',
            sorter: true
          },
          {
            title: 'Posisi',
            width: 200,
            key: 'librarian_type_name',
            dataIndex: 'librarian_type_name'
          },
          {
            title: 'Status',
            width: 130,
            key: 'librarian_isactive',
            dataIndex: 'librarian_isactive',
            render: status => {
              let color = status === 1 ? 'green' : 'volcano';
              let nameStatus = status === 1 ? 'Aktif' : 'Tidak Aktif';
              return (
                <Tag color={color} key={randomString(5)}>
                  {nameStatus.toUpperCase()}
                </Tag>
              );
            },
          },
          {
            title: 'Aksi',
            key: 'operation',
            fixed: 'right',
            width: 100,
            render: (row) => {
              return (
              <span>
                <Dropdown overlay={actionMenu(row, props)}>
                  <Button
                    type="link"
                    style={{ margin: 0 }}
                    onClick={e => e.preventDefault()}
                  >
                    <i className="icon icon-options-vertical" />
                  </Button>
                </Dropdown>
              </span>
              )
            },
          },
        ]
      }
    }
  },
  add: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Pustakawan-PM', to: anggota.list },
      { text: 'Tambah Pustakawan-PM', to: anggota.add },
    ],
    content: {
      title: 'Tambah Pustakawan-PM Baru',
    },
  },
  edit: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Pustakawan-PM', to: anggota.list },
      { text: 'Pembaruan Pustakawan-PM' },
    ],
    content: {
      title: 'Pembaruan Pustakawan-PM',
    },
  },
  schema: {
    librarian_name: {
      label: 'Nama',
      rules: [
        { required: true, message: 'Silahkan input nama Pustakawan-PM' },
      ]
    },
    librarian_email: {
      label: 'Email',
      rules: [
        { required: true, message: 'Silahkan input email.' },
      ]
    },
    librarian_phone: {
      label: 'Nomor Telepon',
      rules: [
        { required: true, message: 'Silahkan input No. HP / Telp.' },
      ]
    },
    librarian_type: {
      label: 'Kategori',
      rules: [
        { required: true, message: 'Silahkan pilih tipe jabatan.' },
      ]
    },
    librarian_isactive: {
      label: 'Status',
      rules: [
        { required: true, message: 'Silahkan pilih status.' },
      ]
    },
  },
  model: {
    status: '',
    turunan: '',
    tipekategori: '',
    search: ''
  },
  data: {
    status: [
      { value: '', text: 'Semua Status', disabled: true },
      { value: 1, text: 'Aktif' },
      { value: 0, text: 'Tidak Aktif' },
    ]
  }
};