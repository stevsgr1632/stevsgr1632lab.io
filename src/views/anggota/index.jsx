import React, { useState } from 'react';
import { Col, Row,Form } from 'antd';
import ListForm from './FormList';
import config from './index.config';
import storeRedux from 'redux/store';
import pathName from 'routes/pathName';
import getMenuAkses from "utils/getMenuAkses";
import { Breadcrumb, Content } from 'components';
import InputSelect from 'components/ant/InputSelect';
import InputSearch from 'components/ant/InputSearch';
/* eslint-disable */

export const MenuAksesContext = React.createContext([]);

export default (props) => {
  const [form] = Form.useForm();
  const { anggota : anggotaPath } = pathName;
  const { list, data } = config;
  const [model, setModel] = useState(config.model);
  const [menuAkses, setmenuAkses] = React.useState([]);

  const handleChange = (name, value) => {
    setModel({ ...model, [name]: value });
  };
  const handleChangeSearch = (name, value) => {
    setModel({ ...model, [name]: value });
  };

  React.useEffect(() => {
    if(storeRedux.getState().menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(anggotaPath.list);
      setmenuAkses(menuAccessFromLocalStorage);
    }    
  },[storeRedux.getState().menuAccess]);

  return (
    <React.Fragment>
    <MenuAksesContext.Provider value={menuAkses}>
      <Breadcrumb items={list.breadcrumb} />
      <Content {...{...props,...list.content, menuAkses}}>
        <Form form={form}>
          <Row gutter={16}>
            <Col span={12}>
              <InputSelect name="status" text="Status" placeholder="Pilih status" data={data.status} onChange={handleChange} />
            </Col>
            <Col span={12} style={{marginTop:'0.6em'}}>
              <InputSearch name="search" text="Pencarian" placeholder="Pencarian berdasarkan nama" onSearch={handleChangeSearch} />
            </Col>
          </Row>
        </Form>
        <hr />
        <ListForm {...props} model={model} />
      </Content>
    </MenuAksesContext.Provider>
    </React.Fragment>
  );
};