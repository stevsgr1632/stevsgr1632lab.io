import React from 'react';
import config from './index.config';
import oProvider from 'providers/oprovider';
import { Button,Form, Row, Col } from 'antd';
import LoadingSpin from 'components/LoadingSpin';
import InputText from 'components/ant/InputText';
import InputSelect from 'components/ant/InputSelect';
/* eslint-disable */

const resource = 'librarian';

const Edit = (props) => {
  const [form] = Form.useForm();
  const { onSubmit, onCancel, schema, paramsId = '' } = props;
  const defCol = { lg: { span: 12 }, xl: { span: 12 } };
  const [loading,setloading] = React.useState(false);
  const [jabatan,setjabatan] = React.useState([]);
  
  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const jabatan = await oProvider.list(`generic-lookup?lookup_name=TEAM-DINAS`);
        setjabatan(jabatan.data);
        if(props.type === 'edit'){
          setloading(true);
          const { data } = await oProvider.list(`${resource}-get-one?id=${paramsId}`);
          form.setFieldsValue(data);
          setloading(false);
        }
      } catch (error) {
        console.log(error?.message);
        setloading(false);
      }
    }
    fetchData();
  },[]);

  return (
  <LoadingSpin loading={loading}>
    <Form form={form} onFinish={onSubmit}>
      <Row gutter={12}>
        <Col {...defCol}>
          <InputText name="librarian_name" schema={schema} />
        </Col>
        <Col {...defCol}>
          <InputText name="librarian_email" type="email" schema={schema} />
        </Col>
        <Col {...defCol}>
          <InputText name="librarian_phone" pattern="\(?(?:\+62|62|0)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}" title="081212341234" schema={schema} />
        </Col>
        <Col {...defCol}>
          <InputSelect name="librarian_type" data={jabatan} schema={schema} />
        </Col>
        <Col {...defCol}>
          <InputSelect name="librarian_isactive" data={config.data.status} schema={schema} />
        </Col>
      </Row>
      <hr />
      <Button htmlType="submit" className="ml-1" type="primary" style={{float:'right'}}>Simpan</Button>
      <Button onClick={onCancel} style={{float:'right'}}>Batal</Button>
      {/* <Button htmlType="submit">Save</Button>
      <Button className="ml-1" onClick={onCancel}>Cancel</Button> */}
    </Form>
  </LoadingSpin>
  );
}

export default Edit;