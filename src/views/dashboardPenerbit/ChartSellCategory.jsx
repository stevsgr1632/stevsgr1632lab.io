import React from 'react';
import moment from 'moment';
import oProvider from 'providers/oprovider';
import config,{colorChart} from './index.config';
import LoadingSpin from 'components/LoadingSpin';
import { Row , Col, Select, Form, Radio} from 'antd';
import useCustomReducer from 'common/useCustomReducer';
import ComposedChartForm from './components/ComposedChartForm';
/* eslint-disable */
export default () => {
  const [form] = Form.useForm();
  const { model, chartSellCategory, data : { yearData} } = config;
  const initialData = {
    loading : false,
    radio : 1,
    data : [],
    configData : {chartSellCategory},
    legendText : {},
    year : moment().format('YYYY')
  };
  const [ dataReducer, reducerFunc] = useCustomReducer(initialData);

  const fetchData = async (year) =>{
    reducerFunc('loading',true,'conventional');
    try {
      const sales =  await oProvider.list(`dashboard-publisher-sales-category?year=${year}`); 
      if(sales){
        const { legend, data } = sales;
        const legendData = [
          {value: legend.total,type:'rect' ,id: 'total',color:colorChart.cr1},
          {value: legend.percentage,type:'line' ,id: 'percentage',color:colorChart.cr3}
        ];
        reducerFunc('legendText',legend);
        reducerFunc('configData',{legendData});
        reducerFunc('radio',1,'conventional');
        reducerFunc('data',sales.data,'conventional');
      }
      reducerFunc('loading',false,'conventional');
    } catch (error) {
      console.log(error?.message);
      reducerFunc('loading',false,'conventional');
    }
  };

  const setTextLegend = (key) => {
    const legendData = [
      {value: dataReducer?.legendText.total,type:'rect' ,id: key[0],color:colorChart.cr1},
      {value: dataReducer?.legendText.percentage,type:'line' ,id: key[1],color:colorChart.cr3}
    ];
    const barData = [
      {dataKey:key[0], fill:colorChart.cr1,type:'Bar',yAxisId:'left'},
      {dataKey:key[1], fill:colorChart.cr3,type:'Line',yAxisId:'right',unitLabel:'%',customLabel:true}
    ];
    reducerFunc('configData',{bar:barData,legendData});
  }

    const changeFilter = (radioVal) =>{
      if(radioVal===1){
        const keyText = ['total','percentage'];
        setTextLegend(keyText);
      }
      if(radioVal===2){
        const keyText = ['totalCopy','percentageCopy'];
        setTextLegend(keyText);
      }
    }

    const handleChange = (params) =>{
      reducerFunc('year',params,'conventional');
    }

    const handleRadioChange = (e) =>{
      reducerFunc('radio',e.target.value,'conventional');
    }

    React.useEffect(()=>{
      changeFilter(dataReducer?.radio);
    },[dataReducer?.radio]);

    React.useEffect(()=>{
      fetchData(dataReducer?.year);
    },[dataReducer?.year]);

    return (
      <>
        <LoadingSpin loading={dataReducer?.loading}>
          <div className='card-title-desc'>
            <font style={{fontSize:'12pt', fontWeight:'bold'}}>Penjualan per Kategori</font>
            <Form form={form} style={{marginBottom:'-40px'}}>
                <Row gutter={[4,4]}>
                  <Col flex={2}>
                    <Form.Item initialValue={model.year} name="year" label="Tahun">
                      <Select style={{ width: '100%' }} onChange={handleChange}>
                        {
                          yearData().map((m, idx)=>{
                            return <Select.Option key={idx} value={m.value}>{m.text}</Select.Option>
                          })
                        }
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col flex={5} style={{textAlign:'right'}}>
                    <Radio.Group onChange={handleRadioChange} value={dataReducer?.radio}>
                      <Radio value={1}>Nominal</Radio>
                      <Radio value={2}>Kuantiti</Radio>
                    </Radio.Group>
                  </Col>
                </Row>
            </Form>
          </div>
          <div style={{ width: '100%', height: '300px' }}>
            <ComposedChartForm data={dataReducer?.data} config={dataReducer?.configData} legend={true}/>
          </div>
        </LoadingSpin>
      </>
    )
};
