import React, { Fragment } from 'react';
import config from './index.config';
import oProvider from '../../providers/oprovider';
import LoadingSpin from '../../components/LoadingSpin';
import useCustomReducer from '../../common/useCustomReducer';
import ComposedChartForm from './components/ComposedChartForm';
/* eslint-disable */
const initialData = {
  loading : false,
  dataChart : [],
  configData : config.trendContent
};

export default () => {
    const [ dataReducer, reducerFunc ] = useCustomReducer(initialData);

    const fetchData = async () =>{
      reducerFunc('loading',true,'conventional');
        try {
          const enhancementData = await oProvider.list(`dashboard-chart`); 
          if(enhancementData){
              const { data, legend } = enhancementData;
              const legendData = [
                  {value: legend.totalAll,type:'rect' ,id: 'totalAll',color:'#fe9f15'},
                  {value: legend.total,type:'rect' ,id: 'total',color:'#003366'},
                  {value: legend.percentage,type:'line' ,id: 'percentage',color:'#8884d8'}
              ];
              reducerFunc('configData',{legendData});
              reducerFunc('dataChart',data,'conventional');
          }
          reducerFunc('loading',false,'conventional');
        } catch (error) {
          console.log(error?.message);
          reducerFunc('loading',false,'conventional');
        }
    }

    React.useEffect(()=>{
        fetchData();
    },[]);

    return (
      <Fragment>
        <LoadingSpin loading={dataReducer?.loading}>
          <div className='card-title-desc'>
            Trend Peningkatan Konten eBook
          </div>
          <div style={{ width: '100%', height: '300px' }}>
            <ComposedChartForm data={dataReducer?.dataChart} config={dataReducer?.configData}/>
          </div>
        </LoadingSpin>
      </Fragment>
    )
};
