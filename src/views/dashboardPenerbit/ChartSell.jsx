import React, { Fragment } from 'react';
import config from './index.config';
import { colorChart } from "./index.config";
import oProvider from '../../providers/oprovider';
import LoadingSpin from '../../components/LoadingSpin';
import useCustomReducer from '../../common/useCustomReducer';
import ComposedChartForm from './components/ComposedChartForm';
/* eslint-disable */
const initialData = {
  loading : false,
  dataChart : [],
  configData : config.trendSell
};

export default () => {
    const [ dataReducer, reducerFunc ] = useCustomReducer(initialData);

    const fetchData = async () =>{
        reducerFunc('loading',true,'conventional');
        try {
          const salesData =  await oProvider.list(`dashboard-publisher-trend-sales`); 
          if(salesData){
              const { data , legend } = salesData;
              const legendData = [
                  {value: legend.totalAll,type:'rect' ,id: 'totalAll',color:colorChart.cr2},
                  {value: legend.total,type:'rect' ,id: 'total',color:colorChart.cr1},
                  {value: legend.percentage,type:'line' ,id: 'percentage',color:colorChart.cr3}
              ]
              reducerFunc('configData',{legend:legendData});
              reducerFunc('dataChart',data,'conventional');
          }
          reducerFunc('loading',false,'conventional');
        } catch (error) {
          console.log(error?.message);
          reducerFunc('loading',false,'conventional');
        }
        
    }

    React.useEffect(()=>{
        fetchData();
    },[]);

    return (
        <Fragment>
            <LoadingSpin loading={dataReducer?.loading}>
                <div className='card-title-desc'>
                    Trend Penjualan eBook
                </div>
                <div style={{ width: '100%', height: '300px' }}>
                    <ComposedChartForm data={dataReducer?.dataChart} config={dataReducer?.configData}/>
                </div>
            </LoadingSpin>
        </Fragment>
    )
}
