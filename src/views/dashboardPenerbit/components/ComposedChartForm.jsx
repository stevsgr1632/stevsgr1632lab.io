import React from 'react';
import {
    ComposedChart, 
    CartesianGrid, 
    Tooltip,
    XAxis,
    YAxis,
    Bar,
    Legend,
    ResponsiveContainer,
    Line,
    Area,
    LabelList,
    Text
} from 'recharts';
import { numberFormat } from "utils/initialEndpoint";
/* eslint-disable */

const ComposedChartForm = (props) => {
    const { data, config } = props;
    const { XAxisKey , bar, YAxisOptions, XAxisDegree } = config;

    const CustomizedLineLabel = (props) => {
      const {x, y, stroke, value, unit} = props;
      return <text x={x} y={y} dy={14} fill={stroke} fontSize={10} textAnchor="middle">{value + unit}</text>;
    }

    const CustomBarLabel = (params) => {
      const {x, y, width, height, value} = params;
      const radius = 10;
      return (
      <text x={x + width / 2} y={y - radius} textAnchor="middle" dominantBaseline="middle">
        {numberFormat(value)}
      </text>);
    }

    const CustomizedAxisTick = (props) =>{
      const {x, y, payload} = props; 
      return ( <Text x={x} y={y} width={75} fill='#666' textAnchor="middle" verticalAnchor="start">{payload.value}</Text>)
    }

    const CustomTooltip = (params) => {
      const { active, payload, label } = params;
      const { legendData } = config;
      if (active) {
        return (
          <div style={{background:'#fff',border:'1px solid #b2b2b2',padding:'20px',borderRadius:'4px'}}>
            <div className="label">{`${label}`}</div>
            {
              (payload) ? (
                payload.map((x,idx) => {
                  return (
                    <div key={idx} style={{color:`${x.color}`}}>
                      {`${legendData.filter( y => y.id===x.dataKey && y.color===x.color )[0].value} : ${numberFormat(x.value)}`}
                    </div>)
                }) ) : "" 
            }
          </div>
        );
      }
      return null;
    };

  return (
    <ResponsiveContainer width="100%" height='100%' className="responsive">
      <ComposedChart data={data} margin={{top: 20, right: 5, left: 5, bottom: 30}}>
        <CartesianGrid strokeDasharray="5 5" />
        <Tooltip  content={CustomTooltip}/>
        <XAxis dataKey={XAxisKey} tick={(XAxisDegree) ? <CustomizedAxisTick/> : true}/>
        {(YAxisOptions) ? (YAxisOptions.map((value, idx )=> <YAxis key={idx} {...value}/>) ) : ""}
        {(props.legend===false) ? "" : <Legend payload={config.legendData} height={50} wrapperStyle={{color:'black',padding:'10px'}}/>}
        {
          (bar) ? (
            bar.map((x, idx) => {
              if(x.type==="Bar"){
                return (
                  <Bar yAxisId={x.yAxisId} key={idx} dataKey={x.dataKey} fill={x.fill} >
                    {(x.customLabel) ? <LabelList dataKey={x.dataKey} position="top" content={CustomBarLabel}/> : ''}
                  </Bar>);
              }else if(x.type==="Line"){
                return (
                  <Line 
                    yAxisId={x.yAxisId}
                    key={idx} 
                    type="monotone" 
                    dataKey={x.dataKey} 
                    dot={{ fill: '#fe9f15', strokeWidth: 2 }} 
                    stroke={x.fill} 
                    label={(x.customLabel) ? <CustomizedLineLabel unit={(x.unitLabel) ? x.unitLabel : ''}/>:false} 
                  />);
              }else if(x.type==="Area"){
                return (<Area type="monotone" yAxisId={x.yAxisId} dataKey={x.dataKey} fill={x.fill} stroke={x.fill} />);
              }else{
                return (<Bar key={idx} yAxisId={x.yAxisId} dataKey={x.dataKey} fill={x.fill} />);
              }
            })
          ) : " " 
        }
      </ComposedChart>
    </ResponsiveContainer>
  )
}

export default ComposedChartForm;
