import React from 'react';
import config from './index.config';
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
/* eslint-disable */
const ListBuy = (props) => {
    const { list } = config;
    const [state, setState] = React.useState({
        loading : false,
        pagination: {
            defaultCurrent: 1,
            pageSize: 5,
            total:  0 ,
        },
        dataSource: []
    });

    const fetchData = async () =>{
      try {
        setState(prev => ({...prev,loading:true}));
        const response = await oProvider.list(`dashboard-publisher-outstanding-sales`);
        if(response){
          const paginationData = {...state.pagination, total: response.data.length};
          setState(prev => ({...prev, loading:false, dataSource:response.data,pagination:paginationData}));
        }
      } catch (error) {
        console.log(error?.message);
        setState(prev => ({...prev,loading:false}));
      }
    }

    React.useEffect(()=>{
        fetchData();
    },[]);

    return (
        <>
            <div className='card-title-desc'>
                Permintaan Pembelian eBook
            </div>
            <div style={{ width: '100%', height: '300px' }}>
                <MocoTable {...list.config(props)} {...state}/>
            </div>
        </>
    )
}

export default ListBuy;
