import React from 'react';
import { Col,Row } from 'antd';
import oProvider from 'providers/oprovider';
import LoadingSpin from 'components/LoadingSpin';
import { numberFormat } from "utils/initialEndpoint";
import { CardView, CardViewLoading } from './components';
/* eslint-disable */

const PenerbitForm = () => { 
  const [ data, setData] = React.useState([]);
  const [ loading, setLoading ] = React.useState(false);

  const fetchData = async () =>{
    setLoading(true);
    try {
      const response = await oProvider.list(`dashboard-publisher-imprint`);
      if(response){
        setData(response.data);
      }
    } catch (error) {
      console.log('error : ',error);
    }
    setLoading(false);
  }

  React.useEffect(()=>{
    fetchData();
  },[])

  return (
    <LoadingSpin loading={loading}>
      <Row gutter={[8,8]}>
        {(data.length > 0) ? 
          data.map((m,idx)=>{
            return ( 
              <Col key={m.id} xs={24} sm={24} md={12} lg={8} xl={6}>
                <CardView image={m.organization_logo}>
                  <div style={{marginBottom:'8px',fontWeight:'bold'}}>{m.organization_name}</div>
                  <div style={{marginBottom:'5px'}}>{m.book.title} : {numberFormat(m.book.value)}</div>
                  <div style={{marginBottom:'5px'}}>{m.category.text} : {numberFormat(m.category.value)}</div>
                  <div>
                  <div>{m.sales.text} (Rp)</div>
                    <div style={{fontWeight:'bold'}}>{numberFormat(m.sales.value)}</div>
                  </div>
                </CardView>
              </Col>);
          }) : <CardViewLoading count={4}/>
        } 
      </Row>
    </LoadingSpin>
  )
}

export default PenerbitForm;
