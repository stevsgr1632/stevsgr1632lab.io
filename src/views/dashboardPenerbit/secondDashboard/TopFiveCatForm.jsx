import React, {Fragment} from 'react';
import { Row, Col } from 'antd';
import config from './index.config';
import oProvider from 'providers/oprovider';
import LoadingSpin from 'components/LoadingSpin';
import { numberFormat } from "utils/initialEndpoint";
/* eslint-disable */

const TopFiveCatForm = () => {
    const { images } = config;
    const [ fiveCatData, setFiveCatData] = React.useState([]);
    const [ loading, setLoading] = React.useState(false);

    const fetchData = async () =>{
        setLoading(true);
        try {
            const response = await oProvider.list(`dashboard-category-best-seller`);
            if(response){
                const { data } = response;
                setFiveCatData(data);
            }
        } catch (error) {
            console.log(`Error : ${error}`);
        }
        setLoading(false);
    }

    const LoadingView = (params) =>{
        let views = [];
        for (let index = 0; index < params.count; index++) {
            views.push (
                <Fragment key={index}>
                    <Col span={'4-8'}>
                        <div className='card-container'>
                            <Row gutter={[4,4]}>
                                <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                    <div style={{width:'100%',border:'1px solid #cacaca',borderRadius:'5px',overflow:'hidden'}}>
                                        <img src={images.noimage} alt='no images' width='100%' />
                                    </div>
                                </Col>
                                <Col xs={24} sm={24} md={16} lg={16} xl={16}>
                                    <div style={{width:'100%'}}>
                                        <div style={{width:'100%',background:'#E2E2E2',height:'20px',marginBottom:'5px'}} />
                                        <div style={{width:'50%',background:'#E2E2E2',height:'20px',marginBottom:'5px'}} />
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Fragment>
            )
        }
        return views;
    }

    React.useEffect(()=>{
        fetchData();
    },[]);

    return (
        <Fragment>
            <LoadingSpin loading={loading}>
                <Row gutter={[8,16]}>
                    {(fiveCatData.length > 0) ?
                        fiveCatData.map((m, idx)=>{
                            return (
                                <Fragment key={idx}>
                                    <Col span={'4-8'}>
                                        <div className='card-container'>
                                            <Row gutter={[4,4]}>
                                                <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                                                   <div style={{width:'100%',height:'100%',border:'1px solid #cacaca',borderRadius:'5px',overflow:'hidden'}}>
                                                        <img src={(m.category_image)?m.category_image:images.noimage} alt={m.category_name} width='100%' />
                                                   </div>
                                                </Col>
                                                <Col xs={24} sm={24} md={16} lg={16} xl={16}>
                                                    <div style={{width:'100%'}}>
                                                        <div style={{fontSize:'10pt',fontWeight:'bold',marginBottom:'5px'}}>{m.category_name}</div>
                                                        <div style={{fontSize:'10pt',marginBottom:'5px'}}>Total : {numberFormat(m.total)}</div>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Col>
                                </Fragment>
                            )
                        }) : <LoadingView count={5}/>
                    }
                </Row>
            </LoadingSpin>
        </Fragment>
    )
}

export default TopFiveCatForm
