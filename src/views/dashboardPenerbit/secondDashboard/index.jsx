import React, { Fragment } from 'react';
import './style.scss';
import { Row, Col } from 'antd';
import config from './index.config';
import ChartForm from './ChartForm';
import { Breadcrumb } from 'components';
import PenerbitForm from './PenerbitForm';
import PopEbookForm from './PopEbookForm';
import { PartContainer } from './components';
import TopFiveCatForm from './TopFiveCatForm';
import TopFiveIlibForm from './TopFiveIlibForm';
/* eslint-disable */

const SecondDashboard = (props) => {
    const { breadcrumb, 
            penerbit, 
            ebookPopular,
            topFiveCategory,
            topFiveLib, 
            images } = config;

    return (
        <Fragment>
            <Breadcrumb items={breadcrumb}/>
            <div className='secondash-container'>
                <div>
                    <PartContainer header={penerbit}>
                        <PenerbitForm/>
                    </PartContainer>
                    <PartContainer header={ebookPopular}>
                        <PopEbookForm />
                    </PartContainer>
                </div>
                <div>
                    <Row gutter={[16,16]}>
                        <Col xs={24} sm={24} md={24} lg={18} xl={18}>
                            <PartContainer header={topFiveCategory}>
                                <TopFiveCatForm />
                                <ChartForm/>
                            </PartContainer>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={6} xl={6}>
                            <div className='card-container' style={{padding: '9px 6px',marginTop:'-10px'}}>
                                <PartContainer header={topFiveLib}>
                                   <TopFiveIlibForm noimage={images.noimage}/>
                                </PartContainer>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        </Fragment>
    )
}

export default SecondDashboard;
