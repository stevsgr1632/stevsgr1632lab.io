import React from 'react';
import { Row, Col } from 'antd';
import config from './index.config';
import oProvider from 'providers/oprovider';
import LoadingSpin from 'components/LoadingSpin';
import useCustomReducer from 'common/useCustomReducer';
import ComposedChart from '../components/ComposedChartForm';
/* eslint-disable */
const initialData = {
  data : [],
  configData : config.contentDist,
  loading : false
};
const ChartForm = () => {
    const [ dataReducer, reducerFunc ] = useCustomReducer(initialData);

    const fetchData = async () => {
      reducerFunc('loading',true,'conventional');  
      try {
        const response = await oProvider.list('dashboard-publisher-chart-categories');
        if(response){
          const { data , legend } = response;
          const legendData = [
            {value: legend.total,type:'rect' ,id: 'total',color:'#003366'},
            {value: legend.totalAll,type:'rect' ,id: 'totalAll',color:'#fe9f15'},
            {value: legend.percentage,type:'line' ,id: 'percentage',color:'#8884d8'},
          ]
          reducerFunc('configData',{legendData}); 
          reducerFunc('data',data,'conventional'); 
        }
      } catch (error) {
        console.log(`Error : ${error}`);
      }
      reducerFunc('loading',false,'conventional');  
    }

    React.useEffect(()=>{
      fetchData();
    },[]);

    return (
        <>
            <LoadingSpin loading={dataReducer?.loading}>
                <Row gutter={[8,16]}>
                    <Col span={24}>
                        <div className='card-container' style={{height:'450px',padding:'10px'}}>
                            <div style={{width:'100%'}}>
                                <font style={{fontSize:'12pt', fontWeight:'bold'}}>Sebaran Konten per Kategori</font>
                            </div>
                            <div style={{ width: '100%', height: '375px' }}>
                                <ComposedChart data={dataReducer?.data} config={dataReducer?.configData} legend={true}/>
                            </div>
                        </div>
                    </Col>
                </Row>
            </LoadingSpin>
        </>
    )
}

export default ChartForm;
