import React from 'react';
import { Row, Col } from 'antd';
import oProvider from 'providers/oprovider';
import LoadingSpin from 'components/LoadingSpin';
import { numberFormat } from "utils/initialEndpoint";
import { CardView,CardViewLoading } from './components';
/* eslint-disable */

const PopEbookForm = () => {
  const [ebookData, setEbookData] = React.useState([]);
  const [loading, setLoading] = React.useState(false);

  const fetchData = async () =>{
    setLoading(true);
    try {
      const response = await oProvider.list(`dashboard-catalog-best-seller`);
      if(response){
        const { data } = response;
        setEbookData(data);
      }
    } catch (error) {
      console.log(`Error : ${error}`);
    }
    setLoading(false);
  }

  React.useEffect(()=>{
      fetchData();
  },[]);

  return (
    <LoadingSpin loading={loading}>
      <Row gutter={[8,8]}>
        {(ebookData.length > 0) ? 
          ebookData.slice(0,4).map((value,idx)=>{
            return (
            <Col key={idx} xs={24} sm={24} md={8} lg={6} xl={6}>
              <CardView image={value.catalog_cover}>
                <div style={{height:'33.33%',marginBottom:'5px'}}>
                  <p className='vertical-center'>{value.catalog_title}</p>
                </div>
                <div style={{height:'33.33%',marginBottom:'5px'}}>
                  <p className='vertical-center'>Penerbit : {value.organization_name}</p>
                </div>
                <div style={{height:'33.33%',marginBottom:'5px'}}>
                  <div className='vertical-center'>Penjualan : {numberFormat(value.total)} eBook</div>
                </div>
              </CardView>
            </Col>);
          }) : <CardViewLoading count={4}/>
        }
      </Row>
    </LoadingSpin>
  )
}

export default PopEbookForm
