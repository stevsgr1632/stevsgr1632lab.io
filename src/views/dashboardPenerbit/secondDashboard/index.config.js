import pathName from 'routes/pathName';
import { noImagePath as noimage } from "utils/initialEndpoint";
/* eslint-disable */

const { initial, dashboardPublisher } = pathName;

export default {
    breadcrumb : [
        {text: 'Home', to :initial},
        {text: 'Dashboard', to : dashboardPublisher.main},
    ],
    images : {
        noimage,
        catalogQty: '/assets/dashboard/buku.png',
        catalogCopy:'/assets/dashboard/salinan.png',
        publisher:'/assets/dashboard/layanan.png',
        epustaka:'/assets/dashboard/dipinjam.png',
        membership:'/assets/dashboard/pemustaka.png',
        procurement:'/assets/dashboard/access.png',
    },
    penerbit : {
        title: 'Penerbit',
    },
    topFiveCategory : {
        title: 'Top 5 Kategori',
    },
    contentDist:{
        XAxisKey : 'title',
        XAxisDegree : 35,
        YAxisOptions : [{yAxisId:'left'},{yAxisId:'right',orientation:'right', hide:true}],
        bar : [
            {dataKey:'total', fill:'#003366',type:'Bar',yAxisId:'left'},
            {dataKey:'totalAll', fill:'#fe9f15',type:'Bar',yAxisId:'left'},
            {dataKey:'percentage', fill:'#8884d8',type:'Line',yAxisId:'right',unitLabel:'%',customLabel:true}
        ],
        legendData: [
            {value: '-',type:'rect' ,id: 'total',color:'#003366'},
            {value: '-',type:'rect' ,id: 'totalAll',color:'#fe9f15'},
            {value: '-',type:'line' ,id: 'percentage',color:'#8884d8'},
            
        ]
    },
    topFiveLib : {
        title: 'Top 5 iLibrary',
    },
    ebookPopular : {
        title: 'eBook Terpopular',
        toolbars: [
          {
            text: 'Lihat Semua', type: 'primary',
            onClick: (e) => {
              alert('clicked');
            }
          },
        ]
    }

}