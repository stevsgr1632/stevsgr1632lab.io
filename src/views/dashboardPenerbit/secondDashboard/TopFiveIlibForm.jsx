import React, { Fragment } from 'react';
import { Row, Col } from 'antd';
import config from './index.config';
import oProvider from 'providers/oprovider';
import LoadingSpin from 'components/LoadingSpin';
import { numberFormat } from "utils/initialEndpoint";
/* eslint-disable */

const TopFiveIlibForm = () => {
    const { images } = config;
    const [ data , setData ] = React.useState([]);
    const [ loading, setLoading ] = React.useState(false);

    const fecthData = async () => {
        setLoading(true);
        try {
            const response = await oProvider.list('dashboard-publisher-top5-ilibrary');
            if(response){
                const { data } = response;
                setData(data);
            }
            setLoading(false);
        } catch (error) {
            console.log(`error : ${error}`);
            setLoading(false);
        }
    }

    React.useEffect(()=>{
        fecthData();
    },[])

    return (
        <Fragment>
            <LoadingSpin loading={loading}>
            {
                (data.length > 0) ? data.map((m, idx)=>{
                   return (
                        <div key={idx} style={{borderBottom:'1px solid  #CACACA',padding:'7px'}}>
                            <Row gutter={[8,8]}>
                                <Col xs={4} sm={4} md={4} lg={8} xl={8}>
                                    <div style={{width:'70px',borderRadius:'50%',border:'1px solid #CACACA'}}>
                                        <img src={(m.ilibrary_logo)? m.ilibrary_logo : images.noimage} alt='logo' style={{borderRadius:'50%',width:'100%'}}/>
                                    </div>
                                </Col>
                                <Col xs={20} sm={20} md={20} lg={16} xl={16}>
                                    <div style={{fontWeight:'bold',marginBottom:'5px'}}>{m.ilibrary_name}</div>
                                    <div style={{fontWeight:'bold'}}>Total Buku</div>
                                    <div>{numberFormat(m.qty_book)} Judul</div>
                                    <div>{numberFormat(m.copy_book)} Copy </div>
                                </Col>
                            </Row>
                        </div>
                    )
                }): ''
            }
            </LoadingSpin>
        </Fragment>
    )
}

export default TopFiveIlibForm;
