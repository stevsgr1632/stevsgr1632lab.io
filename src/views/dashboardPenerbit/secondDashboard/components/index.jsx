import cardView from './CardView';
import partContainer from './PartContainer';
import LoadingCard from './CardViewLoading';

export const PartContainer = partContainer;
export const CardViewLoading = LoadingCard;
export const CardView = cardView;
