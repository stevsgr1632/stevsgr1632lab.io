import React, { Fragment } from 'react';
import config from '../index.config';
/* eslint-disable */

const CardView = (props) => {
    const { image } = props;
    const { images } = config;

    return (
        <Fragment>
            <div className='card-container' style={{...props.style}}>
                <div className='img-thumb' {...(props.imgStyle) ? {style:props.imgStyle} :''}>
                    <img src={(image) ? image : images.noimage} alt=''/>
                </div>
                <div className='description' {...(props.descStyle) ? {style:props.descStyle} :''}>
                    {props.children}
                </div>
            </div>
        </Fragment>
    )
}

export default CardView;
