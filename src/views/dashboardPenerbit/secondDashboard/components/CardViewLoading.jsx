import React, { Fragment } from 'react';
import { Col } from 'antd';
import CardView from './CardView';
import config from '../index.config';
/* eslint-disable */

const CardViewLoading = (props) => {
    const { images } = config;
    const { count } = props;

    const LoopView = (params)=>{
        let views = [];
        for (let index = 0; index < params.count; index++) {
            views.push (
                <Col key={index} xs={24} sm={24} md={12} lg={8} xl={6}>
                    <CardView image={images.noimage} {...props}>
                        <div style={{marginBottom:'8px', background:'#E2E2E2',width:'100%',height:'20px'}}></div>
                        <div style={{marginBottom:'5px',background:'#E2E2E2',width:'60%',height:'20px'}}></div>
                        <div style={{marginBottom:'5px',background:'#E2E2E2',width:'40%',height:'20px'}}></div>
                    </CardView>
                </Col>
            )
        }
        return views;
    }

    return (
        <Fragment>
            <LoopView count={count}/>
        </Fragment>
    )
}

export default CardViewLoading
