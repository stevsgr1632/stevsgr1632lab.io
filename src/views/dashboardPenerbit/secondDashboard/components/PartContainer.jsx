import React, { Fragment } from 'react';
import { Row , Col } from 'antd';
/* eslint-disable */

const PartContainer = (props) => {
    const { header  } = props;

    const Toolbars = (params) =>{
        const { header : { toolbars= [] }} = params;

        if(params.header.toolbars){
            return (
                <div className="card-toolbars text-right" style={{alignSelf:'center'}}>
                    {
                        toolbars.map((m, idx) => {
                            const { onClick } = m;
                            const handleClick = (e) => {
                                e.preventDefault();
                                if (onClick) {
                                  onClick({ ...params, ...m });
                                }
                            }

                            return (
                                <Fragment key={idx}>
                                    <span>
                                        <a href={{link:'#'}} onClick={(e)=>handleClick(e)} style={{fontWeight:'bold'}}>{m.text}</a>
                                    </span>
                                </Fragment>
                            )

                        })
                    }
                </div>
            );            
        }
        return '';
    }

    return (
        <Fragment>
            <div className='part-container'> 
                <div className='part-header'>
                    <Row gutter={[8,8]}>
                        <Col flex={5}>
                            <h4 style={{marginBottom:'0px'}}>{header.title}</h4>
                        </Col>
                        <Col flex={2}>
                            <Toolbars {...props}/>
                        </Col>
                    </Row>
                </div>
                <div className='part-body'>
                    {props.children}
                </div>
            </div>
        </Fragment>
    )
}

export default PartContainer
