import React from 'react';
import './style.scss';
import { Row, Col } from 'antd';
import config from '../index.config';
import pathName from 'routes/pathName';
import { Link } from 'react-router-dom';
import StackedChart from './StackedChart';
import StatusCatalog from './StatusCatalog';
import oProvider from 'providers/oprovider';
import LoadingSpin from 'components/LoadingSpin';
import { numberFormat } from 'utils/initialEndpoint';
import useCustomReducer from 'common/useCustomReducer';
/* eslint-disable */
const { dashboardPublisher } = pathName;

const initialData = {
  loading : false,
  statData : {},
  profileData : {
    name:'-',
    logo:''
  },
  chartData : []
};

export default () => {
  const { images } = config;
  const colSize = {xs:24, sm:24, md:6, lg:6, xl:6 };
  const [ dataReducer, reducerFunc ] = useCustomReducer(initialData);

  const fetchData = async () =>{
    reducerFunc('loading',true,'conventional');
    try {
      const profile = await oProvider.list(`organization-group-profile`);
      const summary = await oProvider.list(`dashboard-publisher-summary`);
      const summaryChart = await oProvider.list(`dashboard-publisher-catalog-chart`);
      if(summary){
        if(summary.data){
          const convertNumber = (letter) => {
            const numberPattern = /\d+/g;
            const letterPattern = /[a-zA-Z\s]+/g;

            const number = numberFormat(letter.match(numberPattern).join(''));
            const string = letter.match(letterPattern).join('')

            const newLetter = number + " " + string;
            return newLetter;
          }
          
          const { data : { catalog, category, publisher, book_status} } = summary;
          const catalogData = convertNumber(catalog) ;
          const categoryData = convertNumber(category);
          const publisherData = convertNumber(publisher);
          reducerFunc('statData',
          {...summary.data,
            catalog : catalogData, 
            category : categoryData, 
            publisher:publisherData,
            book_status
          });
        }
      }
      if(summaryChart){
        reducerFunc('chartData',summaryChart.data,'conventional');
      }
      if(profile){
        const { data } = profile;
        if( data.length > 0 ) {
          reducerFunc('profileData',{
            name:(data[0].organization_group_name) ? data[0].organization_group_name : '-',
            logo:(data[0].organization_group_logo) ? data[0].organization_group_logo : ''
          });
        }
      }
      
      reducerFunc('loading',false,'conventional'); 
    } catch (error) {
      console.log(error?.message);
      reducerFunc('loading',false,'conventional');
    }
  }

  React.useEffect(()=>{
    fetchData();
  },[]);

  return (
    <React.Fragment>
      <LoadingSpin loading={dataReducer?.loading}>
        <Row gutter={[16,16]}>
          <Col {...colSize}>
            <div className='dash-title'>Dashboard</div>
            <div className='dash-description'>{dataReducer?.profileData.name}</div>
            <div className='container-stat'>
              <div className='container-image'>
                <img alt='icon' src={images.catalogQty}/>
              </div>
              <div className='container-text'>
                <div className='text-value'>
                  <Link to={dashboardPublisher.penerbit}>{(dataReducer?.statData.catalog) ? dataReducer?.statData.catalog : '-'}</Link> 
                </div>
              </div>
            </div>
            <div className='container-stat'>
              <div className='container-image'>
                <img alt='icon' src={images.epustaka}/>
              </div>
              <div className='container-text'>
                <div className='text-value'>{(dataReducer?.statData.category) ? dataReducer?.statData.category : '-'}</div>
              </div>
            </div>
            <div className='container-stat'>
                <div className='container-image'>
                  <img alt='icon' src={images.publisher}/>
                </div>
                <div className='container-text'>
                  <div className='text-value'>{(dataReducer?.statData.publisher) ? dataReducer?.statData.publisher : '-'}</div>
                </div>
            </div>
          </Col>
          <Col xs={24} sm={24} md={12} lg={12} xl={12}>
            <div style={{width:'100%'}}>
              <StackedChart data={dataReducer?.chartData}/>
            </div>
          </Col>
          <Col {...colSize}> 
            <StatusCatalog data={(dataReducer?.statData.book_status) ? dataReducer?.statData.book_status : [] }/>
          </Col>
        </Row>
      </LoadingSpin>
    </React.Fragment>
  )
};
