import React, { Fragment } from 'react';
import { Progress } from 'antd';
/* eslint-disable */

const StatusCatalog = (props) => {
    const { data } = props;
    const colors = ['#2ea4ff','#f70000','#f7ca00','#f7ca00'];

    const percenFormat = (percent, successPercent) => {
        return percent + '%';
    }

    return (
        <Fragment>
            <div style={{marginLeft:'10px'}}>
                <div style={{fontSize:'14pt',fontWeight:'bold'}}>Status Catalog</div>
                {
                    data.map((val, idx)=>{
                        return (
                            <div key={idx}>
                                <div>{val.title}</div>
                                <Progress percent={val.value} format={percenFormat} strokeColor={colors[idx]}/>
                            </div>)
                    })
                }
            </div>
        </Fragment>
    )
}

export default StatusCatalog;
