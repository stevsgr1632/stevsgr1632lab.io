import React, { Fragment, useState,useEffect } from 'react';
import {
    AreaChart, 
    Area, 
    XAxis, 
    YAxis, 
    CartesianGrid, 
    Tooltip,
    ResponsiveContainer } from 'recharts';
/* eslint-disable */

const StackedChart = (props) => {
    const { data } = props;
    const [ dataChart, setDataChart] = useState([]);

    const mapData = () =>{
        const key_1 = 'year';
        const key_2 = 'title';
        const newData = data.reduce((value, idx)=>{
            (value[idx[key_1]] = value[idx[key_1]] || []).push(idx);
            return value;
        },{});

        let finalData = [];

        Object.keys(newData).map((value,idx)=>{
            const objData = newData[`${value}`].reduce((xData, idz)=>{
                (xData[idz[key_2]] = xData[idz[key_2]])
                if(xData){
                    xData[idz[key_2]] = idz.value;
                }
                return xData;
            },{});
            finalData.push({ year : value , ...objData});
        });
        setDataChart(finalData);
    }

    useEffect(()=>{
        mapData();
    },[data])

    return (
        <Fragment>
            <ResponsiveContainer width='100%' height={300}>
                <AreaChart data={dataChart} margin={{top: 10, right: 30, left: 0, bottom: 0}}>
                    <CartesianGrid strokeDasharray="3 3"/>
                    <XAxis dataKey="year"/>
                    <YAxis/>
                    <Tooltip/>
                    <Area type='monotone' dataKey='TERVERIVIKASI' stackId="1" stroke='#8884d8' fill='#8884d8' />
                    <Area type='monotone' dataKey='DITANGGUHKAN' stackId="1" stroke='#82ca9d' fill='#82ca9d' />
                    <Area type='monotone' dataKey='DIBLOKIR' stackId="1" stroke='#ffc658' fill='#ffc658' />
                </AreaChart>
            </ResponsiveContainer>
        </Fragment>
    )
}

export default StackedChart
