import moment from 'moment';
import pathName from 'routes/pathName';
import { noImagePath as noimage,numberFormat } from "utils/initialEndpoint";
/* eslint-disable */
const { initial, dashboard } = pathName;
export const colorChart = {
  cr1 : '#003366',
  cr2 : '#fe9f15',
  cr3 : '#8884d8',
}
export default {
    breadcrumb : [
        { text: 'Home', to: initial },
        { text: 'Dashboard', to: dashboard },
    ],
    images : {
        noimage,
        catalogQty: '/assets/dashboard/buku.png',
        catalogCopy:'/assets/dashboard/salinan.png',
        publisher:'/assets/dashboard/layanan.png',
        epustaka:'/assets/dashboard/dipinjam.png',
        membership:'/assets/dashboard/pemustaka.png',
        procurement:'/assets/dashboard/access.png',
    },
    trendContent:{
        XAxisKey : 'year',
        YAxisOptions : [{yAxisId:'right',orientation:'right', hide:true},{yAxisId:'left'}],
        bar : [
            {dataKey:'totalAll', fill:colorChart.cr2,type:'Bar',yAxisId:'left',customLabel:true},
            {dataKey:'total', fill:colorChart.cr1,type:'Bar',yAxisId:'left',customLabel:true},
            {dataKey:'percentage', fill:colorChart.cr3,type:'Line',yAxisId:'right',unitLabel:'%',customLabel:true},
        ],
        legendData: [
            {value: 'Pertumbuhan per Tahun',type:'rect' ,id: 'totalAll',color:colorChart.cr2},
            {value: 'Total Judul',type:'rect' ,id: 'total',color:colorChart.cr1},
            {value: 'Persentase',type:'line' ,id: 'percentage',color:colorChart.cr3}
        ]
    },
    trendSell:{
        XAxisKey : 'year',
        YAxisOptions : [{yAxisId:'right',orientation:'right', hide:true},{yAxisId:'left'}],
        bar : [
            {dataKey:'totalAll', fill:colorChart.cr2,type:'Bar',yAxisId:'left'},
            {dataKey:'total', fill:colorChart.cr1,type:'Bar',yAxisId:'left'},
            {dataKey:'percentage', fill:colorChart.cr3,type:'Line',yAxisId:'right',unitLabel:'%',customLabel:true},
        ],
        legendData: [
            {value: 'Pertumbuhan per Tahun',type:'rect' ,id: 'totalAll',color:colorChart.cr2},
            {value: 'Total Judul',type:'rect' ,id: 'total',color:colorChart.cr1},
            {value: 'Persentase',type:'line' ,id: 'percentage',color:colorChart.cr3}
        ]
    },
    chartSellCategory: {
        XAxisKey : 'title',
        XAxisDegree : 35,
        YAxisOptions : [{yAxisId:'left'},{yAxisId:'right',orientation:'right', hide:true}],
        bar : [
            {dataKey:'total', fill:colorChart.cr1,type:'Bar',yAxisId:'left'},
            {dataKey:'percentage', fill:colorChart.cr3,type:'Line',yAxisId:'right',unitLabel:'%',customLabel:true}
        ],
        legendData: [
            {value: '-',type:'rect' ,id: 'total',color:colorChart.cr1},
            {value: '-',type:'line' ,id: 'percentage',color:colorChart.cr3}
        ]
    },
    list : {
        config : (...props) => {
            return {
                columns : [
                    {
                        title: 'Tanggal',
                        dataIndex: 'transaction_date',
                        key: 'transaction_date',
                        render: row => {
                            return row !== null ? moment(row).format('DD-MM-YYYY') : '-'; 
                        }
                    },
                    {
                        title: 'Keterangan',
                        dataIndex: 'organization_name',
                        key: 'organization_name',
                        render: row => {
                            return row !== null ? row : '-'; 
                        }
                    },
                    {
                        title: 'Status',
                        dataIndex: 'status_name',
                        key: 'status_name',
                        render: row => {
                            return row !== null ? row : '-'; 
                        }
                    },
                    {
                        title: 'Nominal (Rp)',
                        dataIndex: 'transaction_amount',
                        align:'right',
                        key: 'transaction_amount',
                        width: 120,
                        render: row => {
                            return row !== null ? numberFormat(row) : '-'; 
                        },
                    }
                ]
            }
        }
    },
    model: {
        year: moment().format('YYYY'),
    },
    data: {
        yearData: ()=>{
            const yearNow = moment().format('YYYY');
            const yearMin = yearNow - 30;
            let yearValue = [];
            for(let i = yearNow; i >= yearMin ; i--){
                const val = { value: i, text: i };
                yearValue.push(val);
            }

            return yearValue;
        }
    }
}