import React, { useEffect, useState } from "react";
import { Modal } from "antd";
import oProvider from "../../../providers/oprovider";
import MocoTable from "../../../components/MocoTable";
import config from "./index.config";
/* eslint-disable */

const resource = "librarian?limit=5";
const { confirm } = Modal;

const List = props => {
  const { model } = props;
  const [state, setState] = useState({});
  const [url, setUrl] = useState("");

  const showConfirm = props => {
    confirm({
      title: "Do you want to delete these items?",
      icon: "delete",
      content: "When clicked the OK button, this dialog will be closed",
      onOk() {
        return new Promise(async (resolve, reject) => {
          await oProvider.delete(`librarian?id=${props.id}`);
          refresh("", { status: "", search: "" });
          // await oProvider.list(`${resource}?limit=5`);
          setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
        }).catch(() => console.log("Oops errors!"));
      },
      onCancel() {}
    });
  };

  const refresh = async (url, model) => {
    setState({ ...state, loading : true });
    if (model.status) url += `&librarian_isactive=${model.status}`;
    if (model.search) url += `&librarian_name=${model.search}`;
    try{
      const { data } = await oProvider.list(`${resource}${url}`);
      setState({
        loading : false,
        pagination: { pageSize: 5, total: data.length },
        dataSource: data
      });
    }catch (e) {
      console.log(e?.message);
      setState({ ...state, loading : false });
    }
  };

  const handleTableChange = (pagination, filters, sorter) => {
    let url = "";
    if (pagination) url += `&page=${pagination.current}`;
    if (sorter && sorter.field) {
      url += `&sortBy=${sorter.field}`;
      url += `&sortDir=${sorter.order === "ascend" ? "asc" : "desc"}`;
    }
    setUrl(url);
  };

  useEffect(() => {
    refresh(url, model);
  }, [url, model]);

  return (
    <React.Fragment>
      <MocoTable
        {...config.list.config(props, showConfirm)}
        {...state}
        onChange={handleTableChange}
      />
    </React.Fragment>
  );
};

export default List;
