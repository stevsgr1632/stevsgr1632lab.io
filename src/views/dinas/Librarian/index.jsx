import React, { useState } from "react";
import { Col, Row } from "reactstrap";
import { Breadcrumb, Content } from "../../../components";
import InputSelect from "../../../components/ant/InputSelect";
import InputSearch from "../../../components/ant/InputSearch";
import ListForm from "./FormList";
import config from "./index.config";
/* eslint-disable */

export default props => {
  const { list, data } = config;
  const [model, setModel] = useState(config.model);

  const handleChange = (name, value) => {
    setModel({ ...model, [name]: value });
  };
  const handleChangeSearch = (name, value) => {
    setModel({ ...model, [name]: value });
  };

  return (
    <React.Fragment>
      <Breadcrumb items={list.breadcrumb} />
      <Content {...{ ...props, ...list.content }}>
        <Row>
          <Col lg="6" xl="4">
            <InputSelect
              name="status"
              text="Status"
              data={data.status}
              onChange={handleChange}
            />
          </Col>
          <Col lg="6" xl="4">
            <InputSearch
              name="search"
              text="Pencarian"
              placeholder="Cari Berdasarkan Nama"
              onSearch={handleChangeSearch}
            />
          </Col>
        </Row>
        <hr />
        <ListForm {...props} model={model} />
      </Content>
    </React.Fragment>
  );
};
