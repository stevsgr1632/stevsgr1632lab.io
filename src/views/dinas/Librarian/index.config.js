export default {
    add : {
        breadcrumb: [
            { text : 'Beranda', to : '/' },
            { text : 'Admin ePustaka', to : '/librarian/list' },
            { text : 'Tambah Baru Admin ePustaka', to : '/librarian/add' },
        ],
        content : {
            title : 'Buat Admin ePustaka Baru'
        }
    },
    data: {
        status: [
            { value: '', text: 'Pilih Status', disabled: true },
            { value: 0, text: 'Tidak Aktif' },
            { value: 1, text: 'Aktif' },
        ],
    },
    schema : {
        librarian_name : {
            label : "Nama Admin ePustaka",
            rules : [
                { required : true, message : "Silahkan masukan nama Admin ePustaka"},
            ]
        },
        librarian_email : {
            label : "Email",
            rules : [
                { required : true, message : "Silahkan masukan email Admin ePustaka"},
                // { type : "email", message : "Masukan harus dalam format email"},
            ]
        },
        librarian_phone : {
            label : "No Telp",
            rules : [
                { required : true, message : "Silahakn masukan nomer telepon Admin ePustaka"},
                // { type : "number", message : "Hanya dapat memasukan angka"},
            ]
        },
        librarian_type : {
            label : "Type",
            rules : [
                { required: true, message : "Silahkan masukan tipe Admin ePustaka"},
            ]
        },
        librarian_isactive : {
            label : "Status",
            rules : [
                { required: true, message : "Silahkan masukan status Admin ePustaka"},
            ]
        }
    }
}