import React from 'react';
import FormControl from './Form';
import config from './index.config';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import LoadingSpin from 'components/LoadingSpin';
import { Breadcrumb, Content } from 'components';
/* eslint-disable */

const { organisasi: organisasiUrl } = pathName;
const resource = 'organization';

const App = (props) => {
  const { schema } = config;
  const { breadcrumb, content } = config.add;
  const [, setemail] = React.useState([]);
  const [loading, setloading] = React.useState(false);

  const backToList = () => {
    props.history.push(organisasiUrl.list);
  };

  const handleSubmit = async (e) => {
    console.log(e);
  };

  const handleOtherSubmit = async e => {
    setloading(true);
    try{
      (e.organization_ppn) ? e.organization_ppn = 1 : e.organization_ppn = 0;

      const response = await oProvider.insert(`${resource}`, { ...e });
      if(response){
        Notification({
          response: {
            code : response.code, 
            message : response.message
          }, 
          type:'success', 
          placement:'bottomRight'
        });
        
        backToList();
      }
      setloading(false);
      
    } catch (error) {
      console.log(error?.message);
      Notification({
        response: {
          code : 'Error', 
          message : error?.response?.data?.message
        }, 
        type:'error', 
        placement:'bottomRight'
      });
      setloading(false);
    }
  }

  React.useEffect(() => {
    async function fetchData() {
      let dataOrg = await oProvider.list('organization');
      let allEmail = await dataOrg.data.map(x => {
        let dataku = x.organization_email;
        return dataku;
      }).filter(Boolean);

      setemail(allEmail);
    }
    fetchData();
  }, []);

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <LoadingSpin loading={loading}>
          <FormControl
            onSubmit={handleSubmit}
            otherSubmit={handleOtherSubmit}
            onCancel={backToList}
            schema={schema}
            type="add"
            dataRow={{}}
          />
        </LoadingSpin>
      </Content>
    </React.Fragment>
  );
}

export default App;
