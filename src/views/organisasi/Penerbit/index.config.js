import React from 'react';
import pathName from 'routes/pathName';
import randomString from 'utils/genString';
import { getOneMenu } from 'utils/getMenuAkses';
import { PlusOutlined } from '@ant-design/icons';
import { Dropdown, Button, Menu,Tag } from 'antd';
import { noImagePath,numberFormat } from "utils/initialEndpoint";
import ActionTableDropdown from "components/ActionTableDropdown";
/* eslint-disable */
const { initial, organisasi: organisasiUrl } = pathName;

const actionMenu = (row, params) => {
  const [props,modalDelete,menuAksesContext,handleModalView] = params;
  const sunting = () => {
    props.history.push(organisasiUrl.edit(`${row.id}`));
  };

  return (
    <Menu>
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={()=>handleModalView(row)} text="Pratinjau" />
      </Menu.Item>
      { getOneMenu(menuAksesContext,'Sunting') &&
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={sunting} text="Sunting" />
      </Menu.Item>
      }
      { getOneMenu(menuAksesContext,'Hapus') &&
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => modalDelete(row, props.history)} text="Hapus" />
      </Menu.Item>
      }
    </Menu>
  );
};

export default {
  list: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Penerbit', to: organisasiUrl.list },
    ],
    content: {
      title: 'Penerbit',
      subtitle: 'Lakukan pilah untuk memudahkan pencarian',
      toolbars: [
        {
          text: '', type: 'primary', icon: <PlusOutlined/>,
          onClick: (e) => {
            e.history.push(organisasiUrl.add);
          }
        },
      ]
    },
    config: (...props) => {

      return {
        columns: [
          {
            title: 'Logo Penerbit',
            width: 40,
            align: 'center',
            dataIndex: 'organization_logo',
            render: logo => (
              <div style={{ width: "5em" }}>
                <img src={logo || noImagePath} alt="" style={{ width: "100%" }} />
              </div>
            )
          }, {
            title: 'Nama Penerbit',
            dataIndex: 'organization_name',
            key: 'organization_name',
            sorter: true
          }, {
            title: 'Kode',
            dataIndex: 'organization_code',
            key: 'organization_code',
            width: 80,
            render: row => {
              return row !== null ? row : ' - ';
            }
          }, {
            title: 'Email',
            dataIndex: 'organization_email',
            key: 'organization_email',
            width: 250,
            render: row => {
              return row !== null ? row : ' - ';
            }
          }, {
            title: 'Konten',
            dataIndex: 'content_qty',
            key: 'content_qty',
            align: 'right',
            render: row => {
              return (row) ? numberFormat(row) : 0
            }
          }, {
            title: 'Status',
            width: 130,
            align:'center',
            key: 'organization_isactive',
            dataIndex: 'organization_isactive',
            render: status => {
              let color = status === 1 ? 'green' : 'volcano';
              let nameStatus = status === 1 ? 'Aktif' : 'Tidak Aktif';
              return (
                <Tag color={color} key={randomString(5)}>
                  {nameStatus.toUpperCase()}
                </Tag>
              );
            },
          }, {
            title: 'Institusi',
            dataIndex: 'organization_group_name',
            key: 'organization_group_name',
            width: 350,
            sorter: true,
            render: row => {
              return row !== null ? row : ' - ';
            }
          }, {
            title: 'Aksi',
            key: 'operation',
            fixed: 'right',
            width: 100,
            render: (row) => {
              return (
            <span>
              <Dropdown overlay={actionMenu(row, props)}>
                <Button
                  type="link"
                  style={{ margin: 0 }}
                  onClick={e => e.preventDefault()}
                >
                  <i className="icon icon-options-vertical" />
                </Button>
              </Dropdown>
            </span>
              )
            },
          },
        ]
      }
    },
    action : (params) => {
      let dropdown = [{ value : '', text:'Pilih Aksi'}];
      if(getOneMenu(params,'Hapus')) dropdown.push({ value : 'delete', text:'Hapus Penerbit yang dipilih'});
      if(getOneMenu(params,'Sunting')) dropdown.push(
        { value : 'aktif', text:'Ubah Penerbit menjadi Aktif'},
        { value : 'nonaktif', text:'Ubah Penerbit menjadi Tidak Aktif'},
        { value : 'move-institusi', text:'Pindah Institusi yang dipilih'});

      return dropdown;
    }
  },
  add: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Penerbit', to: organisasiUrl.list },
      { text: 'Tambah Penerbit' },
    ],
    content: {
      title: 'Buat Penerbit Baru',
    },
  },
  edit: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'Penerbit', to: organisasiUrl.list },
      { text: 'Edit Penerbit' },
    ],
    content: {
      title: 'Edit Penerbit',
    },
  },
  schema: {
    organization_name: {
      label: 'Nama',
      rules: [
        { required: true, message: 'Silahkan input nama penerbit' },
      ]
    },
    organization_head: {
      label: 'Head ',
    },
    organization_email: {
      label: 'Email',
      rules: [
        { required: true, message: 'Silahkan input email' },
      ]
    },
    organization_website: {
      label: 'Website',
    },
    organization_phone: {
      label: 'Phone',
      rules: [
        { required: true, message: 'Silahkan input nomor telepon' },
      ]
    },
    organization_fax: {
      label: 'Fax',
    },
    organization_ppn: {
      label: 'PPN',
    },
    province_id: {
      label: 'Provinsi',
    },
    regency_id: {
      label: 'Kota/Kab',
    },
    district_id: {
      label: 'Kecamatan',
    },
    village_id: {
      label: 'Kelurahan',
      rules: [
        { required: true, message: 'Silahkan pilih kelurahan terdahulu' },
      ]
    },
    postalcode: {
      label: 'Postal Code'
    },
    organization_npwp: {
      label: 'NPWP'
    },
    organization_address: {
      label: 'Alamat',
    },
    organization_npwp_address: {},
    organization_about: {
      label: 'Tentang Penerbit',
      rules: [
        { required: true, message: 'Silahkan input deskripsi' },
      ]
    },
    organization_logo: {
      label: 'Logo',
    },
    organization_group_id: {
      label: 'Institusi',
      rules: [
        { required: true, message: 'Silahkan pilih institusi' },
      ]
    },
  },
  model: {
    status: 1,
    search: '',
    organization_group_id: ''
  },
  data: {
    status: [
      { value: '', text: 'Semua Status' },
      { value: 1, text: 'Aktif' },
      { value: 0, text: 'Tidak Aktif' },
    ],
  },
  move : {
    config: (...props) => {
      return {
        columns :[
          {
            title: 'Nama Penerbit',
            dataIndex: 'organization_name',
            key: 'organization_name',
            render: row => {
              return (row) ? row : ' - ';
            }
          },
          {
            title: 'Email',
            dataIndex: 'organization_email',
            key: 'organization_email',
            render: row => {
              return (row) ? row : ' - ';
            }
          },
          {
            title: 'Institusi',
            dataIndex: 'organization_group_name',
            key: 'organization_group_name',
            render: row => {
              return (row) ? row : ' - ';
            }
          }
        ]
      }
    }
  }
};