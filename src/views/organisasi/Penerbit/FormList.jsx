import React, { useEffect, useRef } from 'react';
import { Modal } from 'antd';
import config from './index.config';
import { MenuAksesContext } from './index';
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
import ModalMove from './components/ModalMove';
import ModalView from './components/ModalView';
import ActionForm from './components/ActionForm';
import useCustomReducer from 'common/useCustomReducer';
/* eslint-disable */
const resource = 'organization';
const { confirm } = Modal;
const { list } = config;
const initialData = {
  url : '',
  limit : 10,
  dataModal : [],
  selectedRow : [],
  modal : { visible : false },
  state : {
    loading : false,
    pagination: {
      defaultCurrent: 1,
      pageSize: 10,
      total: 0, 
      showSizeChanger: true, 
    },
    dataSource: []
  },
  modalView : { visible : false, data:{} },
}

const List = (props) => {
  const {model} = props;
  const menuAksesContext = React.useContext(MenuAksesContext);
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  const usePrevious = (value) => {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  }

  const onShowSizeChange = (current, pageSize) => {
    // console.log(current, pageSize);
    reducerFunc('limit',pageSize,'conventional');
  };

  const onSelectChange = (idSelected,e) => {
    reducerFunc('dataModal',e,'conventional');
    reducerFunc('selectedRow',idSelected,'conventional');
  };

  const rowSelection = {
    selectedRow : dataReducer?.selectedRow,
    onChange: onSelectChange,
  };

  const modalDelete = (props) => {
    confirm({
      title: 'Do you want to delete these items?',
      icon: 'delete',
      content: 'When clicked the OK button, this dialog will be closed',
      onOk() {
        return new Promise(async (resolve, reject) => {
          await oProvider.delete(`organization-list?id=${props.id}`);
          refresh('',{status:'',search:''});
          // await oProvider.list(`${resource}?limit=5`);
          setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {},
    });
  };

  const refresh = async (url,model) => {
    reducerFunc('state',{ loading: true });
    if(model.status === 0 || model.status === 1) url += `&organization_isactive=${model.status}`;
    if(model.search) url += `&organization_name=${model.search}`;
    if(model.organization_group_id) url += `&organization_group_id=${model.organization_group_id}`;
    
    try {
      const { meta, data } = await oProvider.list(`${resource}?limit=${dataReducer?.limit}${url}`);
      const pagination = {...dataReducer?.state?.pagination, 
        pageSize: dataReducer?.limit, 
        total: parseInt(meta.total), 
        onShowSizeChange:onShowSizeChange 
      };
      reducerFunc('state',{ pagination, dataSource: data, loading:false });
    } catch (error) {
      reducerFunc('state',{ loading: false });
      console.log(error?.message);
      return false;
    }
  };

  const handleTableChange = (pagination, filters, sorter) => {
    let url = '';
    if (pagination) url += `&page=${pagination.current}`;
    if (sorter && sorter.field) {
      url += `&sortBy=${sorter.field}`;
      url += `&sortDir=${sorter.order === 'ascend' ? 'asc' : 'desc'}`;
    }
    reducerFunc('url',url,'conventional');
  };

  const handleModalView = (row)=>{
    reducerFunc('modalView',{ visible:true,data:row });
  }

  const prevModel = usePrevious(model);
  const prevUrl = usePrevious(dataReducer?.url);

  React.useEffect(()=>{
    if(JSON.stringify(prevModel)!==JSON.stringify(model) && prevUrl !== '&page=1'){
      const pagination = {...dataReducer?.state?.pagination, current : 1}
      reducerFunc('state',{ pagination });
      const newUrl = '&page=1';
      reducerFunc('url',newUrl,'conventional');
    } else {
      refresh(dataReducer?.url,model);
    }
  },[model]);
  React.useEffect(() => console.log(dataReducer),[dataReducer]);
  React.useEffect(() => {
    refresh(dataReducer?.url,model);
  }, [dataReducer?.url,dataReducer?.limit]);

  return (
    <React.Fragment>
      <ActionForm 
        list={list}
        model={model}
        refresh={refresh}
        dataReducer={dataReducer}
        reducerFunc={reducerFunc}
        menuAksesContext={menuAksesContext}
      />
      <MocoTable
        {...config.list.config(props,modalDelete,menuAksesContext,handleModalView)}
        {...dataReducer?.state}
        rowSelection={rowSelection} 
        onChange={handleTableChange}
      />
      <ModalMove 
        {...dataReducer?.modal} 
        model={model}
        refreshTable={refresh}
        dataReducer={dataReducer}
        reducerFunc={reducerFunc}
      />
      <ModalView 
        dataReducer={dataReducer}
        reducerFunc={reducerFunc}
      />
    </React.Fragment>
  );
}

export default List;