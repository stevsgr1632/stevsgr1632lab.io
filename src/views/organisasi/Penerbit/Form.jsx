import React from 'react';
import { Button, Divider, Form, 
  Row, Col, Modal,Space } from 'antd';
import Message from 'common/message';
import oProvider from 'providers/oprovider';
import LoadingSpin from 'components/LoadingSpin';
import InputText from 'components/ant/InputText';
import InputFile from 'components/ant/InputFile3';
import InputSelect from 'components/ant/InputSelect';
import useCustomReducer from "common/useCustomReducer";
import InputCheckbox from 'components/ant/InputCheckbox';
import InputTextArea from 'components/ant/InputTextArea';
import MocoEditorQuill from 'components/MocoEditorQuill';
import InputSelectAsync from 'components/ant/InputSelectAsync';
/* eslint-disable */

const initialData = {
  data : {
    province: { loading: false, list: [] },
    city: { loading: false, list: [] },
    district: { loading: false, list: [] },
    village: { loading: false, list: [] }
  },
  loadingForm : false,
  alamat : '',
  dataSubmit : {},
  previewVisible : false
}
const Edit = (props) => {
  const [form] = Form.useForm();
  const { onSubmit, onCancel, schema,otherSubmit } = props;
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  const handlePreview = () => reducerFunc('previewVisible', true, 'conventional');
  const handleCancel = () => reducerFunc('previewVisible', false, 'conventional');

  const handleChangeInput = (name,value,e) => {
    if(name === 'organization_address'){
      reducerFunc('alamat', value, 'conventional');
    }
    reducerFunc('dataSubmit', { [name] : value });
  }

  const handleChangeLogo = (name, file,data) => {
    reducerFunc('dataSubmit', { [name]:data.file_url_download });
  }
  const handleChangeCombo = async (name, value) => {
    switch (name) {
      case 'province_id':
        reducerFunc('dataSubmit', { [name] : value });
        reducerFunc('data', { city: { loading: true, list: [] } });
        const citydata = await oProvider.list(`regencies-dropdown?id=${value}`);
        reducerFunc('data', { city: { loading: false, list: citydata.data }, district: [], village: [] });
        break;
      case 'regency_id':
        reducerFunc('dataSubmit', { [name] : value });
        reducerFunc('data', { district: { loading: true, list: [] } });
        const districtdata = await oProvider.list(`districts-dropdown?id=${value}`);
        reducerFunc('data', { district: { loading: false, list: districtdata.data } });
        break;
      case 'district_id':
        reducerFunc('dataSubmit', { [name] : value });
        reducerFunc('data', { village: { loading: true, list: [] } });
        const villagedata = await oProvider.list(`village-dropdown?id=${value}`);
        reducerFunc('data', { village: { loading: false, list: villagedata.data } });
        break;
      default:
        break;
    }
  };

  const handleChangeEditor = (value) => {
    reducerFunc('dataSubmit', { organization_about : value });
  }

  const handleCheckAlamat = (name,value,e) => {
    if (value) {
      reducerFunc('dataSubmit', { organization_npwp_address:dataReducer?.alamat });
      form.setFieldsValue({organization_npwp_address:dataReducer?.alamat});
    } else {
      form.setFieldsValue({organization_npwp_address:''});
      reducerFunc('dataSubmit', { organization_npwp_address:'' });
    }
  }

  const handleSimpanData = e => {
    e.preventDefault();
    form.validateFields().then(values => {
      const newFormSubmit = {...dataReducer.dataSubmit, organization_about : values.organization_about};
      otherSubmit(newFormSubmit);
    })
    .catch(errorInfo => {
      errorInfo.errorFields.forEach(x => {
        Message({type:'error',text:`Kesalahan, ${x.name.toString()} : ${x.errors.toString()}`});
      });
      return false;
    });
  };

  React.useEffect(() => {
    reducerFunc('loadingForm',true,'conventional');
    async function fetchData() {
      try {
        reducerFunc('data', { province: { loading: true, list: [] } });
        const response = await oProvider.list('province-dropdown');
        reducerFunc('data', { province: { loading: false, list: response?.data } });
  
        if (props.type === 'edit') {
          reducerFunc('data', {
            province: { loading: true, list: [] },
            city: { loading: true, list: [] },
            district: { loading: true, list: [] },
            village: { loading: true, list: [] }
          });
          const datarow = await oProvider.list(`organization-get-one?id=${props.idRow}`);
          const provdata = await oProvider.list(`province-dropdown`);
          const regencydata = await oProvider.list(`regencies-dropdown?id=${datarow?.data?.province_id}`);
          const districtdata = (datarow?.data?.regency_id) ? await oProvider.list(`districts-dropdown?id=${(datarow?.data?.regency_id)}`) : {};
          const villagedata = await oProvider.list(`village-dropdown?id=${datarow?.data?.district_id}`);
          reducerFunc('data', {
            province: { loading: false, list: provdata?.data },
            city: { loading: false, list: regencydata?.data },
            district: { loading: false, list: districtdata?.data },
            village: { loading: false, list: villagedata?.data }  
          });
          form.setFieldsValue({...datarow.data});
          reducerFunc('dataSubmit', datarow.data);
        }
        reducerFunc('loadingForm',false,'conventional');
      } catch(error){
        console.log(error?.message);
        reducerFunc('loadingForm',false,'conventional');
        return false;
      }
    }
    fetchData();
  }, []);

  return (
    <React.Fragment>
      <LoadingSpin loading={dataReducer?.loadingForm}>
      <Form form={form} onFinish={onSubmit} initialValues={[{organization_npwp_address:dataReducer?.alamat}]}>
        <Row gutter={[16, 0]}>
          <Col xs={24} sm={24} md={24} lg={4}>
            <InputFile name="organization_logo" accept=".png,.jpeg,.jpg" schema={schema} onChange={handleChangeLogo} imgUrl={(dataReducer?.dataSubmit?.organization_logo) ? dataReducer?.dataSubmit?.organization_logo : ''} handlePreview={handlePreview}/>        
          </Col>
          <Col xs={24} sm={24} md={24} lg={20}>
            <Row gutter={16}>
              <Col lg={12} md={24}>
                <InputSelectAsync name="organization_group_id" required addressApi='organization-group-dropdown' schema={schema} onChange={handleChangeInput}/>
              </Col>
              <Col lg={12} md={24}>
                <InputText name="organization_name" schema={schema} onBlur={handleChangeInput} />
              </Col>
            </Row>
         
            <Row gutter={16}>
              <Col lg={12} md={24}>
                <InputText name="organization_email" type="email" schema={schema} onBlur={handleChangeInput}/>
              </Col>
              <Col lg={12} md={24}>
                <InputText name="organization_head" schema={schema} onBlur={handleChangeInput}/>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={24}>
                <InputTextArea name="organization_address" schema={schema} onBlur={handleChangeInput} placeholder="Alamat sekarang"/>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col lg={12} md={24}>
                <InputSelect name="province_id" schema={schema} loading={dataReducer?.data?.province?.loading} data={dataReducer?.data?.province?.list} onChange={handleChangeCombo} others={{showSearch:true}} />
              </Col>
              <Col lg={12} md={24}>
                <InputSelect name="regency_id" schema={schema} loading={dataReducer?.data?.city?.loading} data={dataReducer?.data?.city?.list} onChange={handleChangeCombo} others={{showSearch:true}} />
              </Col>
            </Row>

            <Row gutter={16}>
              <Col lg={12} md={24}>
                <InputSelect name="district_id" schema={schema} loading={dataReducer?.data?.district?.loading} data={dataReducer?.data?.district?.list} onChange={handleChangeCombo} others={{showSearch:true}}/>
              </Col>
              <Col lg={12} md={24}>
                <InputSelect name="village_id" schema={schema} loading={dataReducer?.data?.village?.loading} data={dataReducer?.data?.village?.list} onChange={handleChangeInput} others={{showSearch:true}}/>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={24}>
                {/* <InputTextArea name="organization_about" schema={schema} onBlur={handleChangeInput}/> */}
                <MocoEditorQuill 
                  name='organization_about' 
                  onChange={handleChangeEditor} 
                  schema={schema} 
                  placeholder='Tentang Penerbit' 
                  value={dataReducer?.dataSubmit?.organization_about} 
                  form={form}
                  required={false}/>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col lg={8} md={24}>
                <InputText name="organization_website" type="url" schema={schema} onBlur={handleChangeInput}/>
              </Col>
              <Col lg={8} md={24}>
                <InputText name="organization_phone" pattern="\(?(?:\+62|62|0)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}" schema={schema} onBlur={handleChangeInput}/>
              </Col>
              <Col lg={8} md={24}>
                <InputText name="organization_fax" pattern="\(?(?:\+62|62|0)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}" schema={schema} onBlur={handleChangeInput}/>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={24}>
              <Divider orientation="left">PAJAK</Divider>
              </Col>
            </Row>

            <Row gutter={[16,0]}>
              <Col span={8}>
                <InputCheckbox name="organization_ppn" schema={schema} onChange={handleChangeInput} />
              </Col>
              <Col span={16}>
                  <InputCheckbox name="npwp_address" onChange={handleCheckAlamat} others={{disabled : (props.type === 'edit') ? true : false}}>
                    Alamat NPWP sama dengan Alamat Perusahaan
                  </InputCheckbox>
              </Col>
            </Row>
            <Row gutter={[16,0]}>
              <Col span={8}>
                <InputText name="organization_npwp" pattern="^[\d]{11,15}$" schema={schema} onChange={handleChangeInput} />
              </Col>
              <Col span={16}>
                <InputTextArea name="organization_npwp_address" schema={schema} onBlur={handleChangeInput} placeholder="Alamat NPWP" />
              </Col>
            </Row>
            <Row>
            </Row>
          </Col>
        </Row>
        <hr />
        <Space direction="horizontal" style={{float:'right'}}>
          <Button danger type="primary" className="ml-1" onClick={onCancel}>Kembali</Button>
          <Button type="primary" htmlType="button" onClick={handleSimpanData}>Simpan</Button>
        </Space>
        
      </Form>
      </LoadingSpin>
      <Modal
        visible={dataReducer?.previewVisible}
        title="Preview"
        footer={null}
        centered
        onCancel={handleCancel}
      >
      <img alt="example" style={{ width: '100%' }} src={dataReducer?.dataSubmit?.organization_logo} />
    </Modal>
    </React.Fragment>
  );
}

export default Edit;
