import React,{useState} from 'react';
import Axios from 'axios';
import queryString from "query-string";
import {Row,Col,Button,Tooltip} from 'antd';
import randomString from 'utils/genString';
import oProvider from 'providers/oprovider';
import { MessageContent } from 'common/message';
import InputSelect from 'components/ant/InputSelect';
import { initialEndpoint } from 'utils/initialEndpoint';
import LocalStorageService from 'utils/localStorageService';
/* eslint-disable */

const apiUrl = process.env.REACT_APP_BASE_URL;

const ActionForm = (props) => {
  const { model , refresh, list, menuAksesContext,dataReducer, reducerFunc} = props;
  const [selectAction, setSelectAction] = useState('');

  const handleChangeAction = (name,val,e) => {
      setSelectAction(val);
  }

  const getHeaders = () => {
    const { attributes } = LocalStorageService.state_auth.getAccessToken();
    const headers = {
      'Accept': 'application/vnd.api+json',
      'Content-Type': 'application/vnd.api+json',
      'Access-Control-Allow-Origin': true
    }

    if (attributes && attributes.accessToken) {
      headers.Authorization = `Bearer ${attributes.accessToken}`;
    }
    return headers;
  }

  const handleAction = async () => {
    const urlAPI = `${apiUrl}/${initialEndpoint}/organization-move`;
    let type = selectAction;
    let actionId = '';
    let key = randomString(17);
    try {
      if(dataReducer?.selectedRow.length > 0 && selectAction){
        if(selectAction==='move-institusi'){
          reducerFunc('modal',{ visible : true});
        }else {
          if(['aktif','nonaktif'].includes(selectAction)){
            type = 'status';
            actionId = selectAction;
          }
          if(['move-institusi'].includes(selectAction)){
            type = 'move-institusi';
            actionId = selectAction;
          }
          const payloads = { 
            type,
            organizationIds : dataReducer?.selectedRow.join()
          };
  
          if(actionId){
            payloads.actionId = actionId;
          }
          MessageContent({ type :'loading', content:'Loading...',key,duration: 0 });
          const response = await Axios.post(urlAPI, { ...payloads } ,{ headers : getHeaders() });
          if(response){
            MessageContent({ type :'success', content:'Data berhasil diproses',key,duration: 2 });
            refresh('',model);
          }
        }
      } else {
        MessageContent({ type :'info', content:'Silahkan Pilih Penerbit dan Aksi terlebih dahulu', duration: 2 });
      }
    } catch (error) {
      MessageContent({ type :'error', content:'Terjadi kesalahan saat proses data',key,duration: 2 });
      return false
    }
  }

  const downloadExcel = async () => {
    let key = randomString(17);
    MessageContent({ type :'loading', content:'Mengunduh ... ',key, duration: 0 });
    try {
      let paramsObject = {};

      if(model.search) paramsObject['organization_name'] = model.search;
      if(model.organization_group_id) paramsObject['organization_group_id'] = model.organization_group_id;
      if(model.status === 0 || model.status === 1 ) paramsObject['organization_isactive'] = model.status;

      const paramsUrl = queryString.stringify(paramsObject, {skipEmptyString : true});
      const params = (paramsUrl) ? `?${paramsUrl}` : '' ;
      const response = await oProvider.downloadfile(`organization-download-excel${params}`);

      if(response){
        MessageContent({ type :'success', content: 'Selesai!', key, duration: 2 });
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'Penerbit-MCCP.xls'); //or any other extension
        document.body.appendChild(link);
        link.click();
      }
    } catch (error) {
      MessageContent({ type :'error', content: 'Terjadi kesalahan saat download !', key, duration: 2 });
      console.log('error',error);
      return false;
    }
  };
  const styleButton = {width:'100px',background:'#CB1C31',border:'none'};

  return (
    <Row gutter={[16,16]}>
      <Col span={8}>
        <div style={{display:'grid',gridTemplateColumns:'3fr 1fr',gridColumnGap:'0.5em'}}>
          <InputSelect name="aksi" data={[...list.action(menuAksesContext)]} placeholder="Pilih Aksi" onChange={handleChangeAction} />
          <Button type="primary" onClick={handleAction}  style={styleButton}>
          Pilih Aksi
          </Button>
        </div>
      </Col>
      <Col span={16} style={{textAlign:'right'}}>
        <Tooltip title='Download Excel'>
          <Button type="primary" onClick={downloadExcel} style={styleButton}>
          Excel
          </Button>
        </Tooltip>
      </Col>
    </Row>
  )
}

export default ActionForm
