import React from 'react';
import Axios from 'axios';
import config from '../index.config';
import {Button, Modal,Row,Col} from 'antd';
import randomString from 'utils/genString';
import oprovider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
import {CardHeader,CardBody} from 'reactstrap';
import { MessageContent } from 'common/message';
import {CloseSquareFilled} from '@ant-design/icons';
import InputSelect from 'components/ant/InputSelect';
import useCustomReducer from 'common/useCustomReducer';
import { initialEndpoint } from 'utils/initialEndpoint';
import LocalStorageService from 'utils/localStorageService';
/* eslint-disable */

const apiUrl = process.env.REACT_APP_BASE_URL;
const initialData = {
  state : {
    loading : false,
    pagination: {
      defaultCurrent: 1,
      pageSize: 10,
      total: 0,
    },
    dataSource: []
  },
  loading : false,
  institusiId : '',
  listInstitusi : []
}
const ModalMove = (props) => {
  const { move } = config;
  const { visible, dataReducer,reducerFunc,model,refreshTable} = props;
  const [modalReducer,modalReducerFunc] = useCustomReducer(initialData);
  const schema = {
    institusi:{
      label : 'Pilih Institusi'
    }
  }

  const handleChangeAction = (name,value,e) => {
    modalReducerFunc('institusiId',value,'conventional');  
  }

  const getHeaders = () => {
    const { attributes } = LocalStorageService.state_auth.getAccessToken();
    const headers = {
      'Accept': 'application/vnd.api+json',
      'Content-Type': 'application/vnd.api+json',
      'Access-Control-Allow-Origin': true
    }

    if (attributes && attributes.accessToken) {
      headers.Authorization = `Bearer ${attributes.accessToken}`;
    }
    return headers;
  }

  const handleProses = async () => {
    const urlAPI = `${apiUrl}/${initialEndpoint}/organization-move`;
    let key = randomString(17);
    modalReducerFunc('loading',true,'conventional');
    try {
      const payloads = {  
        type: 'move-institusi',
        actionId: modalReducer?.institusiId,
        organizationIds: dataReducer?.selectedRow.join()
      }

      const response = await Axios.post(urlAPI,{...payloads},{ headers : getHeaders() });
      if(response){
        MessageContent({ type :'success', content:'Data berhasil diproses',key,duration: 2 });
        reducerFunc('dataModal',[],'conventional');
        reducerFunc('selectedRow',[],'conventional');
        modalReducerFunc('loading',false,'conventional');
        reducerFunc('modal',{ visible : false });
        refreshTable('',model);
      }
    } catch (error) {
      MessageContent({ type :'error', content:'Terjadi kesalahan saat proses data',key,duration: 2 });
      modalReducerFunc('loading',false,'conventional');
      return false;
    }
  }

  const fetchData = async () => {
    try {
      const response = await oprovider.list('organization-group-dropdown');
      if(response){
        modalReducerFunc('listInstitusi',response.data,'conventional');
      }
    } catch (error) {
      return false;
    }
  }
  const getContainer = (triggernode) => {
    return triggernode.parentNode;
  };

  React.useEffect(()=>{
    fetchData();
  },[]);

  React.useEffect(()=>{
    modalReducerFunc('state',{dataSource:dataReducer?.dataModal});
  },[dataReducer?.dataModal]);

  return (
    <Modal
      width={800}
      visible={visible}
      onCancel={() => reducerFunc('modal',{ visible: false })}
      footer={null}
      bodyStyle={{padding:'0px'}}
      centered={true}
      closeIcon={
        <div style={{width:'100%',height:'100%',justifyContent:'center'}}>
          <CloseSquareFilled style={{fontSize:'35px',color:'white'}}/>
        </div>}
    >
      <CardHeader style={{
        display:'flex',
        justifyContent:'space-between',
        backgroundColor:'#C92036',
        color:'white',
        border:'none'}}
      >
        <div>
          <h3 style={{color:'white'}}>Pindah Instansi</h3>
          <div>Lakukan pengecekan data sebelum data disimpan</div>
        </div>
      </CardHeader>
      <CardBody>
        <Row gutter={16}>
          <Col span={24}>
            <div style={{display:'grid',gridTemplateColumns:'3fr 1fr',gridColumnGap:'0.5em'}}>
              <InputSelect 
                name="institusi" 
                data={modalReducer?.listInstitusi} 
                schema={schema} 
                placeholder="Pilih Institusi" 
                labelCol={12}  
                onChange={handleChangeAction} 
                others = {{getPopupContainer: getContainer, showSearch:true}}/>
              <Button type="primary" onClick={handleProses}  style={{width:'100px',background:'#CB1C31',border:'none'}} loading={modalReducer?.loading}>
                {(modalReducer?.loading) ? 'loading' : 'Proses'}
              </Button>
            </div>
          </Col>
        </Row>
        <MocoTable {...move.config(props)} {...modalReducer?.state}/>
      </CardBody>
    </Modal>
  );
}

export default ModalMove
