import React from 'react';
import { Row, Col,Modal } from 'antd';
import oProvider from 'providers/oprovider';
import LoadingSpin from 'components/LoadingSpin';
import { CardBody, CardHeader } from 'reactstrap';
import { noImagePath } from "utils/initialEndpoint";
import { CloseSquareFilled } from '@ant-design/icons';
/* eslint-disable */

const ModalView = (props) => {
  const { dataReducer, reducerFunc} = props;
  const [state, setState] = React.useState({});
  const [loading, setLoading]= React.useState(false);

  const fetchData = async () =>{
    setLoading(true);
    try {
      if(dataReducer?.modalView?.visible){
        const response = await oProvider.list(`organization-view?id=${dataReducer?.modalView?.data?.id}`);
        console.log(response);
        if (response) {
          setState(response.data);
        }
      }
      setLoading(false);
    } catch (error) {
      console.log('error',error);
      setLoading(false);
    }
  }

  React.useEffect(()=>{
    fetchData();
  },[dataReducer?.modalView?.visible]);


  return (
    <Modal
      width={800}
      visible={dataReducer?.modalView.visible}
      onCancel={() => {
        reducerFunc('modalView',{ visible: false });
        setState({});
      }}
      footer={null}
      bodyStyle={{padding:'0px'}}
      centered
      closeIcon={
        <div style={{width:'100%',height:'100%',justifyContent:'center'}}>
          <CloseSquareFilled style={{fontSize:'35px',color:'white'}}/>
        </div>}
    >
      <CardHeader style={{
          display:'flex',
          justifyContent:'space-between',
          backgroundColor:'#C92036',
          color:'white',
          border:'none'}}>
          <div>
            <h3 style={{color:'white'}}>Pratinjau Penerbit</h3>
            <div>Detail Informasi Penerbit</div>
          </div>
      </CardHeader>
      <CardBody>
        <LoadingSpin loading={loading}>
          <Row gutter={[16,16]}>
            <Col span={4} style={{display:'flex',alignItems:'center'}}>
              <div style={{width:'100%'}}>
                <img src={(state.organization_logo) ? state.organization_logo : noImagePath} 
                  onError={()=>setState(prev => ({...prev,organization_logo:noImagePath}))} 
                  alt='Logo'
                  width="100%"/>
              </div>
            </Col>
            <Col span={20}>
              <Row gutter={[8,8]}>
                  <Col span={4}>
                    Nama Penerbit
                  </Col>
                  <Col span={20}>
                    : {(state.organization_name) ? state.organization_name : '-'}
                  </Col>
              </Row>
              <Row gutter={[8,8]}>
                <Col span={4}>
                  Institusi
                </Col>
                <Col span={20}>
                  : {(state.organization_group_name) ? state.organization_group_name : '-'}
                </Col>
              </Row>
              <Row gutter={[8,8]}>
                <Col span={4}>
                    Email
                </Col>
                <Col span={20}>
                  : {(state.organization_email) ? state.organization_email : '-'}
                </Col>
              </Row>
              <Row gutter={[8,8]}>
                <Col span={4}>
                    Telepon
                </Col>
                <Col span={20}>
                  : {(state.organization_phone) ? state.organization_phone : '-'}
                </Col>
              </Row>
              <Row gutter={[8,8]}>
                <Col span={4}>
                  Alamat
                </Col>
                <Col span={20}>
                    : {(state.organization_address) ? state.organization_address : '-'}
                </Col>
              </Row>
              <Row gutter={[8,8]}>
                <Col span={4}>
                  Tentang Kami :
                </Col>
                <Col span={20}>
                  <span dangerouslySetInnerHTML={{__html: (state.organization_about) ? state.organization_about : '-'}}></span>
                </Col>
              </Row>
            </Col>
          </Row>
        </LoadingSpin>
      </CardBody>
    </Modal>
  )
}

export default ModalView
