import React from 'react';
import { Col, Row,Form } from 'antd';
import ListForm from './FormList';
import config from './index.config';
import { connect } from 'react-redux';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import getMenuAkses from "utils/getMenuAkses";
import { Breadcrumb, Content } from 'components';
import InputSelect from 'components/ant/InputSelect';
import InputSearch from 'components/ant/InputSearch';
import useCustomReducer from 'common/useCustomReducer';
/* eslint-disable */

const initialData = {
  model : config.model,
  menuAkses : [],
  organization : []
};
export const MenuAksesContext = React.createContext([]);

const App = (props) => {
  const [form] = Form.useForm();
  const { list, data } = config;
  const { organisasi } = pathName;
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  const handleChange = (name, value) => {
    reducerFunc('model',{ [name]: value });
  }

  const handleChangeSearch = (e) => {
    if(e.target.value===''){
      reducerFunc('model',{ [e.target.name]: '' });
    }
  }

  const fetchData = async () => {
    try {
      const {data} = await oProvider.list(`organization-group-dropdown`);
      if(data){
        data.unshift({text: "-- Semua Penerbit --", value: ""});
        reducerFunc('organization',data,'conventional');
      }
    } catch (error) {
      console.log(`Error : ${error}`);
    }
  }
 
  React.useEffect(()=>{
    fetchData();
  },[]);

  React.useEffect(() => {
    if(props.menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(organisasi.list);
      reducerFunc('menuAkses',menuAccessFromLocalStorage,'conventional');
    }
  },[props.menuAccess]);
  
  return (
    <MenuAksesContext.Provider value={dataReducer?.menuAkses}>
      <Breadcrumb items={list.breadcrumb} />
      <Content {...{ ...props, ...list.content, menuAkses: dataReducer?.menuAkses }}>
        <Form form={form}>
          <Row gutter={16}>
            <Col span={8}>
              <InputSelect name="status" defaultVal={dataReducer?.model?.status || ''} text="Status" data={data?.status} onChange={handleChange} />
            </Col>
            <Col span={8}>
              <InputSelect name="organization_group_id" defaultVal={dataReducer?.model?.status || ''} text="Institusi" data={dataReducer?.organization} onChange={handleChange} others={{showSearch:true}}/>
            </Col>
            <Col span={8} style={{marginTop:'0.6em'}}>
              <InputSearch name="search" text="Pencarian" placeholder="Cari Berdasarkan Nama" onSearch={handleChange} onChange={handleChangeSearch}/>
            </Col>
          </Row>
        </Form>
        <hr />
        <ListForm {...props} model={dataReducer?.model} />
      </Content>
    </MenuAksesContext.Provider>
  );
};
const mapStateToProps = ({menuAccess}) => ({menuAccess});
export default connect(mapStateToProps)(App);