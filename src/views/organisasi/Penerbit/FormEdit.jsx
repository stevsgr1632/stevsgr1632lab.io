import React from 'react';
import FormControl from './Form';
import config from './index.config';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import { Breadcrumb, Content } from 'components';
/* eslint-disable */
const { organisasi: organisasiUrl } = pathName;
const resource = 'organization';

const App = (props) => {
  const [, setemail] = React.useState([]);
  const [dataRow, setDataRow] = React.useState({});
  const [loading, setloading] = React.useState(false);
  const { schema } = config;
  const { breadcrumb, content } = config.edit;
  const catId = props.match.params.id;

  const backToList = () => {
    props.history.push(organisasiUrl.list);
  };

  const getOrgbyId = async (id) => {
    setloading(true);

    try {
      const { data } = await oProvider.list(`organization-get-one?id=${id}`);
      setDataRow(data);
      setloading(false);
      let dataOrg = await oProvider.list('organization');
      let allEmail = await dataOrg?.data?.map(x => {
        let dataku = x.organization_email;
        return dataku;
      }).filter(Boolean).filter(val => val !== data.organization_email);
      // console.log(allEmail);
      setemail(allEmail);
      // delete data[0].category_image;
    } catch (error) {
      setloading(false);
      console.log(error?.message);
      return false;
    }
  }

  const handleSubmit = async (e) => {
    console.log(e);    
  }

  const handleOtherSubmit = async e => {
    setloading(true);
    try{
      (e.organization_ppn) ? e.organization_ppn = 1 : e.organization_ppn = 0;
 
      const response = await oProvider.update(`${resource}?id=${catId}`, { ...e });
      console.log('ini response',response);
      if(response){
        Notification({
          response: {
            code : 'Success', 
            message : response.message
          }, 
          type:'success', 
          placement:'bottomRight'
        });
        backToList();
      }
      setloading(false);      
    } catch (error) {
      Notification({
        response: {
          code : 'Error', 
          message : error?.response?.data?.message
        }, 
        type:'error', 
        placement:'bottomRight'
      });
      setloading(false);
      return false;
    }
  }

  React.useEffect(() => {
    getOrgbyId(catId);
  }, [catId]);

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <FormControl
          onSubmit={handleSubmit}
          otherSubmit={handleOtherSubmit}
          onCancel={backToList}
          schema={schema}
          type="edit"
          dataRow={!loading && dataRow}
          idRow={catId}
        />
      </Content>
    </React.Fragment>
  );
}

export default App;