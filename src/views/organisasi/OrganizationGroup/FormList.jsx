import React, { useRef } from 'react';
import { Modal, Row, Col, Button, Tooltip } from 'antd';
import Axios from 'axios';
import config from './index.config';
import queryString from "query-string";
import oProvider from 'providers/webhook';
import randomString from 'utils/genString';
import provider from 'providers/oprovider';
import { MenuAksesContext } from './index';
import MocoTable from 'components/MocoTable';
import ModalView from './component/ModalView';
import Notification from 'common/notification';
import { MessageContent } from 'common/message';
import {DeleteOutlined} from '@ant-design/icons';
import InputSelect from 'components/ant/InputSelect';
import useCustomReducer from 'common/useCustomReducer';
import { initialEndpoint } from 'utils/initialEndpoint';
import LocalStorageService from "utils/localStorageService";
/* eslint-disable */

const resource = 'organization-group';
const apiUrl = process.env.REACT_APP_BASE_URL;
const { confirm } = Modal;
const initialData = {
  url : '',
  limit : 10,
  selectedRow : [],
  selectAction : '',
  state : {
    loading : false,
    pagination: {
      defaultCurrent: 1,
      pageSize: 10,
      total: 0, 
      showSizeChanger: true, 
    },
    dataSource: []
  },
  modal : { visible : false, data:{}},
};
const List = (props) => {
  const {list} = config;
  const {model} = props;
  const menuAksesContext = React.useContext(MenuAksesContext);
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  const onShowSizeChange = (current, pageSize) => {
    reducerFunc('limit',pageSize,'conventional');
  }

  const onSelectChange = (idSelected,e) => {
    reducerFunc('selectedRow',idSelected,'conventional');
  };

  const rowSelection = {
    selectedRow : dataReducer?.selectedRow,
    onChange: onSelectChange,
  };

  const handleChangeAction = (name,val,e) => {
    reducerFunc('selectAction',val,'conventional');
  }

  const getHeaders = () => {
    const { attributes } = LocalStorageService.state_auth.getAccessToken();
    const headers = {
        'Accept': 'application/vnd.api+json',
        'Content-Type': 'application/vnd.api+json',
        'Access-Control-Allow-Origin': true
    }

    if (attributes && attributes.accessToken) {
        headers.Authorization = `Bearer ${attributes.accessToken}`;
    }
    return headers;
  }

  const handleAction = async () => {
    const urlAPI = `${apiUrl}/${initialEndpoint}/organization-group-move`;
    let type = dataReducer?.selectAction;
    let actionId = '';
    let key = randomString(17);
    try {
      if(dataReducer?.selectedRow.length > 0 && dataReducer?.selectAction){
        if(['aktif','nonaktif'].includes(dataReducer?.selectAction)){
          type = 'status';
          actionId = dataReducer?.selectAction;
        }
        const payloads = { 
          type,
          organizationIds : dataReducer?.selectedRow.join()
        };

        if(actionId){
          payloads.actionId = actionId;
        }
        MessageContent({ type :'loading', content:'Loading...',key,duration: 0 });
        const response = await Axios.post(urlAPI, payloads ,{ headers : getHeaders() });
        if(response){
          MessageContent({ type :'success', content:'Data berhasil diproses',key,duration: 2 });
          refresh(dataReducer?.url,model);
        }
      } else {
        MessageContent({ type :'info', content:'Silahkan Pilih Institusi dan Aksi terlebih dahulu', duration: 2 });
      }
    } catch (error) {
      MessageContent({ type :'error', content:'Terjadi kesalahan saat proses data',key,duration: 2 });
      return false
    }
  }

  const downloadExcel = async () => {
    let key = randomString(17);
    MessageContent({ type :'loading', content:'Mengunduh ... ',key, duration: 0 });
    try {
      let paramsObject = {};

      if(model.penerbit) paramsObject['organization_id'] = model.penerbit;
      if(model.company) paramsObject['organization_group_id'] = model.company;
      if(model.status === 0 || model.status === 1 ) paramsObject['organization_group_isactive'] = model.status;

      const paramsUrl = queryString.stringify(paramsObject, {skipEmptyString : true});
      const params = (paramsUrl) ? `?${paramsUrl}` : '' ;
      const response = await provider.downloadfile(`organization-group-download-excel${params}`);

      if(response){
        MessageContent({ type :'success', content: 'Selesai!', key, duration: 2 });
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'Institusi-MCCP.xlsx'); //or any other extension
        document.body.appendChild(link);
        link.click();
      }
    } catch (error) {
      MessageContent({ type :'error', content: 'Terjadi kesalahan saat download !', key, duration: 2 });
      console.log('error',error);
      return false;
    }
  }

  const usePrevious = (value) => {
    const ref = useRef();
    React.useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  }

  const openNotification = (type, title, message, placement='topRight') => {
    Notification({type, 
      response : {
        message: title,
        description: message
      }, 
      placement 
    });
  };

  const modalDelete = async (props) => {
    confirm({
      title: 'Anda yakin ingin menghapus data ?',
      icon: <DeleteOutlined/>,
      content: 'Data yang sudah dihapus tidak dapat dibatalkan',
      onOk() {
        let messageText = '';
        return new Promise(async (resolve, reject) => {
          try {
            await oProvider.delete(`organization-group?id=${props.id}`);
            refresh('',{status:'',search:''});
            openNotification('success', 'Moco Catalog', 'Data berhasil dihapus', 'bottomRight');
            resolve();
          } catch (error) {
            messageText = error.message;
            openNotification('error', 'Moco Catalog', messageText, 'bottomRight')
            resolve();
          }          
        });
      },
      onCancel() {},
    });
  };

  const refresh = async (url,model) => {
    reducerFunc('state',{ loading:true });
    try {
      if(model.status=== 0 || model.status=== 1 ) url += `&organization_group_isactive=${model.status}`
      if(model.company) url += `&organization_group_id=${model.company}`
      if(model.penerbit) url += `&organization_id=${model.company}`
      
      const { meta, data } = await oProvider.list(`${resource}?limit=${dataReducer?.limit}${url}`);
      const pagination = {...dataReducer?.state?.pagination, 
        pageSize: dataReducer?.limit, 
        total: parseInt(meta.total), 
        onShowSizeChange:onShowSizeChange 
      };
      reducerFunc('state',{ pagination, dataSource: data, loading:false });
      reducerFunc('selectedRow',[],'conventional');
    } catch (error) {
      reducerFunc('state',{ loading:false });
      console.log(error?.message);
      return false;
    }
  }

  const handleTableChange = (pagination, filters, sorter) => {
    let url = '';
    if (pagination) url += `&page=${pagination.current}`;
    if (sorter && sorter.field) {
      url += `&sortBy=${sorter.field}`;
      url += `&sortDir=${sorter.order === 'ascend' ? 'asc' : 'desc'}`;
    }
    reducerFunc('url',url,'conventional');
  }

  const modalDetail = (row)=>{
    reducerFunc('modal',{ visible : true, data:row });
  };

  const prevModel = usePrevious(model);
  const prevUrl = usePrevious(dataReducer?.url);
  
  React.useEffect(()=>{
    if(JSON.stringify(prevModel)!==JSON.stringify(model) && prevUrl !== '&page=1'){
      const pagination = {...dataReducer?.state?.pagination, current : 1}
      reducerFunc('state',{ pagination });
      const newUrl = '&page=1';
      reducerFunc('url',newUrl,'conventional');
    } else {
      refresh(dataReducer?.url,model);
    }
  },[model]);

  React.useEffect(() => {
      refresh(dataReducer?.url,model);
  }, [dataReducer?.url,dataReducer?.limit,model]);

  return (
    <React.Fragment>
      <hr/>
      <Row gutter={[16,16]}>
        <Col span={8}>
          <div style={{display:'grid',gridTemplateColumns:'3fr 1fr',gridColumnGap:'0.5em'}}>
            <InputSelect name="aksi" data={[...list.action(menuAksesContext)]} placeholder="Pilih Aksi" onChange={handleChangeAction} />
            <Button type="primary" onClick={handleAction}  style={{width:'100px',background:'#CB1C31',border:'none'}}>
              Pilih Aksi
            </Button>
          </div>
        </Col>
        <Col span={16} style={{textAlign:'right'}}>
          <Tooltip title='Download Excel'>
            <Button type="primary" onClick={downloadExcel} style={{width:'100px',background:'#CB1C31',border:'none'}}>
              Excel
            </Button>
          </Tooltip>
        </Col>
      </Row>
      <MocoTable
        {...config.list.config(props, modalDelete, menuAksesContext, modalDetail)}
        {...dataReducer?.state}
        rowSelection={rowSelection} 
        onChange={handleTableChange}
      />
      <ModalView 
        modal={dataReducer?.modal}
        setModal={reducerFunc}/>
    </React.Fragment>
  );
}

export default List;