import React from 'react';
import ListForm from './FormList';
import config from './index.config';
import { Col, Row,Form } from 'antd';
import { connect } from 'react-redux';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import getMenuAkses from "utils/getMenuAkses";
import { Breadcrumb, Content } from 'components';
import InputSelect from 'components/ant/InputSelect';
import useCustomReducer from 'common/useCustomReducer';
/* eslint-disable */

export const MenuAksesContext = React.createContext([]);
const initialData = {
  model: config.model,
  company : [],
  penerbit : [],
  menuAkses : []
};
const App = (props) => {
  const { holding } = pathName;
  const { list,data } = config;
  const [form] = Form.useForm();
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  const handleChange = (name, value) => {
    reducerFunc('model',{ [name]: value });
  }
  const handleSubmit = value => {
    console.log('Sukses',value);
  }
  
  const handleChangeCompany = async (name,value) => {
    try {
      const {data} = await oProvider.list(`organization-dropdown?id=${value}&showAll=1`);
      data.unshift({ value: '', text: '-- Semua Penerbit --' });
      reducerFunc('penerbit',data,'conventional');
      reducerFunc('model',{ [name]: value });
    } catch (error) {
      console.log(error?.message);
    }
  }

  React.useEffect(() => {
    async function fetchData(){
      try {
        let { data } = await oProvider.list(`organization-group-dropdown`);
        data.unshift({ value: '', text: '-- Semua Perusahaan --' });
        reducerFunc('company',data,'conventional');
      } catch (error) {
        console.log(error?.message);
        return false;
      }
    }
    fetchData();
  },[]);
  React.useEffect(() => {
    if(props.menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(holding.list);
      reducerFunc('menuAkses',menuAccessFromLocalStorage,'conventional');
    }
  },[props.menuAccess]);

  return (
    <MenuAksesContext.Provider value={dataReducer?.menuAkses}>
      <Breadcrumb items={list.breadcrumb} />
      <Content {...{ ...props, ...list.content, menuAkses : dataReducer?.menuAkses }} >
        <Form form={form} onFinish={handleSubmit}>
          <Row gutter={[16,24]}>
            <Col span={8}>
              <InputSelect name="company" defaultVal={dataReducer?.model.company || ''} text="Institusi" data={dataReducer?.company} onChange={handleChangeCompany} placeholder="Masukan nama perusahaan" others={{showSearch : true}} />
            </Col>
            <Col span={8}>
              <InputSelect name="penerbit" defaultVal={dataReducer?.model.penerbit || ''} text="Penerbit" data={dataReducer?.penerbit} onChange={handleChange} />
            </Col>
            <Col span={8}>
              <InputSelect name="status" defaultVal={dataReducer?.model.status || ''} text="Status" data={data.status} onChange={handleChange} />
            </Col>
          </Row>
        </Form>
        <ListForm {...props} model={dataReducer?.model} />
      </Content>
    </MenuAksesContext.Provider>
  );
}

const mapStateToProps = ({menuAccess}) => ({menuAccess});
export default connect(mapStateToProps)(App);
