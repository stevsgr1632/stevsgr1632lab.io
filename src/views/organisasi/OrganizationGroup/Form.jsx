import React from 'react';
import moment from 'moment';
import Message from 'common/message';
import { Form, Steps,Divider} from 'antd';
import oProvider from 'providers/oprovider';
import ButtonForm from './component/ButtonForm';
import LoadingSpin from 'components/LoadingSpin';
import InputControl from './component/InputControl';
import useCustomReducer from 'common/useCustomReducer';
import ProfileCompany from './component/ProfileCompany';
/* eslint-disable */

const initialData = {
  current : 0,
  typeOrg : [],
  dataSubmit : {},
  bankChoices : [],
  jenisProduk : [],
  typeOrgGroup : {},
  typeOrgSelect : '',
  loadingForm : false,
  jenisProductSelect : [],
  data : {
    province: { loading: false, list: [] },
    city: { loading: false, list: [] },
    district: { loading: false, list: [] },
    village: { loading: false, list: [] }
  },
};
const Edit = (props) => {
  // console.log(props);
  const { Step } = Steps;
  const [form] = Form.useForm();
  const mountedRef = React.useRef(true);
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);
  const { onSubmit, otherSubmit, onCancel, schema } = props;
  const steps = [
    {title: 'Jenis Perusahaan',},
    {title: 'Jenis Produk',},
    {title: 'Profil Perusahaan',},
  ];

  const handleChangeLogo = (name, value,data) => {
    reducerFunc('dataSubmit',{ organization_group_logo: data.file_url_download });
  }

  const handleImageboxChange = (value) => {
    reducerFunc('dataSubmit',{ org_type_id:value });
    reducerFunc('typeOrgSelect',value,'conventional');
  }
  const handleImageboxChange2 = (value) => {
    reducerFunc('dataSubmit',{ organization_group_content:value.join(',') });
    reducerFunc('jenisProductSelect',[...value],'conventional');
  }

  const handleStepChange = currentNext => {
    if(currentNext === 1){
      if(dataReducer?.current === 0 && !dataReducer?.dataSubmit?.org_type_id){
        Message({type : 'error',text:`${steps[dataReducer?.current].title} harus dipilih`});
        return false;
      }
    }
    if(currentNext === 2 ){
      if(dataReducer?.current === 0 && !dataReducer?.dataSubmit?.org_type_id){
        Message({type : 'error',text:`${steps[dataReducer?.current].title} harus dipilih`});
        return false;
      }
      if(dataReducer?.current === 0 && !dataReducer?.dataSubmit?.organization_group_content){
        Message({type : 'error',text:`${steps[dataReducer?.current + 1].title} harus dipilih`});
        return false;
      }
      if(dataReducer?.current === 1 && !dataReducer?.dataSubmit?.organization_group_content){
        Message({type : 'error',text:`${steps[dataReducer?.current].title} harus dipilih`});
        return false;
      } 
    }
    reducerFunc('current',currentNext,'conventional');
  }

  const next = () => {
    if(dataReducer?.current === 0 && !dataReducer?.dataSubmit?.org_type_id){
      Message({type : 'error',text:'Type organisasi group harus di pilih'});
      return false;
    }
    if(dataReducer?.current === 1 && !dataReducer?.dataSubmit?.organization_group_content){
      Message({type : 'error',text:'Organisasi group konten harus di pilih'});
      return false;
    }
    reducerFunc('current',(dataReducer?.current + 1),'conventional');
  }

  const prev = () => {
    reducerFunc('current',(dataReducer?.current - 1),'conventional');
  }

  const handleChangeFile = (name, name2, file) => {
    reducerFunc('dataSubmit',{ [name]: file.file_url_download, [name2] : file.originalFilename });
  }

  const handleChangeInput = (name, value,e) => {
    if(name === 'lelang' || name === 'rekanan' || name === 'penunjukan' || name === 'corporate' ){
      const group_procurement = {...dataReducer?.dataSubmit.organization_group_procurement , [name]: value }
      reducerFunc('dataSubmit',{ organization_group_procurement : group_procurement });
    } else {
      if(name==='organization_group_isactive'){
        reducerFunc('dataSubmit',{ [name]: value,organization_group_status: value });
      } else {
        reducerFunc('dataSubmit',{ [name]: value });
      }
    }
  }
  const handleBlurInput = (name, value,e) => {
    reducerFunc('dataSubmit',{ [name]: value });
  }

  const handleChangeCombo = async (name, value) => {
    switch (name) {
      case 'province_id':
        reducerFunc('dataSubmit',{ [name]: value });
        reducerFunc('data',{ city: { loading: true, list: [] } });
        try {
          const citydata = await oProvider.list(`regencies-dropdown?id=${value}`);
          reducerFunc('data',{ city: { loading: false, list: citydata.data }, district: [], village: [] });
        } catch (error) {
          reducerFunc('data',{ city: { loading: false, list: [] } });
          console.log(error?.message);
          return false;
        }
        break;
      case 'regency_id':
        reducerFunc('dataSubmit',{ [name]: value });
        reducerFunc('data',{ district: { loading: true, list: [] } });
        try {
          const districtdata = await oProvider.list(`districts-dropdown?id=${value}`);
          reducerFunc('data',{ district: { loading: false, list: districtdata.data } });
        } catch (error) {
          reducerFunc('data',{ district: { loading: false, list: [] } });
          console.log(error?.message);
          return false;
        }
        break;
      case 'district_id':
        reducerFunc('dataSubmit',{ [name]: value });
        reducerFunc('data',{ village: { loading: true, list: [] } });
        try {
          const villagedata = await oProvider.list(`village-dropdown?id=${value}`);
          reducerFunc('data',{ village: { loading: false, list: villagedata.data } });
        } catch (error) {
          reducerFunc('data',{ village: { loading: true, list: [] } });
          console.log(error?.message);
          return false;
        }
        break;
      default:
        break;
    }
  };

  const groupBy = (xs, key) => {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

  React.useEffect(() => {
    reducerFunc('loadingForm',true,'conventional');
    async function fetchData() {
      try {
        const [
          { data : bankChoices },
          { data : jenisProduk },
          { data : typeOrg },
        ] = await Promise.all([
          oProvider.list('bank-dropdown'),
          oProvider.list('content-media-list'),
          oProvider.list('organization-type-dropdown'),
        ]);
        if (!mountedRef.current) return null;
        reducerFunc('data',{ province: { loading: true, list: [] } });
        let response = await oProvider.list('province-dropdown');
        reducerFunc('data',{ province: { loading: false, list: response?.data } });
        reducerFunc('loadingForm',false,'conventional');
  
        if (props.type === 'edit') {
          reducerFunc('loadingForm',true,'conventional');
          reducerFunc('data',{
            province: { loading: true, list: [] },
            city: { loading: true, list: [] },
            district: { loading: true, list: [] },
            village: { loading: true, list: [] }
          });
          const datarow = await oProvider.list(`organization-group-get-one?id=${props.idRow}`);
          const provdata = await oProvider.list(`province-dropdown`);
          const regencydata = await oProvider.list(`regencies-dropdown?id=${datarow?.data?.province_id}`);
          const districtdata = await oProvider.list(`districts-dropdown?id=${datarow?.data?.regency_id}`);
          const villagedata = await oProvider.list(`village-dropdown?id=${datarow?.data?.district_id}`);
          
          reducerFunc('data',{
            province: { loading: false, list: provdata?.data },
            city: { loading: false, list: regencydata?.data },
            district: { loading: false, list: districtdata?.data },
            village: { loading: false, list: villagedata?.data }
          });
          reducerFunc('dataSubmit',datarow.data);
          reducerFunc('loadingForm',false,'conventional');
        } else {
          reducerFunc('dataSubmit',{organization_group_status:1});
        }
        const typeOrgImage = typeOrg.map(({text,image,value:id,cataegory_name:sebagai}) => ({text,src:`/assets/general/${image}`,id,sebagai}));
        const typeOrgGroup = groupBy(typeOrgImage,'sebagai');
        reducerFunc('typeOrgGroup',typeOrgGroup);
        reducerFunc('typeOrg',typeOrgImage,'conventional');
        reducerFunc('bankChoices',bankChoices,'conventional');
        reducerFunc('jenisProduk',jenisProduk.map(({content_media_name:text,content_media_logo:src,id}) => ({id,text,src:`/assets/general/${src}`}) ),'conventional');
      } catch (error) {
        reducerFunc('loadingForm',false,'conventional');
        console.log(error?.message);
        return false;
      }
    }
    fetchData();
    return () => { 
      mountedRef.current = false
    }
  }, []);

  React.useEffect(() => {
    if(dataReducer?.current === 2){
      if(props.type === 'edit'){
        
        if(props.data.contract_end_date) {
          props.data.contract_end_date = moment(props.data.contract_end_date)
        }

        if(props.data.contract_start_date) {
          props.data.contract_start_date = moment(props.data.contract_start_date)
        }

        const { organization_group_procurement = {} } = props.data;

        if(organization_group_procurement?.lelang) props.data.lelang = organization_group_procurement.lelang;
        if(organization_group_procurement?.rekanan) props.data.rekanan = organization_group_procurement.rekanan;
        if(organization_group_procurement?.penunjukan) props.data.penunjukan = organization_group_procurement.penunjukan;
        if(organization_group_procurement?.corporate) props.data.corporate =organization_group_procurement.corporate;
          

        form.setFieldsValue({...props.data});
        if(!props.data?.organization_group_status) form.setFieldsValue({organization_group_status:1});
      }else if(props.type === 'add') {
        form.setFieldsValue({organization_group_status:1});
      }
    }
  }, [dataReducer?.current]);

  const BoxCheck = () => {
    console.log(dataReducer?.typeOrgGroup);
      return Object.keys(dataReducer?.typeOrgGroup).map((item,idx) => {
        return (
          <React.Fragment key={idx}>
            <Divider orientation="left">{item}</Divider>
            <InputControl 
              type="radio-image" 
              items={dataReducer?.typeOrgGroup[item]} 
              onChange={handleImageboxChange} 
              defaultValue={((props.type === 'edit') && (dataReducer?.typeOrgSelect === '')) ? props.data.org_type_id : dataReducer?.typeOrgSelect} 
            />
          </React.Fragment>
        )
      });
  }
  const defaultProps = { form,dataSubmit : dataReducer?.dataSubmit };
  const buttonFormProps = {...defaultProps,current : dataReducer?.current ,steps,otherSubmit,onCancel,next,prev};
  const ProfileCompanyProps = {...defaultProps,
    schema,handleChangeFile,handleChangeLogo,
    handleChangeInput,handleBlurInput,
    bankChoices : dataReducer?.bankChoices,
    handleChangeCombo,data : dataReducer?.data,type:props.type
  };

  return (
    <React.Fragment>
      <Steps current={dataReducer?.current} onChange={handleStepChange}>
        {steps.map(item => (
          <Step key={item.title} title={item.title} />
        ))}
      </Steps>
      <br/>
      <br/>
      <LoadingSpin loading={dataReducer?.loadingForm}>
        <Form form={form} onFinish={onSubmit}>
          {(dataReducer?.current === 0) && (<BoxCheck items={dataReducer?.typeOrgGroup}/>)}
          {(dataReducer?.current === 1 && dataReducer?.jenisProduk.length > 0 ) && (
            <InputControl 
              type="checkbox-image" 
              items={dataReducer?.jenisProduk} 
              onChange={handleImageboxChange2} 
              defaultValue={((props.type === 'edit') && (dataReducer?.jenisProductSelect.length === 0 )) ? props.data.organization_group_content : dataReducer?.jenisProductSelect} 
            />
          )}

          {dataReducer?.current === 2 && (
            <ProfileCompany {...ProfileCompanyProps} />
          )}
          <ButtonForm {...buttonFormProps} />
        </Form>
      </LoadingSpin>
    </React.Fragment>
  );
}

export default Edit;