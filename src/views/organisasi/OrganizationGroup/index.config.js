import React from 'react';
import regexList from 'utils/regex';
import pathName from 'routes/pathName';
import randomString from 'utils/genString';
import {PlusOutlined} from '@ant-design/icons';
import { getOneMenu } from 'utils/getMenuAkses';
import { noImagePath } from "utils/initialEndpoint";
import { Tag,Dropdown,Button,Menu,Collapse } from 'antd';
import ActionTableDropdown from "components/ActionTableDropdown";
/* eslint-disable */

const { initial, holding } = pathName;

const productInstitusi = items => {
  return(<React.Fragment>
    <Collapse>
      <Collapse.Panel header="Klik untuk melihat penerbit" key="1">
        <ul>
          {items.map((item,idx) => <div key={idx}><li>{item.organization_name}</li></div>)}
        </ul>
      </Collapse.Panel>
    </Collapse>
  </React.Fragment>);
}

const actionMenu = (row, params) => {
  const [props,modalDelete,menuAksesContext,modalDetail] = params;
  const sunting = () => {
    props.history.push(holding.edit(`${row.id}`));
  };

  return (
    <Menu>
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={()=> modalDetail(row)} text="Pratinjau" />
      </Menu.Item>
      { getOneMenu(menuAksesContext,'Sunting') &&
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={sunting} text="Sunting" />
      </Menu.Item>
      }
      { getOneMenu(menuAksesContext,'Hapus') && 
      <Menu.Item>
        <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={() => modalDelete(row, props.history)} text="Hapus" />
      </Menu.Item>
      }
    </Menu>
  );
};

export default {
  list: {
    breadcrumb: [
      { text: 'Home', to: initial },
      { text: 'Holding', to: holding.list },
      { text: 'Daftar Group Institusi' },
    ],
    content: {
      title: 'Institusi',
      subtitle: 'Lakukan pilah untuk memudahkan pencarian',
      toolbars: [
        {
          text: '', type: 'primary', icon: <PlusOutlined/>,
          onClick: (e) => {
            e.history.push(holding.add);
          }
        },
      ]
    },
    config: (...props) => {
      return {
        columns: [
          {
            title: '',
            width: 60,
            render: row => {
              return {
                props: {
                  style: { verticalAlign: 'top' },
                },
                children: (
                  <span>
                  <div style={{ width: "4em" }}>
                    <img src={row.organization_group_logo || noImagePath} alt="" style={{ width: "100%" }} />
                  </div>
                </span>  
                )
              }
            },
          },{
            title: 'Nama Institusi',
            key : 'nama_institusi',
            verticalAlign: 'top',
            render: row => {
              return {
                props: {
                  style: { verticalAlign: 'top' },
                },
                children: (
                  <span>
                    <div>
                      <strong>{row.organization_group_name}</strong>
                    </div>
                    <div>
                      {row.organization_group_email}
                    </div>
                  </span>
                )
              }
            }
          },
          {
            title: 'Jenis',
            key : 'jenis_institusi',
            render: row => {
              return {
                props: {
                  style: { verticalAlign: 'top' },
                },
                children: (
                  <span>
                    <div>
                      {row.org_type_name}
                    </div>
                  </span>
                )
              }
            },
            width: 220
          },
          {
            title: 'Penerbit',
            key : 'produk_institusi',
            width: 400,
            render: row => {
              return {
                props: {
                  style: { verticalAlign: 'top' },
                },
                children: (
                  <span>
                    <div>
                      {productInstitusi(row.publishers)}
                    </div>
                  </span>
                )
              }
            }
          },
          {
            title: 'Status',
            width: 130,
            key: 'organization_group_isactive',
            dataIndex: 'organization_group_isactive',
            render: status => {
              let color = status === 1 ? 'green' : 'volcano';
              let nameStatus = status === 1 ? 'Aktif' : 'Tidak Aktif';
              return {
                props: {
                  style: { verticalAlign: 'top' },
                },
                children: (
                  <Tag color={color} key={randomString(5)}>
                    {nameStatus.toUpperCase()}
                  </Tag>
                )
              }
            },
          },
          {
            title: 'Aksi',
            key: 'operation',
            fixed: 'right',
            width: 80,
            render: (row) => {
              return {
                props: {
                  style: { verticalAlign: 'top' },
                },
                children: (
                  <span>
                    <Dropdown overlay={actionMenu(row, props)}>
                      <Button
                        type="link"
                        style={{ margin: 0 }}
                        onClick={e => e.preventDefault()}
                      >
                        <i className="icon icon-options-vertical" />
                      </Button>
                    </Dropdown>
                  </span>
                )
              }}
          },
        ]
      }
    },
    action : (params) => {
      let dropdown = [{ value : '', text:'Pilih Aksi'}];
      if(getOneMenu(params,'Hapus')) dropdown.push({ value : 'delete', text:'Hapus Perusahaan yang dipilih'});
      if(getOneMenu(params,'Sunting')) dropdown.push(
        { value : 'aktif', text:'Ubah Perusahaan menjadi Aktif'},
        { value : 'nonaktif', text:'Ubah Perusahaan menjadi Tidak Aktif'},
        { value : 'block', text:'Ubah Perusahaan menjadi diBlock'});

      return dropdown;
    }
  },
  add: {
    breadcrumb: [
      { text: 'Home', to: initial },
      { text: 'Daftar Institusi', to: holding.list },
      { text: 'New' },
    ],
    content: {
      title: 'Create Institusi',
    },
  },
  edit: {
    breadcrumb: [
      { text: 'Home', to: initial },
      { text: 'Institusi', to: holding.list },
      { text: 'Edit Form' },
    ],
    content: {
      title: 'Edit Institusi',
    },
  },
  schema: {
    organization_group_logo: {
      label: 'Logo',
      rules: [
        { required: true, message: 'Silahkan input logo institusi' },
      ]
    },
    organization_group_name: {
      label: 'Nama Institusi',
      rules: [
        { required: true, message: 'Silahkan input nama institusi' },
      ]
    },
    organization_group_phone: {
      label: 'Phone Institusi',
      rules: [
        { required: true, message: 'Silahkan input nomor telp institusi' },
        // (params) => ({
        //   validator(rule, value) {
        //     // console.log(rule,value,params);
        //     let errorMsg = 'Data input yang Anda masukkan harus berupa nomor telepon benar !';
        //     let regex = regexList('phone');
        //     if (value.match(regex)) {
        //       return Promise.resolve();
        //     } else {
        //       return Promise.reject(errorMsg);
        //     }
        //   },
        // }),
      ]
    },
    organization_group_email: {
      label: 'Email Institusi',
      rules: [
        { required: true, message: 'Silahkan input email institusi' },
      ]
    },
    organization_group_npwp: {
      label: 'NPWP Institusi',
      rules: [
        { required: true, message: 'Silahkan input nomor NPWP' },
        ({getFieldValue}) => ({
          validator(rule, value) {
            // console.log(rule,value,params);
            let errorMsg = 'Data input yang Anda masukkan harus berupa angka !';
            let regex = regexList('numeric');
            if (regex.test(value)) {
              return Promise.resolve();
            } else {
              return Promise.reject(errorMsg);
            }
          },
        }),
      ]
    },
    organization_group_website: {
      label: 'Website Institusi',
      rules : [
        {required : false, message : ''},
        (params) => ({
          validator(rule, value) {
            // console.log(rule,value,params);
            let errorMsg = 'Data input yang Anda masukkan belum benar !';
            let regex = regexList('url');
            try {
              if (regex.test(value)) {
                return Promise.resolve();
              } else {
                return Promise.reject(errorMsg);
              }
            } catch (error) {
              return Promise.resolve();
            }
          },
        }),
      ]
    },
    organization_group_fax: {
      label: 'Fax Institusi',
      rules : [
        {required : false, message : ''},
        // (params) => ({
        //   validator(rule, value) {
        //     // console.log(rule,value,params);
        //     let errorMsg = 'Data input yang Anda masukkan harus berupa nomor telepon benar !';
        //     let regex = regexList('phone');
        //     try {
        //       if (regex.test(value)) {
        //         return Promise.resolve();
        //       } else {
        //         return Promise.reject(errorMsg);
        //       }
        //     } catch (error) {
        //       return Promise.resolve();
        //     }
        //   },
        // }),
      ]
    },
    organization_group_isactive: {
      label: 'Status Institusi',
      rules: [
        { required: true, message: 'Silahkan pilih status institusi' },
      ]
    },
    organization_group_address: {
      label: 'Alamat Institusi',
      rules: [
        { required: true, message: 'Silahkan input alamat institusi' },
      ]
    },
    organization_group_about: {
      // label: 'Tentang Group Institusi',
    },
    org_type_id: {
      label: 'Jenis Institusi',
    },
    province_id: {
      label: 'Province',
    },
    regency_id: {
      label: 'Kota/Kab',
    },
    district_id: {
      label: 'Kecamatan',
    },
    village_id: {
      label: 'Kelurahan',
      rules: [
        { required: true, message: 'Silahkan pilih kelurahan' },
      ]
    },
    postalcode: {
      label: 'Postal Code'
    },
    organization_group_content: {
      label: 'Content Institusi',
    },
    organization_group_ppn: {
      label: 'Jenis Pajak',
    },
    organization_group_npwp_address: {
      label: 'Alamat NPWP',
      rules: [
        { required: true, message: 'Silahkan input alamat dari NPWP' },
      ]
    },
    bank_id: {
      label: 'Nama Bank',
    },
    bank_account: {
      label: 'Nomor Rekening',
      rules:[
        (params) => ({
          validator(rule, value) {
            // console.log(rule,value,params);
            let errorMsg = 'Data input yang Anda masukkan harus berupa angka !';
            let regex = regexList('numeric');
            if (value.match(regex) || !value) {
              return Promise.resolve();
            } else {
              return Promise.reject(errorMsg);
            }
          },
        }),
      ]
    },
    bank_account_name: {
      label: 'Nama Pemilik',
    },
  },
  detailSchema: {
    contract_number: {
      label: 'No. Perjanjian Kerjasama',
      rules: [
        { required: true, message: 'Silahkan input No. Perjanjian Kerjasama' },
      ]
    },
    contract_time: {
      label: 'Masa Kerjasama',
      rules: [
        { required: true, message: 'Silahkan input Masa Kerjasama' },
      ]
    },
    contract_start_date : {
      label: 'Tanggal mulai Kerjasama',
      rules: [
        { required: true, message: 'Silahkan pilih tanggal mulai Kerjasama' },
      ]
    },
    contract_end_date : {
      label: 'Tanggal Kerjasama berakhir',
      rules: [
        { required: true, message: 'Silahkan pilih tanggal berakhirnya Kerjasama' },
      ]
    },
    bank_id : {
      label: 'Nama Bank',
      rules: [
        { required: true, message: 'Silahkan input nama bank' },
      ]
    },
    bank_account : {
      label: 'No rekening',
      rules:[
        (params) => ({
          validator(rule, value) {
            // console.log(rule,value,params);
            let errorMsg = 'Data input yang Anda masukkan harus berupa angka !';
            let regex = regexList('numeric');
            if (value.match(regex) || !value) {
              return Promise.resolve();
            } else {
              return Promise.reject(errorMsg);
            }
          },

          required: true,
        }),
      ]
    },
    bank_account_name : {
      label: 'Nama pemilik rekening',
      rules: [
        { required: true, message: 'Silahkan input nama pemilik rekening' },
      ]
    },
    doc_profile_url : {
      label: 'Profile Perusahaan',
      rules: [
        { required: true, message: 'Silahkan upload file profile perusahaan' },
      ]
    },
    doc_siup_url : {
      label: 'SIUP',
      rules: [
        { required: true, message: 'Silahkan upload file SIUP' },
      ]
    },
    doc_npwp_url : {
      label: 'NPWP',
      rules: [
        { required: true, message: 'Silahkan upload file NPWP' },
      ]
    },
    doc_tdp_url : {
      label: 'TDP',
      rules: [
        { required: true, message: 'Silahkan upload file TDP' },
      ]
    },
    doc_ktp_url : {
      label: 'KTP Direktur',
      rules: [
        { required: true, message: 'Silahkan upload file KTP Direktur' },
      ]
    },
    doc_akta_url : {
      label: 'Akta Pendirian',
      rules: [
        { required: true, message: 'Silahkan upload file Akta Pendirian' },
      ]
    },
    doc_domisili_url : {
      label: 'Surat Keterangan Domisili',
      rules: [
        { required: true, message: 'Silahkan upload file surat keterangan domisili' },
      ]
    },
    doc_tax_url : {
      label: 'bayar pajak 3 Bulan terakhir',
      rules: [
        { required: true, message: 'Silahkan upload file bukti bayar pajak' },
      ]
    },
    doc_tax_expired : {
      label: 'Tentukan tanggal & unggah bukti ',
      rules: [
        { required: true, message: 'Silahkan upload file bukti bayar pajak' },
      ]
    },
    doc_listtax_url : {
      label: 'Surat Keterangan Daftar Pajak',
      rules: [
        { required: true, message: 'Silahkan upload file keterangan daftar pajak' },
      ]
    },
    doc_pkp_url : {
      label: 'Surat Pengukuhan Pengusaha Kena Pajak',
      rules: [
        { required: true, message: 'Silahkan upload file surat pengukuhan kena pajak' },
      ]
    },
    organization_file_rekening : {
      label: 'Rekening Koran / Halaman Pertama Buku Bank',
      rules: [
        { required: true, message: 'Silahkan upload file Rekening Koran / Halaman Pertama Buku Bank' },
      ]
    }
  },
  model: {
    company: '',
    penerbit: '',
    status: ''
  },
  data: {
    status: [
      { value: '', text: '-- Semua Status --' },
      { value: 1, text: 'Aktif' },
      { value: 0, text: 'Tidak Aktif' },
    ],
  }
};