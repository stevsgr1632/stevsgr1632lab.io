import React from 'react';
import FormControl from './Form';
import config from './index.config';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import { Breadcrumb, Content } from 'components';
import LoadingSpin from 'components/LoadingSpin';
/* eslint-disable */

const { holding } = pathName;

const App = (props) => {
  const { schema } = config;
  const { breadcrumb, content } = config.edit;
  const orgId = props.match.params.id;
  const [data,setData] = React.useState({});
  const [loading,setloading] = React.useState(false);

  const backToList = () => {
    props.history.push(holding.list);
  };

  const getOrganizationById = async (id) => {
    try {
      const { data } = await oProvider.list(`organization-group-get-one?id=${id}`);
      setData(data);
    } catch (error) {
      console.log(error?.message);
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields((errors, values) => {
      if (!errors) {
        console.log(values);
        backToList();
      }
    })
  };

  const handleOtherSubmit = async (e) => {
    if(Array.isArray(e.organization_group_content)) e.organization_group_content = e.organization_group_content.join();
    setloading(true);
    try {
      let response = await oProvider.update(`organization-group?id=${orgId}`, { ...e });
      Notification({
        response: {
          code : response.code, 
          message : response.message
        }, 
        type:'success', 
        placement:'bottomRight'
      });
      setloading(false);
      backToList();
    } catch (error) {
      console.log(error?.message);
      setloading(false);
      return false;
    }
  }

  React.useEffect(() => {
    getOrganizationById(orgId);
  }, []);

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <LoadingSpin loading={loading}>
          <FormControl
            onSubmit={handleSubmit}
            otherSubmit={handleOtherSubmit}
            onCancel={backToList}
            schema={schema}
            type='edit'
            data={data}
            idRow={orgId}
          />
        </LoadingSpin>
      </Content>
    </React.Fragment>
  );
}

export default App;