import React from 'react';
import {Row, Col, Modal} from 'antd';
import ProfileDetail from './ProfileDetail';
import InputText from 'components/ant/InputText';
import InputFile from 'components/ant/InputFile3';
import InputSelect from 'components/ant/InputSelect';
import InputTextArea from 'components/ant/InputTextArea';
/* eslint-disable */

const ProfileCompany = props => {
  const [previewVisible, setpreviewVisible] = React.useState(false);
  const {schema,handleChangeLogo,dataSubmit,handleChangeInput,handleBlurInput,handleChangeCombo,data} = props;
  const choicesPajak = [
    {
      text : 'Ditanggung Penerbit',
      value :1
    },
    {
      text : 'Ditanggung MOCO',
      value :2
    }
  ];
  const choicesStatus = [
    { value: 1, text: 'Aktif' },
    { value: 0, text: 'Tidak Aktif' },
  ];

  const handlePreview = () => setpreviewVisible(true);
  const handleCancel = () => setpreviewVisible(false);

  return(<>
  <Row gutter={[16, 24]}>
    <Col lg={6} md={24} sm={24} xs={24}>
      <InputFile name="organization_group_logo" accept=".png,.jpeg,.jpg" schema={schema} onChange={handleChangeLogo} imgUrl={(dataSubmit?.organization_group_logo) ? dataSubmit?.organization_group_logo : ''} handlePreview={handlePreview}/>
    </Col>
    <Col lg={18} md={24} sm={24} xs={24}>
      <Row gutter={16}>
        <Col span={12}>
          <InputText name="organization_group_name" onBlur={handleBlurInput} schema={schema} />
        </Col>
        <Col span={12}>
          <InputText name="organization_group_email" type="email" onBlur={handleBlurInput} schema={schema} />
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={12}>
          <InputText name="organization_group_website" type="url" onBlur={handleBlurInput} schema={schema} />
        </Col>
        <Col span={12}>
          <InputSelect name="organization_group_isactive" data={choicesStatus} onChange={handleChangeInput} schema={schema} />
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={12}>
          <InputText name="organization_group_phone" onBlur={handleBlurInput} schema={schema} />
        </Col>
        <Col span={12}>
          <InputText name="organization_group_fax" pattern="\(?(?:\+62|62|0)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}" onBlur={handleBlurInput} schema={schema} />
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={12}>
          <InputSelect name="province_id" schema={schema} loading={data.province.loading} data={data.province.list} onChange={handleChangeCombo} others={{showSearch:true}} />
        </Col>
        <Col span={12}>
          <InputSelect name="regency_id" schema={schema} loading={data.city.loading} data={data.city.list} onChange={handleChangeCombo} others={{showSearch:true}} />
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={12}>
          <InputSelect name="district_id" schema={schema} loading={data.district.loading} data={data.district.list} onChange={handleChangeCombo} others={{showSearch:true}} />
        </Col>
        <Col span={12}>
          <InputSelect name="village_id" schema={schema} loading={data.village.loading} data={data.village.list} onChange={handleChangeInput} others={{showSearch:true}} />
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={24}>
          <InputTextArea name="organization_group_address" onBlur={handleBlurInput} schema={schema} placeholder="Alamat Instansi" />
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={24}>
          <label>Tentang Instansi</label>
          <InputTextArea name="organization_group_about" onBlur={handleBlurInput} schema={schema} placeholder="Deskripsi Instansi" />
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={12}>
          <InputSelect name="organization_group_ppn" data={choicesPajak} onChange={handleChangeInput} schema={schema} others={{showSearch:true}} />
        </Col>
        <Col span={12}>
          <InputText name="organization_group_npwp" pattern="^\d+$" title="please enter number only" onBlur={handleBlurInput} schema={schema} />
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={24}>
          <InputTextArea name="organization_group_npwp_address" onBlur={handleBlurInput} schema={schema} placeholder="Alamat NPWP Organisasi Group" />
        </Col>
      </Row>
    </Col>
  </Row>
  {
    ( dataSubmit.org_type_id === "1132d15c-548a-11ea-80ba-aee15e33e3db" || dataSubmit.org_type_id === "0dfdad87-6da5-4e79-b6a6-b68f9ecd8895" ) && <ProfileDetail {...props}/>
  }
  <Modal
    visible={previewVisible}
    title={'Preview'}
    footer={null}
    centered
    onCancel={handleCancel}
    >
    <img alt="example" style={{ width: '100%' }} src={dataSubmit?.organization_group_logo} />
  </Modal>
  </>);
};

export default ProfileCompany;