import React from 'react';
import moment from 'moment';
import config from '../index.config';
import { Row , Col, Space } from 'antd';
import InputUpload from './InputUpload';
import InputText from 'components/ant/InputText';
import InputDate from 'components/ant/InputDate';
import InputSelect from 'components/ant/InputSelect';
import InputCheckbox from 'components/ant/InputCheckbox';
import InputNumber from 'components/ant/InputNumber';
/* eslint-disable */

const ProfileDetail = (props) => {
    const { handleChangeInput,handleChangeFile,dataSubmit,bankChoices,form} = props;

    const { detailSchema } = config;
    const styles = {
        col_1 : { span : 24 },
        col_2 : { xs: 24 , sm: 12, md: 12, lg:12 },
        col_3 : { xs: 12 , sm: 6, md: 6 , lg: 6 },
        gutter_1: [16,16],
        gutter_2: [16,4],
        gutter_3: [0,0]
    }

    const checkSharing = (am,publisher) => {
        const totalSharing = parseInt(am) + parseInt(publisher);
        if(totalSharing !== 100){
            const errMessage = 'Total sharing harus 100';

            form.setFields([
                { name: 'sharing_am',errors: [errMessage]},
                { name: 'sharing_publisher',errors: [errMessage]},
             ]);
        }else{
            form.setFields([
                { name: 'sharing_am',errors: false},
                { name: 'sharing_publisher',errors: false},
             ]);
        }
    }

    const handleChange = (name,value) => {
        if(handleChangeInput){
            handleChangeInput(name, value);
        }
    }

    const ChangeFile = (name, name2, file) => {
        if(file.status === 'done' && handleChangeFile){
            handleChangeFile(name, name2, file.response);
        }
    }

    React.useEffect(()=>{
        if(dataSubmit.sharing_am && dataSubmit.sharing_publisher){
            checkSharing(dataSubmit.sharing_am, dataSubmit.sharing_publisher);
        }
    },[dataSubmit.sharing_am,dataSubmit.sharing_publisher])

    React.useEffect(()=>{
        if(dataSubmit){
            console.log('dataSubmit',dataSubmit);
        }
    },[dataSubmit])
    

    return (
        <React.Fragment>
            <hr/>
            <Row gutter={[16,16]}>
                <Col xs={24} sm={24} md={24} lg={6}>
                    <div style={{width:'100%',background:'#C5C5C5',padding:'10px'}}>
                        <div><strong>Syarat unggahan berkas</strong></div>
                        <div>- Tipe berkas yang diunggah adalah PDF, ukuran maksimal adalah 5MB</div>
                        <div>- Kosongkan berkas apabila tidak akan merubah isinya</div>
                    </div>
                </Col>
                <Col xs={24} sm={24} md={24} lg={18}>
                    <Row gutter={styles.gutter_1}>
                        <Col {...styles.col_1}>
                            <div><strong>Detail Tambahan</strong></div>
                            <div><small>Lengkapi detail profile berikut</small></div>
                        </Col>
                    </Row>
                    <Row gutter={styles.gutter_2}>
                        <Col {...styles.col_2}>
                            <InputText name='contract_number' schema={detailSchema} placeholder = 'Masukan No Perjanjian' onChange = {handleChange}/>
                        </Col>
                        <Col {...styles.col_2}>
                            <InputText name='contract_time' schema={detailSchema} placeholder = 'Masukan Masa Kerjasama' onChange = {handleChange}/>
                        </Col>
                    </Row>
                    <Row gutter={styles.gutter_2}>
                        <Col {...styles.col_2}>
                            <InputDate 
                                name='contract_start_date' 
                                format={['YYYY-MM-DD', 'YY-MM-DD']} 
                                schema={detailSchema} 
                                placeholder = {`${moment(new Date()).format('YYYY-MM-DD')}`} 
                                onChange = {handleChange} 
                                style={{width:'100%'}}/>
                        </Col>
                        <Col {...styles.col_2}>
                            <InputDate 
                                name='contract_end_date' 
                                format={['YYYY-MM-DD', 'YY-MM-DD']}
                                schema={detailSchema} 
                                placeholder = {`${moment(new Date()).format('YYYY-MM-DD')}`}
                                onChange = {handleChange} 
                                style={{width:'100%'}}/>
                        </Col>
                    </Row>
                    <Row gutter={styles.gutter_2}>
                        <Col {...styles.col_2}>
                            <Space>
                                <InputNumber 
                                    name='sharing_am' 
                                    schema={detailSchema} 
                                    placeholder = 'Masukan jumlah profit' 
                                    onBlur = {handleChange}
                                    others={{min:0,max:100}}/>
                                <InputNumber 
                                    name='sharing_publisher' 
                                    schema={detailSchema} 
                                    placeholder = 'Masukan jumlah profit' 
                                    onBlur = {handleChange}
                                    others={{min:0,max:100}}/>
                            </Space>
                        </Col>
                        <Col {...styles.col_2}>
                            <InputUpload name='agreement_file' name2='agreement_file_name' allowedFileType='.pdf' uploadURL='upload-document' fileName={dataSubmit} schema={detailSchema} placeholder = 'No file selected' onChange = {ChangeFile}/>
                        </Col>
                    </Row>
                    <Row gutter={styles.gutter_1}>
                        <Col {...styles.col_1}>
                            <div><strong>Detail Bank</strong></div>
                            <div><small>Lengkapi detail profile berikut</small></div>
                        </Col>
                    </Row>
                    <Row gutter={styles.gutter_2}>
                        <Col {...styles.col_2}>
                            <InputSelect name="bank_id" data={bankChoices} placeholder = 'Masukan Nama Bank' onChange={handleChange} schema={detailSchema} others={{showSearch:true}} />
                        </Col>
                        <Col {...styles.col_2}>
                            <InputText name='bank_account' pattern="^\d+$" schema={detailSchema} placeholder = 'Masukan No Rekening' onChange = {handleChange}/>
                        </Col>
                    </Row>
                    <Row gutter={styles.gutter_2}>
                        <Col {...styles.col_2}>
                            <InputText name='bank_account_name' schema={detailSchema} placeholder = 'Masukan Nama Pemilik Rekening' onChange = {handleChange}/>
                        </Col>
                    </Row>
                    <Row gutter={styles.gutter_1}>
                        <Col {...styles.col_1}>
                            <div><strong>Unggah Berkas</strong></div>
                            <div><small>Lengkapi detail profile berikut</small></div>
                        </Col>
                    </Row>
                    <Row gutter={styles.gutter_2}>
                        <Col {...styles.col_2}>
                            <InputUpload name='doc_profile_url' name2='doc_profile_file_name' allowedFileType='.pdf' uploadURL='upload-document' fileName={dataSubmit} schema={detailSchema} placeholder = 'No file selected' onChange = {ChangeFile}/>
                        </Col>
                        <Col {...styles.col_2}>
                            <InputUpload name='doc_siup_url' name2='doc_siup_file_name' allowedFileType='.pdf' uploadURL='upload-document' fileName={dataSubmit} schema={detailSchema} placeholder = 'No file selected' onChange = {ChangeFile}/>
                        </Col>
                    </Row>
                    <Row gutter={styles.gutter_2}>
                        <Col {...styles.col_2}>
                            <InputUpload name='doc_npwp_url' name2='doc_npwp_file_name' allowedFileType='.pdf' uploadURL='upload-document' fileName={dataSubmit} schema={detailSchema} placeholder = 'No file selected' onChange = {ChangeFile}/>
                        </Col>
                        <Col {...styles.col_2}>
                            <InputUpload name='doc_tdp_url' name2='doc_tdp_file_name' allowedFileType='.pdf' uploadURL='upload-document' fileName={dataSubmit} schema={detailSchema} placeholder = 'No file selected' onChange = {ChangeFile}/>
                        </Col>
                    </Row>
                    <Row gutter={styles.gutter_2}>
                        <Col {...styles.col_2}>
                            <InputUpload name='doc_ktp_url' name2='doc_ktp_file_name' allowedFileType='.pdf' uploadURL='upload-document' fileName={dataSubmit} schema={detailSchema} placeholder = 'No file selected' onChange = {ChangeFile}/>
                        </Col>
                        <Col {...styles.col_2}>
                            <InputUpload name='doc_akta_url' name2='doc_akta_file_name' allowedFileType='.pdf' uploadURL='upload-document' fileName={dataSubmit} schema={detailSchema} placeholder = 'No file selected' onChange = {ChangeFile}/>
                        </Col>
                    </Row>
                    <Row gutter={styles.gutter_2}>
                        <Col {...styles.col_2}>
                            <InputUpload name='doc_domisili_url' name2='doc_domisili_file_name' allowedFileType='.pdf' uploadURL='upload-document' fileName={dataSubmit} schema={detailSchema} placeholder = 'No file selected' onChange = {ChangeFile}/>
                        </Col>
                        <Col {...styles.col_2}>
                            <Row>
                            <Col {...styles.col_2}>
                              <InputDate 
                                  name='doc_tax_expired' 
                                  format={['YYYY-MM-DD', 'YY-MM-DD']}
                                  schema={detailSchema} 
                                  placeholder = {`${moment(new Date()).format('YYYY-MM-DD')}`}
                                  onChange = {handleChange} 
                                  style={{width:'100%'}}/>
                              </Col>  
                            <Col {...styles.col_2}>
                              <InputUpload name='doc_tax_url' name2='doc_tax_file_name' allowedFileType='.pdf' uploadURL='upload-document' fileName={dataSubmit} schema={detailSchema} placeholder = 'No file selected' onChange = {ChangeFile}/>
                            </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row gutter={styles.gutter_2}>
                        <Col {...styles.col_2}>
                          Row
                            <InputUpload name='doc_listtax_url' name2='doc_listtax_file_name' allowedFileType='.pdf' uploadURL='upload-document' fileName={dataSubmit} schema={detailSchema} placeholder = 'No file selected' onChange = {ChangeFile}/>
                        </Col>
                        <Col {...styles.col_2}>
                            <InputUpload name='doc_pkp_url' allowedFileType='.pdf' fileName={dataSubmit} schema={detailSchema} placeholder = 'No file selected' onChange = {ChangeFile}/>
                        </Col>
                    </Row>
                    <Row gutter={styles.gutter_2}>
                        <Col {...styles.col_2}>
                            <InputUpload name='organization_file_rekening' name2='doc_pkp_file_name' allowedFileType='.pdf' uploadURL='upload-document' fileName={dataSubmit} schema={detailSchema} placeholder = 'No file selected' onChange = {ChangeFile}/>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <hr/>
            <Row gutter={[0,0]}>
                <Col xs={24} sm={24} md={24} lg={6}>
                    <div style={{width:'100%',height:'100%',background:'#C5C5C5',padding:'10px'}}>
                        <div><strong>Jenis pengadaan yang dapat diakses</strong></div>
                        <div>- Jenis pengadaan yang dapat diakses oleh perusahaan berikut</div>
                        <div>- Hanya dapat diubah oleh Superadmin atau tim Akuisisi</div>
                    </div>
                </Col>
                <Col xs={24} sm={24} md={24} lg={18} style={{borderBottom:'1px solid #E5E5E5',padding:'10px'}}>
                    <Row gutter={styles.gutter_3}>
                        <Col {...styles.col_1}>
                            <div><strong>Akses Pengadaan</strong></div>
                            <div><small>Perusahaan hanya dapat melakukan pengadaan jenis berikut</small></div>
                        </Col>
                    </Row>
                    <Row gutter={styles.gutter_3}>
                        <Col {...styles.col_3}>
                           <InputCheckbox name='lelang' onChange={handleChange}>
                               Lelang
                           </InputCheckbox>
                        </Col>
                        <Col {...styles.col_3}>
                            <InputCheckbox name='rekanan' onChange={handleChange}>
                                Rekanan
                            </InputCheckbox>
                        </Col>
                        <Col {...styles.col_3}>
                            <InputCheckbox name='penunjukan' onChange={handleChange}>
                                Pengadaan Langsung
                            </InputCheckbox>
                        </Col>
                        <Col {...styles.col_3}>
                            <InputCheckbox name='corporate' onChange={handleChange}>
                                Corporate
                            </InputCheckbox>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </React.Fragment>
    )
}

export default ProfileDetail
