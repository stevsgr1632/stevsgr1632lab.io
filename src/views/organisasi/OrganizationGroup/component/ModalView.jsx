import React from 'react';
import { Row, Col } from 'antd';
import Modal from 'antd/lib/modal/Modal';
import oProvider from 'providers/oprovider';
import LoadingSpin from 'components/LoadingSpin';
import { CardBody, CardHeader } from 'reactstrap';
import { noImagePath } from "utils/initialEndpoint";
import { CloseSquareFilled } from '@ant-design/icons';
/* eslint-disable */

const ModalView = (props) => {
  const { setModal, modal} = props;
  const [state, setState] = React.useState({});
  const [loading, setLoading]= React.useState(false);

  const fetchData = async (params) =>{
    setLoading(true);
    try {
      const response = await oProvider.list(`organization-group-view?id=${params.id}`);
      if (response) {
        setState(response.data);
        setLoading(false);
      }
    } catch (error) {
      console.log('error',error);
      setLoading(false);
    }
  }

  React.useEffect(()=>{
    if(modal.visible){
      fetchData(modal.data);
    }
  },[modal]);


  return (
    <Modal
      width={800}
      visible={modal.visible}
      onCancel={() => {
        setModal('modal',{ visible: false });
        setState({});
      }}
      footer={null}
      bodyStyle={{padding:'0px'}}
      centered={true}
      closeIcon={
      <div style={{width:'100%',height:'100%',justifyContent:'center'}}>
        <CloseSquareFilled style={{fontSize:'35px',color:'white'}}/>
      </div>}
    >
      <CardHeader style={{
        display:'flex',
        justifyContent:'space-between',
        backgroundColor:'#C92036',
        color:'white',
        border:'none'}}>
        <div>
          <h3 style={{color:'white'}}>Pratinjau Institusi</h3>
          <div>Detail Informasi Institusi</div>
        </div>
      </CardHeader>
      <CardBody>
        <LoadingSpin loading={loading}>
          <Row gutter={[16,16]}>
              <Col span={4} style={{display:'flex',alignItems:'center'}}>
                  <div style={{width:'100%'}}>
                      <img src={(state.organization_group_logo) ? state.organization_group_logo : noImagePath} 
                        onError={()=>setState(prev => ({...prev,organization_group_logo:noImagePath}))} 
                        alt='Logo'
                        width="100%"/>
                  </div>
              </Col>
              <Col span={20}>
                  <Row gutter={[8,8]}>
                      <Col span={4}>
                          Nama Institusi
                      </Col>
                      <Col span={20}>
                          : {(state.organization_group_name) ? state.organization_group_name : '-'}
                      </Col>
                  </Row>
                  <Row gutter={[8,8]}>
                      <Col span={4}>
                          Jenis Institusi
                      </Col>
                      <Col span={20}>
                          : {(state.org_type_name) ? state.org_type_name : '-'}
                      </Col>
                  </Row>
                  <Row gutter={[8,8]}>
                      <Col span={4}>
                          Email
                      </Col>
                      <Col span={20}>
                          : {(state.organization_group_email) ? state.organization_group_email : '-'}
                      </Col>
                  </Row>
                  <Row gutter={[8,8]}>
                      <Col span={4}>
                          Telepon
                      </Col>
                      <Col span={20}>
                          : {(state.organization_group_phone) ? state.organization_group_phone : '-'}
                      </Col>
                  </Row>
                  <Row gutter={[8,8]}>
                      <Col span={4}>
                          Alamat
                      </Col>
                      <Col span={20}>
                          : {(state.organization_group_address) ? state.organization_group_address : '-'}
                      </Col>
                  </Row>
                  <Row gutter={[8,8]}>
                      <Col span={4}>
                        Tentang Kami :
                      </Col>
                      <Col span={20}>
                        <div dangerouslySetInnerHTML={{__html:(state.organization_group_about) ? state.organization_group_about : '-'}}/>
                      </Col>
                  </Row>
              </Col>
          </Row>
        </LoadingSpin>
      </CardBody>
    </Modal>
  )
}

export default ModalView
