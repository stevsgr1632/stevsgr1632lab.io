import React from 'react';
import { Button } from 'antd';
import Message from 'common/message';
/* eslint-disable */

const ButtonForm = props => {
  const {form,current,steps,otherSubmit,dataSubmit,onCancel,next,prev} = props;

  const handleNext = (e) => {
    e.preventDefault();
    next();     
  }

  const handleClickSave = (e) => {
    e.preventDefault();
    form.validateFields().then(values => {
      console.log('data submit simpan',dataSubmit);
      otherSubmit(dataSubmit);
    })
    .catch(errorInfo => {
      console.log(errorInfo);
      errorInfo.errorFields.map(x => {
        Message({type:'error',text:`Kesalahan, ${x.name.toString()} : ${x.errors.toString()}`});
      });
      return false;
    });
  }
  return(<>
    <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
      {current > 0 && (
        <Button style={{ margin: 8 }} onClick={() => prev()}>
          Sebelumnya
        </Button>
      )}
      {current < steps.length - 1 && (
        <Button style={{ margin: 8 }} type="primary" onClick={handleNext}>
          Berikut
        </Button>
      )}
      {current === steps.length - 1 && (
        <Button style={{ margin: 8 }} type="primary" htmlType="button" onClick={handleClickSave}>Simpan</Button>
      )}
    </div>
    <hr />
    <Button danger type="primary" onClick={onCancel}>Cancel</Button>
  </>);
};

export default ButtonForm;