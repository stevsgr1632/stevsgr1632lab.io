import React from 'react';
import Message from 'common/message';
import interceptor from 'utils/interceptor';
import {UploadOutlined} from '@ant-design/icons';
import { Input, Upload, Button,Form,Progress } from 'antd';
import LocalStorageService from 'utils/localStorageService';
import { initURL,initialEndpoint } from 'utils/initialEndpoint';
/* eslint-disable */
const InputUpload = (props) => {
  const { 
    onChange, name, name2,
    schema = {}, limitFileList = -1, 
    fileMaxSize = 5, allowedFileType = '*', 
    uploadURL = 'upload-image', placeholder = '', fileName = {}, accessToken = false } = props;
  const { attributes } = LocalStorageService.state_auth.getAccessToken();
  const fieldSchema = schema[name] || {};
  const { rules } = fieldSchema;
  const label = props.text || fieldSchema.label || fieldSchema.text;
  const labelCol = props.labelCol || { span: 24};

  const [nameFile,setNameFile] = React.useState('');
  const [progress, setProgress] = React.useState(0);

  const styleButton = {
    backgroundColor:'#4dbd74',color:'#f0f3f5',borderColor:'#4dbd74'
  }

  const handleOnChange = ({ file, fileList, event }) => {
    if(name && onChange){
      onChange(name,name2,file);
    }

    if(file.status === 'done'){
      setNameFile(file.response.originalFilename);
    }
  }

  const uploadFileContent = async options => {
    const { onSuccess, onError, file, onProgress } = options;
    const headers = {  
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/vnd.api+json',
    }

    if(accessToken){
      headers['Authorization'] = `Bearer ${attributes?.accessToken}`;
    }
    let formData = new FormData();
    formData.append('filename', file);
    formData.append('keterangan', 'Upload document');
    
    const onUploadProgress= event => {
      const percent = Math.floor((event.loaded / event.total) * 100);
      setProgress(percent);
      onProgress({ percent: (event.loaded / event.total) * 100 });
    }

    let optionsAxios = {
      headers: headers,
      method: 'POST',
      url: `${initURL}/${initialEndpoint}/${uploadURL}`,
      data: formData,
      onUploadProgress : onUploadProgress
    };
    
    await interceptor.request(optionsAxios).then((result)=>{
      if(result){
        const { data } = result;
        onSuccess(data.data);
        setProgress(0);
      } 
    }).catch((err)=>{
      setProgress(0);
      Message({type:'error',text:`${err.response?.data?.message || 'Terjadi kesalahan saat upload'} `});
      onError({ err });
    })
  };

  const beforeUpload = (file) => {
    let isAccept = null
    if(allowedFileType !== "*"){
      let isAcceptFile = allowedFileType.split(',');
      let typeFile = '.' + file.name.split('.').pop();
      isAccept = isAcceptFile.includes(typeFile);
      if (!isAccept) {
        Message({type:'error',text:`You can only upload ${isAcceptFile.map(x => '.' + x.substring(1).toUpperCase()).join(', ')} file`});
      }
    }
    const isLt2M = file.size / 1024 / 1024 < fileMaxSize;
    if (!isLt2M) {
      Message({type:'error',text:`Image must smaller than ${fileMaxSize}MB`});
    }
    return isAccept && isLt2M;
  }

  React.useEffect(()=>{
    if(fileName[name2]){
      setNameFile(fileName[name2]);
    }
  },[fileName]);

  return (
  <React.Fragment>
      <Form.Item name={name} label={label} rules={rules} labelCol={labelCol}>
        <div>
          <div style={{display:'flex'}}>
              <Input type="text" readOnly value={nameFile} placeholder={placeholder}/>
              <Upload
                accept={allowedFileType}
                customRequest={uploadFileContent}
                onChange={handleOnChange}
                showUploadList={false}
                beforeUpload={beforeUpload}>
                <Button style={styleButton} loading={(progress <= 100 && progress > 0) ? true : false}>
                  {(progress <= 100 && progress > 0) ? 'Loading' : (<><UploadOutlined /> Pilih Berkas</>)}
                </Button>
              </Upload>
          </div>
          <div>
            {(progress <= 100 && progress > 0) ? <Progress percent={progress} status="active" /> : props.children}
          </div>
      </div>
    </Form.Item>
  </React.Fragment>
  )
}

export default InputUpload;
