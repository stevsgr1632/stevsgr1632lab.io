import React from 'react';
import FormControl from './Form';
import config from './index.config';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import { Breadcrumb, Content } from 'components';
import LoadingSpin from 'components/LoadingSpin';
/* eslint-disable */

const { holding } = pathName;
const resource = 'organization-group';

const App = (props) => {
  const { schema } = config;
  const { breadcrumb, content } = config.add;
  const [loading,setloading] = React.useState(false);

  const backToList = () => {
    props.history.push(holding.list);
  };

  const handleOtherSubmit = async (e) => {
    setloading(true);
    try {
      let response = await oProvider.insert(`${resource}`, { ...e });
      Notification({
        response: {
          code : response.code, 
          message : response.message
        }, 
        type:'success', 
        placement:'bottomRight'
      });
      setloading(false);
      backToList();
    } catch (error) {
      console.log(error?.message);
      setloading(false);
      return false;
    }
  };

  const handleSubmit = e => {
    console.log(e);
  };

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <LoadingSpin loading={loading}>
          <FormControl
            onSubmit={handleSubmit}
            onCancel={backToList}
            schema={schema}
            otherSubmit={handleOtherSubmit}
            type='add'
            data={[]}
          />
        </LoadingSpin>
      </Content>
    </React.Fragment>
  );
}

export default App;