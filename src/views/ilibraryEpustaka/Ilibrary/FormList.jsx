import React from 'react';
import { Modal } from 'antd';
import config from './index.config';
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
import {DeleteOutlined} from '@ant-design/icons';
/* eslint-disable */

const resource = 'ilibrary?limit=5';
const { confirm } = Modal;

const List = (props) => {
  const {model} = props;
  const [state, setState] = React.useState({});
  const [url, setUrl] = React.useState('');

  const modalDelete = (props) => {
  
    confirm({
      title: 'Do you want to delete these items?',
      icon: <DeleteOutlined/>,
      content: 'When clicked the OK button, this dialog will be closed',
      onOk() {
        return new Promise(async (resolve, reject) => {
          await oProvider.delete(`ilibrary?id=${props.id}`);
          refresh('',{status:'',search:''});
          // await oProvider.list(`${resource}?limit=5`);
          setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
        }).catch(() => console.log('Oops errors!'));
      },
      onCancel() {},
    });
  }

  const refresh = async (url,model) => {
    if(model.status) url += `&isactive=${model.status}`;
    if(model.search) url += `&ilibrary_name=${model.search}`;
    
    setState({...state, loading : true });
    try {
      const { meta, data } = await oProvider.list(`${resource}${url}`);
      setState({ loading : false, pagination: { pageSize:5,total: parseInt(meta.total) }, dataSource: data });
    } catch (error) {
      setState({...state, loading : false });
      console.log(error?.message);
      return false;
    }
  }

  const handleTableChange = (pagination, filters, sorter) => {
    let url = '';
    if (pagination) url += `&page=${pagination.current}`;
    if (sorter && sorter.field) {
      url += `&sortBy=${sorter.field}`;
      url += `&sortDir=${sorter.order === 'ascend' ? 'asc' : 'desc'}`;
    }
    setUrl(url);
  }

  React.useEffect(() => {
    refresh(url,model);
  }, [url,model]);

  return (
    <React.Fragment>
      <MocoTable
        {...config.list.config(props,modalDelete)}
        {...state}
        onChange={handleTableChange}
      />
    </React.Fragment>
  );
}

export default List;