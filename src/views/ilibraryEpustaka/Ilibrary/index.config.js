import React from 'react';
import pathName from 'routes/pathName';
import { Dropdown, Button, Menu } from 'antd';
import { noImagePath } from "utils/initialEndpoint";
/* eslint-disable */

const { initial, iLibEpus } = pathName;

const actionMenu = (row, params) => {
  const [props, modalDelete] = params;
  // const sunting = () => {
  //   props.history.push(`ilibrary/${row.id}`);
  // };

  return (
    <Menu>
      {/* <Menu.Item>
        <Button type="link" style={{ margin: 0 }} onClick={sunting}>
          Sunting
        </Button>
      </Menu.Item> */}
      <Menu.Item>
        <Button type="link" style={{ margin: 0 }} onClick={() => modalDelete(row, props.history)}>
          Hapus
        </Button>
      </Menu.Item>
    </Menu>
  );
};

export default {
  list: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'iLibrary', to: iLibEpus.ilibrary },
    ],
    content: {
      title: 'iLibrary',
      subtitle: 'Lakukan pilah untuk memudahkan pencarian',
      // toolbars: [
      //   {
      //     text: '', type: 'primary', icon: <PlusOutlined />,
      //     onClick: (e) => {
      //       e.history.push('/ilibrary/add');
      //     }
      //   },
      // ]
    },
    config: (...props) => {
      return {
        columns: [
          {
            title: 'Logo',
            width: 40,
            align: 'center',
            dataIndex: 'ilibrary_logo',
            render: logo => (<div style={{ width: "5em" }}>
                <img src={logo || noImagePath} alt="" style={{ width: "100%" }} />
              </div>
              // : <img src={noImagePath} alt={'no-data'} width={'100%'} />
              )
          },
          {
            title: 'Nama iLibrary',
            key: 'ilibrary_name',
            dataIndex: 'ilibrary_name',
            sorter: true
          },
          {
            title: 'ePustaka',
            width: 80,
            align: 'center',
            key: 'epustaka_qty',
            dataIndex: 'epustaka_qty'
            // render: item => {
            //   return (<center>{item}</center>)
            // }
          },
          {
            title: 'Anggota',
            width: 80,
            align: 'center',
            key: 'member_qty',
            dataIndex: 'member_qty'
            // render: item => {
            //   return (<center>{item}</center>)
            // }
          },
          {
            title: 'Koleksi',
            width: 80,
            align: 'center',
            key: 'catalog_qty',
            dataIndex: 'catalog_qty'
            // render: x => {
            //   return (<center>{x}</center>)
            // }
          },
          {
            title: 'Aksi',
            key: 'operation',
            fixed: 'right',
            width: 80,
            render: (row) => (
              <span>
                <Dropdown overlay={actionMenu(row, props)}>
                  <Button
                    type="link"
                    style={{ margin: 0 }}
                    onClick={e => e.preventDefault()}
                  >
                    <i className="icon icon-options-vertical" />
                  </Button>
                </Dropdown>
              </span>
            ),
          },
        ]
      }
    }
  },
  add: {
    breadcrumb: [
      { text: 'Beranda', to: '/' },
      { text: 'iLibrary Produk', to: '/ilibrary/index' },
      { text: 'New iLibrary', to: '/ilibrary/add' },
    ],
    content: {
      title: 'Buat iLibrary Baru',
    },
  },
  edit: {
    breadcrumb: [
      { text: 'Beranda', to: '/' },
      { text: 'iLibrary Produk', to: '/ilibrary/index' },
      { text: 'Edit iLibrary', to: '/ilibrary/:id' },
    ],
    content: {
      title: 'Edit Kategori',
    },
  },
  schema: {
    category_parent_id: {
      label: 'Kode ID Parent',
    },
    category_name: {
      label: 'Nama Kategori',
      rules: [
        { required: true, message: 'Silahkan input nama kategori' },
      ]
    },
    category_image: {
      label: 'Image Upload',
      rules: [
        { required: true, message: 'Silahkan input upload image' },
      ]
    },
    category_isactive: {
      label: 'Status Kategori',
      rules: [
        { required: true, message: 'Silahkan input status' },
      ]
    },
  },
  model: {
    status: '',
    search: ''
  },
  data: {
    status: [
      { value: '', text: 'Semua Status', disabled: true },
      { value: 1, text: 'Aktif' },
      { value: 0, text: 'Tidak Aktif' },
    ],
  }
};