import React from 'react';
import { Tag,Dropdown,Button,Menu } from 'antd';
import pathName from 'routes/pathName';
import randomString from 'utils/genString';
import { noImagePath } from "utils/initialEndpoint";
/* eslint-disable */

const { initial, iLibEpus } = pathName;

const actionMenu = (row, params) => {
  const [props,modalDelete] = params;
  const sunting = () => {
    props.history.push(`ePustaka/${row.id}`);
  };

  return (
    <Menu>
      {/* <Menu.Item>
        <Button type="link" style={{ margin: 0 }} onClick={sunting}>
          Sunting
        </Button>
      </Menu.Item> */}
      <Menu.Item>
        <Button type="link" style={{ margin: 0 }} onClick={() => modalDelete(row, props.history)}>
          Hapus
        </Button>
      </Menu.Item>
    </Menu>
  );
};

export default {
  list: {
    breadcrumb: [
      { text: 'Beranda', to: initial },
      { text: 'ePustaka', to: iLibEpus.epustaka },
    ],
    content: {
      title: 'ePustaka',
      subtitle: 'Lakukan pilah untuk memudahkan pencarian',
      // toolbars: [
      //   {
      //     text: 'Add', type: 'primary', icon: <PlusOutlined/>,
      //     onClick: (e) => {
      //       e.history.push('/epustaka/add');
      //     }
      //   },
      // ]
    },
    config: (...props) => {
      return {
        columns: [
          {
            title: 'Logo',
            width: 40,
            align: 'center',
            dataIndex: 'epustaka_logo',
            render: logo => (<div style={{ width: "5em" }}>
                <img src={logo || noImagePath} alt="" style={{ width: "100%" }} />
              </div>
              // : <img src={noImagePath} alt={'no-data'} width={'100%'} />
              )
          },
          {
            title: 'Nama ePustaka',
            dataIndex: 'epustaka_name',
            sorter: true
          },
          {
            title: 'Nama iLibrary',
            dataIndex: 'ilibrary_name',
          },
          {
            title: 'Status',
            width: 90,
            key: 'epustaka_isactive',
            dataIndex: 'epustaka_isactive',
            render: status => {
              let color = status === 1 ? 'green' : 'volcano';
              let nameStatus = status === 1 ? 'Aktif' : 'Tidak Aktif';
              return (
                <Tag color={color} key={randomString(5)}>
                  {nameStatus.toUpperCase()}
                </Tag>
              );
            },
          },
          {
            title: 'Aksi',
            key: 'operation',
            fixed: 'right',
            width: 80,
            render: (row) => (<>
            <span>
              <Dropdown overlay={actionMenu(row, props)}>
                <Button
                  type="link"
                  style={{ margin: 0 }}
                  onClick={e => e.preventDefault()}
                >
                  <i className="icon icon-options-vertical" />
                </Button>
              </Dropdown>
            </span>
            </>),
          },
        ]
      }
    }
  },
  add: {
    breadcrumb: [
      { text: 'Beranda', to: '/' },
      { text: 'ePustaka Produk', to: '/epustaka/index' },
      { text: 'New ePustaka', to: '/epustaka/add' },
    ],
    content: {
      title: 'Buat ePustaka Baru',
    },
  },
  edit: {
    breadcrumb: [
      { text: 'Beranda', to: '/' },
      { text: 'ePustaka Produk', to: '/epustaka/index' },
      { text: 'Edit ePustaka', to: '/epustaka/:id' },
    ],
    content: {
      title: 'Edit ePustaka',
    },
  },
  schema: {
    category_parent_id: {
      label: 'Kode ID Parent',
    },
    category_name: {
      label: 'Nama Kategori',
      rules: [
        { required: true, message: 'Silahkan input nama kategori' },
      ]
    },
    category_image: {
      label: 'Image Upload',
      rules: [
        { required: true, message: 'Silahkan input upload image' },
      ]
    },
    category_isactive: {
      label: 'Status Kategori',
      rules: [
        { required: true, message: 'Silahkan input status' },
      ]
    },
  },
  model: {
    status: '',
    search: ''
  },
  data: {
    status: [
      { value: '', text: 'Semua Status', disabled: true },
      { value: 1, text: 'Aktif' },
      { value: 0, text: 'Tidak Aktif' },
    ],
  }
};