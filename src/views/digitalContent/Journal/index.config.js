import pathName from '../../../routes/pathName';

const {base:baseUrl,digital:digitalUrl} = pathName;

export default {
    scope: {
        title: 'Journal Elektronik',
        breadcrumbs: [
            { text: 'Beranda', to: baseUrl },
            { text: 'Kontent Digital', to: digitalUrl.listAll },
            { text: 'Journal Elektronik' },
        ]
    },
    model: {
        first_name: '',
        perusahaan: 'microsoft',
        penerbit: ''
    },
    schema: {
        first_name: {
            label: 'Nama Depan',
            rules: [
                { required: true, message: 'Silahkan input nama depan' },
                { min: 3, message: 'Minimum 3 huruf' },
                { max: 30, message: 'Maximal 30 huruf' },
            ]
        },
        last_name: {
            label: 'Nama Belakang',
            rules: [
                { required: true, message: 'Silahkan input nama belakang' },
            ]
        },
        email: {
            label: 'Email',
            rules: [
                { required: true, message: 'Silahkan input email' },
            ]
        },
        phone_no: {
            label: 'No HP',
            rules: [
                { required: true, message: 'Silahkan No HP' },
            ]
        },
        perusahaan: {
            label: 'Perusahaan',
            rules: [
                { required: true, message: 'Silahkan isi perusahaan Anda' },
            ]
        },
        penerbit: {
            label: 'Penerbit',
            rules: [
                { required: true, message: 'Silahkan isi penerbit' },
            ]
        },
        amount_gross: {
            label: 'Amount Gross',
            rules: [
                { required: true, message: 'Silahkan diisi Amount' },
                { type: 'number', message: 'Harus diisi angka' },
            ]
        },
        info: {
            label: 'Info',
            rules: [
                { required: true, message: 'Silahkan informasi' },
            ]
        },
    },
    data: {
        perusahaan: [
            { value: '', text: 'Semua Perusahaan', disabled: true },
            { value: 'apple', text: 'Apple' },
            { value: 'microsoft', text: 'Microsoft' },
            { value: 'gramed', text: 'Gramedia' },
        ],
        penerbit: [
            { value: '', text: '-- Pilih Penerbit --', disabled: true },
            { value: 'gramed', text: 'Gramedia' },
            { value: 'apple', text: 'Apple' },
            { value: 'microsoft', text: 'Microsoft' },
        ],
        kategori: [
            { value: '', text: 'Semua Kategori', disabled: true },
            { value: 'agama', text: 'Agama' },
            { value: 'sosial', text: 'Sosial' },
            { value: 'bahasa', text: 'Bahasa' },
        ],
        status: [
            { value: '', text: 'Semua Status', disabled: true },
            { value: 'request', text: 'Request' },
            { value: 'verified', text: 'Verified' },
            { value: 'block', text: 'Terblokira' },
        ],
        format: [
            { value: '', text: 'Semua Format', disabled: true },
            { value: 'pdf', text: 'PDF' },
            { value: 'audio', text: 'Audio' },
            { value: 'video', text: 'Video' },
        ],
        tgl_upload: [
            { value: '', text: 'Semua', disabled: true },
            { value: 'hari', text: 'Hari ini' },
            { value: 'bulan', text: 'Bulan ini' },
        ],
    }
};