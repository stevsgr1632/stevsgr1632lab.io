import React, { useState, useEffect } from 'react';
import { Col, Row } from 'reactstrap';
import { Button, Form } from 'antd';
import { Breadcrumb, Content } from '../../../components';
import InputText from '../../../components/ant/InputText';
import InputSelect from '../../../components/ant/InputSelect';
import InputNumber from '../../../components/ant/InputNumber';
import InputTextArea from '../../../components/ant/InputTextArea';
import config from './index.config';
/* eslint-disable */

const App = (props) => {
    const { scope, schema, data } = config;
    const [model, setModel] = useState({});
    const [binding, setBinding] = useState({});

    const handleChange = (name, value) => {
        setModel({ ...model, [name]: value });
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        props.form.validateFields((errors, values) => {
            setBinding({ errors, values });
        })
    }

    schema.form = props.form;

    useEffect(() => {
        console.log('-- useEffect called --')
        setModel(config.model);
    }, [])

    return (
        <React.Fragment>
            <Breadcrumb items={scope.breadcrumbs} />
            <Content {...scope}>
                <Form onSubmit={handleSubmit}>
                    <Row>
                        <Col lg="6" xl="4">
                            <InputText name="first_name" schema={schema} onChange={handleChange} />
                        </Col>
                        <Col lg="6" xl="4">
                            <InputText name="last_name" schema={schema} onChange={handleChange} />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="12" xl="8">
                            <InputText disabled text="Full Name" value={`${model.first_name || ''} ${model.last_name || ''}`} />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="6" xl="4">
                            <InputText name="phone_no" text="Nomor HP" onChange={handleChange} />
                        </Col>
                        <Col lg="6" xl="4">
                            <InputText name="email" schema={schema} onChange={handleChange} />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="12" xl="8">
                            <InputSelect name="perusahaan" text="Perusahaan" onChange={handleChange} data={data.perusahaan} value={model.perusahaan} />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="12" xl="8">
                            <InputSelect name="penerbit" schema={schema} onChange={handleChange} data={data.penerbit} />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="6" xl="4">
                            <InputNumber text="Amount Nett" name="amount_nett" onChange={handleChange} />
                        </Col>
                        <Col lg="6" xl="4">
                            <InputNumber name="amount_gross" schema={schema} onChange={handleChange} formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')} parser={value => value.replace(/\$\s?|(,*)/g, '')} />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="12" xl="8">
                            <InputTextArea name="address" text="Alamat" rows="8" onChange={handleChange} />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="12" xl="8">
                            <InputTextArea name="info" schema={schema} rows="8" onChange={handleChange} />
                        </Col>
                    </Row>
                    <Button htmlType="submit">Submit</Button>
                </Form>
                <hr />
                <Row>
                    <Col lg="6">
                        <h3>On Change</h3>
                        <pre>
                            {JSON.stringify(model, null, 4)}
                        </pre>
                    </Col>
                    <Col lg="6">
                        <h3>On Submit</h3>
                        <pre>
                            {JSON.stringify(binding, null, 4)}
                        </pre>
                    </Col>
                </Row>
            </Content>
        </React.Fragment >
    );
};

export default App;
