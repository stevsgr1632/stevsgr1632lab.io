import React from 'react';
import moment from 'moment';
import { useHistory } from 'react-router-dom';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
import { Breadcrumb, Content } from 'components';
import FormControl from "./form";
import config from './index.config.js';
/* eslint-disable */

const { digital: digitalUrl } = pathName;

const App = props => {
  const catalogId = props.match.params.id;
  let history = useHistory();
  const { schema = {}, edit = {} } = config;
  const [data,setData] = React.useState({});

  const backToList = () => {
    history.push(digitalUrl.listEbook);
  };

  const getDataCatalog = async id => {
    try {
      const { data } = await oProvider.list(`catalog-get-one?id=${id}`);
      return setData(data);
    } catch (error) {
      console.log(error?.message);
      return false;      
    }
  };

  const openNotification = resp => {
    Notification({ response : resp });
  };

  const handleSubmit = async (e, datasubmit) => {
    let copyData = { ...datasubmit };
    if(datasubmit?.catalog_file_edit){
      copyData.catalog_file = datasubmit?.catalog_file_edit?.filePathName;
      copyData.catalog_file_size = datasubmit?.catalog_file_edit?.size;
    }

    if(datasubmit?.catalog_cover_edit){
      copyData.catalog_cover = datasubmit?.catalog_cover_edit?.file_url_download;
      copyData.catalog_cover_original = datasubmit?.catalog_cover_edit?.file_url_download;
    }
    
    copyData.catalog_volume = (copyData.catalog_volume) ? copyData.catalog_volume : '';
    copyData.catalog_edition = (copyData.catalog_edition) ? copyData.catalog_edition : '';
    copyData.catalog_target_market = datasubmit.catalog_target_market?.join();
    copyData.sub_category = datasubmit.sub_category?.join();
    copyData.catalog_publish_date = datasubmit.catalog_publish_date?.substr(0, 4);
    copyData.catalog_epublish_date = datasubmit.catalog_epublish_date?.substr(0, 4);
    copyData.catalog_effective_date = datasubmit.catalog_effective_date ? datasubmit.catalog_effective_date : moment(new Date()).format('YYYY-MM-DD');
    try {
      let response = await oProvider.update(`catalog?id=${catalogId}`, { ...copyData });
      if(response) {
        openNotification(response);
        backToList();
      }
    } catch (error) {
      console.log('error.response',error.response);
      if(error.response){
        const { data, status } = error.response;
        let errorMessage = '';
        if(data.error){
          if(Array.isArray(data.error)){
            data.error.map( x => {
              errorMessage = `${x.dataPath} : ${x.message}` 
              const errorResponse = {
                code : status,
                message : errorMessage
              }
              Notification({response:errorResponse, type : 'error'});
            });
          }else{
            errorMessage = data.error; 
              const errorResponse = {
                code : status,
                message : errorMessage
              }
              Notification({response:errorResponse, type : 'error'});
          }
        } else {
          errorMessage = (data.message) ? data.message : data;
            const errorResponse = {
              code : status,
              message : errorMessage
            }
            Notification({response:errorResponse, type : 'error'});
        }        
      }
      return false;
    }
  };

  const handleOtherSubmit = (e) => {
    console.log(e);
  };

  React.useEffect(() => {
    getDataCatalog(catalogId);
  },[]);

  return (<>
    <Breadcrumb items={edit.breadcrumb} />
    <Content {...edit.content}>
      <FormControl
        onSubmit={handleSubmit}
        onCancel={backToList}
        schema={schema}
        otherSubmit={handleOtherSubmit}
        type="edit"
        data={data}
      />
    </Content>
  </>);
};

export default App;