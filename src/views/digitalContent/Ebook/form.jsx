import React, { useEffect } from 'react';
import moment from 'moment';
import { Form, Steps } from 'antd';
import config from './index.config';
import Harga from './component/Harga';
import Berkas from './component/Berkas';
import Metadata from './component/Metadata';
import Validate from './component/Validate';
import storeRedux from 'redux/store';
import MessageMod from 'common/message';
import useCustomReducer from "common/useCustomReducer";
import oProvider from 'providers/oprovider';
import DeskripsiSinopsis from './component/Sinopsis';
import getMenuAkses, { getOneMenu } from 'utils/getMenuAkses';
import LoadingSpin from 'components/LoadingSpin';
import ButtonActionStep from "./component/ButtonActionStep";
/* eslint-disable */
export const FormContext = React.createContext();

const initialState = {
  current: 0,
  reff: { step: 0 },
  loading : false,
  dataSubmit : {},
  initialData : {},
  stateHarga : {radioValue:'2',disabled:false},
  inputFields : [],
  uploading : {
    catalog_file_name : false,
    catalog_cover_name: false
  }
}

export default (props) => {
  const { Step } = Steps;
  const { schema } = config;
  const { onSubmit, otherSubmit, type, data: dataGetCatalog } = props;
  const [stateReduc,reducFunc] = useCustomReducer(initialState);
  const [form] = Form.useForm();

  const steps = [
    { title: 'Berkas', },
    { title: 'Metadata', },
    { title: 'Deskripsi/Sinopsis', },
    { title: 'Harga', },
  ];
  const prev = () => {
    reducFunc('current',stateReduc.current - 1,'conventional');
  }
  const next = () => {
    // setCurrent(current + 1);
    form.validateFields()
    .then(values => {
      if(stateReduc.uploading.catalog_file_name || stateReduc.uploading.catalog_cover_name){
        MessageMod({type : 'warning', text : `Harap tunggu proses upload selesai !`});
      } else {
        reducFunc('current',stateReduc.current + 1,'conventional');
      }
      
      // console.log(values);
    })
    .catch(errorInfo => {
      errorInfo.errorFields.forEach(x => {
        MessageMod({type : 'error', text : `Kesalahan, ${x.name.toString()} : ${x.errors.toString()}`});
      });
      return false;
    });
  };

  const validateForm = () => {
    return form.validateFields()
        .then( values => {
          if(stateReduc.uploading.catalog_file_name || stateReduc.uploading.catalog_cover_name){
            MessageMod({type : 'warning', text : `Harap tunggu proses upload selesai !`});
            return false;
          } else {
            return true;
          }
        }).catch( error => {
          error.errorFields.forEach(x => {
            MessageMod({type : 'error', text : `Kesalahan, ${x.name.toString()} : ${x.errors.toString()}`});
          });
          return false;
        })
  }

  const checkNavigate = async (now, next) => {
    if(now > -1 && now <= 5 ){
      for (let idx = stateReduc.current ; idx < next; idx++) {
        const isValid = await validateForm();
        if(isValid){
          reducFunc('current',idx + 1,'conventional');
        }
      }
    }
    if(next < now ){
      reducFunc('current',next,'conventional');
    }
  }

  const handleStepChange = async currentNext => {
    if(currentNext > 0 && currentNext < 6){
      checkNavigate(stateReduc.current,currentNext);
    } else {
      reducFunc('current',currentNext,'conventional');
    }
  }

  const handleBerkas = (name, data) => {
    reducFunc('dataSubmit',{[name]: data});
  };

  const checkISBN = (name,subject) => {
    // [979-234-0923-94-9,ISBN 978-979-15000-0-5,ISBN 978-979-3000-00-8,ISBN 978-979-400-000-7,ISBN 978-979-20-0000-9,ISBN 978-602-02-0000-2,ISBN 978-623-xx-0000-x]
    const regexISBN = /^(ISBN[-]*(1[03])*[ ]*(: ){0,1})*(([0-9Xx][- ]*){13}|([0-9Xx][- ]*){10})$/;
    // const regexISBN13 = /^(?:ISBN(?:-13)?:?\ *(97(?:8|9)([ -]?)(?=\d{1,5}\2?\d{1,7}\2?\d{1,6}\2?\d)(?:\d\2*){9}\d))$/;
    // const regexISBN10 = /^(?:ISBN(?:-10)?:?\ *((?=\d{1,5}([ -]?)\d{1,7}\2?\d{1,6}\2?\d)(?:\d\2*){9}[\dX]))$/;
    if (regexISBN.test(subject)) {
      reducFunc('dataSubmit',{[name]: subject});
    } else {
      MessageMod({type : 'error', text : 'Masukkan sesuai format ISBN-10 dan ISBN-13'}); // No valid ISBN found

    }
  }

  const handleChangeInput = (name, value, e) => {
    // console.log(name,typeof value);
    if (name === 'organization_id') {
      let findData = stateReduc.initialData?.penerbit.find(x => x.value === value)?.city || '-';
      let updateData = { organization_id: value, catalog_publish_city: findData };
      reducFunc('dataSubmit',{...updateData});
      form.setFieldsValue({ catalog_publish_city: findData });
    }else if(name === 'catalog_isbn'){
      console.log(value);
    }else { 
      if(name === 'catalog_pages'){
        value = parseInt(value);
      }
      reducFunc('dataSubmit',{[name]: value});
    } 
  }

  const handleChangeISBN = (name, value, e) =>{
    if (name === 'catalog_isbn') checkISBN(name,value);
  };

  const handleChangeCheckbox = (name,value,e) => {
    // console.log(name,value) ==> val_catalog_cover,false
    let dataQC = stateReduc.dataSubmit?.catalog_qc.map(el => {
        if(el.catalog_checklist_code === name){
          return Object.assign({}, el, {qc_status:value ? 1 : 0})
        }
        return el;
    });
    reducFunc('dataSubmit',{catalog_qc: dataQC});
  };


  useEffect(() => {
    if(type === 'edit'){ 
      let isCheckBox = {};
      // let isCheckBoxMap = dataGetCatalog?.catalog_qc?.map(x => {
      //   let nameCheckbox = x.catalog_checklist_code;
      //   let value        = x.qc_status;
      //   isCheckBox = {...isCheckBox,[nameCheckbox] : value}
      // });
      form.setFieldsValue({...isCheckBox});
      reducFunc('dataSubmit',{...dataGetCatalog});
      if (dataGetCatalog?.catalog_isfree === 1) {
        reducFunc('stateHarga',{radioValue:'1',disabled:true});
      } else if (dataGetCatalog?.catalog_isfree === 2) {
        reducFunc('stateHarga',{radioValue:'2',disabled:false});
      }
    }
  }, [dataGetCatalog]);

  useEffect(() => {
    if (stateReduc.reff.step >= 3) {
      reducFunc('dataSubmit',{...stateReduc.dataSubmit});
    }
    if([1,2,3,4].includes(stateReduc.current)){
      if(type === 'edit'){
        form.setFieldsValue({...stateReduc.dataSubmit,
          catalog_epublish_date:(stateReduc.dataSubmit.catalog_epublish_date) ? moment(stateReduc.dataSubmit.catalog_epublish_date) : '',
          catalog_publish_date:(stateReduc.dataSubmit.catalog_publish_date) ? moment(stateReduc.dataSubmit.catalog_publish_date) : ''
        });
      }
    }
    reducFunc('dataSubmit',{ step: stateReduc.current });
  }, [stateReduc.current]);

  useEffect(() => {
    async function fetchData() {
      reducFunc('loading',true,'conventional');
      try {
        const [
          { data: kategori },
          { data: penerbit },
          { data: targetMarket },
          { data: edulevel },
          { data: btkKarya },
          { data: bahasa },
          { data: volume },
          { data: edition },
          { data: asalProduk },
          { data: status }
        ] = await Promise.all([
          oProvider.list('categories-dropdown'),
          oProvider.list('organization-dropdown'),
          oProvider.list('target-market-dropdown'),
          oProvider.list('edulevel-dropdown'),
          oProvider.list('generic-lookup?lookup_name=CATALOG_RESULT'),
          oProvider.list('generic-lookup?lookup_name=LANGUAGE'),
          oProvider.list('generic-lookup?lookup_name=VOLUME'),
          oProvider.list('generic-lookup?lookup_name=BOOK-EDITION'),
          oProvider.list('generic-lookup?lookup_name=SOURCE-OF-PRODUCT'),
          oProvider.list('status-dropdown?process_name=CATALOG')
        ]);

        volume.unshift({ value:'', text:'-- Pilih Jilid --' });
        bahasa.unshift({ value:'', text:'-- Pilih Bahasa --' });
        edition.unshift({ value:'', text:'-- Pilih Edisi --' });
        kategori.unshift({ value:'', text :'-- Pilih Kategori --' });
        penerbit.unshift({ value:'', text :'-- Pilih Penerbit --' });
        btkKarya.unshift({ value:'', text :'-- Pilih Bentuk Karya --' });
        asalProduk.unshift({ value:'', text :'-- Pilih Asal Produk --' });
        edulevel.unshift({ value:'', text :'-- Pilih Jenjang Pendidikan --' });
        
        let catalogMenuAkses = [];
        if(storeRedux.getState().menuAccess){
          catalogMenuAkses = getMenuAkses('/digital/ebook');
        }
        reducFunc('initialData',{ kategori, btkKarya, bahasa, volume, edition, edulevel, penerbit, targetMarket, asalProduk,menuAkses:catalogMenuAkses,status });
        reducFunc('loading',false,'conventional');
      } catch (error) {
        console.log(error?.message);
        reducFunc('loading',false,'conventional');
      }
    }
    fetchData();
  }, []);

  const commonResource = {type, dataGetCatalog, form, schema, dataSubmit:stateReduc.dataSubmit,handleChangeCheckbox,reducFunc};
  const buttonNavDataForm = {  prev, next, steps, current:stateReduc.current, otherSubmit, onSubmit, ...commonResource };
  const berkasResource = { handleBerkas, ...commonResource };
  const metaDataResource = { handleChangeInput, handleChangeISBN, ...commonResource };
  const deskripsiSinopsis = { ...commonResource };
  const hargaResource = { ...commonResource,stateHarga:stateReduc.stateHarga,inputFields:stateReduc.inputFields };
  const validateResource = { ...commonResource};

  return (
  <LoadingSpin loading={stateReduc.loading} >
    <FormContext.Provider value={stateReduc.initialData}>
      <Steps current={stateReduc.current} onChange={handleStepChange}>
        {steps.map(x => <Step key={x.title} title={x.title} />)}
        {type === 'edit' && getOneMenu(stateReduc.initialData.menuAkses,'validasi') && <Step title="Validate" />}
      </Steps>
      <Form form={form} onFinish={onSubmit}>
        <br />
        {stateReduc.current === 0 && <Berkas {...berkasResource} />}
        {stateReduc.current === 1 && <Metadata {...metaDataResource} />}
        {stateReduc.current === 2 && <DeskripsiSinopsis {...deskripsiSinopsis} />}
        {stateReduc.current === 3 && <Harga {...hargaResource} />}
        {(type === 'edit' && stateReduc.current === 4 && getOneMenu(stateReduc.initialData.menuAkses,'validasi')) && <Validate {...validateResource} />}
        <div>
        <ButtonActionStep {...buttonNavDataForm} />
        </div>
      </Form>
    </FormContext.Provider>
  </LoadingSpin>
  );
};