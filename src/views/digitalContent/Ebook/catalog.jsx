import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import queryString from "query-string";
import { useHistory } from 'react-router-dom';
import pathName from 'routes/pathName';
import oProvider from 'providers/webhook';
import { getHeaders } from "utils/request";
import randomString from 'utils/genString';
import Notification from 'common/notification';
import useCustomReducer from "common/useCustomReducer";
import { Content, MocoTable } from 'components';
import { initialEndpoint } from 'utils/initialEndpoint';
import MessageInfo, { MessageContent } from 'common/message';
import ModalForm from './modal';
import config from './index.config';
import { MenuAksesContext,RoleAksesContext } from './index';
import HeaderCatalog1 from "./component/HeaderCatalog1";
import HeaderCatalog2 from "./component/HeaderCatalog2";
import ActionMove from './component/ActionMove';
import ModalDelete from "./component/ModalDelete";
/* eslint-disable */

const apiUrl = process.env.REACT_APP_BASE_URL;

const initialState = {
  modal: { visible: false },
  modalAksi: { visible: false},
  selectedRowKeys: [],
  statusCatalog: [],
  selectAksi:'',
  modalTitle: '-',
  limit: 10,
  state:{
    loading : false,
    pagination: {
      defaultCurrent: 1,
      pageSize: 10,
      total: 0,
      showLessItems: true,
      showSizeChanger: true, 
    },
    dataSource: []
  }

}

const App = ({ filter }) => {
  let history = useHistory();
  const menuAksesContext = React.useContext(MenuAksesContext);
  const roleAksesContext = React.useContext(RoleAksesContext);
  const [stateReduc,reducerFunc] = useCustomReducer(initialState);
  const [, setIsLoaded] = useState(false);

  // const [modal, setModal] = useState({ visible: false });
  // const [modalAksi, setModalAksi] = useState({ visible: false });
  // const [selectedRowKeys, setselectedRowKeys] = React.useState([]);
  // const [statusCatalog, setstatusCatalog] = React.useState([]);
  // const [selectAksi, setselectAksi] = React.useState('');
  // const [modalTitle, setModalTitle] = React.useState('-');
  // const [limit, setLimit] = useState(10);
  // const [state, setState] = useState({
  //   loading : false,
  //   pagination: {
  //     defaultCurrent: 1,
  //     pageSize: 10,
  //     total: 0,
  //     showLessItems: true,
  //     showSizeChanger: true, 
  //   },
  //   dataSource: []
  // });

  // const usePrevious = (value) => {
  //   const ref = useRef();
  //   useEffect(() => {
  //     ref.current = value;
  //   },[value]);
  //   return ref.current;
  // }

  const modalDelete = async props => ModalDelete(props,refreshCatalog);

  const onShowSizeChange = (current, pageSize) => {
    reducerFunc('limit',pageSize,'conventional');
  }

  const refreshCatalog = async () => {
    reducerFunc('state',{loading:true});

    let url = `&page=${stateReduc.state.pagination.current ?? stateReduc.state.pagination.defaultCurrent}`;
    const paramsObj = {};
    paramsObj['limit'] = stateReduc.limit;
    console.log('filter refresh',filter);
    Object.keys(filter).forEach(x => {
      switch (x) {
        case 'company':
          paramsObj['organizationId'] = filter[x];
          break;
        case 'penerbit':
          paramsObj['publisherId'] = filter[x];
          break;
        case 'status':
          paramsObj['statusId'] = filter[x];
          break;
        case 'format':
          paramsObj['format'] = filter[x];
          break;
        case 'created_at':
          paramsObj['start_date'] = filter[x][0];
          paramsObj['end_date'] = filter[x][1];
          break;
        default:
          paramsObj[x] = filter[x];
          break;
      }
    });

    try {
      const params = queryString.stringify(paramsObj, {skipEmptyString : true});
      const response = await oProvider.list(`catalog?${params}${url}`);
      if(response){
        const { meta, data } = response;
        const pagination = { ...stateReduc.state.pagination, 
          pageSize: stateReduc.limit, 
          total: parseInt(meta.total), 
          onShowSizeChange:onShowSizeChange, 
          position: ['topRight', 'bottomRight'] 
        };
        reducerFunc('state',{pagination, dataSource: data, loading: false});
        reducerFunc('statusCatalog',meta.status_catalog,'conventional');
        setIsLoaded(true);
      }
      
    } catch (error) {
      console.log(error?.message);
      reducerFunc('state',{loading: false});
      setIsLoaded(false);
      return false;
    }
  };

  const handleTableChange = (pagination, filter, sorter) => {
    reducerFunc('state',{pagination, filter, sorter});
  };

  const viewDetail = async (row) => {
    const { data } = await oProvider.list(`catalog-view-detail?id=${row.id}`);
    reducerFunc('modal',{ model: data, visible: true });
  };
  
  const viewEdit = async (row) => {
    history.push(pathName.digital.listEbookEdit(row.id));
  };

  // ====== Re rendered happen to this code
  // const prevFilter = usePrevious(filter);

  // useEffect(()=>{
  //   if(JSON.stringify(prevFilter) !== JSON.stringify(filter) && state.pagination.current !== state.pagination.defaultCurrent){
  //     console.log(JSON.stringify(prevFilter));
  //     const pagination = {...state.pagination, current : 1}
  //     setState({...state, pagination:pagination});
  //   }
  // },[filter]);

  useEffect(()=>{
      refreshCatalog();
  },[stateReduc.limit,stateReduc.state.pagination.current,Object.keys(filter).map(x => filter[x]).join()]);

  const onSelectChange = (idSelected,e) => {
    reducerFunc('selectedRowKeys',idSelected,'conventional');
    reducerFunc('modalAksi',{ data: e });
  };

  const rowSelection = {
    selectedRowKeys:stateReduc.selectedRowKeys,
    onChange: onSelectChange,
  };

  const tempUpload = async (catids) => {
    const url = `${apiUrl}/${initialEndpoint}/catalog-allocate-export`;
    const payloads = { catalogIds : catids.join(',') }
    const key = randomString(17);
    MessageContent({ type :'loading', content:'Loading ... ',key, duration: 0 });
    try {
      const response = await Axios.post(url, { ...payloads } ,{ headers : getHeaders() });

      if(response){
        MessageContent({type : 'success', content:'Konten berhasil ditambah ke temporary export',key,duration:2});
      }
    } catch (error) {
      if(error.response){
        const { data, status } = error.response;
        MessageContent({type : 'error', content:'Terjadi kesalahan saat proses data',key,duration:2});
        const errorResponse = {
          code : status,
          message : (data.message) ? data.message : data
        }
        Notification({response:errorResponse, type : 'error'});
      }
      return false;
    }
  }

  const handleActionMove = e => {
    if(stateReduc.selectedRowKeys.length && stateReduc.selectAksi){
      if(stateReduc.selectAksi === 'export'){
        tempUpload(stateReduc.selectedRowKeys);
      } else {
        reducerFunc('modalAksi',{ visible: true , aksi:stateReduc.selectAksi });
      }
    }else{
      MessageInfo({type : 'info', text:'Silahkan Pilih Katalog dan Aksi terlebih dahulu'});
    }
  };
  const handleSelectActionMove = (name,val,e) => {
    reducerFunc('modalTitle',e.children,'conventional');
    reducerFunc('selectAksi',val,'conventional');
  };

  const propsActionSelect = {handleSelectActionMove,handleActionMove,selectedRowKeys:stateReduc.selectedRowKeys};
  const propsActionMove = {
    setModalAksi:stateReduc.setModalAksi,
    modalAksi:stateReduc.modalAksi,
    selectAksi:stateReduc.selectAksi,
    setselectedRowKeys:stateReduc.selectAksi,
    refreshCatalog};
  const propsHeaderCatalog1 = {statusCatalog:stateReduc.statusCatalog,state:stateReduc.state};

  return (
    <Content>
      <HeaderCatalog1 {...propsHeaderCatalog1} />
      <br/>
      <HeaderCatalog2 {...propsActionSelect} />
      <hr/>
      <div className="table-catalog">
        <span style={{margin:"1em 0 0 1em",float:'left'}}>
          {stateReduc.selectedRowKeys.length && stateReduc.selectedRowKeys.length} item dipilih
        </span>
        <MocoTable
          {...stateReduc.state}
          rowSelection={rowSelection} 
          onChange={handleTableChange}
          columns={config.catalog({ viewDetail,viewEdit, modalDelete, menuAksesContext,roleAksesContext }).columns}
        />
      </div>
      <br/>
      <hr />
      <HeaderCatalog1 {...propsHeaderCatalog1} />
      <ActionMove {...stateReduc.modalAksi} {...propsActionMove} title={stateReduc.modalTitle}/>
      <ModalForm {...stateReduc.modal} setModal={stateReduc.setModal} viewDetail={viewDetail} viewEdit={viewEdit}/>
    </Content>
  );
}

export default App;