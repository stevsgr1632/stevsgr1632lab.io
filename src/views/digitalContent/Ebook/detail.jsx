import React, { useEffect, useState } from 'react';
import { Content } from 'components';
import oProvider from 'providers/webhook';
import Notification from 'common/notification';
import ModalForm from './modal';
/* eslint-disable */

const notifError = (message) => {
  Notification({ type:'error', response : {
      message: 'Error', 
      description: message
    }
  });
};
const notifSuccess = (message, description) => {
  Notification({ type:'success', response : {
      message, 
      description
    }
  });
};

const DetailKatalog = (props) => {
  const { match } = props;
  const [modal, setModal] = useState({ visible: false });

  const getDetail = async () => {
    const id = props.id || match.params.id;
    try {
      const { data } = await oProvider.get(`catalog-get-one?id=${id}`)
      setModal({ model: data, visible: true })
      notifSuccess('Detail Catalog');
    } catch (err) {
      notifError(err?.message);
      return false;
    }
  }

  useEffect(() => {
    getDetail();
  }, []);

  return (
    <Content>
      <h1>Detail Product</h1>
      <ModalForm {...modal} setModal={setModal} />
    </Content>
  );
}

export default DetailKatalog;