import React from 'react';
import moment from 'moment';
import { useHistory } from 'react-router-dom';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import { Breadcrumb, Content } from 'components';
import NotificationMod from 'common/notification';
import MessageMod from 'common/message';
import FormControl from "./form";
import config from './index.config.js';
/* eslint-disable */

const { digital: digitalUrl } = pathName;

const App = props => {
  let history = useHistory();
  const { schema = {}, add = {} } = config;

  const backToList = () => {
    history.push(digitalUrl.listEbook);
  }

  const openNotification = resp => {
    NotificationMod({ response : resp });
  };

  const handleSubmit = async (e, datasubmit) => {
    e.preventDefault();
    // console.log(e);
    let copyData = { ...datasubmit };
    copyData.catalog_file = datasubmit.catalog_file.filePathName;
    copyData.catalog_file_size = datasubmit.catalog_file?.size;
    copyData.catalog_cover = datasubmit.catalog_cover.file_url_download;
    copyData.catalog_cover_original = datasubmit.catalog_cover.file_url_download;
    copyData.catalog_target_market = datasubmit.catalog_target_market?.join();
    copyData.sub_category = datasubmit.sub_category?.join();
    copyData.catalog_publish_date = datasubmit.catalog_publish_date.substr(0, 4);
    copyData.catalog_epublish_date = datasubmit.catalog_epublish_date.substr(0, 4);
    copyData.catalog_effective_date = datasubmit.catalog_effective_date ? datasubmit.catalog_effective_date : moment(new Date()).format('YYYY-MM-DD');
    try {
      let response = await oProvider.insert('catalog', { ...copyData });
      if(response) {
        openNotification(response);
        backToList();
      }
    } catch (error) {
      console.log(error?.message);
      if(error.response){
        const { data, status } = error.response;
        let errorMessage = '';
        if(data.error){
          if(Array.isArray(data.error)){
            data.error.map( x => {
              errorMessage = `${x.dataPath} : ${x.message}` 
              MessageMod({ type : 'error', text:errorMessage});
            });
          }else{
            errorMessage = data.error; 
            MessageMod({ type : 'error', text:errorMessage});
          }
        } else {
          errorMessage = (data.message) ? data.message : data;
          MessageMod({ type : 'error', text:error?.response?.data?.message});
        }        
      }
      return false;
    }
  }

  const handleOtherSubmit = (e) => {
    console.log(e);
  }

  return (<>
    <Breadcrumb items={add.breadcrumb} />
    <Content {...add.content}>
      <FormControl
        onSubmit={handleSubmit}
        onCancel={backToList}
        schema={schema}
        otherSubmit={handleOtherSubmit}
        type="add"
        data={[]}
      />
    </Content>
  </>);
};

export default App;