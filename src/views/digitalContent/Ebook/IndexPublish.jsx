import React from 'react';
import IndexComponent from "./index";

const IndexPublish = (props) => <IndexComponent {...props} typeUnpublish={0} />;

export default IndexPublish;