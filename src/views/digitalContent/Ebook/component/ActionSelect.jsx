import React from 'react';
import {Form,Button} from 'antd';
import InputSelect from 'components/ant/InputSelect';
/* eslint-disable */

const App = props => {
  const {handleSelectActionMove,handleActionMove,selectedRowKeys = [],menuAksi} = props;

  const actionChoices = (menuAksi) =>{
    
    let choices = [];
    if(Array.isArray(menuAksi)){
      menuAksi.map(x => {
        if(x.access_role){
          choices.push({ text:x.access_text, value:x.access_name});
        }
      });
    }
    // if(getOneMenu(menuAkses,'Hapus')) choices.push({text:'Hapus Produk yang dipilih',value:'delete'});
    // if(getOneMenu(menuAkses,'Sunting')) choices.push(
    //   { text:'Ubah Menjadi Produk Terverifikasi', value:'terverifikasi'},
    //   { text:'Ubah Menjadi Produk Ditangguhkan', value:'ditangguhkan'},
    //   { text:'Ubah Menjadi Produk Diblokir',value:'diblokir'},
    //   { text:'Ubah Menjadi Produk Ditarik', value:'ditarik' },
    //   { text:'Pindah Penerbit', value:'publisher' },
    //   { text:'Pindah Kategori', value:'category' });

    // choices.push({ text:'Tambahkan ke Temporary Export', value:'export'});
    
    return choices;
  }

  return(<>
  <div style={{display:'grid',gridTemplateColumns:'3fr 1fr',gridColumnGap:'0.5em'}}>
    <Form>
      <InputSelect name="action-move-catalog" data={[...actionChoices(menuAksi)]} placeholder="Pilih Aksi" onChange={handleSelectActionMove} />
    </Form>
    <Button type="primary" onClick={handleActionMove} disabled={!selectedRowKeys > 0} style={{width:'100px'}}>
      Pilih
    </Button>
  </div>
  </>);
}

export default App;