import React from 'react';
import { Row, Col, Radio, Button, Table } from 'antd';
import randomString from "utils/genString";
import InputDate from "components/ant/InputDate";
import InputNumber from "components/ant/InputNumber";
import useCustomReducer from "common/useCustomReducer";
import { formDateDisplayValue, formDateInputValue, numberFormat } from "utils/initialEndpoint";
/* eslint-disable */

const initialState = {
  hide : false,
  objHarga : []
}

export default (props) => {
  const { schema = {}, dataSubmit,inputFields,stateHarga, type, dataGetCatalog, reducFunc } = props;
  const [stateReduc,reducerFunc] = useCustomReducer(initialState);

  const handleRadioChange = e => {
    let { value } = e.target;
    if (value === '1') {
      dataSubmit['catalog_prices'] = [];
      dataSubmit['catalog_isfree'] = parseInt(value);
      reducFunc('stateHarga',{radioValue:'1',disabled:true});
    } else {
      dataSubmit['catalog_isfree'] = 0;
      reducFunc('stateHarga',{radioValue:'2',disabled:false});
    }
  };

  const stylePrice = {
    display: 'grid',
    gridAutoFlow: 'column',
    justifyContent: 'start',
    gridColumnGap: '1em',
  };

  const handleAddFields = () => {
    reducerFunc('hide',true,'conventional');
    props.form.setFieldsValue({
      catalog_price:0,
      catalog_discount_percentage :0, 
      catalog_effective_date: formDateInputValue()
    });
    reducerFunc('objHarga',[...stateReduc.objHarga,{ 
      key: (~~(Math.random() * 1231)),
      catalog_price: 0,
      catalog_discount_percentage: 0,
      catalog_effective_date: formDateDisplayValue()
    }],'conventional');
  };

  const handleRemoveFields = index => {
    const values = [...inputFields];
    values.splice(index, 1);
    dataSubmit['catalog_prices'] = values;
    reducerFunc('objHarga',values,'conventional');
    reducFunc('inputFields',values,'conventional');
  };

  const handleAddToTable = () => {
    props.form.validateFields();

    if(props.form.getFieldValue('catalog_price')){
      reducFunc('inputFields',stateReduc.objHarga,'conventional');
      // dataSubmit['catalog_prices'] = [...inputFields,...objHarga];
      (dataSubmit.catalog_prices) ? dataSubmit.catalog_prices = stateReduc.objHarga : dataSubmit['catalog_prices'] = stateReduc.objHarga;
      reducerFunc('hide',false,'conventional');
    }else{
      return;
    }
  };

  const handleInputChange = (name,val, e) => {
    let attrData = [...stateReduc.objHarga];
    let lengthData = attrData.length;
    if(attrData[0]){
      if(attrData[0][name] === '' || attrData.length === 1){
        lengthData = 0;
      }else{
        lengthData = attrData.length - 1;
      }
    }
    if (name === "catalog_price") {
      attrData[lengthData].catalog_price = parseInt(val);
    } 
    if (name === "catalog_discount_percentage"){
      attrData[lengthData].catalog_discount_percentage = parseInt(val);
    }
    if (name === "catalog_effective_date"){
      attrData[lengthData].catalog_effective_date = val;
    }
    reducerFunc('objHarga',attrData,'conventional');
  };
  
  const columns = [
    {
      title: 'Harga Satuan',
      dataIndex: 'catalog_price',
      align:'center',
      key: 'catalog_price',
      render : x => {
        return {
          props: {
            style: { textAlign: 'right' },
          },
          children: <div>{numberFormat(x,'')  || '-'}</div>,
        };
      }
    },
    {
      title: 'Diskon (%) - optional',
      dataIndex: 'catalog_discount_percentage',
      key: 'catalog_discount_percentage',
      render : x => x ? x : 0
    },
    {
      title: 'Masa Berlaku',
      dataIndex: 'catalog_effective_date',
      key: 'catalog_effective_date',
      render : x => x ? formDateDisplayValue(x) : '-'
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record,index) => {
        return(
        <span>
          <Button size="small" type="danger" onClick={() => handleRemoveFields(index)}>REMOVE</Button>
        </span>
      )},
    },
  ];
  
  React.useEffect(() => {
    if(type ==='edit'){
      dataSubmit['catalog_isfree'] = dataGetCatalog?.catalog_isfree;
      reducFunc('inputFields',dataSubmit.catalog_prices,'conventional');
      reducerFunc('objHarga',dataSubmit.catalog_prices,'conventional');
    }else{
      dataSubmit['catalog_isfree'] = 0;
    }
  },[]);


  return (<>
    <h5>
      Kategori Harga
    </h5>
    <small>Pilih kategori harga</small>
    <br />
    <Radio.Group name="catalog_prices" defaultValue={stateHarga?.radioValue} onChange={handleRadioChange} style={stylePrice}>
      <Button><Radio value='1'>Gratis</Radio></Button>
      <Button><Radio  value='2'>Dijual</Radio></Button>
      <Button type="primary" onClick={() => handleAddFields()} disabled={stateHarga?.disabled} style={{display:stateReduc.hide ? 'none' : ''}}>Tambah Harga Baru</Button>
    </Radio.Group>
    <Row gutter={16}>
      {stateReduc.hide && (<>
        <Col span={8}>
          <InputNumber name="catalog_price" schema={schema} onBlur={(name,val,e) => handleInputChange(name,val,e)}
            others={{min:0, disabled: stateHarga.disabled}} 
          />
        </Col>
        <Col span={8}>
          <InputNumber name="catalog_discount_percentage" schema={schema} onBlur={(name,val,e) => handleInputChange(name,val,e)} 
            others={{min:0,max:100,disabled: stateHarga.disabled}}
          />
        </Col>
        <Col span={7}>
          <InputDate 
            name="catalog_effective_date" 
            schema={schema}
            style={{ width: '100%' }} 
            onChange={handleInputChange}
            disabled={stateHarga.disabled}
          />
        </Col>
        <Col span={1} style={{marginTop:'3em'}}>
          <Button size="small" type="primary" onClick={handleAddToTable}>+</Button>
        </Col>
      </>)}
    </Row>
    <hr/>
    <Table rowKey={(row) => (row.id || randomString(10) )}  dataSource={inputFields} columns={columns} />
  </>);
};