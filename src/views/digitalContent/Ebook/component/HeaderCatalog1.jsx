import React from "react";
import { Row,Col, } from "antd";
/* eslint-disable */

const parseData = params => parseFloat(params).toLocaleString("id-ID");

const statusCatalogPreview = params => {
  return(<React.Fragment key={params.status_code}>
    <Col span={6}>
      <div>Produk {params.status_name} : <b>{parseData(params.count)}</b></div>
    </Col>
  </React.Fragment>);
};

export default params => {
  const { state,statusCatalog } = params;
  return(<Row gutter={24}>
    <Col span={24}>
      Menampilkan Katalog
    </Col>
    <Col span={24}>
      <Row gutter={24}>
        <Col span={6}>
          <div>Total Produk : <b>{parseData(state.pagination.total)}</b></div>
        </Col>
        {statusCatalog.map(x => statusCatalogPreview(x))}
      </Row>
    </Col>
  </Row>)
}