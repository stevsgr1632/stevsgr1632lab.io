import React, { useState } from 'react';
import { Modal, Form, Button, Row, Col } from 'antd';
import { DataIndexContext } from '../index';
import {EditOutlined} from '@ant-design/icons';
import Notif from 'common/notification';
import oProvider from 'providers/oprovider';
import MocoTable from 'components/MocoTable';
import InputSelect from 'components/ant/InputSelect';
/* eslint-disable */

const App = props => {
  const [form] = Form.useForm();
  const {kategori: kategoriDataIndex} = React.useContext(DataIndexContext);
  const dropdownAksi = ['publisher','category'];
  const anotherAksi = ['terverifikasi','ditangguhkan','diblokir','delete','ditarik'];
  const { visible, setModalAksi, modalAksi = {},selectAksi,setselectedRowKeys,refreshCatalog } = props;
  const [state, setState] = useState({
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0,
      size:'small',
      showLessItems: true,
    },
    dataSource: []
  });
  const [dataAPI, setDataAPI] = React.useState([]);
  const [chosenData, setchosenData] = React.useState('');
  const [loading, setloading] = React.useState(false);

  React.useEffect(() => {
    setState({ ...state, dataSource: modalAksi.data});
  },[modalAksi]);

  React.useEffect(() => {
    async function fetchData(){
      try {
        if(selectAksi === 'category'){
          setDataAPI(kategoriDataIndex);
        }
        if(selectAksi === 'publisher'){
          const {data : publisher} = await oProvider.list('organization-dropdown');
          setDataAPI(publisher);
        }
      } catch (error) {
        console.log(error?.message);
        return false;
      }
    }
    fetchData();
  },[selectAksi]);

  const columns = [
    {
      title: "SKU & ISBN",
      key: "sku_isbn",
      render : row => {
        return(<>
          <p>SKU : {row.catalog_sku}</p>
          <p>ISBN : {row.catalog_isbn}</p>
        </>);
      }
    },
    {
      title: "Judul Buku",
      key: "title",
      dataIndex : 'catalog_title',
    },
    {
      title: "Penerbit",
      key: "penerbit",
      dataIndex : 'organization_name',
    },
    {
      title: "Price",
      key: "price",
      dataIndex : 'catalog_price',
    },
    {
      title: "Status",
      key: "status",
      width: 100,
      dataIndex: "status_name"
    },
  ];

  const handleTableChange = (pagination, filter, sorter) => {
    setState((state) => ({ ...state, pagination, filter, sorter }));
  }

  const handleChangeData = (name,val,e) => {
    setchosenData(val);
  }
  const handleClickSave = async (e) => {
    let dataSumbmit = null;
    let actionId = '';
    let type     = selectAksi;

    switch (selectAksi) {
      case 'terverifikasi':
        actionId = 'terverifikasi';
        break;
      case 'ditangguhkan':
        actionId = 'ditangguhkan';
        break;
      case 'diblokir':
        actionId = 'diblokir';
        break;
      case 'delete':
        actionId = 'delete';
        break;
      case 'category':
        actionId = chosenData;
        break;
      case 'ditarik': 
        actionId = 'ditarik';
        break;
      case 'publisher':
        actionId = chosenData;
        break;
      default:
        break;
    }

    if(['terverifikasi','ditangguhkan','diblokir','ditarik'].includes(selectAksi)) {
      type = 'status';
    }

    try {
      setloading(true);
      dataSumbmit = {catalogIds:modalAksi.data.map(x => x.id).join(),actionId,type}
      let req = await oProvider.insert('catalog-move',{...dataSumbmit});
      setloading(false);
      Notif({response:req,type:'info',placement:'bottomRight'});
      setModalAksi({visible: false });
      setselectedRowKeys([]);
      setchosenData('');
      refreshCatalog();
    } catch (error) {
      if(error.response){
        const {status, data } = error.response;
        const response = { code : status, message:(data.message) ? data.message : data }
        Notif({response:response,type:'error',placement:'bottomRight'});
      }
      setloading(false);
    }
  }

  const titleModal = () => {
    return(<>
    <div style={{display:'grid',gridTemplateColumns:'min-content 1fr',gridColumnGap:'1em',alignItems:'center',placeContent:'start'}}>
      <EditOutlined />
      <div>
        <h3>{(props.title) ? props.title : '-'}</h3>
        <h6 style={{color:'rgb(130, 130, 130)'}}>LAKUKAN PENGECEKAN SEBELUM KE TAHAP SELANJUTNYA</h6>
      </div>
    </div>
    </>);
  }

  return (<>
    <Form form={form}></Form>
    <Modal
      width="60vw"
      title={titleModal()}
      centered
      visible={visible}
      onCancel={() => setModalAksi({...modalAksi,visible: false })}
      footer={null}
    >
      {dropdownAksi.includes(selectAksi) && (<>
      <Form form={form}>
        <Row gutter={24}>
          <Col span={21}>
            <div id="area"></div>
            <InputSelect 
              name="perubahanaksi"
              onChange={handleChangeData} 
              data={dataAPI} 
              placeholder="Pilih Data"
              others={{getPopupContainer:() => document.getElementById('area'), showSearch:true}} 
            />
          </Col>
          <Col span={2}>
            <Button type="primary" htmlType="submit" loading={loading} disabled={!chosenData} onClick={handleClickSave}>Proses</Button>
          </Col>
        </Row>
      </Form>
      </>)}
      {anotherAksi.includes(selectAksi) && (
        <div style={{marginBottom:'15px'}}>
          <Button type="primary" onClick={handleClickSave} loading={loading} htmlType="button" style={{float:'right'}}>Proses</Button>
        </div>)}
      <Button style={{display:dropdownAksi.includes(selectAksi) && 'none'}}></Button>

      <MocoTable
        {...state}
        onChange={handleTableChange}
        columns={columns}
      />
    </Modal >
  </>);
}

export default App;