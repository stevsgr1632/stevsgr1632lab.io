import React from 'react';
import {Button} from 'antd';
import {FormContext} from '../form';
import { getOneMenu } from 'utils/getMenuAkses';
/* eslint-disable */

const App = props => {
  const {steps,current,prev, next,type,dataSubmit = {},onSubmit } = props;
  const initialData = React.useContext(FormContext);

	return (<>
		<div style={{display:'flex',justifyContent:'flex-end'}}>
	    {current > 0 && (
	      <Button style={{ margin: 8 }} onClick={() => prev()}>Sebelumnya</Button>
	    )}

	    {(type === 'add' && current < steps.length - 1) && (
	      <Button type="primary" style={{ margin: 8 }} onClick={() => next()}>Berikut</Button>
	    )}
	    {(type === 'edit' && current < 4) && (
			(current === 3) ? 
				(getOneMenu(initialData.menuAkses,'validasi')) ?
				<Button type="primary" style={{ margin: 8 }} onClick={() => next()}>Berikut</Button> 
				: <Button style={{ margin: 8 }} type="primary" onClick={(e) => onSubmit(e,dataSubmit)}>Update</Button>
			: <Button type="primary" style={{ margin: 8 }} onClick={() => next()}>Berikut</Button>
		)}
      
	    {(type === 'add' && current === steps.length - 1) && (
	      <Button style={{ margin: 8 }} type="primary" onClick={(e) => onSubmit(e,dataSubmit)}>Simpan</Button>
	    )}
	    {(type === 'edit' && current === 4) && (
	      <Button style={{ margin: 8 }} type="primary" onClick={(e) => onSubmit(e,dataSubmit)}>Update</Button>
	    )}
	  </div>
	  <hr />
	</>);
}

export default App;