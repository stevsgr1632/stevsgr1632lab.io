import React from 'react';
import moment from 'moment';
import {Form,Input,Dropdown,Button,Menu} from 'antd';
import MocoTable from 'components/MocoTable';
import MocoEditor from 'components/MocoEditor';
import { FormContext } from '../form';
/* eslint-disable */
const Validate = props => {
  const dataForm = React.useContext(FormContext);
  const { form,schema,dataSubmit,setdataSubmit } = props;
  const [state, setState] = React.useState({
    loading : false,
    pagination: {
      defaultCurrent: 1,
      pageSize: 10,
      total: 0,
      showLessItems: true,
      showSizeChanger: true, 
    },
    dataSource: []
  });

  const textRender = type => {
    switch (type.toLowerCase()) {
      case 'ditangguhkan':
        return <Button type="primary" shape="round" style={{backgroundColor:'#e57d32',borderColor:'#e57d32'}}>{type.toUpperCase()}</Button>
        break;
      case 'diblokir':
        return <Button type="primary" danger shape="round">{type.toUpperCase()}</Button>;
        break;
        
      default:
        return <Button type="primary" shape="round">{type.toUpperCase()}</Button>;
        break;
    }
  }

  const actionMenu = (row,params) => {
    // const [props] = params; // Output => Object { type: "edit", dataGetCatalog: {…}, form: {…}, schema: {…}, dataSubmit: {…} ...
    const status = dataForm.status;

    const sunting = data => {
      let changeData = {...dataSubmit?.data_qc,status:data.text};
      setdataSubmit({...dataSubmit,data_qc:changeData,catalog_status:data.value});
    };

    return (
      <Menu>
        <Menu.Item>
          <Button type="link" style={{ margin: 0 }} onClick={() => sunting(status[0])}>{status[0].text}</Button>
        </Menu.Item>
        <Menu.Item>
          <Button type="link" style={{ margin: 0 }} onClick={() => sunting(status[1])}>{status[1].text}</Button>
        </Menu.Item>
        <Menu.Item>
          <Button type="link" style={{ margin: 0 }} onClick={() => sunting(status[2])}>{status[2].text}</Button>
        </Menu.Item>
      </Menu>
    );
  };

  const handleEditorChange = value => {
    dataSubmit.catatan_validasi = value;
    form.setFieldsValue({ catatan_validasi: value });
  };

  const handleTableChange = (e) => {
    console.log(e);
  };

  React.useEffect(() => {
    setState({...state, dataSource: [dataSubmit?.data_qc]});
  },[dataSubmit]);

  const columnsFunc = (...params) => {
    return {
      columns: [
        {
          title: "Deskripsi",
          key: "description",
          dataIndex : 'description',
          render: x => x
        },
        {
          title: "Status Produk",
          key: "status",
          dataIndex : 'status',
          render: (x,row) => textRender(x)
        },
        {
          title: "Tanggal",
          key: "status_date",
          dataIndex : 'status_date',
          render: x => x ? moment(x).format('DD-MM-YYYY') : 'None'
        },
        {
          title: "Oleh",
          key: "validate_by",
          dataIndex : 'validate_by',
          render: x => x ? x : 'None'
        },
        {
          title: '',
          key: 'operation',
          fixed: 'right',
          width: 100,
          render: (row) => (
          <span>
            <Dropdown overlay={actionMenu(row,params)}>
              <Button
                type="link"
                style={{ margin: 0 }}
                onClick={e => e.preventDefault()}
              >
                <i className="icon icon-options-vertical" />
              </Button>
            </Dropdown>
          </span>
          ),
        },
      ]
    }
  }

  const catatanValidasi = form.getFieldValue('catatan_validasi');

  return(<div>
    <b>Verifikasi</b>
    <MocoTable 
      {...state}
      onChange={handleTableChange} 
      columns={columnsFunc(props).columns} 
    />

    <Form.Item label={schema.catatan_validasi.label} labelCol={{ span: 24 }}>
      <MocoEditor value={dataSubmit?.catatan_validasi ? catatanValidasi.toString() : ''} noLabel onChange={handleEditorChange} />
      <Form.Item name="catatan_validasi" rules={schema?.catatan_validasi?.rules || []} noStyle>
        <Input type="hidden" />
      </Form.Item>
    </Form.Item>
  </div>);
};

export default Validate;