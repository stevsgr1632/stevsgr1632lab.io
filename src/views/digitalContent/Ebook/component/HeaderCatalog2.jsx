import React from 'react';
import ActionSelect from './ActionSelect';
import { RoleAksesContext } from '../index';
import { useHistory } from 'react-router-dom';
import { Row,Col,Button,Tooltip } from "antd";
import pathName from 'routes/pathName';
import { getOneMenu } from "utils/getMenuAkses";
import { UploadOutlined, DownloadOutlined } from '@ant-design/icons';
/* eslint-disable */

export default props => {
  const roleAksesContext = React.useContext(RoleAksesContext);
  let history = useHistory();

  return(<Row gutter={24}>
    <Col span={8}>
      <ActionSelect {...props} menuAksi={roleAksesContext}/>
    </Col>
    <Col span={6} offset={6}>
      { getOneMenu(roleAksesContext,'export') && <Tooltip title="ekspor">
          <Button 
            type="primary" 
            icon={<DownloadOutlined />} 
            onClick={() => history.push(pathName.digital.exportFileCatalog)} 
            style={{marginRight:'0.5em',background:'#4DBD74',width:'60px'}}/>
        </Tooltip>
      }
      
      { getOneMenu(roleAksesContext,'upload') && <Tooltip title="import">
        <Button 
          type="primary" 
          icon={<UploadOutlined/>} 
          onClick={() => history.push(pathName.digital.importFileCatalog)} 
          style={{marginRight:'1em'}}
        >Katalog</Button>
      </Tooltip>}
    </Col>
  </Row>)
}