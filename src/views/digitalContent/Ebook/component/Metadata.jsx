import React from 'react';
import moment from 'moment';
import { Row,Col,DatePicker,Form } from 'antd';
import InputText from "components/ant/InputText";
import InputSelect from "components/ant/InputSelect";
import InputCheckbox from "components/ant/InputCheckbox";
import { FormContext } from "../form";
import '../styles/metadata.style.scss';
/* eslint-disable */

const styleCheckbox = {marginTop:'3em',border:'0.125em dashed #777',borderRadius:'.125em',padding:'.3em'};
const dateFormat = 'YYYY';

const Metadata = props => {
  const initialData = React.useContext(FormContext);
  const [permission, setPermission] = React.useState({});
  const {schema, handleChangeInput, handleChangeISBN, handleChangeCheckbox, dataSubmit = {} } = props;

  React.useEffect(() => {
    if(initialData?.menuAkses?.length){
      // permission role akses
      let aksesValidasi = initialData?.menuAkses.find(x => x.access_name.toLowerCase() === 'validasi');
      let aksesUpload = initialData?.menuAkses.find(x => x.access_name.toLowerCase() === 'upload');
      setPermission({...permission,aksesUpload,aksesValidasi});
    }
  },[initialData]);

  return (<>
    <Row gutter={24}>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputText name="catalog_title" schema={schema} onBlur={handleChangeInput} />
          </Col>
          {(props.type === 'edit' && permission?.aksesValidasi?.access_role) && 
          (<Col span={1}>
            <InputCheckbox name="val_catalog_title" schema={schema} onChange={handleChangeCheckbox} styleCustom={styleCheckbox} />
          </Col>)
          }
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputText name="catalog_sub_title" schema={schema} onBlur={handleChangeInput} placeholder='Masukkan Sub Title'/>
            </Col>
            {(props.type === 'edit') &&
            (<Col span={1}>

            </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputText name="catalog_authors" schema={schema} onBlur={handleChangeInput} />
          </Col>
          {(props.type === 'edit' && permission?.aksesValidasi?.access_role) && 
          (<Col span={1}>
            <InputCheckbox name="val_catalog_authors" schema={schema} onChange={handleChangeCheckbox} styleCustom={styleCheckbox}/>
          </Col>)
          }
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputText name="catalog_isbn" schema={schema} placeholder="ISBN 978-979-15000-0-5" title="Contoh format : ISBN 978-979-15000-0-5" onBlur={handleChangeISBN} />
          </Col>
          {(props.type === 'edit' && permission?.aksesValidasi?.access_role) && 
          (<Col span={1}>
            <InputCheckbox name="val_catalog_isbn" schema={schema} onChange={handleChangeCheckbox} styleCustom={styleCheckbox}/>
          </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputText name="catalog_eisbn" schema={schema} onBlur={handleChangeInput} />
          </Col>
          {(props.type === 'edit' && permission?.aksesValidasi?.access_role) && 
          (<Col span={1}>
            <InputCheckbox name="val_catalog_eisbn" schema={schema} onChange={handleChangeCheckbox} styleCustom={styleCheckbox}/>
          </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputSelect name="category_id" data={initialData?.kategori} schema={schema} onChange={handleChangeInput} others={{showSearch:true}} />
          </Col>
          {(props.type === 'edit' && permission?.aksesValidasi?.access_role) && 
          (<Col span={1}>
            <InputCheckbox name="val_category_id" schema={schema} onChange={handleChangeCheckbox} styleCustom={styleCheckbox}/>
          </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputSelect mode="multiple" name="sub_category" data={initialData?.kategori} schema={schema} onChange={handleChangeInput} others={{showSearch:true}} />
          </Col>
          {(props.type === 'edit') &&
            (<Col span={1}>

            </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputSelect name="catalog_edition" data={initialData?.edition} schema={schema} onChange={handleChangeInput} />
          </Col>
          {(props.type === 'edit') &&
            (<Col span={1}>

            </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
            <Col span={props.type === 'edit' ? 23 : 24}>
              <InputText name="catalog_series" schema={schema} onBlur={handleChangeInput} />
            </Col>
            {(props.type === 'edit') &&
            (<Col span={1}>

            </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
            <Col span={props.type === 'edit' ? 23 : 24}>
              <InputSelect name="catalog_volume" data={initialData?.volume} schema={schema} onChange={handleChangeInput} />
            </Col>
              {(props.type === 'edit') &&
            (<Col span={1}>

            </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
            <Col span={props.type === 'edit' ? 23 : 24}>
              <InputSelect name="edu_level_id" data={initialData?.edulevel} schema={schema} onChange={handleChangeInput} />
            </Col>
              {(props.type === 'edit') &&
            (<Col span={1}>

            </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputSelect name="catalog_language" data={initialData?.bahasa} schema={schema} onChange={handleChangeInput} />
          </Col>
          {(props.type === 'edit' && permission?.aksesValidasi?.access_role) && 
          (<Col span={1}>
            <InputCheckbox name="val_catalog_language" schema={schema} onChange={handleChangeCheckbox} styleCustom={styleCheckbox}/>
          </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputSelect name="catalog_result_object" data={initialData?.btkKarya} schema={schema} onChange={handleChangeInput} />
          </Col>
          
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <Form.Item name='catalog_publish_date' label={schema.catalog_publish_date.label} rules={schema.catalog_publish_date.rules} labelCol={{span:24}}>
              <DatePicker picker="year"  
                style={{ width: '100%' }}
                name="catalog_publish_date" 
                initialValues={dataSubmit.catalog_publish_date ? moment(dataSubmit?.catalog_publish_date,dateFormat) : null}
                onChange={(date, dateString) => {
                  handleChangeInput('catalog_publish_date', dateString);
                }}
              />
            </Form.Item>
          </Col>
          {(props.type === 'edit' && permission?.aksesValidasi?.access_role) && 
          (<Col span={1}>
            <InputCheckbox name="val_catalog_publish_date" schema={schema} onChange={handleChangeCheckbox} styleCustom={styleCheckbox}/>
          </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <Form.Item name='catalog_epublish_date' label={schema.catalog_epublish_date.label} rules={schema.catalog_epublish_date.rules} labelCol={{span:24}}>
              <DatePicker picker="year"  
                style={{ width: '100%' }}
                initialValues={dataSubmit.catalog_epublish_date ? moment(dataSubmit?.catalog_epublish_date,dateFormat) : null}
                onChange={(date, dateString) => {
                  handleChangeInput('catalog_epublish_date', dateString);
                }} 
              />
            </Form.Item>
          </Col>
          {(props.type === 'edit' && permission?.aksesValidasi?.access_role) && 
          (<Col span={1}>
            <InputCheckbox name="val_catalog_epublish_date" schema={schema} onChange={handleChangeCheckbox} styleCustom={styleCheckbox}/>
          </Col>)}
        </Row>
        
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputSelect name="organization_id" data={initialData?.penerbit} schema={schema} onChange={handleChangeInput} others={{showSearch:true}} />
          </Col>
          {(props.type === 'edit' && permission?.aksesValidasi?.access_role) && 
          (<Col span={1}>
            <InputCheckbox name="val_organization_id" schema={schema} onChange={handleChangeCheckbox} styleCustom={styleCheckbox}/>
          </Col>)}
        </Row>
      </Col>
      <Col span={12}>
      <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputText name="catalog_publish_city" readOnly schema={schema} onBlur={handleChangeInput} />
          </Col>
          {(props.type === 'edit') &&
          (<Col span={1}>

          </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputSelect name="catalog_source_product" data={initialData?.asalProduk} schema={schema} onChange={handleChangeInput} />
          </Col>
          {(props.type === 'edit' && permission?.aksesValidasi?.access_role) && 
          (<Col span={1}>
            <InputCheckbox name="val_catalog_source_product" schema={schema} onChange={handleChangeCheckbox} styleCustom={styleCheckbox}/>
          </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputText type="number" min="1" name="catalog_pages" schema={schema} onBlur={handleChangeInput} />
          </Col>
          {(props.type === 'edit' && permission?.aksesValidasi?.access_role) && 
          (<Col span={1}>
            <InputCheckbox name="val_catalog_pages" schema={schema} onChange={handleChangeCheckbox} styleCustom={styleCheckbox}/>
          </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputText name="catalog_link" schema={schema} onBlur={handleChangeInput} />
          </Col>
        {(props.type === 'edit') &&
            (<Col span={1}>

            </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputText name="catalog_copyright" schema={schema} onBlur={handleChangeInput} />
            </Col>
        {(props.type === 'edit') &&
            (<Col span={1}>

            </Col>)}
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={5}>
          <Col span={props.type === 'edit' ? 23 : 24}>
            <InputSelect mode="multiple" name="catalog_target_market" data={initialData?.targetMarket} schema={schema} onChange={handleChangeInput} />
          </Col>
          {(props.type === 'edit' && permission?.aksesValidasi?.access_role) && 
          (<Col span={1}>
            <InputCheckbox name="val_catalog_target_market" schema={schema} onChange={handleChangeCheckbox} styleCustom={styleCheckbox}/>
          </Col>)}
        </Row>
      </Col>
    </Row>
  </>);
}

export default Metadata;