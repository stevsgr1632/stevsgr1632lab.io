import React from 'react';
import { Row,Col,Modal } from 'antd';
import {shorterString} from 'utils/genString';
import LoadingSpin from 'components/LoadingSpin';
import InputCheckbox from "components/ant/InputCheckbox";
import useCustomReducer from "common/useCustomReducer";
import {FormContext} from '../form';
import InputUpload from '../../../organisasi/OrganizationGroup/component/InputUpload';

/* eslint-disable */

const initialState = {
  permission : {},
  loadingupload: false,
  previewUpload : {visible:false,title:'',src:''},
  preview: {visible:false,title:'',src:''},
  dataFile : '',
  coverName : '',
  progress : {file:0,cover:0}
}

const App = props => {
  const formContextData = React.useContext(FormContext);
  const { schema={}, handleBerkas,dataSubmit = {}, dataGetCatalog = {}, handleChangeCheckbox, reducFunc} = props;
  const [stateReduc,reducerFunc] = useCustomReducer(initialState);

  const handleUploadImage = async (name,name2,file) => {
    const { originalFileName,file_url_download } = file;
    reducerFunc('coverName',originalFileName,'conventional');
    reducerFunc('loadingupload',true,'conventional');
    handleBerkas(name,file);
    reducerFunc('previewUpload',{visible:false,title:originalFileName,src:file_url_download});
    reducerFunc('loadingupload',false,'conventional');
  }

  const uploadImageChange = (name,name2,file) => {
    if(file.status === 'done'){
      reducFunc('uploading',{[name2]:false});
      handleUploadImage(name,name2,file.response);
    }else {
      reducFunc('uploading',{[name2]:true});
    }
  }

  const handleUploadFile = (name,name2,file) => {
    const { originalFileName } = file;
    reducerFunc('dataFile',originalFileName,'conventional');
    handleBerkas(name,file);
  }

  const uploadFileChange = (name,name2,file) => {
    if(file.status === 'done'){
      reducFunc('uploading',{[name2]:false});
      handleUploadFile(name,name2,file.response);
    }else {
      reducFunc('uploading',{[name2]:true});
    }
  }

  // for upload preview
  const handlePreviewUpload = () => {
    reducerFunc('previewUpload',{visible:true});
  };
  const handleCancelUpload = () => reducerFunc('previewUpload',{visible:false});
  
  // for existing cover preview
  const handlePreview = () => {
    reducerFunc('preview',{ visible:true });
  };
  const handleCancel = () => reducerFunc('preview',{ visible:false });
  
  const styleContainer = {
    display:'grid',alignContent:'start'
  }

  React.useEffect(() => {
    switch (props.type) {
      case 'edit':
        reducerFunc('preview',{title:dataGetCatalog.catalog_cover?.split('/').pop(),src:dataGetCatalog.catalog_cover})
       
        if(dataSubmit.catalog_file_edit){
          reducerFunc('dataFile',dataSubmit.catalog_file_edit?.originalFilename || '','conventional');
        }else{
          reducerFunc('dataFile',dataGetCatalog.catalog_file?.split('/').pop() || '','conventional');
        }
        if(dataSubmit.catalog_cover_edit){
          reducerFunc('previewUpload',{title:dataSubmit.catalog_cover_edit?.originalFileName || '',src:dataSubmit.catalog_cover_edit?.file_url_download || ''});
          let catalog_cover_edit = Object.assign(dataSubmit.catalog_cover_edit, {});
          reducerFunc('coverName',catalog_cover_edit.originalFileName,'conventional');
        }else{
          reducerFunc('coverName',dataGetCatalog.catalog_cover?.split('/').pop(),'conventional');
        }
        break;
      case 'add':
        if(dataSubmit.catalog_file){
          reducerFunc('dataFile',dataSubmit.catalog_file?.originalFilename,'conventional');
        }
        if(dataSubmit.catalog_cover){
          reducerFunc('previewUpload',{title:dataSubmit.catalog_cover?.originalFileName || '',src:dataSubmit.catalog_cover?.file_url_download || ''});
          reducerFunc('coverName',dataSubmit.catalog_cover?.originalFileName,'conventional');
        }
        break;
      default:
        break;
    }
  },[props.type,dataGetCatalog]);

  const inputFileComp = () => {
    return(<>
      {props.type === 'edit' && (
        <div className="ant-form-item-label" style={{justifySelf: 'start'}}>
          <span style={{color:'red'}}>* </span>
          <label>Berkas Content</label>
        </div>
      )}
      <InputUpload 
        name={props.type === 'add' ? 'catalog_file' : 'catalog_file_edit'}
        name2='catalog_file_name'
        allowedFileType='.pdf,.epub' 
        uploadURL='upload-content' 
        fileMaxSize={20}
        fileName={{catalog_file_name:stateReduc.dataFile}}
        accessToken = {true}
        schema={schema} 
        onChange = {uploadFileChange}>
          <small>Berkas Konten : pdf, epub. Maks 20MB</small> 
        </InputUpload>
    </>)
  };

  const inputImageComp = () => {
    return(<>
      {props.type === 'edit' && (
        <div className="ant-form-item-label" style={{justifySelf: 'start'}}>
          <span style={{color:'red'}}>* </span>
          <label>Berkas Sampul</label>
        </div>
      )}
      <InputUpload 
        name={props.type === 'add' ? 'catalog_cover' : 'catalog_cover_edit'}
        name2='catalog_cover_name'
        allowedFileType='.png,.jpg,.jpeg' 
        uploadURL='upload-image' 
        fileMaxSize={2}
        fileName={{catalog_cover_name:stateReduc.coverName}}
        accessToken = {true}
        schema={schema}
        onChange={uploadImageChange}>
          <small>Berkas Sampul : jpg, jpeg, png. Maks 2MB</small> 
      </InputUpload>
  
      <LoadingSpin loading={stateReduc.loadingupload}>
        <div style={{cursor:'pointer'}}>
        {stateReduc.previewUpload?.title && <img src={stateReduc.previewUpload?.src} onClick={handlePreviewUpload} width="150px" height="200px" alt="no-data" />}
        </div>
      </LoadingSpin>
    </>);
  }

  React.useEffect(() => {
    if(formContextData?.menuAkses?.length > 0){
      // permission role akses
      let aksesValidasi = formContextData?.menuAkses.find(x => x.access_name.toLowerCase() === 'validasi');
      let aksesUpload = formContextData?.menuAkses.find(x => x.access_name.toLowerCase() === 'upload');
      reducerFunc('permission',{aksesUpload,aksesValidasi});
    }
  },[formContextData]);

  const WrapperCheckbox = ({name,catalog_type}) => {
    
    return(<>
      <div style={{display:'flex',justifyContent:'space-between'}}>
        <p>{shorterString(dataGetCatalog[catalog_type]?.split('/').pop())}</p>
        <div style={{border:'0.110em dashed #777',borderRadius:'.125em',width:'15%',textAlign: 'center'}}>
          {stateReduc.permission?.aksesValidasi?.access_role && <InputCheckbox name={name} schema={schema} onChange={handleChangeCheckbox} styleForm={{marginBottom:0}} />}
        </div>
      </div>
    </>);
  };

  return (<>
  <Row gutter={[16,16]}>
    {props.type === 'edit' && (<>
      <Col span={12}>
        <Row gutter={[16,16]}>
          <Col span={24} style={styleContainer}>{inputFileComp()}</Col>
          <Col span={24} style={styleContainer}>{inputImageComp()}</Col>
        </Row>
      </Col>
      <Col span={12}>
        <Row gutter={[16,16]}>
          <Col span={12}>
            <div style={{cursor:'pointer',height:'100%'}}>
                <img src={dataGetCatalog?.catalog_cover} onClick={handlePreview} width="100%" height= '100%' alt="no-data" />
                {/* <WrapperCheckbox name="val_catalog_cover" catalog_type="catalog_cover" /> */}
            </div>
          </Col>
          <Col span={12}>
            <div style={{cursor:'pointer',height:'100%'}}>
                <div style={{display:'block',width:'100%',height:'100%',backgroundColor:'#ddd'}}><kbd>{dataGetCatalog?.catalog_file_ext}</kbd></div>
                {/* <WrapperCheckbox name="val_catalog_file" catalog_type="catalog_file" />  */}
            </div>
          </Col>
        </Row>
        <Row gutter={[16,16]}>
          <Col span={12}>
            <WrapperCheckbox name="val_catalog_cover" catalog_type="catalog_cover" /> 
          </Col>
          <Col span={12}>
            <WrapperCheckbox name="val_catalog_file" catalog_type="catalog_file" />
          </Col>
        </Row>
      </Col>
    </>)}
    {props.type === 'add' && (<>
      <Col span={12} style={styleContainer}>
        {inputFileComp()}
      </Col>
      <Col span={12} style={styleContainer}>
        {inputImageComp()}
      </Col>
    </>)}
  </Row>
  <Modal
    visible={stateReduc.preview.visible}
    title={stateReduc.preview.title}
    footer={null}
    centered
    onCancel={handleCancel}
    >
    <img alt="example" style={{ width: '100%' }} src={stateReduc.preview.src} />
  </Modal>
  <Modal
    visible={stateReduc.previewUpload.visible}
    title={stateReduc.previewUpload.title}
    footer={null}
    centered
    onCancel={handleCancelUpload}
    >
    <img alt="example" style={{ width: '100%' }} src={stateReduc.previewUpload.src} />
  </Modal>
  </>);
}

export default App;