import React from 'react';
import { Typography,Space,Modal } from "antd";
import { DeleteOutlined } from '@ant-design/icons';
import oProvider from 'providers/oprovider';
import Notification from 'common/notification';
/* eslint-disable */

export default (props,refreshCatalog) => {
  Modal.confirm({
    title: 'Hapus Katalog',
    icon: <DeleteOutlined />,
    content: <React.Fragment>
      <Space direction="vertical">
        <Typography.Text>Anda yakin ingin menghapus data ini (<b>{props.catalog_title}</b>) dan semua data terkait?</Typography.Text>
        <Typography.Text type="danger">Tindakan ini tidak bisa dibatalkan</Typography.Text>
      </Space>
    </React.Fragment>,
    onOk: async () => {
      try {
        const response = await oProvider.delete(`catalog?catalogIds=${props.id}`);
        if (response) {
          Notification({type: 'success', response : {
              message : 'Data berhasil dihapus',
              code    : response.message,
            }
          });
          refreshCatalog();
        }
      } catch (error) {
        Notification({type: 'error', response : {
            message : 'Data gagal dihapus',
            code    : 'Silahkan muat ulang halaman dan coba lagi',
          } 
        });
        console.log(error.message);
        return;
      }
    },
    onCancel() { },
  });
};