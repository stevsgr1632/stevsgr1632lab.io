import React from 'react';
import MocoEditorQuill from 'components/MocoEditorQuill';
/* eslint-disable */

const App = (props) => {
  const { schema, dataSubmit,form,reducFunc} = props;

  const handleChange = (value) => {
    reducFunc('dataSubmit',{catalog_description:value});
  }

  return (
    <MocoEditorQuill 
      name='catalog_description' 
      onChange={handleChange} 
      schema={schema} 
      placeholder='Tentang eBook' 
      value={dataSubmit.catalog_description} 
      form={form}
      required={true}/>
  );
}

export default App;
