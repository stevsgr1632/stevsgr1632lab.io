import React from 'react';
import "../styles/baca.style.scss";
import { Button,Space } from "antd";
import { usePdf } from '@mikecousins/react-pdf';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import { checkedURL } from "common/validators";
import { Content,Breadcrumb } from 'components';
import { RightOutlined,LeftOutlined,ZoomInOutlined,ZoomOutOutlined } from "@ant-design/icons";
/* eslint-disable */

/*
  Sample PDF url
  https://file-examples.com/index.php/sample-documents-download/sample-pdf-download/
*/
const samplePDFUrl = 'https://file-examples-com.github.io/uploads/2017/10/file-sample_150kB.pdf';
const { digital : digitalUrl, base : baseUrl } = pathName;
const ButtonWrap = props => {
  const { pdfDocument,scale,setscale,page,setPage } = props;
  return (<>
  {Boolean(pdfDocument && pdfDocument.numPages) && (
    <Space direction="vertical" className="baca__button">
      <Button 
        icon={<ZoomInOutlined />}
        disabled={scale === 3} 
        onClick={() => setscale(prev => prev + 1)}
      />
      <Button 
        icon={<ZoomOutOutlined />}
        disabled={scale === 1} 
        onClick={() => setscale(prev => prev - 1)}
      />
      <Button 
        icon={<LeftOutlined />}
        disabled={page === 1} 
        onClick={() => setPage(page - 1)}
      />
      <Button 
        icon={<RightOutlined />} 
        disabled={page === pdfDocument.numPages}
        onClick={() => setPage(page + 1)}
      />
    </Space>
  )}
  </>)
};

const MyPdfViewer = (props) => {
  console.log(props);
  const [page, setPage] = React.useState(1);
  const [scale, setscale] = React.useState(1);
  const [data,setData] = React.useState({catalog_file : samplePDFUrl });
  const canvasRef = React.useRef(null);
  const paramsId = props.match.params.id;

  const { pdfDocument, pdfPage } = usePdf({
    file: data?.catalog_file,
    page,
    canvasRef,
    scale
  });

  const scope = {
    breadcrumb: [
      { text: "Beranda", to: baseUrl },
      { text: "Kontent Digital", to: digitalUrl.listEbook },
      { text: "Read Buku Elektronik" }
    ],
  };

  const fetchData = async () => {
    try {
      const { data } = await oProvider.list(`catalog-get-one?id=${paramsId}`);
      console.log(data);
      setData(prev => ({...prev, ...data, catalog_file : checkedURL(data?.catalog_file) ? data?.catalog_file : samplePDFUrl }));
    } catch (error) {
      console.log(error?.message);
    }
  };

  React.useEffect(() => {
    fetchData();
  },[]);

  const propsButtonWrap = {pdfDocument,scale,setscale,page,setPage};

  return (<>
    <Breadcrumb items={scope.breadcrumb} />
    <Content>
      <div className="baca__container">
        {!pdfDocument && <span>Loading...</span>}
        <canvas ref={canvasRef} />
        <ButtonWrap {...propsButtonWrap} />
      </div>
    </Content>
  </>);
};

export default MyPdfViewer;