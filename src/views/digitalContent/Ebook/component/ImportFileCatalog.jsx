import React from 'react';
import axios from 'axios';
import moment from 'moment';
import config from '../index.config';
import Message from 'common/message';
import oProvider from 'providers/oprovider';
import {UploadOutlined} from '@ant-design/icons';
import InputSelect from 'components/ant/InputSelect';
import { Content, Breadcrumb, MocoTable } from 'components';
import localStorageService from "utils/localStorageService";
import {Form, Row, Col,Button,Input,Upload, Progress} from 'antd';
import { initURL,initialEndpoint,numberFormat } from "utils/initialEndpoint";
/* eslint-disable */

const ImportFileCatalog = props => {
  const authKey = localStorageService.state_auth.getAccessToken();
  const [form]= Form.useForm();
  const {importFile,schema} = config;
  const [nameFile,setnameFile] = React.useState('');
  const [penerbitSelect,setpenerbitSelect] = React.useState({});
  const [state,setState] = React.useState({
    loading: false,
    pagination: {
      current: 1,
      pageSize: 10,
      total: 0,
      showLessItems: true,
    },
    dataSource: []
  });

  const [, setDefaultFileList] = React.useState([]);
  const [progress, setProgress] = React.useState(0);

  const uploadFileContent = async options => {
    if(!penerbitSelect?.organization_id) {
      Message({type:'error',text:`Silahkan pilih penerbit terlebih dahulu`});
      return false;
    }
    const { onSuccess, onError, file, onProgress } = options;

    const fmData = new FormData();
    const config = {
      headers: { "content-type": "multipart/form-data",
                "Accept" : "application/vnd.api+json",
                "Authorization" : `Bearer ${authKey?.attributes?.accessToken}` },
      onUploadProgress: event => {
        const percent = ~~((event.loaded / event.total) * 100);
        setProgress(percent);
        if (percent === 100) {
          setTimeout(() => setProgress(0), 1000);
        }
        onProgress({ percent: (event.loaded / event.total) * 100 });
      }
    };
    fmData.append("organization_id", penerbitSelect?.organization_id);
    fmData.append("filename", file);
    try {
      setState({ ...state, loading: true });
      const {data : {meta,data} = {}} = await axios.post(
        `${initURL}/${initialEndpoint}/upload-catalog`,
        fmData,
        config
      );

      onSuccess("Ok");
      // console.log("server res: ", data);
      const pagination = { ...state.pagination, total: meta?.totalData };
      setState({...state, pagination, dataSource: data, loading: false});
      setnameFile(meta?.originalFileName);
    } catch (err) {
      console.log("Eroor: ", err);
      onError({ err });
      Message({type:'error',text:`Silahkan cek kembali isi file Anda`});
      setState({ ...state, loading: false });
    }
  };
  const beforeUpload = (file) => {
    let isAccept = null, allowedFileType = ".zip", fileMaxSize = 150;
    if(allowedFileType !== "*"){
      let isAcceptFile = allowedFileType.split(',');
      let typeFile = '.' + file.name.split('.').pop();
      isAccept = isAcceptFile.includes(typeFile);
      if (!isAccept) {
        Message({type:'error',text:`You can only upload ${isAcceptFile.map(x => '.' + x.substring(1).toUpperCase()).join(', ')} file`});
      }
    }
    const isLt2M = file.size / 1024 / 1024 < fileMaxSize;
    if (!isLt2M) {
      Message({type:'error',text:`Image must smaller than ${fileMaxSize}MB`});
    }
    return isAccept && isLt2M;
  };

  const handleOnChange = ({ file, fileList, event }) => {
    if(!penerbitSelect?.organization_id) {
      // Message({type:'error',text:`Silahkan pilih penerbit terlebih dahulu`});
      return false;
    }
    // console.log(file, fileList, event);
    //Using Hooks to update the state to the current filelist
    setDefaultFileList(fileList);
    //filelist - [{uid: "-1",url:'Some url to image'}]
  };

  const styleButton = {
    backgroundColor:'#4dbd74',color:'#f0f3f5',borderColor:'#4dbd74'
  }
  
  const handleClickSimpan = async (e) => {
    e.preventDefault();
    try {
      let resp = await oProvider.update(`catalog-upload-save?organization_id=${penerbitSelect?.organization_id}`,{});
      if(!resp) {
        Message({type:'error',text:'Ada Error'});
      }else{
        Message({type:'success',text:`${resp.message}`})
      }
    } catch (error) {
      console.log(error);
    }
  }
  
  const handleTableChange = (pagination, filter, sorter) => {
    setState((state) => ({ ...state, pagination, filter, sorter }));
  }

  const handleChangeSelect = (name,value,e) => {
    // console.log(name,value,e);
    setpenerbitSelect({organization_id : value});
  }

  const columns = [
    {
      title: "ISBN",
      key: "catalog_isbn",
      dataIndex : "catalog_isbn",
      render: x => x
    },
    {
      title: "Judul Buku",
      key: "catalog_title",
      dataIndex : "catalog_title",
      render: x => x
    },
    {
      title: "Kepengarangan",
      key: "catalog_authors",
      dataIndex : "catalog_authors",
      render: x => x
    },
    {
      title: "Tanggal Terbit",
      key: "catalog_publish_date",
      dataIndex : "catalog_publish_date",
      render: x => moment(x).format('DD MMMM YYYY')
    },
    {
      title: "Harga (Rp)",
      key: "price",
      dataIndex : "price",
      render: x => x ? numberFormat(x) : '-'
    },
    {
      title: "Tanggal Harga Efektif",
      key: "price_effective",
      dataIndex : "price_effective",
      render: x => moment(x).format('DD MMMM YYYY')
    },
  ];

  React.useEffect(() => {
    async function fetchData() {
      try {
        const {data : penerbit} = await oProvider.list('organization-dropdown');
        penerbit.unshift({ value: '', text: '-- Semua Penerbit --' });
        setState(prev => ({...prev, penerbit}));
      } catch (error) {
        console.log(error?.message);
      }
    }
    fetchData();
  },[]);

  return(<>
  <Breadcrumb items={importFile.breadcrumb} />
  <Content title={<><UploadOutlined /> {importFile.title}</>}>
    <Form form={form}>
      <Row gutter={16}>
        <Col span={8}>
          <InputSelect 
            name="penerbit" 
            schema={schema} 
            placeholder="Pilih Penerbit" 
            data={state?.penerbit} 
            onChange={handleChangeSelect} 
            others={{showSearch:true}} 
          />
        </Col>
        <Col span={4}></Col>
        <Col span={12}>
          <div className="ant-form-item-label" style={{justifySelf: 'start'}}>
            <span style={{color:'red'}}>* </span>
            <label>Import Katalog</label>
          </div>
          <div style={{display:'flex'}}>
            <Input type="text" readOnly value={nameFile} />
            <Upload
              name="berkas_catalog"
              accept=".zip"
              customRequest={uploadFileContent}
              onChange={handleOnChange}
              showUploadList={false}
              beforeUpload={beforeUpload}
              onProgress={({ percent }) => {
                if (percent === 100) {
                  setTimeout(() => setProgress(0), 1000);
                }
                return setProgress(~~(percent));
              }}
            >
              <Button style={styleButton} loading={(progress < 100 && progress > 0) ? true : false}>
                {(progress < 100 && progress > 0) ? null : (<><UploadOutlined /> Upload</>)}
              </Button>
            </Upload>
          </div>
          <small>Berkas Catalog (berisi file EXCEL) : .zip dan Maks 150MB {progress > 0 ? <Progress percent={progress} /> : null}</small>
        </Col>
      </Row>
      <Button type="primary" htmlType="submit" onClick={handleClickSimpan} style={{float:'right'}}>Simpan</Button>
    </Form>
  </Content>
  <Content>
    <MocoTable
      {...state}
      onChange={handleTableChange}
      columns={columns}
    />
  </Content>
  </>);
};

export default ImportFileCatalog;