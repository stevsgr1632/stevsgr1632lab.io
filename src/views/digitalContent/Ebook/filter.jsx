import React, { useEffect } from 'react';
import { Form } from 'antd';
import oProvider from 'providers/webhook';
import InputForm from 'components/ant/InputForm';
import useCustomReducer from "common/useCustomReducer";
import { DataIndexContext } from "./index";
/* eslint-disable */

const App = (props) => {
  const initiaState = {
    filter: props.filter,
    data: {},
    loading:{ company: true, kategori: true, format: true, status : true }

  }
  const [form] = Form.useForm();
  const dataFromIndex = React.useContext(DataIndexContext);
  const [stateReduc,reducerFunc] = useCustomReducer(initiaState);
  const statusJual = [
    {value: 0, text: '-- Semua Status --' },
    {value: 1, text: 'Dijual'},
    {value: 2, text: 'Gratis' }
  ]

  const onChange = (name, value) => {
    switch (name) {
      case 'company':
        reducerFunc('loading',{ penerbit: true });
        try {
          oProvider.list(`organization-dropdown?id=${value}`).then(({ data: list }) => {
            list.unshift({ value: '', text: '-- Semua Penerbit --' });
            reducerFunc('data',{ penerbit: list });
            reducerFunc('loading',{ penerbit: false });
  
            if (props.schema) {
              setTimeout(() => {
                const { form } = props.schema;
                form.setFieldsValue({ penerbit: '' });
              }, 400);
            }
          });
        } catch (error) {
          console.log(error?.message);
          reducerFunc('loading',{ penerbit: false });
          return false;
        }
        break;
      case 'action':
        if (value === 'clear') {
          clearFilter();
        }
        if (value === 'search') {
          reducerFunc('filter',{ [value]: props.schema.form.getFieldValue(value) });
        }
        break;
      default:
        break;
    }

    

    if (name !== 'action' && name !== 'search') {
      if(name === 'created_at'){
        if(value[0].length > 0 && value[1].length >0){
          reducerFunc('filter',{ [name]: value });
        } 
        if(value[0].length < 1 && value[1].length < 1){
          reducerFunc('filter',{ [name]: value });
        }
      } else if(Object.keys(props.schema).includes(name)){
        reducerFunc('filter',{ [name]: value });
      }      
      
    } 
  }

  const initilize = async () => {
    // console.log(dataFromIndex);
    props.schema.form.setFieldsValue({...stateReduc.filter,catalog_isfree:0});
    reducerFunc('data',{ ...dataFromIndex });
    reducerFunc('loading',{});
  }

  const clearFilter = () => {
    const clearFilter = { company: '', penerbit: '', kategori: '', status: '', format: '', search: '' , catalog_isfree: 0 , created_at: ["",""]};
    props.schema.form.setFieldsValue(clearFilter);
    reducerFunc('filter',{ ...clearFilter });
  }

  const onEnterSearch = (e) => {
    reducerFunc('filter',{ [e.target.name]: e.target.value });
  }

  useEffect(() => {
    props.onFilterChange(stateReduc.filter);
  }, [stateReduc.filter]);

  useEffect(() => {
    initilize();
  }, [dataFromIndex]);

  return (
    <div>
      <InputForm {...props}
        form={form}
        onChange={onChange}
        defCol={{ md: 12, lg: 12, xl: 8 }}
        controls={[
          { type: 'select', name: 'company', data: stateReduc.data.company, loading: stateReduc.loading.company, others:{showSearch:true} },
          { type: 'select', name: 'penerbit', data: stateReduc.data.penerbit, loading: stateReduc.loading.penerbit, others:{showSearch:true} },
          { type: 'select', name: 'kategori', data: stateReduc.data.kategori, loading: stateReduc.loading.kategori, others:{showSearch:true} },
          { type: 'select', name: 'status', data: stateReduc.data.status, loading: stateReduc.loading.status },
          { type: 'select', name: 'format', data: stateReduc.data.format, loading: stateReduc.loading.format },
          { type: 'select', name: 'catalog_isfree', data: statusJual, label: "Status Jual" },
          { type: 'rangepicker', name: 'created_at' , style:{width:'100%'}},
          { type: 'text', name: 'search', onPressEnter:onEnterSearch},
          {
            type: 'action', name: 'action', defcol: { md: 8 }, useLabel: true, actions: [
              { text: 'Pencarian', type: 'primary', style: { marginRight: 5 }, action: 'search' },
              { text: 'Clear', type: 'danger', action: 'clear' }
            ]
          },
        ]}>
        {props.children}
      </InputForm>
    </div >
  );
}

export default App;