import React from 'react';
import './styles/index.scss';
import useCustomReducer from "common/useCustomReducer";
import FilterForm from './filter';
import CatalogForm from './catalog';
import config from './index.config';
import storeRedux from 'redux/store';
import pathName from 'routes/pathName';
import oProvider from 'providers/oprovider';
import getMenuAkses,{ getRoleAkses } from 'utils/getMenuAkses';
import Breadcrumb from 'components/Breadcrumb';
/* eslint-disable */

export const DataIndexContext = React.createContext();
export const MenuAksesContext = React.createContext([]);
export const RoleAksesContext = React.createContext([]);

const initialState = {
  filter : config.model,
  data : {},
  menuAkses : [],
  roleAkses : []
}


export default (props) => {
  const { digital : digitalPath } = pathName;
  const { scope,schemaFilter } = config;
  const [state, reducerFunc] = useCustomReducer(initialState);

  const onFilterChange = (values) => {
    reducerFunc('filter',values,'conventional');
  }

  React.useEffect(() => {
    async function fetchData(){
      try {
        const [
          { data : company },
          { data : kategori },
          { data : format },
          { data : status },
        ] = await Promise.all([
          oProvider.list('organization-group-dropdown?showAll=0'),
          oProvider.list('categories-dropdown'),
          oProvider.list('generic-lookup?lookup_name=FILETYPE'),
          oProvider.list('status-dropdown?process_name=CATALOG'),
        ]);
  
        company.unshift({ value: '', text: '-- Semua Institusi --' });
        kategori.unshift({ value: '', text: '-- Semua Kategori --' });
        format.unshift({ value: '', text: '-- Semua Format --' });
        if(!props.typeUnpublish){
          status.pop();
        }
        status.unshift({ value: '', text: '-- Semua Status --' });
        reducerFunc('data',{ company,kategori,format,status});
      } catch (error) {
        console.log(error?.message);
      }
    }
    fetchData();
  }, []);

  React.useEffect(() => {
    if(storeRedux.getState().menuAccess){
      let menuAccessFromLocalStorage = getMenuAkses(digitalPath.listEbook);
      reducerFunc('menuAkses',menuAccessFromLocalStorage,'conventional');
    }    
  },[storeRedux.getState().menuAccess]);

  React.useEffect(() => {
    if(storeRedux.getState().roleAkses){
      const roleAkses = getRoleAkses(digitalPath.listEbook);
      reducerFunc('roleAkses',roleAkses,'conventional');
    }    
  },[storeRedux.getState().roleAkses]);

  return (<>
    <DataIndexContext.Provider value={state.data}>
      <MenuAksesContext.Provider value={state.menuAkses}>
        <RoleAksesContext.Provider value={state.roleAkses}>
          <Breadcrumb items={scope.breadcrumb} />
          <FilterForm filter={state.filter} schema={schemaFilter} menuAkses={state.menuAkses} scope={scope} onFilterChange={onFilterChange} history={props.history}/>
          <CatalogForm filter={state.filter} />
        </RoleAksesContext.Provider>
      </MenuAksesContext.Provider>
    </DataIndexContext.Provider>
  </>);
};