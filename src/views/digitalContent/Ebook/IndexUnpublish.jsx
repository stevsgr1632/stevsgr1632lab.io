import React from 'react';
import IndexComponent from "./index";

const IndexPublish = (props) => <IndexComponent {...props} typeUnpublish={1} />;

export default IndexPublish;