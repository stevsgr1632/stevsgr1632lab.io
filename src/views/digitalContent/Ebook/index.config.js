import React from "react";
import { Button, Menu, Dropdown } from "antd";
import {PlusOutlined} from '@ant-design/icons';
import pathName from '../../../routes/pathName';
import { getOneMenu } from "../../../utils/getMenuAkses";
import { validURL } from "../../../utils/regex";
import ActionTableDropdown from "../../../components/ActionTableDropdown";
/* eslint-disable */

const {base:baseUrl,digital:digitalUrl} = pathName;
const actionMenu = (row, props) => {
  const { viewDetail,viewEdit,modalDelete, menuAksesContext,roleAksesContext } = props;
  const sunting = () => {
    viewEdit(row);
  };
  const hapus = () => {
    modalDelete(row);
  };

  const baca = () => {
    console.log(row);
    viewDetail(row);
  };

  return (
    <Menu>
      { getOneMenu(roleAksesContext,'Sunting') &&
        <Menu.Item>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={sunting} text="Sunting" />
        </Menu.Item>
      }
      { getOneMenu(roleAksesContext,'Hapus') && 
        <Menu.Item>
          <ActionTableDropdown type="link" style={{ margin: 0 }} onClick={hapus} text="Hapus" />
        </Menu.Item>
      }
      { getOneMenu(roleAksesContext,'readcontent') &&
        <Menu.Item>
            <Button type="link" style={{ margin: 0 }} onClick={baca}>Baca</Button>
        </Menu.Item>
      }
      <Menu.Item>
        <Button type="link" style={{ margin: 0 }} onClick={sunting}>
          Kode QR
        </Button>
      </Menu.Item>
    </Menu>
  );
};

// function toCurrency(numberString) {
//   let number = parseFloat(numberString);
//   return number.toLocaleString("USD");
// }

export default {
  scope: {
    title: "Buku Elektronik",
    subtitle: "Lakukan pilah untuk memudahkan pencarian",
    breadcrumb: [
      { text: "Beranda", to: baseUrl },
      { text: "Kontent Digital", to: digitalUrl.listEbook },
      { text: "Buku Elektronik" }
    ],
    toolbars: [
      {
        text: '', type: 'primary', icon: <PlusOutlined/>,
        onClick: (e) => {
          e.history.push(digitalUrl.listEbookAdd);
        }
      },
    ]
  },
  importFile: {
    title: "Import Data Produk",
    breadcrumb: [
      { text: "Beranda", to: baseUrl },
      { text: "Kontent Digital", to: digitalUrl.listEbook },
      { text: "Import Data Catalog" }
    ]
  },
  exportFile: {
    title: "Download Katalog",
    subtitle: "Transaksi Pengadaan Buku",
    breadcrumb: [
      { text: "Beranda", to: baseUrl },
      { text: "Kontent Digital", to: digitalUrl.listEbook },
      { text: "Export Katalog" }
    ],
  },
  add : {
    content : {
      title: "Tambah Buku Elektronik"
    },
    breadcrumb: [
      { text: "Beranda", to: baseUrl },
      { text: "Kontent Digital", to: digitalUrl.listEbook },
      { text: "Add Buku Elektronik" }
    ]
  },
  edit : {
    content : {
      title: "Edit Buku Elektronik"
    },
    breadcrumb: [
      { text: "Beranda", to: baseUrl },
      { text: "Kontent Digital", to: digitalUrl.listEbook },
      { text: "Edit Buku Elektroinik" }
    ]
  },
  data: {
    company: [{ value: "", text: "-- Semua Institusi --" }],
    penerbit: [{ value: "", text: "-- Semua Penerbit --" }],
    kategori: [{ value: "", text: "-- Semua Kategori --" }],
    format: [{ value: "", text: "-- Semua Format --" }],
    status: [{ value: "", text: "-- Semua Status --" }],
    tgl_upload: [
      { value: "", text: "-- Semua Waktu --" },
      { value: "hari", text: "Hari ini" },
      { value: "bulan", text: "Bulan ini" }
    ]
  },
  model: {
    company: "",
    penerbit: "",
    kategori: "",
    status: "",
    format: "",
    created_at: "",
    search: ""
  },
  schemaFilter:{
    company: { label: "Institusi" },
    penerbit: { label: "Penerbit" },
    kategori: { label: "Kategori" },
    status: { label: "Status" },
    catalog_isfree: { label: "Status Jual" },
    format: { label: "Format" },
    created_at: { label: "Tanggal Unggah" },
    search: { label: "Pencarian" },
  },
  schema: {
    perubahanaksi: { label: "Aksi" },
    uploadCatalogBulk: { label: "Nama File" },
    uploadCatalogBulkToAPI: { label: "Upload data" },
    catalog_cover : {label : "Berkas Sampul", rules :[
      { required: true, message: 'Silahkan upload cover atau sampul' },
    ]},
    catalog_file : {label : "Berkas Content", rules :[
      { required: true, message: 'Silahkan upload berkas' },
    ]},
    catalog_title : {label : "Judul", rules :[
      { required: true, message: 'Silahkan isi judul' },
      { max: 255, message: 'Panjang karakter tidak lebih dari 255' },
    ]},
    catalog_authors : {label : "Kepengarangan",rules:[
      { required: true, message: 'Silahkan isi Kepengarangan' },
    ]},
    catalog_isbn : {label : "ISBN",rules :[
      { required: true, message: 'Silahkan isi ISBN' },
      { pattern: /^(ISBN[-]*(1[03])*[ ]*(: ){0,1})*(([0-9Xx][- ]*){13}|([0-9Xx][- ]*){10})$/ , message: 'Masukkan sesuai format ISBN-10 dan ISBN-13' }
      // { pattern: /^(?:ISBN(?:-13)?:?\ *(97(?:8|9)([ -]?)(?=\d{1,5}\2?\d{1,7}\2?\d{1,6}\2?\d)(?:\d\2*){9}\d))|(?:ISBN(?:-10)?:?\ *((?=\d{1,5}([ -]?)\d{1,7}\2?\d{1,6}\2?\d)(?:\d\2*){9}[\dX]))$/, message: 'Masukkan sesuai format ISBN-10 dan ISBN-13' }
    ]},
    catalog_eisbn : {label : "E-ISBN",rules :[
      { required: true, message: 'Silahkan isi e-ISBN' },
      { pattern: /^(ISBN[-]*(1[03])*[ ]*(: ){0,1})*(([0-9Xx][- ]*){13}|([0-9Xx][- ]*){10})$/ , message: 'Masukkan sesuai format ISBN-10 dan ISBN-13' }
      // { pattern: /^(?:ISBN(?:-13)?:?\ *(97(?:8|9)([ -]?)(?=\d{1,5}\2?\d{1,7}\2?\d{1,6}\2?\d)(?:\d\2*){9}\d))|(?:ISBN(?:-10)?:?\ *((?=\d{1,5}([ -]?)\d{1,7}\2?\d{1,6}\2?\d)(?:\d\2*){9}[\dX]))$/, message: 'Masukkan sesuai format ISBN-10 dan ISBN-13' }
    ]},
    category_id : {label : "Kategori",rules :[
      { required: true, message: 'Silahkan pilih kategori' },
    ]},
    sub_category : {label : "Sub kategori - optional"},
    catalog_edition : {label : "Edisi - optional"},
    catalog_series : {label : "Seri - optional"},
    catalog_volume : {label : "Jilid - optional"},
    edu_level_id : {label : "Buku Pengayaan - optional"},
    catalog_language : {label : "Dalam Bahasa",rules :[
      { required: true, message: 'Silahkan pilih bahasa' },
    ]},
    catalog_result_object : {label : "Bentuk Karya",rules :[
      { required: true, message: 'Silahkan pilih bentuk karya' },
    ]},
    catalog_publish_date : {label : "Tahun Terbit Cetak",rules :[
      { required: true, message: 'Silahkan input tahun terbit cetak / hardcopy' },
    ]},
    catalog_epublish_date : {label : "Tahun Terbit Digital",rules :[
      { required: true, message: 'Silahkan input tahun terbit digital / softcopy' },
    ]},
    organization_id : {label : "Diterbitkan",rules :[
      { required: true, message: 'Silahkan pilih penerbit' },
    ]},
    catalog_publish_city : {label : "Kota Terbit"},
    catalog_source_product : {label : "Asal Produk",rules :[
      { required: true, message: 'Silahkan pilih asal produk' },
    ]},
    catalog_pages : {label : "Jumlah Halaman",rules :[
      { required: true, message: 'Silahkan input jumlah halaman' },
    ]},
    catalog_link : {label : "Tautan - Optional", rules : [
      { pattern: validURL, message: 'Format link yang benar : http://www.mocogawe.com' }
    ]},
    catalog_copyright : {label : "Hak Cipta - Optional"},
    catalog_target_market : {label : "Target Pembaca",rules :[
      { required: true, message: 'Silahkan pilih target pembaca sesuai kebutuhan' },
    ]},
    catalog_description:{
      text : "Deskripsi",
      rules:[
        { required: true, message: 'Silahkan input deskripsi' },
      ]
    },
    catalog_price:{label: "Harga Satuan",rules:[
      { required: true, message: 'Silahkan input harga' },
    ]},
    catalog_discount_percentage:{label: "Diskon (%) - optional"},
    catalog_effective_date:{label: "Masa Berlaku",rules:[
      { required: true, message: 'Silahkan pilih tanggal masa berlaku' },
    ]},
    catalog_isfree:{label: "Kategori harga katalog",rules:[
      { required: true, message: 'Silahkan pilih harga kategori' },
    ]},
    val_catalog_file: { label: "" },
    val_catalog_cover: { label: "" },
    val_catalog_title: { label: "" },
    val_catalog_isbn: { label: "" },
    val_catalog_eisbn: { label: "" },
    val_category_id: { label: "" },
    val_catalog_language: { label: "" },
    val_catalog_result_object: { label: "" },
    val_catalog_publish_date: { label: "" },
    val_catalog_epublish_date: { label: "" },
    val_organization_id: { label: "" },
    val_catalog_source_product: { label: "" },
    val_catalog_pages: { label: "" },
    val_catalog_target_market: { label: "" },
    catatan_validasi: { label: "Catatan - Optional" },
    catalog_sub_title : { label : 'Sub Title - Optional'}
  },
  catalog: props => {
    return {
      columns: [
        {
          title: "Judul Katalog",
          key: "metadata",
          className: "ebook-metadata",
          render: row => {
            return {
              props: {
                style: { verticalAlign: 'top' },
              },
              children: 
                  (
                      <span>
                        <p
                          onClick={() => props.viewDetail(row)}
                          style={{ color: "#007bff", cursor: "pointer" }}
                        >
                          {row.catalog_title}
                        </p>
                        <div>
                          ISBN: {row.catalog_isbn}
                        </div>
                        <div>
                          Kategori: {row.category_name}
                        </div>
                        <div>
                          Penulis: {row.catalog_authors}
                        </div>
                        <div>
                          Penerbit: {row.organization_name}
                        </div>
                      </span>
                  ) 
            }
          }
        },
        {
          title: "Harga",
          key: "catalog_price",
          render: row => {
            return {
              props: {
                style: { verticalAlign: 'top' },
              },
              children: (
                <span>
                  <div>{parseFloat(row.catalog_price).toLocaleString("id-ID")}</div>
                </span>
              )
            }
          }
        },
        {
          title: "Sampul",
          key: "sampul",
          render: row => (
            <span>
              <img
                src={row.catalog_cover}
                alt={row.catalog_cover}
                width={100}
              />
            </span>
          )
        },
        {
          title: "Status",
          width: 100,
          render: row => {
            return {
              props: {
                style: { verticalAlign: 'top' },
              },
              children: (
                <span>
                  <div>{row.status_name}</div>
                </span>
              )
            }
          }
          // dataIndex: "status_name"
        },
        {
          title: "Aksi",
          key: "action",
          width: 80,
          className: "text-center",
          render: row => {
            return {
              props: {
                style: { verticalAlign: 'top' },
              },
              children: (
                  <span>
                    <Dropdown overlay={actionMenu(row, props)}>
                      <Button
                        type="link"
                        style={{ margin: 0 }}
                        onClick={e => e.preventDefault()}
                      >
                        <i className="icon icon-options-vertical" />
                      </Button>
                    </Dropdown>
                  </span>
              )
            }
          }
        }
      ]
    };
  },
  exportCatalog:(...props) => {
    return {
      columns: [
        {
          title: "ISBN",
          dataIndex: "catalog_isbn",
          key: "catalog_isbn",
          render: row => {
            return (row) ? row : '-'
          }
        },
        {
          title: "Judul Buku",
          key: "catalog_title",
          dataIndex: "catalog_title",
          render: row => {
            return (row) ? row : '-'
          }
        },
        {
          title: "Kategori",
          key: "category_name",
          dataIndex: "category_name",
          render: row => {
            return (row) ? row : '-'
          }
        },
        {
          title: "Penerbit",
          key: "catalog_authors",
          dataIndex: "catalog_authors",
          render: row => {
            return (row) ? row : '-'
          }
        }
      ]
    };
  }
};
