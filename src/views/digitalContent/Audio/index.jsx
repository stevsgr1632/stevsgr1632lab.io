import React, { useState } from 'react';
import { Col, Row } from 'reactstrap';
import { Breadcrumb, Content } from '../../../components';
import InputText from '../../../components/ant/InputText';
import InputSelect from '../../../components/ant/InputSelect';
import InputTextArea from '../../../components/ant/InputTextArea';
import config from './index.config';
/* eslint-disable */

export default () => {
    const { scope, data } = config;
    const [model, setModel] = useState(config.model);

    const handleChange = (name, value) => {
        setModel({ ...model, [name]: value });
    }

    return (
        <React.Fragment>
            <Breadcrumb items={scope.breadcrumbs} />
            <Content {...scope}>
                <Row>
                    <Col lg="6" xl="4">
                        <InputSelect name="perusahaan" text="Perusahaan" data={data.perusahaan} onChange={handleChange} value={model.perusahaan} />
                    </Col>
                    <Col lg="6" xl="4">
                        <InputSelect name="penerbit" text="Penerbit" data={data.penerbit} onChange={handleChange} />
                    </Col>
                    <Col lg="6" xl="4">
                        <InputSelect name="kategori" text="Kategori" data={data.kategori} onChange={handleChange} />
                    </Col>
                    <Col lg="6" xl="4">
                        <InputSelect name="status" text="Status" data={data.status} onChange={handleChange} />
                    </Col>
                    <Col lg="6" xl="4">
                        <InputSelect name="format" text="Format" data={data.format} onChange={handleChange} />
                    </Col>
                    <Col lg="6" xl="4">
                        <InputSelect name="tanggal" text="Tanggal Unggah" data={data.tgl_upload} onChange={handleChange} />
                    </Col>
                    <Col lg="6" xl="4">
                        <InputText name="search" text="Search 1" placeholder="search here" onChange={handleChange} />
                    </Col>
                    <Col lg="6" xl="4">
                        <InputText text="&nbsp;" value={model.search} />
                    </Col>
                    <Col xl="12">
                        <InputTextArea name="info" text="Info" placeholder="Info here" rows="8" onChange={handleChange} value={model.info} />
                    </Col>
                </Row>
                <hr />
                <Row>
                    <Col>
                        <h3>Model Value</h3>
                        <pre>
                            {JSON.stringify(model, null, 4)}
                        </pre>
                    </Col>
                </Row>
            </Content>
        </React.Fragment>
    );
};