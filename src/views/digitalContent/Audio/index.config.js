export default {
    scope: {
        title: 'Filter Audio-Video Elektronik',
        subtitle: 'Lakukan pilah untuk memudahkan pencarian',
        breadcrumbs: [
            { text: 'Beranda', to: '/' },
            { text: 'Kontent Digital', to: '/digital/all' },
            { text: 'Audio-Video Elektroinik' },
        ]
    },
    model: {
        perusahaan: 'gramed',
        penerbit: '',
        info: 'no info'
    },
    data: {
        perusahaan: [
            { value: '', text: 'Semua Perusahaan', disabled: true },
            { value: 'apple', text: 'Apple' },
            { value: 'microsoft', text: 'Microsoft' },
            { value: 'gramed', text: 'Gramedia' },
        ],
        penerbit: [
            { value: '', text: 'Semua Penerbit', disabled: true },
            { value: 'gramed', text: 'Gramedia' },
            { value: 'apple', text: 'Apple' },
            { value: 'microsoft', text: 'Microsoft' },
        ],
        kategori: [
            { value: '', text: 'Semua Kategori', disabled: true },
            { value: 'agama', text: 'Agama' },
            { value: 'sosial', text: 'Sosial' },
            { value: 'bahasa', text: 'Bahasa' },
        ],
        status: [
            { value: '', text: 'Semua Status', disabled: true },
            { value: 'request', text: 'Request' },
            { value: 'verified', text: 'Verified' },
            { value: 'block', text: 'Terblokira' },
        ],
        format: [
            { value: '', text: 'Semua Format', disabled: true },
            { value: 'pdf', text: 'PDF' },
            { value: 'audio', text: 'Audio' },
            { value: 'video', text: 'Video' },
        ],
        tgl_upload: [
            { value: '', text: 'Semua', disabled: true },
            { value: 'hari', text: 'Hari ini' },
            { value: 'bulan', text: 'Bulan ini' },
        ],
    }
};