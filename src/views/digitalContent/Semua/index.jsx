import React from 'react';
import { Link } from 'react-router-dom';
import pathName from '../../../routes/pathName';
import { Breadcrumb, Content } from '../../../components';

const {base:baseUrl} = pathName;
const breadcrumbs = [
    { text: 'Beranda', to: baseUrl },
    { text: 'Kontent Digital' },
]

export default () => {
    return (
        <React.Fragment>
            <Breadcrumb items={breadcrumbs} />
            <Content>
                <ul>
                    <li><Link to="/digital/ebook">Ebook</Link></li>
                    <li><Link to="/digital/audio">Video / Audio</Link></li>
                    <li><Link to="/digital/journal">Journal</Link></li>
                </ul>
            </Content>
        </React.Fragment>
    );
};