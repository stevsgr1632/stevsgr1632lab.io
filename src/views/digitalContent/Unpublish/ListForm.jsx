import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { useHistory } from 'react-router-dom';
import queryString from "query-string";
import MessageInfo, { MessageContent } from '../../../common/message';
import { initialEndpoint } from '../../../utils/initialEndpoint';
import randomString from '../../../utils/genString';
import { getHeaders } from '../../../utils/request';
import MocoTable from '../../../components/MocoTable';
import oProvider from '../../../providers/oprovider';
import Action from './components/Action';
import config from './index.config';
import ModalForm from './ModalForm';
import { MenuAksesContext } from './index';
/* eslint-disable */

const ListForm = (props) => {
    const { filter } = props;
    let history = useHistory();
    const apiUrl = process.env.REACT_APP_BASE_URL;
    const menuAksesContext = React.useContext(MenuAksesContext);
    const { list } = config;
    const [selectAll,setSelectAll] = useState({all: false ,value:0})
    const [selectedRowKeys, setselectedRowKeys] = useState([]);
    const [limit, setLimit] = useState(10);
    const [, setIsLoaded] = useState(false);
    const [modal, setModal] = useState({ visible: false });
    const [modalAksi, setModalAksi] = useState({ visible: false });
    const [selectAksi, setselectAksi] = React.useState('');
    const [statusCatalog, setstatusCatalog] = React.useState([]);
    const [modalTitle, setModalTitle] = React.useState('-');
    const [state, setState] = useState({
        loading : false,
        pagination: {
          defaultCurrent: 1,
          pageSize: 10,
          total: 0,
          showLessItems: true,
          showSizeChanger: true, 
        },
        dataSource: []
      });

    const handleSelectActionMove = (name,val,e) => {
        setModalTitle(e.children);
        setselectAksi(val);
    };

    const viewDetail = async (row) => {
        const { data } = await oProvider.list(`catalog-view-detail?id=${row.id}`);
        setModal({ model: data, visible: true });
      };

    const onSelectChange = (idSelected,e) => {
        setselectedRowKeys(idSelected);
        setModalAksi({...modalAksi, data: e });
    };

    const handleTableChange = (pagination, filter, sorter) => {
        setState(prev => ({ ...prev, pagination, filter, sorter }));
    };

    const onSelectAll = (selected, selectedRows, changeRows)=>{
        setSelectAll({all: selected ,value:state.pagination.total});
    }

    const rowSelection = {
        selectedRowKeys,
        onSelectAll:onSelectAll,
        onChange: onSelectChange,
        onSelect: (record, selected, selectedRows) => {
            setSelectAll({all: false ,value:state.pagination.total});
        }
    };

    const handleActionMove = e => {
        if(selectedRowKeys.length && selectAksi){
          if(selectAksi === 'export'){
            tempUpload(selectedRowKeys);
          } else {
            setModalAksi({...modalAksi, visible: true , aksi:selectAksi});
          }
          
        }else{
          MessageInfo({type : 'info', text:'Silahkan Pilih Katalog dan Aksi terlebih dahulu'});
        }
    };

    const tempUpload = async (catids) => {
        const url = `${apiUrl}/${initialEndpoint}/catalog-allocate-export`;
        const payloads = { catalogIds : catids.join(',') }
        const key = randomString(17);
        MessageContent({ type :'loading', content:'Loading ... ',key, duration: 0 });
        try {
          const response = await Axios.post(url, { ...payloads } ,{ headers : getHeaders() });
    
          if(response){
            MessageContent({type : 'success', content:'Konten berhasil ditambah ke temporary export',key,duration:2});
          }
        } catch (error) {
          if(error.response){
            const { data, status } = error.response;
            MessageContent({type : 'error', content:'Terjadi kesalahan saat proses data',key,duration:2});
            const errorResponse = {
              code : status,
              message : (data.message) ? data.message : data
            }
            Notification({response:errorResponse, type : 'error'});
          }
          return false;
        }
    }

    const onShowSizeChange = (current, pageSize) => {
        setLimit(pageSize);
    }

    const refreshCatalog = async () => {
        setState(prev => ({ ...prev, loading: true }));
        let url = `&page=${state.pagination.current ?? state.pagination.defaultCurrent}`;
        const paramsObj = {};
        paramsObj['limit'] = limit;
        paramsObj['unpublish'] = 1;
        Object.keys(filter).forEach(x => {
          switch (x) {
            case 'company':
              paramsObj['organizationId'] = filter[x];
              break;
            case 'penerbit':
              paramsObj['publisherId'] = filter[x];
              break;
            case 'status':
              paramsObj['statusId'] = filter[x];
              break;
            case 'format':
              paramsObj['status'] = filter[x];
              break;
            case 'created_at':
              paramsObj['start_date'] = filter[x][0];
              paramsObj['end_date'] = filter[x][1];
              break;
            default:
              paramsObj[x] = filter[x];
              break;
          }
        });
    
        try {
          const params = queryString.stringify(paramsObj, {skipEmptyString : true});
          const response = await oProvider.list(`catalog?${params}${url}`);
          if(response){
            const { meta, data } = response;
            const pagination = { ...state.pagination, 
              pageSize: limit, 
              total: parseInt(meta.total), 
              onShowSizeChange:onShowSizeChange, 
              position: ['topRight', 'bottomRight'] 
            };
            setState(prev => ({ ...prev, pagination, dataSource: data, loading: false }));
            setstatusCatalog(meta.status_catalog);
            setIsLoaded(true);
          }
          
        } catch (error) {
          console.log(error?.message);
          setState(prev => ({ ...prev, loading: false }));
          setIsLoaded(false);
          return false;
        }
      };

    useEffect(()=>{
        refreshCatalog();
    },[limit,state.pagination.current,Object.keys(filter).map(x => filter[x]).join()]);

    const propsActionSelect = {handleSelectActionMove,handleActionMove,selectedRowKeys};

    return (
        <div style={{marginTop:'20px'}}>
            <Action {...propsActionSelect} />
            <hr/>
            <div className="table-catalog">
                <span style={{margin:"1em 0 1em 0",float:'left'}}>
                    {(selectAll.all) ? selectAll.value : (selectedRowKeys.length && selectedRowKeys.length) } item dipilih
                </span>
                <MocoTable
                {...state}
                rowSelection={rowSelection} 
                onChange={handleTableChange}
                {...list.config({ viewDetail, menuAksesContext })}
                />
            </div>
            <ModalForm {...modal} setModal={setModal} viewDetail={viewDetail}/>
        </div>
    )
}

export default ListForm
