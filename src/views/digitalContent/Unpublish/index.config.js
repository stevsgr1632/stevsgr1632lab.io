import React from 'react';
import pathName from '../../../routes/pathName';
/* eslint-disable */

const {base:baseUrl,digital:digitalUrl} = pathName;

export default {
    list : {
        title: "Buku Elektronik",
        subtitle: "Lakukan pilah untuk memudahkan pencarian",
        breadcrumb: [
            { text: "Beranda", to: baseUrl },
            { text: "Konten Digital", to: digitalUrl.listUnpublish },
            { text: "Konten Ditarik" }
        ],
        config : props => {
            return {
                columns: [
                  {
                    title: "Judul Katalog",
                    key: "metadata",
                    className: "ebook-metadata",
                    render: row => {
                      return {
                        props: {
                          style: { verticalAlign: 'top' },
                        },
                        children: 
                            (
                                <span>
                                  <p
                                    onClick={() => props.viewDetail(row)}
                                    style={{ color: "#007bff", cursor: "pointer" }}
                                  >
                                    {row.catalog_title}
                                  </p>
                                  <div>
                                    ISBN: {row.catalog_isbn}
                                  </div>
                                  <div>
                                    Kategori: {row.category_name}
                                  </div>
                                  <div>
                                    Penulis: {row.catalog_authors}
                                  </div>
                                  <div>
                                    Penerbit: {row.organization_name}
                                  </div>
                                </span>
                            ) 
                      }
                    }
                  },
                  {
                    title: "Harga",
                    key: "catalog_price",
                    render: row => {
                      return {
                        props: {
                          style: { verticalAlign: 'top' },
                        },
                        children: (
                          <span>
                            <div>{parseFloat(row.catalog_price).toLocaleString("id-ID")}</div>
                          </span>
                        )
                      }
                    }
                  },
                  {
                    title: "Sampul",
                    key: "sampul",
                    render: row => (
                      <span>
                        <img
                          src={row.catalog_cover}
                          alt={row.catalog_cover}
                          width={100}
                        />
                      </span>
                    )
                  },
                  {
                    title: "Status",
                    width: 100,
                    render: row => {
                      return {
                        props: {
                          style: { verticalAlign: 'top' },
                        },
                        children: (
                          <span>
                            <div>{row.status_name}</div>
                          </span>
                        )
                      }
                    }
                  }, 
                ]
              };
        }
    },
    filter: {
        scope:{
            company: { label: "Institusi" },
            penerbit: { label: "Penerbit" },
            kategori: { label: "Kategori" },
            created_at: { label: "Tanggal Unggah" },
            search: { label: "Pencarian" },
        },
        model: {
            company: "",
            penerbit: "",
            kategori: "",
            created_at: "",
            search: ""
        }
    },
    exportFile: {
        title: "Download Katalog",
        subtitle: "Transaksi Pengadaan Buku (Unpublish)",
        breadcrumb: [
          { text: "Beranda", to: baseUrl },
          { text: "Kontent Digital", to: digitalUrl.listUnpublish },
          { text: "Export Unpublish" }
        ],
        list:(...props) => {
            return {
              columns: [
                {
                  title: "ISBN",
                  dataIndex: "catalog_isbn",
                  key: "catalog_isbn",
                  render: row => {
                    return (row) ? row : '-'
                  }
                },
                {
                  title: "Judul Buku",
                  key: "catalog_title",
                  dataIndex: "catalog_title",
                  render: row => {
                    return (row) ? row : '-'
                  }
                },
                {
                  title: "Kategori",
                  key: "category_name",
                  dataIndex: "category_name",
                  render: row => {
                    return (row) ? row : '-'
                  }
                },
                {
                  title: "Penerbit",
                  key: "catalog_authors",
                  dataIndex: "catalog_authors",
                  render: row => {
                    return (row) ? row : '-'
                  }
                }
              ]
            };
        }
    },
}