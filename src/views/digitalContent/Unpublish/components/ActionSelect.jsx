import React from 'react';
import { Button } from 'antd';
import InputSelect from '../../../../components/ant/InputSelect';
import { getOneMenu } from "../../../../utils/getMenuAkses";
/* eslint-disable */

const App = props => {
  const {handleSelectActionMove,handleActionMove,selectedRowKeys = [],menuAkses} = props;

  const actionChoices = (menuAkses) =>{
    let choices = [];

    if(getOneMenu(menuAkses,'Hapus')) choices.push({text:'Hapus Produk yang dipilih',value:'delete'});
    if(getOneMenu(menuAkses,'Sunting')) {
      choices.push({ text:'Publish Ulang', value:'terverifikasi'})
    }
    choices.push({ text:'Tambahkan ke Temporary Export', value:'export'});
    return choices
  }

  return(<>
  <div style={{display:'grid',gridTemplateColumns:'3fr 1fr',gridColumnGap:'0.5em'}}>
    <InputSelect name="action-move-catalog" data={[...actionChoices(menuAkses)]} placeholder="Pilih Aksi" onChange={handleSelectActionMove} />
    <Button type="primary" onClick={handleActionMove} disabled={!selectedRowKeys > 0} style={{width:'100px'}}>
      Pilih
    </Button>
  </div>
  </>);
}

export default App;