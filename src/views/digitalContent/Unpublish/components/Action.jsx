import React from 'react';
import ActionSelect from './ActionSelect';
import { MenuAksesContext } from '../index';
import { useHistory } from 'react-router-dom';
import { Row,Col,Button,Tooltip } from "antd";
import pathName from '../../../../routes/pathName';
import { DownloadOutlined } from '@ant-design/icons';
/* eslint-disable */

export default props => {
  const menuAksesContext = React.useContext(MenuAksesContext);
  let history = useHistory();

  return(
    <div>
      <Row gutter={[0,0]}>
        <Col span={8}>
          <ActionSelect {...props}  menuAkses={menuAksesContext}/>
        </Col>
        <Col span={6} offset={6}>
          <Tooltip title="ekspor">
            <Button 
              type="primary" 
              icon={<DownloadOutlined />} 
              onClick={() => history.push(pathName.digital.listUnpublishExport)} 
              style={{marginRight:'0.5em',background:'#4DBD74',width:'60px'}}/>
          </Tooltip>
        </Col>
      </Row>
    </div>)
}