import React, { Fragment, useState, useEffect } from 'react';
import axios from 'axios';
import {FileExcelOutlined, 
  DeleteOutlined,
  ExclamationCircleOutlined 
} from '@ant-design/icons';
import { Modal } from 'antd';
import config from './index.config';
import pathName from '../../../routes/pathName';
import randomString from "../../../utils/genString";
import oProvider from '../../../providers/oprovider';
import {MessageContent} from "../../../common/message";
import Notification from '../../../common/notification';
import { initialEndpoint } from '../../../utils/initialEndpoint';
import LocalStorageService from "../../../utils/localStorageService";
import { Content, Breadcrumb, MocoTable } from '../../../components';
/* eslint-disable */

const url = `${process.env.REACT_APP_BASE_URL}/${initialEndpoint}`;

const ExportFileCatalog = (props) => {
    const { exportFile } = config;
    const [state, setState] = useState({
        loading:false,
        pagination: {
            defaultCurrent : 1,
            pageSize: 10,
            total: 0,
        },
        dataSource : []
    });

    const toolbars = [
        {text: '', type: 'primary', icon: <DeleteOutlined />,tooltip:'Clear',
          onClick: (e) => {
            deleteTemp();
          }
        },
        {text: '', type: 'primary', icon: <FileExcelOutlined/>, tooltip:'Download',
        onClick: (e) => {
          downloadFile();
        }
      }
    ]

    const getHeaders = () => {
        const { attributes } = LocalStorageService.state_auth.getAccessToken();
        const headers = {
            'Accept': 'application/vnd.api+json',
            'Content-Type': 'application/vnd.api+json',
            'Access-Control-Allow-Origin': true
        }
    
        if (attributes && attributes.accessToken) {
            headers.Authorization = `Bearer ${attributes.accessToken}`;
        }
    
        return headers;
    }

    const doDeleteTemp = async () => {
        try {
            const response = await oProvider.delete('catalog-download-selection-del');
            if(response){
                await fetchData();
                const errorResponse = {
                    code : 'Success',
                    message : 'Temporary download berhasil dikosongkan'
                  }
                Notification({response:errorResponse, type : 'info'});
                props.history.push(pathName.digital.listUnpublish);
            }
        } catch (error) {
            if(error.response){
                const { data, status } = error.response;
                const errorResponse = {
                  code : status,
                  message : (data.message) ? data.message : data
                }
                Notification({response:errorResponse, type : 'error'});
            }
            return false;
        }
    }

    const deleteTemp = () => {
        if(state.dataSource.length > 0){
            Modal.confirm({
                icon: <ExclamationCircleOutlined />,
                content: 'Yakin kosongkan temporary download?',
                okText: 'Ok',
                cancelText: 'Cancel',
                onOk(){
                    doDeleteTemp();
                }
            })
        } else {
            let key = randomString(17);
            MessageContent({ type :'info', content: 'Data temporary export kosong !', key, duration: 1 });
        }
        
        
    }

    const downloadFile = async ()=> {
        try {
            let key = randomString(17);
            MessageContent({ type :'loading', content:'Mengunduh ... ',key, duration: 0 });

            const response = await axios({
                headers: getHeaders(),
                url: `${url}/catalog-download-selection-excel`,
                method: 'GET',
                responseType: 'blob', // important
            })

            if(response){
                MessageContent({ type :'success', content: 'Selesai!', key, duration: 1 });
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', 'Catalog-selection.xls'); //or any other extension
                document.body.appendChild(link);
                link.click();
            }
    
        } catch (error) {
            if(error.response){
                const { data, status } = error.response;
                const errorResponse = {
                  code : status,
                  message : (data.message) ? data.message : data
                }
                Notification({response:errorResponse, type : 'error'});
            }
            return false;
        }
    }

    const handleTableChange = (pagination, filter, sorter) => {
        setState((state) => ({ ...state, pagination, filter, sorter }));
    };


    const fetchData = async () => {
        setState({...state,loading:true});
        try {
            const response = await oProvider.list('catalog-download-selection-get');
            if(response){
                const { data } = response
                const pagination = {...state.pagination, total:data.length};
                setState({...state,dataSource:data,pagination,loading:false});
            } else {
                setState({...state,loading:false});
            }
            
        } catch (error) {
            if(error.response){
                const { data, status } = error.response;
                const errorResponse = {
                  code : status,
                  message : (data.message) ? data.message : data
                }
                Notification({response:errorResponse, type : 'error'});
            }
            setState({...state,loading:false});
            return false;
        }
    }

    useEffect(()=>{
        fetchData();
    },[]);

    return (
        <Fragment>
            <Breadcrumb items={exportFile.breadcrumb} />
            <Content {...exportFile} toolbars={toolbars}>
                <MocoTable 
                    {...state}
                    onChange={handleTableChange}
                    {...exportFile.list(props)}
                />
            </Content>
        </Fragment>
    )
}

export default ExportFileCatalog
