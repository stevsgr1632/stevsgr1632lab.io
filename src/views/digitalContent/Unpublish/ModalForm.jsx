import React, { useState } from 'react';
import moment from 'moment';
import { MenuAksesContext } from './index';
import { useHistory } from 'react-router-dom';
import pathName from '../../../routes/pathName';
import { Modal, Row, Col , Menu,Button,Dropdown } from 'antd';
/* eslint-disable */
const { digital : { readEbook } } = pathName;
const styles = {
  tabSelected: {
    borderBottom: '3px solid #cc0000',
  },
  tabNotSelected: {
    borderBottom: '1px solid #cccccc',
    cursor: 'pointer',
  }
};

const actionMenu = (row,menuAksesContext,history,prop) => {
  const { viewEdit } = prop;
  const sunting = () => {
    viewEdit(row);
  };

  const baca = () => {
    history.push(readEbook(row.id));
  };

  return (
    <Menu>
      <Menu.Item>
        <Button type="link" style={{ margin: 0 }} onClick={baca}>
          PDF Reader
        </Button>
      </Menu.Item>
      <Menu.Item>
        <Button type="link" style={{ margin: 0 }} onClick={sunting}>
          Kode QR
        </Button>
      </Menu.Item>
    </Menu>
  );
};

const App = (props) => {
  let history = useHistory();
  const menuAksesContext = React.useContext(MenuAksesContext);
  const { model = {}, visible, setModal } = props;
  const [tabIdx, setTabIdx] = useState(0);

  return (
    <Modal
      width={800}
      visible={visible}
      onCancel={() => setModal({ visible: false })}
      footer={null}
    >
      <h1>Detail Produk</h1>
      <Row gutter={24}>
        <Col span={8}>
          <img src={model.catalog_cover} alt={model.catalog_cover} width={'100%'} />
        </Col>
        <Col span={14}>
          <div>
            <div><strong>{model.catalog_title}</strong></div>
            <div className="mt-3">Kepengarangan</div>
            <div><strong>{model.catalog_authors}</strong></div>
            <div className="mt-3">Kategori</div>
            <div><strong>{model.category_name}</strong></div>
            <div className="mt-3">Ditambahkan pada</div>
            <div><strong>{moment(model.created_at).format('DD MMMM YYYY, HH:mm')}</strong></div>
            <div className="mt-3">Harga</div>
            <div><strong>{parseInt(model.catalog_price).toLocaleString("id-ID")}</strong></div>
          </div>
        </Col>
        <Col span={2}>
          <span id='menu-container'>
            <Dropdown 
              overlay={()=>actionMenu(model,menuAksesContext,history,props)} 
              onVisibleChange={true} 
              getPopupContainer={()=>document.getElementById('menu-container')} 
              placement="bottomRight">
              <Button
                type="link"
                style={{ margin: 0 }}
                onClick={e => e.preventDefault()}
              >
                  <i className="icon icon-options-vertical" />
              </Button>
            </Dropdown>
          </span>
        </Col>
      </Row>
      <div className="mt-5"></div>
      <Row>
        <Col span={12} style={tabIdx === 0 ? styles.tabSelected : styles.tabNotSelected} onClick={() => setTabIdx(0)}>
          <h5>Sinopsis</h5>
        </Col>
        <Col span={12} style={tabIdx === 1 ? styles.tabSelected : styles.tabNotSelected} onClick={() => setTabIdx(1)}>
          <h5>Detail</h5>
        </Col>
      </Row>

      <div style={{ display: tabIdx === 0 ? 'block' : 'none',height:'50vh',overflowY:'scroll' }}>
        <div style={{ padding: '10px 0' }}>
          {<div dangerouslySetInnerHTML={{ __html: model.catalog_description }} /> || '-'}
        </div>
      </div>
      <div style={{ display: tabIdx === 1 ? 'block' : 'none' }}>
        <div style={{ padding: '10px 0' }}>
          <Row>
            <Col span={12}>
              <div className="mt-3">SKU</div>
              <div><strong>{(model.catalog_sku) ? model.catalog_sku : '-'}</strong></div>
              <div className="mt-3">Asal kota penerbit</div>
              <div><strong>{(model.catalog_publish_city) ? model.catalog_publish_city : '-'}</strong></div>
              <div className="mt-3">ISBN</div>
              <div><strong>ISBN : {(model.catalog_isbn)?model.catalog_isbn: '-'}</strong></div>
              <div><strong>EISBN : {(model.catalog_eisbn) ? model.catalog_eisbn : '-'}</strong></div>
              {/* <div><strong>EISSN : {(model.catalog_eissn) ? model.catalog_eissn : '-'}</strong></div> */}
              <div className="mt-3">Jumlah Halaman</div>
              <div><strong>{(model.catalog_pages) ? model.catalog_pages : '0' }</strong></div>
              <div className="mt-3">Produk pengayaan</div>
              <div><strong>{(model.catalog_pengayaan) ? model.catalog_pengayaan : '-' }</strong></div>
              <div className="mt-3">Seri</div>
              <div><strong>{(model.catalog_series) ? model.catalog_series : '-' }</strong></div>
              <div className="mt-3">Edisi</div>
              <div><strong>{(model.catalog_edition) ? model.catalog_edition : '-' }</strong></div>
              
            </Col>
            <Col span={12}>
              <div className="mt-3">Sub Kategori</div>
              <div><strong>{(model.sub_category || [])[0]}</strong></div>
              <div className="mt-3">Bahasa</div>
              <div><strong>{model.catalog_language || '-'}</strong></div>
              <div className="mt-3">Tahun terbit</div>
              <div><strong>Terbit Cetak : {model.catalog_publish_date || '-'}</strong></div>
              <div><strong>Terbit digital : {model.catalog_epublish_date || '-'}</strong></div>
              <div className="mt-3">Masa berlaku harga</div>
              <div><strong>{(model.catalog_prices?.catalog_effective_date) ?  moment(model.catalog_prices?.catalog_effective_date).format('DD-MM-YYYY')  : '-'}</strong></div>
              <div className="mt-3">Bentuk karya</div>
              <div><strong>{model.bentuk_karya || '-'}</strong></div>
              <div className="mt-3">Jilid</div>
              <div><strong>{model.catalog_jilid || '-'}</strong></div>
              <div className="mt-3">Jenis dan ukuran berkas</div>
              <div><strong>{model.catalog_file_ext || '-'} : {model.catalog_file_size || '0'} MB  </strong></div>
            </Col>
          </Row>
        </div>
      </div>
    </Modal >
  );
}

export default App;