import React, { useEffect, useState } from 'react';
import Breadcrumb from '../../../components/Breadcrumb';
import oProvider from '../../../providers/oprovider';
import storeRedux from '../../../redux/store';
import pathName from '../../../routes/pathName';
import getMenuAkses from '../../../utils/getMenuAkses';
import './styles/index.style.scss';
import FilterForm from './FilterForm';
import config from './index.config';
import ListForm from './ListForm';
/* eslint-disable */

export const DataIndexContext = React.createContext();
export const MenuAksesContext = React.createContext([]);

const Unpublish = (props) => {
    const { list,filter } = config;
    const [filterData, setFilterData] = useState(config.filter.model);
    const [data, setData] = useState({});
    const { digital : digitalPath } = pathName;
    const [menuAkses, setmenuAkses] = useState({});

    const fetchData = async () => {
        try {
          const [
            { data : company },
            { data : kategori }
          ] = await Promise.all([
            oProvider.list('organization-group-dropdown?showAll=0'),
            oProvider.list('categories-dropdown')
          ]);
          
          company.unshift({ value: '', text: '-- Semua Institusi --' });
          kategori.unshift({ value: '', text: '-- Semua Kategori --' });   
          setData({...data,company,kategori});
        } catch (error) {
          console.log(error?.message);
        }
    }

    const onFilterChange = (values) => {
        setFilterData(values);
    }

    useEffect(()=>{
        console.log('filterData',filterData);
    },[filterData]);

    useEffect(()=>{
        fetchData();
    },[]);

    React.useEffect(() => {
        if(storeRedux.getState().menuAccess){
          let menuAccessFromLocalStorage = getMenuAkses(digitalPath.listEbook);
          setmenuAkses(menuAccessFromLocalStorage);
        }    
      },[storeRedux.getState().menuAccess]);

    return (
        <DataIndexContext.Provider value={data}>
            <MenuAksesContext.Provider value={menuAkses}>
                <Breadcrumb items={list.breadcrumb} />
                <FilterForm filter={filterData} schema={filter.scope} scope={list} onFilterChange={onFilterChange}>
                    <ListForm filter={filterData} />
                </FilterForm>
            </MenuAksesContext.Provider>
        </DataIndexContext.Provider>
    )
}

export default Unpublish;
