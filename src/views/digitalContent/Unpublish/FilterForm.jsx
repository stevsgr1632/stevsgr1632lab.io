import React, { useEffect,useState } from 'react';
import { Form } from 'antd';
import { InputForm } from '../../../components';
import { DataIndexContext } from './index'; 
import oProvider from '../../../providers/oprovider';
/* eslint-disable */

const FilterForm = (props) => {
    const [form] = Form.useForm();
    const dataFromIndex = React.useContext(DataIndexContext);
    const [filter,setFilter] = useState(props.filter);
    const [ loading, setLoading] = useState({ company: true, kategori: true,penerbit:true});
    const [ data, setData ] = useState({});

    const initialized = () => {
        setData({...data,...dataFromIndex});
        setLoading({});
    }

    const clearFilter = () => {
        const clearFilter = { company: '', penerbit: '', kategori: '', search: '' , created_at: ["",""]};
        props.schema.form.setFieldsValue(clearFilter);
        setFilter(clearFilter);
    }

    const onFilterChange = (name,value) => {
        switch (name) {
            case 'company':
              setLoading({ penerbit: true });
              try {
                oProvider.list(`organization-dropdown?id=${value}`).then(({ data: list }) => {
                  list.unshift({ value: '', text: '-- Semua Penerbit --' })
                  setData({ ...data, penerbit: list });
                  setLoading({ penerbit: false });
        
                  if (props.schema) {
                    setTimeout(() => {
                      const { form } = props.schema;
                      form.setFieldsValue({ penerbit: '' });
                    }, 400);
                  }
                });
              } catch (error) {
                console.log(error?.message);
                setLoading({ penerbit: false });
                return false;
              }
              break;
            case 'action':
              if (value === 'clear') {
                clearFilter();
              }
              if (value === 'search') {
      
                setFilter({ ...filter, [value]: props.schema.form.getFieldValue(value) });
              }
              break;
            default:
              break;
          }
          
        if (name !== 'action' && name !== 'search') {
            if(name === 'created_at'){
                if(value[0].length > 0 && value[1].length >0){
                    setFilter({ ...filter,[name]:value});
                } 
                if(value[0].length < 1 && value[1].length < 1){
                    setFilter({ ...filter,[name]:value});
                }
            } else if(Object.keys(props.schema).includes(name)){
                setFilter({ ...filter, [name]: value });
            }
        }
        
        if(name === 'search' && value ===''){
            setFilter({ ...filter, [name]: '' });
        }
    }

    const onEnterSearch = (e) => {
        setFilter({ ...filter, [e.target.name]: e.target.value });
    }

    const onSearchChange = (name,value,e) =>{
        console.log(name,value,e);
    }

    useEffect(() => {
        console.log('filter',filter);
        props.onFilterChange(filter);
    }, [filter]);

    useEffect(()=>{
        initialized();
    },[dataFromIndex]);

    return (
        <InputForm {...props}
            form={form}
            defCol={{ md: 12, lg: 12, xl: 8 }}
            onChange={onFilterChange}
            controls={[
                { type: 'select', name: 'company', data: data.company, loading: loading.company, others:{showSearch:true} },
                { type: 'select', name: 'penerbit', data: data.penerbit, loading: loading.penerbit, others:{showSearch:true} },
                { type: 'select', name: 'kategori', data: data.kategori, loading: loading.kategori, others:{showSearch:true} },
                { type: 'rangepicker', name: 'created_at' , style:{width:'100%'}},
                { type: 'text', name: 'search', onPressEnter:onEnterSearch},
                {
                  type: 'action', name: 'action', defcol: { md: 8 }, useLabel: true, actions: [
                    { text: 'Pencarian', type: 'primary', style: { marginRight: 5 }, action: 'search' },
                    { text: 'Clear', type: 'danger', action: 'clear' }
                  ]
                },
              ]}>
            {props.children}
        </InputForm>
    )
}

export default FilterForm
