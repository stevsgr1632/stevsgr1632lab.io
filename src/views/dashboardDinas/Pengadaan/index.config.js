import React from 'react';
import pathName from 'routes/pathName';
import { noImagePath,
  numberFormat,
  formDateDisplayValue,
} from "utils/initialEndpoint";
/* eslint-disable */

const { initial, dashboardDinas } = pathName;

export default {
  breadcrumb : [
    { text: 'Beranda', to: initial },
    { text: 'Dashboard', to: dashboardDinas.main },
    { text: 'Pengadaan' },
  ],
  content: {
    title: '',
    subtitle: '',
  },
  form_list : {
    config : (...props) => {
      return {
        columns : [
          {
            title:'Tangggal',
            dataIndex:'transaction_date',
            key:'transaction_date',
            render: x => x ? formDateDisplayValue(x,'DD-MM-YYYY') : '-' 
          },
          {
            title:'Nomor',
            dataIndex:'transaction_number',
            key:'transaction_number',
            render: x => x ? x : '-'
          },
          {
            title:'Jumlah',
            dataIndex:'transaction_amount',
            align:'right',
            key:'transaction_amount',
            render: x => x ? numberFormat(x) : '-'
          }
        ]
      }
    }
  },
  category_list : {
    config : (...props) => {
      return {
        columns : [
          {
            title:'Nama Kategori',
            dataIndex:'category_name',
            key:'category_name',
            render: (x,row) => {
              return {
                props: {
                  style: {},
                },
                children:   
                  <div style={{verticalAlign:'middle'}}>
                    <img 
                      alt='example' 
                      src={(row.image)? row.image : noImagePath } 
                      style={{borderRadius:'50%',
                        border:'1px solid #c8ced3',
                        width:'50px',
                        height:'50px',
                        marginRight:'20px'}}
                    />

                    <p style={{margin:'auto',display:'inline-block'}}>{(x) ? x : '-'}</p>
                  </div>
              };
            }
          },
          {
            title:'Buku',
            dataIndex:'bookQty',
            key:'bookQty',
            align:'right',
            render: x => x ? numberFormat(x) : '-'
          },
          {
            title:'Copy',
            dataIndex:'catalogQty',
            align:'right',
            key:'catalogQty',
            render: x => x ? numberFormat(x) : '-'
          }
        ]
      }
    }
  },
  book_list : {
    config : (...props) => {
      return {
        columns : [
          {
            title:'Judul Buku',
            dataIndex:'catalog_name',
            key:'catalog_name',
            render: x => x ? x : '-',
          },
          {
            title:'Copy',
            dataIndex:'catalog_copy',
            key:'catalog_copy',
            align:'right',
            render: x => x ? numberFormat(x) : '-',
          },
          {
            title:'Harga',
            dataIndex:'catalog_price',
            key:'catalog_price',
            align:'right',
            render: x => x ? numberFormat(x) : '-',
          }
        ]
      }
    }
  }
    
}