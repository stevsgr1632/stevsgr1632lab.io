import React from 'react';
import CardContent from '../components/CardContent';
import MocoTable from 'components/MocoTable';
/* eslint-disable */

export default (props) => {
  const { data , columns } = props;
  const [state, setState] = React.useState({
    loading : false,
    pagination: {
      defaultCurrent: 1,
      pageSize: 5,
      total:  0 ,
    },
    dataSource: []
  });

  React.useEffect(()=>{
    setState(prev => ({...prev,loading:data.loading}));
    if(data){
      const pagination = {...state.pagination, total:data.catData.length}
      setState(prev => ({...prev,dataSource:data.catData,loading:data.loading}));
    }
  },[data]);
  return (
    <CardContent style={{borderRadius:'10px',border:'1px solid #c5c5c5',height:'500px'}}>
      <h4>Kategori Buku</h4>
      <div>
        <MocoTable
          {...columns.config(props)}
          {...state}
          className='category-list'
        />
      </div>
    </CardContent>
  )
};
