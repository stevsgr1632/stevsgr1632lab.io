import React from 'react';
import { Col,Row,Radio } from 'antd';
import CardContent from '../components/CardContent';
import MocoBarChart from 'components/MocoBarChart';
import useCustomReducer from "common/useCustomReducer";
/* eslint-disable */
const colorChart = {
  cr1 : '#003366',
  cr2 : '#fe9f15',
}
const initialData = {
  radio : 1,
  state : [],
  barConfig : {
    XAxisKey : 'publisher',
    bar : [{dataKey:'amount', fill:colorChart.cr1}],
    legendData: [{value:'Nominal Pengadaan',type:'rect' ,id: 'amount',color:colorChart.cr1}]
  }
};

export default (props) => {
  const { dataBar, onClick } = props;
  const [dataReducer,reducerFunc] = useCustomReducer(initialData);

  const handleChange = e =>{
    reducerFunc('radio',e.target.value,'conventional');
  }

  React.useEffect(()=>{
    let legenDataAPI,barData;
    if(dataReducer?.radio===1){
      legenDataAPI = [{value: 'Nominal Pengadaan',type:'rect' ,id: 'amount',color:colorChart.cr1}];
      barData = [{dataKey:'amount', fill:colorChart.cr1}]
    }
    if(dataReducer?.radio===2){
      legenDataAPI = [{value: 'Kuantiti Pengadaan',type:'rect' ,id: 'catalogcopy',color:colorChart.cr2}];
      barData = [{dataKey:'catalogcopy', fill:colorChart.cr2}]
    }
    reducerFunc('barConfig',{bar:barData,legendData:legenDataAPI});
    reducerFunc('state',dataBar,'conventional');
  },[dataReducer?.radio,dataBar]);

  return (
    <CardContent>
      <div style={{width:'100%',}}>
        <Row>
          <Col flex={10}>
            <h4>Grafik Pengadaan</h4>
          </Col>
          <Col flex={1}>
            <Radio.Group 
              onChange={handleChange} 
              value={dataReducer?.radio}
            >
              <Radio value={1}>Nominal</Radio>
              <Radio value={2}>Kuantiti</Radio>
            </Radio.Group>
          </Col>
        </Row>
      </div>
      <div style={{ width: '100%', height: '300px' }}>
        <MocoBarChart 
          data={dataReducer?.state} 
          config={dataReducer?.barConfig} 
          handleClick={onClick}
        />
      </div>
    </CardContent>
  )
};