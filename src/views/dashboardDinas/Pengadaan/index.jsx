import React from 'react';
import './style.scss';
import { Row, Col } from 'antd';
import ListForm from './ListForm';
import BookList from './BookList';
import config from './index.config';
import CategoryList from './CategoryList';
import oProvider from 'providers/oprovider';
import ChartPengadaan from './ChartPengadaan';
import { Breadcrumb,Content } from 'components';
import LoadingSpin from 'components/LoadingSpin';
import useCustomReducer from 'common/useCustomReducer';
/* eslint-disable */
const initialData = {
  loading : false,
  procurementData : [],
  chartProcurementData : [],
  categoryData : {
    loading: false,
    catData : []
  },
  bookData : {
    loading: false,
    books : []
  }
};

export default (props) => {
  const { match } = props;
  const { breadcrumb,content,form_list, category_list, book_list } = config;
  const [dataReducer, reducerFunc] = useCustomReducer(initialData);
  
  const handleChartClick = async (params) => {
    reducerFunc('categoryData',{loading:true});
    reducerFunc('bookData',{loading:true});
    const procCatalogData = await oProvider.list(`dashboard-proc-catalog-instansi?year=${match.params.year}&publisher_id=${params.organization_id}`);
    if(procCatalogData){
      const { data } = procCatalogData;
      reducerFunc('categoryData',{catData:data.categories,loading:false});
      reducerFunc('bookData',{books:data.books,loading:false});
    }else{
      reducerFunc('categoryData',{loading:false});
      reducerFunc('bookData',{loading:false});
    }
  }

  const fetchData = async () => {
    reducerFunc('loading',true,'conventional');
    const procData = await oProvider.list(`dashboard-procurement-instansi?year=${match.params.year}`);
    if(procData){
      const { chartProcurement , procurement} = procData.data;
      const procCatalogDataInit = await oProvider.list(`dashboard-proc-catalog-instansi?year=${match.params.year}`);
      
      const { data } = procCatalogDataInit;
      reducerFunc('categoryData',{catData:data.categories,loading:false});
      reducerFunc('bookData',{books:data.books,loading:false});
      reducerFunc('procurementData',procurement,'conventional');
      reducerFunc('chartProcurementData',chartProcurement,'conventional');
    }    
    reducerFunc('loading',false,'conventional');
  };

  React.useEffect(()=>{
    fetchData();
  },[]);

  const gutterRow = [16, { xs: 8, sm: 16, md: 24, lg: 24 }];
  const sizeCol = {xs : 24, sm: 24};
  return (
    <>
      <Breadcrumb items={breadcrumb}/>
      <Content {...content}>
        <LoadingSpin loading={dataReducer?.loading}>
          <Row gutter={gutterRow}>
            <Col {...sizeCol} md={10} lg={10} xl={10}>
              <ListForm columns={form_list} data={dataReducer?.procurementData}/>
            </Col>
            <Col {...sizeCol} md={14} lg={14} xl={14}>
              <ChartPengadaan dataBar={dataReducer?.chartProcurementData} onClick={handleChartClick}/>
            </Col>
          </Row>
          <Row gutter={gutterRow}>
            <Col {...sizeCol} md={12} lg={12} xl={12}>
              <CategoryList data={dataReducer?.categoryData} columns={category_list}/>
            </Col>
            <Col {...sizeCol} md={12} lg={12} xl={12}>
              <BookList data={dataReducer?.bookData} columns={book_list}/>
            </Col>
          </Row>
        </LoadingSpin>
      </Content>
    </>
  )
};