import React from 'react';
import MocoTable from 'components/MocoTable';
import CardContent from '../components/CardContent';
/* eslint-disable */

export default (props) => {
  const { data , columns } = props;
  const [state, setState] = React.useState({
    loading : false,
    pagination: {
      defaultCurrent: 1,
      pageSize: 10,
      total:  0 ,
    },
    dataSource: []
  });

  React.useEffect(()=>{
    if(data){
      const pagination = {...state.pagination, total:data.length}
      setState(prev => ({...prev,dataSource:data}));
    }
  },[data]);

  return (
    <CardContent>
      <MocoTable 
        {...columns.config(props)}
        {...state} 
        size='middle'
      />
    </CardContent>
  )
};