import React from 'react';
import { Card } from 'antd';
/* eslint-disable */

export default (props) => {
  return (
    <Card className="cardContent__wrapper" {...props}>
      {props.children}
    </Card>
  )
};