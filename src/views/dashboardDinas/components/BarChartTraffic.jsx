import React from 'react';
import CardContent from './CardContent';
import MocoComposedChart from './MocoComposedChart';
/* eslint-disable */

export default (props) => {
  const { dataBar : { data , legend }} = props;
  const [ barTraffic, setBarTraffic ] = React.useState({
    XAxisKey : 'year',
    bar : [
      {dataKey:'totalAll', fill:'#fe9f15',type:'Bar'},
      {dataKey:'total', fill:'#003366',type:'Bar'},
      {dataKey:'percentage', fill:'#8884d8',type:'Line'},
    ],
    legendData: [
      {value: '',type:'rect' ,id: 'totalAll',color:'#fe9f15'},
      {value: '',type:'rect' ,id: 'total',color:'#003366'},
      {value: '',type:'line' ,id: 'percentage',color:'#8884d8'}
    ]
  });

  React.useEffect(()=>{
    if(legend){
      const legenDataAPI = [
        {value: legend.totalAll,type:'rect' ,id: 'totalAll',color:'#fe9f15'},
        {value: legend.total,type:'rect' ,id: 'total',color:'#003366'},
        {value: legend.percentage,type:'line' ,id: 'percentage',color:'#8884d8'}
      ];

      setBarTraffic(prev => ({
        ...prev,
        legendData:legenDataAPI
      }))
    }
  
  },[data,legend]);

  return (
    <CardContent>
      <div className="charttraffic__title"><h4>Content Data Traffic - Repository</h4></div>
      <div className="charttraffic__wrapper">
        <MocoComposedChart data={data} config={barTraffic} />
      </div>
    </CardContent>
  )
};