import React from 'react'
import { Card, Row, Col } from 'antd';
import { noImagePath } from "utils/initialEndpoint";
/* eslint-disable */

export default (props) => {
    
  return (
    <>
      <Card  {...props} className="dashboardcontainer__wrapper">
        <Row gutter={[16,16]}>
          <Col className="dashboardcontainer__header">
            <div className="dashboardcontainer__header--logo">
              <img src={props.header?.logo ?? noImagePath} alt='logo' className="dashboardcontainer__header--logo-img"/>
            </div>
          </Col>
          <Col>
            <div className="dashboardcontainer__name">{props.header.name}</div>
          </Col>
        </Row>
        {props.children}
      </Card>
    </>
  )
};