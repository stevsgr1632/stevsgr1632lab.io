import React from 'react';
import pathName from 'routes/pathName';
import MocoBarChart from 'components/MocoBarChart';
import CardContent from './CardContent';
/* eslint-disable */

export default (props) => {
  const { dashboardDinas } = pathName;
  const { dataBar : { data , legend } } = props;
  const [ barConfig, setBarConfig] = React.useState({
    XAxisKey : 'year',
    bar : [
      {dataKey:'totalAll', fill:'#fe9f15'},
      {dataKey:'total', fill:'#003366'}
    ],
    legendData: [
      {value: '',type:'rect' ,id: 'totalAll',color:'#fe9f15'},
      {value: '',type:'rect' ,id: 'total',color:'#003366'}
    ]
  });

  const handleClick = (params) => {
    props.history.push(dashboardDinas.pengadaan(params.year));
  }

  React.useEffect(()=>{
    if(legend){
      const legenDataAPI = [
        {value: legend.totalAll,type:'rect' ,id: 'totalAll',color:'#fe9f15'},
        {value: legend.total,type:'rect' ,id: 'total',color:'#003366'}
      ];

      setBarConfig(prev => ({
        ...prev,
        legendData:legenDataAPI
      }));
    }
  },[data,legend]);
  
  return (
    <CardContent>
      <div className="chartpengadaan__title"><h4>Grafik Pengadaan</h4></div>
      <div className="chartpengadaan__wrapper">
        <MocoBarChart data={data} config={barConfig} handleClick={handleClick}/>
      </div>
    </CardContent>
  )
};