import React from 'react';
import MocoTable from 'components/MocoTable';
/* eslint-disable */

export default (props) => {
  const { data } = props;
  const [state, setState] = React.useState({
    loading : false,
    pagination: false,
    dataSource: []
  });

  const columns = [
    {
      title: 'ePustaka',
      dataIndex: 'epustaka_name',
      key: 'epustaka_name',
    },
    {
      title: 'Jml',
      dataIndex: 'total',
      key: 'total',
    },
    {
      title: '%',
      dataIndex: 'percentage',
      key: 'percentage',
    }
  ];

  React.useEffect(()=>{
    setState(prev => ({...prev,dataSource:data}))
  },[data]);

  return (
    <MocoTable
      columns = {columns}
      {...state}
    />
  )
};