import React from 'react';
import MocoPieChart from 'components/MocoPieChart';
/* eslint-disable */

export default (props) => {
  const { data } = props; 
  const colors = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042','#BC5C7C','#39158A','#FC6D03','#1CB866','#608427','#53869F'];

  return (
    <MocoPieChart data={data} dataKey='percentage' colors={colors} />
  )
};