import React from 'react';
import { Row, Col } from 'antd';
import CardContent from '../CardContent';
import ListComp from './ListComp';
import PieChartComp from './PieChartComp';
/* eslint-disable */

export default (props) => {
  const { listData } = props;
  
  return (
    <CardContent bodyStyle={{padding:'10px',height:'100%'}}>
      <Row className="epustakatbl__wrapper">
        <div className="epustakatbl__title"><h4>ePustaka - Content</h4></div>
        <Col xs={24} sm={14} md={14} lg={14} xl={14}>
          <ListComp data={listData}/>
        </Col>
        <Col xs={24} sm={10} md={10} lg={10} xl={10}>
          <div className="epustakatbl__chart">
            <PieChartComp data={listData}/>
          </div>
        </Col>
      </Row>
    </CardContent>
  )
};









