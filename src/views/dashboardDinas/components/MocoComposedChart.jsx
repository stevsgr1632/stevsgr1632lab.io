import React from 'react';
import { 
  ComposedChart, 
  CartesianGrid, 
  Tooltip, 
  ResponsiveContainer,
  XAxis,
  YAxis,
  Bar,
  Legend,
  Line,
  Area,
  LabelList
} from 'recharts';
/* eslint-disable */
 
export default (props) => {
  const { data, config } = props;
  const { XAxisKey , bar } = config;

  const CustomizedLabel = (props) => {
    const {x, y, stroke, value} = props;
    return <text x={x} y={y} dy={14} fill={stroke} fontSize={10} textAnchor="middle">{value}%</text>;
  }

  const CustomTooltip = (params) => {
    const { active, payload, label } = params;
    const { legendData } = config;
    if (active) {
      return (
        <div className="chartcomposed__tooltip">
          <div className="label">{`${label}`}</div>
          {
            (payload) ? (
              payload.map((x,idx) => {
                return (<div key={idx} style={{color:`${x.color}`}}>{`${legendData.filter( y => y.id===x.dataKey  )[0].value} : ${x.value}`}</div>)
              }) ) : "" 
          }
        </div>
      );
    }
  
    return null;
  };

  return (
    <>
      <ResponsiveContainer>
        <ComposedChart
          data={data}
          margin={{top: 30, right: 5, left: 0, bottom: 30}}
        >
          <CartesianGrid strokeDasharray="5 5" />
          <Tooltip content={CustomTooltip}/>
          <XAxis dataKey={XAxisKey} />
          <YAxis yAxisId="left"  tickCount={10}/> 
          <YAxis yAxisId="right" orientation='right' hide={true}/>
          <Legend payload={config.legendData} />
          {
            (bar) ? (
              bar.map((x, idx) => {
                if(x.type==="Bar"){
                  return (<Bar yAxisId="left" key={idx} dataKey={x.dataKey} fill={x.fill} >
                            <LabelList dataKey={x.dataKey} position="top" />
                          </Bar>)
                }else if(x.type==="Line"){
                  return (<Line yAxisId="right" key={idx} type="monotone" dataKey={x.dataKey} dot={{ fill: '#fe9f15', strokeWidth: 2 }} stroke={x.fill} label={<CustomizedLabel />} />)
                }else if(x.type==="Area"){
                  return (<Area type="monotone" dataKey={x.dataKey} fill={x.fill} stroke={x.fill} />)
                }else{
                  return (<Bar key={idx} dataKey={x.dataKey} fill={x.fill} />)
                }
              })
            ) : " " 
          }
        </ComposedChart>
      </ResponsiveContainer>
    </>
  )
};