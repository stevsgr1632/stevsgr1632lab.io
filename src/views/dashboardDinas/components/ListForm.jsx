import React from 'react';
import MocoTable from 'components/MocoTable';
import CardContent from './CardContent';
/* eslint-disable */

export default (props) => {
  const { data } = props
  const [state, setState] = React.useState({
      loading : false,
      pagination: false,
      dataSource: []
  });

  const columns = [
    {
      title: 'Klasifikasi',
      dataIndex: 'category_name',
      key: 'category_name',
    },
    {
      title: 'Jumlah',
      dataIndex: 'total',
      align:'right',
      key: 'total',
    }
  ];

  React.useEffect(()=>{
    setState(prev => ({...prev,dataSource:data}));
  },[data]);

  return (
    <CardContent>
      <MocoTable
        columns = {columns}
        {...state}
      />
    </CardContent>
  )
}