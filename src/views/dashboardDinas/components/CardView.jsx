import React from 'react';
import { Card} from 'antd';
import { numberFormat } from "utils/initialEndpoint";
/* eslint-disable */

export default (props) => {
  const { title, value, image } = props; 

  return (
    <Card className="cardview__wrapper" bodyStyle={{padding:'10px'}}>
      <div className="cardview__logo">
        <img alt={`${title}`} src={image} className="cardview__logo--img"/>
      </div>
      <div className="cardview__content">
        <div className="cardview__content--value">{value ? numberFormat(value) : '0'} </div>
        <div className="cardview__content--title">{title}</div>
      </div>
    </Card>
  )
};