import React from 'react';
import ListForm from './list';

const App = (props) => {
  return (
    <React.Fragment>
      <ListForm {...props} />
    </React.Fragment>
  );
}

export default App;