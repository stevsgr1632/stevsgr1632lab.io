import React from 'react';
import { Steps } from 'antd';
import config from './add.config';
import { Breadcrumb, Content, InputControl } from 'components';
/* eslint-disable */

const { Step } = Steps;

const App = (props) => {
  const { title, subtitle, breadcrumbs, headerStyle, data } = config.init(props);
  const [state, setState] = React.useState({ pengadaan: '-' });

  const handleImageboxChange = (value) => {
    console.log(value);
    setState({ ...state, pengadaan: value });
  }
  const handleImageboxChange2 = (value) => {
    console.log(value);
    setState({ ...state, pengadaan2: value });
  }

  return (
    <div>
      <Breadcrumb items={breadcrumbs} />
      <Content>
        <div style={headerStyle}>
          <div className="ant-row" style={{ display: 'flex' }}>
            <div className="ant-col" style={{ flex: '0 0 0', paddingTop: 24 }}>
              <i className="icon-plus" style={{ fontSize: 32, paddingLeft: 20, paddingRight: 20 }} />
            </div>
            <div className="ant-col" style={{ flex: '1 1 auto' }}>
              <div style={{ fontSize: 20, fontWeight: "bold", paddingTop: 12 }}>{title}</div>
              <div style={{ fontSize: 12, color: '#999' }}>{subtitle}</div>
            </div>
          </div>
          <hr />
          <div style={{ padding: 20 }}>
            <Steps current={0}>
              <Step title="Jenis Pengadaan" />
              <Step title="Jenis Produk" />
              <Step title="Detail Pengadaan" />
              <Step title="Pratinjau" />
            </Steps>
          </div>
          <hr />
          <div style={{ padding: 20, paddingTop: 0 }}>
            <div style={{ fontSize: 13, color: '#555' }}>Pilih salah satu jenis pengadaan yang akan digunakan</div>
            <div>
              <InputControl type="radio-image" items={data.pengadaan} onChange={handleImageboxChange} defaultValue={[0]} />
              <InputControl type="checkbox-image" items={data.pengadaan} onChange={handleImageboxChange2} defaultValue={[1, 2]} />
              <pre>
                {JSON.stringify(state, null, 4)}
              </pre>
            </div>
          </div>
        </div>
      </Content>
    </div>
  );
}

export default App;