import React from 'react';
import { Button } from 'antd';
import config from './list.config';
import {PlusOutlined} from '@ant-design/icons';
import { Breadcrumb, Content } from 'components';
/* eslint-disable */

const App = (props) => {
  const { breadcrumbs, filter } = config.init(props);

  const linkAdd = () => {
    props.history.push('/procurement/plan/add')
  }

  return (
    <div>
      <Breadcrumb items={breadcrumbs} />
      <Content>
        <div style={filter.headerStyle}>
          <div className="ant-row" style={{ display: 'flex' }}>
            <div className="ant-col" style={{ flex: '0 0 0', paddingTop: 24 }}>
              <i className="icon-drop" style={{ fontSize: 32, paddingLeft: 20, paddingRight: 20 }} />
            </div>
            <div className="ant-col" style={{ flex: '1 1 auto' }}>
              <div style={{ float: 'right', marginTop: 20, marginRight: 20 }}>
                <Button icon={<PlusOutlined/>} type="danger" shape="circle" size={'large'} onClick={linkAdd}></Button>
              </div>
              <div style={{ fontSize: 20, fontWeight: "bold", paddingTop: 12 }}>{filter.title}</div>
              <div style={{ fontSize: 12, color: '#999' }}>{filter.subtitle}</div>
            </div>
          </div>
          <hr />
        </div>
      </Content>
    </div>
  );
}

export default App;