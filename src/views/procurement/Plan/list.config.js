export default {
  init: () => {
    return {
      breadcrumbs: [
        { text: 'Beranda', to: '/' },
        { text: 'Pengadaan', to: '/procurement' },
        { text: 'RAB Perencanaan' },
      ],
      filter: {
        title: 'Filter RAB Perencanaan',
        subtitle: 'LAKUKAN FILTER UNTUK MEMUDAHKAN PENCARIAN',
        headerStyle: {
          margin: -20,
          // paddingTop: 20,
          // paddingLeft: 20,
          borderBottom: '1px solid #c8ced3',
        }
      }
    }
  }
}