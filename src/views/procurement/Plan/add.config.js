export default {
  init: () => {
    return {
      title: 'Tambah Rencana',
      subtitle: 'MASUKAN DATA SESUAI KEBUTUHAN DAN TELITI KEMBALI SEBELUM MENYELESAIKAN',
      breadcrumbs: [
        { text: 'Beranda', to: '/' },
        { text: 'Pengadaan', to: '/procurement' },
        { text: 'RAB Perencanaan', to: '/procurement/plan' },
        { text: 'Tambah' },
      ],
      headerStyle: {
        margin: -20,
        // paddingTop: 20,
        // paddingLeft: 20,
        borderBottom: '1px solid #c8ced3',
      },
      data: {
        pengadaan: [
          { text: 'Lelang', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
          { text: 'Rekanan', src: 'https://i.picsum.photos/id/12/200/200.jpg', active: 1 },
          { text: 'Penunjukan Langsung', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
          { text: 'Korporat / Mandiri', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
        ]
      }
    }
  }
}