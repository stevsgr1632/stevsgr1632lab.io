import interceptor from "./interceptor";
import { initURL as apiUrl } from "./initialEndpoint";
import LocalStorageService from "./localStorageService";

export const getHeadersBase = {
  'Accept': 'application/vnd.api+json',
  'Content-Type': 'application/vnd.api+json',
  'Access-Control-Allow-Origin': true
};
export const getHeaders = () => {
  const { attributes } = LocalStorageService.state_auth.getAccessToken();
  const headers = {...getHeadersBase};
  if(attributes?.accessToken){
    headers.Authorization = `Bearer ${attributes?.accessToken}`;
  }
  return headers;
};

export const getOption = (method, url, data) => {
  return { headers : getHeaders() ,method, url: `${apiUrl}/${url}`, data }
}

const app = {
  get: async (url) => {
    const option = getOption('get', url);
    
    try {
      const { data } = await interceptor.request(option);
      return data;
    } catch (err) {
      throw err;
    }
  },
  post: async (url, params) => {
    
    const option = getOption('post', url, params);
    try {
      const { data } = await interceptor.request(option);
      return data;
    } catch (err) {
      throw err;
    }
  },
  patch: async (url, params) => {
    
    const option = getOption('patch', url, params);
    try {
      const { data } = await interceptor.create({}).request(option);
      return data;
    } catch (err) {
      throw err;
    }
  },
  put: async (url, params) => {
    
    const option = getOption('put', url, params);
    try {
      const { data } = await interceptor.request(option);
      return data;
    } catch (err) {
      throw err;
    }
  },
  delete: async (url, params) => {
    
    const option = getOption('delete', url, params);
    try {
      const { data } = await interceptor.request(option);
      return data;
    } catch (err) {
      throw err;
    }
  }
};

export default app;