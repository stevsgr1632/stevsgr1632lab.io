const STATE_AUTH = 'state_auth';

const AuthService = (() => {
  const _setToken = (auth_params) => {
    localStorage.setItem(STATE_AUTH, JSON.stringify(auth_params));
  };

  const _getAccessToken = () => {
    return JSON.parse(localStorage.getItem(STATE_AUTH)) || {};
  };

  const _clearToken = () => {
    localStorage.removeItem(STATE_AUTH);
  };

  return {
    setToken : _setToken,
    clearToken : _clearToken,
    getAccessToken : _getAccessToken,
  };
})();

const LocalStorageService = {
  state_auth : AuthService,
}

export default LocalStorageService;