import axios from "axios";
// import state from "../redux";
// import * as jwt from 'jsonwebtoken';
// import { getOption } from "./request";
// import { get, includes } from 'lodash';
import authAction from 'redux/auth/action';
// import Notification from "common/notification";
import { getOption,getHeadersBase } from "./request";
import { tokenCompareTimeNow } from "./initialEndpoint";
import LocalStorageService from "./localStorageService";

const apiUrl = process.env.REACT_APP_BASE_URL;
const interceptor = axios.create({});
const interceptorRefresh = axios.create({});

let isRefreshing = false;
let failedQueue = [];

const processQueue = (error, token = null) => {
  failedQueue.forEach(prom => {
    if (error) {
      prom.reject(error);
    } else {
      prom.resolve(token);
    }
  })
  
  failedQueue = [];
}

interceptorRefresh.interceptors.request.use( async config => {
  return config;
},(error)=>{
  return Promise.reject(error);
});

interceptorRefresh.interceptors.response.use( response => {
  return Promise.resolve(response);
},(error)=>{
  return Promise.reject(error);
});

interceptor.interceptors.request.use( async config => {
  return config;
}, function (error) {
  return Promise.reject(error);
});

interceptor.interceptors.response.use(function (response) {
  return response;
}, async function (error) {
  const { attributes } = LocalStorageService.state_auth.getAccessToken();
  const option = {
    headers: getHeadersBase,
    method : 'patch',
    url: `${apiUrl}/services/auth`,
    data: { refreshToken: attributes?.refreshToken }
  }

  // Reject promise if usual error
  if (error.response.status !== 401) {
    // handling error
    // if(error.response.status === 500){
      // if(error.response){
      //   const { data, status } = error.response;
      //   let errorMessage = '';
      //   if(data.error){
      //     if(Array.isArray(data.error)){
      //       data.error.forEach( x => {
      //         errorMessage = `${x.dataPath} : ${x.message}` 
      //         const errorResponse = {
      //           code : status,
      //           message : errorMessage
      //         }
      //         Notification({response:errorResponse, type : 'error',placement : 'topRight'});
      //       });
      //     }else{
      //       errorMessage = data.error; 
      //       const errorResponse = {
      //         code : status,
      //         message : errorMessage
      //       }
      //       Notification({response:errorResponse, type : 'error',placement : 'topRight'});
      //     }
      //   } else {
      //     errorMessage = (data.message) ? data.message : 'Terjadi kesalahan saat proses data';
      //       const errorResponse = {
      //         code : status,
      //         message : errorMessage
      //       }
      //     Notification({response:errorResponse, type : 'error',placement : 'topRight'});
      //   }        
      // }
    // }
    return Promise.reject(error);
  }

  const _axios = interceptor;
  const originalRequest = error.config;

  if (isRefreshing) {
    return new Promise(function(resolve, reject) {
    failedQueue.push({resolve, reject})
    }).then(token => {
    originalRequest.headers['Authorization'] = 'Bearer ' + token;
    return _axios.request(originalRequest);
    }).catch(err => {
    return Promise.reject(err);
    })
  }
  
  originalRequest._retry = true;
  isRefreshing = true;

  let accessToken = attributes?.accessToken;
  
  if (tokenCompareTimeNow(accessToken)) {
    return new Promise((resolve,reject) =>{
      return interceptorRefresh.request(option).then( response =>{
        authAction.setToken(response.data.data);
        _axios.defaults.headers.common['Authorization'] = `Bearer ${response?.data?.data?.attributes?.accessToken}`;
        originalRequest.headers['Authorization'] = `Bearer ${response?.data?.data?.attributes?.accessToken}`;
        processQueue(null, response?.data?.data?.attributes?.accessToken);
        resolve(_axios(originalRequest));
      }).catch((err)=>{
        authAction.clearAuth();
        LocalStorageService.state_auth.clearToken();
        window.location.reload();
        window.location.replace('/login');
        processQueue(err, null);
        reject(err);
      }).then(()=>{ isRefreshing = false })
    })
  }else{
    return Promise.reject(error);
  }
});

export const validateToken = async () => {
  const { attributes } = LocalStorageService.state_auth.getAccessToken();
  let accessToken = attributes?.accessToken;
  
  if (tokenCompareTimeNow(accessToken)) {
    const option = getOption('patch', `services/auth`, { refreshToken: attributes?.refreshToken });
    await axios.request(option).then(response => {
      authAction.setToken(response.data.data);
      accessToken = response?.data?.data?.attributes?.accessToken;
      axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;
      return false;
    }).catch(() => {
      // authAction.clearAuth();
      LocalStorageService.state_auth.clearToken();
      return false;
    });
  }
};

export default interceptor;