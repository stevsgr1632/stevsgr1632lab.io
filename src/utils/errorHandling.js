import Notification from "common/notification";

const errorHandling = (error) =>{
    if(error.response){
        const { data, status } = error.response;
        let errorMessage = '';

        if(data.error){
            //Jika error message tersedia di API
            if(Array.isArray(data.error)){
                data.error.forEach( x => {
                    errorMessage = `${x.dataPath} : ${x.message}` 
                    const errorResponse = {
                        code : status,
                        message : errorMessage
                    }
                    Notification({response:errorResponse, type : 'error',placement : 'topRight'});
                });
            }else{
                errorMessage = data.error; 
                const errorResponse = {
                    code : status,
                    message : errorMessage
                }
                Notification({response:errorResponse, type : 'error',placement : 'topRight'});
            }
            return true; //Message ditampilkan (true)
        } else if(data.message) {
            errorMessage = data.message
            const errorResponse = {
                code : status,
                message : errorMessage
            }
            Notification({response:errorResponse, type : 'error',placement : 'topRight'});
            return true; //Message ditampilkan (true)
        } else{
            return false; //Message tidak ditampilkan (false) untuk conditional message di page
        }        
    }else{
        return false; //Message tidak ditampilkan (false) untuk conditional message di page
    }
}

export default errorHandling;