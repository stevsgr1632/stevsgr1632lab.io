import storeRedux from 'redux/store';

const getMenuAkses = params => {
  let stateRedux = storeRedux.getState();
  if(stateRedux?.menuAccess){
    let menuAkses = stateRedux.menuAccess?.find(x => x.menu_url === params);
    return menuAkses?.menu_access ?? [];
  }
  return [];
};

export const getRoleAkses = params => {
  const stateRedux = storeRedux.getState();
  let roleAkses = [];
  if(stateRedux.roleAkses){
    let menuAkses = stateRedux.menuAccess.find(x => x.menu_url === params);
    stateRedux.roleAkses.forEach(num => {
      
      if(num.children){
        const aksesRole = num.children.find( x => x.menu_id === menuAkses.id);
        if(aksesRole){
          roleAkses = [...aksesRole.menu_access];
        }
      }
    });
  }
  return roleAkses;
}

export const getOneMenu = (menuakses, params) => {
  if(Array.isArray(menuakses) && menuakses?.find(x => x.access_name === params)?.access_role){
    return true;
  }else{
    return false;
  }
};

export const filterURL = () => {
  if(localStorage.getItem('menuAccess')){
    const url = JSON.parse(localStorage.getItem('menuAccess')).map(rbac => rbac.menu_url);
    url.push('/settings','/profile','/initial');
    return url;
  }
};

export default getMenuAkses;
