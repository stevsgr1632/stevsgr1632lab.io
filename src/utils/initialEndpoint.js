import moment from 'moment';
import * as jwt from 'jsonwebtoken';

export const initURL = process.env.REACT_APP_BASE_URL;
export const initialEndpoint = 'webhook';
export const noImagePath = '/assets/general/no-image.png';
// tgl
export const formatDateInput = (format = 'YYYY-MM-DD') => format;
export const formDateInputValue = (tgl = new Date(), format ='') => moment(tgl, format ? format : formatDateInput()); // output : Object { _isAMomentObject: true, ...}
export const formDateDisplayValue = (tgl = new Date(), format ='') => moment(tgl).format(format ? format : formatDateInput()); // output : 2020-11-30
// decode time
export const decodeTimeToken = token => token ? ((jwt.decode(token).exp) * 1000) : undefined;
export const decodeToken = token => formDateDisplayValue(decodeTimeToken(token), 'YYYY-MM-DD HH:mm:ss Z');
export const tokenCompareTimeNow = token => {
  if(decodeTimeToken(token) < Date.now()){
    return true;
  }else{
    return false;
  }
};

// convert number
export const numberFormat = (params,type ='') => type === 'uang' ? `Rp${new Intl.NumberFormat('id-ID', {}).format(params)},00` : new Intl.NumberFormat('id-ID', {}).format(params);