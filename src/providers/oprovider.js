import axios from 'axios';
import request, {getHeadersBase} from 'utils/request';
import { initURL,initialEndpoint } from 'utils/initialEndpoint';

export default {
  cancel : () => axios.CancelToken.source().cancel(),
  list: (resource) => {
    return request.get(`${initialEndpoint}/${resource}`);
  },
  update: (resource, attributes) => {
    return request.put(`${initialEndpoint}/${resource}`, { ...attributes });
  },
  insert: (resource, attributes) => {
    return request.post(`${initialEndpoint}/${resource}`, { ...attributes });
  },
  delete: (resource) => {
    return request.delete(`${initialEndpoint}/${resource}`);
  },
  downloadfile : async (resource) => {
    let url = `${initURL}/${initialEndpoint}/${resource}`;
    const response = await axios({
      url,
      method: 'GET',
      responseType: 'blob', // important
    });
    return response;
  },
  insertNoToken : async (resource,attributes) => {
    try {
      const { data } = await axios.request({
        headers: getHeadersBase, 
        method:'post', 
        url: `${initURL}/${initialEndpoint}/${resource}`,
        data: {...attributes},
      });
      return data;
    } catch (error) {
      throw error;
    }
    
  },
  insertCustom: (resource, attributes) => {
    return request.post(`${initialEndpoint}/${resource}`, attributes);
  },
};