import axios from 'axios';
import { getHeadersBase } from "utils/request";
import { initURL,initialEndpoint } from 'utils/initialEndpoint';

const auth = JSON.parse(localStorage.getItem('state_auth')) || {};
const { attributes = {} } = auth;

export default {
  insert: async (resource, dataPassing, sendAnonymously = false) => {
    const target = `${initURL}/${initialEndpoint}/${resource}`;
    const formData = new FormData();
    const { filename, keterangan = "keterangan" } = dataPassing;
    formData.append('filename', filename);
    formData.append('keterangan', keterangan);
    
    const headers = {
      ...getHeadersBase,
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    if (!sendAnonymously) headers.Authorization = `Bearer ${attributes.accessToken}`;

    const options = {
      headers,
      method: 'POST',
      url: target,
      data: formData
    };

    const { data } = await axios.request(options);
    return data;
  },
  insertnew : async (resource, dataPassing, sendAnonymously = false) => {
    const target = `${initURL}/${initialEndpoint}/${resource}`;
    const formData = new FormData();
    for(let obj in dataPassing){
      formData.append(obj, dataPassing[obj]);
    }

    let headers = {...getHeadersBase};
    if (!sendAnonymously) headers.Authorization = `Bearer ${attributes.accessToken}`;

    let options = {
      headers: headers,
      method: 'POST',
      url: target,
      data: formData
    };
    try {
      const { data } = await axios.request(options);
      return data;
    } catch (error) {
      console.log(error);
    }
  }
};