import request from 'utils/request';
import authAction from 'redux/auth/action';
import localStorageService from "utils/localStorageService";

const url = 'services/auth';
export default {
  login: async (params) => {
    const option = { provider: 'local', data: params };
    try {
      const resp = await request.post(url, option);
      authAction.setAuth(resp.data);
      return resp;
    } catch (error) {
      throw error;
    }
  },
  refreshToken: async () => {
    const { attributes = {} } = localStorageService.state_auth.getAccessToken();

    if (attributes.refreshToken) {
      try {
        const resp = await request.patch(url, { refreshToken: attributes.refreshToken });
        authAction.setToken(resp.data);
        return resp;
      } catch (error) {
        console.log(error?.message);
      }            
    }
  },
  logout: () => {
    authAction.clearAuth();
    return Promise.resolve();
  }
};