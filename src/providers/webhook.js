import request from 'utils/request';
import { initialEndpoint } from 'utils/initialEndpoint';

export default {
  list: (resource) => {
    return request.get(`${initialEndpoint}/${resource}`);
  },
  get: (resource) => {
    return request.get(`${initialEndpoint}/${resource}`);
  },
  update: (resource, attributes) => {
    return request.patch(`${initialEndpoint}/${resource}`, { data: { "type": "author", attributes } });
  },
  insert: (resource, attributes) => {
    return request.post(`${initialEndpoint}/${resource}`, { data: { attributes } });
  },
  delete: (resource) => {
    return request.delete(`${initialEndpoint}/${resource}`);
  },
};