import axios from 'axios';
import request,{getHeadersBase} from 'utils/request';
import { initURL,initialEndpoint } from 'utils/initialEndpoint';

export default (resource) => {
  return request.get(`${initialEndpoint}/${resource}`);
};

export const newDropDown = async (resource) => {
  const { data } = await axios.request({
    headers: getHeadersBase,
    method:'get', 
    url: `${initURL}/${initialEndpoint}/${resource}`
  });
  return data.data;
};
export const getOne = async (resource) => {
  const { data } = await axios.request({
    headers: getHeadersBase,
    method:'get', 
    url: `${initURL}/${initialEndpoint}/${resource}`
  });
  return data;
};