import reqClient from 'utils/request';
import { initialEndpoint } from 'utils/initialEndpoint';

export default {
    list: async(params) => {
        const result = await reqClient.get(`${initialEndpoint}/banks`, params);
        return result;
    },
    insert: async(id, body) => {
        const result = await reqClient.post(`${initialEndpoint}/banks`, body);
        return result;
    },
    update: async(id, body) => {
        const result = await reqClient.put(`${initialEndpoint}/banks/?id=${id}`, body);
        return result;
    },

    // called when the user navigates to a new location, to check for permissions / roles
    getPermissions: () => Promise.resolve(),
};