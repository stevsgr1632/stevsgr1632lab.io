import React from 'react';
import './styles/sidebar.scss';
import {Menu,Layout} from 'antd';
import { useHistory } from 'react-router-dom';
import stylesCustom from 'utils/stylesCustom';
/* eslint-disable */

const MocoSideBar = params => {
  const { items, router } = params;
  const history = useHistory();
  const { boxShadow } = stylesCustom;
  const [collapse,setcollapse] = React.useState(true);

  const handleBreakPoint = (breakPoint) => {
    // console.log(breakPoint);
  };

  const handleCollapse = (collapsed, type) => {
    // console.log(collapsed, type);
    setcollapse(collapsed);
  };
  
  React.useEffect(() => {
    // console.log(params.items);
  },[params.items]);

  return(
    <Layout.Sider 
      breakpoint="xl"
      collapsedWidth="0"
      onBreakpoint={handleBreakPoint}
      collapsible 
      collapsed={collapse} 
      onCollapse={handleCollapse}
      style={{
        backgroundColor:'#fff',
        boxShadow
      }}
    >
    <React.Suspense>
      <Menu 
        mode="inline"
        defaultSelectedKeys={history.location.pathname}
        selectedKeys={history.location.pathname}
      >
        {items.map(x => x.children ? 
        (
        <Menu.SubMenu key={x.url} title={x.menu}>
          {x.children.map((y,indexY) => <Menu.Item key={y.url}><router.Link to={y.url} >{y.name}</router.Link></Menu.Item>)}
        </Menu.SubMenu>
        ) : (
        <Menu.Item key={x.url}><router.Link to={x.url} >{x.name}</router.Link></Menu.Item>
        ) )}
      </Menu>
    </React.Suspense>
  </Layout.Sider>
  );
};

export default MocoSideBar;
