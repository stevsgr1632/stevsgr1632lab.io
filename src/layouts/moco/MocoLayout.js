import React, { Suspense, useEffect, useState } from 'react';
import { Modal, Button } from 'antd';
import * as router from 'react-router-dom';
import { Redirect, Route, Switch } from 'react-router-dom';
import {
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';

/* eslint-disable */
import routes from 'routes';
import oAuth from 'providers/Auth';
import pathName from 'routes/pathName';
import oWebhook from 'providers/webhook';
import ButtonBack from 'components/ButtonBack';
import LoadingPage from 'components/LoadingPage';

const MocoFooter = React.lazy(() => import('./MocoFooter'));
const MocoHeader = React.lazy(() => import('./MocoHeader'));
const PageNotFound = React.lazy(() => import('views/_pages/NotFound'));
const { base: baseUrl, initial: homeUrl,profile,settings,login:loginUrl } = pathName;

export default (props) => {
  const [items, setItems] = useState([]);
  const [, ] = useState({ checking: 0, refresh: 0 });
  const [minimizeSide,] = useState(true);
  const [tokenParam, setTokenParam] = useState({ visible: false });
  const signOut = async (e) => {
    e.preventDefault()
    await oAuth.logout();
    props.history.push(baseUrl);
  }
  const { history } = props;
  const initMenu = async () => {
    const resp = await oWebhook.list('user-menu');
    const items = resp?.data;
    
    if(!items) {
      props.history.push(loginUrl); 
      return false;
    }else{
      items.forEach(m => {
        m.name = m.name || m.menu;
        if (m.children) {
          m.children.forEach(n => {
            n.name = n.name || n.menu;
            if (n.children) {
              n.children(o => {
                o.name = o.name || o.menu;
              })
            }
          })
        }
      })
      setItems(items);
    }

  }

  useEffect(() => {
    initMenu();
  }, [])

  return (
    <div className="app">
      <AppHeader fixed>
        <Suspense fallback={<LoadingPage/>}>
          <MocoHeader onLogout={e => signOut(e)} onProfile={() => history.push(profile)} onSetting={() => history.push(settings)} />
        </Suspense>
      </AppHeader>
      <div className="app-body">
        <AppSidebar minimized={minimizeSide} fixed display="lg">
          <ButtonBack history={history} title="Go Back" />
          <AppSidebarHeader />
          <AppSidebarForm />
          <Suspense>
            <AppSidebarNav navConfig={{ items }} {...props} router={router} />
          </Suspense>
          <AppSidebarFooter />
          <AppSidebarMinimizer />
        </AppSidebar>
        <main className="main">
          <Suspense fallback={<LoadingPage/>}>
            <Switch>
              {routes.map((route, idx) => {
                return route.component ? (
                  <Route
                    key={idx}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    render={props => (
                      <route.component {...props} />
                    )} />
                ) : (null);
              })}
              <Redirect exact from={baseUrl} to={homeUrl} />
              <Route render={() => <PageNotFound />} />
            </Switch>
          </Suspense>
        </main>
      </div>
      <AppFooter style={{justifyContent:'center'}}>
        <MocoFooter />
      </AppFooter>

      <Modal
        visible={tokenParam.visible}
        title="Token expired"
        footer={[
          <Button key="submit" type="primary" onClick={async () => {
            setTokenParam({ ...tokenParam, visible: false });
            try {
              await oAuth.refreshToken();
              window.location.reload();
            } catch (err) {
              signOut();
            }
          }}>
            Refresh Token
          </Button>,
          <Button key="back" onClick={() => {
            setTokenParam({ ...tokenParam, visible: false })
          }}>
            Cancel
          </Button>,
        ]}
      >
        <p>Your token expired on {tokenParam.expired} {tokenParam.exp}</p>
      </Modal>
    </div>
  );
}
