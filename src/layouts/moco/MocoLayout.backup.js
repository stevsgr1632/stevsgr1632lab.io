import React, { Suspense, useEffect, useState } from 'react';
import { Modal, Button } from 'antd';
import * as jwt from 'jsonwebtoken';
import * as router from 'react-router-dom';
import { Redirect, Route, Switch } from 'react-router-dom';
import {
    AppFooter,
    AppHeader,
    AppSidebar,
    AppSidebarFooter,
    AppSidebarForm,
    AppSidebarHeader,
    AppSidebarMinimizer,
    AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
/* eslint-disable */

import routes from '../../routes';
import oAuth from '../../providers/Auth';
import oWebhook from '../../providers/webhook';
import state from '../../redux';
import pathName from '../../routes/pathName';
import ButtonBack from '../../components/ButtonBack';

const MocoFooter = React.lazy(() => import('./MocoFooter'));
const MocoHeader = React.lazy(() => import('./MocoHeader'));
const PageNotFound = React.lazy(() => import('../../views/_pages/NotFound'));
const { base: baseUrl, login: loginUrl, home: homeUrl } = pathName;

export default (props) => {
    const [items, setItems] = useState([]);
    const [counter, setCounter] = useState({ checking: 0, refresh: 0 });
    const [tokenParam, setTokenParam] = useState({ visible: false });
    const loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>
    const signOut = async (e) => {
        e.preventDefault()
        await oAuth.logout();
        props.history.push(loginUrl)
    }
    const { history } = props;
    const initMenu = async () => {
        const resp = await oWebhook.list('user-menu');
        const items = resp.data;
        items.forEach(m => {
            m.name = m.name || m.menu;
            if (m.children) {
                m.children.forEach(n => {
                    n.name = n.name || n.menu;
                    if (n.children) {
                        n.children(o => {
                            o.name = o.name || o.menu;
                        })
                    }
                })
            }
        })
        setItems(resp.data);
    }

    useEffect(() => {
        const logout = async () => {
            await oAuth.logout();
            history.push(loginUrl)
        }
        const checkToken = async () => {
            const { auth = {} } = state.getState();
            const { attributes = {} } = auth;
            const { accessToken } = attributes;

            if (!accessToken) {
                await logout();
            } else {
                const decode = jwt.decode(accessToken);

                // console.log(counter, '->', decode.exp * 1000 < Date.now() ? 'expired' : 'not expired', new Date(decode.exp * 1000));

                if (decode.exp * 1000 < Date.now()) {
                    try {
                        console.log('refresh', counter.refresh);
                        setTokenParam({ ...tokenParam, visible: true, expired: new Date(decode.exp * 1000).toDateString(), exp: decode.exp });
                        setCounter({ ...counter, refresh: counter.refresh + 1 });
                        // await oAuth.refreshToken();
                    } catch (error) {
                        console.log(error.message);
                        await logout();
                    }
                }
            }
        }

        const timeout = setTimeout(() => {
            checkToken();
            setCounter({ ...counter, checking: counter.checking + 1 });
        }, 60000);

        return () => { clearTimeout(timeout); }
    }, [counter]);

    useEffect(() => {
        initMenu();
    }, [])

    return (
        <div className="app">
            <AppHeader fixed>
                <Suspense fallback={loading()}>
                    <MocoHeader onLogout={e => signOut(e)} />
                </Suspense>
            </AppHeader>
            <div className="app-body">
                <AppSidebar fixed display="lg">
                    <ButtonBack history={history} title="Go Back" />
                    <AppSidebarHeader />
                    <AppSidebarForm />
                    <Suspense>
                        <AppSidebarNav navConfig={{ items }} {...props} router={router} />
                    </Suspense>
                    <AppSidebarFooter />
                    <AppSidebarMinimizer />
                </AppSidebar>
                <main className="main">
                    <Suspense fallback={loading()}>
                        <Switch>
                            {routes.map((route, idx) => {
                                return route.component ? (
                                    <Route
                                        key={idx}
                                        path={route.path}
                                        exact={route.exact}
                                        name={route.name}
                                        render={props => (
                                            <route.component {...props} />
                                        )} />
                                ) : (null);
                            })}
                            <Redirect exact from={baseUrl} to={homeUrl} />
                            <Route render={() => <PageNotFound />} />
                        </Switch>
                    </Suspense>
                </main>
            </div>
            <AppFooter style={{ marginTop: 28 }}>
                <MocoFooter />
            </AppFooter>

            <Modal
                visible={tokenParam.visible}
                title="Token expired"
                footer={[
                    <Button key="submit" type="primary" onClick={async () => {
                        setTokenParam({ ...tokenParam, visible: false });
                        try {
                            await oAuth.refreshToken();
                            window.location.reload();
                        } catch (err) {
                            signOut();
                        }
                    }}>
                        Refresh Token
          </Button>,
                    <Button key="back" onClick={() => {
                        setTokenParam({ ...tokenParam, visible: false })
                    }}>
                        Cancel
          </Button>,
                ]}
            >
                <p>Your token expired on {tokenParam.expired} {tokenParam.exp}</p>
            </Modal>
        </div>
    );
}
