import React from 'react';
import pathName from 'routes/pathName';
/* eslint-disable */

const {developer} = pathName;

export default () => {
  return (
    <React.Fragment>
      <span><a href={developer}>Aksaramaya</a> &copy; 2020 Moco Catalog</span>
    </React.Fragment>
  );
}
