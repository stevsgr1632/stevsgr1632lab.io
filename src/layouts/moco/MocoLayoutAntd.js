import React from 'react';
import * as router from 'react-router-dom';
import { Modal, Button, Layout } from 'antd';
import { Redirect, Route, Switch } from 'react-router-dom';
import routes from 'routes';
import oAuth from 'providers/Auth';
import { connect } from 'react-redux';
import pathName from 'routes/pathName';
import MocoSideBar from './MocoSideBar';
import oWebhook from 'providers/webhook';
import oProvider from 'providers/oprovider';
import ButtonBack from 'components/ButtonBack';
import LoadingPage from 'components/LoadingPage';
import menuAccessReduxAction from 'redux/rbac/action';
import {storage as userInfoStorage} from 'redux/userInfo/reducer';
import getMenuAkses, { filterURL, getOneMenu } from 'utils/getMenuAkses';
/* eslint-disable */

const MocoFooter = React.lazy(() => import('./MocoFooter'));
const MocoHeader = React.lazy(() => import('./MocoHeaderAntd'));
const PageNotFound = React.lazy(() => import('views/_pages/NotFound'));
const PageNotAllow = React.lazy(() => import('views/_pages/NotAllowed'));
const { base: baseUrl, dashboard: dashboardURL,profile,settings,login:loginUrl } = pathName;

const MocoLayoutAntd = (props) => {
  const { history } = props;
  const [items, setItems] = React.useState([]);
  const [, ] = React.useState({ checking: 0, refresh: 0 });
  const [urlAllow,seturlAllow] = React.useState([]);
  const [tokenParam, setTokenParam] = React.useState({ visible: false });
  const signOut = async (e) => {
    e.preventDefault()
    await oAuth.logout();
    window.location.reload();
    props.history.push(baseUrl);
  }
  const initMenu = async () => {
    try {
      const { data : items } = await oWebhook.list('user-menu');      
      if(items) {
        items.forEach(m => {
          m.name = m.name || m.menu;
          if (m.children) {
            m.children.forEach(n => {
              n.name = n.name || n.menu;
              if (n.children) {
                n.children(o => {
                  o.name = o.name || o.menu;
                })
              }
            })
          }
        })
        setItems(items);
      }else{
        props.history.push(loginUrl); 
        return false;
      }
    } catch (error) {
      console.log(error?.message);
    }
  };

  const allowedURLFunc = (urlparams,type) => {
    const splitData = (data) => data.split('/')[1];
    if(urlAllow.length > 0){
      const filterUrl = urlAllow.filter(x => {
        const re = new RegExp(x, 'i');
        //console.log(re);
        const searchUrl = re.test(urlparams) || false;
        // console.log(searchUrl);
        if(searchUrl) return x;
      });
      const normalizeData = filterUrl.join().search(splitData(urlparams));
      if(normalizeData === -1) {
        return true;
      }else{
        if(type){
          const newFilterUrl = filterUrl.reduce(function (a, b) { return a.length > b.length ? a : b; });
          const menuAccess = getMenuAkses(newFilterUrl);
          if(menuAccess.length > 0){
            if(getOneMenu(menuAccess,type)){
              return false;
            }else{
              return true;
            }
          }else{
            return true;
          }
        }else{
          return false;
        }
        
      }
    }
    
  };

  const fetchRoleAkses = async () => {
    const userInfo = JSON.parse(localStorage.getItem(userInfoStorage)) || {};
    try {
      const { data } = await oProvider.list(`role-get-one?id=${userInfo.id}`);
      if(data.role_access){
        menuAccessReduxAction.setRoleAccess(data.role_access);
      }
    } catch (error) {
      console.log('error',error);
    }
  }

  React.useEffect(() => {
    let dataURL = filterURL();
    seturlAllow(dataURL || []);
    fetchRoleAkses();
  },[localStorage.getItem('menuAccess')]);

  React.useEffect(() => {
    if(props.auth.attributes.email){
      setTimeout(() => initMenu(),500);
    }
  }, [props.auth.attributes.email]);

  return (
    <div className="app">
      <ButtonBack history={history} title="Go Back" />
      <Layout>
        <MocoHeader onLogout={e => signOut(e)} onProfile={() => history.push(profile)} onSetting={() => history.push(settings)} />
        <Layout style={{margin:'4.5em 0 0 0'}}>
          <MocoSideBar items={items} {...props} router={router} />
          <Layout>
          <React.Suspense fallback={<LoadingPage/>}>
            <Switch>
              {routes.map((route, idx) => {
                return route.component ? (
                  <Route
                    key={idx}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    render={props => (<>
                      {allowedURLFunc(route.path,route.type) ?
                        <Route render={() => <PageNotAllow />} /> 
                        :
                        <route.component {...props} />
                      }
                    </>)} />
                ) : (null);
              })}
              <Redirect exact from={baseUrl} to={dashboardURL} />
              <Route render={() => <PageNotFound />} />
            </Switch>
          </React.Suspense>
          <Layout.Footer style={{ textAlign: 'center',padding:0,background:'none' }}>
            <MocoFooter />
          </Layout.Footer>
        </Layout>
        </Layout>
      </Layout>
      <Modal
        visible={tokenParam.visible}
        title="Token expired"
        footer={[
          <Button key="submit" type="primary" onClick={async () => {
            setTokenParam({ ...tokenParam, visible: false });
            try {
              await oAuth.refreshToken();
              window.location.reload();
            } catch (err) {
              signOut();
            }
          }}>
            Refresh Token
          </Button>,
          <Button key="back" onClick={() => {
            setTokenParam({ ...tokenParam, visible: false })
          }}>
            Cancel
          </Button>,
        ]}
      >
        <p>Your token expired on {tokenParam.expired} {tokenParam.exp}</p>
      </Modal>
    </div>
  );
}
const mapStateToProps = ({auth}) => ({auth});
export default connect(mapStateToProps)(MocoLayoutAntd);
