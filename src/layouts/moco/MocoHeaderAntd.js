import React from 'react';
import './styles/mocoheader.scss';
import { connect } from 'react-redux';
import logo from './img/brand/logo_red.png';
import { Layout,Dropdown,Menu,Button,Avatar, Typography } from "antd";
import { UserOutlined, AppstoreOutlined,ImportOutlined } from "@ant-design/icons";
/* eslint-disable */

const MocoHeaderAntd = props => {
  const {onLogout,onSetting,onProfile} = props;
  const [userProfile,setuserProfile] = React.useState({}); 
  const [, ] = React.useState({ lang: 'en', ddlLanguageOpen: false });
  // const changeLanguage = (lang) => {
  //   langAction.changeLanguage(lang);
  //   setState((state) => ({ ...state, lang }))
  // };

  React.useEffect(() => {
    if(props.userInfo.email){
      setuserProfile(props.userInfo);
    }
  },[props.userInfo.email]);

  const menuDropDown = (
    <Menu>
      <Menu.Item><Typography.Text strong>{userProfile?.name}</Typography.Text></Menu.Item>
      <Menu.Item>
        <Button type="link" icon={<UserOutlined />} onClick={onProfile}>
          Profile
        </Button>
      </Menu.Item>
      <Menu.Item>
        <Button type="link" icon={<AppstoreOutlined />} onClick={onSetting}>
          Settings
        </Button>
      </Menu.Item>
      <Menu.Item>
        <Button type="link" icon={<ImportOutlined />} onClick={onLogout}>
          Logout
        </Button>
      </Menu.Item>
    </Menu>
  );

  return(
  <Layout.Header className="moco__header">
    <div className="moco__header--wrapper">
      <Menu mode="horizontal">
        <Menu.Item key="1">
          <img src={logo} 
            className="moco__header--img" 
          />
        </Menu.Item>
      </Menu>
      <Dropdown overlay={menuDropDown}>
        <Avatar 
          size="large" 
          className="moco__header--avatar" 
          src={userProfile?.image ?? '../../assets/img/avatars/1.jpg'} 
        />
      </Dropdown>
    </div>
  </Layout.Header>);
};

const mapStateToProps = ({userInfo}) => ({userInfo});
export default connect(mapStateToProps)(MocoHeaderAntd);