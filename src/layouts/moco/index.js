import React from 'react';
import { BrowserRouter as Router, 
  Route, Switch,useHistory,
  useLocation } from 'react-router-dom';
  import './styles/index.scss';
import pathName from 'routes/pathName';
import LoadingPage from 'components/LoadingPage';
import historyReduxAction from 'redux/history/action';
import menuAccessReduxAction from 'redux/rbac/action';
import LocalStorageService from "utils/localStorageService";
/* eslint-disable */

// pages
const MocoPage = React.lazy(() => import('./MocoLayoutAntd'));
const FrontPage = React.lazy(() => import('views/_pages/FrontPage'));
const LoginPage = React.lazy(() => import('views/_pages/LoginPage'));
const LupaPassPage = React.lazy(() => import('views/_pages/LupaPassPage'));
const RegisterPage = React.lazy(() => import('views/_pages/RegisterPage'));

const { base, login,register,forgotPass } = pathName;
const ProtectedRoute = (props) => {
  let history = useHistory();
  const locationBrowser = useLocation();
  const auth = LocalStorageService.state_auth.getAccessToken();
  const { attributes = {} } = auth || {};

  // user redirect last page when token expired and then logged in
  React.useEffect(() => {
    historyReduxAction.getPathNameBefore(locationBrowser.pathname);
  },[locationBrowser]);

  React.useEffect(() => {
    if(!attributes?.accessToken){
      history.push(login);
    }
    if(localStorage.getItem('menuAccess')){
      let parseData = JSON.parse(localStorage.getItem('menuAccess'));
      menuAccessReduxAction.setMenuAccess(parseData);
    }
  },[]);
  
  return (<>
    <Route render={props => attributes.accessToken && <MocoPage {...props} />} />
  </>)
};

const App = () => {
  return (
    <Router>
      <React.Suspense fallback={<LoadingPage/>}>
        <Switch>
          <Route exact path={login} name="Login Page" render={props => <LoginPage {...props} />} />
          <Route exact path={register} name="Register Page" render={props => <RegisterPage {...props} />} />
          <Route exact path={forgotPass} name="Lupa Password Page" render={props => <LupaPassPage {...props} />} />
          <Route exact path={base} name="Base Page" render={props => <FrontPage {...props} />} />
          <ProtectedRoute path={base} />
        </Switch>
      </React.Suspense>
    </Router>
  );
}

export default App;
