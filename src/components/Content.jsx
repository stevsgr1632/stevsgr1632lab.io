import React from 'react';
import { Button, Tooltip } from 'antd';
import { getOneMenu } from 'utils/getMenuAkses';
import { Card, CardBody, CardHeader, Container } from 'reactstrap';

const styleColor = {
  whiteColor : '#f0f2f5',
  grayColor : '#ddd',
  redColor : '#C92036',
};

const Toolbars = (props) => {
  const toolbars = props.toolbars || [];
  if (props.toolbars) {
    return (
      <div 
        className="card-toolbars text-right" 
        style={{alignSelf:'center'}}
      >
        {toolbars.map((m, idx) => {
          const { onClick } = m;

          const handleClick = () => {
            if (onClick) {
              onClick({ ...props, ...m });
            }
          }
          return (<React.Fragment key={idx}>
            <Tooltip title={(m.tooltip) ? m.tooltip :  null}>
            { (props.menuAkses?.length && getOneMenu(props.menuAkses,'Tambah')) ? 
              <Button 
                className="mr-1" 
                key={idx} {...m} 
                onClick={handleClick}
                style={{
                  color:styleColor.redColor,
                  backgroundColor:styleColor.whiteColor,
                  borderColor:styleColor.grayColor
                }}
              >{m.text}</Button> 
            : <Button 
            className="mr-1" 
            key={idx} {...m} 
            onClick={handleClick}
            style={{
              color:styleColor.redColor,
              backgroundColor:styleColor.whiteColor,
              borderColor:styleColor.grayColor
            }}
            >{m.text}</Button>}
            </Tooltip>
          </React.Fragment>)
        })}
      </div>
    );
  }

  return '';
}

const Header = (props) => {
  let resp = '';
  if (props.title || props.subtitle) {
    return (
      <CardHeader 
      style={{
        display:'flex',
        justifyContent:'space-between',
        backgroundColor:styleColor.redColor,
        color:styleColor.whiteColor,
        border:'none'
      }}>
        <div>
          {(props.title) ? (<h3 style={{color:styleColor.whiteColor}}>{props.title}</h3>) : ''}
          {(props.subtitle) ? (<div>{props.subtitle}</div>) : ''}
        </div>
        <Toolbars {...props} />
      </CardHeader>
    );
  }
  return resp;
}

const Content = (props = {}) => {
  return (
    <React.Fragment>
      <Container fluid>
        <div 
          className="page-content animated fadeIn" 
          style={{boxShadow:'0.1825rem 2rem 2.25rem -.875rem hsla(5.5, 79.3%, 56.5%, 0.42)'}}
        >
          <Card style={{border:'none'}}>
            <Header {...props} />
            <CardBody>{props.children}</CardBody>
          </Card>
        </div>
      </Container>
    </React.Fragment>
  );
}

export default Content;