import React from 'react';
import { Input } from 'antd';
/* eslint-disable */

export default (props) => {
  const { name, onSearch, schema = {} } = props;

  const fieldSchema = schema[name] || {};
  const label = props.text || fieldSchema.label || fieldSchema.text;
  // const labelCol = props.labelCol || { span: 24 };
  // const { rules } = fieldSchema;

  const handleChangeSearch = (value, event) => {
    if (name && onSearch) {
      onSearch(name, value, event);
    }
  }

  return (<>
    <label>{label}</label>
    <Input.Search {...props} onSearch={handleChangeSearch} enterButton />
  </>)
};