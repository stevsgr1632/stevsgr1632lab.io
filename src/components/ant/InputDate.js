import React from 'react';
import { Form, DatePicker } from 'antd';

export default (props) => {
    const { name, onChange, schema = {}} = props;

    const fieldSchema = schema[name] || {};
    const label = props.text || fieldSchema.label || fieldSchema.text;
    const labelCol = props.labelCol || { span: 24 };
    const { rules } = fieldSchema;

    const handleChange = (e, date) => {
      if (onChange) {
        onChange(name, date, e);
      }
    };

    return (
    <Form.Item name={name} label={label} rules={rules} labelCol={labelCol}>
      <DatePicker {...props} onChange={handleChange} />
    </Form.Item>
    )
};