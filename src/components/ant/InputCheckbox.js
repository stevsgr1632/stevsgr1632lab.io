import React from 'react';
import { Form, Checkbox } from 'antd';
/* eslint-disable */

export default (props) => {
  const { onChange, name, schema = {}, styleForm = {}, styleCustom = {}, others = {} } = props;
  const fieldSchema = schema[name] || {};
  const label = props.text || fieldSchema.label || fieldSchema.text;
  const { rules } = fieldSchema;

  const handleChange = (e) => {
    if (name && onChange) {
      onChange(name, e.target.checked, e);
    }
  };

  return (
    <Form.Item name={name} label={label} rules={rules} valuePropName="checked" style={styleForm}>
      <Checkbox onChange={handleChange} style={styleCustom} {...others}>
        {props.children || null}
      </Checkbox>
    </Form.Item>
  )
};