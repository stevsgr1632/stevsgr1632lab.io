import React from 'react';
import { Form, DatePicker } from 'antd';

export default (props) => {
    const { name, onChange, schema = {}} = props;

    const fieldSchema = schema[name] || {};
    const label = props.text || fieldSchema.label || fieldSchema.text;
    const labelCol = props.labelCol || { span: 24 };
    const { rules } = fieldSchema;

    const handleChange = async (_,newDates) => {
      if (onChange) {
        onChange(name, newDates);
      }
    }

    return (
    <Form.Item name={name} label={label} rules={rules} labelCol={labelCol}>
      <DatePicker.RangePicker {...props}
        placeholder={props.placeholder}
        onCalendarChange={handleChange} 
      />
    </Form.Item>
    )
};