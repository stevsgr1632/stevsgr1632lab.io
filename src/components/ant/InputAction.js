import React from 'react';
import { Form, Button,Space } from 'antd';
/* eslint-disable */

export default (props) => {
  const { onChange, name } = props;
  const useLabel = props.useLabel || props.text;

  const handleAction = (action) => {
    if (name && onChange) {
      onChange(name, action);
    }
  }

  return (
    <Form.Item style={{ margin: '.6em 0',  }}>
      {useLabel ? (<div>{props.text || '\u00A0'}</div>) : ''}
      <Space size="small">
        {props.actions.map((m, idx) => <Button key={m.key || idx} {...m} onClick={() => handleAction(m.action)}>{m.text}</Button>)}
      </Space>
    </Form.Item>
  );
};