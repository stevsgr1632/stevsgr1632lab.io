import React from 'react';
import { Form, InputNumber } from 'antd';

export default (props) => {
  const { name, onChange, schema = {}, others = {}, onBlur } = props;

  const fieldSchema = schema[name] || {};
  const labelCol = props.labelCol || { span: 24 };
  const label = props.text || fieldSchema.label || fieldSchema.text;
  const { rules } = fieldSchema;

  const handleChange = (e) => {
    if (onChange) {
      onChange(name, e.target.value, e);
    }
  }
  const handleBlur = (e) => {
    if (onBlur) {
      onBlur(name, e.target.value, e);
    }
  }

  return (
    <Form.Item name={name} label={label} rules={rules} labelCol={labelCol}>
      <InputNumber {...others} style={{ width: '100%' }} onChange={handleChange} onBlur={handleBlur} />
    </Form.Item>
  )
};