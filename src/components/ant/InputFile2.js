import React from 'react';
import { Upload, Modal, Form, message } from "antd";
import { initialEndpoint } from '../../utils/initialEndpoint';
/* eslint-disable */

const apiUrl = process.env.REACT_APP_BASE_URL;
const auth = JSON.parse(localStorage.getItem('state_auth')) || {};
const { attributes = {} } = auth;

const InputFile2 = (props) => {
  const { onChange, name, schema = {}, limitFileList = 1, fileMaxSize = 2, accept : allowedFileType = '*', uploadURL = 'upload-image', params = {}, sendAnonymously = false } = props;
  const fieldSchema = schema[name] || {};
  const { rules } = fieldSchema;
  const label = props.text || fieldSchema.label || fieldSchema.text;
  const labelCol = props.labelCol || { span: 24 };

  const [state,setstate] = React.useState({src : '', visible : false});

  const handleChange = async ({file, fileList, event}) => {
    if(file?.percent === 100){
      // console.log(file, fileList, event);
      // console.log(file?.response?.data);
      if(onChange){
        onChange(name,file,file?.response?.data);
      }
    }
  };

  const beforeUpload = (file) => {
    let isAccept = null;
    if(allowedFileType !== "*"){
      let isAcceptFile = allowedFileType.split(',');
      let typeFile = '.' + file.name.split('.').pop();
      isAccept = isAcceptFile.includes(typeFile);
      if (!isAccept) {
        message.error(`You can only upload ${isAcceptFile.map(x => '.' + x.substring(1).toUpperCase()).join(', ')} file`);
      }
    }
    const isLt2M = file.size / 1024 / 1024 < fileMaxSize;
    if (!isLt2M) {
      message.error(`Image must smaller than ${fileMaxSize}MB`);
    }
    return isAccept && isLt2M;
  }

  const onPreview = async file => {
    let src = file.url;
    if (!src) {
      src = await new Promise(resolve => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
      setstate({src, visible : true});
    }
    // const image = new Image();
    // image.src = src;
    // const imgWindow = window.open(src);
    // imgWindow.document.write(image.outerHTML);
  };

  return(<>
  <Form.Item style={{ marginBottom: 5 }} name={name} label={label} rules={rules} labelCol={labelCol}>
    <div style={{display:'flex'}}>
      {props.otherComponent && props.otherComponent}
      <Upload 
        {...props}
        action={`${apiUrl}/${initialEndpoint}/${uploadURL}`}
        name={name} accept={allowedFileType || '*'}
        listType={props.listType || 'picture-card'}
        className="avatar-uploader"
        onChange={handleChange}
        headers={{authorization : `Bearer ${attributes.accessToken}`}}
        data={file => ({...params, filename : file })}
        onPreview={onPreview}
        beforeUpload={beforeUpload}
        openFileDialogOnClick
      >
        Upload
      </Upload>
    </div>
  </Form.Item>
  <Modal
    centered
    width="30vw"
    footer={null}
    onCancel={() => setstate({ ...state, visible : false })}
    visible={state.visible}
  >
    <img src={state.src} alt="no-data-fig" width="100%" />
  </Modal>
  </>);
};

export default InputFile2;