import React from 'react';
import { Form, Input } from 'antd';

export default (props) => {
  const { name, onChange,onBlur,placeholder, schema = {} } = props;

  const fieldSchema = schema[name] || {};
  const label = props.text || fieldSchema.label || fieldSchema.text;
  const labelCol = props.labelCol || { span: 24 };
  const { rules } = fieldSchema;

  const handleChange = (e) => {
    if (name && onChange) {
        onChange(name, e.target.value, e);
    }
  }
  const handleBlur = (e) => {
    if (name && onBlur) {
      onBlur(name, e.target.value, e);
    }
  }

  return (
    <Form.Item name={name} label={label} rules={rules} labelCol={labelCol}>
      <Input.TextArea {...props} onChange={handleChange} rows={fieldSchema.rows || 4} placeholder={placeholder} onBlur={handleBlur} />
    </Form.Item>
  )
};