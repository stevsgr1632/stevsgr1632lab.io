import React from 'react';
import { Form, Select } from 'antd';
import oProvider from '../../providers/dropdownapi';
/* eslint-disable */

export default (props) => {
  const [loading, setloading] = React.useState(false);
  const [data, setdata] = React.useState([]);
  const { onChange, name, schema = {} } = props;
  const [value, setvalue] = React.useState('');
  
  const fieldSchema = schema[name] || {};
  const label = props.text || fieldSchema.label;
  const labelCol = props.labelCol || { span: 24 };
  const { rules } = fieldSchema;

  const handleSearch = async (value) => {
    if (value) {
      let dataFromAPI = await oProvider(`${props.addressApi}?name=${value}`);
      setdata(dataFromAPI.data);
    } else {
      let dataFromAPI = await oProvider(`${props.addressApi}`);
      setdata(dataFromAPI.data);
    }
  }
  React.useEffect(() => {
    setloading(true);
    async function fetchData() {
      let response = await oProvider(props.addressApi);
      setdata(response.data);
      setloading(false);
    }
    fetchData();
  }, []);

  const handleChange = (value, e) => {
    setvalue(value);
    if (onChange) {
      onChange(name, value, e);
    }
  }

  

  return (
    <Form.Item name={name} label={label} rules={rules} labelCol={labelCol}>
      <Select
        style={{ width: '100%' }}
        placeholder={props.placeholder || label}
        optionFilterProp={'text'}
        onChange={handleChange}
        filterOption={(input, option) => {
          return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
        }}
        onSearch={handleSearch}
        value={value}
        showSearch
        defaultActiveFirstOption={false}
      >
        {!loading && data.map((m, idx) => (
          <Select.Option key={idx} value={m.value} disabled={m.disabled}>{m.text}</Select.Option>
        ))}
      </Select>
    </Form.Item>
  )

  // if (name && schema[name] && form) {
  //   const fieldSchema = schema[name];
  //   const label = props.text || fieldSchema.label;
  //   const { getFieldDecorator, getFieldError } = form;
  //   const fieldError = getFieldError(name);

  //   return (
  //     <Form.Item
  //       validateStatus={fieldError ? 'error' : ''}
  //       help={fieldError ? fieldError[0] : ''}
  //       style={{ marginBottom: 5 }}
  //     >
  //       <strong>{label}</strong>
  //       {getFieldDecorator(name, { rules: fieldSchema.rules })(
  //         <Select 
  //           showSearch {...props}
  //           style={{ width: '100%' }} 
  //           onChange={handleChange}
  //         >
  //           {!loading && data.map((m, idx) => (
  //             <Option key={idx} value={m.value} disabled={m.disabled}>{m.text}</Option>
  //           ))}
  //         </Select>
  //       )}
  //     </Form.Item>
  //   )
  // }

  // return (
  //   <Form.Item style={{ marginBottom: 5 }}>
  //     {props.text ? (<strong>{props.text}</strong>) : ''}
  //     <Select 
  //       showSearch {...props}
  //       style={{ width: '100%' }} 
  //       onChange={handleChange}
  //     >
  //       {!loading && data.map((m, idx) => (
  //         <Option key={idx} value={m.value}>{m.text}</Option>
  //       ))}
  //     </Select>
  //   </Form.Item>
  // );
};