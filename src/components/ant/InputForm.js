import React, { useEffect } from 'react';
import { Row, Col, Form } from 'antd';

import Content from '../Content';
import InputControl from './InputControl';
/* eslint-disable */

const App = (props) => {
  const { scope, controls = [], onChange, form, model, menuAkses = [], history = {} } = props;
  const defCol = props.defCol || { lg: { span: 16 }, xl: { span: 14 } }

  useEffect(() => {
    if (form) form.setFieldsValue(model);
  }, []);

  return (
    <Content {...scope} menuAkses={menuAkses} history={history} >
      <Form layout="vertical" form={form}>
        <Row gutter={12}>
          {controls.map((prop, idx) => {
            if (props.schema) props.schema.form = form;
            return (
              <Col key={idx} {...(prop.defcol || defCol)}>
                <InputControl  {...prop} schema={props.schema} onChange={onChange} />
              </Col>
            )
          })}
        </Row>
        {props.children}
      </Form>
    </Content>
  );
};

export default App;