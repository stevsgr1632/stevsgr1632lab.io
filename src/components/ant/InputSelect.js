import React from 'react';
import { Form, Select } from 'antd';
/* eslint-disable */

export default (props) => {
  const { name, onChange, schema = {}, data = [], others = {} } = props;

  const fieldSchema = schema[name] || {};
  const label = props.text || fieldSchema.label || fieldSchema.text;
  const labelCol = props.labelCol || { span: 24 };
  const { rules } = fieldSchema;

  const handleChange = (value, e) => {
    if (onChange) {
      onChange(name, value, e);
    }
  }
  
  const onSearch = (val) => {
    console.log('search:', val);
  }

  return (
    <Form.Item name={name} label={label} rules={rules} labelCol={labelCol}>
      <Select
        mode={props.mode || ''}
        placeholder={props.placeholder || label}
        style={{ width: '100%' }}
        optionFilterProp="children"
        onChange={handleChange}
        filterOption={(input, option) =>
          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
        {...others}
      >
        {data.map((m, idx) => (
          <Select.Option key={idx} value={m.value}>{m.text}</Select.Option>
        ))}
      </Select>
    </Form.Item>
  )
};



// import React from 'react';
// import { Form, Select } from 'antd';

// export default (props) => {
//   const { onChange, name, schema = {}, data = [], defaultVal = '' } = props;
//   const { form } = schema;
//   const { Option } = Select;

//   const handleChange = (value, e) => {
//     if (onChange) {
//       onChange(name, value, e);
//     }
//   }

//   if (name && schema[name] && form) {
//     const fieldSchema = schema[name];
//     const label = props.text || fieldSchema.label;
//     const { getFieldDecorator, getFieldError } = form;
//     const fieldError = getFieldError(name);

//     return (
//       <Form.Item
//         validateStatus={fieldError ? 'error' : ''}
//         help={fieldError ? fieldError[0] : ''}
//         style={{ marginBottom: 5 }}
//       >
//         {label}
//         {getFieldDecorator(name, { rules: fieldSchema.rules })(
//           <Select
//             option={{ initialValue: defaultVal }}
//             showSearch {...props}
//             style={{ width: '100%' }}
//             onChange={handleChange}
//             optionFilterProp={'text'}
//             filterOption={(input, option) => {
//               return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
//             }}
//           >
//             {data.map((m, idx) => (
//               <Option key={idx} value={m.value} disabled={m.disabled}>{m.text}</Option>
//             ))}
//           </Select>
//         )}
//       </Form.Item>
//     )
//   }

//   return (
//     <Form.Item style={{ marginBottom: 5 }}>
//       {props.text ? (<strong>{props.text}</strong>) : ''}
//       <Select
//         defaultValue={defaultVal}
//         showSearch {...props}
//         style={{ width: '100%' }}
//         onChange={handleChange}
//         optionFilterProp={'text'}
//         filterOption={(input, option) => {
//           return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
//         }}
//       >
//         {data.map((m, idx) => (
//           <Option key={idx} value={m.value}>{m.text}</Option>
//         ))}
//       </Select>
//     </Form.Item>
//   );
// };