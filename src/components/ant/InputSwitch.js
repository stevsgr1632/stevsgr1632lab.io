import React from 'react';
import { Form, Switch } from 'antd';
/* eslint-disable */

export default (props) => {
  const { onChange, name, schema = {}, styleForm = {}, styleCustom = {} } = props;
  const fieldSchema = schema[name] || {};
  const label = props.text || fieldSchema.label || fieldSchema.text;
  const { rules } = fieldSchema;

  const handleChange = (checked) => {
    if (name && onChange) {
      onChange(name, checked);
    }
  }

  return (
    <Form.Item name={name} label={label} rules={rules} valuePropName="checked" style={styleForm}>
      <Switch onChange={handleChange} style={styleCustom} />
    </Form.Item>
  )
};