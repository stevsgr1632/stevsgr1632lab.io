import React,{ useEffect } from 'react';
import './InputFile3.scss';
import { Form, Upload, message } from 'antd';
import oProvider from '../../providers/uploadfile';
// import { checkedURL } from "../../common/validators";
import { noImagePath } from "../../utils/initialEndpoint";
/* eslint-disable */

message.config({
  top: 100,
  duration: 2,
  maxCount: 3,
  rtl: true,
});

function getBase64(img, callback) {
  if(img){
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }
}

export default (props) => {
  const { onChange, name, schema = {}, limitFileList = -1, fileMaxSize = 500, accept : allowedFileType = '*', uploadURL = 'upload-image', params = {}, sendAnonymously = false , imgUrl,handlePreview} = props;
  const fieldSchema = schema[name] || {};
  const { rules } = fieldSchema;
  const label = props.text || fieldSchema.label || fieldSchema.text;
  const labelCol = props.labelCol || { span: 24};

  const [imageUrl, setimageUrl] = React.useState('');
  const [fileList, setfileList] = React.useState([]);
  const [dataAction, setdataAction] = React.useState({});

  const beforeUpload = (file) => {
    let isAccept = null;
    if(allowedFileType !== "*"){
      let isAcceptFile = allowedFileType.split(',');
      let typeFile = '.' + file.name.split('.').pop();
      isAccept = isAcceptFile.includes(typeFile);
      if (!isAccept) {
        message.error(`You can only upload ${isAcceptFile.map(x => '.' + x.substring(1).toUpperCase()).join(', ')} file`);
      }
    }

    const isLt2M = file.size / 1024 < fileMaxSize;
    if (!isLt2M) {
      message.error(`Image must smaller than ${fileMaxSize}KB`);
    }
    return isAccept && isLt2M;
  }
  

  const handleChange = async info => {
    // setting fileList for provide data on form submitting
    let fileListInfo = [...info.fileList];
    // limit fileList array 
    fileListInfo = fileListInfo.slice(limitFileList);
    setfileList(fileListInfo.map( x => {
        //get image temporary url and then preview to user
      getBase64(info.file.originFileObj, imageUrl => {
          
          if(x.status){
            setimageUrl(imageUrl);
            x.url = imageUrl;
          }           
      });

      if(!x.status){
          x.thumbUrl = (imgUrl) ? imgUrl : noImagePath;
      }
      // add custom data here
      x.data = dataAction;

      return x;
    }));
  };
  
  const handleActionUpload = async (options) => {
    const { onSuccess, onError, file } = options;
    try {
        let response = await oProvider.insert(uploadURL, { ...params,filename: file, keterangan: 'upload data' }, sendAnonymously);
        setdataAction(response.data);
        if (name && onChange) {
            onChange(name, file,response.data);
        }
        onSuccess("Ok");
    } catch (err) {
        console.log("Error: ", err);
        const error = new Error("Some error");
        onError({ error });
    }
  }

  useEffect(()=>{
    if(imgUrl){
      setfileList([
          {
            uid: '-1',
            name: 'logo.png',
            status: 'done',
            url: (imgUrl) ? imgUrl : '' ,
          }
      ])
    }
  },[imgUrl]);
  
  return (
  <div className='inputfile-container'>
    <Form.Item style={{ marginBottom: 5 }} name={name} label={<div style={{position:'relative',width: '100%'}}>{label}</div>} rules={rules} labelCol={labelCol}>
      <div style={{display:'flex',width:'100%',height:'100%',justifyContent:'center'}}>
        {props.otherComponent && props.otherComponent}
        <div style={{position:'relative'}}>
          <Upload
              {...props}
              customRequest={handleActionUpload}
              beforeUpload={beforeUpload}
              accept={allowedFileType || '*'}
              listType={props.listType || 'picture-card'}
              fileList={fileList}
              onChange={handleChange}
              onPreview={handlePreview}
          >
            {fileList.length < 1 && '+ Upload'}
          </Upload>
          </div>
      </div>
    </Form.Item>
    <div style={{margin:'10px 0px',width:'100%',textAlign:'center'}}>Format : { (allowedFileType) ? allowedFileType.split('.').join(' ').toUpperCase() : '*'}</div>
  </div>);
};
