import React from 'react';
import { Form, Upload } from 'antd';
import MessageComp from "../../common/message";
import { PlusOutlined } from '@ant-design/icons';
import oProvider from '../../providers/uploadfile';
/* eslint-disable */

const getBase64 = (img, callback) => {
  if(img){
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }
};
export default (props) => {
  const { onChange, name, schema = {}, limitFileList = -1, fileMaxSize = 2, accept : allowedFileType = '*', uploadURL = 'upload-image', params = {}, sendAnonymously = false } = props;
  const fieldSchema = schema[name] || {};
  const { rules } = fieldSchema;
  const label = props.text || fieldSchema.label || fieldSchema.text;
  const labelCol = props.labelCol || { span: 24 };

  const [, setimageUrl] = React.useState('');
  const [fileList, setfileList] = React.useState([]);
  const [loading, setloading] = React.useState(false);
  const [dataAction, setdataAction] = React.useState({});

  const beforeUpload = (file) => {
    let isAccept = null;
    if(allowedFileType !== "*"){
      let isAcceptFile = allowedFileType.split(',');
      let typeFile = '.' + file.name.split('.').pop();
      isAccept = isAcceptFile.includes(typeFile);
      if (!isAccept) {
        MessageComp({
          type : 'error', 
          text : `You can only upload ${isAcceptFile.map(x => '.' + x.substring(1).toUpperCase()).join(', ')} file`
        });
      }
    }
    const isLt2M = file.size / 1024 / 1024 < fileMaxSize;
    if (!isLt2M) {
      MessageComp({
        type : 'error', 
        text : `Image must smaller than ${fileMaxSize}MB` 
      });
    }
    return isAccept && isLt2M;
  };

  const handleChange = async info => {
    // setting fileList for provide data on form submitting
    let fileListInfo = [...info.fileList];
    // limit fileList array 
    fileListInfo = fileListInfo.slice(limitFileList);
    setfileList(fileListInfo.map(x => {
      // add custom data here
      x.data = dataAction;
      return x;
    }));

    //get image temporary url and then preview to user
    await getBase64(info.file.originFileObj, imageUrl => {
      setimageUrl(imageUrl);
    });
  };
  
  const handleActionUpload = async (options) => {
    const { onSuccess, onError, file } = options;
    try {
      setloading(true);
      let { data } = await oProvider.insert(uploadURL, { ...params,filename: file, keterangan: 'upload data' }, sendAnonymously);
      setdataAction(data);
      if (name && onChange) {
        onChange(name, file,data);
      }
      setloading(false);
      onSuccess("Ok");
    } catch (error) {
      console.log('error',error);
      onError({ error });
    }
  };
  
  return (
    <Form.Item style={{ marginBottom: 5 }} name={name} label={label} rules={rules} labelCol={labelCol}>
      <div style={{display:'flex'}}>
        {props.otherComponent && props.otherComponent}
        <Upload
          {...props}
          name={name} 
          accept={allowedFileType || '*'}
          listType={props.listType || 'picture-card'}
          className="avatar-uploader"
          showUploadList={false}
          customRequest={handleActionUpload}
          beforeUpload={beforeUpload}
          fileList={fileList}
          onChange={handleChange}
        >
          {props.children ? props.children : <div>
            {loading ? 'Uploading...' : <PlusOutlined/>}
            <div className="ant-upload-text">Upload</div>
          </div>}
        </Upload>
      </div>
    </Form.Item>
  );
};