import React from 'react';

import InputText from './InputText';
import ImageCheck from './ImageCheck';
import InputAction from './InputAction';
import InputSelect from './InputSelect';
import InputTextArea from './InputTextArea';
import InputRangePicker from './InputRangePicker';

export default (props) => {
  const { type } = props;
  const prop = { ...props };

  delete prop.type;

  switch (type) {
    case 'select':
      return <InputSelect {...prop} />
    case 'textarea':
      return <InputTextArea {...prop} />
    case 'action':
      return <InputAction {...prop} />
    case 'rangepicker':
      return <InputRangePicker {...prop} />
    case 'checkbox-image':
      return <ImageCheck {...prop} />
    case 'radio-image':
      return <ImageCheck type="radio" {...prop} />
    default:
      return <InputText {...prop} />
  }
};