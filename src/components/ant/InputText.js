import React from 'react';
import { Form, Input } from 'antd';

export default (props) => {
    const { name, onChange, schema = {}, onBlur} = props;

    const fieldSchema = schema[name] || {};
    const label = props.text || fieldSchema.label || fieldSchema.text;
    const labelCol = props.labelCol || { span: 24 };
    const { rules } = fieldSchema;

    const handleChange = (e) => {
      if (onChange) {
        onChange(name, e.target.value, e);
      }
    }
    const handleBlur = (e) => {
      if (onBlur) {
        onBlur(name, e.target.value, e);
      }
    }

    return (
    <Form.Item name={name} label={label} rules={rules} labelCol={labelCol}>
      <Input {...props} type={props.type || 'text'} placeholder={props.placeholder || label} onChange={handleChange} onBlur={handleBlur} />
    </Form.Item>
    )
};

// import React from 'react';
// import { Form, Input } from 'antd';

// export default (props) => {
//     const { onChange, name, schema = {} } = props;
//     const { form } = schema;

//     const handleChange = (e) => {
//         if (name && onChange) {
//             onChange(name, e.target.value, e);
//         }
//     }

//     if (name && schema[name] && form) {
//         const fieldSchema = schema[name];
//         const label = props.text || fieldSchema.label;
//         const { getFieldDecorator, getFieldError } = form;
//         const fieldError = getFieldError(name);

//         return (
//             <Form.Item
//                 validateStatus={fieldError ? 'error' : ''}
//                 help={fieldError ? fieldError[0] : ''}
//                 style={{ marginBottom: 5 }}
//             >
//                 {label}
//                 {getFieldDecorator(name, { rules: fieldSchema.rules })(
//                     <Input {...props} onChange={handleChange} />
//                 )}
//             </Form.Item>
//         )
//     }

//     return (
//         <Form.Item style={{ marginBottom: 5 }}>
//             {props.text ? (<strong>{props.text}</strong>) : ''}
//             <Input {...props} onChange={handleChange}></Input>
//         </Form.Item>
//     );
// };