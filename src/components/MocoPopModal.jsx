import React from 'react';
import { Modal } from 'antd';
import { CloseSquareFilled } from '@ant-design/icons';
import { CardBody, CardHeader } from 'reactstrap';

const MocoPopModal = (props) => {
    const { onClose, footer = null,visible,width ='800px'} = props;

    return (
        <Modal 
            width={width}
            visible={visible}
            bodyStyle={{padding:'0px'}}
            onCancel={() => {
                onClose();
            }}
            centered={true}
            footer={footer}
            closeIcon={
            <div style={{width:'100%',height:'100%',justifyContent:'center'}}>
                <CloseSquareFilled style={{fontSize:'35px',color:'white'}}/>
            </div>}>
            <CardHeader style={{
                display:'flex',
                justifyContent:'space-between',
                backgroundColor:'#C92036',
                color:'white',
                border:'none'}}>
                <div>
                    {(props.title) ? <h3 style={{color:'white'}}>{props.title}</h3> : '' }
                    {(props.subtitle) ? <div>{props.subtitle}</div> : '' }
                </div>
            </CardHeader>
            <CardBody>
                {props.children}
            </CardBody>
        </Modal>
    )
}

export default MocoPopModal
