import React from 'react';
import { Table } from 'antd';
import randomString from "utils/genString";
import { LoadingOutlined } from '@ant-design/icons';

const MocoTable = (props) => {
  return (
    <Table 
      rowKey={(row) => (row.id || randomString(10) )} 
      {...props} 
      size= 'small' 
      loading={{indicator:<LoadingOutlined spin/> , spinning: props.loading || false }} 
    />
  );
}

export default MocoTable;