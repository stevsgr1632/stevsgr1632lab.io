import React from 'react';
import { Button } from 'antd';
import {BackwardOutlined} from '@ant-design/icons';
import './styles/back.style.scss';

const App = props => {
  const { options = {} } = props;
  const optionDefault = {
    type: 'primary',
    size: 'large',
    className: 'button-back-style',
    onClick: () => props.history.goBack(),
    ...options
  }

  return (
    <Button {...optionDefault}><BackwardOutlined /> {props.title ?? 'BACK'}</Button>
  )
}

export default App;