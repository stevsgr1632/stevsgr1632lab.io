import React from 'react';
import LoadingGif from '../layouts/moco/img/loading.gif';
import { Result } from 'antd';

const LoadingPage = () => {
  return (
    <Result
      icon={<img src={LoadingGif} alt="gambargif" />}
      title=""
      extra=""
    />
  );
}

export default LoadingPage;