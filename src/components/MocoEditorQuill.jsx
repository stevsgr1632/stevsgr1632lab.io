import React from 'react';
import { Form,Input } from 'antd';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

const MocoEditorQuill = (props) => {
    const { name, onChange, schema = {} , required = false, value = '' ,form} = props;
    const fieldSchema = schema[name] || {};
    const { rules } = fieldSchema;
    const label = props.text || fieldSchema.label || fieldSchema.text;
    const labelCol = props.labelCol || { span: 24 };
    

    const editorChange = (content, delta, source, editor) => {
      if(onChange){
        const value = editor.getHTML();
        if(editor.getText().length > 1){
          onChange(value);
          form.setFieldsValue({ [name]: value });
        } else {
          onChange('');
          form.setFieldsValue({ [name]: '' });
        }
        
      }
    }

    const modules = {
        toolbar: {
          container: [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            ['blockquote', 'code-block'],
    
            [{ header: 1 }, { header: 2 }],               // custom button values
            [{ list: 'ordered' }, { list: 'bullet' }],
            [{ script: 'sub' }, { script: 'super' }],      // superscript/subscript
            [{ indent: '-1' }, { indent: '+1' }],          // outdent/indent
            [{ direction: 'rtl' }],                         // text direction
    
            [{ size: ['small', false, 'large', 'huge'] }],  // custom dropdown
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
    
            [{ color: [] }, { background: [] }],          // dropdown with defaults from theme
            [{ font: [] }],
            [{ align: [] }],
    
            ['link'],
            ['clean'],
          ]
        },
        // formula: true
      }
      
    return (
      <Form.Item label={label} labelCol={labelCol} required={required}>
        <ReactQuill 
          {...props}
          value={value ? value.toString() : ''}
          modules={modules} 
          onChange={editorChange}/>
        <Form.Item name={name} rules={rules} noStyle>
            <Input type='hidden'/>
        </Form.Item>
      </Form.Item>
    )
}

export default MocoEditorQuill;
