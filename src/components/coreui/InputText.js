import React from 'react';
import { FormGroup, Label, Input } from 'reactstrap';


export default (props) => {
    return (
        <FormGroup>
            <Label for="examplePassword">{props.text}</Label>
            <Input  {...props}></Input>
        </FormGroup>
    );
};