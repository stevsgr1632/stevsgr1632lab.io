import React from 'react';
import { FormGroup, Label, Input } from 'reactstrap';


export default (props) => {
    const { data = [] } = props;

    return (
        <FormGroup>
            <Label for="examplePassword">{props.text}</Label>
            <Input type="select" {...props}>
                {data.map((m, idx) => (
                    <option key={idx} value={m.value}>{m.text}</option>
                ))}
            </Input>
        </FormGroup>
    );
};