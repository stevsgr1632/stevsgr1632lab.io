import React from 'react';
import { Card, CardBody, CardHeader, Container } from 'reactstrap';

const Header = (props) => {
  let resp = '';
  if (props.title || props.subtitle) {
    return (
      <CardHeader>
        {(props.title) ? (<h3>{props.title}</h3>) : ''}
        {(props.subtitle) ? (<div>{props.subtitle}</div>) : ''}
      </CardHeader>
    )
  }
  return resp;
}

const Content = (props = {}) => {
  return (
    <React.Fragment>
      <Container fluid>
        <div className="page-content animated fadeIn">
          <Card>
            {/* <CardHeader>
              <h3>Header</h3>
              <div>Card subtitle</div>
            </CardHeader> */}
            <Header {...props} />
            <CardBody>{props.children}</CardBody>
          </Card>
        </div>
      </Container>
    </React.Fragment>
  );
}

export default Content;