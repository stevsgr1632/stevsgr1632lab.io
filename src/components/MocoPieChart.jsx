import React from 'react';
import {PieChart, Pie, Cell,ResponsiveContainer } from 'recharts';

const MocoPieChart = (props) => {
    const { data , dataKey , colors } = props;
    const RADIAN = Math.PI / 180;

    const renderCustomizedLabel = ({cx, cy, midAngle, innerRadius, outerRadius, percent}) => {
        const radius = innerRadius + (outerRadius - innerRadius) * 0.4;
        const x = cx + radius * Math.cos(-midAngle * RADIAN);
        const y = cy + radius * Math.sin(-midAngle * RADIAN);
        return (
                <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central" style={{fontSize:'1em'}}>
                    {`${(percent * 100).toFixed(0)}%`}
                </text>
        );
    };

    return (
        <ResponsiveContainer>
            <PieChart>
                <Pie
                    data={data}
                    labelLine={false}
                    label={(item)=>renderCustomizedLabel(item)}
                    outerRadius='100%'
                    isAnimationActive={false}
                    dataKey={dataKey}
                    >
                    {
                        data.map((entry, index) => <Cell key={`cell-${index}`} fill={colors[index % colors.length]} />)
                    }
                </Pie>
            </PieChart>
        </ResponsiveContainer>
    )
}

export default MocoPieChart
