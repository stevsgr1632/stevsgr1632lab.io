import React from 'react';
import { 
    BarChart, 
    CartesianGrid, 
    Tooltip, 
    ResponsiveContainer,
    XAxis,
    YAxis,
    Bar,
    Legend
} from 'recharts';
import { numberFormat } from "utils/initialEndpoint";
 
const MocoBarChart = (props) => {
  const { data, config,handleClick } = props;
  const { XAxisKey , bar , } = config;

  const CustomTooltip = (params) => {
    const { active, payload = [], label } = params;
    const { legendData } = config;
    if (active) {
      return (
        <div style={{background:'#fff',border:'1px solid #b2b2b2',padding:'20px',borderRadius:'4px'}}>
          <div className="label">{`${label}`}</div>
          {
            (payload) ? (
              (payload.length > 0 ) ? 
                payload.map((x,idx) => {
                  return (<div key={idx} style={{color:`${x.color}`}}>{`${legendData.filter( y => y.id===x.name )[0].value} : ${numberFormat(x.value)}`}</div>)
              }) : '' 
            ) : ''
              
          }
        </div>
      );
    }
  
    return null;
  };


  return (
    <React.Fragment>
      <ResponsiveContainer>
        <BarChart
          data={data}
          margin={{top: 5, right: 5, left: 0, bottom: 30}}
        >
          <CartesianGrid strokeDasharray="5 5" />
          <Tooltip content={CustomTooltip}/>
          <XAxis dataKey={XAxisKey} />
          <YAxis />
          <Legend payload={config.legendData} />
          {
            bar.map((x, idx) => {
              return (<Bar cursor="pointer" key={idx} dataKey={x.dataKey} fill={x.fill} onClick={handleClick} />)
            })
          }
        </BarChart>
      </ResponsiveContainer>
    </React.Fragment>
  )
}

export default MocoBarChart
