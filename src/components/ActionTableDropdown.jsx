import React from 'react';
import { Button } from "antd";

const ActionTableDropdown = props => {
  const { text, type, style, onClick, ...rest} = props;

  return(<>
  <Button type={type} style={style} onClick={onClick} {...rest}>
    {text}
  </Button>
  </>);
}

export default ActionTableDropdown;