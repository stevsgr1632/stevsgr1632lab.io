import CBreadcrumb from './Breadcrumb';
import CContent from './Content';

import CMocoEditor from './MocoEditor';
import CMocoTable from './MocoTable';

import CInputControl from './ant/InputControl';
import CInputForm from './ant/InputForm';

export const Breadcrumb = CBreadcrumb;
export const Content = CContent;

export const MocoEditor = CMocoEditor;
export const MocoTable = CMocoTable;

export const InputControl = CInputControl;
export const InputForm = CInputForm;

export default {
  Breadcrumb,
  Content,

  MocoEditor,
  MocoTable,

  InputControl,
  InputForm,
}
