import React from 'react';
import moment from 'moment';
import i18next from "i18next";
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import enTranslations from "./locales/en";
import idTranslations from "./locales/id";
import ErrorPage from './views/_pages/ErrorPage';
import {store, persistor} from './redux/store';
import LoadingPage from './components/LoadingPage';
import { initReactI18next, I18nextProvider } from 'react-i18next';
import { PersistGate } from 'redux-persist/lib/integration/react';
import 'moment/locale/id';

moment().locale('id');

const resources = {
    en: { messages: enTranslations },
    id: { messages: idTranslations },
};
const i18n = i18next.use(initReactI18next);
i18n.init({
    react: {
        wait: true,
    },
    resources: resources,
    lng: 'id',
    fallbackLng: 'id',
    keySeparator: '.',
    interpolation: {
        escapeValue: false,
    },
    ns: ['messages'],
    defaultNS: 'messages',
    fallbackNS: [],
});

const Layout = React.lazy(() => import(`./layouts/moco`));

const App = () => (
  <ErrorPage>
  <I18nextProvider i18n={i18n}>
    <Provider store={store}>
      <PersistGate loading={<LoadingPage />} persistor={persistor}>
        <React.Suspense fallback={<LoadingPage/>}>
          <Layout />
        </React.Suspense>
      </PersistGate>
    </Provider>
  </I18nextProvider>
  </ErrorPage>
);

ReactDOM.render(<App />, document.getElementById('root'));

