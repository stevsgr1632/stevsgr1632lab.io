# Introduction
Mccp Web is a framework from moco_dashboard_core for __MocoCatalog's Dashboard__ products from various Services, we use React as based because React allows developers to create large web applications which can change data, without reloading the page. The main purpose of React is to be fast, scalable, and simple.

We will have another repository for sample implementation of __mccp-web__, in that implementation we will showcase various case which have been implemented before, at it will always be improved

Mccp Web is based on the following frameworks:
* ANT Design UI 4.x https://ant.design/ 
* and many more ...

# Getting Started #
To get started you just need to
* clone this repository
* ```cd mccp-web```
* ```git checkout develop```
* ```npm install```
* ``npm start``

you can view it on localhost:3000


# Features List #
- [x] Custom Layout
- [x] Multiple Data Provider
- [x] Integration with MBAAS
- [x] Theming
- [x] Internationalization
- [ ] Google Firebase Analytics Integration
